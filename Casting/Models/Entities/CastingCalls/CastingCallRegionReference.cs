﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Locations;
using Casting.Model.Enums;

namespace Casting.Model.Entities.CastingCalls
{
    public class CastingCallRegionReference: Logger
    {
        public int Id { get; set; }
        public int CastingCallId { get; set; }
        public CastingCall CastingCall { get; set; }
        public int RegionId { get; set; }
        public Region Region { get; set; }

    }
}
