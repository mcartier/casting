﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Runtime.CompilerServices;
using Casting.DAL.Configurations;


using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Sessions;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Entities.Users.Guests;
using Casting.Model.Entities.Users.Talents;
using Microsoft.AspNet.Identity.EntityFramework;

using Casting.Model.Entities.AttributeReferences;


using Casting.Model.Entities.Attributes;

namespace Casting.DAL
{
    public class CastingContext : IdentityDbContext<ApplicationUser>, ICastingContext
    {

        //locations

        public DbSet<Country> Countries { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Region> Regions { get; set; }
        //personas

        public DbSet<Persona> Personas { get; set; }
        public DbSet<PersonaReference> PersonaReferences { get; set; }
        public DbSet<CastingCallRoleSubmissionPersonaReference> RoleSubmissionPersonaReferences { get; set; }
        public DbSet<CastingCallFolderRoleSubmissionPersonaReference> FolderRoleSubmissionPersonaReferences { get; set; }
        public DbSet<CastingSessionFolderRoleSelectionPersonaReference> FolderRoleSelectionPersonaReferences { get; set; }
        public DbSet<CastingSessionRoleSelectionPersonaReference> RoleSelectionPersonaReferences { get; set; }

        public DbSet<BaseCastingUser> BaseUsers { get; set; }
        // public DbSet<UserReference> UserReferences { get; set; }

       // public DbSet<TalentProfileReference> TalentProfileReferences { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ActorProfile> ActorProfiles { get; set; }
        public DbSet<VoiceActorProfile> VoiceActorProfiles { get; set; }
        public DbSet<TalentHighlight> TalentHighlights { get; set; }
        public DbSet<Training> Trainings { get; set; }
        public DbSet<TalentWorkCredit> TalentWorkCredits { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Director> Directors { get; set; }

        public DbSet<Agent> Agents { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<AgencyOffice> AgencyOffices { get; set; }
        public DbSet<AgentCityReference> AgentCityReferences { get; set; }
        public DbSet<AgentTalentReference> AgentTalentReferences { get; set; }

        //talents
        public DbSet<Talent> Talents { get; set; }
        public DbSet<HouseTalent> HouseTalents { get; set; }
        public DbSet<StudioTalent> StudioTalents { get; set; }
       // public DbSet<HouseTalentPersonaReference> HouseTalentPersonaReferences { get; set; }
       // public DbSet<StudioTalentPersonaReference> StudioTalentPersonaReferences { get; set; }

        //assets
        public DbSet<Audio> Audios { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<Image> Images { get; set; }

        //asset references
        public DbSet<AudioReference> AudioReferences { get; set; }
        public DbSet<VideoReference> VideoReferences { get; set; }
        public DbSet<ResumeReference> ResumeReferences { get; set; }
        public DbSet<ImageReference> ImageReferences { get; set; }

        //persona asset references
        public DbSet<PersonaAudioReference> PersonaAudioReferences { get; set; }
        public DbSet<PersonaVideoReference> PersonaVideoReferences { get; set; }
        public DbSet<PersonaResumeReference> PersonaResumeReferences { get; set; }
        public DbSet<PersonaImageReference> PersonaImageReferences { get; set; }

        //Basket asset references
        public DbSet<BasketAudioReference> BasketAudioReferences { get; set; }
        public DbSet<BasketVideoReference> BasketVideoReferences { get; set; }
        public DbSet<BasketResumeReference> BasketResumeReferences { get; set; }
        public DbSet<BasketImageReference> BasketImageReferences { get; set; }

        //Profile asset references
        public DbSet<ProfileAudioReference> ProfileAudioReferences { get; set; }
        public DbSet<ActorProfileVideoReference> ProfileVideoReferences { get; set; }
        public DbSet<ProfileResumeReference> ProfileResumeReferences { get; set; }
        public DbSet<ActorProfileImageReference> ProfileImageReferences { get; set; }
       // public DbSet<ProfileMainImageReference> ProfileMainImageReferences { get; set; }

        //productions
        public DbSet<Production> Productions { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<ShootDetail> ShootDetails { get; set; }
        public DbSet<AuditionDetail> AuditionDetails { get; set; }
        public DbSet<PayDetail> PayDetails { get; set; }
        public DbSet<Side> Sides { get; set; }
        public DbSet<File> Files { get; set; }

        //production metadata
        public DbSet<ProductionTypeCategory> ProductionTypeCategories { get; set; }

        public DbSet<ProductionType> ProductionTypes { get; set; }
        public DbSet<RoleType> RoleTypes { get; set; }
        public DbSet<RoleGenderType> RoleGenderTypes { get; set; }

        //profile attributes
        public DbSet<Union> Unions { get; set; }
        public DbSet<EthnicStereoType> EthnicStereoTypes { get; set; }
        public DbSet<Ethnicity> Ethnicities { get; set; }
        public DbSet<StereoType> StereoTypes { get; set; }
        public DbSet<Accent> Accents { get; set; }
        public DbSet<Language> Languages { get; set; }

        public DbSet<HairColor> HairColors { get; set; }
        public DbSet<EyeColor> EyeColors { get; set; }
        public DbSet<BodyType> BodyTypes { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillCategory> SkillCategories { get; set; }

        // attribute references
        public DbSet<UnionReference> UnionReferences { get; set; }
        public DbSet<EthnicStereoTypeReference> EthnicStereoTypeReferences { get; set; }
        public DbSet<EthnicityReference> EthnicityReferences { get; set; }
        public DbSet<StereoTypeReference> StereoTypeReferences { get; set; }
        public DbSet<AccentReference> AccentReferences { get; set; }
        public DbSet<LanguageReference> LanguageReferences { get; set; }
        public DbSet<EyeColorReference> EyeColorReferences { get; set; }
        public DbSet<HairColorReference> HairColorReferences { get; set; }
        public DbSet<BodyTypeReference> BodyTypeReferences { get; set; }
        public DbSet<SkillReference> SkillReferences { get; set; }

        //profile attribute references
        public DbSet<ProfileUnionReference> ProfileUnionReferences { get; set; }
        public DbSet<ProfileEthnicStereoTypeReference> ProfileEthnicStereoTypeReferences { get; set; }
        public DbSet<ProfileEthnicityReference> ProfileEthnicityReferences { get; set; }
        public DbSet<ProfileStereoTypeReference> ProfileStereoTypeReferences { get; set; }
        public DbSet<ProfileAccentReference> ProfileAccentReferences { get; set; }
        public DbSet<ProfileLanguageReference> ProfileLanguageReferences { get; set; }
        public DbSet<ProfileEyeColorReference> ProfileEyeColorReferences { get; set; }
        public DbSet<ProfileHairColorReference> ProfileHairColorReferences { get; set; }
        public DbSet<ProfileBodyTypeReference> ProfileBodyTypeReferences { get; set; }
        public DbSet<ProfileSkillReference> ProfileSkillReferences { get; set; }


        //role attribute references
       public DbSet<RoleEthnicStereoTypeReference> RoleEthnicStereoTypeReferences { get; set; }
        public DbSet<RoleEthnicityReference> RoleEthnicityReferences { get; set; }
        public DbSet<RoleStereoTypeReference> RoleStereoTypeReferences { get; set; }
        public DbSet<RoleAccentReference> RoleAccentReferences { get; set; }
        public DbSet<RoleLanguageReference> RoleLanguageReferences { get; set; }
        public DbSet<RoleEyeColorReference> RoleEyeColorReferences { get; set; }
        public DbSet<RoleHairColorReference> RoleHairColorReferences { get; set; }
        public DbSet<RoleBodyTypeReference> RoleBodyTypeReferences { get; set; }
        public DbSet<RoleSkillReference> RoleSkillReferences { get; set; }

        //castingCall attribute references
        public DbSet<CastingCallUnionReference> CastingCallUnionReferences { get; set; }
        public DbSet<CastingCallRoleUnionReference> CastingCallRoleUnionReferences { get; set; }
        //profile search creiteria attribute references
        /* public DbSet<ProfileSearchCriteriaUnionReference> ProfileSearchCriteriaUnionReferences { get; set; }
         public DbSet<ProfileSearchCriteriaEthnicStereoTypeReference> ProfileSearchCriteriaEthnicStereoTypeReferences { get; set; }
         public DbSet<ProfileSearchCriteriaEthnicityReference> ProfileSearchCriteriaEthnicityReferences { get; set; }
         public DbSet<ProfileSearchCriteriaStereoTypeReference> ProfileSearchCriteriaStereoTypeReferences { get; set; }
         public DbSet<ProfileSearchCriteriaAccentReference> ProfileSearchCriteriaAccentReferences { get; set; }
         public DbSet<ProfileSearchCriteriaLanguageReference> ProfileSearchCriteriaLanguageReferences { get; set; }
         */
        //castingCalls
        public DbSet<CastingCall> CastingCalls { get; set; }
        public DbSet<CastingCallPrivacy> CastingCallPrivacies { get; set; }
        public DbSet<CastingCallViewType> CastingCallViewTypes { get; set; }
        public DbSet<CastingCallType> CastingCallTypes { get; set; }

        public DbSet<CastingCallRole> CastingCallRoles { get; set; }
        public DbSet<CastingCallFolder> CastingCallFolders { get; set; }
        public DbSet<CastingCallFolderRole> CastingCallFolderRoles { get; set; }

        public DbSet<CastingCallCityReference> CastingCallCityReferences { get; set; }
        public DbSet<CastingCallRegionReference> CastingCallRegionReferences { get; set; }
        public DbSet<CastingCallCountryReference> CastingCallCountryReferences { get; set; }
       
        //casting sessions

        public DbSet<CastingSession> CastingSessions { get; set; }

        public DbSet<CastingSessionRole> CastingSessionRoles { get; set; }

        public DbSet<CastingSessionFolder> CastingSessionFolders { get; set; }

        public DbSet<CastingSessionFolderRole> CastingSessionFolderRoles { get; set; }

        public CastingContext()
            : base("Casting")
        {
            Configure();
            ((IObjectContextAdapter)this).ObjectContext
                .ObjectMaterialized += (sender, args) =>
                {
                    var entity = args.Entity as IObjectWithState;
                    if (entity != null)
                    {
                        entity.State = State.Unchanged;
                    }
                };
        }
        public CastingContext(string connectionString)
            : base("Casting")
        {
            Configure();
            ((IObjectContextAdapter)this).ObjectContext
                .ObjectMaterialized += (sender, args) =>
                {
                    var entity = args.Entity as IObjectWithState;
                    if (entity != null)
                    {
                        entity.State = State.Unchanged;
                    }
                };
        }
        public static CastingContext Create()
        {
            return new CastingContext();
        }
        private void Configure()
        {
            System.Data.Entity.Database.SetInitializer<CastingContext>(new CastingDBInitializer());
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = true;
            this.Configuration.ValidateOnSaveEnabled = true;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //locations
            modelBuilder.Configurations.Add(new CountryConfiguration());
            modelBuilder.Configurations.Add(new CityConfiguration());
            modelBuilder.Configurations.Add(new RegionConfiguration());

            modelBuilder.Configurations.Add(new BasketConfiguration());
            modelBuilder.Configurations.Add(new PersonaConfiguration());
            //  modelBuilder.Configurations.Add(new HouseTalentPersonaConfiguration());
            // modelBuilder.Configurations.Add(new StudioTalentPersonaConfiguration());

            //modelBuilder.Configurations.Add(new TalentProfileReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileConfiguration());
            modelBuilder.Configurations.Add(new ActorProfileConfiguration());
            modelBuilder.Configurations.Add(new VoiceActorProfileConfiguration());
            modelBuilder.Configurations.Add(new TalentHighlightConfiguration());
            modelBuilder.Configurations.Add(new TrainingConfiguration());
            modelBuilder.Configurations.Add(new TalentWorkCreditConfiguration());

            modelBuilder.Configurations.Add(new PersonaReferenceConfiguration());
            //modelBuilder.Configurations.Add(new HouseTalentPersonaReferenceConfiguration());
            //  modelBuilder.Configurations.Add(new StudioTalentPersonaReferenceConfiguration());
            modelBuilder.Configurations.Add(new CastingCallFolderRoleSubmissionPersonaReferenceConfiguration());
            modelBuilder.Configurations.Add(new CastingCallRoleSubmissionPersonaReferenceConfiguration());
            modelBuilder.Configurations.Add(new CastingSessionFolderRoleSelectionPersonaReferenceConfiguration());
            modelBuilder.Configurations.Add(new CastingSessionRoleSelectionPersonaReferenceConfiguration());


            modelBuilder.Configurations.Add(new TalentConfiguration());
            modelBuilder.Configurations.Add(new HouseTalentConfiguration());
            modelBuilder.Configurations.Add(new StudioTalentConfiguration());

            modelBuilder.Configurations.Add(new ApplicationUserConfiguration());
            //modelBuilder.Configurations.Add(new IdentityUserLoginConfiguration());
            // modelBuilder.Configurations.Add(new IdentityUserRoleConfiguration());

           // modelBuilder.Configurations.Add(new UserReferenceConfiguration());
            modelBuilder.Configurations.Add(new BaseUserConfiguration());
            modelBuilder.Configurations.Add(new GuestConfiguration());
            modelBuilder.Configurations.Add(new DirectorConfiguration());
            modelBuilder.Configurations.Add(new AgentTalentReferenceConfiguration());
            modelBuilder.Configurations.Add(new AgentCityReferenceConfiguration());
            modelBuilder.Configurations.Add(new AgentConfiguration());
            modelBuilder.Configurations.Add(new AgencyConfiguration());
            modelBuilder.Configurations.Add(new AgencyAddressConfiguration());

            modelBuilder.Configurations.Add(new AudioConfiguration());
            modelBuilder.Configurations.Add(new AudioReferenceConfiguration());
            modelBuilder.Configurations.Add(new PersonaAudioReferenceConfiguration());
            modelBuilder.Configurations.Add(new BasketAudioReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileAudioReferenceConfiguration());

            modelBuilder.Configurations.Add(new VideoConfiguration());
            modelBuilder.Configurations.Add(new VideoReferenceConfiguration());
            modelBuilder.Configurations.Add(new PersonaVideoReferenceConfiguration());
            modelBuilder.Configurations.Add(new BasketVideoReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileVideoReferenceConfiguration());

            modelBuilder.Configurations.Add(new ResumeConfiguration());
            modelBuilder.Configurations.Add(new ResumeReferenceConfiguration());
            modelBuilder.Configurations.Add(new PersonaResumeReferenceConfiguration());
            modelBuilder.Configurations.Add(new BasketResumeReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileResumeReferenceConfiguration());

            modelBuilder.Configurations.Add(new ImageConfiguration());
            modelBuilder.Configurations.Add(new ImageReferenceConfiguration());
            modelBuilder.Configurations.Add(new PersonaImageReferenceConfiguration());
            modelBuilder.Configurations.Add(new BasketImageReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileImageReferenceConfiguration());
           // modelBuilder.Configurations.Add(new ProfileMainImageReferenceConfiguration());
            

            modelBuilder.Configurations.Add(new ProductionConfiguration());
            modelBuilder.Configurations.Add(new RoleConfiguration());

            modelBuilder.Configurations.Add(new CastingCallConfiguration());
            modelBuilder.Configurations.Add(new CastingCallRoleConfiguration());
            modelBuilder.Configurations.Add(new CastingCallFolderConfiguration());
            modelBuilder.Configurations.Add(new CastingCallFolderRoleConfiguration());

            modelBuilder.Entity<CastingCallPrivacy>().HasKey(k => k.Id);
            modelBuilder.Entity<CastingCallType>().HasKey(k => k.Id);
            modelBuilder.Entity<CastingCallViewType>().HasKey(k => k.Id);

            modelBuilder.Configurations.Add(new CastingSessionConfiguration());
            modelBuilder.Configurations.Add(new CastingSessionRoleConfiguration());
            modelBuilder.Configurations.Add(new CastingSessionFolderConfiguration());
            modelBuilder.Configurations.Add(new CastingSessionFolderRoleConfiguration());

            modelBuilder.Configurations.Add(new ProductionTypeConfiguration());
            modelBuilder.Configurations.Add(new ProductionTypeCategoryConfiguration());


            modelBuilder.Entity<RoleGenderType>().HasKey(k => k.Id);
            modelBuilder.Entity<RoleType>().HasKey(k => k.Id);

            modelBuilder.Configurations.Add(new EthnicStereoTypeConfiguration());
            modelBuilder.Configurations.Add(new EthnicityConfiguration());
            modelBuilder.Configurations.Add(new StereoTypeConfiguration());
            modelBuilder.Configurations.Add(new UnionConfiguration());
            modelBuilder.Configurations.Add(new AccentConfiguration());
            modelBuilder.Configurations.Add(new LanguageConfiguration());
            modelBuilder.Configurations.Add(new SkillConfiguration());
            modelBuilder.Configurations.Add(new SkillCategoryConfiguration());

            modelBuilder.Configurations.Add(new EthnicStereoTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new EthnicityReferenceConfiguration());
            modelBuilder.Configurations.Add(new StereoTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new UnionReferenceConfiguration());
            modelBuilder.Configurations.Add(new AccentReferenceConfiguration());
            modelBuilder.Configurations.Add(new LanguageReferenceConfiguration());
            modelBuilder.Configurations.Add(new EyeColorReferenceConfiguration());
            modelBuilder.Configurations.Add(new HairColorReferenceConfiguration());
            modelBuilder.Configurations.Add(new BodyTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new SkillReferenceConfiguration());

            modelBuilder.Configurations.Add(new ProfileEthnicStereoTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileEthnicityReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileStereoTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileUnionReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileAccentReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileLanguageReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileEyeColorReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileHairColorReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileBodyTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new ProfileSkillReferenceConfiguration());

            modelBuilder.Configurations.Add(new RoleEthnicStereoTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleEthnicityReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleStereoTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleAccentReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleLanguageReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleEyeColorReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleHairColorReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleBodyTypeReferenceConfiguration());
            modelBuilder.Configurations.Add(new RoleSkillReferenceConfiguration());

            modelBuilder.Configurations.Add(new CastingCallUnionReferenceConfiguration());
            modelBuilder.Configurations.Add(new CastingCallRoleUnionReferenceConfiguration());
            /* modelBuilder.Configurations.Add(new ProfileSearchCriteriaEthnicStereoTypeReferenceConfiguration());
             modelBuilder.Configurations.Add(new ProfileSearchCriteriaEthnicityReferenceConfiguration());
             modelBuilder.Configurations.Add(new ProfileSearchCriteriaStereoTypeReferenceConfiguration());
             modelBuilder.Configurations.Add(new ProfileSearchCriteriaUnionReferenceConfiguration());
             modelBuilder.Configurations.Add(new ProfileSearchCriteriaAccentReferenceConfiguration());
             modelBuilder.Configurations.Add(new ProfileSearchCriteriaLanguageReferenceConfiguration());
             */
        }
        public bool LogChangesDuringSave { get; set; }

        Database ICastingContext.Database
        {
            get
            {
                return base.Database;
            }
            set
            {

            }

          
        }

        private void PrintPropertyValues(
          DbPropertyValues values,
          IEnumerable<string> propertiesToPrint,
          int indent = 1)
        {
            foreach (var propertyName in propertiesToPrint)
            {
                var value = values[propertyName];
                if (value is DbPropertyValues)
                {
                    Console.WriteLine(
                      "{0}- Complex Property: {1}",
                      string.Empty.PadLeft(indent),
                      propertyName);

                    var complexPropertyValues = (DbPropertyValues)value;
                    PrintPropertyValues(
                      complexPropertyValues,
                      complexPropertyValues.PropertyNames,
                      indent + 1);
                }
                else
                {
                    Console.WriteLine(
                      "{0}- {1}: {2}",
                      string.Empty.PadLeft(indent),
                      propertyName,
                      values[propertyName]);
                }
            }
        }

        private IEnumerable<string> GetKeyPropertyNames(object entity)
        {
            var objectContext = ((IObjectContextAdapter)this).ObjectContext;
            return objectContext
            .ObjectStateManager
            .GetObjectStateEntry(entity)
            .EntityKey
            .EntityKeyValues
            .Select(k => k.Key);
        }

        protected override DbEntityValidationResult ValidateEntity(
          DbEntityEntry entityEntry,
          IDictionary<object, object> items)
        {
            var _items = new Dictionary<object, object>();
            return base.ValidateEntity(entityEntry, _items);
        }

        private static void ApplyLoggingData(DbEntityEntry entityEntry)
        {
            var logger = entityEntry.Entity as Logger;
            if (logger == null) return;
            logger.UpdateModificationLogValues(entityEntry.State == EntityState.Added);
        }

        public override int SaveChanges()
        {
            var autoDetectChanges = Configuration.AutoDetectChangesEnabled;

            try
            {
                Configuration.AutoDetectChangesEnabled = false;
                ChangeTracker.DetectChanges();

                var errors = GetValidationErrors().ToList();
                if (errors.Any())
                {
                    throw new DbEntityValidationException("Validation errors found during save.", errors);
                }

                foreach (var entity in this.ChangeTracker
                  .Entries()
                  .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
                {
                    ApplyLoggingData(entity);
                }
                ChangeTracker.DetectChanges();

                Configuration.ValidateOnSaveEnabled = false;
                return base.SaveChanges();
            }
            finally
            {
                Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            }
        }


        public override Task<int> SaveChangesAsync()
        {
            var autoDetectChanges = Configuration.AutoDetectChangesEnabled;

            try
            {
                Configuration.AutoDetectChangesEnabled = false;
                ChangeTracker.DetectChanges();

                var errors = GetValidationErrors().ToList();
                if (errors.Any())
                {
                    throw new DbEntityValidationException("Validation errors found during save.", errors);
                }

                foreach (var entity in this.ChangeTracker
                    .Entries()
                    .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
                {
                    ApplyLoggingData(entity);
                }

                ChangeTracker.DetectChanges();

                Configuration.ValidateOnSaveEnabled = false;
                return base.SaveChangesAsync();
            }
            
            finally
            {
                Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            }
        }

        protected override bool ShouldValidateEntity(DbEntityEntry entityEntry)
        {
            return base.ShouldValidateEntity(entityEntry)
              || (entityEntry.State == EntityState.Deleted
                  && entityEntry.Entity is Talent);
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public void SetState(object entity, EntityState state)
        {
            Entry(entity).State = state;
        }

      
    }
}
