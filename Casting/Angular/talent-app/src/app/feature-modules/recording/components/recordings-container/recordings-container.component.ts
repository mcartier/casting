import { FileUploadService } from '../../../../services/file-upload.service';
import { Component, OnDestroy, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { VideoBlobService } from '../../../../services/video-blob.service';
import { BlobUploadState, StudioTalentBlob } from '../../../../models/blobs';
//import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NewRecordingComponent } from '../new-recording/new-recording.component'
import { UUID } from 'angular2-uuid';
import { MatDialog } from '@angular/material';
import { ChangeDetectorRef } from '@angular/core';
@Component({
    selector: 'app-recordings-container',
    templateUrl: './recordings-container.component.html',
    styleUrls: ['./recordings-container.component.scss']
})
export class RecordingsContainerComponent implements OnDestroy, AfterViewInit, OnInit {

    constructor(private dialog: MatDialog, private videoBlobService: VideoBlobService) { }
    private newBlobCreatedSubscription: Subscription;
    private blobsSubscription: Subscription;
    blobs: StudioTalentBlob[];
    private newBlob: StudioTalentBlob;
    private videoBlob: StudioTalentBlob;
    private blobDataURL: any;
    openDialog(): void {
        const dialogRef = this.dialog.open(NewRecordingComponent,
            {
                width: '90%',
                maxWidth:'600px',
                data: this.newBlob

            });
        /* dialogRef.afterClosed().subscribe(result => {
           console.log('The dialog was closed');
           //this.animal = result;
         });*/
    }
    ngOnInit() {
        this.videoBlobService.registerNewBlogRecordingObserver(this.processNewBlobCreated.bind(this));
    }
    private processNewBlobCreated(newBlob: StudioTalentBlob) {

        var headers: any;
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.access_token) {
                headers =
                    {
                        Authorization: `Bearer ${currentUser.access_token.replace(" ", "")}`
                    }

            }
        }
        let metadata = {
            name: "upload.webm",
            type: "webm/video",
            contentType: "video/*"
        }
        this.newBlob = newBlob;// new StudioTalentBlob(UUID.UUID(), newBlob, "https://cast.com/fileso", headers, metadata);

        this.openDialog();
    }

    private processBlobs(blobs: StudioTalentBlob[]) {
        // this.blobs = blobs;

    }

    ngOnDestroy() {
        // this.newBlobCreatedSubscription.unsubscribe();
        this.videoBlobService.unRegisterNewBlogRecordingObserver();
        //this.blobsSubscription.unsubscribe();
    }
    ngAfterViewInit() {

    }

}
