import { TestBed } from '@angular/core/testing';

import { VideoRecorderService } from './video-recorder.service';

describe('VideoRecorderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideoRecorderService = TestBed.get(VideoRecorderService);
    expect(service).toBeTruthy();
  });
});
