﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Guests;


namespace Casting.DAL.Configurations
{
    class GuestConfiguration : EntityTypeConfiguration<Guest>
    {
        public GuestConfiguration()
        {
            ToTable("Guest");
            HasKey(x => x.Id);
            //HasRequired(t => t.Talent)
            //     .WithOptional(p => p.Profile);
            //  HasRequired(u => u.ApplicationUser)
            //     .WithMany(a => a.Guests)
            //     .HasForeignKey(k => k.UserId);
            //  Property(r => r.UserId).HasMaxLength(128).IsRequired();

        }
    }
    class BaseUserConfiguration : EntityTypeConfiguration<BaseCastingUser>
    {
        public BaseUserConfiguration()
        {
            HasKey(x => x.Id);
            //HasOptional(u => u.User)
            //    .WithOptionalPrincipal(b => b.BaseCastingUser);
            //HasOptional(t => t.UserReference)
            //     .WithRequired(p => p.BaseCastingUser)
            //     .WillCascadeOnDelete(false);
            // Property(i => i.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //     .WithOptional(p => p.Profile);
            //  HasRequired(u => u.ApplicationUser)
            //     .WithMany(a => a.Guests)
            //     .HasForeignKey(k => k.UserId);
            //  Property(r => r.UserId).HasMaxLength(128).IsRequired();

        }
    }
  /*  class UserReferenceConfiguration : EntityTypeConfiguration<UserReference>
    {
        public UserReferenceConfiguration()
        {
           // HasKey(x => x.ApplicationUserId);
           // HasRequired(t => t.ApplicationUser)
              //  .WithOptional(p => p.UserReference);
                //.WillCascadeOnDelete(false);
               // HasRequired(u => u.BaseCastingUser);
                //   .WithOptional(r => r.UserReference);
                //.WillCascadeOnDelete(false);
                //     .WithOptional(p => p.Profile);
                //  HasRequired(u => u.ApplicationUser)
                //     .WithMany(a => a.Guests)
                //     .HasForeignKey(k => k.UserId);
                //  Property(r => r.UserId).HasMaxLength(128).IsRequired();

        }
    }*/
}
;