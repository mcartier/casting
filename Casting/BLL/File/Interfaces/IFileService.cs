﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{

    public interface IFileService
    {
      

        #region sidefiles

        Task<File> GetFileByFileIdAsync(string fileId);
       Task<int> AddNewFileAsync(File file);
        Task<int> DeleteFileAsync(File file);
        #endregion
    }
}