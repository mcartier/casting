import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Subscription } from 'rxjs';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { RoleDialogService } from '../../services/role-dialog.service';
import { ConfirmationDialogService } from '../../../../services/confirmation-dialog.service';
import { ProductionRoleService } from '../../../../services/production-role.service';

@Component({
  selector: 'app-role-list-item',
  templateUrl: './role-list-item.component.html',
    styleUrls: ['./role-list-item.component.scss'],
    host: { 'class': 'p-col' }
})
export class RoleListItemComponent implements OnInit, OnDestroy {

    constructor(private ProductionRoleService: ProductionRoleService, private confirmationService: ConfirmationDialogService, private roleDialogService: RoleDialogService,
      public stateService: StateService,
      public transitionService: TransitionService,
      public transition: Transition) { }
  @Input() production: Production;
    @Input() role: any;
    private editRoleSubscription: Subscription;
    private deleteRoleSubscription: Subscription;

    ngOnInit() {
      }
    ngOnDestroy() {
      if (this.editRoleSubscription) {
        this.editRoleSubscription.unsubscribe();

      }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();

      }

    }

    private editRoleClick(role: Role) {

        if (1 == 1) {
            this.stateService.go("editrole", { production: this.production, productionId: this.production.id, role: role, roleId: role.id });

        } else {
          //setTimeout(() => {
          this.editRoleSubscription = this.roleDialogService.editRole(this.production, this.role)
            .subscribe(
              role => {
                if (role) {
                  this.role = role;
                }
                this.editRoleSubscription.unsubscribe();
              });

          //});
        }
     

    }
    private deleteRoleClick(role: Role) {

      //setTimeout(() => {
        this.deleteRoleSubscription = this.confirmationService.confirmAction("Do you really want to delete role '" + this.role.name + "' from production '" + this.production.name + "'?")
        .subscribe(
          result => {
              if (result) {
                  this.ProductionRoleService.deleteRole(this.role.id,  this.role.productionId).subscribe((role: Role) => {
                      
                    },
                    (error) => {
                      console.log("error " + error)
                    })
            }
              this.deleteRoleSubscription.unsubscribe();
          });

      //});

    }
}
