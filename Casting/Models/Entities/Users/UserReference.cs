﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Identity
{
   public class UserReference
    {
        //  public int Id { get; set; }
        // [Key, Column(Order = 0), ForeignKey("ApplicationUser")]
        [ForeignKey("ApplicationUser")]
        public string ApplicationUserId { get; set; }
       // [ForeignKey("ApplicationUserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
        [ForeignKey("BaseUser")]
        public int BaseUserId { get; set; }
       public virtual BaseUser BaseUser { get; set; }
    }
}
