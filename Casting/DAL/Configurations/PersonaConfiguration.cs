﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;

using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;


namespace Casting.DAL.Configurations
{
    class PersonaConfiguration : EntityTypeConfiguration<Persona>
    {
        public PersonaConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(p => p.Profile)
               .WithMany(p => p.Personas)
                .HasForeignKey(k => k.ProfileId);
             
           // HasRequired(t => t.Talent)
             //   .WithMany(p => p.Personas)
             //   .HasForeignKey(k => k.TalentId);
            HasMany(h => h.ImageReferences)
                .WithRequired(h => h.Persona)
                .HasForeignKey(p => p.PersonaId);
            HasMany(a => a.ResumeReferences)
                .WithRequired(p => p.Persona)
                .HasForeignKey(p => p.PersonaId);
            HasMany(a => a.AudioReferences)
                .WithRequired(p => p.Persona)
                .HasForeignKey(p => p.PersonaId);
            HasMany(a => a.VideoReferences)
                .WithRequired(p => p.Persona)
                .HasForeignKey(p => p.PersonaId);
            HasOptional(g => g.Gender);
           // HasMany(u => u.Unions);
           // HasMany(e => e.EthnicStereoTypes);
          //  HasMany(e => e.Ethnicities);
          //  HasMany(s => s.StereoTypes);
            ////HasMany(r => r.PersonaReferences)
            //    .WithRequired(p => p.Persona)
            //    .HasForeignKey(k => k.PersonaId);
            //   HasOptional(a => a.SubmittingAgent);
            // HasOptional(c => c.ContactAgent);
        }
    }
   /* class HouseTalentPersonaConfiguration : EntityTypeConfiguration<HouseTalentPersona>
    {
        public HouseTalentPersonaConfiguration()
        {
            ToTable("HouseTalentPersona");
            HasKey(x => x.Id);
            HasRequired(t => t.Talent)
                .WithMany(p=>p.Personas)
                .HasForeignKey(k=>k.TalentId);
           


        }
    }
    class StudioTalentPersonaConfiguration : EntityTypeConfiguration<StudioTalentPersona>
    {
        public StudioTalentPersonaConfiguration()
        {
            ToTable("StudioTalentPersona");
            HasKey(x => x.Id);
            HasRequired(t => t.Talent)
                .WithMany(p => p.Personas)
                .HasForeignKey(k => k.TalentId);

        }
    }*/
    class PersonaReferenceConfiguration : EntityTypeConfiguration<PersonaReference>
    {
        public PersonaReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(t => t.Persona)
                .WithMany(r=>r.PersonaReferences)
                .HasForeignKey(k=>k.PersonaId);

        }
    }
    class CastingCallFolderRoleSubmissionPersonaReferenceConfiguration : EntityTypeConfiguration<CastingCallFolderRoleSubmissionPersonaReference>
    {
        public CastingCallFolderRoleSubmissionPersonaReferenceConfiguration()
        {
            ToTable("CastingCallFolderRoleSubmissionPersonaReference");
            HasKey(x => x.Id);
            HasRequired(t => t.Persona);
            HasRequired(r => r.CastingCallFolderRole)
                .WithMany(r => r.Personas)
                .HasForeignKey(k => k.CastingCallFolderRoleId);

        }
    }

    class CastingCallRoleSubmissionPersonaReferenceConfiguration : EntityTypeConfiguration<CastingCallRoleSubmissionPersonaReference>
    {
        public CastingCallRoleSubmissionPersonaReferenceConfiguration()
        {
            ToTable("CastingCallRoleSubmissionPersonaReference");
            HasKey(x => x.Id);
            HasRequired(t => t.Persona);
            HasRequired(r => r.CastingCallRole)
                .WithMany(r => r.Personas)
             .HasForeignKey(k => k.CastingCallRoleId);

        }
    }
    class CastingSessionFolderRoleSelectionPersonaReferenceConfiguration : EntityTypeConfiguration<CastingSessionFolderRoleSelectionPersonaReference>
    {
        public CastingSessionFolderRoleSelectionPersonaReferenceConfiguration()
        {
            ToTable("CastingSessionFolderRoleSelectionPersonaReference");
            HasKey(x => x.Id);
            HasRequired(t => t.Persona);
            HasRequired(r => r.CastingSessionFolderRole)
                .WithMany(r => r.Personas)
                .HasForeignKey(k => k.CastingSessionFolderRoleId);

        }
    }

    class CastingSessionRoleSelectionPersonaReferenceConfiguration : EntityTypeConfiguration<CastingSessionRoleSelectionPersonaReference>
    {
        public CastingSessionRoleSelectionPersonaReferenceConfiguration()
        {
            ToTable("CastingSessionRoleSelectionPersonaReference");
            HasKey(x => x.Id);
            HasRequired(t => t.Persona);
            HasRequired(t => t.CastingSessionRole)
                .WithMany(r => r.Personas)
                .HasForeignKey(k => k.CastingSessionRoleId);

        }
    }
    /*
    class HouseTalentPersonaReferenceConfiguration : EntityTypeConfiguration<HouseTalentPersonaReference>
    {
        public HouseTalentPersonaReferenceConfiguration()
        {
            ToTable("HouseTalentPersonaReference");
            HasKey(x => x.Id);
            HasRequired(t => t.Persona);
            HasRequired(t => t.Talent);
            // .WithOptional(r => r.HouseTalentPersonaReference);


        }
    }
    class StudioTalentPersonaReferenceConfiguration : EntityTypeConfiguration<StudioTalentPersonaReference>
    {
        public StudioTalentPersonaReferenceConfiguration()
        {
            ToTable("StudioTalentPersonaReference");
            HasKey(x => x.Id);
            HasRequired(t => t.Persona);
            HasRequired(t => t.Talent);
            //.WithOptional(r => r.PersonaReference);

        }
    }
    */
}
;