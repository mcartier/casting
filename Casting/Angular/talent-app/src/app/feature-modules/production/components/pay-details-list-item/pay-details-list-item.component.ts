import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { ProductionDialogService } from '../../services/production-dialog.service';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MessageService } from 'primeng/api';
import { ConfirmDecisionSubscriptionService } from '../../../../services/confirm-decision-subscription.service';


@Component({
  selector: 'app-pay-details-list-item',
  templateUrl: './pay-details-list-item.component.html',
  styleUrls: ['./pay-details-list-item.component.scss']
})
export class PayDetailsListItemComponent implements OnInit {

  constructor(
    private productionDialogService: ProductionDialogService,
    private productionExtraService: ProductionExtraService,
    private messageDispatchService: MessageDispatchService,
    private messageService: MessageService,
    private decisionDispatcher: ConfirmDecisionSubscriptionService
  ) { }

  private decisionDispatch: Subscription;
  @Input() detail: PayDetail;

  ngOnInit() {
  }

  ngOnDestroy() {

    if (this.decisionDispatch) {
      this.decisionDispatch.unsubscribe();

    }

  }
  private viewDetails() {
    this.productionDialogService.showPayDetailsDialog(this.detail);

  }
  private deleteDetails() {
    this.decisionDispatcher.poseConfirmDecision('Delete pay detail ' + this.detail.name + '?', 'Confirm to proceed');
    this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
      .subscribe(
        (decision: boolean) => {
          this.decisionDispatch.unsubscribe();

          if (decision) {
            this.productionExtraService.deletePayDetail(this.detail.id, this.detail.productionId).subscribe(
              (payDetail: PayDetail) => {
                //this.payDetail = payDetail;
                this.messageDispatchService.dispatchSuccesMessage("Pay detail '" + this.detail.name + "' was deleted successfully",
                  "Pay Detail Deleted");

              },
              (error) => {
                this.messageDispatchService.dispatchErrorMessage(
                  "Error saving pay detail '" + this.detail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                  "Pay Detail Save Error");
              });

          }
        });


  }

}
