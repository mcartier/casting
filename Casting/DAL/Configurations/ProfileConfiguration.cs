﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;

namespace Casting.DAL.Configurations
{
    public class ProfileConfiguration : EntityTypeConfiguration<Profile>
    {
        public ProfileConfiguration()
        {
            HasKey(x => x.Id);

            HasOptional(t => t.Talent)
                .WithMany(p => p.Profiles);
            HasOptional(a => a.Agent)
                .WithMany(p => p.Profiles);
            HasOptional(a => a.Director)
                .WithMany(p => p.Profiles);
            //HasOptional(t => t.TalentReference)
            //    .WithOptionalDependent(p => p.Profile);

            //HasMany(h => h.ImageReferences)
            // .WithRequired(h => h.Profile)
            // .HasForeignKey(p => p.ProfileId);
            HasMany(a => a.ResumeReferences)
                .WithRequired(p => p.Profile)
                .HasForeignKey(p => p.ProfileId);
            HasMany(a => a.AudioReferences)
                .WithRequired(p => p.Profile)
                .HasForeignKey(p => p.ProfileId);
            //HasMany(a => a.VideoReferences)
                //.WithRequired(p => p.Profile)
               // .HasForeignKey(p => p.ProfileId);
            HasMany(p => p.Personas)
                .WithRequired(t => t.Profile)
               .HasForeignKey(k => k.ProfileId);
           // HasMany(u => u.Languages);
              //  .WithMany(p => p.Profiles);
          //  HasMany(u => u.Unions);
              //  .WithMany(p => p.Profiles);
           // HasMany(u => u.Accents);
               // .WithMany(p => p.Profiles);
            //HasOptional(i => i.MainImageReference)
            //  .WithRequired(p => p.Profile);
            HasMany(h => h.Highlights)
                .WithRequired(p => p.Profile)
                .HasForeignKey(k => k.ProfileId);
            HasMany(h => h.WorkCredits)
                .WithRequired(p => p.Profile)
                .HasForeignKey(k => k.ProfileId);
            HasMany(h => h.Trainings)
                .WithRequired(p => p.Profile)
                .HasForeignKey(k => k.ProfileId);

        }
    }
    public class ActorProfileConfiguration : EntityTypeConfiguration<ActorProfile>
    {
        public ActorProfileConfiguration()
        {
            ToTable("ActorProfile");
            HasKey(x => x.Id);
            HasMany(h => h.ImageReferences)
                .WithRequired(h => h.Profile)
                .HasForeignKey(p => p.ProfileId);
            HasMany(a => a.ResumeReferences);
            HasMany(a => a.AudioReferences);
            HasMany(a => a.VideoReferences)
                .WithRequired(p => p.Profile)
                .HasForeignKey(p => p.ProfileId);
            HasOptional(g => g.Gender);
            HasMany(u => u.Unions);
            HasMany(e => e.EthnicStereoTypes);
            HasMany(p => p.Personas);
           // HasMany(u => u.Ethnicities);
               // .WithMany(p => p.Profiles);
           // HasMany(u => u.EthnicStereoTypes);
               // .WithMany(p => p.Profiles);
          //  HasMany(u => u.StereoTypes);
              //  .WithMany(p => p.Profiles);
              HasOptional(h => h.HairColor);
               // .WithMany(p => p.Profiles)
               // .HasForeignKey(k => k.HairColorId);
            HasOptional(h => h.EyeColor);
               // .WithMany(p => p.Profiles)
              //  .HasForeignKey(k => k.EyeColorId);
            HasOptional(h => h.BodyType);
               // .WithMany(p => p.Profiles)
               // .HasForeignKey(k => k.BodyTypeId);
        }
    }
    public class VoiceActorProfileConfiguration : EntityTypeConfiguration<VoiceActorProfile>
    {
        public VoiceActorProfileConfiguration()
        {
            ToTable("VoiceActorProfile");
            HasKey(x => x.Id);
            HasMany(a => a.ResumeReferences);
            HasMany(a => a.AudioReferences);

        }
    }
    public class TalentHighlightConfiguration : EntityTypeConfiguration<TalentHighlight>
    {
        public TalentHighlightConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(t => t.Profile)
                .WithMany(p => p.Highlights)
                .HasForeignKey(k => k.ProfileId);
        }
    }
    public class TrainingConfiguration : EntityTypeConfiguration<Training>
    {
        public TrainingConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(t => t.Profile)
                .WithMany(p => p.Trainings)
                .HasForeignKey(k => k.ProfileId);
        }
    }
    public class TalentWorkCreditConfiguration : EntityTypeConfiguration<TalentWorkCredit>
    {
        public TalentWorkCreditConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(t => t.Profile)
                .WithMany(p => p.WorkCredits)
                .HasForeignKey(k => k.ProfileId);
        }
    }


    public class TalentProfileReferenceConfiguration : EntityTypeConfiguration<TalentProfileReference>
    {
        public TalentProfileReferenceConfiguration()
        {
            HasKey(x => x.TalentId);
          //  HasRequired(t => t.Talent)
         //       .WithRequiredDependent(p => p.ProfileReference);
         //   HasOptional(p => p.Profile);
            // .WithOptionalDependent();
            // .WithOptional(p => p.Profile);


        }
    }


}
