﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;
using Casting.BLL.AttributeReferences.Interfaces;


namespace Casting.BLL.AttributeReferences
{
    public class AttributeReferenceService : BaseService, IAttributeReferenceService
    {
        private IAttributeReferenceAsyncRepository _attributeReferenceRepository;

        public AttributeReferenceService(IAttributeReferenceAsyncRepository attributeReferenceRepository)
        {
            _attributeReferenceRepository = attributeReferenceRepository;
        }

        #region references

        #region language
        public async Task<LanguageReference> GetLanguageReferenceByIdAsync(int languageId)
        {
            var language = await _attributeReferenceRepository.GetLanguageReferenceByIdAsync(true, languageId);
            return language;
        }
        public async Task<ProfileLanguageReference> GetProfileLanguageReferenceByIdAsync(int languageId)
        {
            var language = await _attributeReferenceRepository.GetProfileLanguageReferenceByIdAsync(true, languageId);
            return language;
        }

        public async Task<RoleLanguageReference> GetRoleLanguageReferenceByIdAsync(int languageId)
        {
            var language = await _attributeReferenceRepository.GetRoleLanguageReferenceByIdAsync(true, languageId);
            return language;
        }
        public async Task<IEnumerable<ProfileLanguageReference>> GetProfileLanguageReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var languageReferences = await _attributeReferenceRepository.GetLanguageReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileLanguageReference>)languageReferences;
        }
        public async Task<IEnumerable<RoleLanguageReference>> GetRoleLanguageReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var languageReferences = await _attributeReferenceRepository.GetLanguageReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleLanguageReference>)languageReferences;
        }
        public async Task<IEnumerable<LanguageReference>> GetCastingCallLanguageReferencesForParentAsync(int castingCallId, bool withAttributes)
        {
            var languageReferences = await _attributeReferenceRepository.GetLanguageReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
            return (IEnumerable<LanguageReference>)languageReferences;
        }
        public async Task<int> AddNewLanguageAttributeReferenceAsync(bool orderAtTheTop,
         LanguageReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewLanguageReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewLanguageAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, LanguageReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewLanguageReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileLanguageAttributeReferenceAsync(bool orderAtTheTop, ProfileLanguageReference attributeReference)
        {
            return await AddNewLanguageAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewRoleLanguageAttributeReferenceAsync(bool orderAtTheTop, RoleLanguageReference attributeReference)
        {
            return await AddNewLanguageAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteLanguageAttributeReferenceAsync(int parentId, LanguageReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteLanguageReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileLanguageAttributeReferenceAsync(ProfileLanguageReference attributeReference)
        {


            return await DeleteLanguageAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> DeleteRoleLanguageAttributeReferenceAsync(RoleLanguageReference attributeReference)
        {


            return await DeleteLanguageAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> UpdateLanguageAttributeReferenceAsync(int parentId,
            LanguageReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateLanguageReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileLanguageAttributeReferenceAsync(ProfileLanguageReference attributeReference)
        {
            return await UpdateLanguageAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> UpdateRoleLanguageAttributeReferenceAsync(RoleLanguageReference attributeReference)
        {
            return await UpdateLanguageAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderLanguageAttributeAsync(int parentId,
            int desiredOrderIndex, LanguageReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderLanguageAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }
        public async Task<int> ReOrderProfileLanguageAttributeAsync(int desiredOrderIndex, ProfileLanguageReference attributeReference)


        {
            return await ReOrderLanguageAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderRoleLanguageAttributeAsync(int desiredOrderIndex, RoleLanguageReference attributeReference)


        {
            return await ReOrderLanguageAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion
        #region accent
        public async Task<AccentReference> GetAccentReferenceByIdAsync(int accentId)
        {
            var accent = await _attributeReferenceRepository.GetAccentReferenceByIdAsync(true, accentId);
            return accent;
        }
        public async Task<ProfileAccentReference> GetProfileAccentReferenceByIdAsync(int accentId)
        {
            var accent = await _attributeReferenceRepository.GetProfileAccentReferenceByIdAsync(true, accentId);
            return accent;
        }

        public async Task<RoleAccentReference> GetRoleAccentReferenceByIdAsync(int accentId)
        {
            var accent = await _attributeReferenceRepository.GetRoleAccentReferenceByIdAsync(true, accentId);
            return accent;
        }
        public async Task<IEnumerable<ProfileAccentReference>> GetProfileAccentReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var accentReferences = await _attributeReferenceRepository.GetAccentReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileAccentReference>)accentReferences;
        }
        public async Task<IEnumerable<RoleAccentReference>> GetRoleAccentReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var accentReferences = await _attributeReferenceRepository.GetAccentReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleAccentReference>)accentReferences;
        }
        public async Task<int> AddNewAccentAttributeReferenceAsync(bool orderAtTheTop,
       AccentReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewAccentReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewAccentAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, AccentReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewAccentReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileAccentAttributeReferenceAsync(bool orderAtTheTop, ProfileAccentReference attributeReference)
        {
            return await AddNewAccentAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewRoleAccentAttributeReferenceAsync(bool orderAtTheTop, RoleAccentReference attributeReference)
        {
            return await AddNewAccentAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteAccentAttributeReferenceAsync(int parentId, AccentReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteAccentReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileAccentAttributeReferenceAsync(ProfileAccentReference attributeReference)
        {


            return await DeleteAccentAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> DeleteRoleAccentAttributeReferenceAsync(RoleAccentReference attributeReference)
        {


            return await DeleteAccentAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }
        public async Task<int> UpdateAccentAttributeReferenceAsync(int parentId,
             AccentReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateAccentReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileAccentAttributeReferenceAsync(ProfileAccentReference attributeReference)
        {
            return await UpdateAccentAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> UpdateRoleAccentAttributeReferenceAsync(RoleAccentReference attributeReference)
        {
            return await UpdateAccentAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderAccentAttributeAsync(int parentId,
           int desiredOrderIndex, AccentReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderAccentAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderProfileAccentAttributeAsync(int desiredOrderIndex, ProfileAccentReference attributeReference)


        {
            return await ReOrderAccentAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }
        public async Task<int> ReOrderRoleAccentAttributeAsync(int desiredOrderIndex, RoleAccentReference attributeReference)


        {
            return await ReOrderAccentAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }

        #endregion
        #region union
        public async Task<UnionReference> GetUnionReferenceByIdAsync(int unionId)
        {
            var union = await _attributeReferenceRepository.GetUnionReferenceByIdAsync(true, unionId);
            return union;
        }
        public async Task<ProfileUnionReference> GetProfileUnionReferenceByIdAsync(int unionId)
        {
            var union = await _attributeReferenceRepository.GetProfileUnionReferenceByIdAsync(true, unionId);
            return union;
        }

        public async Task<CastingCallUnionReference> GetCastingCallUnionReferenceByIdAsync(int unionId)
        {
            var union = await _attributeReferenceRepository.GetCastingCallUnionReferenceByIdAsync(true, unionId);
            return union;
        }
        public async Task<IEnumerable<ProfileUnionReference>> GetProfileUnionReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var unionReferences = await _attributeReferenceRepository.GetUnionReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileUnionReference>)unionReferences;
        }
        public async Task<IEnumerable<CastingCallUnionReference>> GetCastingCallUnionReferencesForParentAsync(int castingCallId, bool withAttributes)
        {
            var unionReferences = await _attributeReferenceRepository.GetUnionReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
            return (IEnumerable<CastingCallUnionReference>)unionReferences;
        }
        public async Task<int> AddNewUnionAttributeReferenceAsync(bool orderAtTheTop,
         UnionReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewUnionReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewUnionAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, UnionReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewUnionReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileUnionAttributeReferenceAsync(bool orderAtTheTop, ProfileUnionReference attributeReference)
        {
            return await AddNewUnionAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewCastingCallUnionAttributeReferenceAsync(bool orderAtTheTop, CastingCallUnionReference attributeReference)
        {
            return await AddNewUnionAttributeReferenceAsync(orderAtTheTop, attributeReference.CastingCallId, attributeReference);
        }
        public async Task<int> AddNewCastingCallRoleUnionAttributeReferenceAsync(bool orderAtTheTop, CastingCallRoleUnionReference attributeReference)
        {
            return await AddNewUnionAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteUnionAttributeReferenceAsync(int parentId, UnionReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteUnionReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileUnionAttributeReferenceAsync(ProfileUnionReference attributeReference)
        {


            return await DeleteUnionAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> DeleteCastingCallUnionAttributeReferenceAsync(CastingCallUnionReference attributeReference)
        {


            return await DeleteUnionAttributeReferenceAsync(attributeReference.CastingCallId, attributeReference);
        }
        public async Task<int> DeleteCastingCallRoleUnionAttributeReferenceAsync(CastingCallRoleUnionReference attributeReference)
        {


            return await DeleteUnionAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }
        public async Task<int> UpdateUnionAttributeReferenceAsync(int parentId,
            UnionReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateUnionReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileUnionAttributeReferenceAsync(ProfileUnionReference attributeReference)
        {
            return await UpdateUnionAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> UpdateCastingCallUnionAttributeReferenceAsync(CastingCallUnionReference attributeReference)
        {
            return await UpdateUnionAttributeReferenceAsync(attributeReference.CastingCallId, attributeReference);
        }

        public async Task<int> UpdateCastingCallRoleUnionAttributeReferenceAsync(CastingCallRoleUnionReference attributeReference)
        {
            return await UpdateUnionAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }
        public async Task<int> ReOrderUnionAttributeAsync(int parentId,
            int desiredOrderIndex, UnionReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderUnionAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderProfileUnionAttributeAsync(int desiredOrderIndex, ProfileUnionReference attributeReference)


        {
            return await ReOrderUnionAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }
        public async Task<int> ReOrderCastingCallUnionAttributeAsync(int desiredOrderIndex, CastingCallUnionReference attributeReference)


        {
            return await ReOrderUnionAttributeAsync(attributeReference.CastingCallId, desiredOrderIndex, attributeReference);
        }
        public async Task<int> ReOrderCastingCallRoleUnionAttributeAsync(int desiredOrderIndex, CastingCallRoleUnionReference attributeReference)


        {
            return await ReOrderUnionAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion
        #region ethnicity
        public async Task<EthnicityReference> GetEthnicityReferenceByIdAsync(int ethnicityId)
        {
            var ethnicity = await _attributeReferenceRepository.GetEthnicityReferenceByIdAsync(true, ethnicityId);
            return ethnicity;
        }
        public async Task<ProfileEthnicityReference> GetProfileEthnicityReferenceByIdAsync(int ethnicityId)
        {
            var ethnicity = await _attributeReferenceRepository.GetProfileEthnicityReferenceByIdAsync(true, ethnicityId);
            return ethnicity;
        }

        public async Task<RoleEthnicityReference> GetRoleEthnicityReferenceByIdAsync(int ethnicityId)
        {
            var ethnicity = await _attributeReferenceRepository.GetRoleEthnicityReferenceByIdAsync(true, ethnicityId);
            return ethnicity;
        }
        public async Task<IEnumerable<ProfileEthnicityReference>> GetProfileEthnicityReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var ethnicityReferences = await _attributeReferenceRepository.GetEthnicityReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileEthnicityReference>)ethnicityReferences;
        }
        public async Task<IEnumerable<RoleEthnicityReference>> GetRoleEthnicityReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var ethnicityReferences = await _attributeReferenceRepository.GetEthnicityReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleEthnicityReference>)ethnicityReferences;
        }
        public async Task<int> AddNewEthnicityAttributeReferenceAsync(bool orderAtTheTop,
        EthnicityReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewEthnicityReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewEthnicityAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, EthnicityReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewEthnicityReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileEthnicityAttributeReferenceAsync(bool orderAtTheTop, ProfileEthnicityReference attributeReference)
        {
            return await AddNewEthnicityAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewRoleEthnicityAttributeReferenceAsync(bool orderAtTheTop, RoleEthnicityReference attributeReference)
        {
            return await AddNewEthnicityAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteEthnicityAttributeReferenceAsync(int parentId, EthnicityReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteEthnicityReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileEthnicityAttributeReferenceAsync(ProfileEthnicityReference attributeReference)
        {


            return await DeleteEthnicityAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> DeleteRoleEthnicityAttributeReferenceAsync(RoleEthnicityReference attributeReference)
        {


            return await DeleteEthnicityAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }
        public async Task<int> UpdateEthnicityAttributeReferenceAsync(int parentId,
         EthnicityReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateEthnicityReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileEthnicityAttributeReferenceAsync(ProfileEthnicityReference attributeReference)
        {
            return await UpdateEthnicityAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> UpdateRoleEthnicityAttributeReferenceAsync(RoleEthnicityReference attributeReference)
        {
            return await UpdateEthnicityAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderEthnicityAttributeAsync(int parentId,
           int desiredOrderIndex, EthnicityReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderEthnicityAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderProfileEthnicityAttributeAsync(int desiredOrderIndex, ProfileEthnicityReference attributeReference)


        {
            return await ReOrderEthnicityAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderRoleEthnicityAttributeAsync(int desiredOrderIndex, RoleEthnicityReference attributeReference)


        {
            return await ReOrderEthnicityAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion
        #region ethnicstereotype
        public async Task<EthnicStereoTypeReference> GetEthnicStereoTypeReferenceByIdAsync(int ethnicStereoTypeId)
        {
            var ethnicStereoType = await _attributeReferenceRepository.GetEthnicStereoTypeReferenceByIdAsync(true, ethnicStereoTypeId);
            return ethnicStereoType;
        }
        public async Task<ProfileEthnicStereoTypeReference> GetProfileEthnicStereoTypeReferenceByIdAsync(int ethnicStereoTypeId)
        {
            var ethnicStereoType = await _attributeReferenceRepository.GetProfileEthnicStereoTypeReferenceByIdAsync(true, ethnicStereoTypeId);
            return ethnicStereoType;
        }

        public async Task<RoleEthnicStereoTypeReference> GetRoleEthnicStereoTypeReferenceByIdAsync(int ethnicStereoTypeId)
        {
            var ethnicStereoType = await _attributeReferenceRepository.GetRoleEthnicStereoTypeReferenceByIdAsync(true, ethnicStereoTypeId);
            return ethnicStereoType;
        }
        public async Task<IEnumerable<ProfileEthnicStereoTypeReference>> GetProfileEthnicStereoTypeReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var ethnicStereoTypeReferences = await _attributeReferenceRepository.GetEthnicStereoTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileEthnicStereoTypeReference>)ethnicStereoTypeReferences;
        }
        public async Task<IEnumerable<RoleEthnicStereoTypeReference>> GetRoleEthnicStereoTypeReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var ethnicStereoTypeReferences = await _attributeReferenceRepository.GetEthnicStereoTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleEthnicStereoTypeReference>)ethnicStereoTypeReferences;
        }
        public async Task<IEnumerable<EthnicStereoTypeReference>> GetCastingCallEthnicStereoTypeReferencesForParentAsync(int castingCallId, bool withAttributes)
        {
            var ethnicStereoTypeReferences = await _attributeReferenceRepository.GetEthnicStereoTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
            return (IEnumerable<EthnicStereoTypeReference>)ethnicStereoTypeReferences;
        }
        public async Task<int> AddNewEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
         EthnicStereoTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewEthnicStereoTypeReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, EthnicStereoTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewEthnicStereoTypeReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop, ProfileEthnicStereoTypeReference attributeReference)
        {
            return await AddNewEthnicStereoTypeAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewRoleEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop, RoleEthnicStereoTypeReference attributeReference)
        {
            return await AddNewEthnicStereoTypeAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteEthnicStereoTypeAttributeReferenceAsync(int parentId, EthnicStereoTypeReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteEthnicStereoTypeReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileEthnicStereoTypeAttributeReferenceAsync(ProfileEthnicStereoTypeReference attributeReference)
        {


            return await DeleteEthnicStereoTypeAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }

        public async Task<int> DeleteRoleEthnicStereoTypeAttributeReferenceAsync(RoleEthnicStereoTypeReference attributeReference)
        {


            return await DeleteEthnicStereoTypeAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> UpdateEthnicStereoTypeAttributeReferenceAsync(int parentId,
            EthnicStereoTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateEthnicStereoTypeReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileEthnicStereoTypeAttributeReferenceAsync(ProfileEthnicStereoTypeReference attributeReference)
        {
            return await UpdateEthnicStereoTypeAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }

        public async Task<int> UpdateRoleEthnicStereoTypeAttributeReferenceAsync(RoleEthnicStereoTypeReference attributeReference)
        {
            return await UpdateEthnicStereoTypeAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderEthnicStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicStereoTypeReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderEthnicStereoTypeAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderProfileEthnicStereoTypeAttributeAsync(int desiredOrderIndex, ProfileEthnicStereoTypeReference attributeReference)


        {
            return await ReOrderEthnicStereoTypeAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderRoleEthnicStereoTypeAttributeAsync(int desiredOrderIndex, RoleEthnicStereoTypeReference attributeReference)


        {
            return await ReOrderEthnicStereoTypeAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion
        #region stereotype
        public async Task<StereoTypeReference> GetStereoTypeReferenceByIdAsync(int stereoTypeId)
        {
            var stereoType = await _attributeReferenceRepository.GetStereoTypeReferenceByIdAsync(true, stereoTypeId);
            return stereoType;
        }
        public async Task<ProfileStereoTypeReference> GetProfileStereoTypeReferenceByIdAsync(int stereoTypeId)
        {
            var stereoType = await _attributeReferenceRepository.GetProfileStereoTypeReferenceByIdAsync(true, stereoTypeId);
            return stereoType;
        }

        public async Task<RoleStereoTypeReference> GetRoleStereoTypeReferenceByIdAsync(int stereoTypeId)
        {
            var stereoType = await _attributeReferenceRepository.GetRoleStereoTypeReferenceByIdAsync(true, stereoTypeId);
            return stereoType;
        }
        public async Task<IEnumerable<ProfileStereoTypeReference>> GetProfileStereoTypeReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var stereoTypeReferences = await _attributeReferenceRepository.GetStereoTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileStereoTypeReference>)stereoTypeReferences;
        }
        public async Task<IEnumerable<RoleStereoTypeReference>> GetRoleStereoTypeReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var stereoTypeReferences = await _attributeReferenceRepository.GetStereoTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleStereoTypeReference>)stereoTypeReferences;
        }
        public async Task<int> AddNewStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
      StereoTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewStereoTypeReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, StereoTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewStereoTypeReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileStereoTypeAttributeReferenceAsync(bool orderAtTheTop, ProfileStereoTypeReference attributeReference)
        {
            return await AddNewStereoTypeAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewRoleStereoTypeAttributeReferenceAsync(bool orderAtTheTop, RoleStereoTypeReference attributeReference)
        {
            return await AddNewStereoTypeAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteStereoTypeAttributeReferenceAsync(int parentId, StereoTypeReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteStereoTypeReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileStereoTypeAttributeReferenceAsync(ProfileStereoTypeReference attributeReference)
        {


            return await DeleteStereoTypeAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> DeleteRoleStereoTypeAttributeReferenceAsync(RoleStereoTypeReference attributeReference)
        {


            return await DeleteStereoTypeAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }
        public async Task<int> UpdateStereoTypeAttributeReferenceAsync(int parentId,
       StereoTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateStereoTypeReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileStereoTypeAttributeReferenceAsync(ProfileStereoTypeReference attributeReference)
        {
            return await UpdateStereoTypeAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> UpdateRoleStereoTypeAttributeReferenceAsync(RoleStereoTypeReference attributeReference)
        {
            return await UpdateStereoTypeAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderStereoTypeAttributeAsync(int parentId,
         int desiredOrderIndex, StereoTypeReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderStereoTypeAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderProfileStereoTypeAttributeAsync(int desiredOrderIndex, ProfileStereoTypeReference attributeReference)


        {
            return await ReOrderStereoTypeAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }
        public async Task<int> ReOrderRoleStereoTypeAttributeAsync(int desiredOrderIndex, RoleStereoTypeReference attributeReference)


        {
            return await ReOrderStereoTypeAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion
        #region bodytype
        public async Task<BodyTypeReference> GetBodyTypeReferenceByIdAsync(int bodyTypeId)
        {
            var bodyType = await _attributeReferenceRepository.GetBodyTypeReferenceByIdAsync(true, bodyTypeId);
            return bodyType;
        }
        /* public async Task<ProfileBodyTypeReference> GetProfileBodyTypeReferenceByIdAsync(int bodyTypeId)
         {
             var bodyType = await _attributeReferenceRepository.GetProfileBodyTypeReferenceByIdAsync(true, bodyTypeId);
             return bodyType;
         }*/

        public async Task<RoleBodyTypeReference> GetRoleBodyTypeReferenceByIdAsync(int bodyTypeId)
        {
            var bodyType = await _attributeReferenceRepository.GetRoleBodyTypeReferenceByIdAsync(true, bodyTypeId);
            return bodyType;
        }
        /*  public async Task<IEnumerable<ProfileBodyTypeReference>> GetProfileBodyTypeReferencesForParentAsync(int profileId, bool withAttributes)
          {
              var bodyTypeReferences = await _attributeReferenceRepository.GetBodyTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
              return (IEnumerable<ProfileBodyTypeReference>)bodyTypeReferences;
          }*/
        public async Task<IEnumerable<RoleBodyTypeReference>> GetRoleBodyTypeReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var bodyTypeReferences = await _attributeReferenceRepository.GetBodyTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleBodyTypeReference>)bodyTypeReferences;
        }
        public async Task<IEnumerable<BodyTypeReference>> GetCastingCallBodyTypeReferencesForParentAsync(int castingCallId, bool withAttributes)
        {
            var bodyTypeReferences = await _attributeReferenceRepository.GetBodyTypeReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
            return (IEnumerable<BodyTypeReference>)bodyTypeReferences;
        }
        public async Task<int> AddNewBodyTypeAttributeReferenceAsync(bool orderAtTheTop,
         BodyTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewBodyTypeReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewBodyTypeAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, BodyTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewBodyTypeReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        /*  public async Task<int> AddNewProfileBodyTypeAttributeReferenceAsync(bool orderAtTheTop, ProfileBodyTypeReference attributeReference)
          {
              return await AddNewBodyTypeAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
          }*/
        public async Task<int> AddNewRoleBodyTypeAttributeReferenceAsync(bool orderAtTheTop, RoleBodyTypeReference attributeReference)
        {
            return await AddNewBodyTypeAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteBodyTypeAttributeReferenceAsync(int parentId, BodyTypeReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteBodyTypeReferenceAsync(parentId, attributeReference);
        }
        /* public async Task<int> DeleteProfileBodyTypeAttributeReferenceAsync(ProfileBodyTypeReference attributeReference)
         {


             return await DeleteBodyTypeAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
         }*/
        public async Task<int> DeleteRoleBodyTypeAttributeReferenceAsync(RoleBodyTypeReference attributeReference)
        {


            return await DeleteBodyTypeAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> UpdateBodyTypeAttributeReferenceAsync(int parentId,
            BodyTypeReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateBodyTypeReferenceAsync(parentId, attributeReference);
        }
        /* public async Task<int> UpdateProfileBodyTypeAttributeReferenceAsync(ProfileBodyTypeReference attributeReference)
         {
             return await UpdateBodyTypeAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
         }*/
        public async Task<int> UpdateRoleBodyTypeAttributeReferenceAsync(RoleBodyTypeReference attributeReference)
        {
            return await UpdateBodyTypeAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderBodyTypeAttributeAsync(int parentId,
            int desiredOrderIndex, BodyTypeReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderBodyTypeAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }
        /*   public async Task<int> ReOrderProfileBodyTypeAttributeAsync(int desiredOrderIndex, ProfileBodyTypeReference attributeReference)


           {
               return await ReOrderBodyTypeAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
           }*/

        public async Task<int> ReOrderRoleBodyTypeAttributeAsync(int desiredOrderIndex, RoleBodyTypeReference attributeReference)


        {
            return await ReOrderBodyTypeAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion

        #region haircolor
        public async Task<HairColorReference> GetHairColorReferenceByIdAsync(int hairColorId)
        {
            var hairColor = await _attributeReferenceRepository.GetHairColorReferenceByIdAsync(true, hairColorId);
            return hairColor;
        }
        /* public async Task<ProfileHairColorReference> GetProfileHairColorReferenceByIdAsync(int hairColorId)
         {
             var hairColor = await _attributeReferenceRepository.GetProfileHairColorReferenceByIdAsync(true, hairColorId);
             return hairColor;
         }*/

        public async Task<RoleHairColorReference> GetRoleHairColorReferenceByIdAsync(int hairColorId)
        {
            var hairColor = await _attributeReferenceRepository.GetRoleHairColorReferenceByIdAsync(true, hairColorId);
            return hairColor;
        }
        /*    public async Task<IEnumerable<ProfileHairColorReference>> GetProfileHairColorReferencesForParentAsync(int profileId, bool withAttributes)
            {
                var hairColorReferences = await _attributeReferenceRepository.GetHairColorReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
                return (IEnumerable<ProfileHairColorReference>)hairColorReferences;
            }*/
        public async Task<IEnumerable<RoleHairColorReference>> GetRoleHairColorReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var hairColorReferences = await _attributeReferenceRepository.GetHairColorReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleHairColorReference>)hairColorReferences;
        }
        /*  public async Task<IEnumerable<HairColorReference>> GetCastingCallHairColorReferencesForParentAsync(int castingCallId, bool withAttributes)
          {
              var hairColorReferences = await _attributeReferenceRepository.GetHairColorReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
              return (IEnumerable<HairColorReference>)hairColorReferences;
          }*/
        public async Task<int> AddNewHairColorAttributeReferenceAsync(bool orderAtTheTop,
         HairColorReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewHairColorReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewHairColorAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, HairColorReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewHairColorReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        /*  public async Task<int> AddNewProfileHairColorAttributeReferenceAsync(bool orderAtTheTop, ProfileHairColorReference attributeReference)
          {
              return await AddNewHairColorAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
          }*/
        public async Task<int> AddNewRoleHairColorAttributeReferenceAsync(bool orderAtTheTop, RoleHairColorReference attributeReference)
        {
            return await AddNewHairColorAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteHairColorAttributeReferenceAsync(int parentId, HairColorReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteHairColorReferenceAsync(parentId, attributeReference);
        }
        /*   public async Task<int> DeleteProfileHairColorAttributeReferenceAsync(ProfileHairColorReference attributeReference)
           {


               return await DeleteHairColorAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
           }*/
        public async Task<int> DeleteRoleHairColorAttributeReferenceAsync(RoleHairColorReference attributeReference)
        {


            return await DeleteHairColorAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> UpdateHairColorAttributeReferenceAsync(int parentId,
            HairColorReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateHairColorReferenceAsync(parentId, attributeReference);
        }
        /*   public async Task<int> UpdateProfileHairColorAttributeReferenceAsync(ProfileHairColorReference attributeReference)
           {
               return await UpdateHairColorAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
           }*/
        public async Task<int> UpdateRoleHairColorAttributeReferenceAsync(RoleHairColorReference attributeReference)
        {
            return await UpdateHairColorAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderHairColorAttributeAsync(int parentId,
            int desiredOrderIndex, HairColorReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderHairColorAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }
        /* public async Task<int> ReOrderProfileHairColorAttributeAsync(int desiredOrderIndex, ProfileHairColorReference attributeReference)


         {
             return await ReOrderHairColorAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
         }*/

        public async Task<int> ReOrderRoleHairColorAttributeAsync(int desiredOrderIndex, RoleHairColorReference attributeReference)


        {
            return await ReOrderHairColorAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }


        #endregion

        #region eyecolor
        public async Task<EyeColorReference> GetEyeColorReferenceByIdAsync(int eyeColorId)
        {
            var eyeColor = await _attributeReferenceRepository.GetEyeColorReferenceByIdAsync(true, eyeColorId);
            return eyeColor;
        }
        /*  public async Task<ProfileEyeColorReference> GetProfileEyeColorReferenceByIdAsync(int eyeColorId)
          {
              var eyeColor = await _attributeReferenceRepository.GetProfileEyeColorReferenceByIdAsync(true, eyeColorId);
              return eyeColor;
          }*/

        public async Task<RoleEyeColorReference> GetRoleEyeColorReferenceByIdAsync(int eyeColorId)
        {
            var eyeColor = await _attributeReferenceRepository.GetRoleEyeColorReferenceByIdAsync(true, eyeColorId);
            return eyeColor;
        }
        /* public async Task<IEnumerable<ProfileEyeColorReference>> GetProfileEyeColorReferencesForParentAsync(int profileId, bool withAttributes)
         {
             var eyeColorReferences = await _attributeReferenceRepository.GetEyeColorReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
             return (IEnumerable<ProfileEyeColorReference>)eyeColorReferences;
         }*/
        public async Task<IEnumerable<RoleEyeColorReference>> GetRoleEyeColorReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var eyeColorReferences = await _attributeReferenceRepository.GetEyeColorReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleEyeColorReference>)eyeColorReferences;
        }
        public async Task<IEnumerable<EyeColorReference>> GetCastingCallEyeColorReferencesForParentAsync(int castingCallId, bool withAttributes)
        {
            var eyeColorReferences = await _attributeReferenceRepository.GetEyeColorReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
            return (IEnumerable<EyeColorReference>)eyeColorReferences;
        }
        public async Task<int> AddNewEyeColorAttributeReferenceAsync(bool orderAtTheTop,
         EyeColorReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewEyeColorReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewEyeColorAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, EyeColorReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewEyeColorReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        /* public async Task<int> AddNewProfileEyeColorAttributeReferenceAsync(bool orderAtTheTop, ProfileEyeColorReference attributeReference)
         {
             return await AddNewEyeColorAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
         }*/
        public async Task<int> AddNewRoleEyeColorAttributeReferenceAsync(bool orderAtTheTop, RoleEyeColorReference attributeReference)
        {
            return await AddNewEyeColorAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteEyeColorAttributeReferenceAsync(int parentId, EyeColorReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteEyeColorReferenceAsync(parentId, attributeReference);
        }
        /* public async Task<int> DeleteProfileEyeColorAttributeReferenceAsync(ProfileEyeColorReference attributeReference)
         {


             return await DeleteEyeColorAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
         }*/
        public async Task<int> DeleteRoleEyeColorAttributeReferenceAsync(RoleEyeColorReference attributeReference)
        {


            return await DeleteEyeColorAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> UpdateEyeColorAttributeReferenceAsync(int parentId,
            EyeColorReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateEyeColorReferenceAsync(parentId, attributeReference);
        }
        /*  public async Task<int> UpdateProfileEyeColorAttributeReferenceAsync(ProfileEyeColorReference attributeReference)
          {
              return await UpdateEyeColorAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
          }*/
        public async Task<int> UpdateRoleEyeColorAttributeReferenceAsync(RoleEyeColorReference attributeReference)
        {
            return await UpdateEyeColorAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderEyeColorAttributeAsync(int parentId,
            int desiredOrderIndex, EyeColorReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderEyeColorAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }
        /*  public async Task<int> ReOrderProfileEyeColorAttributeAsync(int desiredOrderIndex, ProfileEyeColorReference attributeReference)


          {
              return await ReOrderEyeColorAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
          }*/

        public async Task<int> ReOrderRoleEyeColorAttributeAsync(int desiredOrderIndex, RoleEyeColorReference attributeReference)


        {
            return await ReOrderEyeColorAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }

        #endregion

        #region skill
        public async Task<SkillReference> GetSkillReferenceByIdAsync(int skillId)
        {
            var skill = await _attributeReferenceRepository.GetSkillReferenceByIdAsync(true, skillId);
            return skill;
        }
        public async Task<ProfileSkillReference> GetProfileSkillReferenceByIdAsync(int skillId)
        {
            var skill = await _attributeReferenceRepository.GetProfileSkillReferenceByIdAsync(true, skillId);
            return skill;
        }

        public async Task<RoleSkillReference> GetRoleSkillReferenceByIdAsync(int skillId)
        {
            var skill = await _attributeReferenceRepository.GetRoleSkillReferenceByIdAsync(true, skillId);
            return skill;
        }
        public async Task<IEnumerable<ProfileSkillReference>> GetProfileSkillReferencesForParentAsync(int profileId, bool withAttributes)
        {
            var skillReferences = await _attributeReferenceRepository.GetSkillReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Profile, profileId);
            return (IEnumerable<ProfileSkillReference>)skillReferences;
        }
        public async Task<IEnumerable<RoleSkillReference>> GetRoleSkillReferencesForParentAsync(int roleId, bool withAttributes)
        {
            var skillReferences = await _attributeReferenceRepository.GetSkillReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.Role, roleId);
            return (IEnumerable<RoleSkillReference>)skillReferences;
        }
        public async Task<IEnumerable<SkillReference>> GetCastingCallSkillReferencesForParentAsync(int castingCallId, bool withAttributes)
        {
            var skillReferences = await _attributeReferenceRepository.GetSkillReferencesForParentAsync(withAttributes, AttributeReferenceTypeEnum.CastingCall, castingCallId);
            return (IEnumerable<SkillReference>)skillReferences;
        }
        public async Task<int> AddNewSkillAttributeReferenceAsync(bool orderAtTheTop,
         SkillReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewSkillReferenceAsync(orderAtTheTop, attributeReference);
        }

        public async Task<int> AddNewSkillAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, SkillReference attributeReference)
        {
            return await _attributeReferenceRepository.AddNewSkillReferenceAsync(orderAtTheTop, parentId, attributeReference);
        }

        public async Task<int> AddNewProfileSkillAttributeReferenceAsync(bool orderAtTheTop, ProfileSkillReference attributeReference)
        {
            return await AddNewSkillAttributeReferenceAsync(orderAtTheTop, attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> AddNewRoleSkillAttributeReferenceAsync(bool orderAtTheTop, RoleSkillReference attributeReference)
        {
            return await AddNewSkillAttributeReferenceAsync(orderAtTheTop, attributeReference.RoleId, attributeReference);
        }

        public async Task<int> DeleteSkillAttributeReferenceAsync(int parentId, SkillReference attributeReference)
        {


            return await _attributeReferenceRepository.DeleteSkillReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> DeleteProfileSkillAttributeReferenceAsync(ProfileSkillReference attributeReference)
        {


            return await DeleteSkillAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> DeleteRoleSkillAttributeReferenceAsync(RoleSkillReference attributeReference)
        {


            return await DeleteSkillAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> UpdateSkillAttributeReferenceAsync(int parentId,
            SkillReference attributeReference)
        {
            return await _attributeReferenceRepository.UpdateSkillReferenceAsync(parentId, attributeReference);
        }
        public async Task<int> UpdateProfileSkillAttributeReferenceAsync(ProfileSkillReference attributeReference)
        {
            return await UpdateSkillAttributeReferenceAsync(attributeReference.ProfileId, attributeReference);
        }
        public async Task<int> UpdateRoleSkillAttributeReferenceAsync(RoleSkillReference attributeReference)
        {
            return await UpdateSkillAttributeReferenceAsync(attributeReference.RoleId, attributeReference);
        }

        public async Task<int> ReOrderSkillAttributeAsync(int parentId,
            int desiredOrderIndex, SkillReference attributeReference)


        {
            return await _attributeReferenceRepository.ReOrderSkillAttributeAsync(parentId, desiredOrderIndex, attributeReference);
        }
        public async Task<int> ReOrderProfileSkillAttributeAsync(int desiredOrderIndex, ProfileSkillReference attributeReference)


        {
            return await ReOrderSkillAttributeAsync(attributeReference.ProfileId, desiredOrderIndex, attributeReference);
        }

        public async Task<int> ReOrderRoleSkillAttributeAsync(int desiredOrderIndex, RoleSkillReference attributeReference)


        {
            return await ReOrderSkillAttributeAsync(attributeReference.RoleId, desiredOrderIndex, attributeReference);
        }

        #endregion

        #endregion


    }
}
