﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Enums;
using Casting.BLL.TalentAssets.Interfaces;


namespace Casting.BLL.TalentAssets
{
    public class TalentResumeReferenceService : BaseService, ITalentResumeReferenceService
    {
        private ITalentAssetAsyncRepository _assetRepository;

        public TalentResumeReferenceService(ITalentAssetAsyncRepository assetRepository)
        {
            _assetRepository = assetRepository;
        }

        public async Task<ResumeReference> GetResumeReferenceByIdAsync(int resumeId)
        {
            var resume = await _assetRepository.GetResumeAssetReferenceByIdAsync(true, resumeId);
            return resume;
        }
        public async Task<IEnumerable<ResumeReference>> GetProfileResumeReferencesForParentAsync(int profileId, bool withAssets)
        {
            var resumeReferences = await _assetRepository.GetResumeReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Profile, profileId);
            return resumeReferences;
        }
        public async Task<IEnumerable<ResumeReference>> GetPersonaResumeReferencesForParentAsync(int personaId, bool withAssets)
        {
            var resumeReferences = await _assetRepository.GetResumeReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Persona, personaId);
            return resumeReferences;
        }
        public async Task<IEnumerable<ResumeReference>> GetBasketResumeReferencesForParentAsync(int basketId, bool withAssets)
        {
            var resumeReferences = await _assetRepository.GetResumeReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Basket, basketId);
            return resumeReferences;
        }
           public async Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            ResumeReference assetReference)
        {
            return await _assetRepository.AddNewResumeAssetReferenceAsync(orderAtTheTop,assetReference);
        }

        public async Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            int parentId, ResumeReference assetReference)
        {
            return await _assetRepository.AddNewResumeAssetReferenceAsync(orderAtTheTop, parentId, assetReference);
        }

        public async Task<int> AddNewProfileResumeAssetReferenceAsync(bool orderAtTheTop, ProfileResumeReference assetReference)
        {
            return await AddNewResumeAssetReferenceAsync(orderAtTheTop, assetReference.ProfileId, assetReference);
        }
        public async Task<int> AddNewPersonaResumeAssetReferenceAsync(bool orderAtTheTop, PersonaResumeReference assetReference)
        {
            return await AddNewResumeAssetReferenceAsync(orderAtTheTop, assetReference.PersonaId, assetReference);
        }
        public async Task<int> AddNewBasketResumeAssetReferenceAsync(bool orderAtTheTop, BasketResumeReference assetReference)
        {
            return await AddNewResumeAssetReferenceAsync(orderAtTheTop, assetReference.BasketId, assetReference);
        }

        public async Task<int> DeleteResumeAssetReferenceAsync(int parentId, ResumeReference assetReference)
        {


            return await _assetRepository.DeleteResumeAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> DeleteProfileResumeAssetReferenceAsync(ProfileResumeReference assetReference)
        {


            return await DeleteResumeAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> DeleteBasketResumeAssetReferenceAsync(BasketResumeReference assetReference)
        {


            return await DeleteResumeAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> DeletePersonaResumeAssetReferenceAsync(PersonaResumeReference assetReference)
        {


            return await DeleteResumeAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }
        public async Task<int> UpdateResumeAssetReferenceAsync(int parentId,
            ResumeReference assetReference)
        {
            return await _assetRepository.UpdateResumeAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> UpdateProfileResumeAssetReferenceAsync(ProfileResumeReference assetReference)
        {
            return await UpdateResumeAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> UpdateBasketResumeAssetReferenceAsync(BasketResumeReference assetReference)
        {
            return await UpdateResumeAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> UpdatePersonaResumeAssetReferenceAsync(PersonaResumeReference assetReference)
        {
            return await UpdateResumeAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }

        public async Task<int> ReOrderResumeAssetAsync(int parentId,
            int desiredOrderIndex, ResumeReference assetReference)


        {
            return await _assetRepository.ReOrderResumeAssetAsync(parentId, desiredOrderIndex, assetReference);
        }

        public async Task<int> ReOrderProfileResumeAssetAsync(int desiredOrderIndex, ProfileResumeReference assetReference)


        {
            return await ReOrderResumeAssetAsync(assetReference.ProfileId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderBasketResumeAssetAsync(int desiredOrderIndex, BasketResumeReference assetReference)


        {
            return await ReOrderResumeAssetAsync(assetReference.BasketId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderPersonaResumeAssetAsync(int desiredOrderIndex, PersonaResumeReference assetReference)


        {
            return await ReOrderResumeAssetAsync(assetReference.PersonaId, desiredOrderIndex, assetReference);
        }
    }
}
