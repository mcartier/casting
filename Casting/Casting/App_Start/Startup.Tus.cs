﻿using System;
using System.Collections.Generic;
using System.Linq;
using AspNetWebApi.JWT;
using AspNetWebApi.JWT.Options;
using AspNetWebApi.JWT.Providers;
using Casting.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Casting.Web.Providers;
using Casting.Web.Models;
using StructureMap;
using Casting.Web.Tus.Extensions;
using tusdotnet;
using Casting.Web.Tus;

namespace Casting.Web
{
    public partial class Startup
    {
        
        public void ConfigureTus(IAppBuilder app)
        {
            var tusTalentImageConfigurationConfiguration = TalentImageTusConfiguration.CreateTusConfiguration(true);
            var tusCastingSideTusConfiguration = SidesTusConfiguration.CreateTusConfiguration(true);
            var tusTalentVideoConfigurationConfiguration = TalentVideoTusConfiguration.CreateTusConfiguration(true);
            var tusCastingTalentVideoTusConfiguration = CastingTalentVideoTusConfiguration.CreateTusConfiguration(true);
            var tusCastingBasketVideoTusConfiguration = CastingBasketVideoTusConfiguration.CreateTusConfiguration(true);
            // app.SetupSimpleBasicAuth();

            // owinRequest parameter can be used to create a tus configuration based on current user, domain, host, port or whatever.
            // In this case we just return the same configuration for everyone.
            app.UseTus(owinRequest => tusTalentImageConfigurationConfiguration);
            app.UseTus(owinRequest => tusCastingSideTusConfiguration);
            app.UseTus(owinRequest => tusTalentVideoConfigurationConfiguration);
            app.UseTus(owinRequest => tusCastingTalentVideoTusConfiguration);
            app.UseTus(owinRequest => tusCastingBasketVideoTusConfiguration);
            // Setup cleanup job to remove incomplete expired files.
            app.StartCleanupJob(tusCastingSideTusConfiguration);
            app.StartCleanupJob(tusTalentImageConfigurationConfiguration);
            app.StartCleanupJob(tusTalentVideoConfigurationConfiguration);
        }
    }
}
