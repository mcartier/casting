﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Infrastructure;
using System.Diagnostics.Eventing.Reader;
using System.Net.Sockets;
using Casting.DAL;
using Casting.DAL.Repositories;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.SearchCriterias;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{ 

    public class ProfileAsyncRepository : AsyncRepository, IProfileAsyncRepository
    {
        ICastingContext _castingContext;
        public ProfileAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        public async Task<Profile> GetProfileByIdAsync(int profileId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType)
        {
            IQueryable<Profile> profiles = Enumerable.Empty<Profile>().AsQueryable();
            switch (profileType)
            {
                case ProfileTypeEnum.Actor:
                    profiles = GetActorProfilesQueryable(withTalent, withAssets, withAttributes);

                    break;
                case ProfileTypeEnum.VoiceActor:
                    profiles = GetVoiceActorProfilesQueryable(withTalent, withAssets, withAttributes);


                    break;

            }

            return await profiles.FirstOrDefaultAsync(p => p.Id == profileId);


        }

        public async Task<Profile> GetProfileByIdAsync(int profileId, bool withTalent)
        {
         
            return await GetProfilesQueryable(withTalent)
                .FirstOrDefaultAsync(p => p.Id == profileId); 
        }

        public async Task<Profile> GetProfileByTalentIdAsync(int talentId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType)
        {
            IQueryable<Profile> profiles = Enumerable.Empty<Profile>().AsQueryable();
            switch (profileType)
            {
                case ProfileTypeEnum.Actor:
                    profiles = GetActorProfilesQueryable(withTalent, withAssets, withAttributes);

                    break;
                case ProfileTypeEnum.VoiceActor:
                    profiles = GetVoiceActorProfilesQueryable(withTalent, withAssets, withAttributes);


                    break;

            }

            return await profiles.FirstOrDefaultAsync(p => p.Id == talentId);


        }

        public async Task<Profile> GetProfileByTalentIdAsync(int talentId, bool withTalent)
        {
            return await GetProfilesQueryable(withTalent)
                .FirstOrDefaultAsync(p => p.Id == talentId);

        }

        public async Task<IEnumerable<Profile>> GetProfilesByAgentIdAsync(int agentId, bool withTalent, bool withAssets,
            bool withAttributes, ProfileTypeEnum profileType)
        {
            IQueryable<Profile> profiles = Enumerable.Empty<Profile>().AsQueryable();
            switch (profileType)
            {
                case ProfileTypeEnum.Actor:
                    profiles = GetActorProfilesQueryable(withTalent, withAssets, withAttributes);

                    break;
                case ProfileTypeEnum.VoiceActor:
                    profiles = GetVoiceActorProfilesQueryable(withTalent, withAssets, withAttributes);


                    break;

            }

            return await Task.FromResult(new List<Profile>());
            //return await profiles.Where(p => p.TalentReference.Talent.AgentReferences.ToList().Any(a => a.AgentId == agentId)).ToListAsync();
        }

        public async Task<IEnumerable<Profile>> GetProfilesByMainAgentIdAsync(int agentId, bool withTalent, bool withAssets,
            bool withAttributes, ProfileTypeEnum profileType)
        {
            IQueryable<Profile> profiles = Enumerable.Empty<Profile>().AsQueryable();
            switch (profileType)
            {
                case ProfileTypeEnum.Actor:
                    profiles = GetActorProfilesQueryable(withTalent, withAssets, withAttributes);

                    break;
                case ProfileTypeEnum.VoiceActor:
                    profiles = GetVoiceActorProfilesQueryable(withTalent, withAssets, withAttributes);


                    break;

            }

            return await profiles.Where(p => p.Id == agentId).ToListAsync();
        }
        public async Task<IEnumerable<Profile>> GetAllProfilesByAgentIdAsync(int agentId, bool withTalent)
        {
            var profiles = GetProfilesQueryable(withTalent)
                .Where(t => t.Talent.AgentReferences.ToList().Any(a => a.AgentId == agentId));
          
            return await profiles.ToListAsync();
        }
        public async Task<IEnumerable<Profile>> GetAllProfilesByMainAgentIdAsync(int agentId, bool withTalent)
        {
            var profiles = GetProfilesQueryable(withTalent)
                .Where(t => t.Talent.MainAgentId == agentId);

            return await profiles.ToListAsync();
        }

        public async Task<IEnumerable<Profile>> GetProfilesByDirectorIdAsync(int directorId, bool withTalent, bool withAssets,
            bool withAttributes, ProfileTypeEnum profileType)
        {
            IQueryable<Profile> profiles = Enumerable.Empty<Profile>().AsQueryable();
            switch (profileType)
            {
                case ProfileTypeEnum.Actor:
                    profiles = GetActorProfilesQueryable(withTalent, withAssets, withAttributes);

                    break;
                case ProfileTypeEnum.VoiceActor:
                    profiles = GetVoiceActorProfilesQueryable(withTalent, withAssets, withAttributes);


                    break;

            }

            return await Task.FromResult(new List<Profile>());
            //return await profiles.Where(p => p.TalentReference.Talent.AgentReferences.ToList().Any(a => a.AgentId == agentId)).ToListAsync();
        }

        public async Task<IEnumerable<Profile>> GetAllProfilesByDirectorIdAsync(int directorId, bool withTalent)
        {
            var profiles = GetProfilesQueryable(withTalent);
                //.Where(t => ((StudioTalent) t.Talent).DirectorId == directorId);
           
            return await profiles.ToListAsync();
        }


        public IQueryable<Profile> SearchActorProfilesQueryable(ActorProfileSearchCriteria searchCriteria)
        {
            var profiles = from profile in GetProfilesQueryable(false)
                           select profile;
            //if (searchCriteria.Unions.Count > 0)
            //{
                //profiles = profiles.Where(u => u.Unions.Any(union => searchCriteria.Unions.Contains(union)));
            //}
           // if (searchCriteria.Languages.Count > 0)
          //  {
              // profiles = profiles.Where(u => u.Languages.Any(lang => searchCriteria.Languages.Contains(lang)));
          //  }
            return profiles;
        }

        public async Task<int> AddNewProfileAsync(Profile profile)
        {
            _castingContext.Profiles.Add(profile);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteProfileAsync(Profile profile)
        {
            _castingContext.Profiles.Remove(profile);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateProfileAsync(Profile profile)
        {
            SetEntityState(profile, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #region PrivateMethods
        private IQueryable<Profile> GetVoiceActorProfilesQueryable(bool withTalent, bool withAssets,
            bool withAttributes)
        {

            var profiles = GetProfilesQueryable(withTalent).OfType<VoiceActorProfile>();
            if (withAssets)
            {
                profiles = profiles
                    .Include("ResumeReferences.Resume")
                    .Include("AudioReferences.Audio");
                //   .Include(r => r.ResumeReferences)
                //  .Include(a => a.AudioReferences)
            }

            if (withAttributes)
            {
                profiles = profiles
                    .Include(a => a.Accents)
                        .Include(u => u.Unions);
            }
            return from profile in profiles select profile;

        }
        private IQueryable<Profile> GetActorProfilesQueryable(bool withTalent, bool withAssets,
            bool withAttributes)
        {
            var profiles = GetProfilesQueryable(withTalent).OfType<ActorProfile>();
                   
            //    select profile;
            if (withAssets)
            {
                profiles = profiles
                    .Include("ImageReferences.Image")
                    .Include("ResumeReferences.Resume")
                    .Include("AudioReferences.Audio")
                    .Include("VideoReferences.Video");
                // .Include(i => i.ImageReferences)
                //   .Include(r => r.ResumeReferences)
                //  .Include(a => a.AudioReferences)
                //  .Include(v => v.VideoReferences);
            }

            if (withAttributes)
            {
                profiles = profiles
                    .Include(e => e.Ethnicities)
                    .Include(s => s.StereoTypes)
                    .Include(es => es.EthnicStereoTypes)
                    .Include(l => l.Languages)
                    .Include(a => a.Accents)
                    .Include(u => u.Unions)
                    .Include(h => h.HairColor)
                    .Include(e => e.EyeColor)
                    .Include(b => b.BodyType);

            }

            return from profile in profiles select profile;
        }

        private IQueryable<Profile> GetProfilesQueryable(bool withTalent)
        {
            var profiles = _castingContext.Profiles
                .Include(c => c.Location);
            if (withTalent)
            {
                profiles = profiles
                    .Include(t => t.Talent);
            }
            return from profile in  profiles select profile;
        }
        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}
