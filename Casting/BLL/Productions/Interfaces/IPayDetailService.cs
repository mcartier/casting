﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{

    public interface IPayDetailService
    {
    
        Task<PayDetail> GetPayDetailByIdAsync(int payDetailId);
        Task<IEnumerable<PayDetail>> GetPayDetailsByProductionIdAsync(int productionId);
        Task<int> AddNewPayDetailAsync(PayDetail payDetail);
        Task<int> UpdatePayDetailAsync(PayDetail payDetail);
        Task<int> DeletePayDetailAsync(PayDetail payDetail);
    }
}