﻿using Casting.Model.Entities.Productions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
namespace Casting.Model.Entities.AttributeReferences
{
    public class RoleBodyTypeReference: BodyTypeReference
    {
        public RoleBodyTypeReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.Role;
        }
        public int RoleId { get; set; }
       
    }
}
