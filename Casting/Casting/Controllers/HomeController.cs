﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Casting.BLL.Profiles.Interfaces;
using Casting.DAL;
using Casting.DAL.Repositories;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;


using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Users;
using Casting.Model.Enums;
using Casting.Web.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Casting.Web.Models;
using Casting.BLL.Metadata.Interfaces;

namespace Casting.Web.Controllers
{
    public class HomeController : Controller
    {
        private ICastingContext _castingContext;
        private IProfileService _rep;
        private readonly UserManager<ApplicationUser> _userManager;
        private IMetadataService _metadataService;

        public HomeController(UserManager<ApplicationUser> userManager, ICastingContext castingContext, IProfileService rep, IMetadataService metadataService)
        {
            _castingContext = castingContext;// castingContext;
            _userManager = userManager;
            _rep = rep;
            _metadataService = metadataService;// castingContext;



        }

       
        public  ActionResult Index()
        {
          
            /*var profile =  AsyncHelper.RunSync<Profile>(() =>
                _rep.GetCompleteActorProfileByTalentIdAsync(1)
            );*/
            ViewBag.Title = "Home Page";
            //  var persona = _castingContext.Personas.Where(p => p.Id == 1).FirstOrDefault();
            //var housetalentpersona = _castingContext.HouseTalentPersonas.Where(ti => ti.TalentId == 1).Include(ti=>ti.Talent).FirstOrDefault();
            //var houseTalent = _castingContext.HouseTalents.Where(ti => ti.Id == 1).Include(p => p.Personas).Include(a=>a.Agents)
            //    .FirstOrDefault();
            /* var talent = _castingContext.Talents.Where(ti => ti.Id == 7).Include(p => p.Profile).FirstOrDefault();
             if (talent != null && talent.Profile == null)
             {
                 var profile = new ActorProfile()
                 {

                     Talent = talent,
                 };
                 _castingContext.Profiles.Add(profile);
                await _castingContext.SaveChangesAsync();

             }

             if (talent != null && talent.Profile != null)
             {
                var profile = talent.Profile;
              _castingContext.Profiles.Remove(profile);
              _castingContext.VoiceActorProfiles.Add(new VoiceActorProfile() { Talent = talent });
              _castingContext.SaveChanges();
              talent.Profile.Personas.Add(new Persona() { WorkingName = "lkjkj", DOB = DateTime.Now });
              _castingContext.SaveChanges();
              int y = 0;
              Video video = new Video() { OwnerId = 1 };
              _castingContext.Videos.Add(video);
              Video video2 = new Video() { OwnerId = 2 };
              _castingContext.Videos.Add(video2);
              Video video3 = new Video() { OwnerId = 3 };
              _castingContext.Videos.Add(video3);
              Video video4 = new Video() { OwnerId = 4 };
              _castingContext.Videos.Add(video4);
              Video video5 = new Video() { OwnerId = 5 };
              _castingContext.Videos.Add(video5);
              _castingContext.SaveChanges();
               */
            /*
    ActorProfileVideoReference vidref = new ActorProfileVideoReference() { Profile = (ActorProfile)talent.Profile, Video = video };
         ActorProfileVideoReference vidref2 = new ActorProfileVideoReference() { Profile = (ActorProfile)talent.Profile, Video = video2 };

         ActorProfileVideoReference vidref3 = new ActorProfileVideoReference() { Profile = (ActorProfile)talent.Profile, Video = video3 };

         ActorProfileVideoReference vidref4 = new ActorProfileVideoReference() { Profile = (ActorProfile)talent.Profile, Video = video4 };

         ActorProfileVideoReference vidref5 = new ActorProfileVideoReference() { Profile = (ActorProfile)talent.Profile, Video = video5 };
         await _trep.AddNewVideoAssetReferenceAsync(false, profile.TalentId, vidref);
         await _trep.AddNewVideoAssetReferenceAsync(false,  profile.TalentId, vidref2);
         await _trep.AddNewVideoAssetReferenceAsync(false, profile.TalentId, vidref3);
         await _trep.AddNewVideoAssetReferenceAsync(false,  profile.TalentId, vidref4);
         await _trep.AddNewVideoAssetReferenceAsync(true, profile.TalentId, vidref5);
         var vid = await _trep.GetVideoAssetReferenceByIdAsync(false, 1);
         var vid2 = await _trep.GetVideoAssetReferenceByIdAsync(true, 1);
         var b = vid.VideoId;
         await _castingContext.SaveChangesAsync();

             }
        */

            /*

            var talento = new StudioTalent()
            { FirstNames = "test", LastNames = "test", Profile = new ActorProfile() { WorkingName = "dlkjd" } };
            var tttt = await _rep.GetProfileById(true,true,true, ProfileTypeEnum.Actor,1);
            // Task.Run(async() => await _rep.CreateTalentAsync(talento));
            // var talentss = _rep.Personas().ToListAsync();
            var pro = (from prof in _castingContext.Profiles.OfType<ActorProfile>()
                       select new { heads = from hed in prof.ImageReferences select hed.Image, ress = from re in prof.ResumeReferences select re.Resume }).ToList();
            var toto = pro.ToList();
            var profile2 = (ActorProfile)_castingContext.Profiles.OfType<ActorProfile>().Where(p => p.TalentId == 1)
                .Include(h => h.ImageReferences).Include(r => r.ResumeReferences).FirstOrDefault();
                
            var talent2 = _castingContext.Talents.Where(ti => ti.Id == 3).Include(p => p.Profile).FirstOrDefault();
            var talent3 = _castingContext.Talents.Where(ti => ti.Id == 18).Include(p => p.Profile).FirstOrDefault();
            var talent4 = _castingContext.Talents.Where(ti => ti.Id == 6).Include(p => p.Profile).FirstOrDefault();
            var usert = await _userManager.FindByEmailAsync("cartie2r@hogftmail.com").ConfigureAwait(false);
          //usert.BaseCastingUser = talent3;
           // var tt = await _userManager.UpdateAsync(usert).ConfigureAwait(false);
            //  _castingContext.UserReferences.Add(new UserReference() {BaseCastingUser = talent2, ApplicationUser = usert});
            //  await _castingContext.SaveChangesAsync();
            var tall = new HouseTalent() {FirstNames = "lkjkjssd"};
            var profile4 = new ActorProfile()
                {

                    //Talent = tall,
                };
            tall.Profile = profile4;
            _castingContext.BaseUsers.Add(tall);
           // _castingContext.Profiles.Add(profile4);
            // tall.Profile = profile4;
            //  _castingContext.SetState(talent3, EntityState.Modified);
            await _castingContext.SaveChangesAsync().ConfigureAwait(false);
            _castingContext.ActorProfiles.Remove(profile4);
            _castingContext.VoiceActorProfiles.Add(new VoiceActorProfile() { WorkingName = "test", Talent = tall});
         //  tall.Profile = proffff;
           await _castingContext.SaveChangesAsync();
            var user = new ApplicationUser() { UserName = "joebloe999", Email = "cartie999r@hotmail.com", BaseCastingUser = tall };
            await _userManager.CreateAsync(user).ConfigureAwait(false);

//_castingContext.Profiles.Add(profile4);


            int to = 0;
           
            var talent3 = _castingContext.Talents.Where(ti => ti.Id == 18).FirstOrDefault();
            if (talent3 != null && talent3.Profile == null)
            {
                talent3.Profile = new ActorProfile();
                _castingContext.SetModified(talent3);
                _castingContext.SaveChanges();
            }

            var re = _castingContext.Profiles.Find(2);
            if (re != null)
            {
                _castingContext.Profiles.Remove(re);
                var talent2 = _castingContext.Talents.Where(ti => ti.Id == 2).FirstOrDefault();
                if (talent2 != null && talent2.Profile == null)
                {
                    talent2.Profile = new ActorProfile() ;
                    _castingContext.SetModified(talent2);
                    _castingContext.SaveChanges();
                }


            }
            var tall = new HouseTalent() { FirstNames = "lkjkjssd" };
            var profile4 = new ActorProfile()
            {

                //Talent = tall,
            };
            tall.Profile = profile4;
            _castingContext.BaseUsers.Add(tall);
            // _castingContext.Profiles.Add(profile4);

            // tall.Profile = profile4;
            //  _castingContext.SetState(talent3, EntityState.Modified);
          //  await _castingContext.SaveChangesAsync().ConfigureAwait(false);
           // _castingContext.ActorProfiles.Remove(profile4);
          //  _castingContext.VoiceActorProfiles.Add(new VoiceActorProfile() { WorkingName = "test", Talent = tall });
            //  tall.Profile = proffff;
           await _castingContext.SaveChangesAsync();
           var userb = _castingContext.BaseUsers.FirstOrDefault(b => b.Id == 11);
           if (userb != null)
           {
               var user = new ApplicationUser() { UserName = "joebloe999", Email = "cartie999r@hotmail.com", BaseCastingUser = userb };
               await _userManager.CreateAsync(user).ConfigureAwait(false);
            }
             */
            /*
            //var prottt = await _castingContext.Profiles.FirstOrDefaultAsync(p => p.Id == 18);
            var prottt = await _castingContext.Talents.FirstOrDefaultAsync(p => p.Id == 18);
          var profileo = new VoiceActorProfile() {WorkingName = "kjlkj", Talent = prottt};
          // _castingContext.SetState(prottt, EntityState.Modified);
              //_castingContext.Profiles.Remove(prottt);
              _castingContext.Profiles.Add(profileo);
// prottt.Profile = null;
            await _castingContext.SaveChangesAsync();
            return await Task.FromResult(true);*/
            return View("TalentApp");

        }
    }
}
