﻿using Casting.BLL.CastingSessions;
using Casting.BLL.CastingSessions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Sessions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingSessions;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using Casting.DAL;
using Casting.Model.Entities.AttributeReferences;
using System.Data.Entity;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingSessionroles")]
   // [Authorize]
    public class SessionRolesController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingSessionRoleService _castingSessionRoleService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public SessionRolesController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, ICastingSessionRoleService castingSessionRoleService, IProductionService productionService, ICastingContext castingContext) : base(castingContext)
        {
            _productionService = productionService;
            _castingSessionRoleService = castingSessionRoleService;
            _metadataService = metadataService;
            _userManager = userManager;
        }


        // GET: api/CastingSessionRoles
        [ResponseType(typeof(IEnumerable<CastingSessionRole>))]
        public async Task<IHttpActionResult> GetCastingSessionRoles(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionRoles =
                await _castingSessionRoleService.GetCastingSessionRolesByCastingSessionIdAsync(0);

            return Ok(castingSessionRoles);
        }

        // GET: api/CastingSessionRoles
        [System.Web.Http.ActionName("castingSession")]
        [Route("castingSession/{castingSessionId}")]
        [ResponseType(typeof(IEnumerable<CastingSessionRole>))]
        public async Task<IHttpActionResult> GetCastingSessionRolesForCastingSession(int castingSessionId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionRoles =
                await _castingSessionRoleService.GetCastingSessionRolesByCastingSessionIdWithPersonasAndAssetsAsync(castingSessionId);

            return Ok(castingSessionRoles);
        }

        // GET: api/CastingSessionRoles/5
        [ResponseType(typeof(CastingSessionRole))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var castingSessionRole =
                await _castingSessionRoleService.GetCastingSessionRoleByIdAsync(id);

            return Ok(castingSessionRole);
        }

        // POST: api/CastingSessionRoles
        [ResponseType(typeof(CastingSessionRole))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewCastingSessionRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
             /* var castingSessionRole = new CastingSessionRole()
            {
                DirectorId = user.BaseUserId,
                Name = name
            };*/
            var castingSessionRole = new CastingSessionRole()
            {
                CastingSessionId = value.CastingSessionId,
                RoleId = value.RoleId,
                RequiresHeadshot = value.RequiresHeadshot,
                HeadshotMaxCount = value.HeadshotMaxCount,
                RequiresVideo = value.RequiresVideo,
                VideoMaxCount = value.VideoMaxCount,
                RequiresAudio = value.RequiresAudio,
                AudioMaxCount = value.AudioMaxCount,
                RequiresCoverLetter = value.RequiresCoverLetter,


            };
            foreach (var unionId in value.Unions)
            {
                var union = await _metadataService.GetUnionByIdAsync(unionId);
                if (union != null)
                {
                    castingSessionRole.Unions.Add(new CastingSessionRoleUnionReference() { Union = union });
                }
            }

            var ret = await _castingSessionRoleService.AddNewCastingSessionRoleAsync(true, castingSessionRole);
            return CreatedAtRoute("DefaultApi", new { id = castingSessionRole.Id }, castingSessionRole);
        }

   
        // PUT: api/CastingSessionRoles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateCastingSessionRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSessionRole = await _castingSessionRoleService.GetCastingSessionRoleByIdAsync(id);
            if (castingSessionRole == null)
            {
                return NotFound();
            }
            /*if (castingSessionRole.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/

            castingSessionRole.RequiresHeadshot = value.RequiresHeadshot;
            castingSessionRole.HeadshotMaxCount = value.HeadshotMaxCount;
            castingSessionRole.RequiresVideo = value.RequiresVideo;
            castingSessionRole.VideoMaxCount = value.VideoMaxCount;
            castingSessionRole.RequiresAudio = value.RequiresAudio;
            castingSessionRole.AudioMaxCount = value.AudioMaxCount;
            castingSessionRole.RequiresCoverLetter = value.RequiresCoverLetter;
            foreach (var stereoType in castingSessionRole.Unions.OfType<CastingSessionRoleUnionReference>().ToList())
            {
                if (!value.Unions.Any(e => e == stereoType.Id))
                {
                    _castingContext.SetState(stereoType, EntityState.Deleted);
                    castingSessionRole.Unions.Remove(stereoType);
                }
            }
            foreach (var unionId in value.Unions)
            {
                if (!castingSessionRole.Unions.Any(s => s.Id == unionId))
                {
                    var union = await _metadataService.GetUnionByIdAsync(unionId);
                    if (union != null)
                    {
                        castingSessionRole.Unions.Add(new CastingSessionRoleUnionReference() { Union = union });
                    }
                }
            }

            try
            {
                await _castingSessionRoleService.UpdateCastingSessionRoleAsync(castingSessionRole);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/CastingSessionRoles/5
        [ResponseType(typeof(CastingSessionRole))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSessionRole = await _castingSessionRoleService.GetCastingSessionRoleByIdAsync(id);
            if (castingSessionRole == null)
            {
                return NotFound();
            }
            /*if (castingSessionRole.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/



            await _castingSessionRoleService.DeleteCastingSessionRole(castingSessionRole);

            return Ok(castingSessionRole);
        }
    }
}
