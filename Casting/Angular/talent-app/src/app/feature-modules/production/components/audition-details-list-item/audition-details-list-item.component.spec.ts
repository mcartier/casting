import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditionDetailsListItemComponent } from './audition-details-list-item.component';

describe('AuditionDetailsListItemComponent', () => {
  let component: AuditionDetailsListItemComponent;
  let fixture: ComponentFixture<AuditionDetailsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditionDetailsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditionDetailsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
