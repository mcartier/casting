﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.BLL.AttributeReferences.Interfaces
{
    public interface IAttributeReferenceService
    {
        #region references

        #region language
         Task<LanguageReference> GetLanguageReferenceByIdAsync(int languageId);
        Task<ProfileLanguageReference> GetProfileLanguageReferenceByIdAsync(int languageId);
        Task<RoleLanguageReference> GetRoleLanguageReferenceByIdAsync(int languageId);
        Task<IEnumerable<ProfileLanguageReference>> GetProfileLanguageReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleLanguageReference>> GetRoleLanguageReferencesForParentAsync(int roleId, bool withAttributes);
      
        Task<int> AddNewLanguageAttributeReferenceAsync(bool orderAtTheTop,
            LanguageReference attributeReference);

        Task<int> AddNewLanguageAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, LanguageReference attributeReference);

        Task<int> AddNewProfileLanguageAttributeReferenceAsync(bool orderAtTheTop, ProfileLanguageReference attributeReference);
        Task<int> AddNewRoleLanguageAttributeReferenceAsync(bool orderAtTheTop, RoleLanguageReference attributeReference);
        Task<int> DeleteLanguageAttributeReferenceAsync(int parentId, LanguageReference attributeReference);
        Task<int> DeleteProfileLanguageAttributeReferenceAsync(ProfileLanguageReference attributeReference);
        Task<int> DeleteRoleLanguageAttributeReferenceAsync(RoleLanguageReference attributeReference);

        Task<int> UpdateLanguageAttributeReferenceAsync(int parentId,
            LanguageReference attributeReference);

        Task<int> UpdateProfileLanguageAttributeReferenceAsync(ProfileLanguageReference attributeReference);
        Task<int> UpdateRoleLanguageAttributeReferenceAsync(RoleLanguageReference attributeReference);

        Task<int> ReOrderLanguageAttributeAsync(int parentId,
            int desiredOrderIndex, LanguageReference attributeReference);

        Task<int> ReOrderProfileLanguageAttributeAsync(int desiredOrderIndex, ProfileLanguageReference attributeReference);
        Task<int> ReOrderRoleLanguageAttributeAsync(int desiredOrderIndex, RoleLanguageReference attributeReference);

        #endregion
        #region accent
         Task<AccentReference> GetAccentReferenceByIdAsync(int accentId);
        Task<ProfileAccentReference> GetProfileAccentReferenceByIdAsync(int accentId);
        Task<RoleAccentReference> GetRoleAccentReferenceByIdAsync(int accentId);
        Task<IEnumerable<ProfileAccentReference>> GetProfileAccentReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleAccentReference>> GetRoleAccentReferencesForParentAsync(int roleId, bool withAttributes);
       
        Task<int> AddNewAccentAttributeReferenceAsync(bool orderAtTheTop,
            AccentReference attributeReference);

        Task<int> AddNewAccentAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, AccentReference attributeReference);

        Task<int> AddNewProfileAccentAttributeReferenceAsync(bool orderAtTheTop, ProfileAccentReference attributeReference);
        Task<int> AddNewRoleAccentAttributeReferenceAsync(bool orderAtTheTop, RoleAccentReference attributeReference);
         Task<int> DeleteAccentAttributeReferenceAsync(int parentId, AccentReference attributeReference);
        Task<int> DeleteProfileAccentAttributeReferenceAsync(ProfileAccentReference attributeReference);
        Task<int> DeleteRoleAccentAttributeReferenceAsync(RoleAccentReference attributeReference);
        
        Task<int> UpdateAccentAttributeReferenceAsync(int parentId,
            AccentReference attributeReference);

        Task<int> UpdateProfileAccentAttributeReferenceAsync(ProfileAccentReference attributeReference);
        Task<int> UpdateRoleAccentAttributeReferenceAsync(RoleAccentReference attributeReference);
        
        Task<int> ReOrderAccentAttributeAsync(int parentId,
            int desiredOrderIndex, AccentReference attributeReference);

        Task<int> ReOrderProfileAccentAttributeAsync(int desiredOrderIndex, ProfileAccentReference attributeReference);
       Task<int> ReOrderRoleAccentAttributeAsync(int desiredOrderIndex, RoleAccentReference attributeReference);

        #endregion
        #region union
         Task<UnionReference> GetUnionReferenceByIdAsync(int unionId);
        Task<ProfileUnionReference> GetProfileUnionReferenceByIdAsync(int unionId);
        Task<CastingCallUnionReference> GetCastingCallUnionReferenceByIdAsync(int unionId);
        Task<IEnumerable<ProfileUnionReference>> GetProfileUnionReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<CastingCallUnionReference>> GetCastingCallUnionReferencesForParentAsync(int castingCallId, bool withAttributes);

        Task<int> AddNewUnionAttributeReferenceAsync(bool orderAtTheTop,
            UnionReference attributeReference);

        Task<int> AddNewUnionAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, UnionReference attributeReference);

        Task<int> AddNewProfileUnionAttributeReferenceAsync(bool orderAtTheTop, ProfileUnionReference attributeReference);
        Task<int> AddNewCastingCallUnionAttributeReferenceAsync(bool orderAtTheTop, CastingCallUnionReference attributeReference);
        Task<int> AddNewCastingCallRoleUnionAttributeReferenceAsync(bool orderAtTheTop, CastingCallRoleUnionReference attributeReference);
        Task<int> DeleteUnionAttributeReferenceAsync(int parentId, UnionReference attributeReference);
        Task<int> DeleteProfileUnionAttributeReferenceAsync(ProfileUnionReference attributeReference);
        Task<int> DeleteCastingCallUnionAttributeReferenceAsync(CastingCallUnionReference attributeReference);
        Task<int> DeleteCastingCallRoleUnionAttributeReferenceAsync(CastingCallRoleUnionReference attributeReference);

        Task<int> UpdateUnionAttributeReferenceAsync(int parentId,
            UnionReference attributeReference);

        Task<int> UpdateProfileUnionAttributeReferenceAsync(ProfileUnionReference attributeReference);
        Task<int> UpdateCastingCallUnionAttributeReferenceAsync(CastingCallUnionReference attributeReference);
        Task<int> UpdateCastingCallRoleUnionAttributeReferenceAsync(CastingCallRoleUnionReference attributeReference);

        Task<int> ReOrderUnionAttributeAsync(int parentId,
            int desiredOrderIndex, UnionReference attributeReference);

        Task<int> ReOrderProfileUnionAttributeAsync(int desiredOrderIndex, ProfileUnionReference attributeReference);
        Task<int> ReOrderCastingCallUnionAttributeAsync(int desiredOrderIndex, CastingCallUnionReference attributeReference);
        Task<int> ReOrderCastingCallRoleUnionAttributeAsync(int desiredOrderIndex, CastingCallRoleUnionReference attributeReference);

        #endregion
        #region ethnicity
          Task<EthnicityReference> GetEthnicityReferenceByIdAsync(int ethnicityId);
        Task<ProfileEthnicityReference> GetProfileEthnicityReferenceByIdAsync(int ethnicityId);
        Task<RoleEthnicityReference> GetRoleEthnicityReferenceByIdAsync(int ethnicityId);
        Task<IEnumerable<ProfileEthnicityReference>> GetProfileEthnicityReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleEthnicityReference>> GetRoleEthnicityReferencesForParentAsync(int roleId, bool withAttributes);
        
        Task<int> AddNewEthnicityAttributeReferenceAsync(bool orderAtTheTop,
            EthnicityReference attributeReference);

        Task<int> AddNewEthnicityAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, EthnicityReference attributeReference);

        Task<int> AddNewProfileEthnicityAttributeReferenceAsync(bool orderAtTheTop, ProfileEthnicityReference attributeReference);
        Task<int> AddNewRoleEthnicityAttributeReferenceAsync(bool orderAtTheTop, RoleEthnicityReference attributeReference);
       Task<int> DeleteEthnicityAttributeReferenceAsync(int parentId, EthnicityReference attributeReference);
        Task<int> DeleteProfileEthnicityAttributeReferenceAsync(ProfileEthnicityReference attributeReference);
        Task<int> DeleteRoleEthnicityAttributeReferenceAsync(RoleEthnicityReference attributeReference);
       
        Task<int> UpdateEthnicityAttributeReferenceAsync(int parentId,
            EthnicityReference attributeReference);

        Task<int> UpdateProfileEthnicityAttributeReferenceAsync(ProfileEthnicityReference attributeReference);
        Task<int> UpdateRoleEthnicityAttributeReferenceAsync(RoleEthnicityReference attributeReference);
       
        Task<int> ReOrderEthnicityAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicityReference attributeReference);

        Task<int> ReOrderProfileEthnicityAttributeAsync(int desiredOrderIndex, ProfileEthnicityReference attributeReference);
        Task<int> ReOrderRoleEthnicityAttributeAsync(int desiredOrderIndex, RoleEthnicityReference attributeReference);

        #endregion
        #region ethnicstereotype
          Task<EthnicStereoTypeReference> GetEthnicStereoTypeReferenceByIdAsync(int ethnicStereoTypeId);
        Task<ProfileEthnicStereoTypeReference> GetProfileEthnicStereoTypeReferenceByIdAsync(int ethnicStereoTypeId);
        Task<RoleEthnicStereoTypeReference> GetRoleEthnicStereoTypeReferenceByIdAsync(int ethnicStereoTypeId);
        Task<IEnumerable<ProfileEthnicStereoTypeReference>> GetProfileEthnicStereoTypeReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleEthnicStereoTypeReference>> GetRoleEthnicStereoTypeReferencesForParentAsync(int roleId, bool withAttributes);
      
        Task<int> AddNewEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
            EthnicStereoTypeReference attributeReference);

        Task<int> AddNewEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, EthnicStereoTypeReference attributeReference);

        Task<int> AddNewProfileEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop, ProfileEthnicStereoTypeReference attributeReference);
        Task<int> AddNewRoleEthnicStereoTypeAttributeReferenceAsync(bool orderAtTheTop, RoleEthnicStereoTypeReference attributeReference);
        Task<int> DeleteEthnicStereoTypeAttributeReferenceAsync(int parentId, EthnicStereoTypeReference attributeReference);
        Task<int> DeleteProfileEthnicStereoTypeAttributeReferenceAsync(ProfileEthnicStereoTypeReference attributeReference);
        Task<int> DeleteRoleEthnicStereoTypeAttributeReferenceAsync(RoleEthnicStereoTypeReference attributeReference);

        Task<int> UpdateEthnicStereoTypeAttributeReferenceAsync(int parentId,
            EthnicStereoTypeReference attributeReference);

        Task<int> UpdateProfileEthnicStereoTypeAttributeReferenceAsync(ProfileEthnicStereoTypeReference attributeReference);
        Task<int> UpdateRoleEthnicStereoTypeAttributeReferenceAsync(RoleEthnicStereoTypeReference attributeReference);

        Task<int> ReOrderEthnicStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicStereoTypeReference attributeReference);

        Task<int> ReOrderProfileEthnicStereoTypeAttributeAsync(int desiredOrderIndex, ProfileEthnicStereoTypeReference attributeReference);
        Task<int> ReOrderRoleEthnicStereoTypeAttributeAsync(int desiredOrderIndex, RoleEthnicStereoTypeReference attributeReference);

        #endregion
        #region stereotype
         Task<StereoTypeReference> GetStereoTypeReferenceByIdAsync(int stereoTypeId);
        Task<ProfileStereoTypeReference> GetProfileStereoTypeReferenceByIdAsync(int stereoTypeId);
        Task<RoleStereoTypeReference> GetRoleStereoTypeReferenceByIdAsync(int stereoTypeId);
        Task<IEnumerable<ProfileStereoTypeReference>> GetProfileStereoTypeReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleStereoTypeReference>> GetRoleStereoTypeReferencesForParentAsync(int roleId, bool withAttributes);
     
        Task<int> AddNewStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
            StereoTypeReference attributeReference);

        Task<int> AddNewStereoTypeAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, StereoTypeReference attributeReference);

        Task<int> AddNewProfileStereoTypeAttributeReferenceAsync(bool orderAtTheTop, ProfileStereoTypeReference attributeReference);
        Task<int> AddNewRoleStereoTypeAttributeReferenceAsync(bool orderAtTheTop, RoleStereoTypeReference attributeReference);
        Task<int> DeleteStereoTypeAttributeReferenceAsync(int parentId, StereoTypeReference attributeReference);
        Task<int> DeleteProfileStereoTypeAttributeReferenceAsync(ProfileStereoTypeReference attributeReference);
        Task<int> DeleteRoleStereoTypeAttributeReferenceAsync(RoleStereoTypeReference attributeReference);

        Task<int> UpdateStereoTypeAttributeReferenceAsync(int parentId,
            StereoTypeReference attributeReference);

        Task<int> UpdateProfileStereoTypeAttributeReferenceAsync(ProfileStereoTypeReference attributeReference);
        Task<int> UpdateRoleStereoTypeAttributeReferenceAsync(RoleStereoTypeReference attributeReference);

        Task<int> ReOrderStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, StereoTypeReference attributeReference);

        Task<int> ReOrderProfileStereoTypeAttributeAsync(int desiredOrderIndex, ProfileStereoTypeReference attributeReference);
        Task<int> ReOrderRoleStereoTypeAttributeAsync(int desiredOrderIndex, RoleStereoTypeReference attributeReference);

        #endregion
        #region bodytype
        Task<BodyTypeReference> GetBodyTypeReferenceByIdAsync(int bodyTypeId);
        //Task<ProfileBodyTypeReference> GetProfileBodyTypeReferenceByIdAsync(int bodyTypeId);
        Task<RoleBodyTypeReference> GetRoleBodyTypeReferenceByIdAsync(int bodyTypeId);
       // Task<IEnumerable<ProfileBodyTypeReference>> GetProfileBodyTypeReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleBodyTypeReference>> GetRoleBodyTypeReferencesForParentAsync(int roleId, bool withAttributes);
      
        Task<int> AddNewBodyTypeAttributeReferenceAsync(bool orderAtTheTop,
            BodyTypeReference attributeReference);

        Task<int> AddNewBodyTypeAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, BodyTypeReference attributeReference);

       // Task<int> AddNewProfileBodyTypeAttributeReferenceAsync(bool orderAtTheTop, ProfileBodyTypeReference attributeReference);
        Task<int> AddNewRoleBodyTypeAttributeReferenceAsync(bool orderAtTheTop, RoleBodyTypeReference attributeReference);
        Task<int> DeleteBodyTypeAttributeReferenceAsync(int parentId, BodyTypeReference attributeReference);
       // Task<int> DeleteProfileBodyTypeAttributeReferenceAsync(ProfileBodyTypeReference attributeReference);
        Task<int> DeleteRoleBodyTypeAttributeReferenceAsync(RoleBodyTypeReference attributeReference);

        Task<int> UpdateBodyTypeAttributeReferenceAsync(int parentId,
            BodyTypeReference attributeReference);

       // Task<int> UpdateProfileBodyTypeAttributeReferenceAsync(ProfileBodyTypeReference attributeReference);
        Task<int> UpdateRoleBodyTypeAttributeReferenceAsync(RoleBodyTypeReference attributeReference);

        Task<int> ReOrderBodyTypeAttributeAsync(int parentId,
            int desiredOrderIndex, BodyTypeReference attributeReference);

      //  Task<int> ReOrderProfileBodyTypeAttributeAsync(int desiredOrderIndex, ProfileBodyTypeReference attributeReference);
        Task<int> ReOrderRoleBodyTypeAttributeAsync(int desiredOrderIndex, RoleBodyTypeReference attributeReference);

        #endregion

        #region haircolor
          Task<HairColorReference> GetHairColorReferenceByIdAsync(int hairColorId);
        //Task<ProfileHairColorReference> GetProfileHairColorReferenceByIdAsync(int hairColorId);
        Task<RoleHairColorReference> GetRoleHairColorReferenceByIdAsync(int hairColorId);
       // Task<IEnumerable<ProfileHairColorReference>> GetProfileHairColorReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleHairColorReference>> GetRoleHairColorReferencesForParentAsync(int roleId, bool withAttributes);
      
        Task<int> AddNewHairColorAttributeReferenceAsync(bool orderAtTheTop,
            HairColorReference attributeReference);

        Task<int> AddNewHairColorAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, HairColorReference attributeReference);

      //  Task<int> AddNewProfileHairColorAttributeReferenceAsync(bool orderAtTheTop, ProfileHairColorReference attributeReference);
        Task<int> AddNewRoleHairColorAttributeReferenceAsync(bool orderAtTheTop, RoleHairColorReference attributeReference);
        Task<int> DeleteHairColorAttributeReferenceAsync(int parentId, HairColorReference attributeReference);
      //  Task<int> DeleteProfileHairColorAttributeReferenceAsync(ProfileHairColorReference attributeReference);
        Task<int> DeleteRoleHairColorAttributeReferenceAsync(RoleHairColorReference attributeReference);

        Task<int> UpdateHairColorAttributeReferenceAsync(int parentId,
            HairColorReference attributeReference);

       // Task<int> UpdateProfileHairColorAttributeReferenceAsync(ProfileHairColorReference attributeReference);
        Task<int> UpdateRoleHairColorAttributeReferenceAsync(RoleHairColorReference attributeReference);

        Task<int> ReOrderHairColorAttributeAsync(int parentId,
            int desiredOrderIndex, HairColorReference attributeReference);

      //  Task<int> ReOrderProfileHairColorAttributeAsync(int desiredOrderIndex, ProfileHairColorReference attributeReference);
        Task<int> ReOrderRoleHairColorAttributeAsync(int desiredOrderIndex, RoleHairColorReference attributeReference);

        #endregion

        #region eyecolor
          Task<EyeColorReference> GetEyeColorReferenceByIdAsync(int eyeColorId);
       // Task<ProfileEyeColorReference> GetProfileEyeColorReferenceByIdAsync(int eyeColorId);
        Task<RoleEyeColorReference> GetRoleEyeColorReferenceByIdAsync(int eyeColorId);
       // Task<IEnumerable<ProfileEyeColorReference>> GetProfileEyeColorReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleEyeColorReference>> GetRoleEyeColorReferencesForParentAsync(int roleId, bool withAttributes);
      
        Task<int> AddNewEyeColorAttributeReferenceAsync(bool orderAtTheTop,
            EyeColorReference attributeReference);

        Task<int> AddNewEyeColorAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, EyeColorReference attributeReference);

       // Task<int> AddNewProfileEyeColorAttributeReferenceAsync(bool orderAtTheTop, ProfileEyeColorReference attributeReference);
        Task<int> AddNewRoleEyeColorAttributeReferenceAsync(bool orderAtTheTop, RoleEyeColorReference attributeReference);
        Task<int> DeleteEyeColorAttributeReferenceAsync(int parentId, EyeColorReference attributeReference);
       // Task<int> DeleteProfileEyeColorAttributeReferenceAsync(ProfileEyeColorReference attributeReference);
        Task<int> DeleteRoleEyeColorAttributeReferenceAsync(RoleEyeColorReference attributeReference);

        Task<int> UpdateEyeColorAttributeReferenceAsync(int parentId,
            EyeColorReference attributeReference);

       // Task<int> UpdateProfileEyeColorAttributeReferenceAsync(ProfileEyeColorReference attributeReference);
        Task<int> UpdateRoleEyeColorAttributeReferenceAsync(RoleEyeColorReference attributeReference);

        Task<int> ReOrderEyeColorAttributeAsync(int parentId,
            int desiredOrderIndex, EyeColorReference attributeReference);

     //   Task<int> ReOrderProfileEyeColorAttributeAsync(int desiredOrderIndex, ProfileEyeColorReference attributeReference);
        Task<int> ReOrderRoleEyeColorAttributeAsync(int desiredOrderIndex, RoleEyeColorReference attributeReference);

        #endregion

        #region skill
        Task<SkillReference> GetSkillReferenceByIdAsync(int skillId);
        Task<ProfileSkillReference> GetProfileSkillReferenceByIdAsync(int skillId);
        Task<RoleSkillReference> GetRoleSkillReferenceByIdAsync(int skillId);
        Task<IEnumerable<ProfileSkillReference>> GetProfileSkillReferencesForParentAsync(int profileId, bool withAttributes);
        Task<IEnumerable<RoleSkillReference>> GetRoleSkillReferencesForParentAsync(int roleId, bool withAttributes);
      
        Task<int> AddNewSkillAttributeReferenceAsync(bool orderAtTheTop,
            SkillReference attributeReference);

        Task<int> AddNewSkillAttributeReferenceAsync(bool orderAtTheTop,
            int parentId, SkillReference attributeReference);

        Task<int> AddNewProfileSkillAttributeReferenceAsync(bool orderAtTheTop, ProfileSkillReference attributeReference);
        Task<int> AddNewRoleSkillAttributeReferenceAsync(bool orderAtTheTop, RoleSkillReference attributeReference);
        Task<int> DeleteSkillAttributeReferenceAsync(int parentId, SkillReference attributeReference);
        Task<int> DeleteProfileSkillAttributeReferenceAsync(ProfileSkillReference attributeReference);
        Task<int> DeleteRoleSkillAttributeReferenceAsync(RoleSkillReference attributeReference);

        Task<int> UpdateSkillAttributeReferenceAsync(int parentId,
            SkillReference attributeReference);

        Task<int> UpdateProfileSkillAttributeReferenceAsync(ProfileSkillReference attributeReference);
        Task<int> UpdateRoleSkillAttributeReferenceAsync(RoleSkillReference attributeReference);

        Task<int> ReOrderSkillAttributeAsync(int parentId,
            int desiredOrderIndex, SkillReference attributeReference);

        Task<int> ReOrderProfileSkillAttributeAsync(int desiredOrderIndex, ProfileSkillReference attributeReference);
        Task<int> ReOrderRoleSkillAttributeAsync(int desiredOrderIndex, RoleSkillReference attributeReference);

        #endregion

        #endregion

           }
}