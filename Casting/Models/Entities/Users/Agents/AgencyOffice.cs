﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Locations;

namespace Casting.Model.Entities.Users.Agents
{
    public class AgencyOffice:Logger
    {
        public AgencyOffice()
        {
            Agents = new List<Agent>();
         
        }

        public int Id { get; set; }
        public int AgencyId { get; set; }
        public Agency Agency { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
        public ICollection<Agent> Agents { get; set; }

    }
}
