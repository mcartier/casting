﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Metadata;
using Casting.BLL.Metadata.Interfaces;

using Casting.Model.Entities.Attributes;

namespace Casting.BLL.Metadata
{
    public class MetadataService : BaseService, IMetadataService
    {
        private IMetadataAsyncRepository _metadataRepository;

        public MetadataService(IMetadataAsyncRepository metadataRepository)
        {
            _metadataRepository = metadataRepository;
        }

        #region Unions
        public async Task<Union> GetUnionByIdAsync(int unionId)
        {
            var union = await _metadataRepository.GetUnionByIdAsync(unionId);
            return union;
        }
        public async Task<IEnumerable<Union>> GetUnionsAsync()
        {
            var unions = await _metadataRepository.GetUnionsAsync();
            return unions;
        }

        public async Task<int> AddNewUnionAsync(Union union)
        {
            return await _metadataRepository.AddNewUnionAsync(union);
        }

        public async Task<int> UpdateUnionAsync(Union union)
        {
            return await _metadataRepository.UpdateUnionAsync(union);
        }

        public async Task<int> DeleteUnion(Union union)
        {
            return await _metadataRepository.DeleteUnionAsync(union);
        }

        #endregion

        #region EthnicStereoTypes
        public async Task<EthnicStereoType> GetEthnicStereoTypeByIdAsync(int ethnicStereoTypeId)
        {
            var ethnicStereoType = await _metadataRepository.GetEthnicStereoTypeByIdAsync(ethnicStereoTypeId);
            return ethnicStereoType;
        }
        public async Task<IEnumerable<EthnicStereoType>> GetEthnicStereoTypesAsync()
        {
            var ethnicStereoTypes = await _metadataRepository.GetEthnicStereoTypesAsync();
            return ethnicStereoTypes;
        }

        public async Task<int> AddNewEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType)
        {
            return await _metadataRepository.AddNewEthnicStereoTypeAsync(ethnicStereoType);
        }

        public async Task<int> UpdateEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType)
        {
            return await _metadataRepository.UpdateEthnicStereoTypeAsync(ethnicStereoType);
        }

        public async Task<int> DeleteEthnicStereoType(EthnicStereoType ethnicStereoType)
        {
            return await _metadataRepository.DeleteEthnicStereoTypeAsync(ethnicStereoType);
        }

        #endregion

        #region StereoTypes
        public async Task<StereoType> GetStereoTypeByIdAsync(int stereoTypeId)
        {
            var stereoType = await _metadataRepository.GetStereoTypeByIdAsync(stereoTypeId);
            return stereoType;
        }
        public async Task<IEnumerable<StereoType>> GetStereoTypesAsync()
        {
            var stereoTypes = await _metadataRepository.GetStereoTypesAsync();
            return stereoTypes;
        }

        public async Task<int> AddNewStereoTypeAsync(StereoType stereoType)
        {
            return await _metadataRepository.AddNewStereoTypeAsync(stereoType);
        }

        public async Task<int> UpdateStereoTypeAsync(StereoType stereoType)
        {
            return await _metadataRepository.UpdateStereoTypeAsync(stereoType);
        }

        public async Task<int> DeleteStereoType(StereoType stereoType)
        {
            return await _metadataRepository.DeleteStereoTypeAsync(stereoType);
        }

        #endregion

        #region Ethnicities
        public async Task<Ethnicity> GetEthnicityByIdAsync(int ethnicityId)
        {
            var ethnicity = await _metadataRepository.GetEthnicityByIdAsync(ethnicityId);
            return ethnicity;
        }
        public async Task<IEnumerable<Ethnicity>> GetEthnicitiesAsync()
        {
            var ethnicities = await _metadataRepository.GetEthnicitiesAsync();
            return ethnicities;
        }

        public async Task<int> AddNewEthnicityAsync(Ethnicity ethnicity)
        {
            return await _metadataRepository.AddNewEthnicityAsync(ethnicity);
        }

        public async Task<int> UpdateEthnicityAsync(Ethnicity ethnicity)
        {
            return await _metadataRepository.UpdateEthnicityAsync(ethnicity);
        }

        public async Task<int> DeleteEthnicity(Ethnicity ethnicity)
        {
            return await _metadataRepository.DeleteEthnicityAsync(ethnicity);
        }

        #endregion

        #region Languages
        public async Task<Language> GetLanguageByIdAsync(int languageId)
        {
            var language = await _metadataRepository.GetLanguageByIdAsync(languageId);
            return language;
        }
        public async Task<IEnumerable<Language>> GetLanguagesAsync()
        {
            var languages = await _metadataRepository.GetLanguagesAsync();
            return languages;
        }

        public async Task<int> AddNewLanguageAsync(Language language)
        {
            return await _metadataRepository.AddNewLanguageAsync(language);
        }

        public async Task<int> UpdateLanguageAsync(Language language)
        {
            return await _metadataRepository.UpdateLanguageAsync(language);
        }

        public async Task<int> DeleteLanguage(Language language)
        {
            return await _metadataRepository.DeleteLanguageAsync(language);
        }

        #endregion

        #region Accents
        public async Task<Accent> GetAccentByIdAsync(int accentId)
        {
            var accent = await _metadataRepository.GetAccentByIdAsync(accentId);
            return accent;
        }
        public async Task<IEnumerable<Accent>> GetAccentsAsync()
        {
            var accents = await _metadataRepository.GetAccentsAsync();
            return accents;
        }

        public async Task<int> AddNewAccentAsync(Accent accent)
        {
            return await _metadataRepository.AddNewAccentAsync(accent);
        }

        public async Task<int> UpdateAccentAsync(Accent accent)
        {
            return await _metadataRepository.UpdateAccentAsync(accent);
        }

        public async Task<int> DeleteAccent(Accent accent)
        {
            return await _metadataRepository.DeleteAccentAsync(accent);
        }

        #endregion

        #region HairColors
        public async Task<HairColor> GetHairColorByIdAsync(int hairColorId)
        {
            var hairColor = await _metadataRepository.GetHairColorByIdAsync(hairColorId);
            return hairColor;
        }
        public async Task<IEnumerable<HairColor>> GetHairColorsAsync()
        {
            var hairColors = await _metadataRepository.GetHairColorsAsync();
            return hairColors;
        }

        public async Task<int> AddNewHairColorAsync(HairColor hairColor)
        {
            return await _metadataRepository.AddNewHairColorAsync(hairColor);
        }

        public async Task<int> UpdateHairColorAsync(HairColor hairColor)
        {
            return await _metadataRepository.UpdateHairColorAsync(hairColor);
        }

        public async Task<int> DeleteHairColor(HairColor hairColor)
        {
            return await _metadataRepository.DeleteHairColorAsync(hairColor);
        }

        #endregion

        #region EyeColors
        public async Task<EyeColor> GetEyeColorByIdAsync(int eyeColorId)
        {
            var eyeColor = await _metadataRepository.GetEyeColorByIdAsync(eyeColorId);
            return eyeColor;
        }
        public async Task<IEnumerable<EyeColor>> GetEyeColorsAsync()
        {
            var eyeColors = await _metadataRepository.GetEyeColorsAsync();
            return eyeColors;
        }

        public async Task<int> AddNewEyeColorAsync(EyeColor eyeColor)
        {
            return await _metadataRepository.AddNewEyeColorAsync(eyeColor);
        }

        public async Task<int> UpdateEyeColorAsync(EyeColor eyeColor)
        {
            return await _metadataRepository.UpdateEyeColorAsync(eyeColor);
        }

        public async Task<int> DeleteEyeColor(EyeColor eyeColor)
        {
            return await _metadataRepository.DeleteEyeColorAsync(eyeColor);
        }

        #endregion

        #region BodyTypes
        public async Task<BodyType> GetBodyTypeByIdAsync(int bodyTypeId)
        {
            var bodyType = await _metadataRepository.GetBodyTypeByIdAsync(bodyTypeId);
            return bodyType;
        }
        public async Task<IEnumerable<BodyType>> GetBodyTypesAsync()
        {
            var bodyTypes = await _metadataRepository.GetBodyTypesAsync();
            return bodyTypes;
        }

        public async Task<int> AddNewBodyTypeAsync(BodyType bodyType)
        {
            return await _metadataRepository.AddNewBodyTypeAsync(bodyType);
        }

        public async Task<int> UpdateBodyTypeAsync(BodyType bodyType)
        {
            return await _metadataRepository.UpdateBodyTypeAsync(bodyType);
        }

        public async Task<int> DeleteBodyType(BodyType bodyType)
        {
            return await _metadataRepository.DeleteBodyTypeAsync(bodyType);
        }

        #endregion

        
        #region Skills
        public async Task<Skill> GetSkillByIdAsync(int skillId)
        {
            var skill = await _metadataRepository.GetSkillByIdAsync(skillId,false);
            return skill;
        }
        public async Task<IEnumerable<Skill>> GetSkillsAsync()
        {
            var skills = await _metadataRepository.GetSkillsAsync(false);
            return skills;
        }

        public async Task<Skill> GetSkillWithCategoryByIdAsync(int skillId)
        {
            var skill = await _metadataRepository.GetSkillByIdAsync(skillId,true);
            return skill;
        }
        public async Task<IEnumerable<Skill>> GetSkillsWithCategoryAsync()
        {
            var skills = await _metadataRepository.GetSkillsAsync(true);
            return skills;
        }

        public async Task<int> AddNewSkillAsync(Skill skill)
        {
            return await _metadataRepository.AddNewSkillAsync(skill);
        }

        public async Task<int> UpdateSkillAsync(Skill skill)
        {
            return await _metadataRepository.UpdateSkillAsync(skill);
        }

        public async Task<int> DeleteSkill(Skill skill)
        {
            return await _metadataRepository.DeleteSkillAsync(skill);
        }

        #endregion

          
        #region SkillCategories
        public async Task<SkillCategory> GetSkillCategoryByIdAsync(int skillCategoryId)
        {
            var skillCategory = await _metadataRepository.GetSkillCategoryByIdAsync(skillCategoryId,false);
            return skillCategory;
        }
        public async Task<IEnumerable<SkillCategory>> GetSkillCategoriesAsync()
        {
            var skillCategories = await _metadataRepository.GetSkillCategoriesAsync(false);
            return skillCategories;
        }

        public async Task<SkillCategory> GetSkillCategoryWithSkillsByIdAsync(int skillCategoryId)
        {
            var skillCategory = await _metadataRepository.GetSkillCategoryByIdAsync(skillCategoryId,true);
            return skillCategory;
        }
        public async Task<IEnumerable<SkillCategory>> GetSkillCategoriesWithSkillsAsync()
        {
            var skillCategories = await _metadataRepository.GetSkillCategoriesAsync(true);
            return skillCategories;
        }

        public async Task<int> AddNewSkillCategoryAsync(SkillCategory skillCategory)
        {
            return await _metadataRepository.AddNewSkillCategoryAsync(skillCategory);
        }

        public async Task<int> UpdateSkillCategoryAsync(SkillCategory skillCategory)
        {
            return await _metadataRepository.UpdateSkillCategoryAsync(skillCategory);
        }

        public async Task<int> DeleteSkillCategory(SkillCategory skillCategory)
        {
            return await _metadataRepository.DeleteSkillCategoryAsync(skillCategory);
        }

        #endregion



        #region RoleTypes
        public async Task<RoleType> GetRoleTypeByIdAsync(int roleTypeId)
        {
            var roleType = await _metadataRepository.GetRoleTypeByIdAsync(roleTypeId);
            return roleType;
        }
        public async Task<IEnumerable<RoleType>> GetRoleTypesAsync()
        {
            var roleTypes = await _metadataRepository.GetRoleTypesAsync();
            return roleTypes;
        }

        public async Task<int> AddNewRoleTypeAsync(RoleType roleType)
        {
            return await _metadataRepository.AddNewRoleTypeAsync(roleType);
        }

        public async Task<int> UpdateRoleTypeAsync(RoleType roleType)
        {
            return await _metadataRepository.UpdateRoleTypeAsync(roleType);
        }

        public async Task<int> DeleteRoleType(RoleType roleType)
        {
            return await _metadataRepository.DeleteRoleTypeAsync(roleType);
        }

        #endregion

        #region RoleGenderTypes
        public async Task<RoleGenderType> GetRoleGenderTypeByIdAsync(int roleGenderTypeId)
        {
            var roleGenderType = await _metadataRepository.GetRoleGenderTypeByIdAsync(roleGenderTypeId);
            return roleGenderType;
        }
        public async Task<IEnumerable<RoleGenderType>> GetRoleGenderTypesAsync()
        {
            var roleGenderTypes = await _metadataRepository.GetRoleGenderTypesAsync();
            return roleGenderTypes;
        }

        public async Task<int> AddNewRoleGenderTypeAsync(RoleGenderType roleGenderType)
        {
            return await _metadataRepository.AddNewRoleGenderTypeAsync(roleGenderType);
        }

        public async Task<int> UpdateRoleGenderTypeAsync(RoleGenderType roleGenderType)
        {
            return await _metadataRepository.UpdateRoleGenderTypeAsync(roleGenderType);
        }

        public async Task<int> DeleteRoleGenderType(RoleGenderType roleGenderType)
        {
            return await _metadataRepository.DeleteRoleGenderTypeAsync(roleGenderType);
        }

        #endregion

        #region CastingCallTypes
        public async Task<CastingCallType> GetCastingCallTypeByIdAsync(int castingCallTypeId)
        {
            var castingCallType = await _metadataRepository.GetCastingCallTypeByIdAsync(castingCallTypeId);
            return castingCallType;
        }
        public async Task<IEnumerable<CastingCallType>> GetCastingCallTypesAsync()
        {
            var castingCallTypes = await _metadataRepository.GetCastingCallTypesAsync();
            return castingCallTypes;
        }
        public async Task<IEnumerable<CastingCallType>> GetCastingCallTypesByCastingCallAsync(int castingCallId)
        {
            var castingCallTypes = await _metadataRepository.GetCastingCallTypesByCastingCallAsync(castingCallId);
            return castingCallTypes;
        }

        public async Task<int> AddNewCastingCallTypeAsync(CastingCallType castingCallType)
        {
            return await _metadataRepository.AddNewCastingCallTypeAsync(castingCallType);
        }

        public async Task<int> UpdateCastingCallTypeAsync(CastingCallType castingCallType)
        {
            return await _metadataRepository.UpdateCastingCallTypeAsync(castingCallType);
        }

        public async Task<int> DeleteCastingCallType(CastingCallType castingCallType)
        {
            return await _metadataRepository.DeleteCastingCallTypeAsync(castingCallType);
        }

        #endregion

        #region CastingCallViewTypes
        public async Task<CastingCallViewType> GetCastingCallViewTypeByIdAsync(int castingCallViewTypeId)
        {
            var castingCallViewType = await _metadataRepository.GetCastingCallViewTypeByIdAsync(castingCallViewTypeId);
            return castingCallViewType;
        }
        public async Task<IEnumerable<CastingCallViewType>> GetCastingCallViewTypesAsync()
        {
            var castingCallViewTypes = await _metadataRepository.GetCastingCallViewTypesAsync();
            return castingCallViewTypes;
        }
        public async Task<IEnumerable<CastingCallViewType>> GetCastingCallViewTypesByCastingCallAsync(int castingCallId)
        {
            var castingCallViewTypes = await _metadataRepository.GetCastingCallViewTypesByCastingCallAsync(castingCallId);
            return castingCallViewTypes;
        }

        public async Task<int> AddNewCastingCallViewTypeAsync(CastingCallViewType castingCallViewType)
        {
            return await _metadataRepository.AddNewCastingCallViewTypeAsync(castingCallViewType);
        }

        public async Task<int> UpdateCastingCallViewTypeAsync(CastingCallViewType castingCallViewType)
        {
            return await _metadataRepository.UpdateCastingCallViewTypeAsync(castingCallViewType);
        }

        public async Task<int> DeleteCastingCallViewType(CastingCallViewType castingCallViewType)
        {
            return await _metadataRepository.DeleteCastingCallViewTypeAsync(castingCallViewType);
        }

        #endregion

        #region CastingCallPrivacies
        public async Task<CastingCallPrivacy> GetCastingCallPrivacyByIdAsync(int castingCallPrivacyId)
        {
            var castingCallPrivacy = await _metadataRepository.GetCastingCallPrivacyByIdAsync(castingCallPrivacyId);
            return castingCallPrivacy;
        }
        public async Task<IEnumerable<CastingCallPrivacy>> GetCastingCallPrivaciesAsync()
        {
            var castingCallPrivacies = await _metadataRepository.GetCastingCallPrivaciesAsync();
            return castingCallPrivacies;
        }
        public async Task<IEnumerable<CastingCallPrivacy>> GetCastingCallPrivaciesByCastingCallAsync(int castingCallId)
        {
            var castingCallPrivacies = await _metadataRepository.GetCastingCallPrivaciesByCastingCallAsync(castingCallId);
            return castingCallPrivacies;
        }

        public async Task<int> AddNewCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy)
        {
            return await _metadataRepository.AddNewCastingCallPrivacyAsync(castingCallPrivacy);
        }

        public async Task<int> UpdateCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy)
        {
            return await _metadataRepository.UpdateCastingCallPrivacyAsync(castingCallPrivacy);
        }

        public async Task<int> DeleteCastingCallPrivacy(CastingCallPrivacy castingCallPrivacy)
        {
            return await _metadataRepository.DeleteCastingCallPrivacyAsync(castingCallPrivacy);
        }

        #endregion

        #region ProductionTypes
        public async Task<ProductionType> GetProductionTypeByIdAsync(int productionTypeId)
        {
            var productionType = await _metadataRepository.GetProductionTypeByIdAsync(productionTypeId);
            return productionType;
        }
        public async Task<IEnumerable<ProductionType>> GetProductionTypesAsync()
        {
            var productionTypes = await _metadataRepository.GetProductionTypesAsync();
            return productionTypes;
        }
        public async Task<IEnumerable<ProductionType>> GetProductionTypesByProductionAsync(int productionId)
        {
            var productionTypes = await _metadataRepository.GetProductionTypesByProductionAsync(productionId);
            return productionTypes;
        }

        public async Task<IEnumerable<ProductionType>> GetProductionTypesByProductionTypeCatigoryAsync(int productionTypeCategoryId)
        {
            var productionTypes = await _metadataRepository.GetProductionTypesByProductionTypeCatigoryAsync(productionTypeCategoryId);
            return productionTypes;
        }

        public async Task<int> AddNewProductionTypeAsync(ProductionType productionType)
        {
            return await _metadataRepository.AddNewProductionTypeAsync(productionType);
        }

        public async Task<int> UpdateProductionTypeAsync(ProductionType productionType)
        {
            return await _metadataRepository.UpdateProductionTypeAsync(productionType);
        }

        public async Task<int> DeleteProductionType(ProductionType productionType)
        {
            return await _metadataRepository.DeleteProductionTypeAsync(productionType);
        }

        #endregion

        #region ProductionTypeCategories
        public async Task<ProductionTypeCategory> GetProductionTypeCategoryByIdAsync(int productionTypeCategoryId)
        {
            var productionTypeCategory = await _metadataRepository.GetProductionTypeCategoryByIdAsync(productionTypeCategoryId);
            return productionTypeCategory;
        }
        public async Task<IEnumerable<ProductionTypeCategory>> GetProductionTypeCategoriesAsync()
        {
            var productionTypeCategories = await _metadataRepository.GetProductionTypeCategoriesAsync();
            return productionTypeCategories;
        }


        public async Task<IEnumerable<ProductionTypeCategory>> GetCompleteProductionTypeCategoriesAsync()
        {
            var productionTypeCategories = await _metadataRepository.GetCompleteProductionTypeCategoriesAsync();
            return productionTypeCategories;
        }

        public async Task<int> AddNewProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory)
        {
            return await _metadataRepository.AddNewProductionTypeCategoryAsync(productionTypeCategory);
        }

        public async Task<int> UpdateProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory)
        {
            return await _metadataRepository.UpdateProductionTypeCategoryAsync(productionTypeCategory);
        }

        public async Task<int> DeleteProductionTypeCategory(ProductionTypeCategory productionTypeCategory)
        {
            return await _metadataRepository.DeleteProductionTypeCategoryAsync(productionTypeCategory);
        }

        #endregion


    }
}
