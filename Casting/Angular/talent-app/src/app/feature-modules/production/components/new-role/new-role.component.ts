import { Component, OnInit,OnDestroy, Inject, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { ProductionRoleTransferObject, Production, NewProduction, Role, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { RoleDialogService } from '../../services/role-dialog.service';
import { ProductionService } from '../../../../services/production.service';
import { ProductionRoleService } from '../../../../services/production-role.service';
import { ProductionDialogService } from '../../services/production-dialog.service';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';

import { SelectItem } from 'primeng/api';
import { Dropdown } from 'primeng/dropdown';


@Component({
    selector: 'app-new-role',
    templateUrl: './new-role.component.html',
    styleUrls: ['./new-role.component.scss']
})
export class NewRoleComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor(private _formBuilder: FormBuilder,
        private ProductionRoleService: ProductionRoleService,
        public stateService: StateService,
        public transitionService: TransitionService,
        public transition: Transition,
        private productionDialogService: ProductionDialogService,
        private messageDispatchService: MessageDispatchService,
        private productionExtraService: ProductionExtraService) {



    }
    @Input() private production: Production;
    @Input() private metadata: MetadataModel;
    @Input() private role: Role;

    @ViewChild('name', {static: false}) nameField: ElementRef;
    @ViewChild('roletype', {static: false}) roleTypeField: Dropdown;
    @ViewChild('gendertype', {static: false}) genderTypeField: Dropdown;
    private minMinAge: number = 0;
    private minMaxAge: number = 49;
    // private minAge: number = 0;
    // private maxAge: number = 50;
    private maxMinAge: number = 1;
    private maxMaxAge: number = 100;
    private basicRoleInfo: FormGroup;
    private showHints: boolean = false;
    private age: number[] = [0, 100];
    private minAge: number = 0;
    private maxAge: number = 0;
    //roleTypes: SelectItem[];
    private languages: any[] = [];
    private bodyTypes: any[] = [];
    private eyeColors: any[] = [];
    private hairColors: any[] = [];
    private accents: any[] = [];
    private stereoTypes: any[] = [];
    private ethnicStereoTypes: any[] = [];
    private ethnicities: any[] = [];
    private payDetails: any[] = [];
    private genderTypes: any[] = [];
    private roleTypes: any[] = [];

    private messages: any[] = [];

    private newPayDetailSubscription: Subscription;

    ngAfterViewInit() {
        setTimeout(() => this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }));

    }

    ngOnInit() {
        this.payDetails = this.production.payDetails.map(x => ({
            value: x.id,
            label: x.name,
            id: x.id,
            productionId: x.productionId,
            name: x.name
        }));

        this.roleTypes = this.metadata.roleTypes.map(x => ({
            value: x.id,
            label: x.name,
            id: x.id,
            name: x.name
        }));

        this.genderTypes = this.metadata.roleGenderTypes.map(x => ({
            value: x.id,
            label: x.name,
            id: x.id,
            name: x.name
        }));
      
        if (this.role) {
            this.age = [this.role.minAge, this.role.maxAge];
            this.minAge = this.role.minAge;
            this.maxAge = this.role.maxAge;
            this.languages = this.role.languages.map(l => l.language);
            this.bodyTypes = this.role.bodyTypes.map(l => l.bodyType);
            this.eyeColors = this.role.eyeColors.map(l => l.eyeColor);
            this.hairColors = this.role.hairColors.map(l => l.hairColor);
            this.accents = this.role.accents.map(l => l.accent);
            this.ethnicities = this.role.ethnicities.map(l => l.ethnicity);
            this.stereoTypes = this.role.stereoTypes.map(l => l.stereoType);
            this.ethnicStereoTypes = this.role.ethnicStereoTypes.map(l => l.ethnicStereoType);
            this.basicRoleInfo = this._formBuilder.group({
                name: [this.role.name, Validators.required],
                roleTypeId: [this.role.roleTypeId, Validators.required],
                roleGenderTypeId: [this.role.roleGenderTypeId, Validators.required],
                transgender: [this.role.transgender],
                age: [this.age],
                //minAge: [this.role.minAge],
                //maxAge: [this.role.maxAge],
                description: [this.role.description],
                //accents: [this.role.accents.map(a => a.accentId)],
                //languages: [this.role.languages],
                //ethnicities: [this.role.ethnicities.map(a => a.ethnicityId)],
                //ethnicStereoTypes: [this.role.ethnicStereoTypes.map(a => a.ethnicStereoTypeId)],
                //stereoTypes: [this.role.stereoTypes.map(a => a.stereoTypeId)],
                requiresNudity: [this.role.requiresNudity],
               // payDetailId: [this.production.payDetails[this.production.payDetails.findIndex(t => t.id == this.role.payDetailId)]],
                payDetailId: [this.role.payDetailId],

            },
                { updateOn: 'blur' });
        } else {
            this.role = new Role;
            this.basicRoleInfo = this._formBuilder.group({
                name: ['', Validators.required],
                roleTypeId: [null, Validators.required],
                roleGenderTypeId: [null, Validators.required],
                transgender: [false],
                age: [this.age],
                //minAge: [0],
                //maxAge: [100],
                description: [''],
                //accents: [[]],
                //languages: [[]],
                //ethnicities: [[]],
                //ethnicStereoTypes: [[]],
                // stereoTypes: [[]],
                requiresNudity: [false],
                payDetailId: [0],

            },
                { updateOn: 'blur' });
        }
        this.newPayDetailSubscription = this.productionExtraService.subscribeToCreatePayDetailObs()
            .subscribe(
                (detail: PayDetail) => {
                    if (detail) {
                        this.production.payDetails.push(detail);
                        this.payDetails.push({
                            value: detail.id,
                            label: detail.name,
                            id: detail.id,
                            productionId: detail.productionId,
                            name: detail.name
                        });
                        this.basicRoleInfo.patchValue({
                            payDetailId: detail.id
                        });
                    }
                });


    }
    ngOnDestroy() {
        if (this.newPayDetailSubscription) {
            this.newPayDetailSubscription.unsubscribe();
        }


    }

    private onNoClick() {
        this.stateService.go(this.transition.from())
        //this.dialogRef.close();
    }
    private handleAgeSliderChange(e) {
        this.minAge = e.values[0];
        this.maxAge = e.values[1];
    }
    private onMinSliderChange($event) {
        this.minAge = $event.value;
        this.maxMaxAge = this.minAge + 1;

        this.basicRoleInfo.get('minAge').setValue($event.value);
    }
    private removePayDetail() {
        this.basicRoleInfo.patchValue({
            payDetailId: null,
        });

    }

    private addNewPayDetails() {
        this.productionDialogService.showPayDetailsDialog(null);

    }
    private onMaxSliderChange($event) {
        this.maxAge = $event.value;
        this.minMaxAge = this.maxAge - 1;
        this.basicRoleInfo.get('maxAge').setValue($event.value);

    }

    private clearMessages() {
        this.messages = [];
    }

    private getSubmissionObject(): Role {
        let role: Role = new Role;
        role.productionId = this.production.id;
        role.name = this.basicRoleInfo.get('name').value;
        role.transgender = this.basicRoleInfo.get('transgender').value;
        role.minAge = this.basicRoleInfo.get('age').value[0];
        role.maxAge = this.basicRoleInfo.get('age').value[1];
        role.description = this.basicRoleInfo.get('description').value;
        role.languages = this.languages.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.bodyTypes = this.bodyTypes.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.eyeColors = this.eyeColors.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.hairColors = this.hairColors.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.accents = this.accents.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.ethnicities = this.ethnicities.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.ethnicStereoTypes = this.ethnicStereoTypes.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.stereoTypes = this.stereoTypes.map(l => l.id);// this.basicRoleInfo.controls['languages'].value;
        role.skills = [];
        //role.accents = this.basicRoleInfo.get('accents').value;
        // role.ethnicities= this.basicRoleInfo.get('ethnicities').value;
        //  role.ethnicStereoTypes= this.basicRoleInfo.get('ethnicStereoTypes').value;
        //  role.stereoTypes= this.basicRoleInfo.get('stereoTypes').value;
        role.requiresNudity = this.basicRoleInfo.get('requiresNudity').value;
        role.roleTypeId = this.basicRoleInfo.get('roleTypeId').value;
        role.roleGenderTypeId = this.basicRoleInfo.get('roleGenderTypeId').value;
        if (this.basicRoleInfo.get('payDetailId').value)
        {
            role.payDetailId = this.basicRoleInfo.get('payDetailId').value;
        }
        else
        {
            role.payDetailId = 0;
        }
         return role;
    }
    private saveRoleClick() {
        this.clearMessages();
         if (this.basicRoleInfo.valid) {
          let submissionRole: Role = this.getSubmissionObject();
          if (this.role && this.role.id && this.role.id !== 0) {
              this.ProductionRoleService.saveRole(this.role.id, this.production.id, submissionRole).subscribe((role: Role) => {
                  this.production.roles[this.production.roles.findIndex(r => r.id == role.id)] = role;
                  this.messageDispatchService.dispatchSuccesMessage("Role:'" + role.name + "' was saved successfully",
                      "Role Saved");

                var params = {
                    productionId: this.production.id,
                    production: this.production,
                    draftId: this.production.id
                };
                if (this.transition.from().name != "draftproduction" &&
                    this.transition.from().name != "editproduction") {
                    this.stateService.go("production", params)

                } else {
                    this.stateService.go(this.transition.from(), params)

                }

            },
                (error) => {
                    this.messageDispatchService.dispatchErrorMessage(
                        "Error saving role:'" + submissionRole.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                        "Role Save Error");
                    this.messages.push({
                        severity: 'error',
                        summary: "Role Save Error",
                        detail: "Error saving role:'" + submissionRole.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                    });

                })

        } else {
              this.ProductionRoleService.createRole(this.production.id, submissionRole).subscribe((role: Role) => {
                if (role != null) {
                    this.production.roles.push(role);
                }
                  this.messageDispatchService.dispatchSuccesMessage("New role:'" + submissionRole.name + "' was created successfully",
                      "Role Created");

                var params = {
                    productionId: this.production.id,
                    production: this.production,
                    draftId: this.production.id
                };
                if (this.transition.from().name != "draftproduction" &&
                    this.transition.from().name != "editproduction") {
                    this.stateService.go("production", params)

                } else {
                    this.stateService.go(this.transition.from(), params)

                }
            },
                (error) => {
                    this.messageDispatchService.dispatchErrorMessage(
                        "Error creating role:'" + submissionRole.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                        "Role Creation Error");
                    this.messages.push({
                        severity: 'error',
                        summary: "Role Creation Error",
                        detail: "Error creating role:'" + submissionRole.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                    });

                })

        }

    } else {
        if (!this.basicRoleInfo.get('name').valid) {
            this.basicRoleInfo.get('name').markAsDirty();
            this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            this.nameField.nativeElement.focus();
        }
        else if (!this.basicRoleInfo.get('roleTypeId').valid) {
            this.basicRoleInfo.get('roleTypeId').markAsDirty();
            this.roleTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            this.roleTypeField.focusViewChild.nativeElement.focus();
        }
        else if (!this.basicRoleInfo.get('roleGenderTypeId').valid) {
            this.basicRoleInfo.get('roleGenderTypeId').markAsDirty();
            this.genderTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            this.genderTypeField.focusViewChild.nativeElement.focus();
        }

    }

}

}
