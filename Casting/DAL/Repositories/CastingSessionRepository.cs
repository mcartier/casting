﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Enums;
using Casting.Model.Entities.Sessions;

namespace Casting.DAL.Repositories
{
    public class CastingSessionRepository : AsyncRepository, ICastingSessionAsyncRepository
    {
        ICastingContext _castingContext;
        public CastingSessionRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region CastingSessions

        public async Task<CastingSession> GetCastingSessionById(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery, int castingSessionId)
        {
            IQueryable<CastingSession> castingSessions = GetCastingSessionsQueryable(folderParameterIncludeInQuery, parameterIncludeInQuery);

            return await castingSessions.FirstOrDefaultAsync(p => p.Id == castingSessionId);


        }

        public async Task<int> AddNewCastingSessionAsync(CastingSession castingSession)
        {
            _castingContext.Set<CastingSession>().Add(castingSession);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteCastingSessionAsync(CastingSession castingSession)
        {
            _castingContext.Set<CastingSession>().Remove(castingSession);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateCastingSessionAsync(CastingSession castingSession)
        {
            SetEntityState(castingSession, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionId(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery, int productionId)
        {
            var castingSessions = GetCastingSessionsQueryable(folderParameterIncludeInQuery, parameterIncludeInQuery);

            return await castingSessions.Where(d => d.ProductionId == productionId).ToListAsync();
        }

        public async Task<IEnumerable<CastingSession>> GetCastingSessions(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending)
        {
            var castingSessions = GetCastingSessionsQueryable(folderParameterIncludeInQuery, parameterIncludeInQuery);
            switch (orderBy)
            {
                case OrderByValuesEnum.CreatedDateAscending:

                    castingSessions = castingSessions.OrderBy(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.CreatedDateDescending:

                    castingSessions = castingSessions.OrderByDescending(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.NameAscending:

                    // castingSessions = castingSessions.OrderBy(o => o.Name);
                    break;
                case OrderByValuesEnum.NameDescending:

                    // castingSessions = castingSessions.OrderByDescending(o => o.Name);
                    break;
            }
            return await castingSessions.Skip(pageIndex * pageSize).Take(pageSize).ToListAsync();
        }



        #region PrivateMethods
        private IQueryable<CastingSession> GetCastingSessionsQueryable(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery)
        {
            IQueryable<CastingSession> castingSessions = from castingSession in _castingContext.CastingSessions select castingSession;
            if (parameterIncludeInQuery == CastingSessionFolderParameterIncludeInQueryEnum.Roles)
            {
                castingSessions
                    .Include(i => i.Roles);
            }

            if (parameterIncludeInQuery == CastingSessionFolderParameterIncludeInQueryEnum.RolesAndPersonas)
            {
                castingSessions
                    .Include("Roles.Personas.Persona");
            }
            if (parameterIncludeInQuery == CastingSessionFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets)
            {
                castingSessions
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }

            if (folderParameterIncludeInQuery == CastingSessionParameterIncludeInQueryEnum.Folders)
            {
                castingSessions
                    .Include(i => i.Folders);
            }

            if (folderParameterIncludeInQuery == CastingSessionParameterIncludeInQueryEnum.FodersAndRoles)
            {
                castingSessions
                    .Include("Folders.Roles");
            }

            if (folderParameterIncludeInQuery == CastingSessionParameterIncludeInQueryEnum.FoldersRolesAndPersonas)
            {
                castingSessions
                    .Include("Folders.Roles.Personas.Persona");
            }

            if (folderParameterIncludeInQuery == CastingSessionParameterIncludeInQueryEnum.FolderRolesPersonasAndAssets)
            {
                castingSessions
                    .Include("Folders.Roles.Personas.Persona")
                    .Include("Folders.Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Folders.Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Folders.Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Folders.Roles.Personas.Persona.ImageReferences.Image");
            }
            return castingSessions;
        }


        #endregion
        #endregion


        #region Roles

        public async Task<CastingSessionRole> GetCastingSessionRoleById(CastingSessionRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionRoleId)
        {
            IQueryable<CastingSessionRole> castingSessionRoles = GetCastingSessionRolesQueryable(includeInQuery);

            return await castingSessionRoles.FirstOrDefaultAsync(p => p.Id == castingSessionRoleId);


        }

        public async Task<int> AddNewCastingSessionRoleAsync(bool orderAtTheTop, CastingSessionRole castingSessionRole)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingSessionRoleByCastingSessionAsync(castingSessionRole);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingSessionRole.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingSessionRole>().Add(castingSessionRole);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingSessionRoleAsync(1, castingSessionRole);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingSessionRoleAsync(CastingSessionRole castingSessionRole)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingSessionRoleAsync(0, castingSessionRole);

                    _castingContext.Set<CastingSessionRole>().Remove(castingSessionRole);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingSessionRoleAsync(CastingSessionRole castingSessionRole)
        {
            SetEntityState(castingSessionRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionId(CastingSessionRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionId)
        {
            var castingSessionRoles = GetCastingSessionRolesQueryable(includeInQuery);

            return await castingSessionRoles.Where(d => d.CastingSessionId == castingSessionId).ToListAsync();
        }
        public async Task<int> ReOrderCastingSessionRoleAsync(int desiredOrderIndex, CastingSessionRole castingSessionRole)
        {
            IEnumerable<CastingSessionRole> castingSessionRoles = await GetCastingSessionRolesByCastingSessionId(CastingSessionRoleParameterIncludeInQueryEnum.Nothing, castingSessionRole.CastingSessionId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingSessionRoleUpAsync(desiredOrderIndex, castingSessionRole, castingSessionRoles);
            }
            else if (desiredOrderIndex > castingSessionRole.OrderIndex)
            {
                return await ReOrderCastingSessionRoleUpAsync(desiredOrderIndex, castingSessionRole, castingSessionRoles);
            }
            else if (desiredOrderIndex < castingSessionRole.OrderIndex)
            {
                return await ReOrderCastingSessionRoleDownAsync(desiredOrderIndex, castingSessionRole, castingSessionRoles);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingSessionRoleByCastingSessionAsync(CastingSessionRole castingSessionRole)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingSessionRoles
                                 where role.CastingSessionId == castingSessionRole.CastingSessionId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingSessionRoleDownAsync(int desiredOrderIndex, CastingSessionRole castingSessionRole,
            IEnumerable<CastingSessionRole> castingSessionRoles)
        {
            foreach (var role in castingSessionRoles)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingSessionRole.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingSessionRoleUpAsync(int desiredOrderIndex, CastingSessionRole castingSessionRole,
            IEnumerable<CastingSessionRole> castingSessionRoles)
        {
            foreach (var role in castingSessionRoles)
            {
                if (role.OrderIndex > castingSessionRole.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingSessionRole> GetCastingSessionRolesQueryable(CastingSessionRoleParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CastingSessionRole> castingSessionRoles = from castingSessionRole in _castingContext.CastingSessionRoles select castingSessionRole;
            if (includeInQuery == CastingSessionRoleParameterIncludeInQueryEnum.Personas)
            {
                castingSessionRoles
                    .Include("Personas.Persona");
            }
            if (includeInQuery == CastingSessionRoleParameterIncludeInQueryEnum.PersonasAndAssets)
            {
                castingSessionRoles
                    .Include("Personas.Persona")
                    .Include("Personas.Persona.VideoReferences.Video")
                    .Include("Personas.Persona.AudioReferences.Audio")
                    .Include("Personas.Persona.ResumeReferences.Resume")
                    .Include("Personas.Persona.ImageReferences.Image");
            }
            return castingSessionRoles;
        }


        #endregion
        #endregion

        #region Folders

        public async Task<CastingSessionFolder> GetCastingSessionFolderById(CastingSessionFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId)
        {
            IQueryable<CastingSessionFolder> castingSessionFolders = GetCastingSessionFoldersQueryable(includeInQuery);

            return await castingSessionFolders.FirstOrDefaultAsync(p => p.Id == castingSessionFolderId);


        }

        public async Task<int> AddNewCastingSessionFolderAsync(bool orderAtTheTop, CastingSessionFolder castingSessionFolder)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingSessionFolderByCastingSessionAsync(castingSessionFolder);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingSessionFolder.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingSessionFolder>().Add(castingSessionFolder);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingSessionFolderAsync(1, castingSessionFolder);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingSessionFolderAsync(CastingSessionFolder castingSessionFolder)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingSessionFolderAsync(0, castingSessionFolder);

                    _castingContext.Set<CastingSessionFolder>().Remove(castingSessionFolder);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingSessionFolderAsync(CastingSessionFolder castingSessionFolder)
        {
            SetEntityState(castingSessionFolder, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionId(CastingSessionFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionId)
        {
            var castingSessionFolders = GetCastingSessionFoldersQueryable(includeInQuery);

            return await castingSessionFolders.Where(d => d.CastingSessionId == castingSessionId).ToListAsync();
        }
        public async Task<int> ReOrderCastingSessionFolderAsync(int desiredOrderIndex, CastingSessionFolder castingSessionFolder)
        {
            IEnumerable<CastingSessionFolder> castingSessionFolders = await GetCastingSessionFoldersByCastingSessionId(CastingSessionFolderParameterIncludeInQueryEnum.Nothing, castingSessionFolder.CastingSessionId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingSessionFolderUpAsync(desiredOrderIndex, castingSessionFolder, castingSessionFolders);
            }
            else if (desiredOrderIndex > castingSessionFolder.OrderIndex)
            {
                return await ReOrderCastingSessionFolderUpAsync(desiredOrderIndex, castingSessionFolder, castingSessionFolders);
            }
            else if (desiredOrderIndex < castingSessionFolder.OrderIndex)
            {
                return await ReOrderCastingSessionFolderDownAsync(desiredOrderIndex, castingSessionFolder, castingSessionFolders);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingSessionFolderByCastingSessionAsync(CastingSessionFolder castingSessionFolder)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingSessionFolders
                                 where role.CastingSessionId == castingSessionFolder.CastingSessionId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingSessionFolderDownAsync(int desiredOrderIndex, CastingSessionFolder castingSessionFolder,
            IEnumerable<CastingSessionFolder> castingSessionFolders)
        {
            foreach (var role in castingSessionFolders)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingSessionFolder.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolder.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolder, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingSessionFolderUpAsync(int desiredOrderIndex, CastingSessionFolder castingSessionFolder,
            IEnumerable<CastingSessionFolder> castingSessionFolders)
        {
            foreach (var role in castingSessionFolders)
            {
                if (role.OrderIndex > castingSessionFolder.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolder.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolder, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingSessionFolder> GetCastingSessionFoldersQueryable(CastingSessionFolderParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CastingSessionFolder> castingSessionFolders = from castingSessionFolder in _castingContext.CastingSessionFolders select castingSessionFolder;
            if (includeInQuery == CastingSessionFolderParameterIncludeInQueryEnum.Roles)
            {
                castingSessionFolders
                    .Include(r=>r.Roles);
            }
            if (includeInQuery == CastingSessionFolderParameterIncludeInQueryEnum.RolesAndPersonas)
            {
                castingSessionFolders
                    .Include("Roles.Personas.Persona");
            }
            if (includeInQuery == CastingSessionFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets)
            {
                castingSessionFolders
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }
            return castingSessionFolders;
        }


        #endregion
        #endregion

        #region Roles

        public async Task<CastingSessionFolderRole> GetCastingSessionFolderRoleById(CastingSessionFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderRoleId)
        {
            IQueryable<CastingSessionFolderRole> castingSessionFolderRoles = GetCastingSessionFolderRolesQueryable(includeInQuery);

            return await castingSessionFolderRoles.FirstOrDefaultAsync(p => p.Id == castingSessionFolderRoleId);


        }

        public async Task<int> AddNewCastingSessionFolderRoleAsync(bool orderAtTheTop, CastingSessionFolderRole castingSessionFolderRole)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingSessionFolderRoleByCastingSessionFolderAsync(castingSessionFolderRole);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingSessionFolderRole.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingSessionFolderRole>().Add(castingSessionFolderRole);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingSessionFolderRoleAsync(1, castingSessionFolderRole);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingSessionFolderRoleAsync(0, castingSessionFolderRole);

                    _castingContext.Set<CastingSessionFolderRole>().Remove(castingSessionFolderRole);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole)
        {
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderId(CastingSessionFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId)
        {
            var castingSessionFolderRoles = GetCastingSessionFolderRolesQueryable(includeInQuery);

            return await castingSessionFolderRoles.Where(d => d.CastingSessionFolderId == castingSessionFolderId).ToListAsync();
        }
        public async Task<int> ReOrderCastingSessionFolderRoleAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole)
        {
            IEnumerable<CastingSessionFolderRole> castingSessionFolderRoles = await GetCastingSessionFolderRolesByCastingSessionFolderId(CastingSessionFolderRoleParameterIncludeInQueryEnum.Nothing, castingSessionFolderRole.CastingSessionFolderId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingSessionFolderRoleUpAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            else if (desiredOrderIndex > castingSessionFolderRole.OrderIndex)
            {
                return await ReOrderCastingSessionFolderRoleUpAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            else if (desiredOrderIndex < castingSessionFolderRole.OrderIndex)
            {
                return await ReOrderCastingSessionFolderRoleDownAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingSessionFolderRoleByCastingSessionFolderAsync(CastingSessionFolderRole castingSessionFolderRole)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingSessionFolderRoles
                                 where role.CastingSessionFolderId == castingSessionFolderRole.CastingSessionFolderId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingSessionFolderRoleDownAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole,
            IEnumerable<CastingSessionFolderRole> castingSessionFolderRoles)
        {
            foreach (var role in castingSessionFolderRoles)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingSessionFolderRole.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolderRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingSessionFolderRoleUpAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole,
            IEnumerable<CastingSessionFolderRole> castingSessionFolderRoles)
        {
            foreach (var role in castingSessionFolderRoles)
            {
                if (role.OrderIndex > castingSessionFolderRole.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolderRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingSessionFolderRole> GetCastingSessionFolderRolesQueryable(CastingSessionFolderRoleParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CastingSessionFolderRole> castingSessionFolderRoles = from castingSessionFolderRole in _castingContext.CastingSessionFolderRoles select castingSessionFolderRole;
            if (includeInQuery == CastingSessionFolderRoleParameterIncludeInQueryEnum.Personas)
            {
                castingSessionFolderRoles
                    .Include("Personas.Persona");
            }
            if (includeInQuery == CastingSessionFolderRoleParameterIncludeInQueryEnum.PersonasAndAssets)
            {
                castingSessionFolderRoles
                    .Include("Personas.Persona")
                    .Include("Personas.Persona.VideoReferences.Video")
                    .Include("Personas.Persona.AudioReferences.Audio")
                    .Include("Personas.Persona.ResumeReferences.Resume")
                    .Include("Personas.Persona.ImageReferences.Image");
            }
            return castingSessionFolderRoles;
        }


        #endregion
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
