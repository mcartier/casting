﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Enums;
using Casting.BLL.CastingCalls.Interfaces;

namespace Casting.BLL.CastingCalls
{
    public class CastingCallFolderService : BaseService, ICastingCallFolderService
    {
        private ICastingCallAsyncRepository _castingSessionRepository;

        public CastingCallFolderService(ICastingCallAsyncRepository castingSessionAsyncRepository)
        {
            _castingSessionRepository = castingSessionAsyncRepository;
        }

        public async Task<CastingCallFolder> GetCastingCallFolderByIdWithRolesAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingCallFolderByIdAsync(CastingCallFolderParameterIncludeInQueryEnum.Roles, castingSessionFolderId);
        }
        public async Task<CastingCallFolder> GetCastingCallFolderByIdWithRolesAndPersonasAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingCallFolderByIdAsync(CastingCallFolderParameterIncludeInQueryEnum.RolesAndPersonas, castingSessionFolderId);
        }
        public async Task<CastingCallFolder> GetCastingCallFolderByIdWithRolesPersonasAndAssetsAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingCallFolderByIdAsync(CastingCallFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets, castingSessionFolderId);
        }

        public async Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdWithRolesAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingCallFoldersByCastingCallIdAsync(CastingCallFolderParameterIncludeInQueryEnum.Roles,  castingSessionId);
        }

        public async Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdWithRolesAndPersonasAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingCallFoldersByCastingCallIdAsync(CastingCallFolderParameterIncludeInQueryEnum.RolesAndPersonas,  castingSessionId);
        }

        public async Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdWithRolesPersonasAndAssetsAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingCallFoldersByCastingCallIdAsync(CastingCallFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets,  castingSessionId);
        }

        public async Task<int> AddNewCastingCallFolderAsync(bool orderAtTheTop, CastingCallFolder castingSessionFolder)
        {
            return await _castingSessionRepository.AddNewCastingCallFolderAsync(orderAtTheTop, castingSessionFolder);
        }

        public async Task<int> UpdateCastingCallFolderAsync(CastingCallFolder castingSessionFolder)
        {
            return await _castingSessionRepository.UpdateCastingCallFolderAsync(castingSessionFolder);
        }
        public async Task<int> DeleteCastingCallFolder(CastingCallFolder castingSessionFolder)
        {
            return await _castingSessionRepository.DeleteCastingCallFolderAsync(castingSessionFolder);
        }
    }
}
