﻿export class MessageInterface {
  public title: string;
  public confirmationMessage: string;
  public yesText: string;
  public noText: string;
}
