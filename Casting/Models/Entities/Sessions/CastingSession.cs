﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.CastingCalls;

namespace Casting.Model.Entities.Sessions
{
    public class CastingSession:Logger
    {
        public CastingSession()
        {
            Roles = new List<CastingSessionRole>();
            Folders = new List<CastingSessionFolder>();
            CastingCalls = new List<CastingCallCastingSessionReference>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProductionId { get; set; }
        public Production Production { get; set; }
        public OrderByValuesEnum SessionRolesSortOrderI { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int RolesCount { get; set; }
        public ICollection<CastingSessionRole> Roles { get; set; }
        [NotMapped]
        public int FoldersCount { get; set; }
        public ICollection<CastingSessionFolder> Folders { get; set; }
        public ICollection<CastingCallCastingSessionReference> CastingCalls { get; set; }
        public OrderByValuesEnum CastingSessionFoldersSortOrderI { get; set; }
        public OrderByValuesEnum CastingSessionRolesSortOrderI { get; set; }
        public int OrderIndex { get; set; }


    }
}
