﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Attributes;
using Casting.Model.Enums;

namespace Casting.Model.Entities.AttributeReferences
{
    public abstract class AccentReference : Logger
    {
        public int Id { get; set; }
       
        public int AccentId { get; set; }
        public Accent Accent { get; set; }

        public AttributeReferenceTypeEnum AttributeReferenceTypeEnum { get; set; }

        public int OrderIndex { get; set; }
    }
}