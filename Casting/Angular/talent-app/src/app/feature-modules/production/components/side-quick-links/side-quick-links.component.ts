import { Component, OnInit, HostListener } from '@angular/core';
import { ProductionComponentService } from '../../services/production-component.service';
@Component({
  selector: 'app-side-quick-links',
  templateUrl: './side-quick-links.component.html',
  styleUrls: ['./side-quick-links.component.scss']
})
export class SideQuickLinksComponent implements OnInit {

    constructor(private productionComponentService: ProductionComponentService) { }
    public fixed: boolean = false; 

    @HostListener("window:scroll", ['$event'])
    onWindowScroll($event: any) {
     /* let top = window.scrollY;
      if (top > 50) {
        this.fixed = true;
        console.log(top);
      } else if (this.fixed && top < 5) {
        this.fixed = false;
        console.log(top);
      }*/
    // $event.stopPropagation();
    }
  ngOnInit() {
  }

 
  private scrollToProduction() {
    this.productionComponentService.scrollToProduction();
  }
  private scrollToArtistic() {
    this.productionComponentService.scrollToArtistic();
  }
  private scrollToCasting() {
    this.productionComponentService.scrollToCasting();
  }
  private scrollToRoles() {
    this.productionComponentService.scrollToRoles();
  }
  private scrollToBasic() {
    this.productionComponentService.scrollToBasic();
  }
  private scrollToOther() {
    this.productionComponentService.scrollToOther();
  }
  private saveProductionClick() {
    this.productionComponentService.trySaveProduction();
  }
  private deleteProductionClick() {
    this.productionComponentService.tryDeleteProduction();
  }
  private cancelEditProductionClick() {
    this.productionComponentService.tryCancelEditProduction();
  }
}
