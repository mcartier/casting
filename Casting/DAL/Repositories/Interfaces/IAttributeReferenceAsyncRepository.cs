﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using Casting.Model.Entities.AttributeReferences;

using Casting.Model.Enums;
using System.Data.Entity;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IAttributeReferenceAsyncRepository : IAsyncRepository, IDisposable
    {
        #region references

        #region language
        Task<int> DeleteLanguageReferenceAsync(int parentId, LanguageReference attributeReference);

        Task<int> UpdateLanguageReferenceAsync(int parentId,
            LanguageReference attributeReference);

        Task<int> ReOrderLanguageAttributeAsync(int parentId,
            int desiredOrderIndex, LanguageReference attributeReference);

        Task<int> AddNewLanguageReferenceAsync(bool orderAtTheTop, int parentId,
            LanguageReference attributeReference);

        Task<int> AddNewLanguageReferenceAsync(bool orderAtTheTop,
            LanguageReference attributeReference);

        Task<LanguageReference> GetLanguageReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileLanguageReference> GetProfileLanguageReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleLanguageReference> GetRoleLanguageReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<LanguageReference> GetLanguageReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);

        #endregion
        #region accent
        
        Task<int> DeleteAccentReferenceAsync(int parentId, AccentReference attributeReference);

        Task<int> UpdateAccentReferenceAsync(int parentId,
            AccentReference attributeReference);

        Task<int> ReOrderAccentAttributeAsync(int parentId,
            int desiredOrderIndex, AccentReference attributeReference);

        Task<int> AddNewAccentReferenceAsync(bool orderAtTheTop, int parentId,
            AccentReference attributeReference);

        Task<int> AddNewAccentReferenceAsync(bool orderAtTheTop,
            AccentReference attributeReference);

        Task<AccentReference> GetAccentReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileAccentReference> GetProfileAccentReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleAccentReference> GetRoleAccentReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<AccentReference> GetAccentReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);

        #endregion
        #region union

        Task<int> DeleteUnionReferenceAsync(int parentId, UnionReference attributeReference);

        Task<int> UpdateUnionReferenceAsync(int parentId,
            UnionReference attributeReference);

        Task<int> ReOrderUnionAttributeAsync(int parentId,
            int desiredOrderIndex, UnionReference attributeReference);

        Task<int> AddNewUnionReferenceAsync(bool orderAtTheTop, int parentId,
            UnionReference attributeReference);

        Task<int> AddNewUnionReferenceAsync(bool orderAtTheTop,
            UnionReference attributeReference);

        Task<UnionReference> GetUnionReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileUnionReference> GetProfileUnionReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<CastingCallUnionReference> GetCastingCallUnionReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<UnionReference> GetUnionReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);


        #endregion
        #region ethnicity

        Task<int> DeleteEthnicityReferenceAsync(int parentId, EthnicityReference attributeReference);

        Task<int> UpdateEthnicityReferenceAsync(int parentId,
            EthnicityReference attributeReference);

        Task<int> ReOrderEthnicityAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicityReference attributeReference);

        Task<int> AddNewEthnicityReferenceAsync(bool orderAtTheTop, int parentId,
            EthnicityReference attributeReference);

        Task<int> AddNewEthnicityReferenceAsync(bool orderAtTheTop,
            EthnicityReference attributeReference);

        Task<EthnicityReference> GetEthnicityReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileEthnicityReference> GetProfileEthnicityReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleEthnicityReference> GetRoleEthnicityReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<EthnicityReference> GetEthnicityReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);


        #endregion
        #region ethnicstereotype

        Task<int> DeleteEthnicStereoTypeReferenceAsync(int parentId, EthnicStereoTypeReference attributeReference);

        Task<int> UpdateEthnicStereoTypeReferenceAsync(int parentId,
            EthnicStereoTypeReference attributeReference);

        Task<int> ReOrderEthnicStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicStereoTypeReference attributeReference);

        Task<int> AddNewEthnicStereoTypeReferenceAsync(bool orderAtTheTop, int parentId,
            EthnicStereoTypeReference attributeReference);

        Task<int> AddNewEthnicStereoTypeReferenceAsync(bool orderAtTheTop,
            EthnicStereoTypeReference attributeReference);

        Task<EthnicStereoTypeReference> GetEthnicStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileEthnicStereoTypeReference> GetProfileEthnicStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleEthnicStereoTypeReference> GetRoleEthnicStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<EthnicStereoTypeReference> GetEthnicStereoTypeReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);


        #endregion
        #region stereotype

        Task<int> DeleteStereoTypeReferenceAsync(int parentId, StereoTypeReference attributeReference);

        Task<int> UpdateStereoTypeReferenceAsync(int parentId,
            StereoTypeReference attributeReference);

        Task<int> ReOrderStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, StereoTypeReference attributeReference);

        Task<int> AddNewStereoTypeReferenceAsync(bool orderAtTheTop, int parentId,
            StereoTypeReference attributeReference);

        Task<int> AddNewStereoTypeReferenceAsync(bool orderAtTheTop,
            StereoTypeReference attributeReference);

        Task<StereoTypeReference> GetStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileStereoTypeReference> GetProfileStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleStereoTypeReference> GetRoleStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<StereoTypeReference> GetStereoTypeReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);


        #endregion
        #region bodytype

        Task<int> DeleteBodyTypeReferenceAsync(int parentId, BodyTypeReference attributeReference);

        Task<int> UpdateBodyTypeReferenceAsync(int parentId,
            BodyTypeReference attributeReference);

        Task<int> ReOrderBodyTypeAttributeAsync(int parentId,
            int desiredOrderIndex, BodyTypeReference attributeReference);

        Task<int> AddNewBodyTypeReferenceAsync(bool orderAtTheTop, int parentId,
            BodyTypeReference attributeReference);

        Task<int> AddNewBodyTypeReferenceAsync(bool orderAtTheTop,
            BodyTypeReference attributeReference);

        Task<BodyTypeReference> GetBodyTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        //Task<ProfileBodyTypeReference> GetProfileBodyTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleBodyTypeReference> GetRoleBodyTypeReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<BodyTypeReference> GetBodyTypeReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);


        #endregion

        #region haircolor

        
        Task<int> DeleteHairColorReferenceAsync(int parentId, HairColorReference attributeReference);

        Task<int> UpdateHairColorReferenceAsync(int parentId,
            HairColorReference attributeReference);

        Task<int> ReOrderHairColorAttributeAsync(int parentId,
            int desiredOrderIndex, HairColorReference attributeReference);

        Task<int> AddNewHairColorReferenceAsync(bool orderAtTheTop, int parentId,
            HairColorReference attributeReference);

        Task<int> AddNewHairColorReferenceAsync(bool orderAtTheTop,
            HairColorReference attributeReference);

        Task<HairColorReference> GetHairColorReferenceByIdAsync(bool withAttribute, int referenceId);
        //  Task<ProfileHairColorReference> GetProfileHairColorReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleHairColorReference> GetRoleHairColorReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<HairColorReference> GetHairColorReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);

        #endregion

        #region eyecolor

        Task<int> DeleteEyeColorReferenceAsync(int parentId, EyeColorReference attributeReference);

        Task<int> UpdateEyeColorReferenceAsync(int parentId,
            EyeColorReference attributeReference);

        Task<int> ReOrderEyeColorAttributeAsync(int parentId,
            int desiredOrderIndex, EyeColorReference attributeReference);

        Task<int> AddNewEyeColorReferenceAsync(bool orderAtTheTop, int parentId,
            EyeColorReference attributeReference);

        Task<int> AddNewEyeColorReferenceAsync(bool orderAtTheTop,
            EyeColorReference attributeReference);

        Task<EyeColorReference> GetEyeColorReferenceByIdAsync(bool withAttribute, int referenceId);
        // Task<ProfileEyeColorReference> GetProfileEyeColorReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleEyeColorReference> GetRoleEyeColorReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<EyeColorReference> GetEyeColorReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);

        #endregion

        #region skill
        Task<int> DeleteSkillReferenceAsync(int parentId, SkillReference attributeReference);

        Task<int> UpdateSkillReferenceAsync(int parentId,
            SkillReference attributeReference);

        Task<int> ReOrderSkillAttributeAsync(int parentId,
            int desiredOrderIndex, SkillReference attributeReference);

        Task<int> AddNewSkillReferenceAsync(bool orderAtTheTop, int parentId,
            SkillReference attributeReference);

        Task<int> AddNewSkillReferenceAsync(bool orderAtTheTop,
            SkillReference attributeReference);

        Task<SkillReference> GetSkillReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<ProfileSkillReference> GetProfileSkillReferenceByIdAsync(bool withAttribute, int referenceId);
        Task<RoleSkillReference> GetRoleSkillReferenceByIdAsync(bool withAttribute, int referenceId);
        IQueryable<SkillReference> GetSkillReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);

        #endregion

        #endregion
              Task<int> GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum attributeType, int attributeId);

        Task<IEnumerable<BodyTypeReference>> GetBodyTypeReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<EyeColorReference>> GetEyeColorReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<HairColorReference>> GetHairColorReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<LanguageReference>> GetLanguageReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<AccentReference>> GetAccentReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<UnionReference>> GetUnionReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<EthnicityReference>> GetEthnicityReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<EthnicStereoTypeReference>> GetEthnicStereoTypeReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<StereoTypeReference>> GetStereoTypeReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);
        Task<IEnumerable<SkillReference>> GetSkillReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId);




      



       
      

    }
}
