﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Locations;

namespace Casting.BLL.Location.Interfaces
{
    public interface ILocationsService
    {
        Task<City> GetCityByIdAsync(int cityId);
        Task<IEnumerable<City>> GetCitiesByRegionIdAsync(int regionId);
        Task<IEnumerable<City>> GetCitiesAsync();
        Task<IEnumerable<City>> GetCitiesByCountryIdAsync(int countryId);
        Task<Region> GetRegionByIdAsync(int regionId);
        Task<IEnumerable<Region>> GetRegionsByCountryIdAsync(int countryId);
        Task<Country> GetCountrieByIdAsync(int countryId);
        Task<IEnumerable<Country>> GetCountriesAsync();
    }
}