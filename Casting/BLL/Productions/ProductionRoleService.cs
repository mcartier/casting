﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class ProductionRoleService : BaseService, IProductionRoleService
    {
        private IProductionAsyncRepository _productionRepository;

        public ProductionRoleService(IProductionAsyncRepository productionAsyncRepository)
        {
            _productionRepository = productionAsyncRepository;
        }


        public async Task<Role> GetRoleByIdAsync(int roleId)
        {
            return await _productionRepository.GetRoleByIdAsync(roleId, false);
        }
        public async Task<Role> GetCompleteRoleByIdAsync(int roleId)
        {
            return await _productionRepository.GetRoleByIdAsync(roleId,true);
        }
        public async Task<IEnumerable<Role>> GetRolesByProductionIdAsync(int productionId)
        {
            return await _productionRepository.GetRolesByProductionIdAsync(productionId,false);
        }

        public async Task<IEnumerable<Role>> GetCompleteRolesByProductionIdAsync(int productionId)
        {
            return await _productionRepository.GetRolesByProductionIdAsync(productionId,true);
        }

        public async Task<int> AddNewRoleAsync(Role role)
        {
            return await _productionRepository.AddNewRoleAsync(role);
        }

        public async Task<int> UpdateRoleAsync(Role role)
        {
            return await _productionRepository.UpdateRoleAsync(role);
        }

        public async Task<int> DeleteRoleAsync(Role role)
        {
            return await _productionRepository.DeleteRoleAsync(role);
        }
    }
}
