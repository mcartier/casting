﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Infrastructure;
using System.Runtime.InteropServices.WindowsRuntime;

using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.Profiles;
using Casting.Model.Enums;
using SSW.Data.Entities;
using System.Reflection;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Guests;


namespace Casting.DAL.Repositories
{
    public class GuestAsyncRepository : AsyncRepository, IGuestAsyncRepository
    {
        ICastingContext _castingContext;
        public GuestAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Guests

        public async Task<Guest> GetGuestByIdAsync(int guestId)
        {
            return await GetGuestUsersQueryable()
                .FirstOrDefaultAsync(t => t.Id == guestId);

        }

        public async Task<IEnumerable<Guest>> GetGuestsAsync()
        {
            return await GetGuestUsersQueryable().ToListAsync();

        }

        public async Task<int> AddNewGuestAsync(Guest guest)
        {
            _castingContext.Guests.Add(guest);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteGuestAsync(Guest guest)
        {
            _castingContext.Guests.Remove(guest);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateGuestAsync(Guest guest)
        {
            SetEntityState(guest, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion




        #region PrivateMethods


        private IQueryable<Guest> GetGuestUsersQueryable()
        {
            var users = from user in _castingContext.Guests
                        select user;

           
            return users;
        }


        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }
                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public IEnumerable<Guest> GetComplete()
        {
            throw new NotImplementedException();
        }


        #endregion

    }
}
