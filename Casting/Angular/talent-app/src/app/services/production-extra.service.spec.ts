import { TestBed } from '@angular/core/testing';

import { ProductionExtraService } from './production-extra.service';

describe('ProductionExtraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionExtraService = TestBed.get(ProductionExtraService);
    expect(service).toBeTruthy();
  });
});
