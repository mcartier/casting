﻿using Casting.BLL.CastingSessions;
using Casting.BLL.CastingSessions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Sessions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingSessions;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using Casting.DAL;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingSessionfolders")]
   // [Authorize]
    public class SessionFoldersController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingSessionFolderService _castingSessionFolderService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public SessionFoldersController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, ICastingSessionFolderService castingSessionFolderService, IProductionService productionService, ICastingContext castingContext) : base(castingContext)
        {
            _productionService = productionService;
            _castingSessionFolderService = castingSessionFolderService;
            _metadataService = metadataService;
            _userManager = userManager;
        }


        // GET: api/CastingSessionFolders
        [ResponseType(typeof(IEnumerable<CastingSessionFolder>))]
        public async Task<IHttpActionResult> GetCastingSessionFolders(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionFolders =
                await _castingSessionFolderService.GetCastingSessionFoldersByCastingSessionIdWithRolesPersonasAndAssetsAsync(0);

            return Ok(castingSessionFolders);
        }

        // GET: api/CastingSessionFolders
        [System.Web.Http.ActionName("castingSession")]
        [Route("castingSession/{castingSessionId}")]
        [ResponseType(typeof(IEnumerable<CastingSessionFolder>))]
        public async Task<IHttpActionResult> GetCastingSessionFoldersForCastingSession(int castingSessionId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionFolders =
                await _castingSessionFolderService.GetCastingSessionFoldersByCastingSessionIdWithRolesAndPersonasAsync(castingSessionId);

            return Ok(castingSessionFolders);
        }

        // GET: api/CastingSessionFolders/5
        [ResponseType(typeof(CastingSessionFolder))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var castingSessionFolder =
                await _castingSessionFolderService.GetCastingSessionFolderByIdWithRolesPersonasAndAssetsAsync(id);

            return Ok(castingSessionFolder);
        }

        // POST: api/CastingSessionFolders
        [ResponseType(typeof(CastingSessionFolder))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewCastingSessionFolderBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
             /* var castingSessionFolder = new CastingSessionFolder()
            {
                DirectorId = user.BaseUserId,
                Name = name
            };*/
            var castingSessionFolder = new CastingSessionFolder()
            {
                CastingSessionId = value.CastingSessionId,
                Name = value.Name,
               

            };
            var ret = await _castingSessionFolderService.AddNewCastingSessionFolderAsync(true,castingSessionFolder);
            return CreatedAtRoute("DefaultApi", new { id = castingSessionFolder.Id }, castingSessionFolder);
        }


        // PUT: api/CastingSessionFolders/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateCastingSessionFolderBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSessionFolder = await _castingSessionFolderService.GetCastingSessionFolderByIdWithRolesAsync(id);
            if (castingSessionFolder == null)
            {
                return NotFound();
            }
            /*if (castingSessionFolder.CastingSession.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/

            castingSessionFolder.CastingSessionId = value.CastingSessionId;
            castingSessionFolder.Name = value.Name;
          

           

            try
            {
                await _castingSessionFolderService.UpdateCastingSessionFolderAsync(castingSessionFolder);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/CastingSessionFolders/5
        [ResponseType(typeof(CastingSessionFolder))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSessionFolder = await _castingSessionFolderService.GetCastingSessionFolderByIdWithRolesAsync(id);
            if (castingSessionFolder == null)
            {
                return NotFound();
            }
            /*if (castingSessionFolder.CastingSession.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/

            await _castingSessionFolderService.DeleteCastingSessionFolder(castingSessionFolder);

            return Ok(castingSessionFolder);
        }
    }
}
