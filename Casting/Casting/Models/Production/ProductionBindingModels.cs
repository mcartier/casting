﻿using Casting.Model.Entities.Metadata;
using Casting.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Casting.Web.Models.Production
{
    public class NewProductionBindingModel
    {
        [Required]
        public int ProductionTypeId { get; set; }
        public string ProductionCompany { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string ProductionInfo { get; set; }
        public string ExecutiveProducer { get; set; }
        public string CoExecutiveProducer { get; set; }
        public string Producer { get; set; }
        public string CoProducer { get; set; }
        public string CastingDirector { get; set; }
        public string CastingAssistant { get; set; }
        public string CastingAssociate { get; set; }
        public string AssistantCastingAssociate { get; set; }
        public string ArtisticDirector { get; set; }
        public string MusicDirector { get; set; }
        public string Choreographer { get; set; }
        public string TVNetwork { get; set; }
        public string Venue { get; set; }
        public CompensationTypeEnum CompensationType { get; set; }
        public string CompensationContractDetails { get; set; }
        public ProductionUnionStatusEnum ProductionUnionStatus { get; set; }
      
       // public ICollection<NewRoleBindingModel> Roles { get; set; }



    }

    public class UpdateProductionBindingModel
    {
        [Required]
        public int ProductionTypeId { get; set; }
         public string ProductionCompany { get; set; }
         [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string ProductionInfo { get; set; }
        public string ExecutiveProducer { get; set; }
        public string CoExecutiveProducer { get; set; }
        public string Producer { get; set; }
        public string CoProducer { get; set; }
        public string CastingDirector { get; set; }
        public string CastingAssistant { get; set; }
        public string CastingAssociate { get; set; }
        public string AssistantCastingAssociate { get; set; }
        public string ArtisticDirector { get; set; }
        public string MusicDirector { get; set; }
        public string Choreographer { get; set; }
        public string TVNetwork { get; set; }
        public string Venue { get; set; }
        public CompensationTypeEnum CompensationType { get; set; }
        public string CompensationContractDetails { get; set; }
        public ProductionUnionStatusEnum ProductionUnionStatus { get; set; }

    }

    public class NewRoleBindingModel
    {
        
        [Required]
        public int ProductionId { get; set; }
        
        [Required]
        public string Description { get; set; }
        [Required]
        public string Name { get; set; }

        public long MaxAge { get; set; }

        public long MinAge { get; set; }
        public bool RequiresNudity { get; set; }
          public bool Transgender { get; set; }
        public int PayDetailId { get; set; }
        [Required]
        public int RoleTypeId { get; set; }
        [Required]
        public int RoleGenderTypeId { get; set; }
        public ICollection<int> EyeColors { get; set; }
        public ICollection<int> Skills { get; set; }
        public ICollection<int> HairColors { get; set; }
        public ICollection<int> BodyTypes { get; set; }
        public ICollection<int> Languages { get; set; }
        public ICollection<int> Accents { get; set; }
        public ICollection<int> Ethnicities { get; set; }
        public ICollection<int> StereoTypes { get; set; }
        public ICollection<int> EthnicStereoTypes { get; set; }
    }

    public class UpdateRoleBindingModel
    {

       
        [Required]
        public string Description { get; set; }

        [Required]
        public string Name { get; set; }

        public long MaxAge { get; set; }

        public long MinAge { get; set; }
        public bool RequiresNudity { get; set; }

        public bool Transgender { get; set; }

       [Required]
        public int RoleTypeId { get; set; }
      
        [Required]
        public int RoleGenderTypeId { get; set; }
        public int PayDetailId { get; set; }
        public ICollection<int> Skills { get; set; }
        public ICollection<int> EyeColors { get; set; }
        public ICollection<int> HairColors { get; set; }
        public ICollection<int> BodyTypes { get; set; }
        public ICollection<int> Languages { get; set; }
        public ICollection<int> Accents { get; set; }
        public ICollection<int> Ethnicities { get; set; }
        public ICollection<int> StereoTypes { get; set; }
        public ICollection<int> EthnicStereoTypes { get; set; }
    }

}