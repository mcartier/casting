import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { UIRouterModule } from "@uirouter/angular";
import { Transition } from "@uirouter/angular";
import { UIRouter } from '@uirouter/angular';
import { Visualizer } from '@uirouter/visualizer';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { GlobalModule } from '../../global/global.module';
import { ErrorInterceptor } from '../../interceptors/errorInterceptor';
import { JwtInterceptor } from '../../interceptors/jwtInterceptor';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CAMPAIGN_STATES } from './casting-call.states';

import { CastingCallComponentService } from './services/casting-call-component.service';

import { NewCastingCallComponent } from './components/new-casting-call/new-casting-call.component';

@NgModule({
  declarations: [NewCastingCallComponent],
  imports: [
    CommonModule,
      UIRouterModule.forChild({ states: CAMPAIGN_STATES }),
    GlobalModule,
    FormsModule,
    ReactiveFormsModule,

  ],
  exports: [
    // DropFileDirective,
  ],
  entryComponents: [
  ],

  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    
    CastingCallComponentService
  ],
})
export class CastingCallModule { }
