﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{

    public interface ISideService
    {
        Task<Side> GetSideByIdAsync(int sideId);
        Task<IEnumerable<Side>> GetSidesByProductionIdAsync(int productionId);
        Task<int> AddNewSideAsync(Side side);
        Task<int> UpdateSideAsync(Side side);
        Task<int> DeleteSideAsync(Side side);

       
    }
}