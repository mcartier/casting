﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.Personas;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets.References
{
    public class BasketImageReference: ImageReference
    {
        public BasketImageReference()
        {
            base.AssetReferenceTypeEnum = AssetReferenceTypeEnum.Basket;
        }
        public int BasketId { get; set; }
        public Basket Basket { get; set; }

    }
}
