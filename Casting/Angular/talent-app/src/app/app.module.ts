import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NgModuleFactoryLoader, SystemJsNgModuleLoader  } from '@angular/core';
import { GlobalModule } from './global/global.module';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { APP_STATES } from './ui-router-states';
import { routerConfigFn } from './ui-router/router.config';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
//import { ActivatedRoute } from '@angular/router';
import { UIRouterModule, UIView } from "@uirouter/angular";
//import { Transition } from "@uirouter/angular"
import { UIRouter } from '@uirouter/angular';
import { WINDOW_PROVIDERS } from './services/browser.window.provider';
//import { WINDOW_PROVIDERS } from "./services/browser.window";
import { Visualizer } from '@uirouter/visualizer';
//import { StickyNavModule } from 'ng2-sticky-nav';
import { StickyModule } from 'ng2-sticky-kit';
import { APP_INITIALIZER } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { ErrorInterceptor } from './interceptors/errorInterceptor';
import { JwtInterceptor } from './interceptors/jwtInterceptor';
import { AppComponent } from './components/app.component';
import { HomeComponent } from './components/home/home.component';
import { RegisterComponent } from './components/register/register.component';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
//import { SidesDialogComponent } from './components/sides-dialog/sides-dialog.component';

import { EmailAvailabilityValidatorDirective } from './directives/email-availability-validator.directive';
import { UserService } from "./services/user.service";
//import { UppyModule } from './uppy/uppy.module';
//import { VideoRecorderModule } from './video-recorder/video-recorder.module';
import { VideoBlobService } from './services/video-blob.service';
import { MetadataService } from './services/metadata.service';
import { FileUploadService } from './services/file-upload.service';
import { ConfirmationDialogService } from './services/confirmation-dialog.service';
import { ProductionService } from './services/production.service';
import { ProductionExtraService } from './services/production-extra.service';
import { ProductionRoleService } from './services/production-role.service';
import { CastingCallService } from './services/casting-call.service';
import { StateTransitionHandlerService } from './services/state-transition-handler.service';
import { CastingCallRoleService } from './services/casting-call-role.service';
import { StateDependencyResolverService } from './services/state-dependency-resolver.service';
import { MessageDispatchService } from './services/message-dispatch.service';
import { ConfirmDecisionSubscriptionService } from './services/confirm-decision-subscription.service';

//import { RecordingManagerModule } from './feature-modules/recording/recordingManager.module';
//import { RecordingsComponent } from './feature-modules/recording/components/recordings/recordings.component';
//import { RecordingItemComponent } from './feature-modules/recording/components/recording-item/recording-item.component';
//import { MatFileUploadQueueComponent } from './components/mat-file-upload-queue/mat-file-upload-queue.component';
//import { MatFileUploadComponent } from './components/mat-file-upload/mat-file-upload.component';
//import { FileUploadInputFor } from './directives/file-upload-input-for.directive';
//import { BytesPipePipe } from './pipes/bytes-pipe.pipe';
//import { VideoRecorderComponent } from './components/video-recorder/video-recorder.component';
import { RecordRTCComponent } from './components/record-rtc/record-rtc.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { TestmeComponent } from './testme/testme.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { MainPulloutNonScrollComponent } from './components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component';
import { MainNonScrollComponent } from './components/containers/main-non-scroll/main-non-scroll.component';
import { MainPulloutScrollComponent } from './components/containers/main-pullout-scroll/main-pullout-scroll.component';
import { MainScrollComponent } from './components/containers/main-scroll/main-scroll.component';
import { MainSidePulloutScrollComponent } from './components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component';
import { MainSidePulloutNonScrollComponent } from './components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component';
import { PulloutComponent } from './components/containers/pullout/pullout.component';
import { NoPulloutComponent } from './components/containers/no-pullout/no-pullout.component';
//import { DropFileDirective } from './directives/drop-file.directive';


@NgModule({
  declarations: [
    AppComponent,
     HomeComponent,
      EmailAvailabilityValidatorDirective,
    RecordRTCComponent,
    ToolbarComponent,
    TestmeComponent,
     ConfirmationDialogComponent,
     MainPulloutNonScrollComponent,
    MainNonScrollComponent,
    MainPulloutScrollComponent,
    MainScrollComponent,
    MainSidePulloutScrollComponent,
    MainSidePulloutNonScrollComponent,
    PulloutComponent,
    NoPulloutComponent,
    RegisterComponent,
    LoginComponent,
    LogoutComponent,
     // SidesDialogComponent
      // BytesPipePipe,
   // RecordingItemComponent,
 // RecordingsComponent,
// MatFileUploadQueueComponent,
    // MatFileUploadComponent,
     //FileUploadInputFor,
    // BytesPipePipe,
    // VideoRecorderComponent,
    // RecordRtcComponent
   //  DropFileDirective,

  ],
    imports: [

    BrowserModule,
    UIRouterModule.forRoot({
      states: APP_STATES,
      useHash: false,
      otherwise: { state: 'home' },
      config: routerConfigFn,
      }),
    HttpClientModule ,
        BrowserAnimationsModule,
    FormsModule,
      ReactiveFormsModule,
      GlobalModule, 
        //UppyModule,
      //  VideoRecorderModule,
       // StickyNavModule,
        StickyModule,
    LoadingBarHttpClientModule,
   // RecordingManagerModule
    ],
    entryComponents: [
        RecordRTCComponent,
        ConfirmationDialogComponent
      ],
  exports: [
    //BytesPipePipe
    // DropFileDirective,
    ],
  
    providers: [
      { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
      { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
      WINDOW_PROVIDERS,
        { provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader },
        VideoBlobService,
      MetadataService,
        FileUploadService,
        ConfirmationDialogService,
      ProductionService,
      ProductionRoleService,
      CastingCallService,
      CastingCallRoleService,
        StateDependencyResolverService,
        StateTransitionHandlerService,
       ProductionExtraService,
      MessageDispatchService,
      ConfirmDecisionSubscriptionService,
      ],
    bootstrap: [UIView]
})
export class AppModule { }
