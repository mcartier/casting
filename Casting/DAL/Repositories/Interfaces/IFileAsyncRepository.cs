﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IFileAsyncRepository
    {
        Task<File> GetFileByFileIdAsync(string fileId);
        Task<int> AddNewFileAsync(File file);
        Task<int> DeleteFileAsync(File file);
        void Dispose();
        IEnumerable<T> GetAll<T>() where T : class;
        Task<T> GetByIdAsync<T>(int id) where T : class;
        Task<int> AddAsync<T>(T entity) where T : class;
        Task<int> DeleteAsync<T>(T entity) where T : class;
        Task<int> UpdateAsync<T>(T entity) where T : class;
        void SetEntityState<T>(T entity, EntityState state) where T : class;
    }
}