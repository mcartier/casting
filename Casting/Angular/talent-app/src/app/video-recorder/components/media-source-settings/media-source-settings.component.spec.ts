import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaSourceSettingsComponent } from './media-source-settings.component';

describe('MediaSourceSettingsComponent', () => {
  let component: MediaSourceSettingsComponent;
  let fixture: ComponentFixture<MediaSourceSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MediaSourceSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaSourceSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
