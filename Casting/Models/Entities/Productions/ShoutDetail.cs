﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions
{
   public class ShootDetail : Logger
    {
        public ShootDetail()
        {
          

        }
    public int Id { get; set; }
      
        public int ProductionId { get; set; }
        public Production Production { get; set; }

        public string Name { get; set; }
        public string Location { get; set; }
        public string LocationDetail { get; set; }
        public string Details { get; set; }
        public string Instructions { get; set; }
        public bool DatesToBeDecidedLater { get; set; }
        public bool DatesAreTemptative { get; set; }
        public bool IsOneDayOnly { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DaysOfShooting { get; set; }

        
    }
}
