﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

namespace Casting.BLL.CastingCalls.Interfaces
{
    public interface ICastingCallFolderRoleService
    {
        Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdAsync(int castingSessionFolderRoleId);
        Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdWithPersonasAsync(int castingSessionFolderRoleId);
        Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdWithPersonasAndAssetsAsync(int castingSessionFolderRoleId);
        Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdAsync(int castingSessionFolderId);
        Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdWithPersonasAsync(int castingSessionFolderId);
        Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdWithPersonasAndAssetsAsync(int castingSessionFolderId);
        Task<int> AddNewCastingCallFolderRoleAsync(bool orderAtTheTop, CastingCallFolderRole castingSessionFolderRole);
        Task<int> UpdateCastingCallFolderRoleAsync(CastingCallFolderRole castingSessionFolderRole);
        Task<int> DeleteCastingCallFolderRole(CastingCallFolderRole castingSessionFolderRole);
    }
}