﻿import { UIRouter, Category } from '@uirouter/core';
import { Visualizer } from '@uirouter/visualizer';
import { Injector, Injectable } from "@angular/core";
import { requiresAuthenticationHook } from './requiresAuthenticationHook';

/** UIRouter Config  */

export function routerConfigFn(router: UIRouter, injector: Injector) {
    const transitionService = router.transitionService;
   requiresAuthenticationHook(transitionService);
 
   
   //googleAnalyticsHook(transitionService);
  //StateTree.create(router, document.getElementById('statetree'), { width: 200, height: 100 });
  //var pluginInstance = router.plugin(Visualizer);
  //router.trace.enable(Category.TRANSITION);
  //router.plugin(Visualizer);
}
