﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;

using Microsoft.AspNet.Identity;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.BLL.Profiles.Interfaces;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Enums;
using Casting.Web.Helpers;
using Casting.Web.Models.Talent;

namespace Casting.Web.Controllers.Agents
{
    public class ActorProfilesController : BaseAgentApiController
    {
        private IProfileService _profileService;
        private readonly UserManager<ApplicationUser> _userManager;
          public ActorProfilesController(UserManager<ApplicationUser> userManager, IProfileService profileService, ICastingContext castingContext) : base(castingContext)
        {
            _profileService = profileService;// castingContext;
            _userManager = userManager;
        }


        // GET: api/ActorProfiles
        [ResponseType(typeof(IEnumerable<ActorProfile>))]
        public async Task<IHttpActionResult> GetActorProfiles()
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profiles = await _profileService.GetCompleteActorProfilesByAgentIdAsync(user.BaseUserId);
            
            return Ok(profiles);
        }

        // GET: api/ActorProfiles/5
        [ResponseType(typeof(ActorProfile))]
        public async Task<IHttpActionResult> GetActorProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profile = await _profileService.GetCompleteActorProfileByIdAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

       
        // PUT: api/ActorProfiles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutActorProfile(int id, ActorProfileBindingModels profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentActorProfiles = await 
            var tempActorProfile = ((ActorProfile)await _profileService.GetProfileByIdAsync(id));
            if (tempActorProfile == null)
            {
                return NotFound();
            }

            tempActorProfile.AgeMaximum = profile.AgeMaximum;
            tempActorProfile.AgeMinimum = profile.AgeMinimum;
            tempActorProfile.BodyTypeId = profile.BodyType;
            tempActorProfile.EyeColorId = profile.EyeColor;
            tempActorProfile.HairColorId = profile.HairColor;
            //tempActorProfile.Gender = profile.Gender;
           // tempActorProfile.LocationId = profile.Location;
            tempActorProfile.Height = profile.Height;
            tempActorProfile.Weight = profile.Weight;
           
            try
            {
                await _profileService.UpdateProfileAsync(tempActorProfile);
            }
            catch (DbUpdateConcurrencyException)
            {
               
                    throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ActorProfiles
        [ResponseType(typeof(ActorProfile))]
        public async Task<IHttpActionResult> PostActorProfile(NewActorProfileBindingModels profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }

            var existingProfile = await _profileService.GetProfileByTalentIdAsync(profile.TalentId);
            if (existingProfile != null)
            {
                return BadRequest("profile already exists for this talent");
            }
            ActorProfile newActorProfile = new ActorProfile();
            newActorProfile.AgeMaximum = profile.AgeMaximum;
            newActorProfile.AgeMinimum = profile.AgeMinimum;
            newActorProfile.BodyTypeId = profile.BodyType;
            newActorProfile.EyeColorId = profile.EyeColor;
            newActorProfile.HairColorId = profile.HairColor;
            //newActorProfile.Gender = profile.Gender;
            // newActorProfile.LocationId = profile.Location;
            newActorProfile.Height = profile.Height;
            newActorProfile.Weight = profile.Weight;
            await _profileService.AddNewProfileAsync(newActorProfile);

            return CreatedAtRoute("DefaultApi", new { id = newActorProfile.Id }, newActorProfile);
        }

        // DELETE: api/ActorProfiles/5
        [ResponseType(typeof(ActorProfile))]
        public async Task<IHttpActionResult> DeleteActorProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentActorProfiles = await 
            var profile = ((ActorProfile) await _profileService.GetProfileByIdAsync(id));
            if (profile == null)
            {
                return NotFound();
            }
          
            await _profileService.DeleteProfile(profile);

            return Ok(profile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               
            }
            base.Dispose(disposing);
        }

      
    }
}