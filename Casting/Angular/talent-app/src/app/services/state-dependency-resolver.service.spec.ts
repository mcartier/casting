import { TestBed } from '@angular/core/testing';

import { StateDependencyResolverService } from './state-dependency-resolver.service';

describe('StateDependencyResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StateDependencyResolverService = TestBed.get(StateDependencyResolverService);
    expect(service).toBeTruthy();
  });
});
