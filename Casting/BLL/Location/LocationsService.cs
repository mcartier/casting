﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL;
using Casting.Model.Entities.Locations;
using Casting.BLL.Location.Interfaces;

namespace Casting.BLL.Location
{
    public class LocationsService : ILocationsService
    {
        private ICastingContext _castingContext;

        public LocationsService(ICastingContext castingContext)
        {
            _castingContext = castingContext;

        }

        public async Task<City> GetCityByIdAsync(int cityId)
        {
            return await _castingContext.Cities.Where(c => c.Id == cityId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<City>> GetCitiesByRegionIdAsync(int regionId)
        {
            return await _castingContext.Cities.Where(c => c.RegionId == regionId).ToListAsync();
        }
        public async Task<IEnumerable<City>> GetCitiesAsync()
        {
            return await _castingContext.Cities.ToListAsync();
        }
        public async Task<IEnumerable<City>> GetCitiesByCountryIdAsync(int countryId)
        {
            return await _castingContext.Cities.Where(c => c.CountryId == countryId).ToListAsync();
        }
        public async Task<Region> GetRegionByIdAsync(int regionId)
        {
            return await _castingContext.Regions.Where(c => c.Id == regionId).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Region>> GetRegionsByCountryIdAsync(int countryId)
        {
            return await _castingContext.Regions.Where(c => c.CountryId == countryId).ToListAsync();
        }
        public async Task<Country> GetCountrieByIdAsync(int countryId)
        {
            return await _castingContext.Countries.Where(c => c.Id == countryId).FirstOrDefaultAsync();
        }

           public async Task<IEnumerable<Country>> GetCountriesAsync()
        {
            return await _castingContext.Countries.ToListAsync();
        }
      
    }
}
