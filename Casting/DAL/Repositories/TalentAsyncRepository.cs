﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Infrastructure;
using System.Runtime.InteropServices.WindowsRuntime;

using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.Profiles;
using Casting.Model.Enums;
using SSW.Data.Entities;
using System.Reflection;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Entities.Users.Guests;
using Casting.Model.Entities.Users.Talents;

namespace Casting.DAL.Repositories
{
    public class TalentAsyncRepository :AsyncRepository, ITalentAsyncRepository
    {
        ICastingContext _castingContext;
        public TalentAsyncRepository(ICastingContext castingContext):base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Talents

        public async Task<Talent> GetTalentByIdAsync(int talentId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(profileType, withProfile, withAssets, withAttributes, withAllAgentReferences, withAllAgents, withMainAgent)
                .FirstOrDefaultAsync(t => t.Id == talentId);

        }
        public async Task<Talent> GetTalentByIdAsync(int talentId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(withAllAgentReferences,withAllAgents, withMainAgent)
                .FirstOrDefaultAsync(t => t.Id == talentId);

        }

        public async Task<IEnumerable<Talent>> GetTalentsByAgentIdAsync(int agentId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(profileType, withProfile, withAssets, withAttributes, withAllAgentReferences,withAllAgents, withMainAgent)
                .Where(r => r.AgentReferences.Any(a => a.AgentId == agentId)).ToListAsync();

        }

        public async Task<IEnumerable<Talent>> GetTalentsByAgentIdAsync(int agentId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(withAllAgentReferences,withAllAgents, withMainAgent)
                .Where(r => r.AgentReferences.Any(a => a.AgentId == agentId)).ToListAsync();

        }

        public async Task<IEnumerable<Talent>> GetTalentsByMainAgentIdAsync(int agentId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(profileType, withProfile, withAssets, withAttributes, withAllAgentReferences, withAllAgents, withMainAgent)
                .Where(a => a.MainAgentId == agentId).ToListAsync();

        }

        public async Task<IEnumerable<Talent>> GetTalentsByMainAgentIdAsync(int agentId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(withAllAgentReferences, withAllAgents, withMainAgent)
                .Where(a => a.MainAgentId == agentId).ToListAsync();

        }
        public async Task<IEnumerable<Talent>> GetTalentsByDirectorIdAsync(int directorId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(profileType, withProfile, withAssets, withAttributes, withAllAgentReferences, withAllAgents, withMainAgent).OfType<StudioTalent>()
                //.Where(a => a.DirectorId == directorId)
                .ToListAsync();

        }

        public async Task<IEnumerable<Talent>> GetTalentsByDirectorIdAsync(int directorId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            return await GetTalentUsersQueryable(withAllAgentReferences, withAllAgents, withMainAgent).OfType<StudioTalent>()
               // .Where(a => a.DirectorId == directorId)
                .ToListAsync();

        }


        public async Task<int> AddNewTalentAsync(Talent talent)
        {
            _castingContext.Talents.Add(talent);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteTalentAsync(Talent talent)
        {
            _castingContext.Talents.Remove(talent);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateTalentAsync(Talent talent)
        {
           SetEntityState(talent, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion

        #region PrivateMethods

        private IQueryable<Talent> GetTalentUsersQueryable()
        {
            var users =  _castingContext.Talents;
            return from user in users select user; ;
        }
        private IQueryable<Talent> GetTalentUsersQueryable(bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            var users = GetTalentUsersQueryable();

            if (withAllAgentReferences)
            {
                users = users
                    .Include(r=>r.AgentReferences);

            }


            if (withAllAgents)
            {
                users = users
                    .Include("AgentReferences.Agent");

            }

            if (withMainAgent)
            {
                users = users
                    .Include(m => m.MainAgent);

            }

            return from user in users select user; ;
        }

        private IQueryable<Talent> GetTalentUsersQueryable(ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes,bool withAllAgentReferences, bool withAllAgents, bool withMainAgent)
        {
            var users = from user in GetTalentUsersQueryable(withAllAgentReferences, withAllAgents, withMainAgent)
                        select user;
            if (withProfile)
            {
              /*  switch (profileType)
                {
                    case ProfileTypeEnum.Actor:

                        users.Include(p => p.Profile).OfType<ActorProfile>();
                        break;
                    case ProfileTypeEnum.VoiceActor:
                        users.Include(p => p.Profile).OfType<VoiceActorProfile>();
                        break;
                }*/
            }

            if (withAssets)
            {
              /*  switch (profileType)
                {
                    case ProfileTypeEnum.Actor:

                        users.Include(p => p.Profile).OfType<ActorProfile>()
                            .Include("Profile.ImageReferences.Image")
                            .Include("Profile.ResumeReferences.Resume")
                            .Include("Profile.AudioReferences.Audio")
                            .Include("Profile.VideoReferences.Video");
                        break;
                    case ProfileTypeEnum.VoiceActor:
                        users.Include(p => p.Profile).OfType<VoiceActorProfile>()
                            .Include("Profile.ResumeReferences.Resume")
                            .Include("Profile.AudioReferences.Audio");
                        break;
                }*/
            }

            if (withAttributes)
            {
              /*  switch (profileType)
                {
                    case ProfileTypeEnum.Actor:
                        users.Include(p => p.Profile).OfType<ActorProfile>()
                      .Include("Profile.Ethnicities")
                      .Include("Profile.StereoTypes")
                      .Include("Profile.EthnicStereoTypes")
                      .Include("Profile.Languages")
                      .Include("Profile.Accents")
                      .Include("Profile.Unions");


                        break;
                    case ProfileTypeEnum.VoiceActor:
                        users.Include(p => p.Profile).OfType<VoiceActorProfile>()
                            .Include("Profile.Accents")
                            .Include("Profile.Unions");
                        break;
                }*/
            }

            return from user in users select user; ;
        }
        

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }
                disposedValue = true;
            }
        }

     

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public IEnumerable<Talent> GetComplete()
        {
            throw new NotImplementedException();
        }


        #endregion

    }
}
