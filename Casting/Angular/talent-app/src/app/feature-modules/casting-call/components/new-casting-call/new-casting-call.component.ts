import { Component, OnDestroy, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { MetadataService } from '../../../../services/metadata.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { CastingCall, CastingCallRole, NewCastingCall, NewCastingCallRole, UpdateCastingCall, UpdateCastingCallRole } from '../../../../models/casting-call.models';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
//import { RoleDialogService } from '../../services/role-dialog.service';
import { CastingCallService } from '../../../../services/casting-call.service';
import { CastingCallComponentService } from '../../services/casting-call-component.service';
import { CastingCallRoleService } from '../../../../services/casting-call-role.service';
//import { NewRoleComponent } from '../new-role/new-role.component'
import { fadeInAnimation } from '../../../../animations/router.animation';
import { Dropdown } from 'primeng/dropdown';
import { Calendar } from 'primeng/calendar';
import { RadioButton } from 'primeng/radiobutton';

@Component({
  selector: 'app-new-casting-call',
  templateUrl: './new-casting-call.component.html',
    styleUrls: ['./new-casting-call.component.scss'],
  // make fade in animation available to this component
  animations: [fadeInAnimation],

  // attach the fade in animation to the host (root) element of this component
  host: { '[@fadeInAnimation]': '' }
})
export class NewCastingCallComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor(private CastingCallRoleService: CastingCallRoleService,
        private _formBuilder: FormBuilder, private metadataService: MetadataService, private castingCallService: CastingCallService,
        public stateService: StateService,
        public transitionService: TransitionService,
        public transition: Transition,
        private castingCallComponentService: CastingCallComponentService) { }

    private viewTypes: number[];
    @Input() productionId: number;
    @Input() production: Production;
    @Input() private metadata: MetadataModel;
    @Input() castingCallId: number;
    @Input() castingCall: CastingCall;

    private namePlaceHolder: string;
    private meta: MetadataModel = new MetadataModel();
    private basicCastingCallInfo: FormGroup;

    private compensationType: CompensationTypeEnum = CompensationTypeEnum.notPaid;
    //private castingCall: CastingCall;
    private showHints: boolean = false;
    private isDraft: boolean;
    private isEdit: boolean;
    private popRole: boolean;
    private isLoading: boolean;
    private newRoleSubscription: Subscription;
    private deleteRoleSubscription: Subscription;
    private editRoleSubscription: Subscription;
    private saveCastingCallIntentSubscription: Subscription;
    private deleteCastingCallIntentSubscription: Subscription;
    private cancelEditCastingCallIntentSubscription: Subscription;
    @ViewChild('name', {static: false}) nameField: ElementRef;
    //@ViewChild('picker', {static: false}) pickerField: ElementRef;
    @ViewChild('expires', {static: false}) calendarInputField: Calendar;
    @ViewChild('castingCallTypeId', {static: false}) castingCallTypeIdField: Dropdown;
    @ViewChild('castingCallViewType', {static: false}) castingCallViewTypeField: RadioButton;
    private progressIndicatorId: any;
  
    ngOnInit() {
        this.deleteRoleSubscription = this.CastingCallRoleService.subscribeToDeleteRoleObs()
            .subscribe(
                role => {
                    if (role) {
                        this.castingCall.roles.splice(this.castingCall.roles.indexOf(role), 1);
                    }
                });
        /*this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(
                (role: CastingCallRole) => {
                    if (role) {
                        this.castingCall.roles[this.castingCall.roles.indexOf(role)] = role;
                    }
                });*/
        this.deleteCastingCallIntentSubscription = this.castingCallComponentService.subscribeToDeleteCastingCallIntentObs()
            .subscribe(
                value => {
                    if (value) {
                        this.moveToCastingCallsView();
                    }
                });

        this.cancelEditCastingCallIntentSubscription = this.castingCallComponentService.subscribeToCancelEditCastingCallIntentObs()
            .subscribe(
                value => {
                    if (value) {
                        this.moveToCastingCallsView();
                    }
                });

        this.saveCastingCallIntentSubscription = this.castingCallComponentService.subscribeToSaveCastingCallIntentObs()
            .subscribe(
                value => {
                    if (value) {
                        this.saveCastingCallClick();
                    }
                });

        if (this.transition.params().popRole) {
            this.popRole = this.transition.params().popRole;
        }
        if (this.transition.params().isDraft) {
            this.isDraft = this.transition.params().isDraft;
        }
        if (this.transition.params().isEdit) {
            this.isEdit = this.transition.params().isEdit;
        }
        if (this.transition.params().castingCall) {
            this.castingCall = this.transition.params().castingCall;
        }
        if (this.production) {
            this.namePlaceHolder = this.production.name;

        }
        if (this.castingCall) {
            this.basicCastingCallInfo = this._formBuilder.group({
                name: [this.castingCall.name, Validators.required],
                instructions: [this.castingCall.instructions],
                expires: [null, Validators.required],
                castingCallTypeId: [this.castingCall.castingCallType, Validators.required],
                castingCallViewType: [null, Validators.required],// RxwebValidators.oneOf({ matchValues: [1, 3, 7, 15] })],
                unions: [this.castingCall.unions.map(a => a.unionId)],
                roles: [this.castingCall.roles.map(a => a.roleId)]
                // castingCallCompany: [this.castingCall.castingCallCompany],
                // description: [this.castingCall.description, Validators.required],
                // compensationType: [this.castingCall.compensationType],
                //   castingCallUnionStatus: [this.castingCall.castingCallUnionStatus]
            }, { updateOn: 'blur' });


        } else {
            this.castingCall = new CastingCall();
            this.basicCastingCallInfo = this._formBuilder.group({
                name: ['', Validators.required],
                instructions: [''],
                expires: [null, Validators.required],
                castingCallTypeId: ['', Validators.required],
                castingCallViewType: [null, Validators.required],//RxwebValidators.oneOf({ matchValues: [1, 3, 7, 15] })],
                //castingCallViewType: [0, Validators.required],
                unions: [[]],
                roles: [[]],
                // castingCallCompany: [''],
                // description: ['', Validators.required],
                // compensationType: [''],
                //  castingCallUnionStatus: ['']
            }, { updateOn: 'blur' });


        }



    }
    ngOnDestroy() {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();

        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();

        }
        if (this.saveCastingCallIntentSubscription) {
            this.saveCastingCallIntentSubscription.unsubscribe();

        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();

        }

    }

    ngAfterViewInit() {
      setTimeout(() => this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' }));
        //    this.castingCallComponentService.registerRolesScrollElement(this.rolesCard);
        // this.castingCallComponentService.registerBasicScrollElement(this.basicCard);
        if (this.popRole) {
           /* setTimeout(() => {
                this.newRoleSubscription = this.roleDialogService.createRole(this.castingCall)
                    .subscribe(
                        role => {
                            if (role) {
                                this.castingCall.roles.push(role);
                            }
                            this.newRoleSubscription.unsubscribe();
                        });


            })*/
        }

    }

    private setDate(): void {
        // Set today date using the patchValue function
        let date = new Date();
        this.basicCastingCallInfo.patchValue({
            expires: {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate()
                }
            }
        });
    }

    private clearDate(): void {
        // Clear the date using the patchValue function
        this.basicCastingCallInfo.patchValue({ myDate: null });
    }

    private moveToCastingCallView(castingCallId: number) {
        this.stateService.go('castingCall', { castingCall: this.castingCall, castingCallId: castingCallId });

    }
    private moveToCastingCallsView() {
        this.stateService.go('castingCalls');

    }

    private scrollToRoles() {
        //    this.basicCastingCallInfo.reset();
        // this.rolesCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
        //this.castingCallComponentService.scrollToRoles();
    }
    private scrollToBasic() {
        //   this.basicCastingCallInfo.reset();
        //this.basicCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
        //this.castingCallComponentService.scrollToBasic();
    }
    private deleteCastingCallClick() {
        //   this.basicCastingCallInfo.reset();
        this.castingCallComponentService.tryDeleteCastingCall();
    }

    private cancelEditCastingCallClick() {
        //   this.basicCastingCallInfo.reset();
        this.moveToCastingCallsView();
    }
    private selectExpireDate(date: any) {
      this.basicCastingCallInfo.patchValue({
        expires: date,
      });
    }
    private changeUnionSelection(event: any) {
      this.basicCastingCallInfo.patchValue({
        unions: event.value,
      });
    }
    private changeRoleSelection(event: any) {
      this.basicCastingCallInfo.patchValue({
        roles: event.value,
      });
    }

    private saveCastingCallClick() {
          if (this.basicCastingCallInfo.valid) {
            //this.role = this.basicRoleInfo.value;
            let submissionObject = Object.assign(this.basicCastingCallInfo.value,
                {
                    productionId: this.production.id
                });
            if (this.castingCall.id && this.castingCall.id !== 0) {
                this.castingCallService.saveCastingCall(this.castingCall.id, this.production.id, submissionObject).subscribe((castingCall: CastingCall) => {
                    this.moveToCastingCallView(this.castingCall.id);
                    //this.castingCall = castingCall;
                },
                    (error) => {
                        console.log("error " + error)
                    })

            } else {
                // submissionObject.productionId = this.productionId;
                this.castingCallService.createCastingCall(submissionObject, this.production.id).subscribe((castingCall: CastingCall) => {
                    this.castingCall = castingCall;
                    this.moveToCastingCallView(this.castingCall.id);


                },
                    (error) => {
                        console.log("error " + error)
                    })

            }
        } else {
            if (!this.basicCastingCallInfo.get('name').valid) {
                this.basicCastingCallInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.nameField.nativeElement.focus();

            }
            else if (!this.basicCastingCallInfo.get('castingCallTypeId').valid) {
                this.basicCastingCallInfo.get('castingCallTypeId').markAsDirty();
                this.castingCallTypeIdField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.castingCallTypeIdField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicCastingCallInfo.get('castingCallViewType').valid) {
                this.basicCastingCallInfo.get('castingCallViewType').markAsDirty();
                this.castingCallViewTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.castingCallViewTypeField.inputViewChild.nativeElement.focus();
             }
            else if (!this.basicCastingCallInfo.get('expires').valid) {
                this.basicCastingCallInfo.get('expires').markAsDirty();
                this.calendarInputField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.calendarInputField.inputfieldViewChild.nativeElement.focus();
           }


        }

    }
    private addRoleClick() {
        // this.basicCastingCallInfo.reset();
        if (this.castingCall.id && this.castingCall.id !== 0) {
            //setTimeout(() => {
            /*this.newRoleSubscription = this.roleDialogService.createRole(this.castingCall)
                .subscribe(
                    role => {
                        if (role) {
                            this.castingCall.roles.push(role);
                        }
                        this.newRoleSubscription.unsubscribe();
                    });*/
            // });

        } else {

            let submissionObject = Object.assign(this.basicCastingCallInfo.value);
            this.castingCallService.createCastingCall(submissionObject, this.production.id).subscribe((castingCall: CastingCall) => {
                this.isLoading = false;
                this.stateService.go('draftcastingCall', { castingCall: castingCall, draftId: castingCall.id, popRole: true });
            },
                (error) => {
                })

        }
    }

}