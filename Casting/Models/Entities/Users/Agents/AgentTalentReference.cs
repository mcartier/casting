﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Talents;


namespace Casting.Model.Entities.Users.Agents
{
    public class AgentTalentReference
    {
        [Key]
        public int Id { get; set; }

        public int AgentId { get; set; }
        public Agent Agent { get; set; }
        public int TalentId { get; set; }
        public Talent Talent { get; set; }

    }
}
