import { Component, OnInit, ViewChild, AfterViewInit, Inject } from '@angular/core';
//let RecordRTC = require('recordrtc/RecordRTC.min');
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogRef } from '@angular/material';
@Component({
    selector: 'record-rtc',
    templateUrl: './record-rtc.component.html',
    styleUrls: ['./record-rtc.component.scss']
})
export class RecordRTCComponent implements AfterViewInit {

   
    constructor(public dialogRef: MatDialogRef<RecordRTCComponent>,
      @Inject(MAT_DIALOG_DATA) public data: string) {
        // Do stuff
    }
    onNoClick(): void {
      this.dialogRef.close();
    }
    saveMe() {
      //this.videoBlobService.addNewCreatedBlob(this.data);
      this.dialogRef.close();
    }
    ngAfterViewInit() {
        // set the initial state of the video
     
    }

}
