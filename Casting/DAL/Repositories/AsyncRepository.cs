﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;

namespace Casting.DAL.Repositories
{
    public abstract class AsyncRepository: Repository, IAsyncRepository
    {
        private ICastingContext _castingContext;
        protected AsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        public async Task<IEnumerable<T>> GetAllAsync<T>() where T : class
        {
            return await _castingContext.Set<T>().AsQueryable().ToListAsync();
        }

        public async Task<T> GetByIdAsync<T>(int id) where T : class
        {
            return await _castingContext.Set<T>().FindAsync(id);

        }

        public async Task<int> AddAsync<T>(T entity) where T : class
        {
            _castingContext.Set<T>().Add(entity);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> DeleteAsync<T>(T entity) where T : class
        {
            _castingContext.Set<T>().Remove(entity);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateAsync<T>(T entity) where T : class
        {
            _castingContext.SetModified(entity);
            return await _castingContext.SaveChangesAsync();
        }

        public void SetEntityState<T>(T entity, EntityState state) where T : class
        {
            _castingContext.SetState(entity,state);
        }
    }

}
