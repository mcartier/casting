import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditionDetailsListComponent } from './audition-details-list.component';

describe('AuditionDetailsListComponent', () => {
  let component: AuditionDetailsListComponent;
  let fixture: ComponentFixture<AuditionDetailsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditionDetailsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditionDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
