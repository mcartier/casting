﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Casting.Web.Helpers;
using System.Net.Http.Headers;

namespace Casting.Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            //config.Filters.Add(new ValidateModelAttribute());
            
            // Web API routes
            config.MapHttpAttributeRoutes();
            //config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            // config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            // config.Formatters.JsonFormatter.SupportedMediaTypes
            //     .Add(new MediaTypeHeaderValue("text/html"));
            // config.Formatters.Add(new BrowserJsonFormatter());
            // Web API routes
          config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


           config.Routes.MapHttpRoute(
                name: "DefaultApi2",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
           
           

        }
    }
}
