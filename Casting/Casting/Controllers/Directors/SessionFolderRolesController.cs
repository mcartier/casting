﻿using Casting.BLL.CastingSessions;
using Casting.BLL.CastingSessions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Sessions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingSessions;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using Casting.DAL;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingSessionfolderroles")]
   // [Authorize]
    public class SessionFolderRolesController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingSessionFolderRoleService _castingSessionFolderRoleService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public SessionFolderRolesController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, ICastingSessionFolderRoleService castingSessionFolderRoleService, IProductionService productionService, ICastingContext castingContext) : base(castingContext)
        {
            _productionService = productionService;
            _castingSessionFolderRoleService = castingSessionFolderRoleService;
            _metadataService = metadataService;
            _userManager = userManager;
        }


        // GET: api/CastingSessionFolderRoles
        [ResponseType(typeof(IEnumerable<CastingSessionFolderRole>))]
        public async Task<IHttpActionResult> GetCastingSessionFolderRoles(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionFolderRoles =
                await _castingSessionFolderRoleService.GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAndAssetsAsync(0);

            return Ok(castingSessionFolderRoles);
        }

        // GET: api/CastingSessionFolderRoles
        [System.Web.Http.ActionName("castingSessionFolder")]
        [Route("castingSessionFolder/{castingSessionFolderId}")]
        [ResponseType(typeof(IEnumerable<CastingSessionFolderRole>))]
        public async Task<IHttpActionResult> GetCastingSessionFolderRolesForCastingSessionFolder(int castingSessionFolderId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionFolderRoles =
                await _castingSessionFolderRoleService.GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAndAssetsAsync(castingSessionFolderId);

            return Ok(castingSessionFolderRoles);
        }
        // GET: api/CastingSessionFolderRoles
        [System.Web.Http.ActionName("castingSessionRole")]
        [Route("castingSessionRole/{castingSessionRoleId}")]
        [ResponseType(typeof(IEnumerable<CastingSessionFolderRole>))]
        public async Task<IHttpActionResult> GetCastingSessionFolderRolesForCastingSessionRole(int castingSessionRoleId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionFolderRoles =
                await _castingSessionFolderRoleService.GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAndAssetsAsync(castingSessionRoleId);

            return Ok(castingSessionFolderRoles);
        }

        // GET: api/CastingSessionFolderRoles/5
        [ResponseType(typeof(CastingSessionFolderRole))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var castingSessionFolderRole =
                await _castingSessionFolderRoleService.GetCastingSessionFolderRoleByIdAsync(id);

            return Ok(castingSessionFolderRole);
        }

        // POST: api/CastingSessionFolderRoles
        [ResponseType(typeof(CastingSessionFolderRole))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewCastingSessionFolderRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {
               return BadRequest("User is not an Director");

           }*/
             /* var castingSessionFolderRole = new CastingSessionFolderRole()
            {
                DirectorId = user.BaseUserId,
                Name = name
            };*/
            var castingSessionFolderRole = new CastingSessionFolderRole()
            {
                RoleId = value.RoleId,
                CastingSessionRoleId = value.CastingSessionRoleId,
                // ViewTypeId = value.ViewTypeId,
                CastingSessionFolderId = value.CastingSessionFolderId,
                Name = value.Name,
               

            };
            var ret = await _castingSessionFolderRoleService.AddNewCastingSessionFolderRoleAsync(true,castingSessionFolderRole);
            return CreatedAtRoute("DefaultApi", new { id = castingSessionFolderRole.Id }, castingSessionFolderRole);
        }

        // PUT: api/CastingSessionFolderRoles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateCastingSessionFolderRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSessionFolderRole = await _castingSessionFolderRoleService.GetCastingSessionFolderRoleByIdAsync(id);
            if (castingSessionFolderRole == null)
            {
                return NotFound();
            }
            /* if (castingSessionFolderRole.CastingSessionFolder.DirectorId != user.BaseUserId)
           {
               return Unauthorized();
           }*/

            castingSessionFolderRole.RoleId = value.RoleId;
            castingSessionFolderRole.CastingSessionRoleId = value.CastingSessionRoleId;
           // castingSessionFolderRole.ViewTypeId = value.ViewTypeId;
            castingSessionFolderRole.CastingSessionFolderId = value.CastingSessionFolderId;
            castingSessionFolderRole.Name = value.Name;

           

            try
            {
                await _castingSessionFolderRoleService.UpdateCastingSessionFolderRoleAsync(castingSessionFolderRole);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/CastingSessionFolderRoles/5
        [ResponseType(typeof(CastingSessionFolderRole))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSessionFolderRole = await _castingSessionFolderRoleService.GetCastingSessionFolderRoleByIdAsync(id);
            if (castingSessionFolderRole == null)
            {
                return NotFound();
            }
           /* if (castingSessionFolderRole.CastingSessionFolder.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/

            await _castingSessionFolderRoleService.DeleteCastingSessionFolderRole(castingSessionFolderRole);

            return Ok(castingSessionFolderRole);
        }
    }
}
