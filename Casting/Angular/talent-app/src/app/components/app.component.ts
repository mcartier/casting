import { Component, OnInit,OnDestroy  } from '@angular/core';
import { StateService, TransitionService, equals, StateDeclaration } from '@uirouter/core';
import { MessageService, Message } from 'primeng/api';
import { MessageDispatchService } from '../services/message-dispatch.service';
import { Subscription } from 'rxjs';
import { ConfirmDecisionSubscriptionService } from '../services/confirm-decision-subscription.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    title = 'talent-app';
    constructor(
        public transitionService: TransitionService,
        private messageDispatchService: MessageDispatchService,
        private messageService: MessageService,
        private confirmDecisionDispatcher: ConfirmDecisionSubscriptionService) {
      //this.isShowingRouteLoadIndicator = false;

    }
    private messageDispatcher: Subscription;
    private decisionDispatcher: Subscription;

    private isShowingRouteLoadIndicator:boolean;
    private asyncLoadCount = 0;
    ngOnInit() {
      this.messageDispatcher = this.messageDispatchService.subscribeToMessageDispatcherObs()
        .subscribe(messages => {
          this.messageService.addAll(messages);

        });
        this.decisionDispatcher = this.confirmDecisionDispatcher.subscribeToDecisionDispatcherObs()
        .subscribe(message => {
          this.messageService.clear('confirm');
            this.messageService.add(message);


        });
      // Make an editable copy of the pristineContact
      this.transitionService.onBefore({},
          function (transition) {
          this.asyncLoadCount++;
          this.isShowingRouteLoadIndicator = !!this.asyncLoadCount;
        },
        { priority: 10, bind:this });

      this.transitionService.onSuccess({},
        function (transition) {
          this.asyncLoadCount--;
          this.isShowingRouteLoadIndicator = !!this.asyncLoadCount;
        },
        { priority: 10, bind: this });//(this.asyncLoadCount);
      this.transitionService.onError({},
        function (transition) {
          this.asyncLoadCount--;
          this.isShowingRouteLoadIndicator = !!this.asyncLoadCount;
        },
        { priority: 10, bind: this });//(this.asyncLoadCount);

    }

    ngOnDestroy() {
      if (this.messageDispatcher) {
        this.messageDispatcher.unsubscribe();

      }
        if (this.decisionDispatcher) {
            this.decisionDispatcher.unsubscribe();

      }


    }

    onDecisionConfirm() {
        this.messageService.clear('confirm');
        this.confirmDecisionDispatcher.dispatchAgreeDecision();
    }

    onDecisionReject() {
        this.messageService.clear('confirm');
        this.confirmDecisionDispatcher.dispatchDeclineDecision();

    }

}
