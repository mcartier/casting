﻿using Casting.Model.Entities.Attributes;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.AttributeReferences
{
    public class BodyTypeReference:Logger
    {
        public int Id { get; set; }
        public int BodyTypeId { get; set; }
        public BodyType BodyType { get; set; }
        public AttributeReferenceTypeEnum AttributeReferenceTypeEnum { get; set; }

        public int OrderIndex { get; set; }
    }
}
