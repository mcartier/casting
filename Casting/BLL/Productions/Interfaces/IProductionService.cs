﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{
    public interface IProductionService
    {
        Task<Production> GetProductionByIdWithRolesAsync(int productionId);
        Task<Production> GetProductionByIdAsync(int productionId);
        Task<Production> GetProductionByIdWithRolesCastingCallsSessionsAsync(int productionId);
        Task<IEnumerable<Production>> GetProductionsByDirectorIdAsync(int directorId, int pageIndex = 0, int pageSize = 12);
        Task<IEnumerable<Production>> GetCompleteProductionsByDirectorIdAsync(int directorId, int pageIndex = 0, int pageSize = 12);
        Task<int> AddNewProductionAsync(Production production);
        Task<int> UpdateProductionAsync(Production production);
        Task<int> DeleteProduction(Production production);
    }
}