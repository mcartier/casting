﻿

using System;
using System.Web.Mvc;
using System.Linq.Expressions;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using StructureMap.Pipeline;
using StructureMap.TypeRules;

using StructureMap;
using WebGrease.Css.Extensions;

namespace Casting.Web.DependencyResolution
{


    public class ControllerConvention : IRegistrationConvention
    {
        #region Public Methods and Operators

        public void Process(Type type, Registry registry)
        {
            if ((type.CanBeCastTo<Controller>() || type.CanBeCastTo<ApiController>() || type.CanBeCastTo<OAuthAuthorizationServerProvider>()) && !type.IsAbstract)
            {
                registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
            }
        }

        public void ScanTypes(TypeSet types, Registry registry)
        {
            types.FindTypes(TypeClassification.All).ForEach(type =>
            {
               // if (type.CanBeCastTo<ApiController>() && !type.IsAbstract)
              //  {
                 //   registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
              //  } // Register against all the interfaces implemented

            //    if (type.CanBeCastTo<Controller>() && !type.IsAbstract)
           //     {
            //        registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
            //    } // Register against all the interfaces implemented
            });
            // if types.CanBeCastTo<Controller>() && !type.IsAbstract)
            // {
            //      registry.For(type).LifecycleIs(new UniquePerRequestLifecycle());
            //  }
        }

        #endregion
    }
}