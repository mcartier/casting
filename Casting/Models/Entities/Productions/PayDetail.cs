﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Productions
{
   public class PayDetail : Logger
    {
        public PayDetail()
        {
          

        }
    public int Id { get; set; }
      
        public int ProductionId { get; set; }
        public Production Production { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public PayPackageTypeEnum PayPackageTypeEnum { get; set; }


        
    }
}
