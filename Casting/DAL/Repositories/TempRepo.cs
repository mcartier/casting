﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using Casting.Model.Entities.Campaigns;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{
    internal interface ITempRepo
    {
        Task<CastingSessionFolderRole> GetCastingSessionFolderRoleById(ProductionRoleIncludeInQueryParameterEnum includeInQuery, int castingSessionFolderRoleId);
        Task<int> AddNewCastingSessionFolderRoleAsync(bool orderAtTheTop, CastingSessionFolderRole castingSessionFolderRole);
        Task<int> DeleteCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole);
        Task<int> UpdateCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole);
        Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderId(ProductionRoleIncludeInQueryParameterEnum includeInQuery, int castingSessionFolderId);
        Task<int> ReOrderCastingSessionFolderRoleAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole);
    }

    class TempRepo : AsyncRepository, ITempRepo
    {
        ICastingContext _castingContext;
        public TempRepo(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region CastingSessionFolderRoles

        public async Task<CastingSessionFolderRole> GetCastingSessionFolderRoleById(ProductionRoleIncludeInQueryParameterEnum includeInQuery, int castingSessionFolderRoleId)
        {
            IQueryable<CastingSessionFolderRole> castingSessionFolderRoles = GetCastingSessionFolderRolesQueryable(includeInQuery);

            return await castingSessionFolderRoles.FirstOrDefaultAsync(p => p.Id == castingSessionFolderRoleId);


        }

        public async Task<int> AddNewCastingSessionFolderRoleAsync(bool orderAtTheTop, CastingSessionFolderRole castingSessionFolderRole)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingSessionFolderRoleByCastingSessionFolderAsync(castingSessionFolderRole);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingSessionFolderRole.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingSessionFolderRole>().Add(castingSessionFolderRole);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingSessionFolderRoleAsync(1, castingSessionFolderRole);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingSessionFolderRoleAsync(0, castingSessionFolderRole);

                    _castingContext.Set<CastingSessionFolderRole>().Remove(castingSessionFolderRole);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole)
        {
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderId(ProductionRoleIncludeInQueryParameterEnum includeInQuery, int castingSessionFolderId)
        {
            var castingSessionFolderRoles = GetCastingSessionFolderRolesQueryable(includeInQuery);

            return await castingSessionFolderRoles.Where(d => d.CastingSessionFolderId == castingSessionFolderId).ToListAsync();
        }
        public async Task<int> ReOrderCastingSessionFolderRoleAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole)
        {
            IEnumerable<CastingSessionFolderRole> castingSessionFolderRoles = await GetCastingSessionFolderRolesByCastingSessionFolderId(ProductionRoleIncludeInQueryParameterEnum.Nothing, castingSessionFolderRole.CastingSessionFolderId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingSessionFolderRoleUpAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            else if (desiredOrderIndex > castingSessionFolderRole.OrderIndex)
            {
                return await ReOrderCastingSessionFolderRoleUpAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            else if (desiredOrderIndex < castingSessionFolderRole.OrderIndex)
            {
                return await ReOrderCastingSessionFolderRoleDownAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingSessionFolderRoleByCastingSessionFolderAsync(CastingSessionFolderRole castingSessionFolderRole)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingSessionFolderRoles
                                 where role.CastingSessionFolderId == castingSessionFolderRole.CastingSessionFolderId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingSessionFolderRoleDownAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole,
            IEnumerable<CastingSessionFolderRole> castingSessionFolderRoles)
        {
            foreach (var role in castingSessionFolderRoles)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingSessionFolderRole.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolderRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingSessionFolderRoleUpAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole,
            IEnumerable<CastingSessionFolderRole> castingSessionFolderRoles)
        {
            foreach (var role in castingSessionFolderRoles)
            {
                if (role.OrderIndex > castingSessionFolderRole.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolderRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingSessionFolderRole> GetCastingSessionFolderRolesQueryable(ProductionRoleIncludeInQueryParameterEnum includeInQuery)
        {
            IQueryable<CastingSessionFolderRole> castingSessionFolderRoles = from castingSessionFolderRole in _castingContext.CastingSessionFolderRoles select castingSessionFolderRole;
            if (includeInQuery == ProductionRoleIncludeInQueryParameterEnum.Personas)
            {
                castingSessionFolderRoles
                    .Include("Roles.SubmittedTalents.Persona");
            }
            if (includeInQuery == ProductionRoleIncludeInQueryParameterEnum.PersonasAndAssets)
            {
                castingSessionFolderRoles
                    .Include("Roles.SubmittedTalents.Persona")
                    .Include("Roles.SubmittedTalents.Persona.VideoReferences.Video")
                    .Include("Roles.SubmittedTalents.Persona.AudioReferences.Audio")
                    .Include("Roles.SubmittedTalents.Persona.ResumeReferences.Resume")
                    .Include("Roles.SubmittedTalents.Persona.ImageReferences.Image");
            }
            return castingSessionFolderRoles;
        }


        #endregion
        #endregion

    }
}
