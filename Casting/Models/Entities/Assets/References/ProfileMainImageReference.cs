﻿using Casting.Model.Entities.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets.References
{
    public class ProfileMainImageReference: ImageReference
    {
        public ProfileMainImageReference()
        {
            base.AssetReferenceTypeEnum = AssetReferenceTypeEnum.ProfileMainImage;
        }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }


    }
}
