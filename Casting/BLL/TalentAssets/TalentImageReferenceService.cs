﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Enums;
using Casting.BLL.TalentAssets.Interfaces;


namespace Casting.BLL.TalentAssets
{
    public class TalentImageReferenceService : BaseService, ITalentImageReferenceService
    {
        private ITalentAssetAsyncRepository _assetRepository;

        public TalentImageReferenceService(ITalentAssetAsyncRepository assetRepository)
        {
            _assetRepository = assetRepository;
        }

        public async Task<ImageReference> GetImageReferenceByIdAsync(int imageId)
        {
            var image = await _assetRepository.GetImageAssetReferenceByIdAsync(true, imageId);
            return image;
        }
        public async Task<IEnumerable<ImageReference>> GetProfileImageReferencesForParentAsync(int profileId, bool withAssets)
        {
            var imageReferences = await _assetRepository.GetImageReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Profile, profileId);
            return imageReferences;
        }
        public async Task<IEnumerable<ImageReference>> GetPersonaImageReferencesForParentAsync(int personaId, bool withAssets)
        {
            var imageReferences = await _assetRepository.GetImageReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Persona, personaId);
            return imageReferences;
        }
        public async Task<IEnumerable<ImageReference>> GetBasketImageReferencesForParentAsync(int basketId, bool withAssets)
        {
            var imageReferences = await _assetRepository.GetImageReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Basket, basketId);
            return imageReferences;
        }
        public async Task<IEnumerable<ImageReference>> GetProfileMainImageImageReferencesForParentAsync(int profileId, bool withAssets)
        {
            var imageReferences = await _assetRepository.GetImageReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.ProfileMainImage, profileId);
            return imageReferences;
        }
        public async Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
            ImageReference assetReference)
        {
            return await _assetRepository.AddNewImageAssetReferenceAsync(orderAtTheTop,assetReference);
        }

        public async Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
            int parentId, ImageReference assetReference)
        {
            return await _assetRepository.AddNewImageAssetReferenceAsync(orderAtTheTop, parentId, assetReference);
        }

        public async Task<int> AddNewProfileImageAssetReferenceAsync(bool orderAtTheTop, ActorProfileImageReference assetReference)
        {
            return await AddNewImageAssetReferenceAsync(orderAtTheTop, assetReference.ProfileId, assetReference);
        }
        public async Task<int> AddNewPersonaImageAssetReferenceAsync(bool orderAtTheTop, PersonaImageReference assetReference)
        {
            return await AddNewImageAssetReferenceAsync(orderAtTheTop, assetReference.PersonaId, assetReference);
        }
        public async Task<int> AddNewBasketImageAssetReferenceAsync(bool orderAtTheTop, BasketImageReference assetReference)
        {
            return await AddNewImageAssetReferenceAsync(orderAtTheTop, assetReference.BasketId, assetReference);
        }

        public async Task<int> DeleteImageAssetReferenceAsync(int parentId, ImageReference assetReference)
        {


            return await _assetRepository.DeleteImageAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> DeleteProfileImageAssetReferenceAsync(ActorProfileImageReference assetReference)
        {


            return await DeleteImageAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> DeleteBasketImageAssetReferenceAsync(BasketImageReference assetReference)
        {


            return await DeleteImageAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> DeletePersonaImageAssetReferenceAsync(PersonaImageReference assetReference)
        {


            return await DeleteImageAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }
        public async Task<int> UpdateImageAssetReferenceAsync(int parentId,
            ImageReference assetReference)
        {
            return await _assetRepository.UpdateImageAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> UpdateProfileImageAssetReferenceAsync(ActorProfileImageReference assetReference)
        {
            return await UpdateImageAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> UpdateBasketImageAssetReferenceAsync(BasketImageReference assetReference)
        {
            return await UpdateImageAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> UpdatePersonaImageAssetReferenceAsync(PersonaImageReference assetReference)
        {
            return await UpdateImageAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }

        public async Task<int> ReOrderImageAssetAsync(int parentId,
            int desiredOrderIndex, ImageReference assetReference)


        {
            return await _assetRepository.ReOrderImageAssetAsync(parentId, desiredOrderIndex, assetReference);
        }

        public async Task<int> ReOrderProfileImageAssetAsync(int desiredOrderIndex, ActorProfileImageReference assetReference)


        {
            return await ReOrderImageAssetAsync(assetReference.ProfileId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderBasketImageAssetAsync(int desiredOrderIndex, BasketImageReference assetReference)


        {
            return await ReOrderImageAssetAsync(assetReference.BasketId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderPersonaImageAssetAsync(int desiredOrderIndex, PersonaImageReference assetReference)


        {
            return await ReOrderImageAssetAsync(assetReference.PersonaId, desiredOrderIndex, assetReference);
        }
    }
}
