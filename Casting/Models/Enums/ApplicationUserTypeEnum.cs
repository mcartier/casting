﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum ApplicationUserTypeEnum
    {
        Guest = 1,
        Talent = 3,
        Agent = 7,
        Director = 15
    }
}
