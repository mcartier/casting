import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayDetailsListItemComponent } from './pay-details-list-item.component';

describe('PayDetailsListItemComponent', () => {
  let component: PayDetailsListItemComponent;
  let fixture: ComponentFixture<PayDetailsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayDetailsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayDetailsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
