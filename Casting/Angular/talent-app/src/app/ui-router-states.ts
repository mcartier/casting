﻿import { Transition } from "@uirouter/angular";
import { AppComponent } from './components/app.component';
import { WINDOW_PROVIDERS } from './services/browser.window.provider';
import { HomeComponent } from './components/home/home.component';
import { LogoutComponent } from './components/logout/logout.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { MainSidePulloutScrollComponent } from './components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component';
import { MainSidePulloutNonScrollComponent } from './components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component';
import { MainPulloutScrollComponent } from './components/containers/main-pullout-scroll/main-pullout-scroll.component';
import { MainPulloutNonScrollComponent } from './components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component';
import { PulloutComponent } from './components/containers/pullout/pullout.component';
import { NoPulloutComponent } from './components/containers/no-pullout/no-pullout.component';
import { MainScrollComponent } from './components/containers/main-scroll/main-scroll.component';
import { MainNonScrollComponent } from './components/containers/main-non-scroll/main-non-scroll.component';

/** States */
export const rootState = {
  name: 'root',
  redirectTo: 'home',
  url: '',
  component: AppComponent
};

export const pulloutContainerState = {
  parent: 'root',
  name: 'pulloutContainer',
  //redirectTo: 'home',
  url: '',
  //views: {
  //     "main@root": { component: MainContainerComponent }
  //   },
  component: PulloutComponent
};
export const noPulloutContainerState = {
  parent: 'root',
  name: 'noPulloutContainer',
  //redirectTo: 'home',
  url: '',
  //views: {
  //     "main@root": { component: MainContainerComponent }
  //   },
    component: NoPulloutComponent
};
export const mainScrollContainerState = {
    parent: 'noPulloutContainer',
  name: 'mainScrollContainer',
  //redirectTo: 'home',
  url: '',
  views: {
      "main@noPulloutContainer": { component: MainScrollComponent }
  },
  //component: MainScrollComponent
};

export const mainNonScrollContainerState = {
    parent: 'noPulloutContainer',
    name: 'mainNonScrollContainer',
  //redirectTo: 'home',
  url: '',
  views: {
      "main@noPulloutContainer": { component: MainNonScrollComponent }
  },
  //component: MainNonScrollComponent
};

export const mainPulloutNonScrollContainerState = {
    parent: 'pulloutContainer',
  name: 'mainPulloutNonScrollContainer',
  //redirectTo: 'home',
  url: '',
  views: {
      "main@pulloutContainer": { component: MainPulloutNonScrollComponent }
    },
  //component: MainPulloutNonScrollComponent
};


export const mainPulloutScrollContainerState = {
    parent: 'pulloutContainer',
  name: 'mainPulloutScrollContainer',
  views: {
      "main@pulloutContainer": { component: MainPulloutScrollComponent }
  },
  //component: MainPulloutScrollComponent,
  //redirectTo: 'home',
  url: '',
  // component: SidePulloutContainerComponent
    data: { requiresAuth: true }
};

export const mainSidePulloutNonScrollContainerState = {
    parent: 'pulloutContainer',
    name: 'mainSidePulloutNonScrollContainer',
  //redirectTo: 'home',
  url: '',
  views: {
      "main@pulloutContainer": { component: MainSidePulloutNonScrollComponent }
  },
  //component: MainSidePulloutNonScrollComponent
};


export const mainSidePulloutScrollContainerState = {
    parent: 'pulloutContainer',
    name: 'mainSidePulloutScrollContainer',
  views: {
      "main@pulloutContainer": { component: MainSidePulloutScrollComponent }
  },
  //component: MainSidePulloutScrollComponent,
  //redirectTo: 'home',
  url: '',
  // component: SidePulloutContainerComponent
};



export function returnTo($transition$: Transition): any {
    if ($transition$.redirectedFrom() != null) {
        // The user was redirected to the login state (e.g., via the requiresAuth hook when trying to activate contacts)
        // Return to the original attempted target state (e.g., contacts)
        return $transition$.redirectedFrom().targetState();
    }

    const $state = $transition$.router.stateService;

    // The user was not redirected to the login state; they directly activated the login state somehow.
    // Return them to the state they came from.
    if ($transition$.from().name !== '') {
      return $state.target($transition$.from(), $transition$.params('from'));
    }

    // If the fromState's name is empty, then this was the initial transition. Just return them to the home state
    return $state.target('home');
}

export const homeState = {
  name: "home",
  url: '/home',
    parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: HomeComponent }
  },
 // data: { requiresAuth: true }

};

/**
 * This is the login state.  It is activated when the user navigates to /login, or if a unauthenticated
 * user attempts to access a protected state (or substate) which requires authentication. (see routerhooks/requiresAuth.js)
 *
 * It shows a fake login dialog and prompts the user to authenticate.  Once the user authenticates, it then
 * reactivates the state that the user originally came from.
 */
export const loginState = {
  name: 'login',
  url: '/login',
  parent: "mainScrollContainer",
  views: {
    "main@mainScrollContainer": { component: LoginComponent }
  },
  resolve: [
    { token: 'returnTo', deps: [Transition], resolveFn: returnTo },
  ]
};

export const registerState = {
  name: 'register',
  url: '/register',
  parent: "mainScrollContainer",
  views: {
    "main@mainScrollContainer": { component: RegisterComponent }
  },
  resolve: [
    { token: 'returnTo', deps: [Transition], resolveFn: returnTo },
  ]
};

export const logoutState = {
  name: "logout",
  url: '/logout',
    component: LogoutComponent
};

// This future state is a placeholder for all the lazy loaded feature module states
// The feature modules are not loaded until their link is activated


export const talentFutureState = {
  name: 'talent.**',
  url: '/talent',
  loadChildren: './feature-modules/talent/talent.module#TalentModule'
};
export const recordFutureState = {
  name: 'record.**',
  url: '/record',
  loadChildren: './feature-modules/recording/recordingManager.module#RecordingManagerModule'
};

export const newProductionFutureState = {
  name: 'productions.**',
  url: '/productions',
  loadChildren: './feature-modules/production/production.module#NewProductionModule'
};

export const productionCastingCallsFutureState = {
  name: 'productioncastingCalls.**',
  url: '/productions/:productionId/casting-calls',
  loadChildren: './feature-modules/casting-call/casting-call.module#CastingCallModule'
};

export const newCastingCallFutureState = {
  name: 'newcastingCall.**',
  url: '/productions/:productionId/casting-calls/new',
  loadChildren: './feature-modules/casting-call/casting-call.module#CastingCallModule'
};

export const castingCallsFutureState = {
  name: 'castingCalls.**',
  url: '/casting-calls',
  loadChildren: './feature-modules/casting-call/casting-call.module#CastingCallModule'
};


// This future state is a placeholder for the lazy loaded Prefs states
export const prefsFutureState = {
    name: 'prefs.**',
    url: '/prefs',
    loadChildren: './prefs/prefs.module#PrefsModule'
};

// This future state is a placeholder for the lazy loaded My Messages feature module
export const mymessagesFutureState = {
    name: 'mymessages.**',
    url: '/mymessages',
    loadChildren: './mymessages/mymessages.module#MymessagesModule'
};


export const APP_STATES = [
    rootState,
    pulloutContainerState,
    noPulloutContainerState,
    mainScrollContainerState,
    mainPulloutScrollContainerState,
    mainSidePulloutScrollContainerState,
    mainNonScrollContainerState,
    mainPulloutNonScrollContainerState,
    mainSidePulloutNonScrollContainerState,
    homeState,
    logoutState,
    registerState,
    loginState,
    recordFutureState,
    newProductionFutureState,
    castingCallsFutureState,
    productionCastingCallsFutureState,
    newCastingCallFutureState
];

