﻿import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from './metadata';
export class ProductionRoleTransferObject {
    public production: Production;
    public role:Role;
}
export class BaseProduction {
   /* constructor() {
        this.productionCompany = "";
        this.name = "";
        this.description = "";
        this.productionInfo = "";
        this.executiveProducer = "";
        this.coExecutiveProducer = "";
        this.producer = "";
        this.coProducer = "";
        this.castingDirector = "";
        this.castingAssistant = "";
        this.castingAssociate = "";
        this.assistantCastingAssociate = "";
        this.artisticDirector = "";
        this.musicDirector = "";
        this.choreographer = "";
        this.tVNetwork = "";
        this.venue = "";
        this.compensationType = CompensationTypeEnum.notPaid;
        this.compensationContractDetails = "";
        this.productionUnionStatus = ProductionUnionStatusEnum.both;
    }*/
    public productionCompany:string;
    public isSave: boolean;
    public productionTypeId: number;
    public name: string;
    public description:string;
    public productionInfo:string;
    public executiveProducer:string;
    public coExecutiveProducer:string;
    public producer:string;
    public coProducer:string;
    public castingDirector:string;
    public castingAssistant:string;
    public castingAssociate:string;
    public assistantCastingAssociate:string;
    public artisticDirector:string;
    public musicDirector:string;
    public choreographer:string;
    public tVNetwork:string;
    public venue:string;
    public compensationType: CompensationTypeEnum;
    public compensationContractDetails:string;
    public productionUnionStatus: ProductionUnionStatusEnum;
}
export class Production extends BaseProduction {
  /*  constructor() {

        super();
    }
    */
    public id: number;
    public productionTypes: MetadataItem[] = [];
    public roles: Role[] = [];
    public sides: Side[] = [];
    public auditionDetails: AuditionDetail[] = [];
    public shootDetails: ShootDetail[] = [];
    public payDetails: PayDetail[] = [];
}
export class NewProduction extends BaseProduction {
  public productionTypes: number[];
}
export class UpdateProduction extends BaseProduction {
    public id: number;
   // public productionTypes: number[];
}
export class BaseRole {
    public productionId: number;
    public description:string;
    public name:string;
    public maxAge: number;
    public minAge: number;
    public requiresNudity: boolean;
    public requiresHeadshot: boolean;
    public headshotMaxCount: number;
    public requiresVideo: boolean;
    public videoMaxCount: number;
    public requiresAudio: boolean;
    public audioMaxCount: number;
    public requiresCover: boolean;
    public transgender: boolean;
    public roleTypeId: number;
    public roleGenderTypeId: number;
    public ethnicityId: number;
    public payDetailId:number;
    public languages: any[];
    public hairColors: any[];
    public eyeColors: any[];
    public bodyTypes: any[];
    public skills: any[];
    public accents: any[];
    public ethnicities: any[];
    public stereoTypes: any[];
    public ethnicStereoTypes: any[];

}
export class Role extends BaseRole {
    public id: number;
     public roleType: any;
    public roleGenderType: any;
}

export class NewRole extends BaseRole {

    public stereoTypes: number[];
    public ethnicStereoTypes: number[];
}
export class UpdateRole extends BaseRole {

    public id: number;
    public stereoTypes: number[];
    public ethnicStereoTypes: number[];
}
export class BaseProductionExtra {
  public id: number;
  public productionId: number;
  public name: string;

}
export class AuditionDetail extends BaseProductionExtra {
    public startDate: Date;
    public endDate: Date;
    public location: string;
    public locationDetail: string;
    public details: string;
    public instructions: string;
    public auditionTypeEnum: AuditionTypeEnum;
    public isOneDayOnly: boolean;
    public onlyThoseSelectedWillParticipate: boolean;
    public datesToBeDecidedLater: boolean;
    public datesAreTemptative: boolean;

}


export class ShootDetail extends BaseProductionExtra {
  public startDate: Date;
  public endDate: Date;
  public location: string;
  public locationDetail: string;
  public details: string;
  public instructions: string;
   public isOneDayOnly: boolean;
  public datesToBeDecidedLater: boolean;
  public datesAreTemptative: boolean;
  public daysOfShooting: number;
}
export class Side extends BaseProductionExtra {
  public fileName: string;
  public originalFileName: string;
        public pastedText: string;
    public fileId:string;
}
export class PayDetail extends BaseProductionExtra {
  public details: string;
  public payPackageType: PayPackageTypeEnum;

}
export enum CompensationTypeEnum {
    notPaid = 1,

    somePaid = 3,

    paid = 7
}
export enum ProductionUnionStatusEnum {
    notRelevan = 0,

    nonUnion = 1,

    union = 3,

    both = 7
}
export enum PayPackageTypeEnum
{
  money = 1,
  credit = 3,
  gift = 7
}
export enum AuditionTypeEnum
{
  TapedSubmissionsOnly = 1,
  TapedSubmissionAndInPersonAudition = 3,
  TapedSubmissionThenInPersonAudition = 7,
  InPersonAuditionOnly = 15
}
