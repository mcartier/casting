﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{
  

    public class FileAsyncRepository : AsyncRepository, IFileAsyncRepository
    {
        ICastingContext _castingContext;
        public FileAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }
     

       
        #region sidefiles
        public async Task<File> GetFileByFileIdAsync(string fileId)
        {
            return await _castingContext.Files.FirstOrDefaultAsync(f => f.FileId == fileId);


        }

        public async Task<int> AddNewFileAsync(File file)
        {
            _castingContext.Set<File>().Add(file);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteFileAsync(File file)
        {
            _castingContext.Set<File>().Remove(file);
            return await _castingContext.SaveChangesAsync();
        }


        #endregion

     


        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
