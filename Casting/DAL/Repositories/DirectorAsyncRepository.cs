﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Infrastructure;
using System.Runtime.InteropServices.WindowsRuntime;

using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.Profiles;
using Casting.Model.Enums;
using SSW.Data.Entities;
using System.Reflection;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;


namespace Casting.DAL.Repositories
{
   
    public class DirectorAsyncRepository :AsyncRepository, IDirectorAsyncRepository
    {
        ICastingContext _castingContext;
        public DirectorAsyncRepository(ICastingContext castingContext):base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Directors

        public async Task<Director> GetDirectorByIdAsync(int directorId, bool withProductions)
        {
            return await GetDirectorUsersQueryable(withProductions)
                .FirstOrDefaultAsync(t => t.Id == directorId);

        }
        
        public async Task<IEnumerable<Director>> GetDirectorsAsync(bool withProductions)
        {
            return await GetDirectorUsersQueryable(withProductions).ToListAsync();

        }

        public async Task<int> AddNewDirectorAsync(Director director)
        {
            _castingContext.Directors.Add(director);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteDirectorAsync(Director director)
        {
            _castingContext.Directors.Remove(director);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateDirectorAsync(Director director)
        {
           SetEntityState(director, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion




        #region PrivateMethods

       
        private IQueryable<Director> GetDirectorUsersQueryable(bool withProductions)
        {
            var users = from user in _castingContext.Directors
                select user;
            
            if (withProductions)
            {
               
                        users.Include(p => p.Productions);
                 
            }

            return users;
        }
        

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }
                disposedValue = true;
            }
        }

     

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public IEnumerable<Director> GetComplete()
        {
            throw new NotImplementedException();
        }


        #endregion

    }
}
