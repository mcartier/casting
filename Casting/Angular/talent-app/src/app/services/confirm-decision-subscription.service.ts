import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { Message } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class ConfirmDecisionSubscriptionService {

    constructor() { }

    private _confirmDecisionDispatcherObs: Subject<boolean> = new Subject<boolean>();
    public readonly confirmDecisionDispatcherObs: Observable<boolean> = this._confirmDecisionDispatcherObs.asObservable();

    public subscribeToConfirmDecisionDispatcherObs(): Observable<boolean> {
      return this._confirmDecisionDispatcherObs;
    }

    private _decisionDispatcherObs: Subject<Message> = new Subject<Message>();
    public readonly decisionDispatcherObs: Observable<Message> = this._decisionDispatcherObs.asObservable();

    public subscribeToDecisionDispatcherObs(): Observable<Message> {
      return this._decisionDispatcherObs;
    }
    public poseConfirmDecision(summary: string, detail: string) {
      let message: Message = { key: 'confirm', sticky: true, severity: 'warn', summary: summary, detail: detail };
        this._decisionDispatcherObs.next(message);
    }

   
    public dispatchAgreeDecision() {
         this._confirmDecisionDispatcherObs.next(true);
    }

    public dispatchDeclineDecision() {
          this._confirmDecisionDispatcherObs.next(false);
    }

}
