﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Sessions;

namespace Casting.BLL.CastingSessions.Interfaces
{
    public interface ICastingSessionRoleService
    {
        Task<CastingSessionRole> GetCastingSessionRoleByIdAsync(int castingSessionRoleId);
        Task<CastingSessionRole> GetCastingSessionRoleByIdWithPersonasAsync(int castingSessionRoleId);
        Task<CastingSessionRole> GetCastingSessionRoleByIdWithPersonasAndAssetsAsync(int castingSessionRoleId);
        Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdAsync(int castingSessionId);
        Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdWithPersonasAsync(int castingSessionId);
        Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdWithPersonasAndAssetsAsync(int castingSessionId);
        Task<int> AddNewCastingSessionRoleAsync(bool orderAtTheTop, CastingSessionRole castingSessionRole);
        Task<int> UpdateCastingSessionRoleAsync(CastingSessionRole castingSessionRole);
        Task<int> DeleteCastingSessionRole(CastingSessionRole castingSessionRole);
    }
}