﻿import { map, retry, catchError, publishLast, refCount } from 'rxjs/operators';
import { Observable, of, BehaviorSubject, Subject, throwError } from 'rxjs';
import * as tus from 'tus-js-client';
import { BlobUploadState, CreatedBlob } from '../models/blobs';

export interface ITusUploadObject {
 /* constructor(private _file: File, _uploadUrl: string, _headers: any, _metadata: any) {
    this.totalSize = _file.size;
    this.file = _file;
    this.uploadUrl = _uploadUrl;
    this.metadata = _metadata;
    this.headers = _headers;
    this.obs = new Subject<this>();
  }*/
  //obs: Subject<BlobUploadState>;
  id:any;
  metadata: any;
  upload: tus.Upload;
  uploadUrl: string;
  uploadedFileId: string;
  file: File;
  isUploading: boolean;
  totalSize: number;
  uploadedSize: number;
  percentageUploaded: number;
  isComplete: boolean;
  isSuccess: boolean;
  isError: boolean;
    error: any;
    buildMetadata():void;
  sartUpload():void;
  successCallback(file):void;
  errorCallback(file, error):void;
  progressCallback(file, bytesUploaded, bytesTotal, percentage):void;
}

export class TusHelper {
  constructor() {
  }

    static tusUploadHelper(blobUploadState: ITusUploadObject
      // videoArray: uploadFiles[],
      // uploadArray: tus.Upload[],
      // success: any
    ): tus.Upload {
      const upload = new tus.Upload(blobUploadState.file, {
        uploadUrl: blobUploadState.uploadUrl,
        //uploadUrl: 'https://cast.com/fileso/',
        endpoint: blobUploadState.uploadUrl,
        retryDelays: [0, 1000, 3000, 5000],
        metadata: blobUploadState.metadata,
        onError: error => {
          blobUploadState.errorCallback(blobUploadState.file, error);
          console.log('Failed: ' + blobUploadState.file.name + error);
        },
        onProgress: (bytesUploaded, bytesTotal) => {
          const percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2);
          blobUploadState.progressCallback(blobUploadState.file, bytesUploaded, bytesTotal, percentage);

          console.log(
            'file: ' + blobUploadState.file.name + ' of :',
            bytesUploaded,
            bytesTotal,
            percentage + '%'
          );
        },
        onSuccess: () => {
          console.log('Download' + blobUploadState.file.name + 'from' + upload.url + ' ' + blobUploadState.file.name);
          blobUploadState.successCallback(blobUploadState.file);
          // success();
          console.log('Videos uploaded successfully');

        },
      });
      return upload;
  }

  
}