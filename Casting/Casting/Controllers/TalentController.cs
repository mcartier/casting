﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Casting.Web.Controllers
{
    public class TalentController : Controller
    {
        // GET: Talent
        public ActionResult Index()
        {
            return View();
        }

        // GET: Talent/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Talent/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Talent/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Talent/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Talent/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Talent/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Talent/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
