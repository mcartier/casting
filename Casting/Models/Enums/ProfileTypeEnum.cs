﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum ProfileTypeEnum
    {
        Actor = 1,
        VoiceActor = 2
    }
}
