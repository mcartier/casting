﻿using Casting.Model.Entities.CastingCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Locations
{
    public class Country:Logger
    {
        public Country()
        {
            CastingCalls = new List<CastingCallCountryReference>();
            Cities = new List<City>();
            Regions = new List<Region>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Region> Regions { get; set; }
        public ICollection<City> Cities { get; set; }
        public string ShortName { get; set; }
        public ICollection<CastingCallCountryReference> CastingCalls { get; set; }

    }
}
