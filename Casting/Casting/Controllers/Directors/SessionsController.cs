﻿using Casting.BLL.CastingSessions;
using Casting.BLL.CastingSessions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Sessions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingSessions;
using System.Data.Entity.Infrastructure;
using System.Web.Http.Results;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using WebGrease.Css.Extensions;
using Casting.DAL;
using System.Data.Entity;
using Casting.Model.Entities.AttributeReferences;
using Casting.Web.Models;
using Casting.BLL.AttributeReferences.Interfaces;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingSessions")]
   // [Authorize]
    public class CastingSessionsController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingSessionService _castingSessionService;
        private ICastingSessionRoleService _castingSessionRoleService;
        private IUnionAttributeReferenceService _unionService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public CastingSessionsController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, IUnionAttributeReferenceService unionService,
            ICastingSessionService castingSessionService, ICastingSessionRoleService castingSessionRoleService, IProductionService productionService, ICastingContext castingContext) : base(castingContext)
        {
            _productionService = productionService;
            _castingSessionService = castingSessionService;
            _castingSessionRoleService = castingSessionRoleService;
            _unionService = unionService;
            _metadataService = metadataService;
            _userManager = userManager;
        }


        // GET: api/CastingSessions
        [ResponseType(typeof(IEnumerable<CastingSession>))]
        public async Task<IHttpActionResult> GetCastingSessions(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessions =
                await _castingSessionService.GetCastingSessionsAsync(CastingSessionParameterIncludeInQueryEnum.CastingSessions, pageIndex, pageSize);

            return Ok(castingSessions);
        }

        /*
        #### replaced by method in the production controller
        
        // GET: api/CastingSessions
        [System.Web.Http.ActionName("production")]
        [Route("production/{productionId}")]
        [ResponseType(typeof(IEnumerable<CastingSession>))]
        public async Task<IHttpActionResult> GetCastingSessionsForProduction(int productionId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
           
            var castingSessions =
                await _castingSessionService.GetCastingSessionsByProductionIdWithRolesPersonasAsync(productionId);

            return Ok(castingSessions);
        }
    */
        // GET: api/CastingSessions/5
        [ResponseType(typeof(CastingSession))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var castingSession =
                await _castingSessionService.GetCastingSessionByIdAsync(id);

            return Ok(castingSession);
        }

        // GET: api/CastingSessions/[id}/Roles
        [Route("{id}/roles")]
        [ResponseType(typeof(IEnumerable<CastingSessionRole>))]
        public async Task<IHttpActionResult> GetCastingSessionRoles(int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingSessionRoles =
                await _castingSessionRoleService.GetCastingSessionRolesByCastingSessionIdWithPersonasAndAssetsAsync(Id);

            return Ok(castingSessionRoles);
        }



        // POST: api/CastingSessions
        [ResponseType(typeof(CastingSession))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewCastingSessionBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
             /* var castingSession = new CastingSession()
            {
                DirectorId = user.BaseUserId,
                Name = name
            };*/
             var production = await _productionService.GetProductionByIdWithRolesAsync(value.ProductionId);
             if (production == null)
             {
                return BadRequest("Production with Id " + value.ProductionId + " does not exist");
             }

            // foreach (var roleId in value.Roles)
            // {
            //     if (!production.Roles.Any(r => r.Id == roleId))
            //     {
            //         return BadRequest("Role with Id " + roleId + " was not found in the given production");
           //      }
           //  }

             var castingSession = new CastingSession()
            {
                ProductionId = value.ProductionId,
                CastingSessionType = value.CastingSessionType,
               // ViewTypeId = value.ViewTypeId,
                TalentSourceLocation = value.TalentSourceLocation,
                AcceptingSubmissions = value.AcceptingSubmissions,
                Instructions = value.Instructions,
                Expires = value.Expires,
                LocationType = value.LocationType,
                PostedDate = value.PostedDate,
                RequiresHeadshot = value.RequiresHeadshot,
                HeadshotMaxCount = value.HeadshotMaxCount,
                RequiresVideo = value.RequiresVideo,
                VideoMaxCount = value.VideoMaxCount,
                RequiresAudio = value.RequiresAudio,
                AudioMaxCount = value.AudioMaxCount,
                RequiresCoverLetter = value.RequiresCoverLetter,

             };
             foreach (var unionId in value.Unions)
             {
                 var union = await _metadataService.GetUnionByIdAsync(unionId);
                 if (union != null)
                 {
                     castingSession.Unions.Add(new CastingSessionUnionReference() { Union = union });
                 }
             }

           // foreach (var roleId in value.Roles)
            //{
            // castingSession.Roles.Add(new CastingSessionRole()
           //  {
            //     RoleId = roleId
           //  });
         //   }

            var ret = await _castingSessionService.AddNewCastingSessionAsync(castingSession);
            return CreatedAtRoute("DefaultApi", new { id = castingSession.Id }, castingSession);
        }

       
        // PUT: api/CastingSessions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateCastingSessionBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSession = await _castingSessionService.GetCastingSessionByIdAsync(id);
            if (castingSession == null)
            {
                return NotFound();
            }
            if (castingSession.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }

            castingSession.ProductionId = value.ProductionId;
            castingSession.CastingSessionType = value.CastingSessionType;
           // castingSession.ViewTypeId = value.ViewTypeId;
            castingSession.TalentSourceLocation = value.TalentSourceLocation;
            castingSession.AcceptingSubmissions = value.AcceptingSubmissions;
            castingSession.Instructions = value.Instructions;
            castingSession.Expires = value.Expires;
            castingSession.LocationType = value.LocationType;
            castingSession.PostedDate = value.PostedDate;
            castingSession.RequiresHeadshot = value.RequiresHeadshot;
            castingSession.HeadshotMaxCount = value.HeadshotMaxCount;
            castingSession.RequiresVideo = value.RequiresVideo;
            castingSession.VideoMaxCount = value.VideoMaxCount;
            castingSession.RequiresAudio = value.RequiresAudio;
            castingSession.AudioMaxCount = value.AudioMaxCount;
            castingSession.RequiresCoverLetter = value.RequiresCoverLetter;
            foreach (var stereoType in castingSession.Unions.OfType<CastingSessionUnionReference>().ToList())
            {
                if (!value.Unions.Any(e => e == stereoType.Id))
                {
                    _castingContext.SetState(stereoType, EntityState.Deleted);
                    castingSession.Unions.Remove(stereoType);
                }
            }
            foreach (var unionId in value.Unions)
            {
                if (!castingSession.Unions.Any(s => s.Id == unionId))
                {
                    var union = await _metadataService.GetUnionByIdAsync(unionId);
                    if (union != null)
                    {
                        castingSession.Unions.Add(new CastingSessionUnionReference() { Union = union });
                    }
                }
            }

            try
            {
                await _castingSessionService.UpdateCastingSessionAsync(castingSession);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/CastingSessions/5
        [ResponseType(typeof(CastingSession))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingSession = await _castingSessionService.GetCastingSessionByIdAsync(id);
            if (castingSession == null)
            {
                return NotFound();
            }
            if (castingSession.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }

            await _castingSessionService.DeleteCastingSession(castingSession);

            return Ok(castingSession);
        }

        #region CastingSession attribute references
        // GET: api/castingSessions/[id}/unions
        [Route("{id}/unions")]
        [ResponseType(typeof(IEnumerable<CastingSessionUnionReference>))]
        public async Task<IHttpActionResult> GetCastingSessionUnions(int Id)
        {

            var castingSessionUnionss =
                await _unionService.GetCastingSessionUnionReferencesForParentAsync(Id, true);

            return Ok(castingSessionUnionss);
        }

        // POST: api/castingSessions/[id}/unions
        [Route("{id}/unions")]
        [ResponseType(typeof(CastingSessionUnionReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostCastingSessionUnion(int Id, CastingSessionAttributeReferenceBindingModel union)
        {
            var attribureReference = new CastingSessionUnionReference() { CastingSessionId = Id, UnionId = union.AttributeId };
            var ret =
                await _unionService.AddNewCastingSessionUnionAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/castingSessions/{id}/unions/{unionReferenceId}
        [Route("{id}/unions/{unionReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutCastingSessionUnion(int id, int unionReferenceId, CastingSessionAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var union = await _unionService.GetCastingSessionUnionReferenceByIdAsync(unionReferenceId);
            if (union == null || union.CastingSessionId != id)
            {
                return NotFound();
            }

            //make modifications to union object here from values of the CastingSessionAttributeReferenceBindingModel
            try
            {
                await _unionService.UpdateCastingSessionUnionAttributeReferenceAsync(union);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/castingSessions/[id}/unions/{unionId}
        [Route("{id}/unions/{unionReferenceId}")]
        [ResponseType(typeof(CastingSessionUnionReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCastingSessionUnion(int Id, int unionReferenceId)
        {

            var union = await _unionService.GetCastingSessionUnionReferenceByIdAsync(unionReferenceId);
            if (union == null || union.CastingSessionId != Id)
            {
                return NotFound();
            }


            await _unionService.DeleteCastingSessionUnionAttributeReferenceAsync(union);

            return Ok(union);

        }


        #endregion
    }
}
