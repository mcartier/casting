﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;


namespace Casting.DAL.Configurations
{

    public class CastingSessionConfiguration : EntityTypeConfiguration<CastingSession>
    {
        public CastingSessionConfiguration()
        {
            HasKey(x => x.Id);
            HasMany(s => s.Folders)
                .WithRequired(c => c.CastingSession)
                .HasForeignKey(k => k.CastingSessionId);
               HasMany(s => s.Roles)
                   .WithRequired(c => c.CastingSession)
                .HasForeignKey(k => k.CastingSessionId);
                 HasRequired(p => p.Production)
                    .WithMany(c => c.CastingSessions)
                     .HasForeignKey(k => k.ProductionId)
                        .WillCascadeOnDelete(false);
                 HasMany(c => c.CastingCalls)
                     .WithRequired(s => s.CastingSession)
                     .HasForeignKey(k=>k.CastingSessionId);
        }
    }
    public class CastingSessionRoleConfiguration : EntityTypeConfiguration<CastingSessionRole>
    {
        public CastingSessionRoleConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(r => r.Role)
                .WithMany(c => c.CastingSessionRoles)
                .HasForeignKey(k => k.RoleId)
                .WillCascadeOnDelete(false);
            HasRequired(p => p.CastingSession)
                .WithMany(r => r.Roles)
                .HasForeignKey(k=>k.CastingSessionId);
            HasMany(f=>f.FolderRoles)
                .WithRequired(r=>r.CastingSessionRole)
                .HasForeignKey(k=>k.CastingSessionRoleId)
                .WillCascadeOnDelete(false);
            HasMany(r => r.Personas)
                .WithRequired(r => r.CastingSessionRole)
                .HasForeignKey(k => k.CastingSessionRoleId);
        }
    }
    public class CastingSessionFolderConfiguration : EntityTypeConfiguration<CastingSessionFolder>
    {
        public CastingSessionFolderConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.CastingSession)
                .WithMany(f => f.Folders)
                .HasForeignKey(k => k.CastingSessionId);
               HasMany(s => s.Roles)
                   .WithRequired(c => c.CastingSessionFolder)
                .HasForeignKey(k => k.CastingSessionFolderId);


        }
    }
    public class CastingSessionFolderRoleConfiguration : EntityTypeConfiguration<CastingSessionFolderRole>
    {
        public CastingSessionFolderRoleConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(r => r.CastingSessionRole)
                .WithMany(c => c.FolderRoles)
                .HasForeignKey(k => k.CastingSessionRoleId)
                .WillCascadeOnDelete(false);
            HasRequired(p => p.CastingSessionFolder)
                .WithMany(r => r.Roles)
                .HasForeignKey(k=>k.CastingSessionFolderId);
            HasRequired(r => r.Role)
                .WithMany(c => c.CastingSessionFolderRoles)
                .HasForeignKey(k => k.RoleId)
                .WillCascadeOnDelete(false);
            HasMany(r => r.Personas)
                .WithRequired(r => r.CastingSessionFolderRole)
                .HasForeignKey(k => k.CastingSessionFolderRoleId);

        }
    }
}
