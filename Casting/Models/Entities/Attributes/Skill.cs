﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Profiles;

namespace Casting.Model.Entities.Attributes
{
    public class Skill:Logger
    {
        public int Id { get; set; }
        public int SkillCategoryId { get; set; }
        public SkillCategory SkillCategory { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
    }
}
