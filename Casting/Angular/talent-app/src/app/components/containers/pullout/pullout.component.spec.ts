import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PulloutComponent } from './pullout.component';

describe('PulloutComponent', () => {
  let component: PulloutComponent;
  let fixture: ComponentFixture<PulloutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PulloutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PulloutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
