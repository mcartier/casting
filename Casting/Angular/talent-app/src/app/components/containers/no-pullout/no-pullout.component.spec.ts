import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoPulloutComponent } from './no-pullout.component';

describe('NoPulloutComponent', () => {
  let component: NoPulloutComponent;
  let fixture: ComponentFixture<NoPulloutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoPulloutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoPulloutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
