﻿import { Observable, of, BehaviorSubject, Subject, throwError } from 'rxjs';
import { TusHelper, ITusUploadObject } from '../utilities/tus.helper';
import { blobFileHelper } from '../utilities/blobFileHelper';
import * as tus from 'tus-js-client';



export enum uploadDestinationEnum {
  Talent = 1,
  Basket = 3
}
export class CreatedBlob {
    constructor(_id: any, _blob: Blob, _uploadUrl: string, _headers: any, _metadata: any) {
        this.blob = _blob;
        this.id = _id;
        this.uploadState = new BlobUploadState(blobFileHelper.blobToFile(_blob, "video.webm"), _uploadUrl, _headers, _metadata);

    }

    id: any;
    blob: Blob;
    fileSize: number;
    sessionName: string;
    roleName: string;
    talentName: string;
    talentId: number;
    uploadState: BlobUploadState
    createdDate: Date;
}

export class StudioTalentBlob implements ITusUploadObject {
    constructor(_id: any, _blob: Blob, _talentId: number, _sessionId: number, _roleId: number, _talentName: string = "studio_Talent", _roleName: string = "Role", _sessionName: string = "session") {
        this.uploadUrl = "https://cast.com/castingbasketvideoupload";
        this.file = blobFileHelper.blobToFile(_blob, _talentName + '_' + _roleName + '.webm');
        this.blob = _blob;
        this.totalSize = this.file.size;
        this.file = this.file;
        this.talentId = _talentId;
        this.roleId = _roleId;
        this.sessionId = _sessionId;
        this.talentName = _talentName;
        this.roleName = _roleName;
        this.sessionName = _sessionName;
        this.id = _id;
        
        this.obs = new Subject<this>();
    }
    uploadDestination: uploadDestinationEnum = uploadDestinationEnum.Talent;
    id: any;
    blob: Blob;
    uploadedFileId: string;
    sessionName: string;
    sessionId: number;
    roleName: string;
    roleId: number;
    talentName: string;
    talentId: number;
    upload: tus.Upload;
    obs: Subject<StudioTalentBlob>;
    metadata: any;
    uploadUrl: string;
    file: File;
    isUploading: boolean = false;
    totalSize: number;
    uploadedSize: number = 0;
    percentageUploaded: number = 0;
    isComplete: boolean = false;
    isSuccess: boolean = false;
    isError: boolean = false;
    error: any;
    buildMetadata() {
      this.metadata = {
        name: this.talentName + '_' + this.roleName  + '.webm',
        type: "webm/video",
        contentType: "video/*",
        talentId: this.talentId,
        roleId: this.roleId,
        sessionId: this.sessionId
      }
    };
    sartUpload() {
        if (this.upload) {
            this.isUploading = true;
            this.upload.start();
            this.obs.next(this);
        }
    };
    successCallback(file) {
        this.isUploading = false;
        this.isComplete = true;
        this.isSuccess = true;
        if (this.upload) {
          //this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
        this.obs.complete();
     };
    errorCallback(file, error) {
        this.isUploading = false;
        this.isComplete = true;
        this.isError = true;
        this.error = error;
        if (this.upload) {
          this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
    };
    progressCallback(file, bytesUploaded, bytesTotal, percentage) {
        //this.isUploading = true;
        this.uploadedSize = bytesUploaded;
        this.totalSize = bytesTotal;
        this.percentageUploaded = percentage;
        this.obs.next(this);
    };
}

export class ProductionSideFile implements ITusUploadObject {
    constructor(_id: any, _file: File,_sideId:number, _productionId: number,_sideName:string = "side name", _productionName: string = "production") {
        this.uploadUrl = "//cast.com/castingsideupload";
        this.file = _file;
        this.totalSize = this.file.size;
        this.productionId = _productionId;
        this.productionName = _productionName;
        this.sideId = _sideId;
        this.sideName = _sideName;
        this.id = _id;

        this.obs = new Subject<this>();
    }
    uploadDestination: uploadDestinationEnum = uploadDestinationEnum.Talent;
    id: any;
    blob: Blob;
    productionName: string;
    productionId: number;
    sideName: string;
    uploadedFileId: string;
    sideId:number;
    upload: tus.Upload;
    obs: Subject<ProductionSideFile>;
    metadata: any;
    uploadUrl: string;
    file: File;
    isUploading: boolean = false;
    totalSize: number;
    uploadedSize: number = 0;
    percentageUploaded: number = 0;
    isComplete: boolean = false;
    isSuccess: boolean = false;
    isError: boolean = false;
    error: any;
    buildMetadata() {
      this.metadata = {
          name: this.productionName + '_' + this.sideName + '.pdf',
          productionId: this.productionId,
          sideName: "my side",
           realName: this.file.name,
        contentType: "application/pdf",
        sideId: this.sideId,
       type: this.file.type, //"webm/video",

    }
    };
    sartUpload() {
        if (this.upload) {
            this.isUploading = true;
            this.upload.start();
            this.obs.next(this);
        }
    };
    successCallback(file) {
        this.isUploading = false;
        this.isComplete = true;
        this.isSuccess = true;
        if (this.upload) {
            //this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
        this.obs.complete();
    };
    errorCallback(file, error) {
        this.isUploading = false;
        this.isComplete = true;
        this.isError = true;
        this.error = error;
        if (this.upload) {
            this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
    };
    progressCallback(file, bytesUploaded, bytesTotal, percentage) {
        //this.isUploading = true;
        this.uploadedSize = bytesUploaded;
        this.totalSize = bytesTotal;
        this.percentageUploaded = percentage;
        this.obs.next(this);
    };
}


export class BlobUploadState implements ITusUploadObject {
    constructor(private _file: File, _uploadUrl: string, _headers: any, _metadata: any) {
        this.totalSize = _file.size;
        this.file = _file;
        this.uploadUrl = _uploadUrl;
        this.metadata = _metadata;
        this.headers = _headers;
        this.obs = new Subject<this>();
    }
    obs: Subject<ITusUploadObject>;
    id: any;
    headers: any;
    metadata: any;
    uploadUrl: string;
    file: File;
    uploadedFileId: string;
        upload: tus.Upload;
    isUploading: boolean;
    totalSize: number;

    buildMetadata() {
    };

    uploadedSize: number = 0;
    percentageUploaded: number = 0;
    isComplete: boolean;
    isSuccess: boolean;
    isError: boolean;
    error: any;
    sartUpload() {
        this.isUploading = true;
        this.obs.next(this);
    };
    successCallback(file) {
        this.isUploading = false;
        this.isComplete = true;
        this.isSuccess = true;
        this.obs.next(this);
    };
    errorCallback(file, error) {
        this.isUploading = false;
        this.isComplete = true;
        this.isError = true;
        this.error = error;
        this.obs.next(this);
    };
    progressCallback(file, bytesUploaded, bytesTotal, percentage) {
        this.isUploading = true;
        this.uploadedSize = bytesUploaded;
        this.totalSize = bytesTotal;
        this.percentageUploaded = percentage;
        this.obs.next(this);
    };
}