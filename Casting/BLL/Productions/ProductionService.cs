﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class ProductionService : BaseService, IProductionService
    {
        private readonly IProductionAsyncRepository _productionRepository;

        public ProductionService(IProductionAsyncRepository productionAsyncRepository)
        {
            _productionRepository = productionAsyncRepository;
        }

        public async Task<Production> GetProductionByIdAsync(int productionId)
        {
            return await _productionRepository.GetProductionByIdAsync(false, false, false, false, false, productionId);
        }
        public async Task<Production> GetProductionByIdWithRolesAsync(int productionId)
        {
            return await _productionRepository.GetProductionByIdAsync(true, true, false, false, false, productionId);
        }
        public async Task<Production> GetProductionByIdWithRolesCastingCallsSessionsAsync(int productionId)
        {
            return await _productionRepository.GetProductionByIdAsync(true, true, true, true, true, productionId);
        }

        public async Task<IEnumerable<Production>> GetProductionsByDirectorIdAsync(int directorId, int pageIndex = 0, int pageSize = 12)
        {
            return await _productionRepository.GetProductionsByDirectorIdAsync(false, false, false, false, false, directorId, pageIndex, pageSize);
        }

        public async Task<IEnumerable<Production>> GetCompleteProductionsByDirectorIdAsync(int directorId, int pageIndex = 0, int pageSize = 12)
        {
            return await _productionRepository.GetProductionsByDirectorIdAsync(true, true, true, true, true, directorId, pageIndex, pageSize);
        }

        public async Task<int> AddNewProductionAsync(Production production)
        {
            return await _productionRepository.AddNewProductionAsync(production);
        }

        public async Task<int> UpdateProductionAsync(Production production)
        {
            return await _productionRepository.UpdateProductionAsync(production);
        }
        public async Task<int> DeleteProduction(Production production)
        {
            return await _productionRepository.DeleteProductionAsync(production);
        }
    }
}
