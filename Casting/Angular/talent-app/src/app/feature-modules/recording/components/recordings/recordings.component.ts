import { Component, OnDestroy, AfterViewInit, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { VideoBlobService } from '../../../../services/video-blob.service';
import {StudioTalentBlob } from '../../../../models/blobs';
import { BytesPipePipe } from '../../../../pipes/bytes-pipe.pipe';
@Component({
  selector: 'app-recordings',
  templateUrl: './recordings.component.html',
    styleUrls: ['./recordings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RecordingsComponent implements OnInit, OnDestroy {

    constructor(private videoBlobService: VideoBlobService, private changeDetectorRef: ChangeDetectorRef) { }

  private blobsSubscription: Subscription;
    blobs:StudioTalentBlob[];
    ngOnInit() {
      this.blobsSubscription = this.videoBlobService.subscribeToBlobsObs()
          .subscribe(
            blobs => {
                this.blobs = blobs;
                this.blobs = this.blobs;
                this.changeDetectorRef.detectChanges();
            });
       this.changeDetectorRef.detectChanges();

  }
    private processBlobs(blobs: StudioTalentBlob[]) {
      this.blobs = blobs;
     
    }
    private trackByFn(index, item) {
      //debugger;
        return item.id; // or item.id
    }
    ngOnDestroy() {
        this.blobsSubscription.unsubscribe();
    }

}
