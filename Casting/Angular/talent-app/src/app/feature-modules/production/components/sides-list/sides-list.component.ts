import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { Observable, Subject, Subscription } from 'rxjs';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';

@Component({
  selector: 'app-sides-list',
  templateUrl: './sides-list.component.html',
  styleUrls: ['./sides-list.component.scss']
})
export class SidesListComponent implements OnInit {

  constructor(
    private productionExtraService: ProductionExtraService,
    private messageDispatchService: MessageDispatchService
  ) { }

  private newSideSubscription: Subscription;
  private deleteSideSubscription: Subscription;
  private editSideSubscription: Subscription;

  @Input() sides: Side[];

  ngOnInit() {
    this.editSideSubscription = this.productionExtraService.subscribeToEditSideObs()
      .subscribe(
        (detail: Side) => {
        });

    this.newSideSubscription = this.productionExtraService.subscribeToCreateSideObs()
      .subscribe(
        (detail: Side) => {
        });

    this.deleteSideSubscription = this.productionExtraService.subscribeToDeleteSideObs()
      .subscribe(
        (detail: Side) => {
          if (detail) {
          }
        });

  }

  ngOnDestroy() {

    if (this.editSideSubscription) {
      this.editSideSubscription.unsubscribe();

    }
    if (this.newSideSubscription) {
      this.newSideSubscription.unsubscribe();

    }
    if (this.deleteSideSubscription) {
      this.deleteSideSubscription.unsubscribe();

    }
  }
}
