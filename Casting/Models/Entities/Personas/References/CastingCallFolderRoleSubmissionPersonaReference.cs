﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
   public class CastingCallFolderRoleSubmissionPersonaReference: PersonaReference
    {
        public CastingCallFolderRoleSubmissionPersonaReference()
        {
            base.PersonaReferenceType = PersonaReferenceTypeEnum.CastingCallFolderRoleSubmission;
        }
        public int CastingCallFolderRoleId { get; set; }
        public CastingCallFolderRole CastingCallFolderRole { get; set; }

    }
}
