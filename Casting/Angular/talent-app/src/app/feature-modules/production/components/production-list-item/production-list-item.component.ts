import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Subscription } from 'rxjs';
import { MetadataService } from '../../../../services/metadata.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { ProductionService } from '../../../../services/production.service';
import { ProductionComponentService } from '../../services/production-component.service';
@Component({
  selector: 'app-production-list-item',
  templateUrl: './production-list-item.component.html',
    styleUrls: ['./production-list-item.component.scss'],
  host: { 'class': 'p-col' }
})
export class ProductionListItemComponent implements OnInit {

  constructor() { }
  @Input() production: Production;

    ngOnInit() {
    }

}
