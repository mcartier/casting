import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { Production, ShootDetail, AuditionTypeEnum,AuditionDetail, PayDetail, Side } from '../../../../models/production.models';
import { ProductionDialogService } from '../../services/production-dialog.service';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MessageService } from 'primeng/api';
import { ConfirmDecisionSubscriptionService } from '../../../../services/confirm-decision-subscription.service';


@Component({
  selector: 'app-audition-details-list-item',
  templateUrl: './audition-details-list-item.component.html',
  styleUrls: ['./audition-details-list-item.component.scss']
})
export class AuditionDetailsListItemComponent implements OnInit {

 
  constructor(
    private productionDialogService: ProductionDialogService,
    private productionExtraService: ProductionExtraService,
    private messageDispatchService: MessageDispatchService,
    private messageService: MessageService,
    private decisionDispatcher: ConfirmDecisionSubscriptionService
  ) { }

  private decisionDispatch: Subscription;
  @Input() detail: AuditionDetail;
  auditionTypeEnum = AuditionTypeEnum;
  ngOnInit() {
  }

  ngOnDestroy() {

    if (this.decisionDispatch) {
      this.decisionDispatch.unsubscribe();

    }

  }
  private viewDetails() {
    this.productionDialogService.showAuditionDetailsDialog(this.detail);

  }
  private deleteDetails() {
    this.decisionDispatcher.poseConfirmDecision('Delete audition detail ' + this.detail.name + '?', 'Confirm to proceed');
    this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
      .subscribe(
        (decision: boolean) => {
          this.decisionDispatch.unsubscribe();

          if (decision) {
            this.productionExtraService.deleteAuditionDetail(this.detail.id, this.detail.productionId).subscribe(
              (shootDetail: AuditionDetail) => {
                //this.shootDetail = shootDetail;
                this.messageDispatchService.dispatchSuccesMessage("Audition detail '" + this.detail.name + "' was deleted successfully",
                  "Audition Detail Deleted");

              },
              (error) => {
                this.messageDispatchService.dispatchErrorMessage(
                  "Error saving audition detail '" + this.detail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                  "Audition Detail Save Error");
              });

          }
        });


  }
}
