import { Component, OnInit, OnDestroy, Inject, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ProductionDialogService, AuditionDetailDialogObject } from '../../services/production-dialog.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { AuditionDetail, ShootDetail, PayDetail, Side, AuditionTypeEnum, PayPackageTypeEnum } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { Dialog } from 'primeng/dialog';
import { SelectItem } from 'primeng/api';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { RadioButton } from 'primeng/radiobutton';


@Component({
    selector: 'app-audition-details-dialog',
    templateUrl: './audition-details-dialog.component.html',
    styleUrls: ['./audition-details-dialog.component.scss']
})
export class AuditionDetailsDialogComponent implements OnInit, OnDestroy, AfterViewInit {

    constructor(private productionDialogService: ProductionDialogService,
        private productionExtraService: ProductionExtraService,
        private _formBuilder: FormBuilder,
        public mediaMatcher: MediaMatcher,
        private messageDispatchService: MessageDispatchService) {
    }
    @Input() auditionDetail: AuditionDetail;
    @Input() productionId: number;
    @ViewChild('formDirective', {static: false}) private formDirective: NgForm;
    @ViewChild('name', {static: false}) nameField: ElementRef;
    @ViewChild('location', {static: false}) locationField: ElementRef;
    @ViewChild('locationDetail', {static: false}) locationDetailField: ElementRef;
    @ViewChild('startDate', {static: false}) startDateField: any;
    @ViewChild('endDate', {static: false}) endDateField: any;
    @ViewChild('detaildialog', {static: false}) detaildialog: Dialog;
    @ViewChild('auditionType', {static: false}) auditionTypeField: RadioButton;
   
        
    bigSizeMatcher: MediaQueryList;
    smallSizeMatcher: MediaQueryList;
    isSmallDevice: boolean = false;

    private messages: any[] = [];
    private saveIsDisabled: boolean = false;
    private cancelIsDisabled: boolean = false;

    private isOneDayOnly:boolean = false;
    private datesToBeDecidedLater:boolean = false;


    isVisible: boolean = false;
    private dialogVisibleStateSubscription: Subscription;
    private basicDialogInfo: FormGroup;
    minDate: Date;
    maxDate: Date;
    invalidDates: Array<Date>;
   
    auditionTypeEnum = AuditionTypeEnum;
    isInPersonAudition: boolean = true;
    ngOnInit() {
        this.isSmallDevice = window.innerWidth < 768;

        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));

        let today = new Date();
        let month = today.getMonth();
        let year = today.getFullYear();
        this.minDate = new Date();
        this.minDate.setDate(today.getDate() + 1);
        this.maxDate = new Date();
        this.maxDate.setDate(today.getDate() + 1);

        this.basicDialogInfo = this._formBuilder.group({
            location: ['', Validators.required],
            locationDetail: [''],
            isOneDayOnly: [false],
            details: [''],
            instructions: [''],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],
            name: ['', Validators.required],
            auditionType: [0, Validators.required],
            onlyThoseSelectedWillParticipate: [false],
            datesToBeDecidedLater: [false],
            datesAreTemptative: [false]
        },
            { updateOn: 'blur' });
       // this.basicDialogInfo.patchValue({
       //     isOneDayOnly: false,
      //  });
      //  this.basicDialogInfo.controls['endDate'].enable();


        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToAuditionDetailsDialogShowStateObs()
            .subscribe(
                dialogObject => {
                    this.clearMessages();
                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    if (dialogObject.isVisible && dialogObject.auditionDetail != null) {
                        this.auditionDetail = dialogObject.auditionDetail;
                        this.basicDialogInfo.patchValue({
                          location: this.auditionDetail.location,
                          locationDetail: this.auditionDetail.locationDetail,
                          isOneDayOnly: this.auditionDetail.isOneDayOnly,
                          details: this.auditionDetail.details,
                          instructions: this.auditionDetail.instructions,
                          startDate: new Date(this.auditionDetail.startDate),
                          endDate: new Date(this.auditionDetail.endDate),
                          name: this.auditionDetail.name,
                          auditionType: this.auditionDetail.auditionTypeEnum,
                          onlyThoseSelectedWillParticipate: this.auditionDetail.onlyThoseSelectedWillParticipate,
                          datesToBeDecidedLater: this.auditionDetail.datesToBeDecidedLater,
                          datesAreTemptative: this.auditionDetail.datesAreTemptative

                        });
                        this.auditionTypeChange(this.auditionDetail.auditionTypeEnum);
                          //check if dates disabled for later
                         if (this.auditionDetail.auditionTypeEnum != AuditionTypeEnum.TapedSubmissionsOnly) {
                          if (this.auditionDetail.datesToBeDecidedLater) {
                            this.dateDecidionChange(this.auditionDetail.datesToBeDecidedLater);

                          } else {
                            //check if one day only
                            if (this.auditionDetail.isOneDayOnly) {
                              this.oneDayAuditionChange(this.auditionDetail.isOneDayOnly);
                            } else {
                              this.basicDialogInfo.controls['endDate'].enable();
                              this.basicDialogInfo.controls['startDate'].enable();
                            }

                          }
                        }
                        
                      
                    } else {
                      this.auditionDetail = null;
                      this.basicDialogInfo.patchValue({
                        location: '',
                        locationDetail: '',
                        isOneDayOnly: false,
                        details: '',
                        instructions: '',
                        startDate: '',
                        endDate: '',
                        name: '',
                        auditionType: AuditionTypeEnum.InPersonAuditionOnly,
                        onlyThoseSelectedWillParticipate: false,
                        datesToBeDecidedLater: false,
                        datesAreTemptative: false

                      });
                      this.auditionTypeChange(AuditionTypeEnum.InPersonAuditionOnly);
                      this.dateDecidionChange(false);
                      this.oneDayAuditionChange(false);

                        //this.basicDialogInfo.controls['endDate'].enable();
                                    
                    }
                    this.isVisible = dialogObject.isVisible;
                    // this.detaildialog.maximized = true;
                    //this.detaildialog.toggleMaximize();
                    this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                    this.nameField.nativeElement.focus();

                });


    }

    ngAfterViewInit() {


    }

    ngOnDestroy() {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();

        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);

    }
    private clearMessages() {
        this.messages = [];
    }

    mediaQueryBigChangeListener(event) {

        if (event.matches) {
            this.isSmallDevice = false;
        }
    }
    mediaQuerySmallChangeListener(event) {
        if (event.matches) {

            this.isSmallDevice = true;
        }
    }
    private selectStartDate(date: any) {
        this.basicDialogInfo.patchValue({
            startDate: date,
        });
    }
    private selectEndDate(date: any) {
        this.basicDialogInfo.patchValue({
            endDate: date,
        });
    }
  
    private auditionTypeOptionClick(auditionType: AuditionTypeEnum) {
        this.auditionTypeChange(auditionType);

    }

    private auditionTypeChange(auditionType: AuditionTypeEnum) {
        var isInPersonAudition: boolean;
        switch (auditionType) {
            case AuditionTypeEnum.InPersonAuditionOnly:
            case AuditionTypeEnum.TapedSubmissionAndInPersonAudition:
            case AuditionTypeEnum.TapedSubmissionThenInPersonAudition:
              isInPersonAudition = true;
                break;
            case AuditionTypeEnum.TapedSubmissionsOnly:
              isInPersonAudition = false;
                break;
        }
        if (this.isInPersonAudition != isInPersonAudition) {
            this.isInPersonAudition = isInPersonAudition;
            if (this.isInPersonAudition) {
                this.basicDialogInfo.controls['endDate'].enable();
                this.basicDialogInfo.controls['startDate'].enable();
                this.basicDialogInfo.controls['location'].enable();
                
            } else {
                
                this.basicDialogInfo.controls['endDate'].disable();
                this.basicDialogInfo.controls['startDate'].disable();
                this.basicDialogInfo.controls['location'].disable();
                this.basicDialogInfo.patchValue({
                  location: '',
                  locationDetail: '',
                  startDate: '',
                  endDate: '',
                  datesToBeDecidedLater: false,
                  datesAreTemptative: false

                });
               
                // this.basicDialogInfo.controls['location'].setValidators([]);
                // this.basicDialogInfo.controls['location'].updateValueAndValidity();
                //   this.basicDialogInfo.get('title').setValidators([]); // or clearValidators()
                //  this.basicDialogInfo.get('title').updateValueAndValidity();
            }
        }

    }
    private oneDayAuditionChange(value) {
      this.isOneDayOnly = value;
        if (value) {
            this.basicDialogInfo.controls['endDate'].disable();
           } else {
          if (!this.datesToBeDecidedLater) {
            this.basicDialogInfo.controls['endDate'].enable();
          }
        }
        this.basicDialogInfo.patchValue({
          endDate: '',
        });
    }
    private clickOneDaySwitch(selection: any) {
        this.oneDayAuditionChange(selection.checked);
    }
    private dateDecidionChange(value) {
      this.datesToBeDecidedLater = value;
        if (value) {
            this.basicDialogInfo.controls['endDate'].disable();
             this.basicDialogInfo.controls['startDate'].disable();
               } else {
            this.basicDialogInfo.controls['endDate'].enable();
            if (!this.isOneDayOnly) {
              this.basicDialogInfo.controls['startDate'].enable();
            }
        }
        this.basicDialogInfo.patchValue({
          endDate: '',
        });
        this.basicDialogInfo.patchValue({
          startDate: '',
        });


    }
    private clickDateDecisionSwitch(selection: any) {
      this.dateDecidionChange(selection.checked);

    }
    private onDialogShow(event: any, dialog: Dialog) {
        if (this.isSmallDevice) {
            setTimeout(() => {
                dialog.maximize();
            },
                0);
        }
    }
    private onDialogHide(event: any, dialog: Dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();

    }
    private cancelClick() {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();

    }


    private resetDialog() {
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.messages = [];
        this.isInPersonAudition = true;
    }

    private saveClick() {
        this.clearMessages();
        if (this.basicDialogInfo.valid) {
            this.saveIsDisabled = true;
            this.cancelIsDisabled = true;

            let tauditionDetail: AuditionDetail = new AuditionDetail;
            tauditionDetail.productionId = this.productionId;
            tauditionDetail.name = this.basicDialogInfo.get('name').value;
            tauditionDetail.auditionTypeEnum = this.basicDialogInfo.get('auditionType').value;
            tauditionDetail.location = this.basicDialogInfo.get('location').value;
            tauditionDetail.locationDetail = this.basicDialogInfo.get('locationDetail').value;
            tauditionDetail.details = this.basicDialogInfo.get('details').value;
            tauditionDetail.instructions = this.basicDialogInfo.get('instructions').value;
            tauditionDetail.startDate = this.basicDialogInfo.get('startDate').value;
            tauditionDetail.endDate = this.basicDialogInfo.get('endDate').value;
            tauditionDetail.isOneDayOnly = this.basicDialogInfo.get('isOneDayOnly').value;
            tauditionDetail.onlyThoseSelectedWillParticipate = this.basicDialogInfo.get('onlyThoseSelectedWillParticipate').value;
            tauditionDetail.datesToBeDecidedLater = this.basicDialogInfo.get('datesToBeDecidedLater').value;
            tauditionDetail.datesAreTemptative = this.basicDialogInfo.get('datesAreTemptative').value;
            if (this.auditionDetail && this.auditionDetail.id && this.auditionDetail.id !== 0) {
              this.productionExtraService.saveAuditionDetail(this.auditionDetail.id, this.productionId, tauditionDetail)
                .subscribe(
                  (auditionDetail: AuditionDetail) => {
                    //this.auditionDetail = auditionDetail;
                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    this.isVisible = false;
                    this.messageDispatchService.dispatchSuccesMessage(
                      "Audition detail '" + auditionDetail.name + "' was saved successfully",
                      "Audition Detail Saved");

                  },
                  (error) => {
                    this.saveIsDisabled = false;
                    this.cancelIsDisabled = false;

                    this.messageDispatchService.dispatchErrorMessage(
                      "Error saving audition detail '" + tauditionDetail.name + "'. " + (error)
                      ? (error.error)
                      ? (error.error.exceptionMessage)
                      ? error.error.exceptionMessage
                      : error.error
                      : error
                      : "unknown error",
                      "Audition Detail Save Error");
                    this.messages.push({
                      severity: 'error',
                      summary: "Audition Detail Save Error",
                      detail: "Error saving audition detail '" + tauditionDetail.name + "'. " + (error)
                        ? (error.error)
                        ? (error.error.exceptionMessage)
                        ? error.error.exceptionMessage
                        : error.error
                        : error
                        : "unknown error"
                    });
                  });

            } else {
              this.productionExtraService.createAuditionDetail(this.productionId, tauditionDetail).subscribe(
                (auditionDetail: AuditionDetail) => {
                  if (auditionDetail != null) {
                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    this.isVisible = false;
                    this.messageDispatchService.dispatchSuccesMessage(
                      "New audition detail '" + auditionDetail.name + "' was created successfully",
                      "Audition Detail Created");
                  }

                },
                (error) => {
                  this.saveIsDisabled = false;
                  this.cancelIsDisabled = false;

                  this.messageDispatchService.dispatchErrorMessage(
                    "Error creating audition detail '" + tauditionDetail.name + "'. " + (error)
                    ? (error.error)
                    ? (error.error.exceptionMessage)
                    ? error.error.exceptionMessage
                    : error.error
                    : error
                    : "unknown error",
                    "Audition Detail Creation Error");
                  this.messages.push({
                    severity: 'error',
                    summary: "Audition Detail Creation Error",
                    detail: "Error creating audition detail '" + tauditionDetail.name + "'. " + (error)
                      ? (error.error)
                      ? (error.error.exceptionMessage)
                      ? error.error.exceptionMessage
                      : error.error
                      : error
                      : "unknown error"
                  });
                });
            }

        } else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }  else if (!this.basicDialogInfo.get('auditionType').valid) {
              this.basicDialogInfo.get('auditionType').markAsDirty();
              this.auditionTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
              this.auditionTypeField.inputViewChild.nativeElement.focus();
            } else if (!this.basicDialogInfo.get('location').valid) {
                this.basicDialogInfo.get('location').markAsDirty();
                this.locationField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.locationField.nativeElement.focus();
            } else if (!this.basicDialogInfo.get('startDate').valid) {
                this.basicDialogInfo.get('startDate').markAsDirty();
                this.startDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.startDateField.inputfieldViewChild.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('endDate').valid) {
                this.basicDialogInfo.get('endDate').markAsDirty();
                this.endDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.endDateField.inputfieldViewChild.nativeElement.focus();
            }
        }
    }
}
