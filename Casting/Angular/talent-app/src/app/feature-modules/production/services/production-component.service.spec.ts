import { TestBed } from '@angular/core/testing';

import { ProductionComponentService } from './production-component.service';

describe('ProductionComponentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionComponentService = TestBed.get(ProductionComponentService);
    expect(service).toBeTruthy();
  });
});
