import { Component, OnInit, Input } from '@angular/core';
import { TargetState, StateService } from '@uirouter/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
//import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    @Input() returnTo: TargetState;
    loginForm: FormGroup;
    submitted = false;
    credentials = { username: null, password: null };
    submitting: boolean;
    errorMessage: string;


    constructor(private _formBuilder: FormBuilder, private authenticationService: AuthenticationService, public stateService: StateService) { }
    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.submitting = true;
        const returnToOriginalState = () => {
            const state = this.returnTo.state();

            const params = this.returnTo.params();
            const options = Object.assign({}, this.returnTo.options(), { reload: true });
            if (state.name == 'logout') {
                this.stateService.go('home');

            } else {
                this.stateService.go(state, params, options);

            }
        };

        const showError = (errorMessage) =>
            this.errorMessage = errorMessage;

        const stop = () => this.submitting = false;
        this.authenticationService.login(this.loginForm.value.userName, this.loginForm.value.password)
            .then(returnToOriginalState)
            .catch(showError)
            .then(stop, stop);

    }
    ngOnInit() {
        this.loginForm = this._formBuilder.group({
            userName: ['bobob', Validators.required],
            password: ['Brazil99?', Validators.required]
        });
    }

}
