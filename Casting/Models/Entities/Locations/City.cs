﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.CastingCalls;

namespace Casting.Model.Entities.Locations
{
    public class City:Logger
    {
        public City()
        {
            CastingCalls = new List<CastingCallCityReference>();
            CoveringAgentReferences = new List<AgentCityReference>();
            AgencyOffices = new List<AgencyOffice>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int RegionId { get; set; }
        public Region Region { get; set; }

        public int CountryId { get; set; }
        public Country Country { get; set; }
        public bool IsPopular { get; set; }
        public ICollection<CastingCallCityReference> CastingCalls { get; set; }

        public ICollection<AgentCityReference> CoveringAgentReferences { get; set; }
        public ICollection<AgencyOffice> AgencyOffices { get; set; }
        public string ShortName { get; set; }

    }
}
