﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Profiles
{
    public  class Training:Logger
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        public int OrderIndex { get; set; }
        public string School { get; set; }
        public string DegreeCourse { get; set; }
        public string Instructor { get; set; }
        public int YearsCompleted { get; set; }
    }
}
