import { Injectable } from '@angular/core';
import { ProductionRoleTransferObject, Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../models/production.models';
//import { MatDialog, MatDialogRef } from '@angular/material';
import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component'
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { MessageInterface } from '../models/confirmation.models';

@Injectable({
  providedIn: 'root'
})
export class ConfirmationDialogService {

    constructor() { }
    private confirmationDialogRef: any;
    private _confirmationResultObs: Subject<boolean> = new Subject<boolean>();
    public readonly confirmationResultObs: Observable<boolean> = this._confirmationResultObs.asObservable();

    public subscribeToConfirmationResultObs(): Observable<boolean> {
        return this._confirmationResultObs;
    }

    public confirmAction(confirmationMessage: string, title: string= "Confirm", yesText:string = "Confirm", noText:string = "Cancel"): Observable<boolean> {
        let transferObject: MessageInterface = new MessageInterface;
        transferObject.title = title;
        transferObject.confirmationMessage = confirmationMessage;
        transferObject.yesText = yesText;
        transferObject.noText = noText;
       /* const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        //width: '90%',
        data: transferObject,
        autoFocus: false,
       // backdropClass: 'logindialog-overlay',
       // panelClass: 'logindialog-panel'
      });

      dialogRef.afterClosed().subscribe(result => {
        let confirmationResult: boolean = result;
          this._confirmationResultObs.next(confirmationResult);
        // this.animal = result;
      });*/
        return this._confirmationResultObs;
    }



}
