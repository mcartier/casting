﻿using Casting.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Casting.Web.Models.Production
{
    public class AuditionDetailBindingModel
    {
        public int ProductionId { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Instructions { get; set; }

        public string Location { get; set; }
        public string LocationDetail { get; set; }
        public AuditionTypeEnum auditionTypeEnum { get; set; }
        public bool OnlyThoseSelectedWillParticipate { get; set; }
        public bool DatesToBeDecidedLater { get; set; }
        public bool DatesAreTemptative { get; set; }
        public bool IsOneDayOnly { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }


    }

    public class ShootDetailBindingModel
    {
        public int ProductionId { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string Instructions { get; set; }
        public string Location { get; set; }
        public string LocationDetail { get; set; }
        public bool DatesToBeDecidedLater { get; set; }
       public bool DatesAreTemptative { get; set; }
        public bool IsOneDayOnly { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DaysOfShooting { get; set; }


    }

    public class PayDetailBindingModel
    {
        public int ProductionId { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public PayPackageTypeEnum PayPackageType { get; set; }

        
    }
    public class SideBindingModel
    {
        public int ProductionId { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string FileId { get; set; }
        public string OriginalFileName { get; set; }

        public string PastedText { get; set; }


    }
}