import { TestBed } from '@angular/core/testing';

import { MessageDispatchService } from './message-dispatch.service';

describe('MessageDispatchService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageDispatchService = TestBed.get(MessageDispatchService);
    expect(service).toBeTruthy();
  });
});
