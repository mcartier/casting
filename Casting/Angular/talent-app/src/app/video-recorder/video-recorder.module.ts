import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideoRecorderComponent } from './components/video-recorder/video-recorder.component';
import { VideoRecorderService } from './video-recorder.service';
import { GlobalModule } from '../global/global.module';
import { MediaSourceSettingsComponent } from './components/media-source-settings/media-source-settings.component';

@NgModule({

  declarations: [VideoRecorderComponent, MediaSourceSettingsComponent],
     imports: [
         CommonModule,
         GlobalModule
    ],
  entryComponents: [
    MediaSourceSettingsComponent
  ],
     exports: [VideoRecorderComponent],
    providers: [VideoRecorderService]
})
export class VideoRecorderModule { }
