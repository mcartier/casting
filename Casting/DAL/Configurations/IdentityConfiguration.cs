﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Casting.DAL.Configurations
{

    public class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {

        public ApplicationUserConfiguration()
        {
            HasKey(k => k.Id);
           // HasMany(a => a.Agents)
          //      .WithRequired(u => u.ApplicationUser)
          //      .HasForeignKey(k => k.UserId);
         //   HasMany(a => a.Directors)
         //       .WithRequired(u => u.ApplicationUser)
         //       .HasForeignKey(k => k.UserId);
           // HasMany(a => a.Guests)
          //      .WithRequired(u => u.ApplicationUser)
          //      .HasForeignKey(k => k.UserId);
         //   HasMany(a => a.Agents)
          //      .WithRequired(u => u.ApplicationUser)
         //       .HasForeignKey(k => k.UserId);
           // HasMany(a => a.Talents)
           //    .WithOptional(u => u.ApplicationUser)
            //   .HasForeignKey(k => k.UserId)
           //    .WillCascadeOnDelete(false);
           // Property(i => i.IdI).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(b => b.BaseCastingUser);
          //  Property(b=>b.BaseUserId).
              //  .WithOptionalDependent(a => a.User);

        }

    }


    public class IdentityUserLoginConfiguration : EntityTypeConfiguration<IdentityUserLogin>
        {

            public IdentityUserLoginConfiguration()
            {
                HasKey(iul => iul.UserId);
            }

        }

        public class IdentityUserRoleConfiguration : EntityTypeConfiguration<IdentityUserRole>
        {

            public IdentityUserRoleConfiguration()
            {
                HasKey(iur => iur.RoleId);
            }

        }
   
}
