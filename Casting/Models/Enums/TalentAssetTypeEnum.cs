﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum TalentAssetTypeEnum
    {
        Audio = 1,
        Video = 2,
        Resume = 3,
        Image = 4
    }
}
