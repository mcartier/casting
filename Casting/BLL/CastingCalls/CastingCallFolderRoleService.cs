﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Enums;
using Casting.BLL.CastingCalls.Interfaces;

namespace Casting.BLL.CastingCalls
{
    public class CastingCallFolderRoleService : BaseService, ICastingCallFolderRoleService
    {
        private ICastingCallAsyncRepository _castingSessionRepository;

        public CastingCallFolderRoleService(ICastingCallAsyncRepository castingSessionAsyncRepository)
        {
            _castingSessionRepository = castingSessionAsyncRepository;
        }

        public async Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdAsync(int castingSessionFolderRoleId)
        {
            return await _castingSessionRepository.GetCastingCallFolderRoleByIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.Roles, castingSessionFolderRoleId);
        }
        public async Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdWithPersonasAsync(int castingSessionFolderRoleId)
        {
            return await _castingSessionRepository.GetCastingCallFolderRoleByIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.Personas, castingSessionFolderRoleId);
        }
        public async Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdWithPersonasAndAssetsAsync(int castingSessionFolderRoleId)
        {
            return await _castingSessionRepository.GetCastingCallFolderRoleByIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.PersonasAndAssets, castingSessionFolderRoleId);
        }

        public async Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingCallFolderRolesByCastingCallFolderIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.Roles,  castingSessionFolderId);
        }

        public async Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdWithPersonasAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingCallFolderRolesByCastingCallFolderIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.Personas,  castingSessionFolderId);
        }

        public async Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdWithPersonasAndAssetsAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingCallFolderRolesByCastingCallFolderIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.PersonasAndAssets,  castingSessionFolderId);
        }

        public async Task<int> AddNewCastingCallFolderRoleAsync(bool orderAtTheTop, CastingCallFolderRole castingSessionFolderRole)
        {
            return await _castingSessionRepository.AddNewCastingCallFolderRoleAsync(orderAtTheTop, castingSessionFolderRole);
        }

        public async Task<int> UpdateCastingCallFolderRoleAsync(CastingCallFolderRole castingSessionFolderRole)
        {
            return await _castingSessionRepository.UpdateCastingCallFolderRoleAsync(castingSessionFolderRole);
        }
        public async Task<int> DeleteCastingCallFolderRole(CastingCallFolderRole castingSessionFolderRole)
        {
            return await _castingSessionRepository.DeleteCastingCallFolderRoleAsync(castingSessionFolderRole);
        }
    }
}
