﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casting.Model.Entities.Sessions
{
    public class CastingSessionRole:Logger
    {
        [Key] public int Id { get; set; }
        public int CastingSessionId { get; set; }
       public CastingSession CastingSession { get; set; }
       [NotMapped]
       public bool CountIsSet { get; set; }
       [NotMapped]
       public int PersonasCount { get; set; }
        public ICollection<CastingSessionRoleSelectionPersonaReference> Personas { get; set; }
        public OrderByValuesEnum SelectedPersonasSortOrderI { get; set; }
        [NotMapped]
        public int FolderRolesCount { get; set; }
        public ICollection<CastingSessionFolderRole> FolderRoles { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public int OrderIndex { get; set; }
    }
}
