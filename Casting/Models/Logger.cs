﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model
{
    public abstract class Logger
    {
        public DateTime LastModifiedDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public void UpdateModificationLogValues(bool isAdded)
        {
            if (isAdded)
            {
                CreatedDate = DateTime.Now;
            }

            LastModifiedDate = DateTime.Now;
        }
    }
}
