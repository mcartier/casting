import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
//import { MatDialogRef, MAT_DIALOG_DATA, MatRadioChange } from '@angular/material';
import { ProductionRoleTransferObject, Production, NewProduction, Role, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../models/production.models';
import { MessageInterface } from '../../models/confirmation.models';
import { ConfirmationDialogService} from '../../services/confirmation-dialog.service';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss']
})

export class ConfirmationDialogComponent implements OnInit {

    constructor() { }
      
  ngOnInit() {
  }

}
