import { Injectable, Inject } from '@angular/core';
import { Transition } from "@uirouter/angular";
import { ProductionService } from './production.service';
import { Production, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../models/production.models';
import { CastingCallService } from './casting-call.service';
import { CastingCall, NewCastingCall, NewCastingCallRole, UpdateCastingCall, UpdateCastingCallRole } from '../models/casting-call.models';

@Injectable({
  providedIn: 'root'
})
export class StateDependencyResolverService {

    constructor() { }

public  getEmptyCastingCall() {
  let prod: CastingCall;
  return prod;
}
public getCastingCallFromTransactionOrServer(castingCallService: CastingCallService, transition: Transition): Promise<CastingCall> {
      let castingCallId: number;
      let castingCall: CastingCall;
      let productionId: number;
      let promise: Promise<CastingCall>;
      if (transition.params().castingCall) {
        return new Promise<CastingCall>((resolve) => {
            resolve(transition.params().castingCall);
        });
      } else {
          if (transition.params().castingCallId && transition.params().castingCallId !== 0) {
              castingCallId = transition.params().castingCallId;
        }
          else if (transition.params().draftId && transition.params().draftId !== 0) {
              castingCallId = transition.params().draftId;

        }
          if (transition.params().productionId && transition.params().productionId !== 0) {
            productionId = transition.params().productionId;
          }
        if (castingCallId !== 0) {
          return new Promise<CastingCall>((resolve) => {
              resolve(castingCallService.getCastingCall(castingCallId, productionId).toPromise());
          });
        } else {
          return new Promise<CastingCall>((resolve) => {
            resolve(null);
          });
        }

      }
    }

public getCastingCallsFromServer(castingCallService: CastingCallService, transition: Transition): Promise<CastingCall[]> {
      let castingCalls: CastingCall[];
      let promise: Promise<CastingCall>;
      return new Promise<CastingCall[]>((resolve) => {
          resolve(castingCallService.getCastingCalls().toPromise());
      });


    }

    public getEmptyProduction() {
      let prod: Production;
      return prod;
    }
public getProductionFromTransactionOrServer(productionService: ProductionService, transition: Transition): Promise<Production> {
      let productionId: number;
      let production: Production;
      let promise: Promise<Production>;
        if (transition.params().production) {
        return new Promise<Production>((resolve) => {
            resolve(transition.params().production);
        });
      } else {
            if (transition.params().productionId && transition.params().productionId !== 0) {
                productionId = transition.params().productionId;
        }
            else if (transition.params().draftId && transition.params().draftId !== 0) {
                productionId = transition.params().draftId;

        }
        if (productionId !== 0) {
          return new Promise<Production>((resolve) => {
              resolve(productionService.getProduction(productionId).toPromise());
          });
        } else {
          return new Promise<Production>((resolve) => {
            resolve(null);
          });
        }

      }
    }

public getProductionsFromServer(productionService: ProductionService, transition: Transition): Promise<Production[]> {

      let productions: Production[];
      let promise: Promise<Production>;
      return new Promise<Production[]>((resolve) => {
        resolve(productionService.getProductions().toPromise());
      });


    }
}
