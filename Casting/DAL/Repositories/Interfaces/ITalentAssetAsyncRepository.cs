﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Personas;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface ITalentAssetAsyncRepository : IAsyncRepository, IDisposable
    {
        IQueryable<VideoReference> GetVideoReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId);
        IQueryable<AudioReference> GetAudioReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId);
        IQueryable<ResumeReference> GetResumeReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId);
        IQueryable<ImageReference> GetImageReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId);

        Task<IEnumerable<VideoReference>> GetVideoReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId);

        Task<IEnumerable<AudioReference>> GetAudioReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId);

        Task<IEnumerable<ResumeReference>> GetResumeReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId);

        Task<IEnumerable<ImageReference>> GetImageReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId);
        Task<AudioReference> GetAudioAssetReferenceByIdAsync(bool withAsset, int referenceId);
        Task<VideoReference> GetVideoAssetReferenceByIdAsync(bool withAsset, int referenceId);
        Task<ResumeReference> GetResumeAssetReferenceByIdAsync(bool withAsset, int referenceId);
        Task<ImageReference> GetImageAssetReferenceByIdAsync(bool withAsset, int referenceId);

        
        Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            VideoReference assetReference);
        Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            int parentId, VideoReference assetReference);
        Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            AudioReference assetReference);
        Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            int parentId, AudioReference assetReference);
        Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            ResumeReference assetReference);
        Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            int parentId, ResumeReference assetReference);

        Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
        ImageReference assetReference);
        Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
            int parentId, ImageReference assetReference);

        Task<int> DeleteVideoAssetReferenceAsync(int parentId, VideoReference assetReference);
        Task<int> DeleteAudioAssetReferenceAsync(int parentId, AudioReference assetReference);
        Task<int> DeleteResumeAssetReferenceAsync(int parentId, ResumeReference assetReference);
        Task<int> DeleteImageAssetReferenceAsync(int parentId, ImageReference assetReference);

        Task<int> ReOrderResumeAssetAsync(int parentId,
            int desiredOrderIndex, ResumeReference assetReference);

        Task<int> ReOrderAudioAssetAsync(int parentId,
            int desiredOrderIndex, AudioReference assetReference);

        Task<int> ReOrderVideoAssetAsync(int parentId,
            int desiredOrderIndex, VideoReference assetReference);

        Task<int> ReOrderImageAssetAsync(int parentId,
            int desiredOrderIndex, ImageReference assetReference);

        Task<int> UpdateVideoAssetReferenceAsync(int parentId, VideoReference assetReference);
        Task<int> UpdateAudioAssetReferenceAsync(int parentId, AudioReference assetReference);
        Task<int> UpdateResumeAssetReferenceAsync(int parentId, ResumeReference assetReference);
        Task<int> UpdateImageAssetReferenceAsync(int parentId, ImageReference assetReference);

        // Task<int> DeleteTalentAssetReference(TalentAssetTypeEnum assentType, int referenceId);
        Task<int> GetReferenceCountPerAssetAsync(TalentAssetTypeEnum assetType, int assetId);



        Task<Audio> GetAudioAssetByIdAsync(int assetId);
        Task<Video> GetVideoAssetByIdAsync(int assetId);
        Task<Resume> GetResumeAssetByIdAsync(int assetId);
        Task<Image> GetImageAssetByIdAsync(int assetId);


        Task<int> AddNewVideoAssetAsync(Video asset);
        Task<int> AddNewAudioAssetAsync(Audio asset);
        Task<int> AddNewResumeAssetAsync(Resume asset);

        Task<int> AddNewImageAssetAsync(Image asset);

        Task<int> DeleteVideoAssetAsync(Video asset);
        Task<int> DeleteAudioAssetAsync(Audio asset);
        Task<int> DeleteResumeAssetAsync(Resume asset);
        Task<int> DeleteImageAssetAsync(Image asset);

        Task<int> UpdateVideoAssetAsync(Video asset);
        Task<int> UpdateAudioAssetAsync(Audio asset);
        Task<int> UpdateResumeAssetAsync(Resume asset);
        Task<int> UpdateImageAssetAsync(Image asset);

    }
}
