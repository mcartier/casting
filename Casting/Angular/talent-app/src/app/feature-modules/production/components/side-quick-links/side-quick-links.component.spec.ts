import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideQuickLinksComponent } from './side-quick-links.component';

describe('SideQuickLinksComponent', () => {
  let component: SideQuickLinksComponent;
  let fixture: ComponentFixture<SideQuickLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideQuickLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideQuickLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
