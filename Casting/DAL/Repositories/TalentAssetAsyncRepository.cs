﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net.Sockets;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Enums;
using System.Data.SqlClient;
namespace Casting.DAL.Repositories
{
  

    public class TalentAssetAsyncRepository : AsyncRepository, ITalentAssetAsyncRepository
    {
        ICastingContext _castingContext;
        public TalentAssetAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }
        #region AssetReferences
        #region videos

        public async Task<int> DeleteVideoAssetReferenceAsync(int parentId, VideoReference assetReference)
        {

            var referenceVideoCnt = await GetReferenceCountPerAssetAsync(TalentAssetTypeEnum.Video, assetReference.VideoId);
            var asset = assetReference.Video;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderVideoAssetAsync(parentId, 0, assetReference);

                    _castingContext.VideoReferences.Remove(assetReference);
                    if (referenceVideoCnt < 2)
                    {
                        _castingContext.Videos.Remove(asset);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateVideoAssetReferenceAsync(int parentId,
            VideoReference assetReference)
        {
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderVideoAssetAsync(int parentId,
            int desiredOrderIndex, VideoReference assetReference)
        {
            IEnumerable<VideoReference> assetReferences = new List<VideoReference>();
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = await (from refs in GetVideoReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<BasketVideoReference>().Where(ar => ar.BasketId == parentId).OrderBy(o => o.OrderIndex)
                                             select (VideoReference)refs).ToListAsync();

                    break;

                case AssetReferenceTypeEnum.Profile:
                    assetReferences = await (from refs in GetVideoReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<ActorProfileVideoReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                             select (VideoReference)refs).ToListAsync();
                    break;
                case AssetReferenceTypeEnum.Persona:

                    assetReferences = await (from refs in GetVideoReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<PersonaVideoReference>().Where(ar => ar.PersonaId == parentId).OrderBy(o => o.OrderIndex)
                                             select (VideoReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderVideoAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex > assetReference.OrderIndex)
            {
                return await ReOrderVideoAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex < assetReference.OrderIndex)
            {
                return await ReOrderVideoAssetDownAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderVideoAssetDownAsync(int desiredOrderIndex, VideoReference assetReference,
            IEnumerable<VideoReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex >= desiredOrderIndex &&
                    assetRef.OrderIndex < assetReference.OrderIndex)
                {
                    assetRef.OrderIndex++;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderVideoAssetUpAsync(int desiredOrderIndex, VideoReference assetReference,
            IEnumerable<VideoReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex > assetReference.OrderIndex &&
                    (assetRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    assetRef.OrderIndex--;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexVideoReferenceByAssetReferenceTypeAsync(int parentId,
            VideoReference assetReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (assetReference.AssetReferenceTypeEnum)
                {
                    case AssetReferenceTypeEnum.Basket:
                        maxOrderIndex = (from assetRef in _castingContext.BasketVideoReferences
                                         where assetRef.BasketId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AssetReferenceTypeEnum.Profile:
                        maxOrderIndex = (from assetRef in _castingContext.ProfileVideoReferences
                                         where assetRef.ProfileId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case AssetReferenceTypeEnum.Persona:

                        maxOrderIndex = (from assetRef in _castingContext.PersonaVideoReferences
                                         where assetRef.PersonaId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop, int parentId,
            VideoReference assetReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexVideoReferenceByAssetReferenceTypeAsync(parentId,
                    assetReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    assetReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<VideoReference>().Add(assetReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderVideoAssetAsync(parentId, 1, assetReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            VideoReference assetReference)

        {

            int parentId = 0;
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    parentId = ((BasketVideoReference)assetReference).BasketId;
                    break;

                case AssetReferenceTypeEnum.Profile:
                    parentId = ((ActorProfileVideoReference)assetReference).ProfileId;
                    break;
                case AssetReferenceTypeEnum.Persona:
                    parentId = ((PersonaVideoReference)assetReference).PersonaId;
                    break;
            }
            return await AddNewVideoAssetReferenceAsync(orderAtTheTop, parentId, assetReference);

        }
        public async Task<VideoReference> GetVideoAssetReferenceByIdAsync(bool withAsset, int referenceId)
        {
            return await GetAllVideoReferencesQueryable(withAsset).FirstOrDefaultAsync(assetRef => assetRef.Id == referenceId);

        }
        public async Task<IEnumerable<VideoReference>> GetVideoReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            return await GetVideoReferencesQueryableForParent(withAsset, assetReferenceType, parentId).ToListAsync();
        }
        public IQueryable<VideoReference> GetVideoReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            var assetReferences = GetAllVideoReferencesQueryable(withAsset);
            switch (assetReferenceType)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = from assetRef in assetReferences.OfType<BasketVideoReference>()
                       where assetRef.BasketId == parentId
                                      select assetRef;
                    break;
                case AssetReferenceTypeEnum.Profile:
                    assetReferences = from assetRef in assetReferences.OfType<ActorProfileVideoReference>()
                        where assetRef.ProfileId == parentId
                                      select assetRef;

                    break;
                case AssetReferenceTypeEnum.Persona:
                    assetReferences = from assetRef in assetReferences.OfType<PersonaVideoReference>()
                         where assetRef.PersonaId == parentId
                                      select assetRef;

                    break;
            }
            return assetReferences;
        }

        #endregion

        #region Audios

        public async Task<int> DeleteAudioAssetReferenceAsync(int parentId, AudioReference assetReference)
        {


            var referenceAudioCnt = await GetReferenceCountPerAssetAsync(TalentAssetTypeEnum.Audio, assetReference.AudioId);
            var asset = assetReference.Audio;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderAudioAssetAsync(parentId, 0, assetReference);
                    _castingContext.AudioReferences.Remove(assetReference);
                    if (referenceAudioCnt < 2)
                    {
                        _castingContext.Audios.Remove(asset);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }
        public async Task<int> UpdateAudioAssetReferenceAsync(int parentId,
            AudioReference assetReference)
        {
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> ReOrderAudioAssetAsync(int parentId,
            int desiredOrderIndex, AudioReference assetReference)
        {
            IEnumerable<AudioReference> assetReferences = new List<AudioReference>();
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = await (from refs in GetAudioReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<BasketAudioReference>().Where(ar => ar.BasketId == parentId).OrderBy(o => o.OrderIndex)
                                             select (AudioReference)refs).ToListAsync();

                    break;

                case AssetReferenceTypeEnum.Profile:
                    assetReferences = await (from refs in GetAudioReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<ProfileAudioReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                             select (AudioReference)refs).ToListAsync();
                    break;
                case AssetReferenceTypeEnum.Persona:

                    assetReferences = await (from refs in GetAudioReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<PersonaAudioReference>().Where(ar => ar.PersonaId == parentId).OrderBy(o => o.OrderIndex)
                                             select (AudioReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderAudioAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex > assetReference.OrderIndex)
            {
                return await ReOrderAudioAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex < assetReference.OrderIndex)
            {
                return await ReOrderAudioAssetDownAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderAudioAssetDownAsync(int desiredOrderIndex, AudioReference assetReference,
            IEnumerable<AudioReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex >= desiredOrderIndex &&
                    assetRef.OrderIndex < assetReference.OrderIndex)
                {
                    assetRef.OrderIndex++;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderAudioAssetUpAsync(int desiredOrderIndex, AudioReference assetReference,
            IEnumerable<AudioReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex > assetReference.OrderIndex &&
                    (assetRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    assetRef.OrderIndex--;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexAudioReferenceByAssetReferenceTypeAsync(int parentId,
            AudioReference assetReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (assetReference.AssetReferenceTypeEnum)
                {
                    case AssetReferenceTypeEnum.Basket:
                        maxOrderIndex = (from assetRef in _castingContext.BasketAudioReferences
                                         where assetRef.BasketId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AssetReferenceTypeEnum.Profile:
                        maxOrderIndex = (from assetRef in _castingContext.ProfileAudioReferences
                                         where assetRef.ProfileId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case AssetReferenceTypeEnum.Persona:

                        maxOrderIndex = (from assetRef in _castingContext.PersonaAudioReferences
                                         where assetRef.PersonaId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                }
            });
            return maxOrderIndex;
        }
        public async Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop, int parentId,
            AudioReference assetReference)

        {

            int maxOrderIndex = 0;
            maxOrderIndex = await GetHighestOrderIndexAudioReferenceByAssetReferenceTypeAsync(parentId, assetReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    assetReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<AudioReference>().Add(assetReference);
                     await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderAudioAssetAsync(parentId, 1, assetReference);
                    }
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            AudioReference assetReference)

        {

            int parentId = 0;
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    parentId = ((BasketAudioReference)assetReference).BasketId;
                    break;

                case AssetReferenceTypeEnum.Profile:
                    parentId = ((ProfileAudioReference)assetReference).ProfileId;
                    break;
                case AssetReferenceTypeEnum.Persona:
                    parentId = ((PersonaAudioReference)assetReference).PersonaId;
                    break;
            }
            return await AddNewAudioAssetReferenceAsync(orderAtTheTop, parentId, assetReference);

        }
        public async Task<AudioReference> GetAudioAssetReferenceByIdAsync(bool withAsset, int referenceId)
        {
         return await GetAllAudioReferencesQueryable(withAsset).FirstOrDefaultAsync(assetRef => assetRef.Id == referenceId);
           
        }
        public async Task<IEnumerable<AudioReference>> GetAudioReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            return await GetAudioReferencesQueryableForParent(withAsset, assetReferenceType, parentId).ToListAsync();
        }
        public IQueryable<AudioReference> GetAudioReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            var assetReferences = GetAllAudioReferencesQueryable(withAsset);
            switch (assetReferenceType)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = from assetRef in assetReferences.OfType<BasketAudioReference>()
                        where assetRef.BasketId == parentId
                                      select assetRef;
                    break;
                case AssetReferenceTypeEnum.Profile:
                    assetReferences = from assetRef in assetReferences.OfType<ProfileAudioReference>()
                        where assetRef.ProfileId == parentId
                                      select assetRef;

                    break;
                case AssetReferenceTypeEnum.Persona:
                    assetReferences = from assetRef in assetReferences.OfType<PersonaAudioReference>()
                        where assetRef.PersonaId == parentId
                                      select assetRef;

                    break;
            }
            return assetReferences;
        }
        #endregion

        #region Resumes

        public async Task<int> DeleteResumeAssetReferenceAsync(int parentId, ResumeReference assetReference)
        {

            var referenceResumeCnt = await GetReferenceCountPerAssetAsync(TalentAssetTypeEnum.Resume, assetReference.ResumeId);
            var asset = assetReference.Resume;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderResumeAssetAsync(parentId, 0, assetReference);
                    _castingContext.ResumeReferences.Remove(assetReference);
                    if (referenceResumeCnt < 2)
                    {
                        _castingContext.Resumes.Remove(asset);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }
        public async Task<int> UpdateResumeAssetReferenceAsync(int parentId,
            ResumeReference assetReference)
        {
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> ReOrderResumeAssetAsync(int parentId,
            int desiredOrderIndex, ResumeReference assetReference)
        {
            IEnumerable<ResumeReference> assetReferences = new List<ResumeReference>();
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = await (from refs in GetResumeReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<BasketResumeReference>().Where(ar => ar.BasketId == parentId).OrderBy(o => o.OrderIndex)
                                             select (ResumeReference)refs).ToListAsync();

                    break;

                case AssetReferenceTypeEnum.Profile:
                    assetReferences = await (from refs in GetResumeReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<ProfileResumeReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                             select (ResumeReference)refs).ToListAsync();
                    break;
                case AssetReferenceTypeEnum.Persona:

                    assetReferences = await (from refs in GetResumeReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<PersonaResumeReference>().Where(ar => ar.PersonaId == parentId).OrderBy(o => o.OrderIndex)
                                             select (ResumeReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderResumeAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex > assetReference.OrderIndex)
            {
                return await ReOrderResumeAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex < assetReference.OrderIndex)
            {
                return await ReOrderResumeAssetDownAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderResumeAssetDownAsync(int desiredOrderIndex, ResumeReference assetReference,
            IEnumerable<ResumeReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex >= desiredOrderIndex &&
                    assetRef.OrderIndex < assetReference.OrderIndex)
                {
                    assetRef.OrderIndex++;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderResumeAssetUpAsync(int desiredOrderIndex, ResumeReference assetReference,
            IEnumerable<ResumeReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex > assetReference.OrderIndex &&
                    (assetRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    assetRef.OrderIndex--;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexResumeReferenceByAssetReferenceTypeAsync(int parentId,
            ResumeReference assetReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (assetReference.AssetReferenceTypeEnum)
                {
                    case AssetReferenceTypeEnum.Basket:
                        maxOrderIndex = (from assetRef in _castingContext.BasketResumeReferences
                                         where assetRef.BasketId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AssetReferenceTypeEnum.Profile:
                        maxOrderIndex = (from assetRef in _castingContext.ProfileResumeReferences
                                         where assetRef.ProfileId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case AssetReferenceTypeEnum.Persona:

                        maxOrderIndex = (from assetRef in _castingContext.PersonaResumeReferences
                                         where assetRef.PersonaId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                }
            });
            return maxOrderIndex;
        }
        public async Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop, int parentId,
            ResumeReference assetReference)

        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexResumeReferenceByAssetReferenceTypeAsync(parentId,
                    assetReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    assetReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<ResumeReference>().Add(assetReference);
                    result = await _castingContext.SaveChangesAsync();

                    if (orderAtTheTop)
                    {
                        await ReOrderResumeAssetAsync(parentId, 1, assetReference);
                    }
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            ResumeReference assetReference)

        {

            int parentId = 0;
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    parentId = ((BasketResumeReference)assetReference).BasketId;
                    break;

                case AssetReferenceTypeEnum.Profile:
                    parentId = ((ProfileResumeReference)assetReference).ProfileId;
                    break;
                case AssetReferenceTypeEnum.Persona:
                    parentId = ((PersonaResumeReference)assetReference).PersonaId;
                    break;
            }
            return await AddNewResumeAssetReferenceAsync(orderAtTheTop, parentId, assetReference);

        }
        public async Task<ResumeReference> GetResumeAssetReferenceByIdAsync(bool withAsset, int referenceId)
        {
        
                return await GetAllResumeReferencesQueryable(withAsset).FirstOrDefaultAsync(assetRef => assetRef.Id == referenceId);
           
        }
        public async Task<IEnumerable<ResumeReference>> GetResumeReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            return await GetResumeReferencesQueryableForParent(withAsset, assetReferenceType, parentId).ToListAsync();
        }
        public IQueryable<ResumeReference> GetResumeReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            var assetReferences = GetAllResumeReferencesQueryable(withAsset);
            switch (assetReferenceType)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = from assetRef in assetReferences.OfType<BasketResumeReference>()
                            where assetRef.BasketId == parentId

                                      select assetRef;

                    break;
                case AssetReferenceTypeEnum.Profile:
                    assetReferences = from assetRef in assetReferences.OfType<ProfileResumeReference>()
                        where assetRef.ProfileId == parentId
                                      select assetRef;

                    break;
                case AssetReferenceTypeEnum.Persona:
                    assetReferences = from assetRef in assetReferences.OfType<PersonaResumeReference>()
                        where assetRef.PersonaId == parentId
                                      select assetRef;

                    break;
            }
            return assetReferences;
        }
        #endregion

        #region Images

        public async Task<int> DeleteImageAssetReferenceAsync(int parentId, ImageReference assetReference)
        {

            var referenceImageCnt = await GetReferenceCountPerAssetAsync(TalentAssetTypeEnum.Image, assetReference.ImageId);
            var asset = assetReference.Image;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderImageAssetAsync(parentId, 0, assetReference);
                    _castingContext.ImageReferences.Remove(assetReference);
                    if (referenceImageCnt < 2)
                    {
                        _castingContext.Images.Remove(asset);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateImageAssetReferenceAsync(int parentId,
            ImageReference assetReference)
        {
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> ReOrderImageAssetAsync(int parentId,
            int desiredOrderIndex, ImageReference assetReference)
        {
            IEnumerable<ImageReference> assetReferences = new List<ImageReference>();
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = await (from refs in GetImageReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<BasketImageReference>().Where(ar => ar.BasketId == parentId).OrderBy(o => o.OrderIndex)
                                             select (ImageReference)refs).ToListAsync();

                    break;

                case AssetReferenceTypeEnum.Profile:
                    assetReferences = await (from refs in GetImageReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<ActorProfileImageReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                             select (ImageReference)refs).ToListAsync();
                    break;
                case AssetReferenceTypeEnum.Persona:

                    assetReferences = await (from refs in GetImageReferencesQueryableForParent(false, assetReference.AssetReferenceTypeEnum, parentId)
                            .OfType<PersonaImageReference>().Where(ar => ar.PersonaId == parentId).OrderBy(o => o.OrderIndex)
                                             select (ImageReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderImageAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex > assetReference.OrderIndex)
            {
                return await ReOrderImageAssetUpAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            else if (desiredOrderIndex < assetReference.OrderIndex)
            {
                return await ReOrderImageAssetDownAsync(desiredOrderIndex, assetReference, assetReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderImageAssetDownAsync(int desiredOrderIndex, ImageReference assetReference,
            IEnumerable<ImageReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex >= desiredOrderIndex &&
                    assetRef.OrderIndex < assetReference.OrderIndex)
                {
                    assetRef.OrderIndex++;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderImageAssetUpAsync(int desiredOrderIndex, ImageReference assetReference,
            IEnumerable<ImageReference> assetReferences)
        {
            foreach (var assetRef in assetReferences)
            {
                if (assetRef.OrderIndex > assetReference.OrderIndex &&
                    (assetRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    assetRef.OrderIndex--;
                    SetEntityState(assetRef, EntityState.Modified);
                }

            }

            assetReference.OrderIndex = desiredOrderIndex;
            SetEntityState(assetReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexImageReferenceByAssetReferenceTypeAsync(int parentId,
            ImageReference assetReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (assetReference.AssetReferenceTypeEnum)
                {
                    case AssetReferenceTypeEnum.Basket:
                        maxOrderIndex = (from assetRef in _castingContext.BasketImageReferences
                                         where assetRef.BasketId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AssetReferenceTypeEnum.Profile:
                        maxOrderIndex = (from assetRef in _castingContext.ProfileImageReferences
                                         where assetRef.ProfileId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case AssetReferenceTypeEnum.Persona:

                        maxOrderIndex = (from assetRef in _castingContext.PersonaImageReferences
                                         where assetRef.PersonaId == parentId
                                         select assetRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                }
            });
            return maxOrderIndex;
        }
        public async Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop, int parentId,
            ImageReference assetReference)

        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexImageReferenceByAssetReferenceTypeAsync(parentId,
                    assetReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    assetReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<ImageReference>().Add(assetReference);
                    result = await _castingContext.SaveChangesAsync();

                    if (orderAtTheTop)
                    {
                        await ReOrderImageAssetAsync(parentId, 1, assetReference);
                    }
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
            ImageReference assetReference)

        {

            int parentId = 0;
            switch (assetReference.AssetReferenceTypeEnum)
            {
                case AssetReferenceTypeEnum.Basket:
                    parentId = ((BasketImageReference)assetReference).BasketId;
                    break;

                case AssetReferenceTypeEnum.Profile:
                    parentId = ((ActorProfileImageReference)assetReference).ProfileId;
                    break;
                case AssetReferenceTypeEnum.Persona:
                    parentId = ((PersonaImageReference)assetReference).PersonaId;
                    break;
            }
            return await AddNewImageAssetReferenceAsync(orderAtTheTop, parentId, assetReference);

        }
        public async Task<ImageReference> GetImageAssetReferenceByIdAsync(bool withAsset, int referenceId)
        {
           
                return await GetAllImageReferencesQueryable(withAsset).FirstOrDefaultAsync(assetRef => assetRef.Id == referenceId);
           
        }

        public async Task<IEnumerable<ImageReference>> GetImageReferencesForParentAsync(bool withAsset,
            AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            return await GetImageReferencesQueryableForParent(withAsset, assetReferenceType, parentId).ToListAsync();
        }

        public IQueryable<ImageReference> GetImageReferencesQueryableForParent(bool withAsset, AssetReferenceTypeEnum assetReferenceType, int parentId)
        {
            var assetReferences = GetAllImageReferencesQueryable(withAsset);
            switch (assetReferenceType)
            {
                case AssetReferenceTypeEnum.Basket:
                    assetReferences = from assetRef in assetReferences.OfType<BasketImageReference>()
                        where assetRef.BasketId == parentId
                                      select assetRef;
                    break;
                case AssetReferenceTypeEnum.Profile:
                    assetReferences = from assetRef in assetReferences.OfType<ActorProfileImageReference>()
                        where assetRef.ProfileId == parentId
                                      select assetRef;

                    break;
                case AssetReferenceTypeEnum.Persona:
                    assetReferences = from assetRef in assetReferences.OfType<PersonaImageReference>()
                        where assetRef.PersonaId == parentId
                                      select assetRef;

                    break;
            }
            return assetReferences;
        }
        #endregion
        #region forall
        /*
        public async Task<int> DeleteTalentAssetReference(TalentAssetTypeEnum assetType, int referenceId)
        {

            switch (assetType)
            {
                case TalentAssetTypeEnum.Image:
                    var haeadshotAssetRef = await GetImageAssetReferenceByIdAsync(referenceId);
                    var referenceHeadCnt = await GetReferenceCountPerAssetAsync(assetType, haeadshotAssetRef.ImageId);
                    _castingContext.ImageReferences.Remove(haeadshotAssetRef);
                    if (referenceHeadCnt < 2)
                    {
                        _castingContext.Images.Remove(haeadshotAssetRef.Image);
                        //here we delete the actual media file from the disk too!!!!!
                    }

                    break;
                case TalentAssetTypeEnum.Resume:
                    var resumeAssetRef = await GetResumeAssetReferenceByIdAsync(referenceId);
                    var referenceResumeCnt = await GetReferenceCountPerAssetAsync(assetType, resumeAssetRef.ResumeId);
                    _castingContext.ResumeReferences.Remove(resumeAssetRef);
                    if (referenceResumeCnt < 2)
                    {
                        _castingContext.Resumes.Remove(resumeAssetRef.Resume);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    break;
                case TalentAssetTypeEnum.Audio:
                    var audioAssetRef = await GetAudioAssetReferenceByIdAsync(referenceId);
                    var referenceAudioCnt = await GetReferenceCountPerAssetAsync(assetType, audioAssetRef.AudioId);
                    _castingContext.AudioReferences.Remove(audioAssetRef);
                    if (referenceAudioCnt < 2)
                    {
                        _castingContext.Audios.Remove(audioAssetRef.Audio);
                        //here we delete the actual media file from the disk too!!!!!
                    }

                    break;
                case TalentAssetTypeEnum.Video:
                    var videoAssetRef = await GetVideoAssetReferenceByIdAsync(referenceId);
                    var referenceVideoCnt = await GetReferenceCountPerAssetAsync(assetType, videoAssetRef.VideoId);
                    _castingContext.VideoReferences.Remove(videoAssetRef);
                    if (referenceVideoCnt < 2)
                    {
                        _castingContext.Videos.Remove(videoAssetRef.Video);
                        //here we delete the actual media file from the disk too!!!!!
                    }

                    break;
            }
            return await _castingContext.SaveChangesAsync();

        }
        */
        public async Task<int> GetReferenceCountPerAssetAsync(TalentAssetTypeEnum assetType, int assetId)
        {
            int cnt = 0;
            await Task.Run(() =>
            {
                switch (assetType)
                {
                    case TalentAssetTypeEnum.Image:
                        cnt = _castingContext.ImageReferences.Count(i => i.ImageId == assetId);

                        break;
                    case TalentAssetTypeEnum.Resume:
                        cnt = _castingContext.ResumeReferences.Count(i => i.ResumeId == assetId);

                        break;
                    case TalentAssetTypeEnum.Audio:
                        cnt = _castingContext.AudioReferences.Count(i => i.AudioId == assetId);

                        break;
                    case TalentAssetTypeEnum.Video:
                        cnt = _castingContext.VideoReferences.Count(i => i.VideoId == assetId);

                        break;
                }
            });
            return cnt;
        }
        #endregion
        #region privatemethods

        private IQueryable<VideoReference> GetAllVideoReferencesQueryable(bool withAsset)
        {
            return from assetRef in(withAsset ? _castingContext.VideoReferences.Include(asset=>asset.Video) : _castingContext.VideoReferences) select assetRef;
        }



        private IQueryable<AudioReference> GetAllAudioReferencesQueryable(bool withAsset)
        {
            return from assetRef in (withAsset ? _castingContext.AudioReferences.Include(asset => asset.Audio) : _castingContext.AudioReferences) select assetRef;
        }

        private IQueryable<ResumeReference> GetAllResumeReferencesQueryable(bool withAsset)
        {
        return from assetRef in (withAsset ? _castingContext.ResumeReferences.Include(asset => asset.Resume) : _castingContext.ResumeReferences) select assetRef;
        }

        private IQueryable<ImageReference> GetAllImageReferencesQueryable(bool withAsset)
        {
            return from assetRef in (withAsset ? _castingContext.ImageReferences.Include(asset => asset.Image) : _castingContext.ImageReferences) select assetRef;
        }

        #endregion
        #endregion
        #region Assets
        public async Task<Audio> GetAudioAssetByIdAsync(int assetId)
        {
            return await _castingContext.Set<Audio>().FindAsync(assetId);
        }

        public async Task<Video> GetVideoAssetByIdAsync(int assetId)
        {
            return await _castingContext.Set<Video>().FindAsync(assetId);
        }

        public async Task<Resume> GetResumeAssetByIdAsync(int assetId)
        {
            return await _castingContext.Set<Resume>().FindAsync(assetId);
        }

        public async Task<Image> GetImageAssetByIdAsync(int assetId)
        {
            return await _castingContext.Set<Image>().FindAsync(assetId);
        }

        public async Task<int> AddNewVideoAssetAsync(Video asset)
        {
            _castingContext.Set<Video>().Add(asset);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> AddNewAudioAssetAsync(Audio asset)
        {
            _castingContext.Set<Audio>().Add(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> AddNewResumeAssetAsync(Resume asset)
        {
            _castingContext.Set<Resume>().Add(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> AddNewImageAssetAsync(Image asset)
        {
            _castingContext.Set<Image>().Add(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> DeleteVideoAssetAsync(Video asset)
        {
            _castingContext.Set<Video>().Remove(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> DeleteAudioAssetAsync(Audio asset)
        {
            _castingContext.Set<Audio>().Remove(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> DeleteResumeAssetAsync(Resume asset)
        {
            _castingContext.Set<Resume>().Remove(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> DeleteImageAssetAsync(Image asset)
        {
            _castingContext.Set<Image>().Remove(asset);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateVideoAssetAsync(Video asset)
        {
            SetEntityState(asset, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateAudioAssetAsync(Audio asset)
        {
            SetEntityState(asset, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateResumeAssetAsync(Resume asset)
        {
            SetEntityState(asset, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateImageAssetAsync(Image asset)
        {
            SetEntityState(asset, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion

    }
}
