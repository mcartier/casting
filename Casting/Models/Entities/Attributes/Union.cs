﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Profiles;

namespace Casting.Model.Entities.Attributes
{
    public class Union : Logger
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }
        public bool IsUnion { get; set; }
    }
}
