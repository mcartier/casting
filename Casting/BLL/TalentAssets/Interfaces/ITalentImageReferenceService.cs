﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;

namespace Casting.BLL.TalentAssets.Interfaces
{

    public interface ITalentImageReferenceService
    {
        Task<ImageReference> GetImageReferenceByIdAsync(int imageId);
        Task<IEnumerable<ImageReference>> GetProfileImageReferencesForParentAsync(int profileId, bool withAssets);
        Task<IEnumerable<ImageReference>> GetPersonaImageReferencesForParentAsync(int personaId, bool withAssets);
        Task<IEnumerable<ImageReference>> GetBasketImageReferencesForParentAsync(int basketId, bool withAssets);
        Task<IEnumerable<ImageReference>> GetProfileMainImageImageReferencesForParentAsync(int profileId, bool withAssets);

        Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
            ImageReference assetReference);

        Task<int> AddNewImageAssetReferenceAsync(bool orderAtTheTop,
            int parentId, ImageReference assetReference);

        Task<int> AddNewProfileImageAssetReferenceAsync(bool orderAtTheTop, ActorProfileImageReference assetReference);
        Task<int> AddNewPersonaImageAssetReferenceAsync(bool orderAtTheTop, PersonaImageReference assetReference);
        Task<int> AddNewBasketImageAssetReferenceAsync(bool orderAtTheTop, BasketImageReference assetReference);
        Task<int> DeleteImageAssetReferenceAsync(int parentId, ImageReference assetReference);
        Task<int> DeleteProfileImageAssetReferenceAsync(ActorProfileImageReference assetReference);
        Task<int> DeleteBasketImageAssetReferenceAsync(BasketImageReference assetReference);
        Task<int> DeletePersonaImageAssetReferenceAsync(PersonaImageReference assetReference);

        Task<int> UpdateImageAssetReferenceAsync(int parentId,
            ImageReference assetReference);

        Task<int> UpdateProfileImageAssetReferenceAsync(ActorProfileImageReference assetReference);
        Task<int> UpdateBasketImageAssetReferenceAsync(BasketImageReference assetReference);
        Task<int> UpdatePersonaImageAssetReferenceAsync(PersonaImageReference assetReference);

        Task<int> ReOrderImageAssetAsync(int parentId,
            int desiredOrderIndex, ImageReference assetReference);

        Task<int> ReOrderProfileImageAssetAsync(int desiredOrderIndex, ActorProfileImageReference assetReference);
        Task<int> ReOrderBasketImageAssetAsync(int desiredOrderIndex, BasketImageReference assetReference);
        Task<int> ReOrderPersonaImageAssetAsync(int desiredOrderIndex, PersonaImageReference assetReference);
    }
}