﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Talents;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
    public class HouseTalentPersonaReference : PersonaReference
    {
        public HouseTalentPersonaReference()
        {
            base.PersonaReferenceType = PersonaReferenceTypeEnum.HouseTalent;
        }
        public int TalentId { get; set; }
        public HouseTalent Talent { get; set; }
      
    }
}
