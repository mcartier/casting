﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.BLL.CastingSessions.Interfaces;

namespace Casting.BLL.CastingSessions
{
    public class CastingSessionFolderService : BaseService, ICastingSessionFolderService
    {
        private ICastingSessionAsyncRepository _castingSessionRepository;

        public CastingSessionFolderService(ICastingSessionAsyncRepository castingSessionAsyncRepository)
        {
            _castingSessionRepository = castingSessionAsyncRepository;
        }

        public async Task<CastingSessionFolder> GetCastingSessionFolderByIdWithRolesAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderByIdAsync(CastingSessionFolderParameterIncludeInQueryEnum.Roles, castingSessionFolderId);
        }
        public async Task<CastingSessionFolder> GetCastingSessionFolderByIdWithRolesAndPersonasAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderByIdAsync(CastingSessionFolderParameterIncludeInQueryEnum.RolesAndPersonas, castingSessionFolderId);
        }
        public async Task<CastingSessionFolder> GetCastingSessionFolderByIdWithRolesPersonasAndAssetsAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderByIdAsync(CastingSessionFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets, castingSessionFolderId);
        }

        public async Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdWithRolesAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionFoldersByCastingSessionIdAsync(CastingSessionFolderParameterIncludeInQueryEnum.Roles,  castingSessionId);
        }

        public async Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdWithRolesAndPersonasAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionFoldersByCastingSessionIdAsync(CastingSessionFolderParameterIncludeInQueryEnum.RolesAndPersonas,  castingSessionId);
        }

        public async Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdWithRolesPersonasAndAssetsAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionFoldersByCastingSessionIdAsync(CastingSessionFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets,  castingSessionId);
        }

        public async Task<int> AddNewCastingSessionFolderAsync(bool orderAtTheTop, CastingSessionFolder castingSessionFolder)
        {
            return await _castingSessionRepository.AddNewCastingSessionFolderAsync(orderAtTheTop, castingSessionFolder);
        }

        public async Task<int> UpdateCastingSessionFolderAsync(CastingSessionFolder castingSessionFolder)
        {
            return await _castingSessionRepository.UpdateCastingSessionFolderAsync(castingSessionFolder);
        }
        public async Task<int> DeleteCastingSessionFolder(CastingSessionFolder castingSessionFolder)
        {
            return await _castingSessionRepository.DeleteCastingSessionFolderAsync(castingSessionFolder);
        }
    }
}
