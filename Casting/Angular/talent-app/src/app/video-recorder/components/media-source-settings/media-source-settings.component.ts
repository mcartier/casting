import { Component, OnInit, Input, Inject } from '@angular/core';
import { MediaSourceState, CameraState, ServiceState, MediaSources } from '../../video-recorder';
import { VideoRecorderService } from '../../video-recorder.service';
import { Subscription } from 'rxjs';
import {MatDialogRef, MAT_DIALOG_DATA, MatRadioChange } from '@angular/material';
@Component({
  selector: 'app-media-source-settings',
  templateUrl: './media-source-settings.component.html',
  styleUrls: ['./media-source-settings.component.scss']
})
export class MediaSourceSettingsComponent implements OnInit {

    constructor(private recorderService: VideoRecorderService, public dialogRef: MatDialogRef<MediaSourceSettingsComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }
    audioChange($event: MatRadioChange) {
      this.audioSource = $event.value;
    }
    videoChange($event: MatRadioChange) {
      this.videoSource = $event.value;

    }
    audioOutputChange($event: MatRadioChange) {
     
    }
    saveMediaSourcesClick(): void {
           this.videoSource = this.videoDeviceInfos.find(d => d.deviceId == this.videoSource.deviceId);
            this.audioSource = this.audioDeviceInfos.find(d => d.deviceId == this.audioSource.deviceId);
            if (this.audioSource && this.videoSource) {
              localStorage.setItem('audioSource', JSON.stringify(this.audioSource));
              localStorage.setItem('videoSource', JSON.stringify(this.videoSource));
              this.recorderService.startCapture(this.videoSource, this.audioSource);
            }
      }       
    audioSource: any = null;
    videoSource: any = null;

    private resolutions: any = {};
    private resolutionsColection: any[]=[];

   
    private videoDeviceInfos: any[] = [];
    private audioDeviceInfos: any[] = [];
    private audioOutputDeviceInfos: any[] = [];

    private deviceInfoIsLoading: boolean;
    private deviceInfoIsError: boolean;
    private deviceInfoError: any;

    private camera: MediaStream;
    private cameraIsLoading: boolean;
    private cameraIsError: boolean;
    private cameraError: any;

    private mediaSourcesStateSubscription: Subscription;
    private cameraStateSubscription: Subscription;
    private videoServiceStateSubscription: Subscription;

    private serviceIsLoading: boolean;

    private audioRadioGroup: any;
    private videoRadioGroup: any;
    private audioOutputRadioGroup: any;

    private processMediaSourcesState(mediaSourcesState: MediaSources) {


      this.videoDeviceInfos = mediaSourcesState.videoDeviceInfos;
      this.audioDeviceInfos = mediaSourcesState.audioDeviceInfos;
      this.audioOutputDeviceInfos = mediaSourcesState.audioOutpurDeviceInfos;
      this.deviceInfoIsLoading = mediaSourcesState.isLoading;
      this.deviceInfoError = mediaSourcesState.error;
      this.deviceInfoIsError = mediaSourcesState.isError;
      if (!mediaSourcesState.isError) {

        if (this.videoDeviceInfos.length > 0) {
          //this.videoSource = this.videoDeviceInfos[0];
        }
        if (this.audioDeviceInfos.length > 0) {
          //this.audioSource = this.audioDeviceInfos[0];
        }
        if (this.audioSource && this.videoSource) {
         // this.recorderService.startCapture(this.videoSource, this.audioSource);
        }
      }
    }

    private processCameraState(cameraState: CameraState) {
      this.cameraError = cameraState.error;
      this.cameraIsError = cameraState.isError;
     // this.camera = cameraState.camera;
      this.cameraIsLoading = cameraState.isLoading;

      if (!this.cameraIsError && !this.cameraIsLoading) {
       // localStorage.setItem('audioSource', JSON.stringify(this.audioSource));
       // localStorage.setItem('videoSource', JSON.stringify(this.videoSource));

      }
    }
    private processServiceState(serviceStateIdLoading: boolean) {
      this.serviceIsLoading = serviceStateIdLoading;
    }
    ngOnInit() {
      if (localStorage.getItem('videoSource')) {
        this.videoSource = JSON.parse(localStorage.getItem('videoSource'));
      }

      if (localStorage.getItem('audioSource')) {
        this.audioSource = JSON.parse(localStorage.getItem('audioSource'));
      }

      this.resolutions["qvgaConstraints"] = {
        video: { width: { exact: 320 }, height: { exact: 240 } }
      };

      this.resolutions["vgaConstraints"] = {
        video: { width: { exact: 640 }, height: { exact: 480 } }
      };

      this.resolutions["hdConstraints"] = {
        video: { width: { exact: 1280 }, height: { exact: 720 } }
      };

      this.resolutions["fullHdConstraints"] = {
        video: { width: { exact: 1920 }, height: { exact: 1080 } }
      };

      this.resolutions["fourKConstraints"] = {
        video: { width: { exact: 4096 }, height: { exact: 2160 } }
      };

      this.resolutions["eightKConstraints"] = {
        video: { width: { exact: 7680 }, height: { exact: 4320 } }
      };
      var collection = [];
      for (var key in this.resolutions) {
            collection.push(this.resolutions[key]);
      }
      this.resolutionsColection = collection;
       this.mediaSourcesStateSubscription = this.recorderService.registerForMediaSourcesState()
        .subscribe(mediaSourcesState => this.processMediaSourcesState(mediaSourcesState));

     // this.cameraStateSubscription = this.recorderService.registerForCameraState()
     //   .subscribe(cameraState => this.processCameraState(cameraState));

      this.videoServiceStateSubscription = this.recorderService.registerForServiceState()
          .subscribe(serviceState => this.processServiceState(serviceState));

      this.recorderService.requestDevicesFromSystem();

  }



}
