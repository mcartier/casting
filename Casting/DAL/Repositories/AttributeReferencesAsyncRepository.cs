﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net.Sockets;
using Casting.DAL.Repositories.Interfaces;

using Casting.Model.Enums;
using System.Data.SqlClient;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.DAL.Repositories
{
   

    public class AttributeReferenceAsyncRepository : AsyncRepository,IAttributeReferenceAsyncRepository
    {
        ICastingContext _castingContext;
        public AttributeReferenceAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }
        #region AttributeReferences
        #region languages

        public async Task<int> DeleteLanguageReferenceAsync(int parentId, LanguageReference attributeReference)
        {

            var referenceLanguageCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.Language, attributeReference.LanguageId);
            var attribute = attributeReference.Language;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderLanguageAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.LanguageReferences.Remove(attributeReference);
                    if (referenceLanguageCnt < 2)
                    {
                        _castingContext.Languages.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateLanguageReferenceAsync(int parentId,
            LanguageReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderLanguageAttributeAsync(int parentId,
            int desiredOrderIndex, LanguageReference attributeReference)
        {
            IEnumerable<LanguageReference> attributeReferences = new List<LanguageReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetLanguageReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleLanguageReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                             select (LanguageReference)refs).ToListAsync();

                    break;

                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetLanguageReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileLanguageReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                             select (LanguageReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderLanguageAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderLanguageAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderLanguageAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderLanguageAttributeDownAsync(int desiredOrderIndex, LanguageReference attributeReference,
            IEnumerable<LanguageReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderLanguageAttributeUpAsync(int desiredOrderIndex, LanguageReference attributeReference,
            IEnumerable<LanguageReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexLanguageReferenceByAttributeReferenceTypeAsync(int parentId,
            LanguageReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleLanguageReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileLanguageReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                           }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewLanguageReferenceAsync(bool orderAtTheTop, int parentId,
            LanguageReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexLanguageReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<LanguageReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderLanguageAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewLanguageReferenceAsync(bool orderAtTheTop,
            LanguageReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleLanguageReference)attributeReference).RoleId;
                    break;

                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileLanguageReference)attributeReference).ProfileId;
                    break;
                 }
            return await AddNewLanguageReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<LanguageReference> GetLanguageReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllLanguageReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleLanguageReference> GetRoleLanguageReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllLanguageReferencesQueryable(withAttribute).OfType<RoleLanguageReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);
    
        }
        public async Task<ProfileLanguageReference> GetProfileLanguageReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllLanguageReferencesQueryable(withAttribute).OfType<ProfileLanguageReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<IEnumerable<LanguageReference>> GetLanguageReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetLanguageReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<LanguageReference> GetLanguageReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllLanguageReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleLanguageReference>()
                       where attributeRef.RoleId == parentId
                                      select attributeRef;
                    break;
                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileLanguageReference>()
                        where attributeRef.ProfileId == parentId
                                      select attributeRef;

                    break;
                        }
            return attributeReferences;
        }

        #endregion

        #region accents

        public async Task<int> DeleteAccentReferenceAsync(int parentId, AccentReference attributeReference)
        {

            var referenceAccentCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.Accent, attributeReference.AccentId);
            var attribute = attributeReference.Accent;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderAccentAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.AccentReferences.Remove(attributeReference);
                    if (referenceAccentCnt < 2)
                    {
                        _castingContext.Accents.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateAccentReferenceAsync(int parentId,
            AccentReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderAccentAttributeAsync(int parentId,
            int desiredOrderIndex, AccentReference attributeReference)
        {
            IEnumerable<AccentReference> attributeReferences = new List<AccentReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetAccentReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleAccentReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (AccentReference)refs).ToListAsync();

                    break;

                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetAccentReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileAccentReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (AccentReference)refs).ToListAsync();
                    break;
                        }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderAccentAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderAccentAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderAccentAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderAccentAttributeDownAsync(int desiredOrderIndex, AccentReference attributeReference,
            IEnumerable<AccentReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderAccentAttributeUpAsync(int desiredOrderIndex, AccentReference attributeReference,
            IEnumerable<AccentReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexAccentReferenceByAttributeReferenceTypeAsync(int parentId,
            AccentReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleAccentReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileAccentReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                        }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewAccentReferenceAsync(bool orderAtTheTop, int parentId,
            AccentReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexAccentReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<AccentReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderAccentAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewAccentReferenceAsync(bool orderAtTheTop,
            AccentReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleAccentReference)attributeReference).RoleId;
                    break;

                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileAccentReference)attributeReference).ProfileId;
                    break;
                }
            return await AddNewAccentReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<AccentReference> GetAccentReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllAccentReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<ProfileAccentReference> GetProfileAccentReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllAccentReferencesQueryable(withAttribute).OfType<ProfileAccentReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleAccentReference> GetRoleAccentReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllAccentReferencesQueryable(withAttribute).OfType<RoleAccentReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<IEnumerable<AccentReference>> GetAccentReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetAccentReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<AccentReference> GetAccentReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllAccentReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleAccentReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileAccentReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;
                   }
            return attributeReferences;
        }

        #endregion

        #region unions

        public async Task<int> DeleteUnionReferenceAsync(int parentId, UnionReference attributeReference)
        {

            var referenceUnionCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.Union, attributeReference.UnionId);
            var attribute = attributeReference.Union;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderUnionAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.UnionReferences.Remove(attributeReference);
                    if (referenceUnionCnt < 2)
                    {
                        _castingContext.Unions.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateUnionReferenceAsync(int parentId,
            UnionReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderUnionAttributeAsync(int parentId,
            int desiredOrderIndex, UnionReference attributeReference)
        {
            IEnumerable<UnionReference> attributeReferences = new List<UnionReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                     case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetUnionReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileUnionReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (UnionReference)refs).ToListAsync();
                    break;
                case AttributeReferenceTypeEnum.CastingCall:

                    attributeReferences = await (from refs in GetUnionReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<CastingCallUnionReference>().Where(ar => ar.CastingCallId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (UnionReference)refs).ToListAsync();
                    break;
                case AttributeReferenceTypeEnum.CastingCallRole:

                    attributeReferences = await (from refs in GetUnionReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<CastingCallRoleUnionReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (UnionReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderUnionAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderUnionAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderUnionAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderUnionAttributeDownAsync(int desiredOrderIndex, UnionReference attributeReference,
            IEnumerable<UnionReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderUnionAttributeUpAsync(int desiredOrderIndex, UnionReference attributeReference,
            IEnumerable<UnionReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexUnionReferenceByAttributeReferenceTypeAsync(int parentId,
            UnionReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                 case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileUnionReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case AttributeReferenceTypeEnum.CastingCall:

                        maxOrderIndex = (from attributeRef in _castingContext.CastingCallUnionReferences
                                         where attributeRef.CastingCallId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case AttributeReferenceTypeEnum.CastingCallRole:

                        maxOrderIndex = (from attributeRef in _castingContext.CastingCallRoleUnionReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewUnionReferenceAsync(bool orderAtTheTop, int parentId,
            UnionReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexUnionReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<UnionReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderUnionAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewUnionReferenceAsync(bool orderAtTheTop,
            UnionReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileUnionReference)attributeReference).ProfileId;
                    break;
                case AttributeReferenceTypeEnum.CastingCall:
                    parentId = ((CastingCallUnionReference)attributeReference).CastingCallId;
                    break;
                case AttributeReferenceTypeEnum.CastingCallRole:
                    parentId = ((CastingCallRoleUnionReference)attributeReference).RoleId;
                    break;
            }
            return await AddNewUnionReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<UnionReference> GetUnionReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllUnionReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<ProfileUnionReference> GetProfileUnionReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllUnionReferencesQueryable(withAttribute).OfType<ProfileUnionReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<CastingCallUnionReference> GetCastingCallUnionReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllUnionReferencesQueryable(withAttribute).OfType<CastingCallUnionReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<IEnumerable<UnionReference>> GetUnionReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetUnionReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<UnionReference> GetUnionReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllUnionReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                  case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileUnionReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;
                case AttributeReferenceTypeEnum.CastingCall:
                    attributeReferences = from attributeRef in attributeReferences.OfType<CastingCallUnionReference>()
                                          where attributeRef.CastingCallId == parentId
                                          select attributeRef;

                    break;
                case AttributeReferenceTypeEnum.CastingCallRole:
                    attributeReferences = from attributeRef in attributeReferences.OfType<CastingCallRoleUnionReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;

                    break;
            }
            return attributeReferences;
        }

        #endregion

        #region ethnicities

        public async Task<int> DeleteEthnicityReferenceAsync(int parentId, EthnicityReference attributeReference)
        {

            var referenceEthnicityCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.Ethnicity, attributeReference.EthnicityId);
            var attribute = attributeReference.Ethnicity;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderEthnicityAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.EthnicityReferences.Remove(attributeReference);
                    if (referenceEthnicityCnt < 2)
                    {
                        _castingContext.Ethnicities.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateEthnicityReferenceAsync(int parentId,
            EthnicityReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderEthnicityAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicityReference attributeReference)
        {
            IEnumerable<EthnicityReference> attributeReferences = new List<EthnicityReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetEthnicityReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleEthnicityReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (EthnicityReference)refs).ToListAsync();

                    break;

                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetEthnicityReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileEthnicityReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (EthnicityReference)refs).ToListAsync();
                    break;
                  }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderEthnicityAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderEthnicityAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderEthnicityAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderEthnicityAttributeDownAsync(int desiredOrderIndex, EthnicityReference attributeReference,
            IEnumerable<EthnicityReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderEthnicityAttributeUpAsync(int desiredOrderIndex, EthnicityReference attributeReference,
            IEnumerable<EthnicityReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexEthnicityReferenceByAttributeReferenceTypeAsync(int parentId,
            EthnicityReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleEthnicityReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileEthnicityReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                          }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewEthnicityReferenceAsync(bool orderAtTheTop, int parentId,
            EthnicityReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexEthnicityReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<EthnicityReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderEthnicityAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewEthnicityReferenceAsync(bool orderAtTheTop,
            EthnicityReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleEthnicityReference)attributeReference).RoleId;
                    break;

                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileEthnicityReference)attributeReference).ProfileId;
                    break;
                }
            return await AddNewEthnicityReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<EthnicityReference> GetEthnicityReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEthnicityReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<ProfileEthnicityReference> GetProfileEthnicityReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEthnicityReferencesQueryable(withAttribute).OfType<ProfileEthnicityReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleEthnicityReference> GetRoleEthnicityReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEthnicityReferencesQueryable(withAttribute).OfType<RoleEthnicityReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<IEnumerable<EthnicityReference>> GetEthnicityReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetEthnicityReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<EthnicityReference> GetEthnicityReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllEthnicityReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleEthnicityReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileEthnicityReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;
                }
            return attributeReferences;
        }

        #endregion

        #region ethnicStereotype

        public async Task<int> DeleteEthnicStereoTypeReferenceAsync(int parentId, EthnicStereoTypeReference attributeReference)
        {

            var referenceEthnicStereoTypeCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.EthnicStereoType, attributeReference.EthnicStereoTypeId);
            var attribute = attributeReference.EthnicStereoType;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderEthnicStereoTypeAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.EthnicStereoTypeReferences.Remove(attributeReference);
                    if (referenceEthnicStereoTypeCnt < 2)
                    {
                        _castingContext.EthnicStereoTypes.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateEthnicStereoTypeReferenceAsync(int parentId,
            EthnicStereoTypeReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderEthnicStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, EthnicStereoTypeReference attributeReference)
        {
            IEnumerable<EthnicStereoTypeReference> attributeReferences = new List<EthnicStereoTypeReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetEthnicStereoTypeReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleEthnicStereoTypeReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (EthnicStereoTypeReference)refs).ToListAsync();

                    break;

                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetEthnicStereoTypeReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileEthnicStereoTypeReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (EthnicStereoTypeReference)refs).ToListAsync();
                    break;
                       }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderEthnicStereoTypeAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderEthnicStereoTypeAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderEthnicStereoTypeAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderEthnicStereoTypeAttributeDownAsync(int desiredOrderIndex, EthnicStereoTypeReference attributeReference,
            IEnumerable<EthnicStereoTypeReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderEthnicStereoTypeAttributeUpAsync(int desiredOrderIndex, EthnicStereoTypeReference attributeReference,
            IEnumerable<EthnicStereoTypeReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexEthnicStereoTypeReferenceByAttributeReferenceTypeAsync(int parentId,
            EthnicStereoTypeReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleEthnicStereoTypeReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileEthnicStereoTypeReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                   }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewEthnicStereoTypeReferenceAsync(bool orderAtTheTop, int parentId,
            EthnicStereoTypeReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexEthnicStereoTypeReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<EthnicStereoTypeReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderEthnicStereoTypeAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewEthnicStereoTypeReferenceAsync(bool orderAtTheTop,
            EthnicStereoTypeReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleEthnicStereoTypeReference)attributeReference).RoleId;
                    break;

                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileEthnicStereoTypeReference)attributeReference).ProfileId;
                    break;
                        }
            return await AddNewEthnicStereoTypeReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<EthnicStereoTypeReference> GetEthnicStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEthnicStereoTypeReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<ProfileEthnicStereoTypeReference> GetProfileEthnicStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEthnicStereoTypeReferencesQueryable(withAttribute).OfType<ProfileEthnicStereoTypeReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleEthnicStereoTypeReference> GetRoleEthnicStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEthnicStereoTypeReferencesQueryable(withAttribute).OfType<RoleEthnicStereoTypeReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<IEnumerable<EthnicStereoTypeReference>> GetEthnicStereoTypeReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetEthnicStereoTypeReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<EthnicStereoTypeReference> GetEthnicStereoTypeReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllEthnicStereoTypeReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleEthnicStereoTypeReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileEthnicStereoTypeReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;
                 }
            return attributeReferences;
        }

        #endregion

        #region stereotypes

        public async Task<int> DeleteStereoTypeReferenceAsync(int parentId, StereoTypeReference attributeReference)
        {

            var referenceStereoTypeCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.StereoType, attributeReference.StereoTypeId);
            var attribute = attributeReference.StereoType;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderStereoTypeAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.StereoTypeReferences.Remove(attributeReference);
                    if (referenceStereoTypeCnt < 2)
                    {
                        _castingContext.StereoTypes.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateStereoTypeReferenceAsync(int parentId,
            StereoTypeReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderStereoTypeAttributeAsync(int parentId,
            int desiredOrderIndex, StereoTypeReference attributeReference)
        {
            IEnumerable<StereoTypeReference> attributeReferences = new List<StereoTypeReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetStereoTypeReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleStereoTypeReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (StereoTypeReference)refs).ToListAsync();

                    break;

                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetStereoTypeReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileStereoTypeReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (StereoTypeReference)refs).ToListAsync();
                    break;
                       }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderStereoTypeAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderStereoTypeAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderStereoTypeAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderStereoTypeAttributeDownAsync(int desiredOrderIndex, StereoTypeReference attributeReference,
            IEnumerable<StereoTypeReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderStereoTypeAttributeUpAsync(int desiredOrderIndex, StereoTypeReference attributeReference,
            IEnumerable<StereoTypeReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexStereoTypeReferenceByAttributeReferenceTypeAsync(int parentId,
            StereoTypeReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleStereoTypeReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileStereoTypeReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                      }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewStereoTypeReferenceAsync(bool orderAtTheTop, int parentId,
            StereoTypeReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexStereoTypeReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<StereoTypeReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderStereoTypeAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewStereoTypeReferenceAsync(bool orderAtTheTop,
            StereoTypeReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleStereoTypeReference)attributeReference).RoleId;
                    break;

                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileStereoTypeReference)attributeReference).ProfileId;
                    break;
                   }
            return await AddNewStereoTypeReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<StereoTypeReference> GetStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllStereoTypeReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }

        public async Task<ProfileStereoTypeReference> GetProfileStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllStereoTypeReferencesQueryable(withAttribute).OfType<ProfileStereoTypeReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }

        public async Task<RoleStereoTypeReference> GetRoleStereoTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllStereoTypeReferencesQueryable(withAttribute).OfType<RoleStereoTypeReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }

        public async Task<IEnumerable<StereoTypeReference>> GetStereoTypeReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
         
            return await GetStereoTypeReferencesQueryableForParent(withAttribute,attributeReferenceType,parentId).ToListAsync();
        }

        public IQueryable<StereoTypeReference> GetStereoTypeReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllStereoTypeReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleStereoTypeReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileStereoTypeReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;
                          }
            return attributeReferences;
        }

        #endregion

        #region bodyTypes

        public async Task<int> DeleteBodyTypeReferenceAsync(int parentId, BodyTypeReference attributeReference)
        {

            var referenceBodyTypeCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.BodyType, attributeReference.BodyTypeId);
            var attribute = attributeReference.BodyType;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderBodyTypeAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.BodyTypeReferences.Remove(attributeReference);
                    if (referenceBodyTypeCnt < 2)
                    {
                        _castingContext.BodyTypes.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateBodyTypeReferenceAsync(int parentId,
            BodyTypeReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderBodyTypeAttributeAsync(int parentId,
            int desiredOrderIndex, BodyTypeReference attributeReference)
        {
            IEnumerable<BodyTypeReference> attributeReferences = new List<BodyTypeReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetBodyTypeReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleBodyTypeReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (BodyTypeReference)refs).ToListAsync();

                    break;

             /*   case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetBodyTypeReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileBodyTypeReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (BodyTypeReference)refs).ToListAsync();
                    break;*/
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderBodyTypeAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderBodyTypeAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderBodyTypeAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderBodyTypeAttributeDownAsync(int desiredOrderIndex, BodyTypeReference attributeReference,
            IEnumerable<BodyTypeReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderBodyTypeAttributeUpAsync(int desiredOrderIndex, BodyTypeReference attributeReference,
            IEnumerable<BodyTypeReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexBodyTypeReferenceByAttributeReferenceTypeAsync(int parentId,
            BodyTypeReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleBodyTypeReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                 /*   case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileBodyTypeReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;*/
                }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewBodyTypeReferenceAsync(bool orderAtTheTop, int parentId,
            BodyTypeReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexBodyTypeReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<BodyTypeReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderBodyTypeAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewBodyTypeReferenceAsync(bool orderAtTheTop,
            BodyTypeReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleBodyTypeReference)attributeReference).RoleId;
                    break;

               /* case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileBodyTypeReference)attributeReference).ProfileId;
                    break;*/
            }
            return await AddNewBodyTypeReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<BodyTypeReference> GetBodyTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllBodyTypeReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleBodyTypeReference> GetRoleBodyTypeReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllBodyTypeReferencesQueryable(withAttribute).OfType<RoleBodyTypeReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        /*  public async Task<ProfileBodyTypeReference> GetProfileBodyTypeReferenceByIdAsync(bool withAttribute, int referenceId)
          {
              return await GetAllBodyTypeReferencesQueryable(withAttribute).OfType<ProfileBodyTypeReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

          }*/
        public async Task<IEnumerable<BodyTypeReference>> GetBodyTypeReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetBodyTypeReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<BodyTypeReference> GetBodyTypeReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllBodyTypeReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleBodyTypeReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
              /*  case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileBodyTypeReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;*/
            }
            return attributeReferences;
        }

        #endregion

        #region hairColors

        public async Task<int> DeleteHairColorReferenceAsync(int parentId, HairColorReference attributeReference)
        {

            var referenceHairColorCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.HairColor, attributeReference.HairColorId);
            var attribute = attributeReference.HairColor;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderHairColorAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.HairColorReferences.Remove(attributeReference);
                    if (referenceHairColorCnt < 2)
                    {
                        _castingContext.HairColors.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateHairColorReferenceAsync(int parentId,
            HairColorReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderHairColorAttributeAsync(int parentId,
            int desiredOrderIndex, HairColorReference attributeReference)
        {
            IEnumerable<HairColorReference> attributeReferences = new List<HairColorReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetHairColorReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleHairColorReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (HairColorReference)refs).ToListAsync();

                    break;

              /*  case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetHairColorReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileHairColorReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (HairColorReference)refs).ToListAsync();
                    break;*/
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderHairColorAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderHairColorAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderHairColorAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderHairColorAttributeDownAsync(int desiredOrderIndex, HairColorReference attributeReference,
            IEnumerable<HairColorReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderHairColorAttributeUpAsync(int desiredOrderIndex, HairColorReference attributeReference,
            IEnumerable<HairColorReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexHairColorReferenceByAttributeReferenceTypeAsync(int parentId,
            HairColorReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleHairColorReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                 /*   case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileHairColorReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;*/
                }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewHairColorReferenceAsync(bool orderAtTheTop, int parentId,
            HairColorReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexHairColorReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<HairColorReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderHairColorAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewHairColorReferenceAsync(bool orderAtTheTop,
            HairColorReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleHairColorReference)attributeReference).RoleId;
                    break;

              /*  case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileHairColorReference)attributeReference).ProfileId;
                    break;*/
            }
            return await AddNewHairColorReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<HairColorReference> GetHairColorReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllHairColorReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleHairColorReference> GetRoleHairColorReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllHairColorReferencesQueryable(withAttribute).OfType<RoleHairColorReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
     /*   public async Task<ProfileHairColorReference> GetProfileHairColorReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllHairColorReferencesQueryable(withAttribute).OfType<ProfileHairColorReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }*/
        public async Task<IEnumerable<HairColorReference>> GetHairColorReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetHairColorReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<HairColorReference> GetHairColorReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllHairColorReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleHairColorReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
              /*  case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileHairColorReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;*/
            }
            return attributeReferences;
        }

        #endregion

        #region eyeColors

        public async Task<int> DeleteEyeColorReferenceAsync(int parentId, EyeColorReference attributeReference)
        {

            var referenceEyeColorCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.EyeColor, attributeReference.EyeColorId);
            var attribute = attributeReference.EyeColor;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderEyeColorAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.EyeColorReferences.Remove(attributeReference);
                    if (referenceEyeColorCnt < 2)
                    {
                        _castingContext.EyeColors.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateEyeColorReferenceAsync(int parentId,
            EyeColorReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderEyeColorAttributeAsync(int parentId,
            int desiredOrderIndex, EyeColorReference attributeReference)
        {
            IEnumerable<EyeColorReference> attributeReferences = new List<EyeColorReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetEyeColorReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleEyeColorReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (EyeColorReference)refs).ToListAsync();

                    break;

              /*  case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetEyeColorReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileEyeColorReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                                 select (EyeColorReference)refs).ToListAsync();
                    break;*/
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderEyeColorAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderEyeColorAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderEyeColorAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderEyeColorAttributeDownAsync(int desiredOrderIndex, EyeColorReference attributeReference,
            IEnumerable<EyeColorReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderEyeColorAttributeUpAsync(int desiredOrderIndex, EyeColorReference attributeReference,
            IEnumerable<EyeColorReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexEyeColorReferenceByAttributeReferenceTypeAsync(int parentId,
            EyeColorReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleEyeColorReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                  /*  case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileEyeColorReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;*/
                }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewEyeColorReferenceAsync(bool orderAtTheTop, int parentId,
            EyeColorReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexEyeColorReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<EyeColorReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderEyeColorAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewEyeColorReferenceAsync(bool orderAtTheTop,
            EyeColorReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleEyeColorReference)attributeReference).RoleId;
                    break;

               /* case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileEyeColorReference)attributeReference).ProfileId;
                    break;*/
            }
            return await AddNewEyeColorReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<EyeColorReference> GetEyeColorReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEyeColorReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleEyeColorReference> GetRoleEyeColorReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEyeColorReferencesQueryable(withAttribute).OfType<RoleEyeColorReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
      /*  public async Task<ProfileEyeColorReference> GetProfileEyeColorReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllEyeColorReferencesQueryable(withAttribute).OfType<ProfileEyeColorReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }*/
        public async Task<IEnumerable<EyeColorReference>> GetEyeColorReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetEyeColorReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<EyeColorReference> GetEyeColorReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllEyeColorReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleEyeColorReference>()
                                          where attributeRef.RoleId == parentId
                                          select attributeRef;
                    break;
             /*   case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileEyeColorReference>()
                                          where attributeRef.ProfileId == parentId
                                          select attributeRef;

                    break;*/
            }
            return attributeReferences;
        }

        #endregion

         #region skills

        public async Task<int> DeleteSkillReferenceAsync(int parentId, SkillReference attributeReference)
        {

            var referenceSkillCnt = await GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum.Skill, attributeReference.SkillId);
            var attribute = attributeReference.Skill;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderSkillAttributeAsync(parentId, 0, attributeReference);

                    _castingContext.SkillReferences.Remove(attributeReference);
                    if (referenceSkillCnt < 2)
                    {
                        _castingContext.Skills.Remove(attribute);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdateSkillReferenceAsync(int parentId,
            SkillReference attributeReference)
        {
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderSkillAttributeAsync(int parentId,
            int desiredOrderIndex, SkillReference attributeReference)
        {
            IEnumerable<SkillReference> attributeReferences = new List<SkillReference>();
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = await (from refs in GetSkillReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<RoleSkillReference>().Where(ar => ar.RoleId == parentId).OrderBy(o => o.OrderIndex)
                                             select (SkillReference)refs).ToListAsync();

                    break;

                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = await (from refs in GetSkillReferencesQueryableForParent(false, attributeReference.AttributeReferenceTypeEnum, parentId)
                            .OfType<ProfileSkillReference>().Where(ar => ar.ProfileId == parentId).OrderBy(o => o.OrderIndex)
                                             select (SkillReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderSkillAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex > attributeReference.OrderIndex)
            {
                return await ReOrderSkillAttributeUpAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            else if (desiredOrderIndex < attributeReference.OrderIndex)
            {
                return await ReOrderSkillAttributeDownAsync(desiredOrderIndex, attributeReference, attributeReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderSkillAttributeDownAsync(int desiredOrderIndex, SkillReference attributeReference,
            IEnumerable<SkillReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex >= desiredOrderIndex &&
                    attributeRef.OrderIndex < attributeReference.OrderIndex)
                {
                    attributeRef.OrderIndex++;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderSkillAttributeUpAsync(int desiredOrderIndex, SkillReference attributeReference,
            IEnumerable<SkillReference> attributeReferences)
        {
            foreach (var attributeRef in attributeReferences)
            {
                if (attributeRef.OrderIndex > attributeReference.OrderIndex &&
                    (attributeRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    attributeRef.OrderIndex--;
                    SetEntityState(attributeRef, EntityState.Modified);
                }

            }

            attributeReference.OrderIndex = desiredOrderIndex;
            SetEntityState(attributeReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexSkillReferenceByAttributeReferenceTypeAsync(int parentId,
            SkillReference attributeReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (attributeReference.AttributeReferenceTypeEnum)
                {
                    case AttributeReferenceTypeEnum.Role:
                        maxOrderIndex = (from attributeRef in _castingContext.RoleSkillReferences
                                         where attributeRef.RoleId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case AttributeReferenceTypeEnum.Profile:
                        maxOrderIndex = (from attributeRef in _castingContext.ProfileSkillReferences
                                         where attributeRef.ProfileId == parentId
                                         select attributeRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                           }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewSkillReferenceAsync(bool orderAtTheTop, int parentId,
            SkillReference attributeReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexSkillReferenceByAttributeReferenceTypeAsync(parentId,
                    attributeReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    attributeReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<SkillReference>().Add(attributeReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderSkillAttributeAsync(parentId, 1, attributeReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewSkillReferenceAsync(bool orderAtTheTop,
            SkillReference attributeReference)

        {

            int parentId = 0;
            switch (attributeReference.AttributeReferenceTypeEnum)
            {
                case AttributeReferenceTypeEnum.Role:
                    parentId = ((RoleSkillReference)attributeReference).RoleId;
                    break;

                case AttributeReferenceTypeEnum.Profile:
                    parentId = ((ProfileSkillReference)attributeReference).ProfileId;
                    break;
                 }
            return await AddNewSkillReferenceAsync(orderAtTheTop, parentId, attributeReference);

        }
        public async Task<SkillReference> GetSkillReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllSkillReferencesQueryable(withAttribute).FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<RoleSkillReference> GetRoleSkillReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllSkillReferencesQueryable(withAttribute).OfType<RoleSkillReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);
    
        }
        public async Task<ProfileSkillReference> GetProfileSkillReferenceByIdAsync(bool withAttribute, int referenceId)
        {
            return await GetAllSkillReferencesQueryable(withAttribute).OfType<ProfileSkillReference>().FirstOrDefaultAsync(attributeRef => attributeRef.Id == referenceId);

        }
        public async Task<IEnumerable<SkillReference>> GetSkillReferencesForParentAsync(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {

            return await GetSkillReferencesQueryableForParent(withAttribute, attributeReferenceType, parentId).ToListAsync();
        }
        public IQueryable<SkillReference> GetSkillReferencesQueryableForParent(bool withAttribute, AttributeReferenceTypeEnum attributeReferenceType, int parentId)
        {
            var attributeReferences = GetAllSkillReferencesQueryable(withAttribute);
            switch (attributeReferenceType)
            {
                case AttributeReferenceTypeEnum.Role:
                    attributeReferences = from attributeRef in attributeReferences.OfType<RoleSkillReference>()
                       where attributeRef.RoleId == parentId
                                      select attributeRef;
                    break;
                case AttributeReferenceTypeEnum.Profile:
                    attributeReferences = from attributeRef in attributeReferences.OfType<ProfileSkillReference>()
                        where attributeRef.ProfileId == parentId
                                      select attributeRef;

                    break;
                        }
            return attributeReferences;
        }

        #endregion


        #region forall
        /*
        public async Task<int> DeleteTalentAttributeReference(ProfileAttributeTypeEnum attributeType, int referenceId)
        {

            switch (attributeType)
            {
                case ProfileAttributeTypeEnum.Accent:
                    var haeadshotAttributeRef = await GetAccentAttributeReferenceByIdAsync(referenceId);
                    var referenceHeadCnt = await GetReferenceCountPerAttributeAsync(attributeType, haeadshotAttributeRef.AccentId);
                    _castingContext.AccentReferences.Remove(haeadshotAttributeRef);
                    if (referenceHeadCnt < 2)
                    {
                        _castingContext.Accents.Remove(haeadshotAttributeRef.Accent);
                        //here we delete the actual media file from the disk too!!!!!
                    }

                    break;
                case ProfileAttributeTypeEnum.Union:
                    var resumeAttributeRef = await GetUnionAttributeReferenceByIdAsync(referenceId);
                    var referenceUnionCnt = await GetReferenceCountPerAttributeAsync(attributeType, resumeAttributeRef.UnionId);
                    _castingContext.UnionReferences.Remove(resumeAttributeRef);
                    if (referenceUnionCnt < 2)
                    {
                        _castingContext.Unions.Remove(resumeAttributeRef.Union);
                        //here we delete the actual media file from the disk too!!!!!
                    }
                    break;
                case ProfileAttributeTypeEnum.Ethnicity:
                    var audioAttributeRef = await GetEthnicityAttributeReferenceByIdAsync(referenceId);
                    var referenceEthnicityCnt = await GetReferenceCountPerAttributeAsync(attributeType, audioAttributeRef.EthnicityId);
                    _castingContext.EthnicityReferences.Remove(audioAttributeRef);
                    if (referenceEthnicityCnt < 2)
                    {
                        _castingContext.Ethnicities.Remove(audioAttributeRef.Ethnicity);
                        //here we delete the actual media file from the disk too!!!!!
                    }

                    break;
                case ProfileAttributeTypeEnum.Language:
                    var languageAttributeRef = await GetLanguageAttributeReferenceByIdAsync(referenceId);
                    var referenceLanguageCnt = await GetReferenceCountPerAttributeAsync(attributeType, languageAttributeRef.LanguageId);
                    _castingContext.LanguageReferences.Remove(languageAttributeRef);
                    if (referenceLanguageCnt < 2)
                    {
                        _castingContext.Languages.Remove(languageAttributeRef.Language);
                        //here we delete the actual media file from the disk too!!!!!
                    }

                    break;
            }
            return await _castingContext.SaveChangesAsync();

        }
        */
        public async Task<int> GetReferenceCountPerAttributeAsync(ProfileAttributeTypeEnum attributeType, int attributeId)
        {
            int cnt = 0;
            await Task.Run(() =>
            {
                switch (attributeType)
                {
                    case ProfileAttributeTypeEnum.Language:
                        cnt = _castingContext.LanguageReferences.Count(i => i.LanguageId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.Accent:
                        cnt = _castingContext.AccentReferences.Count(i => i.AccentId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.Union:
                        cnt = _castingContext.UnionReferences.Count(i => i.UnionId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.Ethnicity:
                        cnt = _castingContext.EthnicityReferences.Count(i => i.EthnicityId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.EthnicStereoType:
                        cnt = _castingContext.EthnicStereoTypeReferences.Count(i => i.EthnicStereoTypeId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.StereoType:
                        cnt = _castingContext.StereoTypeReferences.Count(i => i.StereoTypeId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.BodyType:
                        cnt = _castingContext.BodyTypeReferences.Count(i => i.BodyTypeId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.HairColor:
                        cnt = _castingContext.HairColorReferences.Count(i => i.HairColorId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.EyeColor:
                        cnt = _castingContext.EyeColorReferences.Count(i => i.EyeColorId == attributeId);

                        break;
                    case ProfileAttributeTypeEnum.Skill:
                        cnt = _castingContext.SkillReferences.Count(i => i.SkillId == attributeId);

                        break;
                }
            });
            return cnt;
        }
        #endregion


        #region privatemethods

        private IQueryable<LanguageReference> GetAllLanguageReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.LanguageReferences.Include(attribute => attribute.Language) : _castingContext.LanguageReferences) select attributeRef;
        }

        private IQueryable<AccentReference> GetAllAccentReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.AccentReferences.Include(attribute => attribute.Accent) : _castingContext.AccentReferences) select attributeRef;
        }
        private IQueryable<UnionReference> GetAllUnionReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.UnionReferences.Include(attribute => attribute.Union) : _castingContext.UnionReferences) select attributeRef;
        }
        private IQueryable<EthnicityReference> GetAllEthnicityReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.EthnicityReferences.Include(attribute => attribute.Ethnicity) : _castingContext.EthnicityReferences) select attributeRef;
        }
        private IQueryable<EthnicStereoTypeReference> GetAllEthnicStereoTypeReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.EthnicStereoTypeReferences.Include(attribute => attribute.EthnicStereoType) : _castingContext.EthnicStereoTypeReferences) select attributeRef;
        }
        private IQueryable<StereoTypeReference> GetAllStereoTypeReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.StereoTypeReferences.Include(attribute => attribute.StereoType) : _castingContext.StereoTypeReferences) select attributeRef;
        }
        private IQueryable<EyeColorReference> GetAllEyeColorReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.EyeColorReferences.Include(attribute => attribute.EyeColor) : _castingContext.EyeColorReferences) select attributeRef;
        }
        private IQueryable<HairColorReference> GetAllHairColorReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.HairColorReferences.Include(attribute => attribute.HairColor) : _castingContext.HairColorReferences) select attributeRef;
        }
        private IQueryable<BodyTypeReference> GetAllBodyTypeReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.BodyTypeReferences.Include(attribute => attribute.BodyType) : _castingContext.BodyTypeReferences) select attributeRef;
        }
        private IQueryable<SkillReference> GetAllSkillReferencesQueryable(bool withAttribute)
        {
            return from attributeRef in (withAttribute ? _castingContext.SkillReferences.Include(attribute => attribute.Skill) : _castingContext.SkillReferences) select attributeRef;
        }



        #endregion
        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion

    }
}
