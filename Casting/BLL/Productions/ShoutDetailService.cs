﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class ShootDetailService : BaseService, IShootDetailService
    {
        private IProductionAsyncRepository _productionRepository;

        public ShootDetailService(IProductionAsyncRepository productionAsyncRepository)
        {
            _productionRepository = productionAsyncRepository;
        }


        public async Task<ShootDetail> GetShootDetailByIdAsync(int shootDetailId)
        {
            return await _productionRepository.GetShootDetailByIdAsync(shootDetailId, false);
        }
           public async Task<IEnumerable<ShootDetail>> GetShootDetailsByProductionIdAsync(int productionId)
        {
            return await _productionRepository.GetShootDetailsByProductionIdAsync(productionId,false);
        }
        
        public async Task<int> AddNewShootDetailAsync(ShootDetail shootDetail)
        {
            return await _productionRepository.AddNewShootDetailAsync(shootDetail);
        }

        public async Task<int> UpdateShootDetailAsync(ShootDetail shootDetail)
        {
            return await _productionRepository.UpdateShootDetailAsync(shootDetail);
        }

        public async Task<int> DeleteShootDetailAsync(ShootDetail shootDetail)
        {
            return await _productionRepository.DeleteShootDetailAsync(shootDetail);
        }
    }
}
