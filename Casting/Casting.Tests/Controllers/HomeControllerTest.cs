﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Casting;
using Casting.BLL.Productions;
using Casting.Web.Controllers;
using Casting.DAL.Tests;
using Casting.BLL.Productions.Interfaces;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.BLL.Metadata.Interfaces;
using Microsoft.AspNet.Identity;
using Casting.Model.Entities.Users;
using System.Threading.Tasks;

namespace Casting.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private IProductionService _productionService;
        private ICastingCallService _castingCallService;
        private IProductionRoleService _productionRoleService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public HomeControllerTest(UserManager<ApplicationUser> userManager, IMetadataService metadataService, IProductionService productionService, IProductionRoleService productionRoleService, ICastingCallService castingCallService)
        {
            _productionService = productionService;
            _castingCallService = castingCallService;
            _productionRoleService = productionRoleService;
            _metadataService = metadataService;
            _userManager = userManager;
        }

        [TestMethod]
        public async Task Index()
        {
            // Arrange
            //  HomeController controller = new HomeController(new TestCastingContext());

            var role = await _productionRoleService.GetRoleByIdAsync(1);
          foreach (var ste in role.EthnicStereoTypes)
          {
              role.EthnicStereoTypes.Remove(ste);
          }

          var t = await _productionRoleService.UpdateRoleAsync(role);
          // Act
          //  ViewResult result = controller.Index() as ViewResult;

          // Assert
          //  Assert.IsNotNull(result);
          //  Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
