import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { Observable, Subject, Subscription } from 'rxjs';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';

@Component({
  selector: 'app-audition-details-list',
  templateUrl: './audition-details-list.component.html',
  styleUrls: ['./audition-details-list.component.scss']
})
export class AuditionDetailsListComponent implements OnInit {

  constructor(
    private productionExtraService: ProductionExtraService,
    private messageDispatchService: MessageDispatchService
  ) { }

  private newAuditionDetailSubscription: Subscription;
  private deleteAuditionDetailSubscription: Subscription;
  private editAuditionDetailSubscription: Subscription;

  @Input() auditionDetails: AuditionDetail[];

  ngOnInit() {
    this.editAuditionDetailSubscription = this.productionExtraService.subscribeToEditAuditionDetailObs()
      .subscribe(
        (detail: AuditionDetail) => {
        });

    this.newAuditionDetailSubscription = this.productionExtraService.subscribeToCreateAuditionDetailObs()
      .subscribe(
        (detail: AuditionDetail) => {
        });

    this.deleteAuditionDetailSubscription = this.productionExtraService.subscribeToDeleteAuditionDetailObs()
      .subscribe(
        (detail: AuditionDetail) => {
          if (detail) {
          }
        });

  }

  ngOnDestroy() {

    if (this.editAuditionDetailSubscription) {
      this.editAuditionDetailSubscription.unsubscribe();

    }
    if (this.newAuditionDetailSubscription) {
      this.newAuditionDetailSubscription.unsubscribe();

    }
    if (this.deleteAuditionDetailSubscription) {
      this.deleteAuditionDetailSubscription.unsubscribe();

    }
  }

}
