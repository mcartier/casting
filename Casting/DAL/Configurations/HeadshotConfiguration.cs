﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Net.Http.Headers;
using Casting.Model;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;

namespace Casting.DAL.Configurations
{
    class ImageConfiguration : EntityTypeConfiguration<Image>
    {
        public ImageConfiguration()
        {
            HasKey(x => x.Id);
          
        }
    }

    class ImageReferenceConfiguration : EntityTypeConfiguration<ImageReference>
    {
        public ImageReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(m => m.Image);


        }
    }
    class PersonaImageReferenceConfiguration : EntityTypeConfiguration<PersonaImageReference>
    {
        public PersonaImageReferenceConfiguration()
        {
            ToTable("PersonaImageReferences");
            HasKey(x => x.Id);
            HasRequired(m => m.Image);
            HasRequired(p => p.Persona)
                .WithMany(r => r.ImageReferences)
                .HasForeignKey(k => k.PersonaId);


        }
    }
    class BasketImageReferenceConfiguration : EntityTypeConfiguration<BasketImageReference>
    {
        public BasketImageReferenceConfiguration()
        {
            ToTable("BasketImageReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Image);
            HasRequired(p => p.Basket)
                .WithMany(r => r.ImageReferences)
                .HasForeignKey(k => k.BasketId);
        }
    }

    class ProfileImageReferenceConfiguration : EntityTypeConfiguration<ActorProfileImageReference>
    {
        public ProfileImageReferenceConfiguration()
        {
            ToTable("ActorProfileImageReference");
            HasKey(x => x.Id);
            Property(p => p.ProfileId).HasColumnName("ActorProfileId");
            HasRequired(m => m.Image);
            HasRequired(p => p.Profile)
                .WithMany(r => r.ImageReferences)
                .HasForeignKey(k => k.ProfileId);
        }
    }
 /*   class ProfileMainImageReferenceConfiguration : EntityTypeConfiguration<ProfileMainImageReference>
    {
        public ProfileMainImageReferenceConfiguration()
        {
            ToTable("ProfileMainImageReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Image);
            
           // HasRequired(p => p.Profile)
            //    .WithOptional(r => r.MainImageReference);
        }
    }*/
}
;