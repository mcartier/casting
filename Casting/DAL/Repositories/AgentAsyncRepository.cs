﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Infrastructure;
using System.Runtime.InteropServices.WindowsRuntime;

using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.Profiles;
using Casting.Model.Enums;
using SSW.Data.Entities;
using System.Reflection;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;


namespace Casting.DAL.Repositories
{
    public class AgentAsyncRepository : AsyncRepository, IAgentAsyncRepository
    {
        ICastingContext _castingContext;
        public AgentAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Agents

        public async Task<Agent> GetAgentByIdAsync(int agentId)
        {
            return await GetAgentUsersQueryable(false,false,false,false,false,false,false)
                .FirstOrDefaultAsync(t => t.Id == agentId);

        }

        public async Task<Agent> GetAgentByIdAsync(int agentId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice,withAgency,withTalentReferences,withTalents,withManagedTalents,withCoverageCityReferences,withCities)
                .FirstOrDefaultAsync(t => t.Id == agentId);

        }

        public async Task<IEnumerable<Agent>> GetAgentsAsync()
        {
            return await GetAgentUsersQueryable(false, false, false, false, false, false, false)
                .ToListAsync();

        }

        public async Task<IEnumerable<Agent>> GetAgentsAsync(bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice, withAgency, withTalentReferences, withTalents, withManagedTalents, withCoverageCityReferences, withCities)
                .ToListAsync();

        }
        public async Task<IEnumerable<Agent>> GetAgentsByTalentIdAsync(int talentId)
        {
            return await GetAgentUsersQueryable(false, false, false, false, false, false, false)
                .Where(a => a.TalentReferences.Any(t => t.TalentId == talentId))
                .ToListAsync();

        }


        public async Task<IEnumerable<Agent>> GetAgentsByTalentIdAsync(int talentId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice, withAgency, withTalentReferences, withTalents, withManagedTalents, withCoverageCityReferences, withCities)
                .Where(a=>a.TalentReferences.Any(t=>t.TalentId == talentId))
                .ToListAsync();

        }
        public async Task<IEnumerable<Agent>> GetAgentsByCityIdAsync(int cityId)
        {
            return await GetAgentUsersQueryable(false, false, false, false, false, false, false)
                .Where(a => a.CityReferences.Any(t => t.CityId == cityId))
                .ToListAsync();

        }
        public async Task<IEnumerable<Agent>> GetAgentsByCityIdAsync(int cityId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice, withAgency, withTalentReferences, withTalents, withManagedTalents, withCoverageCityReferences, withCities)
                .Where(a => a.CityReferences.Any(t => t.CityId == cityId))
                .ToListAsync();

        }
        public async Task<IEnumerable<Agent>> GetAgentsByCityIdsAsync(List<int> cityIds)
        {
            return await GetAgentUsersQueryable(false, false, false, false, false, false, false)
                .Where(a => a.CityReferences.Any(t => cityIds.Contains(t.CityId)))
                .ToListAsync();

        }

        public async Task<IEnumerable<Agent>> GetAgentsByCityIdsAsync(List<int> cityIds, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice, withAgency, withTalentReferences, withTalents, withManagedTalents, withCoverageCityReferences, withCities)
                .Where(a => a.CityReferences.Any(t => cityIds.Contains(t.CityId)))
                .ToListAsync();

        }
        public async Task<IEnumerable<Agent>> GetAgentsByAgencyOfficeIdAsync(int agencyOfficeId)
        {
            return await GetAgentUsersQueryable(false, false, false, false, false, false, false)
                .Where(a => a.AgencyOfficeId == agencyOfficeId)
                .ToListAsync();

        }

        public async Task<IEnumerable<Agent>> GetAgentsByAgencyOfficeIdAsync(int agencyOfficeId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice, withAgency, withTalentReferences, withTalents, withManagedTalents, withCoverageCityReferences, withCities)
                .Where(a => a.AgencyOfficeId == agencyOfficeId)
                .ToListAsync();

        }

        public async Task<IEnumerable<Agent>> GetAgentsByAgencyIdAsync(int agencyId)
        {
            return await GetAgentUsersQueryable(false, false, false, false, false, false, false)
                .Where(a => a.AgencyOffice.AgencyId == agencyId)
                .ToListAsync();

        }
        public async Task<IEnumerable<Agent>> GetAgentsByAgencyIdAsync(int agencyId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            return await GetAgentUsersQueryable(withAgencyOffice, withAgency, withTalentReferences, withTalents, withManagedTalents, withCoverageCityReferences, withCities)
                .Where(a => a.AgencyOffice.AgencyId == agencyId)
                .ToListAsync();

        }
        public async Task<int> AddNewAgentAsync(Agent agent)
        {
            _castingContext.Agents.Add(agent);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteAgentAsync(Agent agent)
        {
            _castingContext.Agents.Remove(agent);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateAgentAsync(Agent agent)
        {
            SetEntityState(agent, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion

        #region Agencies

        public async Task<Agency> GetAgencyByIdAsync(int agencyId)
        {
            return await GetAgenciesQueryable(false,false)
                .FirstOrDefaultAsync(t => t.Id == agencyId);

        }

        public async Task<Agency> GetAgencyByIdAsync(int agencyId, bool withAgents, bool withOffices)
        {
            return await GetAgenciesQueryable(withAgents,withOffices)
                .FirstOrDefaultAsync(t => t.Id == agencyId);

        }

        public async Task<IEnumerable<Agency>> GetAgenciesAsync()
        {
            return await GetAgenciesQueryable(false, false)
                .ToListAsync();

        }

        public async Task<IEnumerable<Agency>> GetAgenciesAsync(bool withAgents, bool withOffices)
        {
            return await GetAgenciesQueryable(withAgents, withOffices)
                .ToListAsync();

        }

        public async Task<int> AddNewAgencyAsync(Agency agency)
        {
            _castingContext.Agencies.Add(agency);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteAgencyAsync(Agency agency)
        {
            _castingContext.Agencies.Remove(agency);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateAgencyAsync(Agency agency)
        {
            SetEntityState(agency, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion

        #region AgencyOffices

        public async Task<AgencyOffice> GetAgencyOfficeByIdAsync(int agencyOfficeId)
        {
            return await GetAgencyOfficesQueryable()
                .FirstOrDefaultAsync(t => t.Id == agencyOfficeId);

        }

        public async Task<IEnumerable<AgencyOffice>> GetAgencyOfficesAsync()
        {
            return await GetAgencyOfficesQueryable().ToListAsync();

        }

        public async Task<int> AddNewAgencyOfficeAsync(AgencyOffice agencyOffice)
        {
            _castingContext.AgencyOffices.Add(agencyOffice);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteAgencyOfficeAsync(AgencyOffice agencyOffice)
        {
            _castingContext.AgencyOffices.Remove(agencyOffice);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateAgencyOfficeAsync(AgencyOffice agencyOffice)
        {
            SetEntityState(agencyOffice, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion

        #region AgentCityReferences

        public async Task<AgentCityReference> GetAgentCityReferenceByIdAsync(int agentCityReferenceId)
        {
            return await GetAgentCityReferencesQueryable()
                .FirstOrDefaultAsync(t => t.Id == agentCityReferenceId);

        }

        public async Task<IEnumerable<AgentCityReference>> GetAgentCityReferencesAsync()
        {
            return await GetAgentCityReferencesQueryable().ToListAsync();

        }

        public async Task<int> AddNewAgentCityReferenceAsync(AgentCityReference agentCityReference)
        {
            _castingContext.AgentCityReferences.Add(agentCityReference);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteAgentCityReferenceAsync(AgentCityReference agentCityReference)
        {
            _castingContext.AgentCityReferences.Remove(agentCityReference);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateAgentCityReferenceAsync(AgentCityReference agentCityReference)
        {
            SetEntityState(agentCityReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion

        #region AgentTalentReferences

        public async Task<AgentTalentReference> GetAgentTalentReferenceByIdAsync(int agentTalentReferenceId, bool withAgent, bool withTalent)
        {
            return await GetAgentTalentReferencesQueryable(withAgent,withTalent)
                .FirstOrDefaultAsync(t => t.Id == agentTalentReferenceId);

        }

        public async Task<IEnumerable<AgentTalentReference>> GetAgentTalentReferencesByAgentIdAsync(int agentId, bool withAgent, bool withTalent)
        {
            return await GetAgentTalentReferencesQueryable(withAgent, withTalent)
                .Where(r => r.AgentId == agentId)
                .ToListAsync();

        }
        public async Task<IEnumerable<AgentTalentReference>> GetAgentTalentReferencesByTalentIdAsync(int talentId, bool withAgent, bool withTalent)
        {
            return await GetAgentTalentReferencesQueryable(withAgent, withTalent)
                .Where(r=>r.TalentId == talentId)
                .ToListAsync();

        }

        public async Task<int> AddNewAgentTalentReferenceAsync(AgentTalentReference agentTalentReference)
        {
            _castingContext.AgentTalentReferences.Add(agentTalentReference);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> DeleteAgentTalentReferenceAsync(AgentTalentReference agentTalentReference)
        {
            _castingContext.AgentTalentReferences.Remove(agentTalentReference);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> UpdateAgentTalentReferenceAsync(AgentTalentReference agentTalentReference)
        {
            SetEntityState(agentTalentReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        #endregion



        #region PrivateMethods


        private IQueryable<Agent> GetAgentUsersQueryable(bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities)
        {
            var users = from user in _castingContext.Agents
                select user;
            if (withCoverageCityReferences)
            {
                if (withCities)
                {
                    users.Include("CityReferences.City");
                }
                else
                {
                    users.Include(c => c.CityReferences);
                }
            }

            if (withTalentReferences)
            {
                if (withTalents)
                {
                    users.Include("TalentReferences.Talent");
                }
                else
                {
                    users.Include(t => t.TalentReferences);
                }
            }
            if (withManagedTalents)
            {
               
                    users.Include(t => t.ManagedTalents);
            
            }

            if (withAgencyOffice)
            {
                if (withAgency)
                {
                    users.Include("AgencyOffice.Agency");
                }
                else
                {
                    users.Include(o => o.AgencyOffice);
                }
            }
            return users;
        }


        private IQueryable<Agency> GetAgenciesQueryable(bool withAgents, bool withOffices)
        {
            var agencys = from agency in _castingContext.Agencies
                select agency;
            if (withAgents)
            {
                agencys.Include(a => a.Agents);
            }

            if (withOffices)
            {
                agencys.Include(o => o.Offices);
            }

            return agencys;
        }

        private IQueryable<AgencyOffice> GetAgencyOfficesQueryable()
        {
            var agencyOffices = from agencyOffice in _castingContext.AgencyOffices
                select agencyOffice;


            return agencyOffices;
        }

        private IQueryable<AgentTalentReference> GetAgentTalentReferencesQueryable(bool withAgent, bool withTalent)
        {
            var talentReferences = from talentReference in _castingContext.AgentTalentReferences
                select talentReference;
            if (withAgent)
            {
                talentReferences.Include(a => a.Agent);
            }

            if (withTalent)
            {
                talentReferences.Include(t => t.Talent);
            }
            return talentReferences;
        }
        private IQueryable<AgentCityReference> GetAgentCityReferencesQueryable()
        {
            var cityReferences = from cityReference in _castingContext.AgentCityReferences
                select cityReference;


            return cityReferences;
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }
                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public IEnumerable<Agent> GetComplete()
        {
            throw new NotImplementedException();
        }


        #endregion

    }
}
