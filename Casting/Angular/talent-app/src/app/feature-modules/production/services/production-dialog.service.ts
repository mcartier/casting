import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { ProductionRoleTransferObject, Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../models/production.models';
import { AuditionDetail, ShootDetail, PayDetail, Side } from '../../../models/production.models';

@Injectable({
  providedIn: 'root'
})
export class AuditionDetailDialogObject {
  public auditionDetail: AuditionDetail;
  public isVisible: boolean;
}
export class NewRoleDialogObject {
  public role: Role;
  public isVisible: boolean;
}
export class ShootDetailDialogObject {
    public shootDetail: ShootDetail;
  public isVisible: boolean;
}
export class PayDetailDialogObject {
    public payDetail: PayDetail;
  public isVisible: boolean;
}
export class SideDialogObject {
    public side: Side;
  public isVisible: boolean;
}
export class ProductionDialogService {

    constructor() { }
    private _sidesDialogIsVisible: boolean;
    private _sidesDialogShowStateObs: Subject<SideDialogObject> = new Subject<SideDialogObject>();
    
    private _payDetailsDialogIsVisible: boolean;
    private _payDetailsDialogShowStateObs: Subject<PayDetailDialogObject> = new Subject<PayDetailDialogObject>();
   
    private _shootDetailsDialogIsVisible: boolean;
    private _shootDetailsDialogShowStateObs: Subject<ShootDetailDialogObject> = new Subject<ShootDetailDialogObject>();
   
    private _auditionDetailsDialogIsVisible: boolean;
    private _auditionDetailsDialogShowStateObs: Subject<AuditionDetailDialogObject> = new Subject<AuditionDetailDialogObject>();

  private _newRoleDialogIsVisible: boolean;
  private _newRoleDialogShowStateObs: Subject<NewRoleDialogObject> = new Subject<NewRoleDialogObject>();

  public readonly newRoleDialogShowStateObs: Observable<NewRoleDialogObject> = this._newRoleDialogShowStateObs.asObservable();

  public subscribeToNewRoleDialogShowStateObs(): Observable<NewRoleDialogObject> {
    return this._newRoleDialogShowStateObs;
  }

    public readonly auditionDetailsDialogShowStateObs: Observable<AuditionDetailDialogObject> = this._auditionDetailsDialogShowStateObs.asObservable();

    public subscribeToAuditionDetailsDialogShowStateObs(): Observable<AuditionDetailDialogObject> {
      return this._auditionDetailsDialogShowStateObs;
    }
   
    

    public readonly shootDetailsDialogShowStateObs: Observable<ShootDetailDialogObject> = this._shootDetailsDialogShowStateObs.asObservable();

    public subscribeToShootDetailsDialogShowStateObs(): Observable<ShootDetailDialogObject> {
      return this._shootDetailsDialogShowStateObs;
    }
    
   
    public readonly payDetailsDialogShowStateObs: Observable<PayDetailDialogObject> = this._payDetailsDialogShowStateObs.asObservable();

    public subscribeToPayDetailsDialogShowStateObs(): Observable<PayDetailDialogObject> {
      return this._payDetailsDialogShowStateObs;
    }
    
    
    public readonly sidesDialogShowStateObs: Observable<SideDialogObject> = this._sidesDialogShowStateObs.asObservable();

    public subscribeToSidesDialogShowStateObs(): Observable<SideDialogObject> {
      return this._sidesDialogShowStateObs;
    }

  
    

    public showSidesDialog(side:Side) {
      this._sidesDialogIsVisible = true;
        this._sidesDialogShowStateObs.next({side:side, isVisible:this._sidesDialogIsVisible});
    }
    public hideSidesDialog() {
      this._sidesDialogIsVisible = false;
        this._sidesDialogShowStateObs.next({ side: null, isVisible: this._sidesDialogIsVisible })
    }
    public toggleSidesDialog() {
      this._sidesDialogIsVisible = !this._sidesDialogIsVisible;
        this._sidesDialogShowStateObs.next({ side: null, isVisible: this._sidesDialogIsVisible })
    }



    public showPayDetailsDialog(payDetail:PayDetail) {
      this._payDetailsDialogIsVisible = true;
        this._payDetailsDialogShowStateObs.next({ payDetail: payDetail, isVisible: this._payDetailsDialogIsVisible })
    }
    public hidePayDetailsDialog() {
      this._payDetailsDialogIsVisible = false;
      this._payDetailsDialogShowStateObs.next({ payDetail: null, isVisible: this._payDetailsDialogIsVisible })
    }
    public togglePayDetailsDialog() {
      this._payDetailsDialogIsVisible = !this._payDetailsDialogIsVisible;
      this._payDetailsDialogShowStateObs.next({ payDetail: null, isVisible: this._payDetailsDialogIsVisible })
    }


   
    public showShootDetailsDialog(shootDetail:ShootDetail) {
      this._shootDetailsDialogIsVisible = true;
        this._shootDetailsDialogShowStateObs.next({ shootDetail: shootDetail, isVisible: this._shootDetailsDialogIsVisible })
    }
    public hideShootDetailsDialog() {
      this._shootDetailsDialogIsVisible = false;
        this._shootDetailsDialogShowStateObs.next({ shootDetail: null, isVisible: this._shootDetailsDialogIsVisible })
    }
    public toggleShootDetailsDialog() {
      this._shootDetailsDialogIsVisible = !this._shootDetailsDialogIsVisible;
        this._shootDetailsDialogShowStateObs.next({ shootDetail: null, isVisible: this._shootDetailsDialogIsVisible })
    }


   
  public showAuditionDetailsDialog(auditionDetail: AuditionDetail) {
    this._auditionDetailsDialogIsVisible = true;
    this._auditionDetailsDialogShowStateObs.next({ auditionDetail: auditionDetail, isVisible: this._auditionDetailsDialogIsVisible })
  }
  public hideAuditionDetailsDialog() {
    this._auditionDetailsDialogIsVisible = false;
    this._auditionDetailsDialogShowStateObs.next({ auditionDetail: null, isVisible: this._auditionDetailsDialogIsVisible })
  }
  public toggleAuditionDetailsDialog() {
    this._auditionDetailsDialogIsVisible = !this._auditionDetailsDialogIsVisible;
    this._auditionDetailsDialogShowStateObs.next({ auditionDetail: null, isVisible: this._auditionDetailsDialogIsVisible })
  }

  public showNewRolesDialog(newRole: Role) {
    this._newRoleDialogIsVisible = true;
    this._newRoleDialogShowStateObs.next({ role: newRole, isVisible: this._newRoleDialogIsVisible })
  }
  public hideNewRolesDialog() {
    this._newRoleDialogIsVisible = false;
    this._newRoleDialogShowStateObs.next({ role: null, isVisible: this._newRoleDialogIsVisible })
  }
  public toggleNewRolesDialog() {
    this._newRoleDialogIsVisible = !this._newRoleDialogIsVisible;
    this._newRoleDialogShowStateObs.next({ role: null, isVisible: this._newRoleDialogIsVisible })
  }

    public uploadSide() {

    }
    public pasteAndSaveSide() {

    }

}

