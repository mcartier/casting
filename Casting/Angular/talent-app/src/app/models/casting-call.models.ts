﻿import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from './metadata';
export class CastingCallRoleTransferObject {
    public castingCall: CastingCall;
    public role:CastingCallRole;
}
export class BaseCastingCall {
  
   public name: string;
   public castingCallType: CastingCallTypeEnum;
    public castingCallViewType: CastingCallTypeEnum;
     public expires:Date;
    public instructions: string;
    public acceptSubmissions: boolean;
   public productionId: number;
    public production: any;


   
}
export class CastingCall extends BaseCastingCall {
  public unions:any[];
    public id: number;
     public roles: CastingCallRole[] = [];
    public cityTalentLocations: any[]
    public regionTalentLocations: any[]
    public countryTalentLocations: any[]

}
export class NewCastingCall {
  public name: string;
  public productionId: number;
  public castingCallType: CastingCallTypeEnum;
}
export class UpdateCastingCall extends BaseCastingCall {
    public id: number;
}
export class BaseCastingCallRole {
  public castingCallId: number;
  public roleId: number;
  public description: string;
  public name: string;
  public maxAge: number;
  public minAge: number;
  public requiresNudity: boolean;
  public requiresHeadshot: boolean;
  public headshotMaxCount: number;
  public requiresVideo: boolean;
  public videoMaxCount: number;
  public requiresAudio: boolean;
  public audioMaxCount: number;
  public requiresCover: boolean;
  public transgender: boolean;
  public roleTypeId: number;
  public roleGenderTypeId: number;
  public ethnicityId: number;
  public ethnicity: MetadataItem;
  public stereoTypes: MetadataItem[];
  public ethnicStereoTypes: MetadataItem[];
  public roleType: MetadataItem;
  public roleGenderType: MetadataItem;

}
export class CastingCallRole extends BaseCastingCallRole {
  public castingCall: CastingCall;
  public id: number;

}

export class NewCastingCallRole extends BaseCastingCallRole {

  public castingCallId: number;
  public roleId: number;

}
export class UpdateCastingCallRole extends BaseCastingCallRole {

  public id: number;

}

export class BaseCastingCallFolder {
  public name: string;
}
export class CastingCallFolder extends BaseCastingCallFolder {
  public id: number;
  public castingCallId: number;
  public castingCall: CastingCall;
  public folderRoles: CastingCallFolderRole[] = [];

}

export class NewCastingCallFolder extends BaseCastingCallFolder {
  public castingCallId: number;



}
export class UpdateCastingCallFolder extends BaseCastingCallFolder {

  public id: number;

}

export class CastingCallFolderRole {
  public id: number;
  public castingCallId: number;
    public castingCall:CastingCall;
  public castingCallFolderId: number;
   public castingCallFolder: CastingCallFolder;
   public castingCallRoleId: number;
  public castingCallRole: CastingCallRole;

}

export class NewCastingCallFolderRole {
  public castingCallId: number;
  public castingCallRoleId: number;
  public castingCallFolderId: number;

}

export enum CastingCallTypeEnum {
    publicAll = 1,

    publicMembers = 3,

    privateSelectMembers = 7
}
export enum CastingCallUnionStatusEnum {
    notRelevan = 0,

    nonUnion = 1,

    union = 3,

    both = 7
}

export class castingCallLocationReference {
    public id: number;
    public name: string;
    public locationId: number;
    public locationType:LocationTypeEnum;
}

export enum LocationTypeEnum {
  na = 0,
  city = 1,
    region = 3,
  country = 7,
}