﻿

using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using tusdotnet.Models;

// ReSharper disable once CheckNamespace
namespace tusdotnet
{
    /// <summary>
    /// Extension methods for adding tusdotnet to an application pipeline.
    /// </summary>
    public static class TusAppBuilderExtensions
    {
        public static IAppBuilder UseTus(
            this IAppBuilder builder,
            Func<IOwinRequest, DefaultTusConfiguration> configFactory)
        {
            return builder.UseTus((IOwinRequest ctx) =>
            {
                return Task.FromResult<DefaultTusConfiguration>(configFactory(ctx));
            });
            
        }
        

        public static IAppBuilder UseTus(
            this IAppBuilder builder,
            Func<IOwinRequest, Task<DefaultTusConfiguration>> configFactory)
        {
            return builder.Use<TusOwinMiddleware>((object)configFactory);
        }
    }
}

