﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Guests;

namespace Casting.DAL.Repositories.Interfaces
{ 
    public interface IGuestAsyncRepository
    { 
        Task<Guest> GetGuestByIdAsync(int guestId);
        Task<IEnumerable<Guest>> GetGuestsAsync();
        Task<int> AddNewGuestAsync(Guest guest);
        Task<int> DeleteGuestAsync(Guest guest);
        Task<int> UpdateGuestAsync(Guest guest);
    }
}