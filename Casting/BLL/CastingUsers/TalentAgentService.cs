﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Talents;

namespace Casting.BLL.CastingUsers
{

   public class TalentAgentService : BaseService, ITalentAgentService
    {
        private IAgentAsyncRepository _agentRepository;

        public TalentAgentService(IAgentAsyncRepository talentAgentAsyncRepository)
        {
            _agentRepository = talentAgentAsyncRepository;
        }

        public async Task<AgentTalentReference> GetTalentAgentReferenceByIdAsync(int referenceId)
        {
            return await _agentRepository.GetAgentTalentReferenceByIdAsync(referenceId, false, false);
        }


        public async Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByAgentIdAsync(int agentId)
        {
            return await _agentRepository.GetAgentTalentReferencesByAgentIdAsync(agentId, false, false);
        }

        public async Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByAgentIdWithAgentAndTalentAsync(
            int agentId)
        {
            return await _agentRepository.GetAgentTalentReferencesByAgentIdAsync(agentId, true, true);
        }

        public async Task<AgentTalentReference> GetTalentAgentReferenceByIdWithAgentAndTalentAsync(int referenceId)
        {
            return await _agentRepository.GetAgentTalentReferenceByIdAsync(referenceId, true, true);
        }

        public async Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByTalentIdAsync(int talenId)
        {
            return await _agentRepository.GetAgentTalentReferencesByTalentIdAsync(talenId, false, false);
        }

        public async Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByTalentIdWithAgentAndTalentAsync(
            int talenId)
        {
            return await _agentRepository.GetAgentTalentReferencesByTalentIdAsync(talenId, true, true);
        }

        public async Task<int> AddNewAgentTalentReferenceAsync(AgentTalentReference agentTalentReference)
        {
            return await _agentRepository.AddNewAgentTalentReferenceAsync(agentTalentReference);
        }

        public async Task<int> UpdateAgentTalentReferenceAsync(AgentTalentReference agentTalentReference)
        {
            return await _agentRepository.UpdateAgentTalentReferenceAsync(agentTalentReference);
        }

        public async Task<int> DeleteAgentTalentReference(AgentTalentReference agentTalentReference)
        {
            return await _agentRepository.DeleteAgentTalentReferenceAsync(agentTalentReference);
        }
    }


}