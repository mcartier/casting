﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Metadata;

//using Casting.Model.Entities.SearchCriterias.ProfileAttributes;

namespace Casting.Model.Entities.Profiles.SearchCriterias
{
    public abstract class ProfileSearchCriteria
    {
        public ProfileSearchCriteria()
        {
         //   Accents = new List<ProfileSearchCriteriaAccentReference>();
         //   Unions = new List<ProfileSearchCriteriaUnionReference>();
         //   Languages = new List<ProfileSearchCriteriaLanguageReference>();

        }
        public int Id { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
     //   public ICollection<ProfileSearchCriteriaUnionReference> Unions { get; set; }
        public bool Transgender { get; set; }
      //  public ICollection<ProfileSearchCriteriaAccentReference> Accents { get; set; }
     //   public ICollection<ProfileSearchCriteriaLanguageReference> Languages { get; set; }
        public RoleGenderType Gender { get; set; }

    }
}
