﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Casting.Web.Models
{
    public class MetadaBindingModels
    {
        [Required] public int Value { get; set; }
    }
}