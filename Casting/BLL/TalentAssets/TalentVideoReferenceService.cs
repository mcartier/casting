﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Enums;
using Casting.BLL.TalentAssets.Interfaces;


namespace Casting.BLL.TalentAssets
{
    public class TalentVideoReferenceService : BaseService, ITalentVideoReferenceService
    {
        private ITalentAssetAsyncRepository _assetRepository;

        public TalentVideoReferenceService(ITalentAssetAsyncRepository assetRepository)
        {
            _assetRepository = assetRepository;
        }

        public async Task<VideoReference> GetVideoReferenceByIdAsync(int videoId)
        {
            var video = await _assetRepository.GetVideoAssetReferenceByIdAsync(true, videoId);
            return video;
        }
        public async Task<IEnumerable<VideoReference>> GetProfileVideoReferencesForParentAsync(int profileId, bool withAssets)
        {
            var videoReferences = await _assetRepository.GetVideoReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Profile, profileId);
            return videoReferences;
        }
        public async Task<IEnumerable<VideoReference>> GetPersonaVideoReferencesForParentAsync(int personaId, bool withAssets)
        {
            var videoReferences = await _assetRepository.GetVideoReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Persona, personaId);
            return videoReferences;
        }
        public async Task<IEnumerable<VideoReference>> GetBasketVideoReferencesForParentAsync(int basketId, bool withAssets)
        {
            var videoReferences = await _assetRepository.GetVideoReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Basket, basketId);
            return videoReferences;
        }
             public async Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            VideoReference assetReference)
        {
            return await _assetRepository.AddNewVideoAssetReferenceAsync(orderAtTheTop,assetReference);
        }

        public async Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            int parentId, VideoReference assetReference)
        {
            return await _assetRepository.AddNewVideoAssetReferenceAsync(orderAtTheTop, parentId, assetReference);
        }

        public async Task<int> AddNewProfileVideoAssetReferenceAsync(bool orderAtTheTop, ActorProfileVideoReference assetReference)
        {
            return await AddNewVideoAssetReferenceAsync(orderAtTheTop, assetReference.ProfileId, assetReference);
        }
        public async Task<int> AddNewPersonaVideoAssetReferenceAsync(bool orderAtTheTop, PersonaVideoReference assetReference)
        {
            return await AddNewVideoAssetReferenceAsync(orderAtTheTop, assetReference.PersonaId, assetReference);
        }
        public async Task<int> AddNewBasketVideoAssetReferenceAsync(bool orderAtTheTop, BasketVideoReference assetReference)
        {
            return await AddNewVideoAssetReferenceAsync(orderAtTheTop, assetReference.BasketId, assetReference);
        }

        public async Task<int> DeleteVideoAssetReferenceAsync(int parentId, VideoReference assetReference)
        {


            return await _assetRepository.DeleteVideoAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> DeleteProfileVideoAssetReferenceAsync(ActorProfileVideoReference assetReference)
        {


            return await DeleteVideoAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> DeleteBasketVideoAssetReferenceAsync(BasketVideoReference assetReference)
        {


            return await DeleteVideoAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> DeletePersonaVideoAssetReferenceAsync(PersonaVideoReference assetReference)
        {


            return await DeleteVideoAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }
        public async Task<int> UpdateVideoAssetReferenceAsync(int parentId,
            VideoReference assetReference)
        {
            return await _assetRepository.UpdateVideoAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> UpdateProfileVideoAssetReferenceAsync(ActorProfileVideoReference assetReference)
        {
            return await UpdateVideoAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> UpdateBasketVideoAssetReferenceAsync(BasketVideoReference assetReference)
        {
            return await UpdateVideoAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> UpdatePersonaVideoAssetReferenceAsync(PersonaVideoReference assetReference)
        {
            return await UpdateVideoAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }

        public async Task<int> ReOrderVideoAssetAsync(int parentId,
            int desiredOrderIndex, VideoReference assetReference)


        {
            return await _assetRepository.ReOrderVideoAssetAsync(parentId, desiredOrderIndex, assetReference);
        }

        public async Task<int> ReOrderProfileVideoAssetAsync(int desiredOrderIndex, ActorProfileVideoReference assetReference)


        {
            return await ReOrderVideoAssetAsync(assetReference.ProfileId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderBasketVideoAssetAsync(int desiredOrderIndex, BasketVideoReference assetReference)


        {
            return await ReOrderVideoAssetAsync(assetReference.BasketId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderPersonaVideoAssetAsync(int desiredOrderIndex, PersonaVideoReference assetReference)


        {
            return await ReOrderVideoAssetAsync(assetReference.PersonaId, desiredOrderIndex, assetReference);
        }
    }
}
