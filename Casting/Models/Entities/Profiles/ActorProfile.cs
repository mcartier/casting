﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Metadata;

using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Entities.Attributes;

namespace Casting.Model.Entities.Profiles
{
    public class ActorProfile: VoiceActorProfile
    {
        public ActorProfile()
        {
           // base.ProfileType = ProfileTypeEnum.Actor;
            Ethnicities = new List<ProfileEthnicityReference>();
            StereoTypes = new List<ProfileStereoTypeReference>();
            EthnicStereoTypes = new List<ProfileEthnicStereoTypeReference>();
            VideoReferences = new List<ActorProfileVideoReference>();
            ImageReferences = new List<ActorProfileImageReference>();
        }

        public string Tagline { get; set; }
        public bool HasDriversLicense { get; set; }
       
        public HairColor HairColor { get; set; }

        public int? HairColorId { get; set; }

        public EyeColor EyeColor { get; set; }
        public int? EyeColorId { get; set; }

        public BodyType BodyType { get; set; }
        public int? BodyTypeId { get; set; }
        public int Height { get; set; }

        public int Weight { get; set; }
        public ICollection<ProfileEthnicityReference> Ethnicities { get; set; }
        public ICollection<ProfileStereoTypeReference> StereoTypes { get; set; }
        public ICollection<ProfileEthnicStereoTypeReference> EthnicStereoTypes { get; set; }
        public ICollection<ActorProfileVideoReference> VideoReferences { get; set; }
        public ICollection<ActorProfileImageReference> ImageReferences { get; set; }

        public OrderByValuesEnum ResumeReferencesSortOrderI { get; set; }
        public OrderByValuesEnum VideoReferencesSortOrderI { get; set; }
        public OrderByValuesEnum ImageReferencesSortOrderI { get; set; }

        public OrderByValuesEnum AudioReferencesSortOrderI { get; set; }

    }
}
