﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions
{
   public class Side : Logger
    {
        public Side()
        {
          

        }
    public int Id { get; set; }
      
        public int ProductionId { get; set; }
        public Production Production { get; set; }

        public string Name { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }
        public string FileId { get; set; }
        public string PastedText { get; set; }
        
    }
}
