﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;

namespace Casting.DAL.Configurations
{
    class VideoConfiguration : EntityTypeConfiguration<Video>
    {
        public VideoConfiguration()
        {
            HasKey(x => x.Id);
         
        }
    }

    class VideoReferenceConfiguration : EntityTypeConfiguration<VideoReference>
    {
        public VideoReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(m => m.Video);
        }
    }

    class PersonaVideoReferenceConfiguration : EntityTypeConfiguration<PersonaVideoReference>
    {
        public PersonaVideoReferenceConfiguration()
        {
            ToTable("PersonaVideoReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Video);
            HasRequired(p => p.Persona)
                .WithMany(r => r.VideoReferences)
                .HasForeignKey(k => k.PersonaId);


        }
    }
    class BasketVideoReferenceConfiguration : EntityTypeConfiguration<BasketVideoReference>
    {
        public BasketVideoReferenceConfiguration()
        {
            ToTable("BasketVideoReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Video);
            HasRequired(p => p.Basket)
                .WithMany(r => r.VideoReferences)
                .HasForeignKey(k => k.BasketId);
        }
    }

    class ProfileVideoReferenceConfiguration : EntityTypeConfiguration<ActorProfileVideoReference>
    {
        public ProfileVideoReferenceConfiguration()
        {
            ToTable("ActorProfileVideoReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Video);
            HasRequired(p => p.Profile)
                .WithMany(r => r.VideoReferences)
                .HasForeignKey(k => k.ProfileId);
        }
    }
}
;