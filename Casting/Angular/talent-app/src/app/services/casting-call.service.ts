import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CastingCall, UpdateCastingCall, NewCastingCall } from '../models/casting-call.models';
import { WINDOW } from './browser.window.provider';
import { HttpHelpers } from '../utilities/HttpHelpers';
@Injectable({
    providedIn: 'root'
})
export class CastingCallService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }


    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;

    private _deleteCastingCallObs: Subject<CastingCall> = new Subject<CastingCall>();
    public readonly deleteCastingCallObs: Observable<CastingCall> = this._deleteCastingCallObs.asObservable();

    private _editCastingCallObs: Subject<CastingCall> = new Subject<CastingCall>();
    public readonly editCastingCallObs: Observable<CastingCall> = this._editCastingCallObs.asObservable();

    private _newCastingCallObs: Subject<CastingCall> = new Subject<CastingCall>();
    public readonly newCastingCallObs: Observable<CastingCall> = this._newCastingCallObs.asObservable();

    public subscribeToCreateCastingCallObs(): Observable<CastingCall> {
        return this.newCastingCallObs;
    }
    public subscribeToEditCastingCallObs(): Observable<CastingCall> {
        return this.editCastingCallObs;
    }

    public subscribeToDeleteCastingCallObs(): Observable<CastingCall> {
        return this.deleteCastingCallObs;
    }

    private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/casting-calls/";
        return "/api/productions/";
    }

    public createCastingCall(castingCall: CastingCall, productionId: number): Observable<CastingCall> {
        let controllerQuery = this.getBaseURL() + productionId + "/castingcalls" ;
        return this.postAction<CastingCall>(castingCall, controllerQuery)
            .pipe(
                map((res: CastingCall) => {
                    //do anything with the returned data here...
                    this._newCastingCallObs.next(res);
                    return res
                })
            )
    };

    public saveCastingCall(id: number, productionId: number, castingCall: CastingCall): Observable<CastingCall> {
        castingCall.id = id;
        let controllerQuery = this.getBaseURL() + productionId + "/castingcalls/" +  id;
        return this.putAction<CastingCall>(castingCall, controllerQuery)
            .pipe(
                map((res: CastingCall) => {
                    //do anything with the returned data here...
                    this._editCastingCallObs.next(res);
                    return res
                })
            )
    };


    public deleteCastingCall(castingCallId: number, productionId: number): Observable<CastingCall> {
        let controllerQuery = this.getBaseURL() + productionId + "/castingcalls/" +  castingCallId;
        return this.deleteAction<CastingCall>(controllerQuery)
            .pipe(
                map((res: CastingCall) => {
                    //do anything with the returned data here...
                    this._deleteCastingCallObs.next(res);
                    return res
                })
            )
    };



    public getCastingCalls(pageIndex: number = 0, pageSize: number = 24): Observable<CastingCall[]> {
      let controllerQuery = this.getBaseURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
      return this.getAction<CastingCall[]>(controllerQuery)
        .pipe(
          map((res: CastingCall[]) => {
            //do anything with the returned data here...
            return res
          })
        );
    };

    public getProductionCastingCalls(productionId:number, pageIndex: number = 0, pageSize: number = 24): Observable<CastingCall[]> {
      let controllerQuery = this.getBaseURL() + "production/" + productionId + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
      return this.getAction<CastingCall[]>(controllerQuery)
        .pipe(
          map((res: CastingCall[]) => {
            //do anything with the returned data here...
            return res
          })
        );
    };

    public getCastingCall(castingCallId: number, productionId: number): Observable<CastingCall> {
        let controllerQuery = this.getBaseURL() + productionId + "/castingcalls/" + castingCallId;
        return this.getAction<CastingCall>(controllerQuery)
            .pipe(
                map((res: CastingCall) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };
}
