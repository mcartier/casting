import { Component, OnInit, OnDestroy, Inject, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ProductionDialogService, AuditionDetailDialogObject } from '../../services/production-dialog.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { FileUploadService } from '../../../../services/file-upload.service';
import { AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { Dialog } from 'primeng/dialog';
import { SelectItem } from 'primeng/api';
import { ProductionSideFile, CreatedBlob } from '../../../../models/blobs';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';

@Component({
    selector: 'app-sides-dialog',
    templateUrl: './sides-dialog.component.html',
    styleUrls: ['./sides-dialog.component.scss']
})
export class SidesDialogComponent implements OnInit, OnDestroy, AfterViewInit {

    constructor(private productionDialogService: ProductionDialogService,
        private productionExtraService: ProductionExtraService,
        private _formBuilder: FormBuilder,
        public mediaMatcher: MediaMatcher,
        private fileUploadService: FileUploadService,
        private messageDispatchService: MessageDispatchService) {
    }
    @Input() side: Side;
    @Input() productionId: number;
    @ViewChild('formDirective', {static: false}) private formDirective: NgForm;
    @ViewChild('name', {static: false}) nameField: ElementRef;
    @ViewChild('detaildialog', {static: false}) detaildialog: Dialog;
    @ViewChild('file', {static: false}) file


    bigSizeMatcher: MediaQueryList;
    smallSizeMatcher: MediaQueryList;
    isSmallDevice: boolean = false;

    sideFiles: ProductionSideFile[] = [];
    private messages: any[] = [];
    private saveIsDisabled: boolean = true;
    private cancelIsDisabled: boolean = false;
    private selectIsDisabled: boolean = false;
    private dropPanelIsHiden: boolean = false;
    private nameIsDisable: boolean = false;
    private removeFileIsDisabled: boolean = false;
    private uploadWasSuccessfull: boolean = false;
    private successfullFileUploadId: string = "";
   
    isVisible: boolean = false;
    private dialogVisibleStateSubscription: Subscription;
    private basicDialogInfo: FormGroup;
    private fileUploadSubscription: Subscription;


    ngOnInit() {
        this.isSmallDevice = window.innerWidth < 768;

        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));


        this.basicDialogInfo = this._formBuilder.group({
            name: ['', Validators.required]
        },
            { updateOn: 'blur' });

        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToSidesDialogShowStateObs()
            .subscribe(
                dialogObject => {
                    this.clearMessages();

                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    if (dialogObject.isVisible && dialogObject.side != null) {
                        this.side = dialogObject.side;
                        this.basicDialogInfo.patchValue({
                            name: this.side.name
                        });
                    } else {
                        this.side = null;
                        this.basicDialogInfo.patchValue({
                            name: ''
                        });

                    }
                    this.isVisible = dialogObject.isVisible;
                    // this.detaildialog.maximized = true;
                    //this.detaildialog.toggleMaximize();
                    this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                    this.nameField.nativeElement.focus();

                });


    }

    ngAfterViewInit() {


    }

    ngOnDestroy() {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();

        }
        if (this.fileUploadSubscription) {
            this.fileUploadSubscription.unsubscribe();

        }

        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);

    }
    private clearMessages() {
        this.messages = [];
    }
    addFiles() {
        this.file.nativeElement.click();
    }
    onFilesAdded() {
        const files: { [key: string]: File } = this.file.nativeElement.files;
        for (let key in files) {
            if (!isNaN(parseInt(key))) {
                this.sideFiles.push(new ProductionSideFile(0, files[key], 0, this.productionId));
            }
        }
        if (this.sideFiles && this.sideFiles.length > 0) {
            this.dropPanelIsHiden = true;
            this.saveIsDisabled = false;
            this.selectIsDisabled = true;
        }
    }
    public remove(fileUploadState: ProductionSideFile): void {
        this.clearMessages();

        if (fileUploadState.upload) {
            fileUploadState.upload.abort();
        }
        this.sideFiles.splice(this.sideFiles.indexOf(fileUploadState), 1);
        this.saveIsDisabled = true;
        this.selectIsDisabled = false;
        this.dropPanelIsHiden = false;

    }
    onFilesDropped(event) {
        if (event.length > 1) {

        } else if (event.length == 1) {
            this.dropPanelIsHiden = true;
            this.saveIsDisabled = false;
            this.selectIsDisabled = true;
            this.sideFiles.push(new ProductionSideFile(0, event[0], 0, this.productionId));

        }
    }

    mediaQueryBigChangeListener(event) {

        if (event.matches) {
            this.isSmallDevice = false;
       }
    }
    mediaQuerySmallChangeListener(event) {
        if (event.matches) {

            this.isSmallDevice = true;
        }
    }

    private onDialogShow(event: any, dialog: Dialog) {
       
        if (this.isSmallDevice) {
            setTimeout(() => {
                dialog.maximize();
            },
                0);
        } 
    }
    private onDialogHide(event: any, dialog: Dialog) {
       this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();


    }
    private cancelClick() {
      this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
   
    }
    private resetDialog() {
        this.sideFiles = [];
        this.saveIsDisabled = true;
        this.selectIsDisabled = false;
        this.dropPanelIsHiden = false;
        this.nameIsDisable = false;
        this.removeFileIsDisabled = false;
        this.cancelIsDisabled = false;
        this.uploadWasSuccessfull = false;
        this.successfullFileUploadId = "";
        this.messages = [];

    }

    private saveFormData() {
        let tside: Side = new Side;
        tside.productionId = this.productionId;
        tside.name = this.basicDialogInfo.get('name').value;
        tside.fileName = this.successfullFileUploadId;
        tside.fileId = this.successfullFileUploadId;
        if (this.side && this.side.id && this.side.id !== 0) {
          this.productionExtraService.saveSide(this.side.id, this.productionId, tside).subscribe(
            (side: Side) => {
              //this.side = side;
              this.formDirective.resetForm();
              this.basicDialogInfo.reset();
              this.isVisible = false;
              this.messageDispatchService.dispatchSuccesMessage("New Side '" + side.name + "' was created successfully",
                "Side Created");


            },
            (error) => {
              this.nameIsDisable = false;
              this.saveIsDisabled = false;
              this.selectIsDisabled = true;
              this.dropPanelIsHiden = true;
                this.removeFileIsDisabled = true;
              this.cancelIsDisabled = false;

              this.messageDispatchService.dispatchErrorMessage(
                "Error creating side '" + tside.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                "Side Creation Error");
              this.messages.push({
                severity: 'error',
                summary: "Side Creation Error",
                detail: "Error creating side '" + tside.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
              });

            });

        } else {
          this.productionExtraService.createSide(this.productionId, tside).subscribe(
            (side: Side) => {
              if (side != null) {
                this.formDirective.resetForm();
                this.basicDialogInfo.reset();
                this.isVisible = false;

                this.messageDispatchService.dispatchSuccesMessage(
                  "New Side '" + side.name + "' was created successfully",
                  "Side Created");

              }

            },
            (error) => {
              this.nameIsDisable = false;
              this.saveIsDisabled = false;
              this.selectIsDisabled = true;
              this.dropPanelIsHiden = true;
              this.removeFileIsDisabled = true;
              this.cancelIsDisabled = false;

              this.messageDispatchService.dispatchErrorMessage(
                "Error creating side '" + tside.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                "Side Creation Error");
              this.messages.push({
                severity: 'error',
                summary: "Side Creation Error",
                detail: "Error creating side '" + tside.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
              });
            });

        }

    }

    private uploadFile() {
        this.fileUploadSubscription = this.sideFiles[0].obs.asObservable()
            .subscribe(blobUploadUpdate => {
                if (this.sideFiles[0].isComplete && this.sideFiles[0].isSuccess) {
                    this.uploadWasSuccessfull = true;
                    this.successfullFileUploadId = this.sideFiles[0].uploadedFileId;

                    this.messageDispatchService.dispatchSuccesMessage("File " + this.sideFiles[0].file.name + "." + this.sideFiles[0].file.type + " uploaded successfully", "Upload Success");

                    this.fileUploadSubscription.unsubscribe();
                    this.nameIsDisable = false;
                    this.saveIsDisabled = false;
                    this.selectIsDisabled = true;
                    this.dropPanelIsHiden = true;
                    this.removeFileIsDisabled = true;
                    this.cancelIsDisabled = false;

                    this.saveFormData();

                } else if (this.sideFiles[0].isComplete && this.sideFiles[0].isError) {
                    this.nameIsDisable = false;
                    this.saveIsDisabled = false;
                    this.selectIsDisabled = true;
                    this.dropPanelIsHiden = true;
                    this.removeFileIsDisabled = false;
                    this.cancelIsDisabled = false;
                   
                    this.messageDispatchService.dispatchErrorMessage(this.sideFiles[0].error, "Upload Error");
                    this.messages.push({
                        severity: 'error',
                        summary: "Upload Error",
                        detail: this.sideFiles[0].error
                    });
                    this.fileUploadSubscription.unsubscribe();

                    //this.sideFiles[0] = blobUploadUpdate;
                }

            });
        this.fileUploadService.uploadFiles(this.sideFiles);
    }
    private saveClick() {
        this.clearMessages();
        if (this.basicDialogInfo.valid) {
            let tside: Side = new Side;
            tside.productionId = this.productionId;
            tside.name = this.basicDialogInfo.get('name').value;

            if (this.uploadWasSuccessfull && this.successfullFileUploadId.length > 0) {
                this.saveFormData();
            } else {
                this.nameIsDisable = true;
                this.saveIsDisabled = true;
                this.selectIsDisabled = true;
                this.dropPanelIsHiden = true;
                this.removeFileIsDisabled = true;
                this.cancelIsDisabled = true;

                this.uploadFile();
            }

        } else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.nameField.nativeElement.focus();
            }

        }

    }
}
