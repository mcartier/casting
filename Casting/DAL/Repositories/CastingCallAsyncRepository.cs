﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{
   
    public class CastingCallAsyncRepository : AsyncRepository, ICastingCallAsyncRepository
    {
        ICastingContext _castingContext;
        public CastingCallAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region CastingCalls

        public async Task<CastingCall> GetCastingCallByIdAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery, int castingCallId)
        {
            IQueryable<CastingCall> castingCalls = GetCastingCallsQueryable(parameterIncludeInQuery);

            return await castingCalls.FirstOrDefaultAsync(p => p.Id == castingCallId);


        }

        public async Task<int> AddNewCastingCallAsync(CastingCall castingCall)
        {
            _castingContext.Set<CastingCall>().Add(castingCall);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteCastingCallAsync(CastingCall castingCall)
        {
            _castingContext.Set<CastingCall>().Remove(castingCall);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateCastingCallAsync(CastingCall castingCall)
        {
            SetEntityState(castingCall, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery, int productionId)
        {
            var castingCalls = GetCastingCallsQueryable(parameterIncludeInQuery);

            return await castingCalls.Where(d => d.ProductionId == productionId).ToListAsync();
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending)
        {
            var castingCalls = GetCastingCallsQueryable(parameterIncludeInQuery);
            switch (orderBy)
            {
                case OrderByValuesEnum.CreatedDateAscending:

                    castingCalls = castingCalls.OrderBy(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.CreatedDateDescending:

                    castingCalls = castingCalls.OrderByDescending(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.NameAscending:

                    // castingCalls = castingCalls.OrderBy(o => o.Name);
                    break;
                case OrderByValuesEnum.NameDescending:

                    // castingCalls = castingCalls.OrderByDescending(o => o.Name);
                    break;
            }
            return await castingCalls.Skip(pageIndex * pageSize).Take(pageSize).ToListAsync();
        }



        #region PrivateMethods
        private IQueryable<CastingCall> GetCastingCallsQueryable(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery)
        {
            IQueryable<CastingCall> castingCalls = from castingCall in _castingContext.CastingCalls select castingCall;
            castingCalls = castingCalls.Include("CityTalentLocations")
                .Include("RegionTalentLocations")
                .Include("CountryTalentLocations")
                .Include("CityTalentLocations.City")
                .Include("RegionTalentLocations.Region")
                .Include("CountryTalentLocations.Country");
            if (parameterIncludeInQuery == CastingCallParameterIncludeInQueryEnum.Roles)
            {
                castingCalls
                    .Include(i => i.Roles);
            }

            if (parameterIncludeInQuery == CastingCallParameterIncludeInQueryEnum.RolesAndPersonas)
            {
                castingCalls
                    .Include("Roles.Personas.Persona");
            }
            if (parameterIncludeInQuery == CastingCallParameterIncludeInQueryEnum.RolesPersonasAndAssets)
            {
                castingCalls
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }
            return castingCalls;
        }


        #endregion
        #endregion


        #region Roles

        public async Task<CastingCallRole> GetCastingCallRoleByIdAsync(CastingCallRoleParameterIncludeInQueryEnum includeInQuery, int castingCallRoleId)
        {
            IQueryable<CastingCallRole> castingCallRoles = GetCastingCallRolesQueryable(includeInQuery);

            return await castingCallRoles.FirstOrDefaultAsync(p => p.Id == castingCallRoleId);


        }

        public async Task<int> AddNewCastingCallRoleAsync(bool orderAtTheTop, CastingCallRole castingCallRole)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingCallRoleByCastingCallAsync(castingCallRole);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingCallRole.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingCallRole>().Add(castingCallRole);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingCallRoleAsync(1, castingCallRole);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingCallRoleAsync(CastingCallRole castingCallRole)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingCallRoleAsync(0, castingCallRole);

                    _castingContext.Set<CastingCallRole>().Remove(castingCallRole);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingCallRoleAsync(CastingCallRole castingCallRole)
        {
            SetEntityState(castingCallRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdAsync(CastingCallRoleParameterIncludeInQueryEnum includeInQuery, int castingCallId)
        {
            var castingCallRoles = GetCastingCallRolesQueryable(includeInQuery);

            return await castingCallRoles.Where(d => d.CastingCallId == castingCallId).ToListAsync();
        }
        public async Task<int> ReOrderCastingCallRoleAsync(int desiredOrderIndex, CastingCallRole castingCallRole)
        {
            IEnumerable<CastingCallRole> castingCallRoles = await GetCastingCallRolesByCastingCallIdAsync(CastingCallRoleParameterIncludeInQueryEnum.Roles, castingCallRole.CastingCallId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingCallRoleUpAsync(desiredOrderIndex, castingCallRole, castingCallRoles);
            }
            else if (desiredOrderIndex > castingCallRole.OrderIndex)
            {
                return await ReOrderCastingCallRoleUpAsync(desiredOrderIndex, castingCallRole, castingCallRoles);
            }
            else if (desiredOrderIndex < castingCallRole.OrderIndex)
            {
                return await ReOrderCastingCallRoleDownAsync(desiredOrderIndex, castingCallRole, castingCallRoles);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingCallRoleByCastingCallAsync(CastingCallRole castingCallRole)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingCallRoles
                                 where role.CastingCallId == castingCallRole.CastingCallId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingCallRoleDownAsync(int desiredOrderIndex, CastingCallRole castingCallRole,
            IEnumerable<CastingCallRole> castingCallRoles)
        {
            foreach (var role in castingCallRoles)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingCallRole.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingCallRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingCallRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingCallRoleUpAsync(int desiredOrderIndex, CastingCallRole castingCallRole,
            IEnumerable<CastingCallRole> castingCallRoles)
        {
            foreach (var role in castingCallRoles)
            {
                if (role.OrderIndex > castingCallRole.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingCallRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingCallRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingCallRole> GetCastingCallRolesQueryable(CastingCallRoleParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CastingCallRole> castingCallRoles = from castingCallRole in _castingContext.CastingCallRoles select castingCallRole;
            if (includeInQuery == CastingCallRoleParameterIncludeInQueryEnum.Personas)
            {
                castingCallRoles
                    .Include("Roles.Personas.Persona");
            }
            if (includeInQuery == CastingCallRoleParameterIncludeInQueryEnum.PersonasAndAssets)
            {
                castingCallRoles
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }
            return castingCallRoles;
        }


        #endregion
        #endregion

        #region Folders

        public async Task<CastingCallFolder> GetCastingCallFolderByIdAsync(CastingCallFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId)
        {
            IQueryable<CastingCallFolder> castingSessionFolders = GetCastingCallFoldersQueryable(includeInQuery);

            return await castingSessionFolders.FirstOrDefaultAsync(p => p.Id == castingSessionFolderId);


        }

        public async Task<int> AddNewCastingCallFolderAsync(bool orderAtTheTop, CastingCallFolder castingSessionFolder)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingCallFolderByCastingCallAsync(castingSessionFolder);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingSessionFolder.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingCallFolder>().Add(castingSessionFolder);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingCallFolderAsync(1, castingSessionFolder);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingCallFolderAsync(CastingCallFolder castingSessionFolder)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingCallFolderAsync(0, castingSessionFolder);

                    _castingContext.Set<CastingCallFolder>().Remove(castingSessionFolder);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingCallFolderAsync(CastingCallFolder castingSessionFolder)
        {
            SetEntityState(castingSessionFolder, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdAsync(CastingCallFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionId)
        {
            var castingSessionFolders = GetCastingCallFoldersQueryable(includeInQuery);

            return await castingSessionFolders.Where(d => d.CastingCallId == castingSessionId).ToListAsync();
        }
        public async Task<int> ReOrderCastingCallFolderAsync(int desiredOrderIndex, CastingCallFolder castingSessionFolder)
        {
            IEnumerable<CastingCallFolder> castingSessionFolders = await GetCastingCallFoldersByCastingCallIdAsync(CastingCallFolderParameterIncludeInQueryEnum.Folders, castingSessionFolder.CastingCallId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingCallFolderUpAsync(desiredOrderIndex, castingSessionFolder, castingSessionFolders);
            }
            else if (desiredOrderIndex > castingSessionFolder.OrderIndex)
            {
                return await ReOrderCastingCallFolderUpAsync(desiredOrderIndex, castingSessionFolder, castingSessionFolders);
            }
            else if (desiredOrderIndex < castingSessionFolder.OrderIndex)
            {
                return await ReOrderCastingCallFolderDownAsync(desiredOrderIndex, castingSessionFolder, castingSessionFolders);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingCallFolderByCastingCallAsync(CastingCallFolder castingSessionFolder)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingCallFolders
                                 where role.CastingCallId == castingSessionFolder.CastingCallId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingCallFolderDownAsync(int desiredOrderIndex, CastingCallFolder castingSessionFolder,
            IEnumerable<CastingCallFolder> castingSessionFolders)
        {
            foreach (var role in castingSessionFolders)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingSessionFolder.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolder.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolder, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingCallFolderUpAsync(int desiredOrderIndex, CastingCallFolder castingSessionFolder,
            IEnumerable<CastingCallFolder> castingSessionFolders)
        {
            foreach (var role in castingSessionFolders)
            {
                if (role.OrderIndex > castingSessionFolder.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolder.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolder, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingCallFolder> GetCastingCallFoldersQueryable(CastingCallFolderParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CastingCallFolder> castingSessionFolders = from castingSessionFolder in _castingContext.CastingCallFolders select castingSessionFolder;
            if (includeInQuery == CastingCallFolderParameterIncludeInQueryEnum.Roles)
            {
                castingSessionFolders
                    .Include(r => r.Roles);
            }
            if (includeInQuery == CastingCallFolderParameterIncludeInQueryEnum.RolesAndPersonas)
            {
                castingSessionFolders
                    .Include("Roles.Personas.Persona");
            }
            if (includeInQuery == CastingCallFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets)
            {
                castingSessionFolders
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }
            return castingSessionFolders;
        }


        #endregion
        #endregion

        #region Roles

        public async Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderRoleId)
        {
            IQueryable<CastingCallFolderRole> castingSessionFolderRoles = GetCastingCallFolderRolesQueryable(includeInQuery);

            return await castingSessionFolderRoles.FirstOrDefaultAsync(p => p.Id == castingSessionFolderRoleId);


        }

        public async Task<int> AddNewCastingCallFolderRoleAsync(bool orderAtTheTop, CastingCallFolderRole castingSessionFolderRole)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCastingCallFolderRoleByCastingCallFolderAsync(castingSessionFolderRole);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    castingSessionFolderRole.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CastingCallFolderRole>().Add(castingSessionFolderRole);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCastingCallFolderRoleAsync(1, castingSessionFolderRole);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCastingCallFolderRoleAsync(CastingCallFolderRole castingSessionFolderRole)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCastingCallFolderRoleAsync(0, castingSessionFolderRole);

                    _castingContext.Set<CastingCallFolderRole>().Remove(castingSessionFolderRole);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCastingCallFolderRoleAsync(CastingCallFolderRole castingSessionFolderRole)
        {
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId)
        {
            var castingSessionFolderRoles = GetCastingCallFolderRolesQueryable(includeInQuery);

            return await castingSessionFolderRoles.Where(d => d.CastingCallFolderId == castingSessionFolderId).ToListAsync();
        }
        public async Task<int> ReOrderCastingCallFolderRoleAsync(int desiredOrderIndex, CastingCallFolderRole castingSessionFolderRole)
        {
            IEnumerable<CastingCallFolderRole> castingSessionFolderRoles = await GetCastingCallFolderRolesByCastingCallFolderIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum.Roles, castingSessionFolderRole.CastingCallFolderId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCastingCallFolderRoleUpAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            else if (desiredOrderIndex > castingSessionFolderRole.OrderIndex)
            {
                return await ReOrderCastingCallFolderRoleUpAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            else if (desiredOrderIndex < castingSessionFolderRole.OrderIndex)
            {
                return await ReOrderCastingCallFolderRoleDownAsync(desiredOrderIndex, castingSessionFolderRole, castingSessionFolderRoles);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCastingCallFolderRoleByCastingCallFolderAsync(CastingCallFolderRole castingSessionFolderRole)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CastingCallFolderRoles
                                 where role.CastingCallFolderId == castingSessionFolderRole.CastingCallFolderId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCastingCallFolderRoleDownAsync(int desiredOrderIndex, CastingCallFolderRole castingSessionFolderRole,
            IEnumerable<CastingCallFolderRole> castingSessionFolderRoles)
        {
            foreach (var role in castingSessionFolderRoles)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < castingSessionFolderRole.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolderRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCastingCallFolderRoleUpAsync(int desiredOrderIndex, CastingCallFolderRole castingSessionFolderRole,
            IEnumerable<CastingCallFolderRole> castingSessionFolderRoles)
        {
            foreach (var role in castingSessionFolderRoles)
            {
                if (role.OrderIndex > castingSessionFolderRole.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            castingSessionFolderRole.OrderIndex = desiredOrderIndex;
            SetEntityState(castingSessionFolderRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CastingCallFolderRole> GetCastingCallFolderRolesQueryable(CastingCallFolderRoleParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CastingCallFolderRole> castingSessionFolderRoles = from castingSessionFolderRole in _castingContext.CastingCallFolderRoles select castingSessionFolderRole;
            if (includeInQuery == CastingCallFolderRoleParameterIncludeInQueryEnum.Personas)
            {
                castingSessionFolderRoles
                    .Include("Personas.Persona");
            }
            if (includeInQuery == CastingCallFolderRoleParameterIncludeInQueryEnum.PersonasAndAssets)
            {
                castingSessionFolderRoles
                    .Include("Personas.Persona")
                    .Include("Personas.Persona.VideoReferences.Video")
                    .Include("Personas.Persona.AudioReferences.Audio")
                    .Include("Personas.Persona.ResumeReferences.Resume")
                    .Include("Personas.Persona.ImageReferences.Image");
            }
            return castingSessionFolderRoles;
        }


        #endregion
        #endregion

        #region TalentLocations

        private IQueryable<CastingCallCityReference> GetAllCastingCallTalentCityReferencesQueryable(bool withCity)
        {
            var locationRerereces = from locationRef in _castingContext.CastingCallCityReferences select locationRef;
            if (withCity)
            {
                locationRerereces.Include("city.region.country");
            }
            return from user in locationRerereces select user; ;
           // return locationRerereces;
        }
        public async Task<IEnumerable<CastingCallCityReference>> GetCastingCallTalentCityReferencesByCastingCallId(int castingCallId)
        {


            return await GetAllCastingCallTalentCityReferencesQueryable(true).Where(c => c.CastingCallId == castingCallId).ToListAsync();
        }

        public async Task<CastingCallCityReference> GetCastingCallTalentCityReferenceById(int castingCallLocationReferenceId)
        {


            return await GetAllCastingCallTalentCityReferencesQueryable(true).Where(c => c.Id == castingCallLocationReferenceId).FirstOrDefaultAsync();
        }

        public async Task<int> AddNewCityTalentLocationAsync(CastingCallCityReference castingCallCityReference)
        {
            _castingContext.Set<CastingCallCityReference>().Add(castingCallCityReference);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteCityTalentReferenceAsync(CastingCallCityReference castingCallCityReference)
        {
            _castingContext.Set<CastingCallCityReference>().Remove(castingCallCityReference);
            return await _castingContext.SaveChangesAsync();
        }
        private IQueryable<CastingCallRegionReference> GetAllCastingCallTalentRegionReferencesQueryable(bool withRegion)
        {
            var locationRerereces = from locationRef in _castingContext.CastingCallRegionReferences select locationRef;
            if (withRegion)
            {
                locationRerereces.Include("region.country");
            }

            return locationRerereces;
        }
        public async Task<IEnumerable<CastingCallRegionReference>> GetCastingCallTalentRegionReferencesByCastingCallId(int castingCallId)
        {


            return await GetAllCastingCallTalentRegionReferencesQueryable(true).Where(c => c.CastingCallId == castingCallId).ToListAsync();
        }

        public async Task<CastingCallRegionReference> GetCastingCallTalentRegionReferenceById(int castingCallLocationReferenceId)
        {


            return await GetAllCastingCallTalentRegionReferencesQueryable(true).Where(c => c.Id == castingCallLocationReferenceId).FirstOrDefaultAsync();
        }


        public async Task<int> AddNewRegionTalentLocationAsync(CastingCallRegionReference castingCallRegionReference)
        {
            _castingContext.Set<CastingCallRegionReference>().Add(castingCallRegionReference);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteRegionTalentReferenceAsync(CastingCallRegionReference castingCallRegionReference)
        {
            _castingContext.Set<CastingCallRegionReference>().Remove(castingCallRegionReference);
            return await _castingContext.SaveChangesAsync();
        }
        private IQueryable<CastingCallCountryReference> GetAllCastingCallTalentCountryReferencesQueryable(bool withCountry)
        {
            var locationRerereces = from locationRef in _castingContext.CastingCallCountryReferences select locationRef;
            if (withCountry)
            {
                locationRerereces.Include("country");
            }

            return locationRerereces;
        }
        public async Task<IEnumerable<CastingCallCountryReference>> GetCastingCallTalentCountryReferencesByCastingCallId(int castingCallId)
        {


            return await GetAllCastingCallTalentCountryReferencesQueryable(true).Where(c => c.CastingCallId == castingCallId).ToListAsync();
        }

        public async Task<CastingCallCountryReference> GetCastingCallTalentCountryReferenceById(int castingCallLocationReferenceId)
        {


            return await GetAllCastingCallTalentCountryReferencesQueryable(true).Where(c => c.Id == castingCallLocationReferenceId).FirstOrDefaultAsync();
        }


        public async Task<int> AddNewCountryTalentLocationAsync(CastingCallCountryReference castingCallCountryReference)
        {
            _castingContext.Set<CastingCallCountryReference>().Add(castingCallCountryReference);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteCountryTalentReferenceAsync(CastingCallCountryReference castingCallCountryReference)
        {
            _castingContext.Set<CastingCallCountryReference>().Remove(castingCallCountryReference);
            return await _castingContext.SaveChangesAsync();
        }

        #endregion
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
