import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../models/production.models';
import { WINDOW } from './browser.window.provider';
import { HttpHelpers } from '../utilities/HttpHelpers';
@Injectable({
  providedIn: 'root'
})
export class ProductionExtraService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;
    }


    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;
    private getBaseURL(productionId:number): string {
      // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/productions/";
      return "/api/productions/"+ productionId + "/extras";
    }
    /*
    private _sideCreatedObs: Subject<Side> = new Subject<Side>();
    private _payDetailCreatedObs: Subject<PayDetail> = new Subject<PayDetail>();
    private _shootDetailCreatedObs: Subject<ShootDetail> = new Subject<ShootDetail>();
    private _auditionDetailCreatedObs: Subject<AuditionDetail> = new Subject<AuditionDetail>();

    public readonly auditionDetailCreatedObs: Observable<AuditionDetail> = this._auditionDetailCreatedObs.asObservable();

    public subscribeToAuditionDetailCreatedObs(): Observable<AuditionDetail> {
      return this._auditionDetailCreatedObs;
    }
    public readonly shootDetailCreatedObs: Observable<ShootDetail> = this._shootDetailCreatedObs.asObservable();

    public subscribeToShootDetailCreatedObs(): Observable<ShootDetail> {
      return this._shootDetailCreatedObs;
    }
    public readonly payDetailCreatedObs: Observable<PayDetail> = this._payDetailCreatedObs.asObservable();

    public subscribeToPayDetailCreatedObs(): Observable<PayDetail> {
      return this._payDetailCreatedObs;
    }
    public readonly sideCreatedObs: Observable<Side> = this._sideCreatedObs.asObservable();

    public subscribeToSideCreatedObs(): Observable<Side> {
      return this._sideCreatedObs;
    }

    private _sideUpdatedObs: BehaviorSubject<Side> = new BehaviorSubject<Side>();
    private _payDetailUpdatedObs: BehaviorSubject<PayDetail> = new BehaviorSubject<PayDetail>();
    private _shootDetailUpdatedObs: BehaviorSubject<ShootDetail> = new BehaviorSubject<ShootDetail>();
    private _auditionDetailUpdatedObs: BehaviorSubject<AuditionDetail> = new BehaviorSubject<AuditionDetail>();

    public readonly auditionDetailUpdatedObs: Observable<AuditionDetail> = this._auditionDetailUpdatedObs.asObservable();

    public subscribeToAuditionDetailUpdatedObs(): Observable<AuditionDetail> {
      return this._auditionDetailUpdatedObs;
    }
    public readonly shootDetailUpdatedObs: Observable<ShootDetail> = this._shootDetailUpdatedObs.asObservable();

    public subscribeToShootDetailUpdatedObs(): Observable<ShootDetail> {
      return this._shootDetailUpdatedObs;
    }
    public readonly payDetailUpdatedObs: Observable<PayDetail> = this._payDetailUpdatedObs.asObservable();

    public subscribeToPayDetailUpdatedObs(): Observable<PayDetail> {
      return this._payDetailUpdatedObs;
    }
    public readonly sideUpdatedObs: Observable<Side> = this._sideUpdatedObs.asObservable();

    public subscribeToSideUpdatedObs(): Observable<Side> {
      return this._sideUpdatedObs;
    }
    private _sideDeletedObs: BehaviorSubject<Side> = new BehaviorSubject<Side>();
    private _payDetailDeletedObs: BehaviorSubject<PayDetail> = new BehaviorSubject<PayDetail>();
    private _shootDetailDeletedObs: BehaviorSubject<ShootDetail> = new BehaviorSubject<ShootDetail>();
    private _auditionDetailDeletedObs: BehaviorSubject<AuditionDetail> = new BehaviorSubject<AuditionDetail>();

    public readonly auditionDetailDeletedObs: Observable<AuditionDetail> = this._auditionDetailDeletedObs.asObservable();

    public subscribeToAuditionDetailDeletedObs(): Observable<AuditionDetail> {
      return this._auditionDetailDeletedObs;
    }
    public readonly shootDetailDeletedObs: Observable<ShootDetail> = this._shootDetailDeletedObs.asObservable();

    public subscribeToShootDetailDeletedObs(): Observable<ShootDetail> {
      return this._shootDetailDeletedObs;
    }
    public readonly payDetailDeletedObs: Observable<PayDetail> = this._payDetailDeletedObs.asObservable();

    public subscribeToPayDetailDeletedObs(): Observable<PayDetail> {
      return this._payDetailDeletedObs;
    }
    public readonly sideDeletedObs: Observable<Side> = this._sideDeletedObs.asObservable();

    public subscribeToSideDeletedObs(): Observable<Side> {
      return this._sideDeletedObs;
    }
     */
    private _deleteAuditionDetailObs: Subject<AuditionDetail> = new Subject<AuditionDetail>();
    public readonly deleteAuditionDetailObs: Observable<AuditionDetail> = this._deleteAuditionDetailObs.asObservable();

    private _editAuditionDetailObs: Subject<AuditionDetail> = new Subject<AuditionDetail>();
    public readonly editAuditionDetailObs: Observable<AuditionDetail> = this._editAuditionDetailObs.asObservable();

    private _newAuditionDetailObs: Subject<AuditionDetail> = new Subject<AuditionDetail>();
    public readonly newAuditionDetailObs: Observable<AuditionDetail> = this._newAuditionDetailObs.asObservable();

    public subscribeToCreateAuditionDetailObs(): Observable<AuditionDetail> {
        return this.newAuditionDetailObs;
    }
    public subscribeToEditAuditionDetailObs(): Observable<AuditionDetail> {
        return this.editAuditionDetailObs;
    }

    public subscribeToDeleteAuditionDetailObs(): Observable<AuditionDetail> {
        return this.deleteAuditionDetailObs;
    }
   
    private getBaseAuditionDetailURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/auditionDetails/";
        return "/auditiondetails/";
    }

    public createAuditionDetail(productionId:number,auditionDetail: AuditionDetail): Observable<AuditionDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL();
        return this.postAction<AuditionDetail>(auditionDetail, controllerQuery)
            .pipe(
                map((res: AuditionDetail) => {
                    //do anything with the returned data here...
                    this._newAuditionDetailObs.next(res);
                    return res
                })
            )
    };

    public saveAuditionDetail(id: number, productionId: number, auditionDetail: AuditionDetail): Observable<AuditionDetail> {
        auditionDetail.id = id;
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + id;
        return this.putAction<AuditionDetail>(auditionDetail, controllerQuery)
            .pipe(
            map((res: AuditionDetail) => {
                   //do anything with the returned data here...
                this._editAuditionDetailObs.next(auditionDetail);
                return auditionDetail
                })
            )
    };


    public deleteAuditionDetail(auditionDetailId: number, productionId: number): Observable<AuditionDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + auditionDetailId;
        return this.deleteAction<AuditionDetail>(controllerQuery)
            .pipe(
                map((res: AuditionDetail) => {
                    //do anything with the returned data here...
                    this._deleteAuditionDetailObs.next(res);
                    return res
                })
            )
    };



    public getAuditionDetails(productionId:number,pageIndex: number = 0, pageSize: number = 24): Observable<AuditionDetail[]> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction<AuditionDetail[]>(controllerQuery)
            .pipe(
                map((res: AuditionDetail[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getAuditionDetail(auditionDetailId: number, productionId:number): Observable<AuditionDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + auditionDetailId;
        return this.getAction<AuditionDetail>(controllerQuery)
            .pipe(
                map((res: AuditionDetail) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    private _deleteShootDetailObs: Subject<ShootDetail> = new Subject<ShootDetail>();
    public readonly deleteShootDetailObs: Observable<ShootDetail> = this._deleteShootDetailObs.asObservable();

    private _editShootDetailObs: Subject<ShootDetail> = new Subject<ShootDetail>();
    public readonly editShootDetailObs: Observable<ShootDetail> = this._editShootDetailObs.asObservable();

    private _newShootDetailObs: Subject<ShootDetail> = new Subject<ShootDetail>();
    public readonly newShootDetailObs: Observable<ShootDetail> = this._newShootDetailObs.asObservable();

    public subscribeToCreateShootDetailObs(): Observable<ShootDetail> {
        return this.newShootDetailObs;
    }
    public subscribeToEditShootDetailObs(): Observable<ShootDetail> {
        return this.editShootDetailObs;
    }

    public subscribeToDeleteShootDetailObs(): Observable<ShootDetail> {
        return this.deleteShootDetailObs;
    }

    private getBaseShootDetailURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/shootDetails/";
        return "/shootdetails/";
    }

    public createShootDetail(productionId:number,shootDetail: ShootDetail): Observable<ShootDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL();
        return this.postAction<ShootDetail>(shootDetail, controllerQuery)
            .pipe(
                map((res: ShootDetail) => {
                    //do anything with the returned data here...
                    this._newShootDetailObs.next(res);
                    return res
                })
            )
    };

    public saveShootDetail(id: number, productionId: number, shootDetail: ShootDetail): Observable<ShootDetail> {
        shootDetail.id = id;
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + id;
        return this.putAction<ShootDetail>(shootDetail, controllerQuery)
            .pipe(
                map((res: ShootDetail) => {
                    //do anything with the returned data here...
                this._editShootDetailObs.next(shootDetail);

                return shootDetail
                })
            )
    };


    public deleteShootDetail(shootDetailId: number, productionId: number): Observable<ShootDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + shootDetailId;
        return this.deleteAction<ShootDetail>(controllerQuery)
            .pipe(
                map((res: ShootDetail) => {
                    //do anything with the returned data here...
                    this._deleteShootDetailObs.next(res);
                    return res
                })
            )
    };



    public getShootDetails(productionId:number,pageIndex: number = 0, pageSize: number = 24): Observable<ShootDetail[]> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction<ShootDetail[]>(controllerQuery)
            .pipe(
                map((res: ShootDetail[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getShootDetail(shootDetailId: number, productionId: number): Observable<ShootDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + shootDetailId;
        return this.getAction<ShootDetail>(controllerQuery)
            .pipe(
                map((res: ShootDetail) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    private _deletePayDetailObs: Subject<PayDetail> = new Subject<PayDetail>();
    public readonly deletePayDetailObs: Observable<PayDetail> = this._deletePayDetailObs.asObservable();

    private _editPayDetailObs: Subject<PayDetail> = new Subject<PayDetail>();
    public readonly editPayDetailObs: Observable<PayDetail> = this._editPayDetailObs.asObservable();

    private _newPayDetailObs: Subject<PayDetail> = new Subject<PayDetail>();
    public readonly newPayDetailObs: Observable<PayDetail> = this._newPayDetailObs.asObservable();

    public subscribeToCreatePayDetailObs(): Observable<PayDetail> {
        return this.newPayDetailObs;
    }
    public subscribeToEditPayDetailObs(): Observable<PayDetail> {
        return this.editPayDetailObs;
    }

    public subscribeToDeletePayDetailObs(): Observable<PayDetail> {
        return this.deletePayDetailObs;
    }

    private getBasePayDetailURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/payDetails/";
        return "/paydetails/";
    }

    public createPayDetail(productionId:number,payDetail: PayDetail): Observable<PayDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL();
        return this.postAction<PayDetail>(payDetail, controllerQuery)
            .pipe(
                map((res: PayDetail) => {
                    //do anything with the returned data here...
                    this._newPayDetailObs.next(res);
                    return res
                })
            )
    };

    public savePayDetail(id: number, productionId: number, payDetail: PayDetail): Observable<PayDetail> {
        payDetail.id = id;
        let controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + id;
        return this.putAction<PayDetail>(payDetail, controllerQuery)
            .pipe(
                map((res: PayDetail) => {
                    //do anything with the returned data here...
                this._editPayDetailObs.next(payDetail);
                return payDetail
                })
            )
    };


    public deletePayDetail(payDetailId: number, productionId: number): Observable<PayDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + payDetailId;
        return this.deleteAction<PayDetail>(controllerQuery)
            .pipe(
                map((res: PayDetail) => {
                    //do anything with the returned data here...
                    this._deletePayDetailObs.next(res);
                    return res
                })
            )
    };



    public getPayDetails(productionId:number,pageIndex: number = 0, pageSize: number = 24): Observable<PayDetail[]> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction<PayDetail[]>(controllerQuery)
            .pipe(
                map((res: PayDetail[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getPayDetail(payDetailId: number, productionId: number): Observable<PayDetail> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + payDetailId;
        return this.getAction<PayDetail>(controllerQuery)
            .pipe(
                map((res: PayDetail) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    private _deleteSideObs: Subject<Side> = new Subject<Side>();
    public readonly deleteSideObs: Observable<Side> = this._deleteSideObs.asObservable();

    private _editSideObs: Subject<Side> = new Subject<Side>();
    public readonly editSideObs: Observable<Side> = this._editSideObs.asObservable();

    private _newSideObs: Subject<Side> = new Subject<Side>();
    public readonly newSideObs: Observable<Side> = this._newSideObs.asObservable();

    public subscribeToCreateSideObs(): Observable<Side> {
        return this.newSideObs;
    }
    public subscribeToEditSideObs(): Observable<Side> {
        return this.editSideObs;
    }

    public subscribeToDeleteSideObs(): Observable<Side> {
        return this.deleteSideObs;
    }

    private getBaseSideURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/sides/";
        return "/sides/";
    }

    public createSide(productionId:number,side: Side): Observable<Side> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL();
        return this.postAction<Side>(side, controllerQuery)
            .pipe(
                map((res: Side) => {
                    //do anything with the returned data here...
                    this._newSideObs.next(res);
                    return res
                })
            )
    };

    public saveSide(id: number, productionId:number,side: Side): Observable<Side> {
        side.id = id;
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + id;
        return this.putAction<Side>(side, controllerQuery)
            .pipe(
                map((res: Side) => {
                    //do anything with the returned data here...
                this._editSideObs.next(side);
                    return side
                })
            )
    };


    public deleteSide(sideId: number, productionId: number): Observable<Side> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + sideId;
        return this.deleteAction<Side>(controllerQuery)
            .pipe(
                map((res: Side) => {
                    //do anything with the returned data here...
                    this._deleteSideObs.next(res);
                    return res
                })
            )
    };



    public getSides(productionId:number,pageIndex: number = 0, pageSize: number = 24): Observable<Side[]> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction<Side[]>(controllerQuery)
            .pipe(
                map((res: Side[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getSide(sideId: number, productionId: number): Observable<Side> {
        let controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + sideId;
        return this.getAction<Side>(controllerQuery)
            .pipe(
                map((res: Side) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

}
