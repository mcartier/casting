﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Sessions;
using Casting.Model.Entities.AttributeReferences;


namespace Casting.Model.Entities.CastingCalls
{
   public class CastingCall : Logger
    {
        public CastingCall()
        {
            CityTalentLocations = new List<CastingCallCityReference>();
            RegionTalentLocations = new List<CastingCallRegionReference>();
            CountryTalentLocations = new List<CastingCallCountryReference>();
            Roles = new List<CastingCallRole>();
            Folders = new List<CastingCallFolder>();
            CastingSessions = new List<CastingCallCastingSessionReference>();
            Unions = new List<CastingCallUnionReference>();
        }
        public int Id { get; set; }
        public int ProductionId { get; set; }
        public Production Production { get; set; }
        public int? PayDetailId { get; set; }
        public PayDetail PayDetail { get; set; }
        public int? ShootDetailId { get; set; }
        public ShootDetail ShootDetail { get; set; }
        public int? AuditionDetailId { get; set; }
        public AuditionDetail AuditionDetail { get; set; }
        public bool AcceptingSubmissions { get; set; }
        public string Name { get; set; }
        public string Instructions { get; set; }

        public bool RequiresHeadshot { get; set; }
        public int HeadshotMaxCount { get; set; }
        public bool RequiresVideo { get; set; }
        public int VideoMaxCount { get; set; }
        public bool RequiresAudio { get; set; }
        public int AudioMaxCount { get; set; }
        public bool RequiresCoverLetter { get; set; }

        public DateTimeOffset Expires { get; set; }

        public DateTimeOffset ExpiresUtc { get; set; }

        public CastingCallType CastingCallType { get; set; }
        public CastingCallPrivacy CastingCallPrivacy { get; set; }
       // public int ViewTypeId { get; set; }
        public CastingCallViewType CastingCallViewType { get; set; }
        public ICollection<CastingCallUnionReference> Unions { get; set; }

        public CastingCallTalentSourceLocationEnum TalentSourceLocation { get; set; }
        public ICollection<CastingCallCityReference> CityTalentLocations { get; set; }
        public ICollection<CastingCallRegionReference> RegionTalentLocations { get; set; }
        public ICollection<CastingCallCountryReference> CountryTalentLocations { get; set; }

        public bool SendNotifications { get; set; }
        public string LocationType { get; set; }

        public DateTimeOffset PostedDate { get; set; }

         public DateTimeOffset PostedDateUtc { get; set; }
        
         public object UpcomingAuditions { get; set; }
         [NotMapped]
         public bool CountIsSet { get; set; }
         [NotMapped]
         public int RolesCount { get; set; }
         public ICollection<CastingCallRole> Roles { get; set; }
         [NotMapped]
         public int FoldersCount { get; set; }
         public ICollection<CastingCallFolder> Folders { get; set; }
         public OrderByValuesEnum CastingCallRolesSortOrderI { get; set; }
         public ICollection<CastingCallCastingSessionReference> CastingSessions { get; set; }
        public int OrderIndex { get; set; }

    }
}
