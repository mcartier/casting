﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model
{
       public interface IObjectWithState
        {
            State State { get; set; }
        }

        public enum State
        {
            Added,
            Unchanged,
            Deleted
        }
   
}
