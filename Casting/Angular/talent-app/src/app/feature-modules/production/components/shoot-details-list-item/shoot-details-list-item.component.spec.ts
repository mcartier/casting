import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShootDetailsListItemComponent } from './shoot-details-list-item.component';

describe('ShootDetailsListItemComponent', () => {
  let component: ShootDetailsListItemComponent;
  let fixture: ComponentFixture<ShootDetailsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShootDetailsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShootDetailsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
