﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Users.Guests
{
   public class Guest: BaseCastingUser
    {
        public Guest()
        {
            base.ApplicationUserType = ApplicationUserTypeEnum.Guest;

        }
      
    }
}
