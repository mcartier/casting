﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.AccessControl;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;
using Casting.Model.Entities.Profiles;

namespace Casting.Model.Entities.Users.Agents
{
    public class Agent: BaseCastingUser
    {
        public Agent()
        {
            TalentReferences = new List<AgentTalentReference>();
            ManagedTalents = new List<Talent>();
            CityReferences = new List<AgentCityReference>();
            Profiles = new List<Profile>();

            base.ApplicationUserType = ApplicationUserTypeEnum.Agent;
        }

        public int AgencyOfficeId { get; set; }
        public AgencyOffice AgencyOffice { get; set; }
        public int AgentType { get; set; }
        public int PublishStatus { get; set; }
        public string Company { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public ICollection<Persona> PersonaContacts { get; set; }
        public ICollection<Persona> PersonaSubmissions { get; set; }
        public IList<Profile> Profiles { get; set; }


        public ICollection<AgentTalentReference> TalentReferences { get; set; }
        public OrderByValuesEnum TalentsSortOrderI { get; set; }
        public ICollection<Talent> ManagedTalents { get; set; }
        public OrderByValuesEnum ManagedTalentsSortOrderI { get; set; }

        public ICollection<AgentCityReference> CityReferences { get; set; }

        [NotMapped]
        public int TalentReferenceCount { get; set; }
        [NotMapped]
        public int ManagedTalentCount { get; set; }
        [NotMapped]
        public int CityReferenceCount { get; set; }


    }
}
