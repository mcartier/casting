﻿
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Casting.Web.Models;
using Casting.BLL.Metadata.Interfaces;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Metadata;
using System.Collections.Generic;
using Casting.Web.Helpers;

namespace Casting.Web.Controllers
{
    [RoutePrefix("api/Metadata")]
    public class MetadataController : ApiController
    {
        private IMetadataService _metadataService;
        private ICastingContext _ct;
        private IMetadataAsyncRepository _mr;
        public MetadataController(IMetadataService metadataService)
        {
            //_ct = ct;
           // _mr = _mr;
            _metadataService = metadataService;
        }



        // GET: api/"metadata/usermetadata
        [Route("metadata")]
        [System.Web.Http.ActionName("metadata")]
        [ResponseType(typeof(MetadataViewModel))]
        [HttpGet]
        public async Task<IHttpActionResult> GetMetadatas()
        {
           MetadataViewModel model = new MetadataViewModel();
           model.Accents = await _metadataService.GetAccentsAsync();
           model.EthnicStereoTypes = await _metadataService.GetEthnicStereoTypesAsync();
           model.StereoTypes = await _metadataService.GetStereoTypesAsync();
           model.Ethnicities = await _metadataService.GetEthnicitiesAsync();
           model.Unions = await _metadataService.GetUnionsAsync();
           model.Languages = await _metadataService.GetLanguagesAsync();
           model.BodyTypes = await _metadataService.GetBodyTypesAsync();
           model.HairColors = await _metadataService.GetHairColorsAsync();
           model.EyeColors = await _metadataService.GetEyeColorsAsync();
           model.SkillCategories = await _metadataService.GetSkillCategoriesWithSkillsAsync();

           model.CastingCallPrivacies = await _metadataService.GetCastingCallPrivaciesAsync();
           model.CastingCallTypes = await _metadataService.GetCastingCallTypesAsync();
           model.CastingCallViewTypes = await _metadataService.GetCastingCallViewTypesAsync();

           model.ProductionTypes = await _metadataService.GetProductionTypesAsync();
           model.ProductionTypeCategories = await _metadataService.GetProductionTypeCategoriesAsync();
           
           model.ProductionTypeCategories = await _metadataService.GetCompleteProductionTypeCategoriesAsync();
           model.RoleGenderTypes = await _metadataService.GetRoleGenderTypesAsync();
           model.RoleTypes = await _metadataService.GetRoleTypesAsync();
            return Ok(model);
        }

        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               
            }
            base.Dispose(disposing);
        }

    }
}