﻿using Casting.BLL.CastingCalls;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.CastingCalls;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingCalls;
using System.Data.Entity.Infrastructure;
using System.Web.Http.Results;
using Casting.BLL.CastingCalls.Interface;
using Casting.BLL.Location.Interfaces;
using Casting.BLL.Productions.Interfaces;
using WebGrease.Css.Extensions;
using Casting.DAL;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingcalltalentlocations")]
   // [Authorize]
    public class CastingCallTalentLocationsController : BaseDirectorApiController
    {
        private ICastingCallTalentLocationService _castingCallTalentLocationService;
        private ICastingCallService _castingCallService;
        private ILocationsService _locationsService;
        private readonly UserManager<ApplicationUser> _userManager;

        public CastingCallTalentLocationsController(UserManager<ApplicationUser> userManager, ILocationsService locationsService, ICastingCallService castingCallService, ICastingCallTalentLocationService castingCallTalentLocationService, ICastingContext castingContext) : base(castingContext)
        {
            _castingCallService = castingCallService;
            _castingCallTalentLocationService = castingCallTalentLocationService;
            _locationsService = locationsService;
            _userManager = userManager;
        }
        #region city

        // GET: api/castingcalltalentlocations/citylocations/4
        [ResponseType(typeof(IEnumerable<CastingCallCityReference>))]
        [System.Web.Http.ActionName("citylocations")]
        [Route("citylocations/{castingCallId}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallCityLocationsByCastingCall(int castingCallId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallTalentCityLocations =
                await _castingCallTalentLocationService.GetCastingCallTalentCityReferencesByCastingCallId(castingCallId);

            return Ok(castingCallTalentCityLocations);
        }
        // GET: api/castingcalltalentlocations/citylocation/4
        [ResponseType(typeof(CastingCallCityReference))]
        [System.Web.Http.ActionName("citylocation")]
        [Route("citylocation/{id}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallCityLocation(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallTalentCityLocation =
                await _castingCallTalentLocationService.GetCastingCallTalentCityReferenceById(id);

            return Ok(castingCallTalentCityLocation);
        }

        // POST: api/castingcalltalentlocations/citylocations/4
        [System.Web.Http.ActionName("citylocations")]
        [Route("citylocations/{castingCallId}")]
        [ResponseType(typeof(CastingCallCityReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostCityLocationReference(int castingCallId, NewCastingCallLocationReference value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
            /* var castingCall = new CastingCall()
           {
               DirectorId = user.BaseUserId,
               Name = name
           };*/
            if (value.CastingCallId != castingCallId)
            {
                return BadRequest("castingCall with Id " + value.CastingCallId + " does not exist");

            }
            var castingCall = await _castingCallService.GetCastingCallByIdAsync(castingCallId);
            if (castingCall == null)
            {
                return BadRequest("castingCall with Id " + value.CastingCallId + " does not exist");

            }
            var city = await _locationsService.GetCityByIdAsync(value.LocationId);
            if (city == null)
            {
                return BadRequest("City with Id " + value.LocationId + " does not exist");
            }

            var cityReferences =
                await _castingCallTalentLocationService.GetCastingCallTalentCityReferencesByCastingCallId(castingCallId);
            if (cityReferences.Any(c => c.CityId == value.LocationId))
            {
                return BadRequest("City with Id " + value.LocationId + " is already in  the list of refered cities");

            }
           
            var castingCallCityReference = new CastingCallCityReference()
            {
                CastingCallId = value.CastingCallId,
                CastingCall = castingCall,
                CityId = value.LocationId,
                City = city
                
            };
          
            var ret = await _castingCallTalentLocationService.AddNewCityTalentLocationAsync(castingCallCityReference);
            return CreatedAtRoute("DefaultApi", new { id = castingCall.Id }, castingCallCityReference);
        }


        // DELETE: api/castingcalltalentlocations/citylocation/4
        [System.Web.Http.ActionName("citylocation")]
        [Route("citylocation/{id}")]
        [ResponseType(typeof(CastingCallCityReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCityLocationReference(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var castingCallTalentCityLocation =
                await _castingCallTalentLocationService.GetCastingCallTalentCityReferenceById(id);



             if (castingCallTalentCityLocation == null)
            {
                return NotFound();
            }
            //if (castingCall.Production.DirectorId != user.BaseUserId)
           // {
          //      return Unauthorized();
          //  }

            await _castingCallTalentLocationService.DeleteCityTalentReferenceAsync(castingCallTalentCityLocation);

            return Ok(castingCallTalentCityLocation);
        }

        #endregion

        #region region

        // GET: api/castingcalltalentlocations/regionlocations/4
        [ResponseType(typeof(IEnumerable<CastingCallRegionReference>))]
        [System.Web.Http.ActionName("regionlocations")]
        [Route("regionlocations/{castingCallId}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallRegionLocationsByCastingCall(int castingCallId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallTalentRegionLocations =
                await _castingCallTalentLocationService.GetCastingCallTalentRegionReferencesByCastingCallId(castingCallId);

            return Ok(castingCallTalentRegionLocations);
        }
        // GET: api/castingcalltalentlocations/regionlocation/4
        [ResponseType(typeof(CastingCallRegionReference))]
        [System.Web.Http.ActionName("regionlocation")]
        [Route("regionlocation/{id}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallRegionLocation(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallTalentRegionLocation =
                await _castingCallTalentLocationService.GetCastingCallTalentRegionReferenceById(id);

            return Ok(castingCallTalentRegionLocation);
        }

        // POST: api/castingcalltalentlocations/regionlocations/4
        [System.Web.Http.ActionName("regionlocations")]
        [Route("regionlocations/{castingCallId}")]
        [ResponseType(typeof(CastingCallRegionReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostRegionLocationReference(int castingCallId, NewCastingCallLocationReference value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
            /* var castingCall = new CastingCall()
           {
               DirectorId = user.BaseUserId,
               Name = name
           };*/
            if (value.CastingCallId != castingCallId)
            {
                return BadRequest("castingCall with Id " + value.CastingCallId + " does not exist");

            }
            var castingCall = await _castingCallService.GetCastingCallByIdAsync(castingCallId);
            if (castingCall == null)
            {
                return BadRequest("castingCall with Id " + value.CastingCallId + " does not exist");

            }
            var region = await _locationsService.GetRegionByIdAsync(value.LocationId);
            if (region == null)
            {
                return BadRequest("Region with Id " + value.LocationId + " does not exist");
            }

            var regionReferences =
                await _castingCallTalentLocationService.GetCastingCallTalentRegionReferencesByCastingCallId(castingCallId);
            if (regionReferences.Any(c => c.RegionId == value.LocationId))
            {
                return BadRequest("Region with Id " + value.LocationId + " is already in  the list of refered regions");

            }

            var castingCallRegionReference = new CastingCallRegionReference()
            {
                CastingCallId = value.CastingCallId,
                CastingCall = castingCall,
                RegionId = value.LocationId,
                Region = region

            };

            var ret = await _castingCallTalentLocationService.AddNewRegionTalentLocationAsync(castingCallRegionReference);
            return CreatedAtRoute("DefaultApi", new { id = castingCall.Id }, castingCallRegionReference);
        }


        // DELETE: api/castingcalltalentlocations/regionlocation/4
        [System.Web.Http.ActionName("regionlocation")]
        [Route("regionlocation/{id}")]
        [ResponseType(typeof(CastingCallRegionReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteRegionLocationReference(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var castingCallTalentRegionLocation =
                await _castingCallTalentLocationService.GetCastingCallTalentRegionReferenceById(id);



            if (castingCallTalentRegionLocation == null)
            {
                return NotFound();
            }
            //if (castingCall.Production.DirectorId != user.BaseUserId)
            // {
            //      return Unauthorized();
            //  }

            await _castingCallTalentLocationService.DeleteRegionTalentReferenceAsync(castingCallTalentRegionLocation);

            return Ok(castingCallTalentRegionLocation);
        }

        #endregion


        #region country

        // GET: api/castingcalltalentlocations/countrylocations/4
        [ResponseType(typeof(IEnumerable<CastingCallCountryReference>))]
        [System.Web.Http.ActionName("countrylocations")]
        [Route("countrylocations/{castingCallId}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallCountryLocationsByCastingCall(int castingCallId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallTalentCountryLocations =
                await _castingCallTalentLocationService.GetCastingCallTalentCountryReferencesByCastingCallId(castingCallId);

            return Ok(castingCallTalentCountryLocations);
        }
        // GET: api/castingcalltalentlocations/countrylocation/4
        [ResponseType(typeof(CastingCallCountryReference))]
        [System.Web.Http.ActionName("countrylocation")]
        [Route("countrylocation/{id}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallCountryLocation(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallTalentCountryLocation =
                await _castingCallTalentLocationService.GetCastingCallTalentCountryReferenceById(id);

            return Ok(castingCallTalentCountryLocation);
        }

        // POST: api/castingcalltalentlocations/countrylocations/4
        [System.Web.Http.ActionName("countrylocations")]
        [Route("countrylocations/{castingCallId}")]
        [ResponseType(typeof(CastingCallCountryReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostCountryLocationReference(int castingCallId, NewCastingCallLocationReference value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
            /* var castingCall = new CastingCall()
           {
               DirectorId = user.BaseUserId,
               Name = name
           };*/
            if (value.CastingCallId != castingCallId)
            {
                return BadRequest("castingCall with Id " + value.CastingCallId + " does not exist");

            }
            var castingCall = await _castingCallService.GetCastingCallByIdAsync(castingCallId);
            if (castingCall == null)
            {
                return BadRequest("castingCall with Id " + value.CastingCallId + " does not exist");

            }
            var country = await _locationsService.GetCountrieByIdAsync(value.LocationId);
            if (country == null)
            {
                return BadRequest("Country with Id " + value.LocationId + " does not exist");
            }

            var countryReferences =
                await _castingCallTalentLocationService.GetCastingCallTalentCountryReferencesByCastingCallId(castingCallId);
            if (countryReferences.Any(c => c.CountryId == value.LocationId))
            {
                return BadRequest("Country with Id " + value.LocationId + " is already in  the list of refered countries");

            }

            var castingCallCountryReference = new CastingCallCountryReference()
            {
                CastingCallId = value.CastingCallId,
                CastingCall = castingCall,
                CountryId = value.LocationId,
                Country = country

            };

            var ret = await _castingCallTalentLocationService.AddNewCountryTalentLocationAsync(castingCallCountryReference);
            return CreatedAtRoute("DefaultApi", new { id = castingCall.Id }, castingCallCountryReference);
        }


        // DELETE: api/castingcalltalentlocations/countrylocation/4
        [System.Web.Http.ActionName("countrylocation")]
        [Route("countrylocation/{id}")]
        [ResponseType(typeof(CastingCallCountryReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCountryLocationReference(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var castingCallTalentCountryLocation =
                await _castingCallTalentLocationService.GetCastingCallTalentCountryReferenceById(id);



            if (castingCallTalentCountryLocation == null)
            {
                return NotFound();
            }
            //if (castingCall.Production.DirectorId != user.BaseUserId)
            // {
            //      return Unauthorized();
            //  }

            await _castingCallTalentLocationService.DeleteCountryTalentReferenceAsync(castingCallTalentCountryLocation);

            return Ok(castingCallTalentCountryLocation);
        }

        #endregion


    }
}
