﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Profiles
{
    public class TalentHighlight:Logger
    {
        public int Id { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        public int OrderIndex { get; set; }
        public string Highlight { get; set; }
        public DateTime HighlightDate { get; set; }
        public bool ShowHighlightDate { get; set; }
    }
}
