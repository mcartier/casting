﻿export class MediaSourceState {
  sourceIsValid: boolean;
    source: any;
    isLoading: boolean;
}

export class CameraState {
  camera: MediaStream;
    cameraIsValid: any;
    isLoading: boolean;
    isError: boolean;
    error: any;
}
export class ServiceState {
  isLoading: boolean;
}
export class MediaSources {
  audioDeviceInfos: any[];
  videoDeviceInfos: any[];
   audioOutpurDeviceInfos: any[];
    isLoading: boolean;
    isError: boolean;
    error:any;
}
