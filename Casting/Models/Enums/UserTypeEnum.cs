﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum UserTypeEnum
    {
        Guest = 1,
        Actor = 3,
        VoiceActor = 4,
        Model = 5,
        Dancer = 6,
        Agent = 7,
        CastingDirector = 15,
        Director = 16,
        Producer = 17
    }
}
