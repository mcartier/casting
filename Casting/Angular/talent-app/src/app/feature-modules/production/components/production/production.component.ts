import { Component, OnDestroy, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { MetadataService } from '../../../../services/metadata.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { Production, Role, AuditionDetail, ShootDetail, PayDetail, Side, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { ProductionService } from '../../../../services/production.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
//import { MatDialog } from '@angular/material';
import { ProductionRoleService } from '../../../../services/production-role.service';
import { RoleDialogService } from '../../services/role-dialog.service';
import { ProductionDialogService } from '../../services/production-dialog.service';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';

@Component({
    selector: 'app-production',
    templateUrl: './production.component.html',
    styleUrls: ['./production.component.scss']
})
export class ProductionComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor(private ProductionRoleService: ProductionRoleService,
        private productionExtraService: ProductionExtraService,
        private metadataService: MetadataService,
        private productionService: ProductionService,
        public stateService: StateService,
        public transitionService: TransitionService,
        public transition: Transition, private roleDialogService: RoleDialogService,
        private productionDialogService: ProductionDialogService,
        private messageDispatchService: MessageDispatchService) { }

  @Input() production: Production
  @Input() private metadata: MetadataModel;
    private newRoleSubscription: Subscription;
    private deleteRoleSubscription: Subscription;
    private editRoleSubscription: Subscription;


    private newShootDetailSubscription: Subscription;
    private deleteShootDetailSubscription: Subscription;
    private editShootDetailSubscription: Subscription;

    private newAuditionDetailSubscription: Subscription;
    private deleteAuditionDetailSubscription: Subscription;
    private editAuditionDetailSubscription: Subscription;

    private newPayDetailSubscription: Subscription;
    private deletePayDetailSubscription: Subscription;
    private editPayDetailSubscription: Subscription;

    private newSideSubscription: Subscription;
    private deleteSideSubscription: Subscription;
    private editSideSubscription: Subscription;

    public auditionDetail: AuditionDetail;
    public shootDetail: ShootDetail;
    public payDetail: PayDetail;
    public side:Side;
    ngOnInit() {
        this.deleteRoleSubscription = this.ProductionRoleService.subscribeToDeleteRoleObs()
            .subscribe(
                role => {
                    if (role) {
                        this.production.roles.splice(this.production.roles.indexOf(role), 1);
                    }
                });
        this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(
                (role: Role) => {
                    if (role) {
                        this.production.roles[this.production.roles.indexOf(role)] = role;
                    }
                });
        this.editShootDetailSubscription = this.productionExtraService.subscribeToEditShootDetailObs()
          .subscribe(
            (detail: ShootDetail) => {
                if (detail) {
                    this.production.shootDetails[this.production.shootDetails.indexOf(this.production.shootDetails.find(d => d.id == detail.id))] = detail;
              }
            });

        this.newShootDetailSubscription = this.productionExtraService.subscribeToCreateShootDetailObs()
          .subscribe(
            (detail: ShootDetail) => {
                if (detail) {
                    this.production.shootDetails.push(detail);
              }
            });

        this.deleteShootDetailSubscription = this.productionExtraService.subscribeToDeleteShootDetailObs()
          .subscribe(
            (detail: ShootDetail) => {
                if (detail) {
                   this.production.shootDetails.splice(this.production.shootDetails.indexOf(this.production.shootDetails.find(d => d.id == detail.id)), 1);
              }
            });

        this.editSideSubscription = this.productionExtraService.subscribeToEditSideObs()
          .subscribe(
            (detail: Side) => {
              if (detail) {
                this.production.sides[this.production.sides.indexOf(this.production.sides.find(d => d.id == detail.id))] = detail;
              }
            });

        this.newSideSubscription = this.productionExtraService.subscribeToCreateSideObs()
          .subscribe(
            (detail: Side) => {
              if (detail) {
                this.production.sides.push(detail);
              }
            });

        this.deleteSideSubscription = this.productionExtraService.subscribeToDeleteSideObs()
          .subscribe(
            (detail: Side) => {
              if (detail) {
                this.production.sides.splice(this.production.sides.indexOf(this.production.sides.find(d => d.id == detail.id)), 1);
              }
            });

        this.editAuditionDetailSubscription = this.productionExtraService.subscribeToEditAuditionDetailObs()
          .subscribe(
            (detail: AuditionDetail) => {
              if (detail) {
                this.production.auditionDetails[this.production.auditionDetails.indexOf(this.production.auditionDetails.find(d => d.id == detail.id))] = detail;
              }
            });

        this.newAuditionDetailSubscription = this.productionExtraService.subscribeToCreateAuditionDetailObs()
          .subscribe(
            (detail: AuditionDetail) => {
              if (detail) {
                this.production.auditionDetails.push(detail);
              }
            });

        this.deleteAuditionDetailSubscription = this.productionExtraService.subscribeToDeleteAuditionDetailObs()
          .subscribe(
            (detail: AuditionDetail) => {
              if (detail) {
                this.production.auditionDetails.splice(this.production.auditionDetails.indexOf(this.production.auditionDetails.find(d => d.id == detail.id)), 1);
              }
            });

        this.editPayDetailSubscription = this.productionExtraService.subscribeToEditPayDetailObs()
          .subscribe(
            (detail: PayDetail) => {
              if (detail) {
                this.production.payDetails[this.production.payDetails.indexOf(this.production.payDetails.find(d => d.id == detail.id))] = detail;
              }
            });

        this.newPayDetailSubscription = this.productionExtraService.subscribeToCreatePayDetailObs()
          .subscribe(
            (detail: PayDetail) => {
              if (detail) {
                this.production.payDetails.push(detail);
              }
            });

        this.deletePayDetailSubscription = this.productionExtraService.subscribeToDeletePayDetailObs()
          .subscribe(
            (detail: PayDetail) => {
              if (detail) {
                this.production.payDetails.splice(this.production.payDetails.indexOf(this.production.payDetails.find(d => d.id == detail.id)), 1);
              }
            });

    }
    ngOnDestroy() {
        this.productionDialogService.hideSidesDialog();
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();

        }

        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();

        }
        if (this.editShootDetailSubscription) {
          this.editShootDetailSubscription.unsubscribe();

        }
        if (this.newShootDetailSubscription) {
          this.newShootDetailSubscription.unsubscribe();

        }
        if (this.deleteShootDetailSubscription) {
          this.deleteShootDetailSubscription.unsubscribe();

        }

        if (this.editSideSubscription) {
          this.editSideSubscription.unsubscribe();

        }
        if (this.newSideSubscription) {
          this.newSideSubscription.unsubscribe();

        }
        if (this.deleteSideSubscription) {
          this.deleteSideSubscription.unsubscribe();

        }

        if (this.editAuditionDetailSubscription) {
          this.editAuditionDetailSubscription.unsubscribe();

        }
        if (this.newAuditionDetailSubscription) {
          this.newAuditionDetailSubscription.unsubscribe();

        }
        if (this.deleteAuditionDetailSubscription) {
          this.deleteAuditionDetailSubscription.unsubscribe();

        }

        if (this.editPayDetailSubscription) {
          this.editPayDetailSubscription.unsubscribe();

        }
        if (this.newPayDetailSubscription) {
          this.newPayDetailSubscription.unsubscribe();

        }
        if (this.deletePayDetailSubscription) {
          this.deletePayDetailSubscription.unsubscribe();

        }
    }

    ngAfterViewInit() {

        if (false) {
            setTimeout(() => {
                this.newRoleSubscription = this.roleDialogService.createRole(this.production)
                    .subscribe(
                        role => {
                            if (role) {
                                this.production.roles.push(role);
                            }
                            this.newRoleSubscription.unsubscribe();
                        });


            })
        }

    }
    private addSide() {
      this.productionDialogService.showSidesDialog(null);
    }
    private addAuditionDetails() {
        this.productionDialogService.showAuditionDetailsDialog(null);
    }
    private addShootDetails() {
        this.productionDialogService.showShootDetailsDialog(null);
    }
    private addPayDetails() {
        this.productionDialogService.showPayDetailsDialog(null);
    }

       private viewAuditionDetails(detail: AuditionDetail) {
      // this.shootDetail = detail;
      this.productionDialogService.showAuditionDetailsDialog(detail);

    }
    private viewPayDetails(detail: PayDetail) {
      // this.shootDetail = detail;
      this.productionDialogService.showPayDetailsDialog(detail);

    }
    private viewSides(detail: Side) {
      // this.shootDetail = detail;
      this.productionDialogService.showSidesDialog(detail);

    }
    private createNewProduction() {

    }
    private createNewRole() {
        if (1 == 1) {
          this.productionDialogService.showNewRolesDialog(null);
 // this.stateService.go("productionnewrole", { production: this.production, productionId: this.production.id });

        } else {
            this.newRoleSubscription = this.roleDialogService.createRole(this.production)
                .subscribe(
                    role => {
                        if (role) {
                            this.production.roles.push(role);
                        }
                        this.newRoleSubscription.unsubscribe();
                    });
        }

    }
    private createNewCastingCall() {
        this.stateService.go("newcastingCall", { productionId: this.production.id });
    }

}
