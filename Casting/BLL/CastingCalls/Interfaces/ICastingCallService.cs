﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Enums;

namespace Casting.BLL.CastingCalls.Interfaces
{
    public interface ICastingCallService
    {
        Task<CastingCall> GetCastingCallByIdAsync(int castingCallId);
        Task<CastingCall> GetCastingCallByIdWithRolesAsync(int castingCallId);
        Task<CastingCall> GetCastingCallByIdWithRolesAndPersonasAsync(int castingCallId);
        Task<CastingCall> GetCastingCallByIdWithRolesPersonasAndAssetsAsync(int castingCallId);
        Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdAsync(int productionId);
        Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdWithRolesAsync(int productionId);
        Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdWithRolesPersonasAsync(int productionId);
        Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdWithRolesPersonasAssetsAsync(int productionId);

        Task<IEnumerable<CastingCall>> GetCastingCallsAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery,
            int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending);

        Task<int> AddNewCastingCallAsync(CastingCall castingCall);
        Task<int> UpdateCastingCallAsync(CastingCall castingCall);
        Task<int> DeleteCastingCall(CastingCall castingCall);
    }
}