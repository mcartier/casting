import { Component, OnDestroy, OnInit, Input} from '@angular/core';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Subscription } from 'rxjs';
import { MetadataService } from '../../../../services/metadata.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { ProductionService } from '../../../../services/production.service';
import { ProductionComponentService } from '../../services/production-component.service';
import { fadeInAnimation } from '../../../../animations/router.animation';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-production-list',
  templateUrl: './production-list.component.html',
    styleUrls: ['./production-list.component.scss'],
  host: { 'class': 'p-grid p-dir-col' }
})
export class ProductionListComponent implements OnInit {

    constructor(

     // private metadataService: MetadataService,
        private productionService: ProductionService,
      public stateService: StateService,
      public transitionService: TransitionService,
        public transition: Transition,
   //      private productionComponentService: ProductionComponentService
    ) { }
    private length = 100;
    private pageSize = 10;
    private pageSizeOptions: number[] = [5, 10, 25, 100];
   
    @Input() productions: Production[];

    ngOnInit() {
    }
    changePageSizeOption($event: PageEvent) {
    
      this.length = $event.length;
       this.pageSize = $event.pageSize;
      this.length = $event.length;
        this.productionService.getProductions($event.pageIndex, $event.pageSize)
            .subscribe(productions => {
             this.productions = productions;
          })

    }
  private createNewProduction() {
    this.stateService.go('newproduction');
    //this.productionComponentService.scrollToProduction();
  }

}
