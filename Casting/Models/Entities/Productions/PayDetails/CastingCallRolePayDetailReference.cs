﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Campaigns;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions.PayDetails
{
   public class CampaignRolePayDetailReference : Logger
    {
        public CampaignRolePayDetailReference()
        {
          

        }
        public int Id { get; set; }
      
        public int PayDetailId { get; set; }
        public PayDetail PayDetail { get; set; }

        public int CampaignRoleId { get; set; }
        public CampaignRole CampaignRole { get; set; }


    }
}
