import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShootDetailsListComponent } from './shoot-details-list.component';

describe('ShootDetailsListComponent', () => {
  let component: ShootDetailsListComponent;
  let fixture: ComponentFixture<ShootDetailsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShootDetailsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShootDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
