﻿import { TransitionService } from '@uirouter/core';
import { UserService } from '../services/user.service'

export function loadSpinnerOnLongTransitionHook(transitionService: TransitionService) {
     transitionService.onBefore({}, function (transition) {
      console.log(
        "Successful Transition from " + transition.from().name +
        " to " + transition.to().name
      );
    }
    , { priority: 10 });
}
