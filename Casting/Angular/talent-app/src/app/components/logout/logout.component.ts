import { Component, OnInit } from '@angular/core';
import { StateService} from '@uirouter/core';
import { AuthenticationService } from '../../services/authentication.service';
//import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
@Component({
  templateUrl: './logout.component.html'
})
export class LogoutComponent implements OnInit {

  
    constructor(private authenticationService: AuthenticationService, public stateService: StateService) { }
      ngOnInit() {
        this.authenticationService.logout();
      }

      
}
