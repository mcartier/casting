﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;
using System.ComponentModel.DataAnnotations;

namespace Casting.Model.Entities.Productions
{
   public class File : Logger
    {
        public File()
        {
          

        }
        public int Id { get; set; }
        public string FileName { get; set; }
        public string SubFolder { get; set; }
        public string FileId { get; set; }
        
        
    }
}
