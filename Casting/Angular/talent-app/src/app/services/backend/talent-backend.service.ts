import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Talent } from '../../models/talent';
import { WINDOW } from '../browser.window.provider';
import { HttpHelpers } from '../../utilities/HttpHelpers';
@Injectable({
  providedIn: 'root'
})
export class TalentBackendService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }
    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;
    private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/talents/";
        return "/api/talents/";
    }

    public createTalent(talent: Talent): Observable<Talent> {
        let controllerQuery = this.getBaseURL();
        return this.postAction<Talent>(talent, controllerQuery)
            .pipe(
                map((res: Talent) => {
                    //do anything with the returned data here...
                    return res
                })
            )
    };

    public saveTalent(talent: Talent): Observable<Talent> {
        let controllerQuery = this.getBaseURL() + talent.id;
        return this.putAction<Talent>(talent, controllerQuery)
            .pipe(
                map((res: Talent) => {
                    //do anything with the returned data here...
                    return res
                })
            )
    };


    public deleteTalent(talentId: number): Observable<Talent> {
        let controllerQuery = this.getBaseURL() + talentId;
        return this.deleteAction<Talent>(controllerQuery)
            .pipe(
            map((res: Talent) => {
                    //do anything with the returned data here...
                    return res
                })
            )
    };



    public getTalents(orderByIndex: number): Observable<Talent[]> {
        let controllerQuery = this.getBaseURL() + "?orderBy=" + orderByIndex;
          return this.getAction<Talent[]>(controllerQuery)
            .pipe(
                map((res: Talent[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getTalent(talentId: number): Observable<Talent> {
        let controllerQuery = this.getBaseURL() + talentId;
        return this.getAction<Talent>(controllerQuery)
            .pipe(
                map((res: Talent) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

}
