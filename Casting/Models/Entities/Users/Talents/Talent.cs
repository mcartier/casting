﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Users.Talents
{
    public abstract class Talent:BaseCastingUser
   
    {
        public Talent()
        {
            AgentReferences = new List<AgentTalentReference>();
            Profiles = new List<Profile>();
            base.ApplicationUserType = ApplicationUserTypeEnum.Talent;

        }
       // public string FirstNames { get; set; }
       // public string LastNames { get; set; }

       // public int Id { get; set; }
        public TalentTypeEnum TalentType { get; set; }
        public ICollection<AgentTalentReference> AgentReferences { get; set; }
        public Agent MainAgent { get; set; }
        public int? MainAgentId { get; set; }
        public IList<Profile> Profiles { get; set; }

        /*public ProfileTypeEnum ProfileType { get; set; }

        [ForeignKey("Profile")]
        public int? ProfileId { get; set; }
          private Profile _profile;
        public Profile Profile
        {
            get { return _profile; }
            set
            {
                if (value != null)
                {
                    _profile = value;
                       _profile.TalentType = this.TalentType;
                    this.ProfileType = _profile.ProfileType;
                }

            }
        }
        */


        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        [NotMapped]
        public int AgentReferenceCount { get; set; }

    }
}
