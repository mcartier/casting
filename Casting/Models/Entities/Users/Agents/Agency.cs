﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Locations;

namespace Casting.Model.Entities.Users.Agents
{
    public class Agency:Logger
    {
        public Agency()
        {
            Agents = new List<Agent>();
            Offices = new List<AgencyOffice>();

        }
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Agent> Agents { get; set; }

        public ICollection<AgencyOffice> Offices { get; set; }

        [NotMapped]
        public int AgentCount { get; set; }

        [NotMapped]
        public int OfficeCount { get; set; }
    }
}
