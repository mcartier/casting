﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Talents;


namespace Casting.Model
{
    public class TalentAgentPivot
    {
      

        public int TalentId { get; set; }
        public Talent Talent { get; set; }
        public int AgentId { get; set; }
        public Agent Agent { get; set; }
        public bool IsMainAgent { get; set; }
    }
}
