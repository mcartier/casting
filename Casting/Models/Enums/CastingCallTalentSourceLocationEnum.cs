﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum CastingCallTalentSourceLocationEnum
    {
        Local = 1,
        National = 3,
        Worldwide = 7
    }
}
