﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Campaigns;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{
    public class CampaignRepository : AsyncRepository, ICampaignAsyncRepository
    {
        ICastingContext _castingContext;
        public CampaignRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Campaigns

        public async Task<Campaign> GetCampaignById(CampaignParameterIncludeInQueryEnum parameterIncludeInQuery, int campaignId)
        {
            IQueryable<Campaign> campaigns = GetCampaignsQueryable(parameterIncludeInQuery);

            return await campaigns.FirstOrDefaultAsync(p => p.Id == campaignId);


        }

        public async Task<int> AddNewCampaignAsync(Campaign campaign)
        {
            _castingContext.Set<Campaign>().Add(campaign);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteCampaignAsync(Campaign campaign)
        {
            _castingContext.Set<Campaign>().Remove(campaign);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateCampaignAsync(Campaign campaign)
        {
            SetEntityState(campaign, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Campaign>> GetCampaignsByProductionId(CampaignParameterIncludeInQueryEnum parameterIncludeInQuery, int productionId)
        {
            var campaigns = GetCampaignsQueryable(parameterIncludeInQuery);

            return await campaigns.Where(d => d.ProductionId == productionId).ToListAsync();
        }

        public async Task<IEnumerable<Campaign>> GetCampaigns(CampaignParameterIncludeInQueryEnum parameterIncludeInQuery, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending)
        {
            var campaigns = GetCampaignsQueryable(parameterIncludeInQuery);
            switch (orderBy)
            {
                case OrderByValuesEnum.CreatedDateAscending:

                    campaigns = campaigns.OrderBy(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.CreatedDateDescending:

                    campaigns = campaigns.OrderByDescending(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.NameAscending:

                    // campaigns = campaigns.OrderBy(o => o.Name);
                    break;
                case OrderByValuesEnum.NameDescending:

                    // campaigns = campaigns.OrderByDescending(o => o.Name);
                    break;
            }
            return await campaigns.Skip(pageIndex * pageSize).Take(pageSize).ToListAsync();
        }



        #region PrivateMethods
        private IQueryable<Campaign> GetCampaignsQueryable(CampaignParameterIncludeInQueryEnum parameterIncludeInQuery)
        {
            IQueryable<Campaign> campaigns = from campaign in _castingContext.Campaigns select campaign;
            if (parameterIncludeInQuery == CampaignParameterIncludeInQueryEnum.Roles)
            {
                campaigns
                    .Include(i => i.Roles);
            }

            if (parameterIncludeInQuery == CampaignParameterIncludeInQueryEnum.RolesAndPersonas)
            {
                campaigns
                    .Include("Roles.Personas.Persona");
            }
            if (parameterIncludeInQuery == CampaignParameterIncludeInQueryEnum.RolesPersonasAndAssets)
            {
                campaigns
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }
            return campaigns;
        }


        #endregion
        #endregion


        #region Roles

        public async Task<CampaignRole> GetCampaignRoleById(CampaignRoleParameterIncludeInQueryEnum includeInQuery, int campaignRoleId)
        {
            IQueryable<CampaignRole> campaignRoles = GetCampaignRolesQueryable(includeInQuery);

            return await campaignRoles.FirstOrDefaultAsync(p => p.Id == campaignRoleId);


        }

        public async Task<int> AddNewCampaignRoleAsync(bool orderAtTheTop, CampaignRole campaignRole)
        {

            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexCampaignRoleByCampaignAsync(campaignRole);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    campaignRole.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<CampaignRole>().Add(campaignRole);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderCampaignRoleAsync(1, campaignRole);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;

        }
        public async Task<int> DeleteCampaignRoleAsync(CampaignRole campaignRole)
        {

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderCampaignRoleAsync(0, campaignRole);

                    _castingContext.Set<CampaignRole>().Remove(campaignRole);

                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    result = 0;
                    dbContextTransaction.Rollback();
                }
            }

            return result;
        }

        public async Task<int> UpdateCampaignRoleAsync(CampaignRole campaignRole)
        {
            SetEntityState(campaignRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<CampaignRole>> GetCampaignRolesByCampaignId(CampaignRoleParameterIncludeInQueryEnum includeInQuery, int campaignId)
        {
            var campaignRoles = GetCampaignRolesQueryable(includeInQuery);

            return await campaignRoles.Where(d => d.CampaignId == campaignId).ToListAsync();
        }
        public async Task<int> ReOrderCampaignRoleAsync(int desiredOrderIndex, CampaignRole campaignRole)
        {
            IEnumerable<CampaignRole> campaignRoles = await GetCampaignRolesByCampaignId(CampaignRoleParameterIncludeInQueryEnum.Nothing, campaignRole.CampaignId);

            if (desiredOrderIndex == 0)
            {
                return await ReOrderCampaignRoleUpAsync(desiredOrderIndex, campaignRole, campaignRoles);
            }
            else if (desiredOrderIndex > campaignRole.OrderIndex)
            {
                return await ReOrderCampaignRoleUpAsync(desiredOrderIndex, campaignRole, campaignRoles);
            }
            else if (desiredOrderIndex < campaignRole.OrderIndex)
            {
                return await ReOrderCampaignRoleDownAsync(desiredOrderIndex, campaignRole, campaignRoles);
            }
            return await Task.FromResult(0);

        }



        #region PrivateMethods
        private async Task<int> GetHighestOrderIndexCampaignRoleByCampaignAsync(CampaignRole campaignRole)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                maxOrderIndex = (from role in _castingContext.CampaignRoles
                                 where role.CampaignId == campaignRole.CampaignId
                                 select role.OrderIndex).DefaultIfEmpty(0).Max();




            });

            return maxOrderIndex;
        }

        private async Task<int> ReOrderCampaignRoleDownAsync(int desiredOrderIndex, CampaignRole campaignRole,
            IEnumerable<CampaignRole> campaignRoles)
        {
            foreach (var role in campaignRoles)
            {
                if (role.OrderIndex >= desiredOrderIndex &&
                    role.OrderIndex < campaignRole.OrderIndex)
                {
                    role.OrderIndex++;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            campaignRole.OrderIndex = desiredOrderIndex;
            SetEntityState(campaignRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderCampaignRoleUpAsync(int desiredOrderIndex, CampaignRole campaignRole,
            IEnumerable<CampaignRole> campaignRoles)
        {
            foreach (var role in campaignRoles)
            {
                if (role.OrderIndex > campaignRole.OrderIndex &&
                    (role.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    role.OrderIndex--;
                    SetEntityState(role, EntityState.Modified);
                }

            }

            campaignRole.OrderIndex = desiredOrderIndex;
            SetEntityState(campaignRole, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }


        private IQueryable<CampaignRole> GetCampaignRolesQueryable(CampaignRoleParameterIncludeInQueryEnum includeInQuery)
        {
            IQueryable<CampaignRole> campaignRoles = from campaignRole in _castingContext.CampaignRoles select campaignRole;
            if (includeInQuery == CampaignRoleParameterIncludeInQueryEnum.Personas)
            {
                campaignRoles
                    .Include("Roles.Personas.Persona");
            }
            if (includeInQuery == CampaignRoleParameterIncludeInQueryEnum.PersonasAndAssets)
            {
                campaignRoles
                    .Include("Roles.Personas.Persona")
                    .Include("Roles.Personas.Persona.VideoReferences.Video")
                    .Include("Roles.Personas.Persona.AudioReferences.Audio")
                    .Include("Roles.Personas.Persona.ResumeReferences.Resume")
                    .Include("Roles.Personas.Persona.ImageReferences.Image");
            }
            return campaignRoles;
        }


        #endregion
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
