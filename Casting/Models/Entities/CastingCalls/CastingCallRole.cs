﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.AttributeReferences;


namespace Casting.Model.Entities.CastingCalls
{
    public class CastingCallRole : Logger
    {
        public CastingCallRole()
        {
            Unions = new List<CastingCallRoleUnionReference>();

        }
        public int Id { get; set; }
        public int CastingCallId { get; set; }
        public CastingCall CastingCall { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int FolderRolesCount { get; set; }
        public ICollection<CastingCallFolderRole> FolderRoles { get; set; }
        public Role Role { get; set; }
        public int RoleId { get; set; }
        public int? PayDetailId { get; set; }
        public PayDetail PayDetail { get; set; }
        public int? ShootDetailId { get; set; }
        public ShootDetail ShootDetail { get; set; }
        public int? AuditionDetailId { get; set; }
        public AuditionDetail AuditionDetail { get; set; }
        public bool RequiresHeadshot { get; set; }
        public int HeadshotMaxCount { get; set; }
        public bool RequiresVideo { get; set; }
        public int VideoMaxCount { get; set; }
        public bool RequiresAudio { get; set; }
        public int AudioMaxCount { get; set; }
        public bool RequiresCoverLetter { get; set; }

        public ICollection<CastingCallRoleUnionReference> Unions { get; set; }
        public int OrderIndex { get; set; }
        [NotMapped]
        public int PersonasCount { get; set; }
        public ICollection<CastingCallRoleSubmissionPersonaReference> Personas { get; set; }
        public OrderByValuesEnum SubmissionsSortOrderI { get; set; }


        
    }
}
