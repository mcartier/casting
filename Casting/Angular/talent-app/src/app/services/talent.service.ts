import { Injectable, Inject } from '@angular/core';
import { Observable, of } from 'rxjs';
import {  HttpErrorResponse } from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { Talent } from '../models/talent';
import { TalentBackendService } from './backend/talent-backend.service';
import { Subject } from "rxjs";
import { BehaviorSubject, throwError } from "rxjs";
import { AlertService } from "./alert.service.js";
import { IAlert, AlertType } from '../models/alertInterface'
@Injectable({
    providedIn: 'root'
})
export class TalentService {

    constructor(private alertService:AlertService,private backEnd: TalentBackendService) {


    }

    private compareFirstName(a, b): number {
        if (a.firstName < b.firstName)
            return -1;
        if (a.firstName > b.firstName)
            return 1;
        return 0;
    };
    private compareLastName(a, b): number {
        if (a.lastName < b.lastName)
            return -1;
        if (a.lastName > b.lastName)
            return 1;
        return 0;
    };
    nyteest() {

        return () => { }
    }
    private _talents: Talent[] = [];
    private _orderByIndex: number = 0;
    private _talentsObs: BehaviorSubject<Talent[]> = new BehaviorSubject([]);
    private _talentObsArray = {};
    public readonly talentsObs: Observable<Talent[]> = this._talentsObs.asObservable();

    public SubscribeToTalentObs(talentId: number): Observable<Talent> {

        return this._getOrCreateTalentObsInArray(talentId).asObservable();
    }

    private makeString(value: number): string {
        return value.toString();
    };
    private _getOrCreateTalentObsInArray(talentId: number): BehaviorSubject<Talent> {
        var talentId$ = this.makeString(talentId);
        if (!(talentId$ in this._talentObsArray)) {
            this._talentObsArray[talentId$] = new BehaviorSubject<Talent>({
                firstName: "",
                lastName: "",
                id: 0,
                createdDate: new Date
            });
        }
        return this._talentObsArray[talentId$];
    }
    private _removeTalentObsFromArray(talentId: number): void {
        var talentId$ = this.makeString(talentId);
        if (talentId$ in this._talentObsArray) {
            delete this._talentObsArray[talentId$];
        }
    }

    private _isTalentIsTalentArray(talentId: number): boolean {
        return this._talents.some(s => s.id == talentId);
    }

    private _getTalents(orderByIndex: number, dispatchAlert: boolean = false): Observable<Talent[]> {

      if (orderByIndex == null || orderByIndex != 1) {
        orderByIndex = 0;
        //}
        if (this._orderByIndex == orderByIndex && this._talents !== null && this._talents.length > 0) {
          return of(this._talents)
            .pipe(
              map((res: Talent[]) => {
                return res
              })
            );
        } else {
          return this.backEnd.getTalents(orderByIndex)
            .pipe(
              map((res: Talent[]) => {
                this._talents = res;
                res.forEach((talent) => {
                  if (!this._isTalentIsTalentArray(talent.id)) {
                    this._talents.push(talent);
                  } else {
                    this._talents[this._talents.findIndex(s => s.id == talent.id)] = talent;
                  }
                });
                if (dispatchAlert) {
                  this.alertService.success("Retrieving talents succeeded.");
                }

                return res
              }),
              catchError((error) => {
                return this._handleError(error, "Retrieving talents failed.", dispatchAlert);;
              })
            );
        }
      };
    }

    private _getTalent(talentId: number, dispatchAlert: boolean = false): Observable<Talent> {

        if (this._isTalentIsTalentArray(talentId)) {
            return of(this._talents.find(s => s.id == talentId))
                .pipe(
                    map((res: Talent) => {
                      return res;
                    })
                );
        }
        else {
            return this.backEnd.getTalent(talentId)
                .pipe(
                map((res: Talent) => {
                    if (dispatchAlert) { this.alertService.success("Retrieving talent succeeded."); }
                     if (!this._isTalentIsTalentArray(talentId)) {
                       this._talents.push(res);
                         this._talentsObs.next(this._talents);
                     }

                     return res;
                }),
                catchError((error) => {
                    return this._handleError(error, "Retrieving talent failed.", dispatchAlert);
                })
                );
        }
    };
     public LoadTalent(talentId: number, dispatchAlert:boolean = false): void {
        let studObs = this.getTalent(talentId, dispatchAlert);
        studObs.subscribe();
    }
    public LoadTalentList(orderByIndex: number, dispatchAlert: boolean = false): void {
        let studObs = this.getTalents(orderByIndex, dispatchAlert);
        studObs.subscribe();
    }
    public addTalent(talent: Talent, dispatchAlert: boolean = false): void {
        let studObs = this.createTalent(talent, dispatchAlert);
        studObs.subscribe();
    }
    public updateTalent(talent: Talent, dispatchAlert: boolean = false): void {
        let studObs = this.saveTalent(talent, dispatchAlert);
        studObs.subscribe();
    }
    public removeTalent(talent: Talent, dispatchAlert: boolean = false): void {
        this.removeTalentById(talent.id, dispatchAlert);
    }
    public removeTalentById(talentId: number, dispatchAlert: boolean = false): void {
        let studObs = this.deleteTalent(talentId, dispatchAlert);
        studObs.subscribe();
    }
    public getTalents(orderByIndex: number, dispatchAlert: boolean = false): Observable<Talent[]> {

        if (orderByIndex == null || orderByIndex != 1) {
            orderByIndex = 0;
        }
        return this._getTalents(orderByIndex, dispatchAlert)
            .pipe(
            map((res: Talent[]) => {
                this._talentsObs.next(this._talents);

                    return res
            })
            );
    };
    public getTalent(talentId: number, dispatchAlert: boolean = false): Observable<Talent> {

        return this._getTalent(talentId, dispatchAlert)
            .pipe(
                map((res: Talent) => {
                this._getOrCreateTalentObsInArray(talentId).next(res);
                return res
            })
            );
    };

    public createTalent(talent: Talent, dispatchAlert: boolean = false): Observable<Talent> {
        return this.backEnd.createTalent(talent)
            .pipe(
                map((res: Talent) => {
                    //do anything with the returned data here...
                if (!this._isTalentIsTalentArray(res.id)) {
                    this._talents.push(res);
                    this._talentsObs.next(this._talents);
                };
                    
                    this._getOrCreateTalentObsInArray(res.id).next(res);
                this._talentsObs.next(this._talents);
                if (dispatchAlert) { this.alertService.success("Saving new talent " + talent.firstName + " " + talent.lastName + " succeeded."); }

                    return res
            }),
            catchError((error) => {
                return this._handleError(error, "Saving new talent " + talent.firstName + " " + talent.lastName + " failed.", dispatchAlert);;
            })
            )
    };



    public saveTalent(talent: Talent, dispatchAlert: boolean = false): Observable<Talent> {

        return this.backEnd.saveTalent(talent)
            .pipe(
                map((res: Talent) => {
                    //do anything with the returned data here...
                if (!this._isTalentIsTalentArray(talent.id)) {
                    this._talents.push(talent);
                }
                else {
                    this._talents[this._talents.findIndex(s => s.id == talent.id)] = talent;
                }
                this._talentsObs.next(this._talents);
                this._getOrCreateTalentObsInArray(talent.id).next(talent);
               if (dispatchAlert) { this.alertService.success("Updating talent " + talent.firstName + " " + talent.lastName + " succeeded."); }

                return talent;
            }),
            catchError((error) => {
                return this._handleError(error, "Updating talent " + talent.firstName + " " + talent.lastName + " failed.", dispatchAlert);;
            })
            )
    };

    public deleteTalent(talentId: number, dispatchAlert: boolean = false): Observable<Talent> {
        return this.backEnd.deleteTalent(talentId)
            .pipe(
            map((res: Talent) => {
                    //do anything with the returned data here...
                    this._talents.splice(this._talents.findIndex(s => s.id == res.id), 1);
                    this._getOrCreateTalentObsInArray(talentId).complete();
                    this._removeTalentObsFromArray(talentId);
                this._talentsObs.next(this._talents);

                    if(dispatchAlert) {this.alertService.success("Deleting talent succeeded.");}
                    return res
            }),
            catchError((error) => {
                return this._handleError(error, "Deleting of talent failed.", dispatchAlert);;
            })
            )
    };
    private _handleSuccessError(message: string, dispatchAlert: boolean) {
        if (dispatchAlert) {
            this.alertService.success(message);
        }
    }
    private _handleError(error: HttpErrorResponse, message: string, dispatchAlert:boolean) {
        this.alertService.error(error, message);
     
        return throwError(error);
    }

}
