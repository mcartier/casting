﻿using Casting.Model.Entities.Productions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
namespace Casting.Model.Entities.AttributeReferences
{
    public class ProfileHairColorReference: HairColorReference
    {
        public ProfileHairColorReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.Profile;
        }
        public int ProfileId { get; set; }
        
    }
}
