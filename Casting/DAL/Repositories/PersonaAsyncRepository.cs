﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{
   
    public class PersonaAsyncRepository : AsyncRepository, IPersonaAsyncRepository
    {
        ICastingContext _castingContext;
        public PersonaAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Personas
        public async Task<Persona> GetPersonaByIdAsync(bool withTalent, bool withAssets, bool withAttributes, bool  withAgents, int personaId)
        {
            IQueryable<Persona> personas = GetPersonasQueryable(withTalent, withAssets, withAttributes);// from persona in _castingContext.Personas select persona;
           
            return await personas.FirstOrDefaultAsync(p => p.Id == personaId);
        }

        public async Task<int> AddNewPersonaAsync(Persona persona)
        {
            _castingContext.Set<Persona>().Add(persona);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeletePersonaAsync(Persona persona)
        {
            _castingContext.Set<Persona>().Remove(persona);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdatePersonaAsync(Persona persona)
        {
            SetEntityState(persona, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Persona>> GetPersonasByCastingCallFolderRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId)
        {
            var personas = GetCastingFolderRolePersonasQueryable(withTalent, withAssets, withAttributes,
                roleId);
            return await personas.ToListAsync();
        }
        public async Task<IEnumerable<Persona>> GetPersonasByCastingCallRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId)
        {
            var personas = GetCastingCallRoleSubmissionPersonasQueryable(withTalent, withAssets, withAttributes,
                roleId);
            return await personas.ToListAsync();
        }


        public async Task<IEnumerable<Persona>> GetPersonasBySessionFolderRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId)
        {
            var personas = GetCastingFolderRolePersonasQueryable(withTalent, withAssets, withAttributes,
                roleId);
            return await personas.ToListAsync();
        }

        public async Task<IEnumerable<Persona>> GetPersonasBySessionRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId)
        {
            var personas = GetCastingRolePersonasQueryable(withTalent, withAssets, withAttributes,
                roleId);
            return await personas.ToListAsync();
        }


        #region PrivateMethods
        private IQueryable<Persona> GetCastingCallRoleSubmissionPersonasQueryable(bool withTalent, bool withAssets,
           bool withAttributes, int roleId)
        {
            var personas = from personaReferences in _castingContext.RoleSubmissionPersonaReferences
                           where personaReferences.CastingCallRoleId == roleId
                           select personaReferences.Persona;
            if (withAssets)
            {
                personas
                    .Include(i => i.ImageReferences)
                    .Include(r => r.ResumeReferences)
                    .Include(a => a.AudioReferences)
                    .Include(v => v.VideoReferences);
            }

            if (withAttributes)
            {
                personas
                    .Include(e => e.Ethnicities)
                    .Include(s => s.StereoTypes)
                    .Include(es => es.EthnicStereoTypes)
                    .Include(l => l.Languages)
                    .Include(a => a.Accents)
                    .Include(u => u.Unions);
            }
            if (withTalent)
            {
                personas
                    .Include(t => t.Profile.Talent);
            }

            return personas;
        }
        private IQueryable<Persona> GetCastingFolderRolePersonasQueryable(bool withTalent, bool withAssets,
            bool withAttributes, int roleId)
        {
            var personas = from personaReferences in _castingContext.FolderRoleSelectionPersonaReferences
                           where personaReferences.CastingSessionFolderRoleId == roleId
                           select personaReferences.Persona;
            if (withAssets)
            {
                personas
                    .Include(i => i.ImageReferences)
                    .Include(r => r.ResumeReferences)
                    .Include(a => a.AudioReferences)
                    .Include(v => v.VideoReferences);
            }

            if (withAttributes)
            {
                personas
                    .Include(e => e.Ethnicities)
                    .Include(s => s.StereoTypes)
                    .Include(es => es.EthnicStereoTypes)
                    .Include(l => l.Languages)
                    .Include(a => a.Accents)
                    .Include(u => u.Unions);
            }
            if (withTalent)
            {
                personas
                   .Include(t => t.Profile.Talent);
            }

            return personas;
        }

        private IQueryable<Persona> GetCastingRolePersonasQueryable(bool withTalent, bool withAssets,
            bool withAttributes, int roleId)
        {
            var personas = from personaReferences in _castingContext.RoleSelectionPersonaReferences
                           where personaReferences.CastingSessionRoleId == roleId
                           select personaReferences.Persona;
            if (withAssets)
            {
                personas
                    .Include(i => i.ImageReferences)
                    .Include(r => r.ResumeReferences)
                    .Include(a => a.AudioReferences)
                    .Include(v => v.VideoReferences);
            }

            if (withAttributes)
            {
                personas
                    .Include(e => e.Ethnicities)
                    .Include(s => s.StereoTypes)
                    .Include(es => es.EthnicStereoTypes)
                    .Include(l => l.Languages)
                    .Include(a => a.Accents)
                    .Include(u => u.Unions);
            }
            if (withTalent)
            {
                personas
                    .Include(t => t.Profile.Talent);
            }

            return personas;
        }

        private IQueryable<Persona> GetPersonasQueryable(bool withTalent)
        {
            var personas = from persona in _castingContext.Personas
                select persona;
            if (withTalent)
            {
                personas = from persona in personas
                        .Include(t => t.Profile.Talent)
                    select persona;
            }
            return personas;
        }
        private IQueryable<Persona> GetPersonasQueryable(bool withTalent, bool withAssets,
            bool withAttributes)
        {
            var personas = from persona in _castingContext.Personas
                select persona;
            if (withAssets)
            {
                personas
                    .Include(i => i.ImageReferences)
                    .Include(r => r.ResumeReferences)
                    .Include(a => a.AudioReferences)
                    .Include(v => v.VideoReferences);
            }

            if (withAttributes)
            {
                personas
                    .Include(e => e.Ethnicities)
                    .Include(s => s.StereoTypes)
                    .Include(es => es.EthnicStereoTypes)
                    .Include(l => l.Languages)
                    .Include(a => a.Accents)
                    .Include(u => u.Unions);
            }
            if (withTalent)
            {
                personas = from persona in personas
                        .Include(t => t.Profile.Talent)
                    select persona;
            }
            return personas;
        }
        #endregion
        #endregion
        #region PersonaReferences

        public async Task<int> DeletePersonaReferenceAsync(int parentId, PersonaReference personaReference)
        {

            var referenceVideoCnt = await GetReferenceCountPerPersonaAsync(personaReference.PersonaId);
            var persona = personaReference.Persona;
            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    await ReOrderPersonaReferenceAsync(parentId, 0, personaReference);

                    _castingContext.PersonaReferences.Remove(personaReference);
                    if (referenceVideoCnt < 2)
                    {
                        _castingContext.Personas.Remove(persona);

                    }
                    result = await _castingContext.SaveChangesAsync();
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                }
            }

            return result;


        }

        public async Task<int> UpdatePersonaReferenceAsync(int parentId,
            PersonaReference personaReference)
        {
            SetEntityState(personaReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<int> ReOrderPersonaReferenceAsync(int parentId,
            int desiredOrderIndex, PersonaReference personaReference)
        {
            IEnumerable<PersonaReference> personaReferences = new List<PersonaReference>();
            switch (personaReference.PersonaReferenceType)
            {
                case PersonaReferenceTypeEnum.CastingCallRoleSubmission:
                    personaReferences = await (from refs in GetPersonaReferencesQueryableForParent(false, personaReference.PersonaReferenceType, parentId)
                            .OfType<CastingCallRoleSubmissionPersonaReference>().Where(ar => ar.CastingCallRoleId == parentId).OrderBy(o => o.OrderIndex)
                        select (PersonaReference)refs).ToListAsync();

                    break;

                case PersonaReferenceTypeEnum.CastingCallFolderRoleSubmission:
                    personaReferences = await (from refs in GetPersonaReferencesQueryableForParent(false, personaReference.PersonaReferenceType, parentId)
                            .OfType<CastingCallFolderRoleSubmissionPersonaReference>().Where(ar => ar.CastingCallFolderRoleId == parentId).OrderBy(o => o.OrderIndex)
                        select (PersonaReference)refs).ToListAsync();

                    break;

                case PersonaReferenceTypeEnum.CastingSessionFolderRoleSelection:
                    personaReferences = await (from refs in GetPersonaReferencesQueryableForParent(false, personaReference.PersonaReferenceType, parentId)
                            .OfType<CastingSessionFolderRoleSelectionPersonaReference>().Where(ar => ar.CastingSessionFolderRoleId == parentId).OrderBy(o => o.OrderIndex)
                                               select (PersonaReference)refs).ToListAsync();
                    break;
                case PersonaReferenceTypeEnum.CastingSessionRoleSelection:

                    personaReferences = await (from refs in GetPersonaReferencesQueryableForParent(false, personaReference.PersonaReferenceType, parentId)
                            .OfType<CastingSessionRoleSelectionPersonaReference>().Where(ar => ar.CastingSessionRoleId == parentId).OrderBy(o => o.OrderIndex)
                                               select (PersonaReference)refs).ToListAsync();
                    break;
            }

            if (desiredOrderIndex == 0)
            {
                return await ReOrderPersonaReferenceUpAsync(desiredOrderIndex, personaReference, personaReferences);
            }
            else if (desiredOrderIndex > personaReference.OrderIndex)
            {
                return await ReOrderPersonaReferenceUpAsync(desiredOrderIndex, personaReference, personaReferences);
            }
            else if (desiredOrderIndex < personaReference.OrderIndex)
            {
                return await ReOrderPersonaReferenceDownAsync(desiredOrderIndex, personaReference, personaReferences);
            }
            return await Task.FromResult(0);

        }

        private async Task<int> ReOrderPersonaReferenceDownAsync(int desiredOrderIndex, PersonaReference personaReference,
            IEnumerable<PersonaReference> personaReferences)
        {
            foreach (var personaRef in personaReferences)
            {
                if (personaRef.OrderIndex >= desiredOrderIndex &&
                    personaRef.OrderIndex < personaReference.OrderIndex)
                {
                    personaRef.OrderIndex++;
                    SetEntityState(personaRef, EntityState.Modified);
                }

            }

            personaReference.OrderIndex = desiredOrderIndex;
            SetEntityState(personaReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> ReOrderPersonaReferenceUpAsync(int desiredOrderIndex, PersonaReference personaReference,
            IEnumerable<PersonaReference> personaReferences)
        {
            foreach (var personaRef in personaReferences)
            {
                if (personaRef.OrderIndex > personaReference.OrderIndex &&
                    (personaRef.OrderIndex <= desiredOrderIndex || desiredOrderIndex == 0))
                {
                    personaRef.OrderIndex--;
                    SetEntityState(personaRef, EntityState.Modified);
                }

            }

            personaReference.OrderIndex = desiredOrderIndex;
            SetEntityState(personaReference, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        private async Task<int> GetHighestOrderIndexPersonaReferenceByPersonaReferenceTypeAsync(int parentId,
            PersonaReference personaReference)
        {
            int maxOrderIndex = 0;
            await Task.Run(() =>
            {
                switch (personaReference.PersonaReferenceType)
                {
                    case PersonaReferenceTypeEnum.CastingCallRoleSubmission:
                        maxOrderIndex = (from personaRef in _castingContext.RoleSubmissionPersonaReferences
                                         where personaRef.CastingCallRoleId == parentId
                                         select personaRef.OrderIndex).DefaultIfEmpty(0).Max();

                        break;

                    case PersonaReferenceTypeEnum.CastingSessionFolderRoleSelection:
                        maxOrderIndex = (from personaRef in _castingContext.FolderRoleSelectionPersonaReferences
                                         where personaRef.CastingSessionFolderRoleId == parentId
                                         select personaRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                    case PersonaReferenceTypeEnum.CastingSessionRoleSelection:

                        maxOrderIndex = (from personaRef in _castingContext.RoleSelectionPersonaReferences
                                         where personaRef.CastingSessionRoleId == parentId
                                         select personaRef.OrderIndex).DefaultIfEmpty(0).Max();
                        break;
                }

            });

            return maxOrderIndex;
        }
        public async Task<int> AddNewPersonaReferenceAsync(bool orderAtTheTop, int parentId,
            PersonaReference personaReference)

        {
            int maxOrderIndex = 0;
            maxOrderIndex =
                await GetHighestOrderIndexPersonaReferenceByPersonaReferenceTypeAsync(parentId,
                    personaReference);

            int result = 0;
            using (var dbContextTransaction = _castingContext.Database.BeginTransaction())
            {
                try
                {
                    personaReference.OrderIndex = maxOrderIndex + 1;
                    _castingContext.Set<PersonaReference>().Add(personaReference);
                    result = await _castingContext.SaveChangesAsync();
                    if (orderAtTheTop)
                    {
                        await ReOrderPersonaReferenceAsync(parentId, 1, personaReference);
                    }
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {

                    dbContextTransaction.Rollback();
                    result = 0;
                }
            }

            return result;
        }
        public async Task<int> AddNewPersonaReferenceAsync(bool orderAtTheTop,
            PersonaReference personaReference)

        {

            int parentId = 0;
            switch (personaReference.PersonaReferenceType)
            {
                case PersonaReferenceTypeEnum.CastingCallRoleSubmission:
                    parentId = ((CastingCallRoleSubmissionPersonaReference)personaReference).CastingCallRoleId;
                    break;

                case PersonaReferenceTypeEnum.CastingCallFolderRoleSubmission:
                    parentId = ((CastingCallFolderRoleSubmissionPersonaReference)personaReference).CastingCallFolderRoleId;
                    break;

                case PersonaReferenceTypeEnum.CastingSessionFolderRoleSelection:
                    parentId = ((CastingSessionFolderRoleSelectionPersonaReference)personaReference).CastingSessionFolderRoleId;
                    break;
                case PersonaReferenceTypeEnum.CastingSessionRoleSelection:
                    parentId = ((CastingSessionRoleSelectionPersonaReference)personaReference).CastingSessionRoleId;
                    break;
            }
            return await AddNewPersonaReferenceAsync(orderAtTheTop, parentId, personaReference);

        }
        public async Task<PersonaReference> GetPersonaReferenceByIdAsync(bool withPersona, int referenceId)
        {
            return await GetAllPersonaReferencesQueryable(withPersona).FirstOrDefaultAsync(personaRef => personaRef.Id == referenceId);

        }
        public IQueryable<PersonaReference> GetPersonaReferencesQueryableForParent(bool withPersona, PersonaReferenceTypeEnum personaReferenceType, int parentId)
        {
            var personaReferences = GetAllPersonaReferencesQueryable(withPersona);
            switch (personaReferenceType)
            {
                case PersonaReferenceTypeEnum.CastingCallRoleSubmission:
                    personaReferences = from personaRef in personaReferences.OfType<CastingCallRoleSubmissionPersonaReference>()
                        where personaRef.CastingCallRoleId == parentId
                        select personaRef;
                    break;
                case PersonaReferenceTypeEnum.CastingCallFolderRoleSubmission:
                    personaReferences = from personaRef in personaReferences.OfType<CastingCallFolderRoleSubmissionPersonaReference>()
                        where personaRef.CastingCallFolderRoleId == parentId
                        select personaRef;
                    break;
                case PersonaReferenceTypeEnum.CastingSessionFolderRoleSelection:
                    personaReferences = from personaRef in personaReferences.OfType<CastingSessionFolderRoleSelectionPersonaReference>()
                                        where personaRef.CastingSessionFolderRoleId == parentId
                                        select personaRef;

                    break;
                case PersonaReferenceTypeEnum.CastingSessionRoleSelection:
                    personaReferences = from personaRef in personaReferences.OfType<CastingSessionRoleSelectionPersonaReference>()
                                        where personaRef.CastingSessionRoleId == parentId
                                        select personaRef;

                    break;
            }
            return personaReferences;
        }

        public async Task<int> GetReferenceCountPerPersonaAsync(int personaId)
        {
            int cnt = 0;
            await Task.Run(() =>
            {
                cnt = _castingContext.PersonaReferences.Count(i => i.PersonaId == personaId);


            });
            return cnt;
        }

        #region PrivateMethods
        private IQueryable<PersonaReference> GetAllPersonaReferencesQueryable(bool withPersona)
        {
            return from personaRef in (withPersona ? _castingContext.PersonaReferences.Include(persona => persona.Persona) : _castingContext.PersonaReferences) select personaRef;
        }
        #endregion

#endregion



        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }

               

                disposedValue = true;
            }
        }

     

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

      
        #endregion

    }
}
