﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casting.Model.Entities.Sessions
{
    public class CastingSessionFolder:Logger
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public int CastingSessionId { get; set; }
        public CastingSession CastingSession { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int RolesCount { get; set; }
        public ICollection<CastingSessionFolderRole> Roles { get; set; }
        public OrderByValuesEnum CastingSessionFolderRolesSortOrderI { get; set; }
        public int OrderIndex { get; set; }


    }
}
