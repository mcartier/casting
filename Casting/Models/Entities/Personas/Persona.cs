﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Security.AccessControl;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Profiles;

using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

using Casting.Model.Entities.Attributes;

namespace Casting.Model
{
    public class Persona:Logger
    {
        public Persona()
        {
            Accents = new List<Accent>();
            Ethnicities = new List<Ethnicity>();
            StereoTypes = new List<StereoType>();
            EthnicStereoTypes = new List<EthnicStereoType>();
            Unions = new List<Union>();
            Languages = new List<Language>();

            VideoReferences = new List<PersonaVideoReference>();
            AudioReferences = new List<PersonaAudioReference>();
            ImageReferences = new List<PersonaImageReference>();
            ResumeReferences = new List<PersonaResumeReference>();

        }
        [Key]
        public int Id { get; set; }

        public DateTime DOB { get; set; }
        public PersonaTypeEnum PersonaType { get; set; }
        public string WorkingName { get; set; }
        public int AgeMinimum { get; set; }
        public int AgeMaximum { get; set; }
        public int OwnerId { get; set; }
        public OwnerTypeEnum OwnerType { get; set; }
        public int ProfileId { get; set; }

        private Profile _profile;
        public Profile Profile {
            get { return _profile; }
            set
            {
                _profile = value;
                if (_profile != null)
                {
                    //this.TalentType = _profile.;
                   // this.ProfileType = _profile.ProfileType;
                }

            }
        }
        public ProfileTypeEnum ProfileType { get; set; }

        public TalentTypeEnum TalentType { get; set; }
        [NotMapped]
        public ICollection<Union> Unions { get; set; }
        [NotMapped]
        public ICollection<Language> Languages { get; set; }
        public bool Transgender { get; set; }
        public RoleGenderType Gender { get; set; }
        [NotMapped]
        public ICollection<Accent> Accents { get; set; }
        [NotMapped]
        public ICollection<Ethnicity> Ethnicities { get; set; }
        [NotMapped]
        public ICollection<StereoType> StereoTypes { get; set; }
        [NotMapped]
        public ICollection<EthnicStereoType> EthnicStereoTypes { get; set; }
        public ICollection<PersonaVideoReference> VideoReferences { get; set; }
        public ICollection<PersonaAudioReference> AudioReferences { get; set; }
        public ICollection<PersonaImageReference> ImageReferences { get; set; }

        public ICollection<PersonaResumeReference> ResumeReferences { get; set; }

        public OrderByValuesEnum ResumeReferencesSortOrderI { get; set; }
        public OrderByValuesEnum VideoReferencesSortOrderI { get; set; }
        public OrderByValuesEnum ImageReferencesSortOrderI { get; set; }

        public OrderByValuesEnum AudioReferencesSortOrderI { get; set; }
        public ICollection<PersonaReference> PersonaReferences { get; set; }


    }
}
