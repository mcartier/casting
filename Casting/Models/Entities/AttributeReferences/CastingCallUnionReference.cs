﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Casting.Model.Entities.Personas;


using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Entities.CastingCalls;

namespace Casting.Model.Entities.AttributeReferences
{
    public class CastingCallUnionReference: UnionReference
    {
        public CastingCallUnionReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.CastingCall;
        }
        public int CastingCallId { get; set; }
        public CastingCall CastingCall { get; set; }

    }
}
