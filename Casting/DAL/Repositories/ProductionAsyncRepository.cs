﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories
{
  

    public class ProductionAsyncRepository : AsyncRepository, IProductionAsyncRepository
    {
        ICastingContext _castingContext;
        public ProductionAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }
        #region Productions

        public async Task<Production> GetProductionByIdAsync(bool withRoles, bool withRoleMetadata, bool withCastingCalls, bool withSessions, bool withAuditionData, int productionId)
        {
            IQueryable<Production> productions = GetProductionsQueryable(withRoles, withRoleMetadata, withCastingCalls, withSessions, withAuditionData);

            return await productions.FirstOrDefaultAsync(p => p.Id == productionId);


        }

        public async Task<int> AddNewProductionAsync(Production production)
        {
            _castingContext.Set<Production>().Add(production);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteProductionAsync(Production production)
        {
            _castingContext.Set<Production>().Remove(production);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateProductionAsync(Production production)
        {
            SetEntityState(production, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }
        public async Task<IEnumerable<Production>> GetProductionsByDirectorIdAsync(bool withRoles, bool withRoleMetadata, bool withCastingCalls,
            bool withSessions, bool withAuditionData, int directorId, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending)
        {
            var productions = GetProductionsQueryable(withRoles, withRoleMetadata, withCastingCalls,
                withSessions, withAuditionData);
            switch (orderBy)
            {
                case OrderByValuesEnum.CreatedDateAscending:

                    productions = productions.OrderBy(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.CreatedDateDescending:

                    productions = productions.OrderByDescending(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.NameAscending:

                    productions = productions.OrderBy(o => o.Name);
                    break;
                case OrderByValuesEnum.NameDescending:

                    productions = productions.OrderByDescending(o => o.Name);
                    break;
            }
            return await productions.Where(d => d.DirectorId == directorId).ToListAsync();
        }

        public async Task<IEnumerable<Production>> GetProductionsByDirectorIdAsync(bool withRoles, bool withRoleMetadata, bool withCastingCalls,
            bool withSessions, bool withAuditionData, int directorId, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending)
        {
            var productions = GetProductionsQueryable(withRoles, withRoleMetadata, withCastingCalls,
                withSessions, withAuditionData);
            switch (orderBy)
            {
                case OrderByValuesEnum.CreatedDateAscending:

                    productions = productions.OrderBy(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.CreatedDateDescending:

                    productions = productions.OrderByDescending(o => o.CreatedDate);
                    break;
                case OrderByValuesEnum.NameAscending:

                    productions = productions.OrderBy(o => o.Name);
                    break;
                case OrderByValuesEnum.NameDescending:

                    productions = productions.OrderByDescending(o => o.Name);
                    break;
            }
            return await productions.Where(d => d.DirectorId == directorId).Skip(pageIndex * pageSize).Take(pageSize).ToListAsync();
        }
        
        
        #region  PrivateMethods
        private IQueryable<Production> GetProductionsQueryable(bool withRoles, bool withRoleMetadata, bool withCastingCalls,
            bool withSessions, bool withAuditionData)
        {
            IQueryable<Production> productions = from production in _castingContext.Productions select production;
            productions = productions
                .Include(i => i.Director)
                .Include(i => i.ProductionType)
                .Include("ProductionType.ProductionTypeCategory");
            if (withAuditionData)
            {
                productions = productions
                    .Include(i => i.AuditionDetails)
                    .Include(i => i.PayDetails)
                    .Include(i => i.ShootDetails)
                    .Include(i => i.Sides);
            }

            if (withCastingCalls)
            {
                productions = productions
                    .Include(i => i.CastingCalls);
            }

            if (withSessions)
            {
                productions = productions
                    .Include(u => u.CastingSessions);
            }
            if (withRoles)
            {
                productions = productions
                    .Include(t => t.Roles);
                if (withRoleMetadata)
                {
                    productions = productions
                        .Include("Roles.Accents.Accent")
                        // .Include("Roles.Skills.Skill")
                        //.Include("Roles.BodyTypes.BodyType")
                       // .Include("Roles.EyeColors.EyeColor")
                       // .Include("Roles.HairColors.HairColor")
                        .Include("Roles.Languages.Language")
                        .Include("Roles.Ethnicities.Ethnicity")
                        .Include("Roles.EthnicStereoTypes.EthnicStereoType")
                        .Include("Roles.StereoTypes.StereoType")
                        .Include("Roles.RoleType")
                        .Include("Roles.RoleGenderType");
                }
                
            }
            return productions;
        }
        #endregion
        #endregion

        #region ProductionRoles


        public async Task<Role> GetRoleByIdAsync(int roleId, bool withAttributes)
        {
            IQueryable<Role> roles = GetRolesQueryable(withAttributes);

            return await roles.FirstOrDefaultAsync(p => p.Id == roleId);


        }

        public async Task<int> AddNewRoleAsync(Role role)
        {
            _castingContext.Set<Role>().Add(role);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteRoleAsync(Role role)
        {
            _castingContext.Set<Role>().Remove(role);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateRoleAsync(Role role)
        {
            SetEntityState(role, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Role>> GetRolesByProductionIdAsync(int productionId, bool withAttributes)
        {
            var roles = GetRolesQueryable(withAttributes);

            return await roles.Where(d => d.ProductionId == productionId).ToListAsync();
        }


        #region PrivateMethods

        private IQueryable<Role> GetRolesQueryable(bool withAttributes)
        {
            IQueryable<Role> roles = from role in _castingContext.Roles select role;
            roles = roles
                .Include(a => a.RoleType)
                .Include(a => a.RoleGenderType)
                .Include("Accents.Accent")
                .Include("Languages.Language")
                .Include("Ethnicities.Ethnicity")
                .Include("EthnicStereoTypes.EthnicStereoType")
                .Include("StereoTypes.StereoType")
                .Include("EyeColors.EyeColor")
                .Include("HairColors.HairColor")
               .Include("Skills.Skill")
                .Include(p=>p.PayDetail);


            return roles;
        }
        #endregion
        #endregion

        #region PayDetails


        public async Task<PayDetail> GetPayDetailByIdAsync(int payDetailId, bool withAttributes)
        {
            IQueryable<PayDetail> payDetails = GetPayDetailsQueryable(withAttributes);

            return await payDetails.FirstOrDefaultAsync(p => p.Id == payDetailId);


        }

        public async Task<int> AddNewPayDetailAsync(PayDetail payDetail)
        {
            _castingContext.Set<PayDetail>().Add(payDetail);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeletePayDetailAsync(PayDetail payDetail)
        {
            _castingContext.Set<PayDetail>().Remove(payDetail);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdatePayDetailAsync(PayDetail payDetail)
        {
            SetEntityState(payDetail, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<PayDetail>> GetPayDetailsByProductionIdAsync(int productionId, bool withAttributes)
        {
            var payDetails = GetPayDetailsQueryable(withAttributes);

            return await payDetails.Where(d => d.ProductionId == productionId).ToListAsync();
        }


        #region PrivateMethods

        private IQueryable<PayDetail> GetPayDetailsQueryable(bool withAttributes)
        {
            IQueryable<PayDetail> payDetails = from payDetail in _castingContext.PayDetails select payDetail;


            return payDetails;
        }
        #endregion
        #endregion

        #region Sides


        public async Task<Side> GetSideByIdAsync(int sideId, bool withAttributes)
        {
            IQueryable<Side> sides = GetSidesQueryable(withAttributes);

            return await sides.FirstOrDefaultAsync(p => p.Id == sideId);


        }

        public async Task<int> AddNewSideAsync(Side side)
        {
            _castingContext.Set<Side>().Add(side);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteSideAsync(Side side)
        {
            _castingContext.Set<Side>().Remove(side);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateSideAsync(Side side)
        {
            SetEntityState(side, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<Side>> GetSidesByProductionIdAsync(int productionId, bool withAttributes)
        {
            var sides = GetSidesQueryable(withAttributes);

            return await sides.Where(d => d.ProductionId == productionId).ToListAsync();
        }

        #region PrivateMethods

        private IQueryable<Side> GetSidesQueryable(bool withAttributes)
        {
            IQueryable<Side> sides = from side in _castingContext.Sides select side;


            return sides;
        }
        #endregion
        #endregion


        #region ShootDetails


        public async Task<ShootDetail> GetShootDetailByIdAsync(int shootDetailId, bool withAttributes)
        {
            IQueryable<ShootDetail> shootDetails = GetShootDetailsQueryable(withAttributes);

            return await shootDetails.FirstOrDefaultAsync(p => p.Id == shootDetailId);


        }

        public async Task<int> AddNewShootDetailAsync(ShootDetail shootDetail)
        {
            _castingContext.Set<ShootDetail>().Add(shootDetail);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteShootDetailAsync(ShootDetail shootDetail)
        {
            _castingContext.Set<ShootDetail>().Remove(shootDetail);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateShootDetailAsync(ShootDetail shootDetail)
        {
            SetEntityState(shootDetail, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<ShootDetail>> GetShootDetailsByProductionIdAsync(int productionId, bool withAttributes)
        {
            var shootDetails = GetShootDetailsQueryable(withAttributes);

            return await shootDetails.Where(d => d.ProductionId == productionId).ToListAsync();
        }


        #region PrivateMethods

        private IQueryable<ShootDetail> GetShootDetailsQueryable(bool withAttributes)
        {
            IQueryable<ShootDetail> shootDetails = from shootDetail in _castingContext.ShootDetails select shootDetail;


            return shootDetails;
        }
        #endregion
        #endregion

        #region AuditionDetails


        public async Task<AuditionDetail> GetAuditionDetailByIdAsync(int auditionDetailId, bool withAttributes)
        {
            IQueryable<AuditionDetail> auditionDetails = GetAuditionDetailsQueryable(withAttributes);

            return await auditionDetails.FirstOrDefaultAsync(p => p.Id == auditionDetailId);


        }

        public async Task<int> AddNewAuditionDetailAsync(AuditionDetail auditionDetail)
        {
            _castingContext.Set<AuditionDetail>().Add(auditionDetail);
            return await _castingContext.SaveChangesAsync();

        }

        public async Task<int> DeleteAuditionDetailAsync(AuditionDetail auditionDetail)
        {
            _castingContext.Set<AuditionDetail>().Remove(auditionDetail);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<int> UpdateAuditionDetailAsync(AuditionDetail auditionDetail)
        {
            SetEntityState(auditionDetail, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<AuditionDetail>> GetAuditionDetailsByProductionIdAsync(int productionId, bool withAttributes)
        {
            var auditionDetails = GetAuditionDetailsQueryable(withAttributes);

            return await auditionDetails.Where(d => d.ProductionId == productionId).ToListAsync();
        }


        #region PrivateMethods

        private IQueryable<AuditionDetail> GetAuditionDetailsQueryable(bool withAttributes)
        {
            IQueryable<AuditionDetail> auditionDetails = from auditionDetail in _castingContext.AuditionDetails select auditionDetail;


            return auditionDetails;
        }
        #endregion
        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _castingContext.Dispose();
                }



                disposedValue = true;
            }
        }



        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }


        #endregion
    }
}
