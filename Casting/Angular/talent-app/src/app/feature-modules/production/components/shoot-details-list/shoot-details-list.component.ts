import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { Observable, Subject, Subscription } from 'rxjs';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';

@Component({
  selector: 'app-shoot-details-list',
  templateUrl: './shoot-details-list.component.html',
  styleUrls: ['./shoot-details-list.component.scss']
})
export class ShootDetailsListComponent implements OnInit {

    constructor(
        private productionExtraService: ProductionExtraService,
        private messageDispatchService: MessageDispatchService
      ) { }

    private newShootDetailSubscription: Subscription;
    private deleteShootDetailSubscription: Subscription;
    private editShootDetailSubscription: Subscription;

    @Input() shootDetails: ShootDetail[];

    ngOnInit() {
      this.editShootDetailSubscription = this.productionExtraService.subscribeToEditShootDetailObs()
        .subscribe(
          (detail: ShootDetail) => {
           });

      this.newShootDetailSubscription = this.productionExtraService.subscribeToCreateShootDetailObs()
        .subscribe(
          (detail: ShootDetail) => {
               });

      this.deleteShootDetailSubscription = this.productionExtraService.subscribeToDeleteShootDetailObs()
        .subscribe(
          (detail: ShootDetail) => {
            if (detail) {
          }
          });

  }

  ngOnDestroy() {
    
    if (this.editShootDetailSubscription) {
      this.editShootDetailSubscription.unsubscribe();

    }
    if (this.newShootDetailSubscription) {
      this.newShootDetailSubscription.unsubscribe();

    }
    if (this.deleteShootDetailSubscription) {
      this.deleteShootDetailSubscription.unsubscribe();

    }
  }
}
