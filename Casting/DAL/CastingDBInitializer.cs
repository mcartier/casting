﻿using Casting.Model;


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Entities.Users.Guests;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

using Casting.Model.Entities.Attributes;

namespace Casting.DAL
{
    public class CastingDBInitializer : CreateDatabaseIfNotExists<CastingContext>
    {
        protected override void Seed(CastingContext context)
        {
context.Database.ExecuteSqlCommand("DROP INDEX [IX_BaseUserId] ON [dbo].[ApplicationUser]");
            context.Database.ExecuteSqlCommand("CREATE UNIQUE INDEX [IX_BaseUserId] ON [dbo].[ApplicationUser]([BaseUserId] ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)");
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Average','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Slim','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Athletic / Toned','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Muscular','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Curvy','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Heavyset / Stocky','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[BodyType] VALUES ('Plus-Sized / Full-Figured','',GETDATE(),GETDATE());
");
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('SAG-AFTRA',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('Equity (U.S.)',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('Equity (U.K.)',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('Equity (Canada)',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('Equity (Ireland)',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('Equity (Australia)',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('Equity (New Zealand)',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('ACTRA',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('AFM',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('AGMA',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('AGVA',1,GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Union] ([Name],[IsUnion], [LastModifiedDate], [CreatedDate]) VALUES ('BECTU',1,GETDATE(),GETDATE());
");
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Black','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Brown','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Blond','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Auburn','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Chestnut','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Red','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Gray','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('White','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Bald','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Salt & Pepper','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Strawberry Blond','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[HairColor] VALUES ('Multicolored/Dyed','',GETDATE(),GETDATE());
");
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Amber','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Blue','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Brown','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Gray','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Green','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Hazel','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Red','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[EyeColor] VALUES ('Violet','',GETDATE(),GETDATE());
");
            context.Database.ExecuteSqlCommand(
                @" INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Caucasian','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('African American','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Latino/Hispanic','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Asian',GETDATE(),'',GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('South Asian','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Native American','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Middle Eastern','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Southeast Asian / Pacific Islander','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('Ethnically Ambiguous / Mixed Race','',GETDATE(),GETDATE());
INSERT INTO [Casting].[dbo].[Ethnicity] VALUES ('African Descent','',GETDATE(),GETDATE());
");
           context.Database.ExecuteSqlCommand(
                @"SET IDENTITY_INSERT [dbo].[ProductionTypeCategory] ON 
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (1, N'Theater', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (2, N'Film', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (3, N'TV & Video', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (4, N'Commercials', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (5, N'Modeling', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (6, N'Performing Arts', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (7, N'Voiceover', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (8, N'Entertainment Jobs & Crew', GetDate(), GetDate())
INSERT [dbo].[ProductionTypeCategory] ([Id], [Name], [LastModifiedDate], [CreatedDate]) VALUES (9, N'Events & Representation', GetDate(), GetDate())
SET IDENTITY_INSERT [dbo].[ProductionTypeCategory] OFF
");
            context.Database.ExecuteSqlCommand(
                @"SET IDENTITY_INSERT [dbo].[ProductionType] ON 
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (1, N'Plays', 1, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (2, N'Musicals', 1, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (3, N'Chorus Calls', 1, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (4, N'Feature Films', 2, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (5, N'Short Films', 2, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (6, N'Student Films', 2, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (7, N'Scripted TV & Video', 3, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (8, N'Reality TV & Documentary', 3, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (9, N'Music Videos', 3, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (10, N'Demo & Instructional Videos', 3, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (11, N'Multimedia', 3, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (12, N'National Commercials', 4, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (13, N'Local Commercials', 4, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (14, N'Spec Commercials', 4, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (15, N'Online Commercials & Promos', 4, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (16, N'Print Modeling', 5, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (17, N'Fashion & Runway Modeling', 5, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (18, N'Commercial & Fit Modeling', 5, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (19, N'Promotional & Event Modeling', 5, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (20, N'Dancers & Choreographers', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (21, N'Cabaret & Variety', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (22, N'Singers', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (23, N'Musicians & Composers', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (24, N'Comedians & Improv', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (25, N'Theme Parks', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (26, N'Cruise Lines', 6, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (27, N'Commercial & Film Voiceover', 7, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (28, N'Animation & Videogame Voiceover', 7, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (29, N'Audio Books', 7, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (30, N'Radio & Podcasts', 7, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (31, N'Stage Staff & Tech', 8, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (32, N'Film & TV Crew', 8, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (33, N'Writers', 8, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (34, N'Gigs', 8, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (35, N'Agents & Managers', 9, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (36, N'Workshops', 9, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (37, N'Competitions', 9, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (38, N'Festivals & Events', 9, GetDate(), GetDate())
INSERT [dbo].[ProductionType] ([Id], [Name], [ProductionTypeCategoryId], [LastModifiedDate], [CreatedDate]) VALUES (39, N'Groups & Membership Companies', 9, GetDate(), GetDate())
SET IDENTITY_INSERT [dbo].[ProductionType] OFF
");
            context.Database.ExecuteSqlCommand(
                @"INSERT [dbo].[CastingCallType] VALUES ('Theater', GetDate(), GetDate())

INSERT [dbo].[CastingCallPrivacy] VALUES ('Theater', GetDate(), GetDate())

INSERT [dbo].[CastingCallViewType] VALUES ('Theater', GetDate(), GetDate())
");
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Lead',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Supporting',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Day Player',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Background / Extra',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Chorus / Ensemble',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Staff / Crew',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Unspecified',GetDate(),GetDate());
");
            context.Database.ExecuteSqlCommand(
                @"INSERT INTO [Casting].[dbo].[RoleGenderType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Males & Females',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleGenderType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Male',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleGenderType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('Female',GetDate(),GetDate());
INSERT INTO [Casting].[dbo].[RoleGenderType] ([Name],[LastModifiedDate],[CreatedDate]) VALUES ('N/A',GetDate(),GetDate());
"
            );
            //// var container = new StructureMap.Container();
            UserManager <ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));// container.GetInstance<UserManager<ApplicationUser>>();

           // context.Accents.Add(new Accent() {Name = "Italian"});
            var country = new Country() { Name = "Canada", ShortName = "Ca" };
            var region = new Region() { Name = "British Columbia", ShortName = "BC", Country = country };
            var city = new City() { Name = "Vancouver", ShortName = "Van", Country = country, Region = region };
            context.Countries.Add(country);
            context.Regions.Add(region);
            context.Cities.Add(city);
            var country2 = new Country() { Name = "United States", ShortName = "US" };
            var region2 = new Region() { Name = "California", ShortName = "CA", Country = country2 };
            var city2 = new City() { Name = "Los Angeles", ShortName = "LA", Country = country2, Region = region2 };
            var city3 = new City() { Name = "Hollywood", ShortName = "Hol", Country = country2, Region = region2 };
            var city4 = new City() { Name = "San Francisco", ShortName = "SF", Country = country2, Region = region2 };
            context.Countries.Add(country2);
            context.Regions.Add(region2);
            context.Cities.Add(city2);
            context.Cities.Add(city3);
            context.Cities.Add(city4);
            var agency = new Agency() {Name = "Best Talents"};
            var agencyoffice = new AgencyOffice() {City = city, Agency = agency};
            context.Agencies.Add(agency);
            context.AgencyOffices.Add(agencyoffice);
        var gue = new Guest() {FirstNames = "kjklj"};
           //var uref = new UserReference() {ApplicationUser = user2, BaseCastingUser = gue};
           //context.UserReferences.Add(uref);
           context.BaseUsers.Add(gue);
           var director = new Director() {FirstNames = "lkj", LastNames = "lkjkj"};
           context.Directors.Add(director);
            // var talent2 = new HouseTalent {FirstNames = "Mike", LastNames = "McDonald", Profile = new ActorProfile() {}  };
           // var talent3 = new StudioTalent { FirstNames = "Marc", LastNames = "Rouin", Profile = new ActorProfile() { }, Director = director };
           // var talent4 = new HouseTalent { FirstNames = "Tim", LastNames = "Lepp", Profile = new ActorProfile() { } };
           // var talent5 = new StudioTalent { FirstNames = "Steen", LastNames = "Jensen", Profile = new ActorProfile() { }, Director = director };
          //  var talent6 = new HouseTalent { FirstNames = "Alex", LastNames = "Wood", Profile = new ActorProfile() { }  };
           

            var agent1 = new Agent { FirstNames = "Mike", LastNames = "McDonald", AgencyOffice = agencyoffice};
            var agent2 = new Agent { FirstNames = "Marc", LastNames = "Rouin", AgencyOffice = agencyoffice };
            var agent3 = new Agent { FirstNames = "Tim", LastNames = "Lepp", AgencyOffice = agencyoffice };
            var agent4 = new Agent { FirstNames = "Steen", LastNames = "Jensen", AgencyOffice = agencyoffice };
            var agent5 = new Agent { FirstNames = "Alex", LastNames = "Wood", AgencyOffice = agencyoffice };

            var talent21 = new HouseTalent { FirstNames = "Therese", LastNames = "Gibson"};
            var talent22 = new HouseTalent { FirstNames = "Marina", LastNames = "Julien" };
            var talent23 = new HouseTalent { FirstNames = "Ray", LastNames = "OBrian" };
            var talent24 = new HouseTalent { FirstNames = "Brent", LastNames = "Dumont" };
            var talent25 = new HouseTalent { FirstNames = "Keegan", LastNames = "Fortin" };

            //load some talents
        
            
            var agent11 = new Agent { FirstNames = "Therese", LastNames = "Gibson", AgencyOffice = agencyoffice};
            var agent12 = new Agent { FirstNames = "Marina", LastNames = "Julien", AgencyOffice = agencyoffice };
            var agent13 = new Agent { FirstNames = "Ray", LastNames = "OBrian", AgencyOffice = agencyoffice };
            var agent14 = new Agent { FirstNames = "Brent", LastNames = "Dumont", AgencyOffice = agencyoffice };
            var agent15 = new Agent { FirstNames = "Keegan", LastNames = "Fortin", AgencyOffice = agencyoffice };

            var profile = new ActorProfile() { };
            profile.Talent = talent21;
           // context.VoiceActorProfiles.Add(profile3);
          // context.VoiceActorProfiles.Add(profile4);
          //  context.Profiles.Add(profile);
          //  context.Profiles.Add(profile2);
            var persona = new Persona()
            {
                WorkingName = "lkjw",
                DOB = DateTime.Now,
                Profile = profile
            };

             context.Personas.Add(persona);
            //context.Personas.Add(persona2);
            // context.Personas.Add(persona3);
            //  context.Personas.Add(persona4);
            // context.HouseTalentPersonaReferences.Add(personaRef);
            var basket = new Basket {DOB = DateTime.Now};
            context.Baskets.Add(basket);
            var resume = new Resume {OwnerId = 9};
            context.Resumes.Add(new Resume { OwnerId = 9 });
            var resref = new ProfileResumeReference {Resume = resume, Profile = profile, Name = "dkjlkjwe"};
            context.ResumeReferences.Add(resref);
            var resreff = new BasketResumeReference {Resume = resume, Basket = basket, Name = "lkjjlkj"};
            context.ResumeReferences.Add(resreff);
            var hedshot = new Image {OwnerId = 9};
            context.Images.Add(hedshot);
            var hedref = new ActorProfileImageReference { Image = hedshot, Profile = (ActorProfile)profile, Name = "lkjweki" };
            context.ImageReferences.Add(hedref);
            var hedreff = new BasketImageReference { Image = hedshot, Basket = basket, Name = "lkjweki" };
            context.ImageReferences.Add(hedreff);

            //context.Accents.Add(new Accent() { Name = "Italian"});
           // context.Accents.Add(new Accent() { Name = "French"});
           // context.Unions.Add(new Union() { Name = "BCPA"});
            //context.Unions.Add(new Union() { Name = "ACTRA"});
            context.EthnicStereoTypes.Add(new EthnicStereoType() { Name = "Short Italian" });
            //context.Ethnicities.Add(new Ethnicity() { Name = "Iranian" });
            context.StereoTypes.Add(new StereoType() { Name = "Lazy" });
            context.StereoTypes.Add(new StereoType() { Name = "Big eater" });

            context.Languages.Add(new Language() { Name = "English", Name2 = "Anglais"});
            context.Languages.Add(new Language() { Name = "French", Name2 = "Francais"});
            //context.HairColors.Add(new HairColor() { Name = "Blond" });
            //context.EyeColors.Add(new EyeColor() { Name = "Blue" });
            //context.BodyTypes.Add(new BodyType() { Name = "Muscular" });
           


            context.Agents.Add(agent1);
            context.Agents.Add(agent2);
            context.Agents.Add(agent3);
            context.Agents.Add(agent4);
            context.Agents.Add(agent5);
            agent11.CityReferences.Add(new AgentCityReference() { City = city });
            agent12.CityReferences.Add(new AgentCityReference() { City = city });
            agent13.CityReferences.Add(new AgentCityReference() { City = city });
            agent14.CityReferences.Add(new AgentCityReference() { City = city });

            context.Agents.Add(agent11);
            context.Agents.Add(agent12);
            context.Agents.Add(agent13);
            context.Agents.Add(agent14);
            context.Agents.Add(agent15);
          
            talent21.AgentReferences.Add(new AgentTalentReference() {Agent = agent11});
            talent21.AgentReferences.Add(new AgentTalentReference() { Agent = agent14 });
            talent21.AgentReferences.Add(new AgentTalentReference() { Agent = agent13 });
            talent21.MainAgent = agent12;
           
           // context.Talents.Add(talent2);
          //  context.Talents.Add(talent3);
            //context.Personas.Add(talent4);
           // context.Talents.Add(talent5);
          //  context.Talents.Add(talent6);

            context.Talents.Add(talent22);
            context.Talents.Add(talent23);
            context.Talents.Add(talent24);
            context.Talents.Add(talent25);
            context.Talents.Add(talent21);
          //      var user = new ApplicationUser() { UserName = "joebloe", Email = "cartier@hotmail.com", BaseCastingUser = talent6 };

          //  var user2 = new ApplicationUser() { UserName = "joebloe2", Email = "cartie2r@hotmail.com", BaseCastingUser = talent3 };

          //  IdentityResult result = userManager.Create(user);
          //  IdentityResult result2 = userManager.Create(user2);
         //   var user3 = new ApplicationUser() { UserName = "joeblfoe", Email = "cartier@hotmfrail.com", BaseCastingUser = talent4 };

          //  var user23 = new ApplicationUser() { UserName = "jodfebloe2", Email = "cartie2r@hogftmail.com", BaseCastingUser = talent5 };
          //  IdentityResult result3 = userManager.Create(user3);
         //   IdentityResult result23 = userManager.Create(user23);
            context.SaveChanges();
          
           // context.SaveChanges();
            //var profileRef = new HouseTalentprofileReference {profile = profile, Talent = talent2};



        }
    }
}
