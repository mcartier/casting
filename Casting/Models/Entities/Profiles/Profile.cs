﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;

namespace Casting.Model.Entities.Profiles
{
   public abstract class Profile:Logger
    {
        public Profile()
        {
            Personas = new List<Persona>();
            Accents = new List<ProfileAccentReference>();
            Unions = new List<ProfileUnionReference>();
            Languages = new List<ProfileLanguageReference>();

            AudioReferences = new List<ProfileAudioReference>();
            ResumeReferences = new List<ProfileResumeReference>();
           

        }
        [Key]
        public int Id { get; set; }
        //public int? TalentId { get; set; }
        public Agent Agent { get; set; }
        public int? AgentId { get; set; }
        public int? TalentId { get; set; }
        private Talent _talent;
        public virtual Talent Talent
        {
            get { return _talent; }
            set
            {
                _talent = value;
                if (_talent != null)
                {
                    this.TalentType = _talent.TalentType;
                   // _talent.ProfileType = this.ProfileType;
                }

            }
        }

        public int? DirectorId { get; set; }
        public Director Director { get; set; }
        /*
      private TalentProfileReference _talentReference;
        public virtual TalentProfileReference TalentReference
        {
            get { return _talentReference; }
            set
            {
                _talentReference = value;
                if (_talentReference != null)
                {
                   // _profileReference.TalentType = this.TalentType;
                }

            }
        }

        */

        // [ForeignKey("Talent")]
        //  [Key, ForeignKey("Talent")]
        // [ForeignKey("Talent")]
        //  public int? TalentId { get; set; }
        // [Key] public int TalentId { get; set; }
        public TalentTypeEnum TalentType { get; set; }
       /*   public TalentTypeEnum TalentType { get; set; }
        private Talent _talent;
       public Talent Talent
        {
            get { return _talent; }
            set
            {
                _talent = value;
                if (_talent != null)
                {
                    this.TalentType = _talent.TalentType;
                    _talent.ProfileType = this.ProfileType;
                }
                
            }
        }

        */
       // public ProfileMainImageReference MainImageReference { get; set; }
        public string WorkingName { get; set; }
        public int AgeMinimum { get; set; }
        public int AgeMaximum { get; set; }
        public bool Transgender { get; set; }
        public RoleGenderType Gender { get; set; }

        public bool HasPassport { get; set; }
        public City Location { get; set; }
        public ICollection<TalentHighlight> Highlights { get; set; }
        public ICollection<TalentWorkCredit> WorkCredits { get; set; }
        public ICollection<Training> Trainings { get; set; }
        public ICollection<ProfileUnionReference> Unions { get; set; }
        public ICollection<ProfileAccentReference> Accents { get; set; }
        public ICollection<ProfileLanguageReference> Languages { get; set; }

         public ICollection<ProfileAudioReference> AudioReferences { get; set; }
        public ICollection<ProfileResumeReference> ResumeReferences { get; set; }
      
        // public ICollection<ActorProfileVideoReference> VideoReferences { get; set; }
        // public ICollection<ActorProfileImageReference> ImageReferences { get; set; }

        public ICollection<Persona> Personas { get; set; }
    }
   
}
