import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPulloutNonScrollComponent } from './main-pullout-non-scroll.component';

describe('MainPulloutNonScrollComponent', () => {
  let component: MainPulloutNonScrollComponent;
  let fixture: ComponentFixture<MainPulloutNonScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPulloutNonScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPulloutNonScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
