import { Component, OnDestroy, AfterViewInit, OnInit, ViewChild, Input, Inject  } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { StudioTalentBlob } from '../../../../models/blobs';
import { BytesPipePipe } from '../../../../pipes/bytes-pipe.pipe';
import { FileUploadService } from '../../../../services/file-upload.service';
import { VideoBlobService } from '../../../../services/video-blob.service';
import { NewRecordingComponent } from '../new-recording/new-recording.component'
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-recording-item',
  templateUrl: './recording-item.component.html',
  styleUrls: ['./recording-item.component.scss']
})
export class RecordingItemComponent implements OnInit, OnDestroy {

    constructor(private videoBlobService: VideoBlobService, private fileUploadService: FileUploadService, private dialog: MatDialog) { }
  //private blobUploadSubscription: Subscription;
  @Input()
    private blobUploadState: StudioTalentBlob;
    private blobUploadSubscription:Subscription;
    

    openDialog(): void {
      const dialogRef = this.dialog.open(NewRecordingComponent,
        {
          width: '90%',
          maxWidth: '600px',
            data: this.blobUploadState

        });
      /* dialogRef.afterClosed().subscribe(result => {
         console.log('The dialog was closed');
         //this.animal = result;
       });*/
    }


    ngOnInit() {
        this.blobUploadSubscription = this.blobUploadState.obs.asObservable()
            .subscribe(blobUploadUpdate => {
                if (this.blobUploadState.isComplete && this.blobUploadState.isSuccess) {
                  this.blobUploadSubscription.unsubscribe();
                  this.videoBlobService.deleteBlob(this.blobUploadState.id);

                } else {
                  this.blobUploadState = blobUploadUpdate;
                }
                
            });

  }

    ngOnDestroy() {
      this.blobUploadSubscription.unsubscribe();
     
    }

    public upload(): void {
      let blobsCol: StudioTalentBlob[] = [this.blobUploadState];
      this.fileUploadService.uploadFiles(blobsCol);

    }

    public edit(): void {
    this.openDialog();

    }

    public remove(): void {
        if (this.blobUploadState.upload) {
          this.blobUploadState.upload.abort();
        }
    this.videoBlobService.deleteBlob(this.blobUploadState.id);
  }

}
