﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum CompensationTypeEnum
    {
        NotPaid = 1,
        SomePaid = 3,
        Paid = 7,

    }
}
