﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
public class CastingCallRoleSubmissionPersonaReference : PersonaReference
    {
        public CastingCallRoleSubmissionPersonaReference()
        {
            base.PersonaReferenceType = PersonaReferenceTypeEnum.CastingCallRoleSubmission;

        }
        public int CastingCallRoleId { get; set; }
        public CastingCallRole CastingCallRole { get; set; }
    }
}
