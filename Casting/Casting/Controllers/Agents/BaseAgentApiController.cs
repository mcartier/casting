﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Casting.DAL;

namespace Casting.Web.Controllers.Agents
{
    public class BaseAgentApiController:BaseApiController
    {
        public BaseAgentApiController(ICastingContext castingContext) : base(castingContext)
        {

        }
    }
}