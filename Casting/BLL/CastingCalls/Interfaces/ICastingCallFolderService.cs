﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

namespace Casting.BLL.CastingCalls.Interfaces
{
    public interface ICastingCallFolderService
    {
        Task<CastingCallFolder> GetCastingCallFolderByIdWithRolesAsync(int castingSessionFolderId);
        Task<CastingCallFolder> GetCastingCallFolderByIdWithRolesAndPersonasAsync(int castingSessionFolderId);
        Task<CastingCallFolder> GetCastingCallFolderByIdWithRolesPersonasAndAssetsAsync(int castingSessionFolderId);
        Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdWithRolesAsync(int castingSessionId);
        Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdWithRolesAndPersonasAsync(int castingSessionId);
        Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdWithRolesPersonasAndAssetsAsync(int castingSessionId);
        Task<int> AddNewCastingCallFolderAsync(bool orderAtTheTop, CastingCallFolder castingSessionFolder);
        Task<int> UpdateCastingCallFolderAsync(CastingCallFolder castingSessionFolder);
        Task<int> DeleteCastingCallFolder(CastingCallFolder castingSessionFolder);
    }
}