﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casting.Model.Entities.CastingCalls
{
    public class CastingCallFolder:Logger
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public int CastingCallId { get; set; }
        public CastingCall CastingCall { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int RolesCount { get; set; }
        public ICollection<CastingCallFolderRole> Roles { get; set; }
        public OrderByValuesEnum CastingCallFolderRolesSortOrderI { get; set; }
        public int OrderIndex { get; set; }


    }
}
