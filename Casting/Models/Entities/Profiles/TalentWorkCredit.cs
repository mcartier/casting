﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Metadata;

namespace Casting.Model.Entities.Profiles
{
    public class TalentWorkCredit:Logger
    {
        public int  Id { get; set; }
        public int ProfileId { get; set; }
        public Profile Profile { get; set; }
        public int OrderIndex { get; set; }
        public ProductionType ProductionType { get; set; }
        public string ProjectName { get; set; }
        public string Role { get; set; }
        public string DirectorProductionCompany { get; set; }
        public string Locatione { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }


    }
}
