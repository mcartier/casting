import { Injectable } from '@angular/core';
import { ProductionRoleTransferObject, Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../models/production.models';
//import { MatDialog, MatDialogRef } from '@angular/material';
import { NewRoleComponent } from '../components/new-role/new-role.component'
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RoleDialogService {

    constructor() { }
    private roleDialogRef:any;
    private _editRoleObs: Subject<Role> = new Subject<Role>();
    public readonly editRoleObs: Observable<Role> = this._editRoleObs.asObservable();

    private _newRoleObs: Subject<Role> = new Subject<Role>();
    public readonly newRoleObs: Observable<Role> = this._newRoleObs.asObservable();

    public subscribeToCreateRoleObs(): Observable<Role> {
        return this.newRoleObs;
    }
    public subscribeToEditRoleObs(): Observable<Role> {
        return this.editRoleObs;
    }

    public createRole(production: Production): Observable<Role> {
        let transferObject: ProductionRoleTransferObject = new ProductionRoleTransferObject;
        transferObject.production = production;
     /* const dialogRef = this.dialog.open(NewRoleComponent, {
        //width: '90%',
          data: transferObject,
        autoFocus: false,
        backdropClass: 'logindialog-overlay',
        panelClass: 'logindialog-panel'
      });

      dialogRef.afterClosed().subscribe(result => {
          let role: Role = result;
          this._newRoleObs.next(role);
        // this.animal = result;
      });*/
      return this.newRoleObs;
    }

    public editRole(production: Production, role: Role): Observable<Role> {
      let transferObject: ProductionRoleTransferObject = new ProductionRoleTransferObject;
      transferObject.production = production;
      transferObject.role = role;
     /* const dialogRef = this.dialog.open(NewRoleComponent, {
        //width: '90%',
        data: transferObject,
        autoFocus: false,
        backdropClass: 'logindialog-overlay',
        panelClass: 'logindialog-panel'
      });

        dialogRef.afterClosed().subscribe((result: Role) => {
       
       // let role: Role = result;
            this._editRoleObs.next(result);
        // this.animal = result;
      });*/
      return this.editRoleObs;
    }
    public deleteRole(production: Production, role: Role): Observable<Role> {
      let transferObject: ProductionRoleTransferObject = new ProductionRoleTransferObject;
      transferObject.production = production;
      transferObject.role = role;
    /*  const dialogRef = this.dialog.open(NewRoleComponent, {
        //width: '90%',
        data: transferObject,
        autoFocus: false,
        backdropClass: 'logindialog-overlay',
        panelClass: 'logindialog-panel'
      });

      dialogRef.afterClosed().subscribe(result => {
        let role: Role = result;
        this._editRoleObs.next(role);
        // this.animal = result;
      });*/
      return this.editRoleObs;
    }
}
