﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.BLL.Profiles.Interfaces;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Profiles;
using Casting.Model.Enums;

namespace Casting.BLL.Profiles
{
    public class ProfileService:BaseService, IProfileService
    {
        private IProfileAsyncRepository _profileRepository;

        public ProfileService(IProfileAsyncRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }


        public async Task<Profile> GetProfileByIdAsync(int profileId)
        {
            var profile = await _profileRepository.GetProfileByIdAsync(profileId, false);
            return profile;

        }

        public async Task<Profile> GetActorProfileByIdAsync(int profileId)
        {
            var profile = await _profileRepository.GetProfileByIdAsync(profileId, false, false, false, ProfileTypeEnum.Actor);
            return profile;

        }

        public async Task<Profile> GetVoiceActorProfileByIdAsync(int profileId)
        {
            var profile = await _profileRepository.GetProfileByIdAsync(profileId, false, false, false, ProfileTypeEnum.VoiceActor);
            return profile;

        }


        public async Task<Profile> GetCompleteActorProfileByIdAsync(int profileId)
        {
            var profile = await _profileRepository.GetProfileByIdAsync(profileId, true, true, true, ProfileTypeEnum.Actor);
            return profile;

        }

        public async Task<Profile> GetCompleteVoiceActorProfileByIdAsync(int profileId)
        {
            var profile = await _profileRepository.GetProfileByIdAsync(profileId, true, true, true, ProfileTypeEnum.VoiceActor);
            return profile;

        }

        public async Task<Profile> GetProfileByTalentIdAsync(int talentId)
        {
            var profile = await _profileRepository.GetProfileByTalentIdAsync(talentId, false);
            return profile;

        }

        public async Task<Profile> GetCompleteActorProfileByTalentIdAsync(int talentId)
        {
            var profile = await _profileRepository.GetProfileByTalentIdAsync(talentId, true, true, true, ProfileTypeEnum.Actor);
            return profile;

        }

        public async Task<Profile> GetCompleteVoiceActorProfileByTalentIdAsync(int talentId)
        {
            var profile = await _profileRepository.GetProfileByTalentIdAsync(talentId, true, true, true, ProfileTypeEnum.VoiceActor);
            return profile;

        }
        public async Task<IEnumerable<Profile>> GetCompleteActorProfilesByAgentIdAsync(int agentId)
        {
            var profiles = await _profileRepository.GetProfilesByAgentIdAsync(agentId, true, true, true, ProfileTypeEnum.Actor);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetCompleteVoiceActorProfilesByAgentIdAsync(int agentId)
        {
            var profiles = await _profileRepository.GetProfilesByAgentIdAsync(agentId, true, true, true, ProfileTypeEnum.VoiceActor);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetProfilesByAgentIdAsync(int agentId)
        {
            var profiles = await _profileRepository.GetAllProfilesByAgentIdAsync(agentId, true);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetCompleteActorProfilesByMainAgentIdAsync(int agentId)
        {
            var profiles = await _profileRepository.GetProfilesByMainAgentIdAsync(agentId, true, true, true, ProfileTypeEnum.Actor);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetCompleteVoiceActorProfilesByMainAgentIdAsync(int agentId)
        {
            var profiles = await _profileRepository.GetProfilesByMainAgentIdAsync(agentId, true, true, true, ProfileTypeEnum.VoiceActor);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetProfileByMainAgentIdAsync(int agentId)
        {
            var profiles = await _profileRepository.GetAllProfilesByMainAgentIdAsync(agentId, true);
            return profiles;

        }


        public async Task<IEnumerable<Profile>> GetCompleteActorProfilesByDirectorId(int directorId)
        {
            var profiles = await _profileRepository.GetProfilesByDirectorIdAsync(directorId, true, true, true, ProfileTypeEnum.Actor);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetCompleteVoiceActorProfilesByDirectorId(int directorId)
        {
            var profiles = await _profileRepository.GetProfilesByDirectorIdAsync(directorId, true, true, true, ProfileTypeEnum.VoiceActor);
            return profiles;

        }
        public async Task<IEnumerable<Profile>> GetProfilesByDirectorId(int directorId)
        {
            var profiles = await _profileRepository.GetAllProfilesByDirectorIdAsync(directorId, true);
            return profiles;

        }


        public async Task<int> AddNewProfileAsync(Profile profile)
        {
            return await _profileRepository.AddNewProfileAsync(profile);
        }

        public async Task<int> UpdateProfileAsync(Profile profile)
        {
            return await _profileRepository.UpdateProfileAsync(profile);
        }
        public async Task<int> DeleteProfile(Profile profile)
        {
            return await _profileRepository.DeleteProfileAsync(profile);
        }

    }
}
