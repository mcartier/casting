﻿using System.Net.Http;
using System.Threading;
using System.Web;
using tusdotnet.Models;
using Microsoft.Owin;
//using System.Web;

namespace tusdotnet.Adapters
{
    /// <summary>
    /// Context adapter that handles different pipeline contexts.
    /// </summary>
    internal sealed class ContextAdapter
    {
        public RequestAdapter Request { get; set; }

        public ResponseAdapter Response { get; set; }

        public DefaultTusConfiguration Configuration { get; set; }

        public CancellationToken CancellationToken { get; set; }

        public HttpContext HttpContext { get; set; }



        public IOwinContext OwinContext { get; set; }


    }
}