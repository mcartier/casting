﻿using Casting.Model;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Talents;

namespace Casting.DAL.Tests
{
    public class TestTalentDbSet : TestDbSet<Talent>
    {
        public override Talent Find(params object[] keyValues)
        {
            var id = (int)keyValues.Single();
            return this.SingleOrDefault(b => b.Id == id);
        }
        public override Task<Talent> FindAsync(params object[] keyValues)
        {
            var id = (int)keyValues.Single();
            return Task.FromResult<Talent>(this.SingleOrDefault(b => b.Id == id));
        }
    }
}