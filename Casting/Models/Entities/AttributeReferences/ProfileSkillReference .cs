﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Productions;

using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.AttributeReferences
{
    public class ProfileSkillReference : SkillReference
    {
        public ProfileSkillReference ()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.Profile;
        }
        public int ProfileId { get; set; }

    }
}
