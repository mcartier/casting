import { Component, OnInit, Inject, OnDestroy, AfterViewInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { BlobUploadState, StudioTalentBlob, uploadDestinationEnum } from '../../../../models/blobs';
import { MatDialogRef, MatRadioChange, MatSelectChange } from '@angular/material';
import { VideoBlobService } from '../../../../services/video-blob.service';
import { FileUploadService } from '../../../../services/file-upload.service';
import { WINDOW } from '../../../../services/browser.window.provider';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import Basket = uploadDestinationEnum.Basket;
import Talent = uploadDestinationEnum.Talent;
import Basket1 = uploadDestinationEnum.Basket;

//import { WINDOW } from "../../../../services/browser.window";
@Component({
    selector: 'app-new-recording',
    templateUrl: './new-recording.component.html',
    styleUrls: ['./new-recording.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewRecordingComponent implements OnInit, OnDestroy, AfterViewInit {

    constructor(private _formBuilder: FormBuilder, public dialogRef: MatDialogRef<NewRecordingComponent>,
        @Inject(MAT_DIALOG_DATA) public data: StudioTalentBlob, private fileUploadService: FileUploadService, private videoBlobService: VideoBlobService,
        @Inject(WINDOW) public window: Window) { }
    private video: HTMLVideoElement;
    @ViewChild('playbackVideoElement', {static: false}) videoElement;
    private selectedRole: any = {};//{ name: "", id: 0, talents: []};
    private selectedTalent: any;
    private clipDestinationInfo: FormGroup;
    //private uploadDestination: string = 'talent';
    private uploadDestination: uploadDestinationEnum = uploadDestinationEnum.Talent;
            
    private roles: any[] = [
        {
            name: "role 1", id: 1, talents: [
                { name: "John Doe", id: 1 },
                { name: "Bob Villa Dow", id: 2 },
                { name: "Alex Wood", id: 3 }]
        }
        , {
            name: "role 2", id: 2, talents: [
                { name: "Marc De Ste Croix", id: 4 },
                { name: "Jogn McGraw", id: 5 },
                { name: "Dana Johnston", id: 6 }]
        }
    ];
    ngOnInit() {
      this.video = this.videoElement.nativeElement;
      this.video.muted = false;
      this.video.controls = true;
      this.video.src = this.window.URL.createObjectURL(this.data.blob);
      this.video.play();
      this.clipDestinationInfo = this._formBuilder.group({
        destination: [''],
        role: ['', Validators.required],
        talent: ['', Validators.required],
      }, { updateOn: 'blur' });
      this.selectedRole = this.roles.find(r => r.id == this.data.roleId);
      this.selectedTalent = this.selectedRole.talents.find(t => t.id == this.data.talentId);


    }

    ngOnDestroy() {
      this.video.src = null;
      this.video.srcObject = null;
      this.data = null;
    }
    ngAfterViewInit() {
      // set the initial state of the video

      // this.video.play();

    }
    upload(): void {
        this.videoBlobService.addNewCreatedBlob(this.data);
        let blobsCol: StudioTalentBlob[] = [this.data];
        this.fileUploadService.uploadFiles(blobsCol);

        this.dialogRef.close();
    }
    saveMe() {
        this.videoBlobService.addNewCreatedBlob(this.data);
        this.dialogRef.close();
    }
    discardClick() {
        this.videoBlobService.deleteBlob(this.data.id);
        this.dialogRef.close();
    }
    roleChange($event: MatSelectChange) {

        if ($event.value && $event.value.id != 0) {
            this.data.roleId = $event.value.id;
            this.data.buildMetadata();
        }
    }
    talentChange($event: MatSelectChange) {
        if ($event.value && $event.value.id != 0) {
            this.data.talentId = $event.value.id;
            this.data.buildMetadata();
        }
    }
    uploadDestinationChange($event: MatRadioChange) {
      debugger;
      if ($event.value) {
            if ($event.value == uploadDestinationEnum.Talent) {
              this.data.uploadDestination = uploadDestinationEnum.Talent;
              this.data.uploadUrl = 'https://cast.com/castingtalentvideoupload';
          }
            else if ($event.value == uploadDestinationEnum.Basket) {
              this.data.uploadDestination = uploadDestinationEnum.Basket;
              this.data.uploadUrl = 'https://cast.com/castingbasketvideoupload';
              //this.data.talentId = 0;
              //this.data.roleId = 0
             
          }
           this.data.buildMetadata();
        }
        
    }

   
}
