﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Locations;



namespace Casting.DAL.Configurations
{
    class CountryConfiguration : EntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
            HasKey(x => x.Id);
            HasMany(r => r.Regions)
                .WithRequired()
                .HasForeignKey(k => k.CountryId);
            HasMany(c => c.Cities)
                .WithRequired(c => c.Country)
                .HasForeignKey(k => k.CountryId);
            HasMany(c => c.CastingCalls)
                .WithRequired(c => c.Country)
                .HasForeignKey(k => k.CountryId);


        }
    }

    class RegionConfiguration : EntityTypeConfiguration<Region>
    {
        public RegionConfiguration()
        {
             HasKey(x => x.Id);
             HasRequired(c => c.Country)
                 .WithMany(r => r.Regions)
                 .HasForeignKey(k => k.CountryId)
                 .WillCascadeOnDelete(false);
             HasMany(c => c.Cities)
                 .WithRequired(c => c.Region)
                 .HasForeignKey(k => k.RegionId)
                 .WillCascadeOnDelete(false);
             HasMany(c => c.CastingCalls)
                 .WithRequired(c => c.Region)
                 .HasForeignKey(k => k.RegionId);

        }
    }
    class CityConfiguration : EntityTypeConfiguration<City>
    {
        public CityConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(c => c.Country)
                .WithMany(r => r.Cities)
                .HasForeignKey(k => k.CountryId)
                .WillCascadeOnDelete(false);
            HasRequired(c => c.Region)
                .WithMany(r => r.Cities)
                .HasForeignKey(k => k.RegionId)
                .WillCascadeOnDelete(false);
            HasMany(s => s.CoveringAgentReferences)
                .WithRequired(a => a.City)
                .HasForeignKey(k => k.CityId);

            HasMany(c => c.CastingCalls)
                .WithRequired(c => c.City)
                .HasForeignKey(k => k.CityId);

        }
    }
}
;