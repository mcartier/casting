﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;

using Casting.Model.Entities.Baskets;

namespace Casting.DAL.Configurations
{
    class BasketConfiguration : EntityTypeConfiguration<Basket>
    {
        public BasketConfiguration()
        {
            HasKey(x => x.Id);
            HasMany(h => h.ImageReferences)
                .WithRequired(h => h.Basket)
                .HasForeignKey(p => p.BasketId);
            HasMany(a => a.ResumeReferences)
                .WithRequired(p => p.Basket)
                .HasForeignKey(p => p.BasketId);
            HasMany(a => a.AudioReferences)
                .WithRequired(p => p.Basket)
                .HasForeignKey(p => p.BasketId);
            HasMany(a => a.VideoReferences)
                .WithRequired(p => p.Basket)
                .HasForeignKey(p => p.BasketId);
            // HasOptional(t => t.TalentReference)
            //     .WithRequired(p => p.Basket);
        }
    }
}
;