import { TestBed } from '@angular/core/testing';

import { ConfirmDecisionSubscriptionService } from './confirm-decision-subscription.service';

describe('ConfirmDecisionSubscriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfirmDecisionSubscriptionService = TestBed.get(ConfirmDecisionSubscriptionService);
    expect(service).toBeTruthy();
  });
});
