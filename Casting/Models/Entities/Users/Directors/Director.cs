﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;
using Casting.Model.Entities.Profiles;

namespace Casting.Model.Entities.Users.Directors
{
   public class Director : BaseCastingUser
    {
        public Director()
        {
            base.ApplicationUserType = ApplicationUserTypeEnum.Director;
            Productions = new List<Production>();
            CastingCalls = new List<CastingCall>();
           // StudioTalents = new List<StudioTalent>();
            Profiles = new List<Profile>();

        }
        public ICollection<Production> Productions { get; set; }

        public ICollection<CastingCall> CastingCalls { get; set; }
        public OrderByValuesEnum ProductionsSortOrderI { get; set; }
        public OrderByValuesEnum CastingCallsSortOrderI { get; set; }

      //  public ICollection<StudioTalent> StudioTalents { get; set; }
        public IList<Profile> Profiles { get; set; }


    }
}
