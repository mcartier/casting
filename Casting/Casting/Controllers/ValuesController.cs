﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;

namespace Casting.Web.Controllers
{
      public class ValuesController : ApiController
    {

        private ICastingContext _castingContext;
        private IProfileAsyncRepository _rep;
        private ITalentAssetAsyncRepository _trep;
        private readonly UserManager<ApplicationUser> _userManager;

        public ValuesController(UserManager<ApplicationUser> userManager, ICastingContext castingContext, IProfileAsyncRepository rep, ITalentAssetAsyncRepository trep)
        {
            _castingContext = castingContext;// castingContext;
            _userManager = userManager;
            _rep = rep;
            _trep = trep;


        }  // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            var ttt = _userManager.Users.Any(u => u.UserName == "lkj");
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
