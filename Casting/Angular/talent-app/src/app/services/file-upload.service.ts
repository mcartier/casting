import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { BlobUploadState, CreatedBlob } from '../models/blobs';
import { UUID } from 'angular2-uuid';
import { WINDOW } from './browser.window.provider';
//import * as tus from 'tus-js-client';
import { TusHelper, ITusUploadObject } from '../utilities/tus.helper';
import * as tus from 'tus-js-client';
@Injectable({
    providedIn: 'root'
})
export class FileUploadService {

    constructor(@Inject(WINDOW) private window: Window) {

    }

    public uploadFiles(uploadObjects: ITusUploadObject[]
    ): any {
        var headers: any;
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.access_token) {
                headers =
                    {
                        Authorization: `Bearer ${currentUser.access_token.replace(" ", "")}`
                    }

            }
        }
        uploadObjects.forEach(blobUploadState => {
            blobUploadState.buildMetadata();
            let upload = new tus.Upload(blobUploadState.file, {
                uploadUrl: window.location.protocol + blobUploadState.uploadUrl,
                endpoint: blobUploadState.uploadUrl,
                retryDelays: [0, 1000, 3000, 5000],
                resume: true,
                // chunkSize:1000000,
                metadata: blobUploadState.metadata,
                headers: headers,
                onError: error => {
                    blobUploadState.errorCallback(blobUploadState.file, error);
                    console.log('Failed: ' + blobUploadState.file.name + error);
                },
                onProgress: (bytesUploaded, bytesTotal) => {
                    const percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2);
                    blobUploadState.progressCallback(blobUploadState.file, bytesUploaded, bytesTotal, percentage);

                    
                },
                onSuccess: () => {
                    if (upload.url.endsWith('/')) {
                        upload.url = upload.url.slice(0, -1);
                    }
                    var fileId = upload.url.split('/').pop();
                    blobUploadState.uploadedFileId = fileId;
                    console.log('Download ' + blobUploadState.uploadedFileId + ' from ' + upload.url + ' ' + blobUploadState.file.name);
                    blobUploadState.successCallback(blobUploadState.file);
                 
                },
            });
            blobUploadState.upload = upload;
            blobUploadState.sartUpload();
        });

        return true;
    }
}
