﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Casting.Model.Entities.Metadata;

namespace Casting.Web.Models.Talent
{
    public class NewProfileMetadataBindingModels
    {
        [Required]
        public int Value { get; set; }
        
        [Required]
        public int ProfileId { get; set; }

    }
    public class ProfileMetadataBindingModels
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int ProfileId { get; set; }

    }
    public class TalentBindingModels
    {
        [Required]
        public string FirstNames { get; set; }
        [Required]
        public string LastNames { get; set; }

    }
    public class TalentAgentReferenceBindingModels
    {
        [Required]
        public int TalentId { get; set; }
        [Required]
        public int AgentId { get; set; }

    }

    public class NewActorProfileBindingModels : ActorProfileBindingModels
    {
        [Required]
        public int TalentId { get; set; }
    }

    public class ActorProfileBindingModels
    {
        [Required]
        public string WorkingName { get; set; }
        [Required]
        public int AgeMinimum { get; set; }
        [Required]
        public int AgeMaximum { get; set; }
        public bool Transgender { get; set; }
        [Required]
        public int Gender { get; set; }
        public int HairColor { get; set; }


        public int EyeColor { get; set; }

        public int BodyType { get; set; }
        public int Height { get; set; }

        public int Weight { get; set; }
        public string Tagline { get; set; }
        public bool HasPassport { get; set; }
        public bool HasDriversLicense { get; set; }
        public int Location { get; set; }


    }
    public class NewVoiceActorProfileBindingModels : VoiceActorProfileBindingModels
    {
        [Required]
        public int TalentId { get; set; }
    }


    public class VoiceActorProfileBindingModels
    {
        [Required]
        public string WorkingName { get; set; }
        [Required]
        public int AgeMinimum { get; set; }
        [Required]
        public int AgeMaximum { get; set; }
        public bool Transgender { get; set; }
        [Required]
        public int Gender { get; set; }
    
        public string Tagline { get; set; }
        public bool HasPassport { get; set; }
        public bool HasDriversLicense { get; set; }
        public int Location { get; set; }


    }

}