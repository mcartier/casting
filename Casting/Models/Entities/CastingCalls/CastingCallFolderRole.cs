﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casting.Model.Entities.CastingCalls
{
    public class CastingCallFolderRole:Logger
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
          public int CastingCallFolderId { get; set; }
        public CastingCallFolder CastingCallFolder { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int PersonasCount { get; set; }
        public ICollection<CastingCallFolderRoleSubmissionPersonaReference> Personas { get; set; }
        public int CastingCallRoleId { get; set; }
        public CastingCallRole CastingCallRole { get; set; }
        public OrderByValuesEnum SelectedPersonasSortOrderI { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }

        public int OrderIndex { get; set; }
    }
}
