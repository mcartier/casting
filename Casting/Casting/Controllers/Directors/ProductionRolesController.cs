﻿
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using System.Data.Entity.Infrastructure;
using Casting.BLL.AttributeReferences.Interfaces;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.AttributeReferences;
using Casting.Web.Models.Production;
using Casting.DAL;
using Casting.Web.Models;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/productions/{productionId}/roles")]
    public class ProductionRolesController : BaseDirectorApiController
    {
        private IProductionRoleService _productionRoleService;
        private IAttributeReferenceService _attributeService;
        private IMetadataService _metadataService;
        private IPayDetailService _payDetailService;

        private readonly UserManager<ApplicationUser> _userManager;
        //  private ICastingContext _castingContext;
        public ProductionRolesController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, IProductionRoleService productionRoleService, ICastingContext castingContext,
            IAttributeReferenceService attributeService, IPayDetailService payDetailService
            ) : base(castingContext)
        {
            // _castingContext = castingContext;
            _productionRoleService = productionRoleService;
            _attributeService = attributeService;
            _metadataService = metadataService;
            _payDetailService = payDetailService;
            _userManager = userManager;
        }



        // GET: api/ProductionRoles
        [ResponseType(typeof(IEnumerable<Role>))]
        [HttpGet]
        public async Task<IHttpActionResult> GetProductionRoles(int productionId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
            var productionRoles =
                await _productionRoleService.GetRolesByProductionIdAsync(productionId);

            return Ok(productionRoles);
        }

        // GET: api/ProductionRoles/5
        [ResponseType(typeof(Role))]
        [Route("{Id}")]
        public async Task<IHttpActionResult> Get(int productionId, int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var productionRole =
                await _productionRoleService.GetCompleteRoleByIdAsync(Id);

            return Ok(productionRole);
        }

        // POST: api/ProductionRoles
        [ResponseType(typeof(Role))]
        [HttpPost]
        [Route("")]
        public async Task<IHttpActionResult> Post(int productionId, NewRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            var role = new Role()
            {
                ProductionId = value.ProductionId,
                Description = value.Description,
                Name = value.Name,
                MaxAge = value.MaxAge,
                MinAge = value.MinAge,
                RequiresNudity = value.RequiresNudity,
                Transgender = value.Transgender,
                RoleTypeId = value.RoleTypeId,
                RoleGenderTypeId = value.RoleGenderTypeId,
                //  EthnicityId = value.EthnicityId
            };
            if (value.PayDetailId != 0)
            {

                var payDetail = await _payDetailService.GetPayDetailByIdAsync(value.PayDetailId);
                if (payDetail != null)
                {
                    role.PayDetailId = value.PayDetailId;
                    role.PayDetail = payDetail;

                }


            }

            foreach (var stereotypeId in value.StereoTypes)
            {
                var stereotype = await _metadataService.GetStereoTypeByIdAsync(stereotypeId);
                if (stereotype != null)
                {
                    role.StereoTypes.Add(new RoleStereoTypeReference() { StereoType = stereotype });
                }
            }
            foreach (var etnicitystereotypeId in value.EthnicStereoTypes)
            {
                var ethnicitystereotype = await _metadataService.GetEthnicStereoTypeByIdAsync(etnicitystereotypeId);
                if (ethnicitystereotype != null)
                {
                    role.EthnicStereoTypes.Add(new RoleEthnicStereoTypeReference() { EthnicStereoType = ethnicitystereotype });
                }
            }

            foreach (var etnicityId in value.Ethnicities)
            {
                var ethnicity = await _metadataService.GetEthnicityByIdAsync(etnicityId);
                if (ethnicity != null)
                {
                    role.Ethnicities.Add(new RoleEthnicityReference() { Ethnicity = ethnicity });
                }
            }
            foreach (var accentId in value.Accents)
            {
                var accent = await _metadataService.GetAccentByIdAsync(accentId);
                if (accent != null)
                {
                    role.Accents.Add(new RoleAccentReference() { Accent = accent });
                }
            }

            foreach (var languageId in value.Languages)
            {
                var language = await _metadataService.GetLanguageByIdAsync(languageId);
                if (language != null)
                {
                    role.Languages.Add(new RoleLanguageReference() { Language = language });
                }
            }
            foreach (var bodyTypeId in value.BodyTypes)
            {
                var bodyType = await _metadataService.GetBodyTypeByIdAsync(bodyTypeId);
                if (bodyType != null)
                {
                    role.BodyTypes.Add(new RoleBodyTypeReference() { BodyType = bodyType });
                }
            }
            foreach (var eyeColorId in value.EyeColors)
            {
                var eyeColor = await _metadataService.GetEyeColorByIdAsync(eyeColorId);
                if (eyeColor != null)
                {
                    role.EyeColors.Add(new RoleEyeColorReference() { EyeColor = eyeColor });
                }
            }
            foreach (var hairColorId in value.HairColors)
            {
                var hairColor = await _metadataService.GetHairColorByIdAsync(hairColorId);
                if (hairColor != null)
                {
                    role.HairColors.Add(new RoleHairColorReference() { HairColor = hairColor });
                }
            }

            foreach (var skillId in value.Skills)
            {
                var skill = await _metadataService.GetSkillByIdAsync(skillId);
                if (skill != null)
                {
                    role.Skills.Add(new RoleSkillReference() { Skill = skill });
                }
            }

            var ret = await _productionRoleService.AddNewRoleAsync(role);
            var newRole = await _productionRoleService.GetCompleteRoleByIdAsync(role.Id);
            return Created("api/productions/" + productionId + "/roles/" + role.Id, newRole);
        }

        // PUT: api/ProductionRoles/5
        [ResponseType(typeof(Role))]
        [Route("{Id}")]
        [HttpPut]
        public async Task<IHttpActionResult> Put(int productionId, int id, UpdateRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Direc tor)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            var productionRole = await _productionRoleService.GetCompleteRoleByIdAsync(id);
            if (productionRole == null)
            {
                return NotFound();
            }

            productionRole.Description = value.Description;
            productionRole.Name = value.Name;
            productionRole.MaxAge = value.MaxAge;
            productionRole.MinAge = value.MinAge;
            productionRole.RequiresNudity = value.RequiresNudity;
            productionRole.Transgender = value.Transgender;
            if (productionRole.RoleTypeId != value.RoleTypeId)
            {
                var roleType = await _metadataService.GetRoleTypeByIdAsync(value.RoleTypeId);
                if (roleType != null)
                {
                    productionRole.RoleTypeId = value.RoleTypeId;
                    productionRole.RoleType = roleType;

                }
            }
            if (productionRole.RoleGenderTypeId != value.RoleGenderTypeId)
            {
                var roleGenderType = await _metadataService.GetRoleGenderTypeByIdAsync(value.RoleGenderTypeId);
                if (roleGenderType != null)
                {
                    productionRole.RoleGenderTypeId = value.RoleGenderTypeId;
                    productionRole.RoleGenderType = roleGenderType;

                }
            }
            if (productionRole.PayDetailId != value.PayDetailId)
            {
                if (value.PayDetailId != 0)
                {

                    var payDetail = await _payDetailService.GetPayDetailByIdAsync(value.PayDetailId);
                    if (payDetail != null)
                    {
                        productionRole.PayDetailId = value.PayDetailId;
                        productionRole.PayDetail = payDetail;

                    }
                }
                else
                {
                    productionRole.PayDetailId = null;
                    productionRole.PayDetail = null;

                }
            }

            foreach (var etnicity in productionRole.Ethnicities.OfType<RoleEthnicityReference>().ToList())
            {
                if (!value.Ethnicities.Any(e => e == etnicity.Id))
                {

                    _castingContext.SetState(etnicity, EntityState.Deleted);
                    productionRole.Ethnicities.Remove(etnicity);
                }
            }

            foreach (var etnicityStereotype in productionRole.EthnicStereoTypes.OfType<RoleEthnicStereoTypeReference>().ToList())
            {
                if (!value.EthnicStereoTypes.Any(e => e == etnicityStereotype.Id))
                {
                    _castingContext.SetState(etnicityStereotype, EntityState.Deleted);
                    productionRole.EthnicStereoTypes.Remove(etnicityStereotype);
                }
            }
            foreach (var stereoType in productionRole.StereoTypes.OfType<RoleStereoTypeReference>().ToList())
            {
                if (!value.StereoTypes.Any(e => e == stereoType.Id))
                {
                    _castingContext.SetState(stereoType, EntityState.Deleted);
                    productionRole.StereoTypes.Remove(stereoType);
                }
            }
            foreach (var accent in productionRole.Accents.OfType<RoleAccentReference>().ToList())
            {
                if (!value.Accents.Any(e => e == accent.Id))
                {
                    _castingContext.SetState(accent, EntityState.Deleted);
                    productionRole.Accents.Remove(accent);
                }
            }
            foreach (var language in productionRole.Languages.OfType<RoleLanguageReference>().ToList())
            {
                if (!value.Languages.Any(e => e == language.Id))
                {
                    _castingContext.SetState(language, EntityState.Deleted);
                    productionRole.Languages.Remove(language);
                }
            }
            foreach (var hairColor in productionRole.HairColors.OfType<RoleHairColorReference>().ToList())
            {
                if (!value.HairColors.Any(e => e == hairColor.Id))
                {
                    _castingContext.SetState(hairColor, EntityState.Deleted);
                    productionRole.HairColors.Remove(hairColor);
                }
            }
            foreach (var eyeColor in productionRole.EyeColors.OfType<RoleEyeColorReference>().ToList())
            {
                if (!value.EyeColors.Any(e => e == eyeColor.Id))
                {
                    _castingContext.SetState(eyeColor, EntityState.Deleted);
                    productionRole.EyeColors.Remove(eyeColor);
                }
            }
            foreach (var bodyType in productionRole.BodyTypes.OfType<RoleBodyTypeReference>().ToList())
            {
                if (!value.BodyTypes.Any(e => e == bodyType.Id))
                {
                    _castingContext.SetState(bodyType, EntityState.Deleted);
                    productionRole.BodyTypes.Remove(bodyType);
                }
            }
            foreach (var skill in productionRole.Skills.OfType<RoleSkillReference>().ToList())
            {
                if (!value.Skills.Any(e => e == skill.Id))
                {
                    _castingContext.SetState(skill, EntityState.Deleted);
                    productionRole.Skills.Remove(skill);
                }
            }

            foreach (var etnicityId in value.Ethnicities)
            {
                if (!productionRole.EthnicStereoTypes.Any(s => s.Id == etnicityId))
                {
                    var ethnicity = await _metadataService.GetEthnicityByIdAsync(etnicityId);
                    if (ethnicity != null)
                    {
                        productionRole.Ethnicities.Add(new RoleEthnicityReference() { Ethnicity = ethnicity });
                    }
                }
            }
            foreach (var etnicitystereotypeId in value.EthnicStereoTypes)
            {
                if (!productionRole.EthnicStereoTypes.Any(s => s.Id == etnicitystereotypeId))
                {
                    var ethnicitystereotype = await _metadataService.GetEthnicStereoTypeByIdAsync(etnicitystereotypeId);
                    if (ethnicitystereotype != null)
                    {
                        productionRole.EthnicStereoTypes.Add(new RoleEthnicStereoTypeReference() { EthnicStereoType = ethnicitystereotype });
                    }
                }
            }
            foreach (var stereoTypeId in value.StereoTypes)
            {
                if (!productionRole.StereoTypes.Any(s => s.Id == stereoTypeId))
                {
                    var stereotype = await _metadataService.GetStereoTypeByIdAsync(stereoTypeId);
                    if (stereotype != null)
                    {
                        productionRole.StereoTypes.Add(new RoleStereoTypeReference() { StereoType = stereotype });
                    }
                }
            }

            foreach (var accentId in value.Accents)
            {
                if (!productionRole.Accents.Any(s => s.Id == accentId))
                {
                    var accent = await _metadataService.GetAccentByIdAsync(accentId);
                    if (accent != null)
                    {
                        productionRole.Accents.Add(new RoleAccentReference() { Accent = accent });
                    }
                }
            }
            foreach (var languageId in value.Languages)
            {
                if (!productionRole.Languages.Any(s => s.Id == languageId))
                {
                    var language = await _metadataService.GetLanguageByIdAsync(languageId);
                    if (language != null)
                    {
                        productionRole.Languages.Add(new RoleLanguageReference() { Language = language });
                    }
                }
            }
            foreach (var bodyTypeId in value.BodyTypes)
            {
                if (!productionRole.BodyTypes.Any(s => s.Id == bodyTypeId))
                {
                    var bodyType = await _metadataService.GetBodyTypeByIdAsync(bodyTypeId);
                    if (bodyType != null)
                    {
                        productionRole.BodyTypes.Add(new RoleBodyTypeReference() { BodyType = bodyType });
                    }
                }
            }
            foreach (var eyeColorId in value.EyeColors)
            {
                if (!productionRole.EyeColors.Any(s => s.Id == eyeColorId))
                {
                    var eyeColor = await _metadataService.GetEyeColorByIdAsync(eyeColorId);
                    if (eyeColor != null)
                    {
                        productionRole.EyeColors.Add(new RoleEyeColorReference() { EyeColor = eyeColor });
                    }
                }
            }
            foreach (var hairColorId in value.HairColors)
            {
                if (!productionRole.HairColors.Any(s => s.Id == hairColorId))
                {
                    var hairColor = await _metadataService.GetHairColorByIdAsync(hairColorId);
                    if (hairColor != null)
                    {
                        productionRole.HairColors.Add(new RoleHairColorReference() { HairColor = hairColor });
                    }
                }
            }
            foreach (var skillId in value.Skills)
            {
                if (!productionRole.Skills.Any(s => s.Id == skillId))
                {
                    var skill = await _metadataService.GetSkillByIdAsync(skillId);
                    if (skill != null)
                    {
                        productionRole.Skills.Add(new RoleSkillReference() { Skill = skill });
                    }
                }
            }

            try
            {
                await _productionRoleService.UpdateRoleAsync(productionRole);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return Ok(productionRole);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/ProductionRoles/5
        [ResponseType(typeof(Role))]
        [Route("{Id}")]
        [HttpDelete]
        public async Task<IHttpActionResult> Delete(int productionId, int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var productionRole = await _productionRoleService.GetRoleByIdAsync(id);
            if (productionRole == null)
            {
                return NotFound();
            }

            await _productionRoleService.DeleteRoleAsync(productionRole);

            return Ok(productionRole);
        }

        #region role attribute references

        #region ethnicities
        // GET: api/productionroles/[id}/ethnicities
        [Route("{id}/ethnicities")]
        [ResponseType(typeof(IEnumerable<RoleEthnicityReference>))]
        public async Task<IHttpActionResult> GetProductionRoleEthnicities(int productionId, int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
            var productionRoleEthnicitiess =
                await _attributeService.GetRoleEthnicityReferencesForParentAsync(Id, true);

            return Ok(productionRoleEthnicitiess);
        }
        // PUT: api/productionroles/{id}/ethnicities/{ethnicityReferenceId}
        [Route("{id}/ethnicities/{ethnicityReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleEthnicity(int productionId, int id, int ethnicityReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            var ethnicity = await _attributeService.GetRoleEthnicityReferenceByIdAsync(ethnicityReferenceId);
            if (ethnicity == null || ethnicity.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to ethnicity object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleEthnicityAttributeReferenceAsync(ethnicity);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/productionroles/[id}/ethnicitys
        [Route("{id}/ethnicities")]
        [ResponseType(typeof(RoleEthnicityReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleEthnicity(int productionId, int Id, RoleAttributeReferenceBindingModel ethnicity)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleEthnicityReference() { RoleId = Id, EthnicityId = ethnicity.AttributeId };
            var ret =
                await _attributeService.AddNewRoleEthnicityAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }

        // DELETE: api/productionroles/[id}/ethnicitys/{ethnicityId}
        [Route("{id}/ethnicities/{ethnicityReferenceId}")]
        [ResponseType(typeof(RoleEthnicityReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleEthnicity(int productionId, int Id, int ethnicityReferenceId)
        {

            var ethnicity = await _attributeService.GetRoleEthnicityReferenceByIdAsync(ethnicityReferenceId);
            if (ethnicity == null || ethnicity.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleEthnicityAttributeReferenceAsync(ethnicity);

            return Ok(ethnicity);

        }


        #endregion

        #region ethnicStereoTypes
        // GET: api/productionroles/[id}/ethnicStereoTypes
        [Route("{id}/ethnicethnicstereotypes")]
        [ResponseType(typeof(IEnumerable<RoleEthnicStereoTypeReference>))]
        public async Task<IHttpActionResult> GetProductionRoleEthnicStereoTypes(int productionId, int Id)
        {

            var productionRoleEthnicStereoTypess =
                await _attributeService.GetRoleEthnicStereoTypeReferencesForParentAsync(Id, true);

            return Ok(productionRoleEthnicStereoTypess);
        }
        // POST: api/productionroles/[id}/ethnicEthnicStereoTypes
        [Route("{id}/ethnicethnicstereotypes")]
        [ResponseType(typeof(RoleEthnicStereoTypeReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleEthnicStereoType(int productionId, int Id, RoleAttributeReferenceBindingModel ethnicEthnicStereoType)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleEthnicStereoTypeReference() { RoleId = Id, EthnicStereoTypeId = ethnicEthnicStereoType.AttributeId };
            var ret =
                await _attributeService.AddNewRoleEthnicStereoTypeAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/productionroles/{id}/ethnicstereotypes/{ethnicStereoTypeReferenceId}
        [Route("{id}/ethnicstereotypes/{ethnicStereoTypeReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleEthnicStereoType(int productionId, int id, int ethnicStereoTypeReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var ethnicStereoType = await _attributeService.GetRoleEthnicStereoTypeReferenceByIdAsync(ethnicStereoTypeReferenceId);
            if (ethnicStereoType == null || ethnicStereoType.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to ethnicStereoType object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleEthnicStereoTypeAttributeReferenceAsync(ethnicStereoType);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        // DELETE: api/productionroles/[id}/ethnicStereoTypes/{ethnicStereoTypeId}
        [Route("{id}/ethnicstereotypes/{ethnicStereoTypeReferenceId}")]
        [ResponseType(typeof(RoleEthnicStereoTypeReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleEthnicStereoType(int productionId, int Id, int ethnicStereoTypeReferenceId)
        {

            var ethnicStereoType = await _attributeService.GetRoleEthnicStereoTypeReferenceByIdAsync(ethnicStereoTypeReferenceId);
            if (ethnicStereoType == null || ethnicStereoType.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleEthnicStereoTypeAttributeReferenceAsync(ethnicStereoType);

            return Ok(ethnicStereoType);

        }


        #endregion

        #region stereoTypes
        // GET: api/productionroles/[id}/stereoTypes
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(IEnumerable<RoleStereoTypeReference>))]
        public async Task<IHttpActionResult> GetProductionRoleStereoTypes(int productionId, int Id)
        {

            var productionRoleStereoTypess =
                await _attributeService.GetRoleStereoTypeReferencesForParentAsync(Id, true);

            return Ok(productionRoleStereoTypess);
        }

        // POST: api/productionroles/[id}/stereoTypes
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(RoleStereoTypeReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleStereoType(int productionId, int Id, RoleAttributeReferenceBindingModel stereoType)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleStereoTypeReference() { RoleId = Id, StereoTypeId = stereoType.AttributeId };
            var ret =
                await _attributeService.AddNewRoleStereoTypeAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/productionroles/{id}/stereotypes/{stereoTypeReferenceId}
        [Route("{id}/stereotypes/{stereoTypeReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleStereoType(int productionId, int id, int stereoTypeReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var stereoType = await _attributeService.GetRoleStereoTypeReferenceByIdAsync(stereoTypeReferenceId);
            if (stereoType == null || stereoType.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to stereoType object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleStereoTypeAttributeReferenceAsync(stereoType);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        // DELETE: api/productionroles/[id}/stereoTypes/{stereoTypeId}
        [Route("{id}/stereotypes/{stereoTypeReferenceId}")]
        [ResponseType(typeof(RoleStereoTypeReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleStereoType(int productionId, int Id, int stereoTypeReferenceId)
        {

            var stereoType = await _attributeService.GetRoleStereoTypeReferenceByIdAsync(stereoTypeReferenceId);
            if (stereoType == null || stereoType.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleStereoTypeAttributeReferenceAsync(stereoType);

            return Ok(stereoType);

        }

        #endregion

        #region languages
        // GET: api/productionroles/[id}/languages
        [Route("{id}/languages")]
        [ResponseType(typeof(IEnumerable<RoleLanguageReference>))]
        public async Task<IHttpActionResult> GetProductionRoleLanguages(int productionId, int Id)
        {

            var productionRoleLanguagess =
                await _attributeService.GetRoleLanguageReferencesForParentAsync(Id, true);

            return Ok(productionRoleLanguagess);
        }
        // POST: api/productionroles/[id}/languages
        [Route("{id}/languages")]
        [ResponseType(typeof(RoleLanguageReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleLanguage(int productionId, int Id, RoleAttributeReferenceBindingModel language)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleLanguageReference() { RoleId = Id, LanguageId = language.AttributeId };
            var ret =
                await _attributeService.AddNewRoleLanguageAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/productionroles/{id}/languages/{languageReferenceId}
        [Route("{id}/languages/{languageReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleLanguage(int productionId, int id, int languageReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var language = await _attributeService.GetRoleLanguageReferenceByIdAsync(languageReferenceId);
            if (language == null || language.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to language object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleLanguageAttributeReferenceAsync(language);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/productionroles/[id}/languages/{languageId}
        [Route("{id}/languages/{languageReferenceId}")]
        [ResponseType(typeof(RoleLanguageReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleLanguage(int productionId, int Id, int languageReferenceId)
        {

            var language = await _attributeService.GetRoleLanguageReferenceByIdAsync(languageReferenceId);
            if (language == null || language.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleLanguageAttributeReferenceAsync(language);

            return Ok(language);

        }

        #endregion

        #region accents
        // GET: api/productionroles/[id}/accents
        [Route("{id}/accents")]
        [ResponseType(typeof(IEnumerable<RoleAccentReference>))]
        public async Task<IHttpActionResult> GetProductionRoleAccents(int productionId, int Id)
        {

            var productionRoleAccentss =
                await _attributeService.GetRoleAccentReferencesForParentAsync(Id, true);

            return Ok(productionRoleAccentss);
        }
        // POST: api/productionroles/[id}/accents
        [Route("{id}/accents")]
        [ResponseType(typeof(RoleAccentReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleAccent(int productionId, int Id, RoleAttributeReferenceBindingModel accent)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleAccentReference() { RoleId = Id, AccentId = accent.AttributeId };
            var ret =
                await _attributeService.AddNewRoleAccentAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/productionroles/{id}/accents/{accentReferenceId}
        [Route("{id}/accents/{accentReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleAccent(int productionId, int id, int accentReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var accent = await _attributeService.GetRoleAccentReferenceByIdAsync(accentReferenceId);
            if (accent == null || accent.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to accent object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleAccentAttributeReferenceAsync(accent);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        // DELETE: api/productionroles/[id}/accents/{accentId}
        [Route("{id}/accents/{accentReferenceId}")]
        [ResponseType(typeof(RoleAccentReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleAccent(int productionId, int Id, int accentReferenceId)
        {

            var accent = await _attributeService.GetRoleAccentReferenceByIdAsync(accentReferenceId);
            if (accent == null || accent.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleAccentAttributeReferenceAsync(accent);

            return Ok(accent);

        }


        #endregion

        #region bodyTypes
        // GET: api/productionroles/[id}/bodytypes
        [Route("{id}/bodytypes")]
        [ResponseType(typeof(IEnumerable<RoleBodyTypeReference>))]
        public async Task<IHttpActionResult> GetProductionRoleBodyTypes(int productionId, int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
            var productionRoleBodyTypess =
                await _attributeService.GetRoleBodyTypeReferencesForParentAsync(Id, true);

            return Ok(productionRoleBodyTypess);
        }
        // PUT: api/productionroles/{id}/bodytypes/{bodyTypeReferenceId}
        [Route("{id}/bodytypes/{bodyTypeReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleBodyType(int productionId, int id, int bodyTypeReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var bodyType = await _attributeService.GetRoleBodyTypeReferenceByIdAsync(bodyTypeReferenceId);
            if (bodyType == null || bodyType.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to bodyType object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleBodyTypeAttributeReferenceAsync(bodyType);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/productionroles/[id}/bodyTypes
        [Route("{id}/bodytypes")]
        [ResponseType(typeof(RoleBodyTypeReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleBodyType(int productionId, int Id, RoleAttributeReferenceBindingModel bodyType)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleBodyTypeReference() { RoleId = Id, BodyTypeId = bodyType.AttributeId };
            var ret =
                await _attributeService.AddNewRoleBodyTypeAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }

        // DELETE: api/productionroles/[id}/bodyTypes/{bodyTypeId}
        [Route("{id}/bodytypes/{bodyTypeReferenceId}")]
        [ResponseType(typeof(RoleBodyTypeReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleBodyType(int productionId, int Id, int bodyTypeReferenceId)
        {

            var bodyType = await _attributeService.GetRoleBodyTypeReferenceByIdAsync(bodyTypeReferenceId);
            if (bodyType == null || bodyType.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleBodyTypeAttributeReferenceAsync(bodyType);

            return Ok(bodyType);

        }


        #endregion



        #region hairColors
        // GET: api/productionroles/[id}/haircolors
        [Route("{id}/haircolors")]
        [ResponseType(typeof(IEnumerable<RoleHairColorReference>))]
        public async Task<IHttpActionResult> GetProductionRoleHairColors(int productionId, int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
            var productionRoleHairColorss =
                await _attributeService.GetRoleHairColorReferencesForParentAsync(Id, true);

            return Ok(productionRoleHairColorss);
        }
        // PUT: api/productionroles/{id}/haircolors/{hairColorReferenceId}
        [Route("{id}/haircolors/{hairColorReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleHairColor(int productionId, int id, int hairColorReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var hairColor = await _attributeService.GetRoleHairColorReferenceByIdAsync(hairColorReferenceId);
            if (hairColor == null || hairColor.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to hairColor object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleHairColorAttributeReferenceAsync(hairColor);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/productionroles/[id}/hairColors
        [Route("{id}/haircolors")]
        [ResponseType(typeof(RoleHairColorReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleHairColor(int productionId, int Id, RoleAttributeReferenceBindingModel hairColor)
        {
            var attribureReference = new RoleHairColorReference() { RoleId = Id, HairColorId = hairColor.AttributeId };
            var ret =
                await _attributeService.AddNewRoleHairColorAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }

        // DELETE: api/productionroles/[id}/hairColors/{hairColorId}
        [Route("{id}/haircolors/{hairColorReferenceId}")]
        [ResponseType(typeof(RoleHairColorReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleHairColor(int productionId, int Id, int hairColorReferenceId)
        {

            var hairColor = await _attributeService.GetRoleHairColorReferenceByIdAsync(hairColorReferenceId);
            if (hairColor == null || hairColor.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleHairColorAttributeReferenceAsync(hairColor);

            return Ok(hairColor);

        }


        #endregion


        #region eyeColors
        // GET: api/productionroles/[id}/eyecolors
        [Route("{id}/eyecolors")]
        [ResponseType(typeof(IEnumerable<RoleEyeColorReference>))]
        public async Task<IHttpActionResult> GetProductionRoleEyeColors(int productionId, int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
            var productionRoleEyeColorss =
                await _attributeService.GetRoleEyeColorReferencesForParentAsync(Id, true);

            return Ok(productionRoleEyeColorss);
        }
        // PUT: api/productionroles/{id}/eyecolors/{eyeColorReferenceId}
        [Route("{id}/eyecolors/{eyeColorReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleEyeColor(int productionId, int id, int eyeColorReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var eyeColor = await _attributeService.GetRoleEyeColorReferenceByIdAsync(eyeColorReferenceId);
            if (eyeColor == null || eyeColor.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to eyeColor object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleEyeColorAttributeReferenceAsync(eyeColor);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/productionroles/[id}/eyeColors
        [Route("{id}/eyecolors")]
        [ResponseType(typeof(RoleEyeColorReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleEyeColor(int productionId, int Id, RoleAttributeReferenceBindingModel eyeColor)
        {
            var attribureReference = new RoleEyeColorReference() { RoleId = Id, EyeColorId = eyeColor.AttributeId };
            var ret =
                await _attributeService.AddNewRoleEyeColorAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }

        // DELETE: api/productionroles/[id}/eyeColors/{eyeColorId}
        [Route("{id}/eyecolors/{eyeColorReferenceId}")]
        [ResponseType(typeof(RoleEyeColorReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleEyeColor(int productionId, int Id, int eyeColorReferenceId)
        {

            var eyeColor = await _attributeService.GetRoleEyeColorReferenceByIdAsync(eyeColorReferenceId);
            if (eyeColor == null || eyeColor.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleEyeColorAttributeReferenceAsync(eyeColor);

            return Ok(eyeColor);

        }


        #endregion

        #region skills
        // GET: api/productionroles/[id}/skills
        [Route("{id}/skills")]
        [ResponseType(typeof(IEnumerable<RoleSkillReference>))]
        public async Task<IHttpActionResult> GetProductionRoleSkills(int productionId, int Id)
        {

            var productionRoleSkillss =
                await _attributeService.GetRoleSkillReferencesForParentAsync(Id, true);

            return Ok(productionRoleSkillss);
        }
        // POST: api/productionroles/[id}/skills
        [Route("{id}/skills")]
        [ResponseType(typeof(RoleSkillReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionRoleSkill(int productionId, int Id, RoleAttributeReferenceBindingModel skill)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var attribureReference = new RoleSkillReference() { RoleId = Id, SkillId = skill.AttributeId };
            var ret =
                await _attributeService.AddNewRoleSkillAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/productionroles/{id}/skills/{skillReferenceId}
        [Route("{id}/skills/{skillReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionRoleSkill(int productionId, int id, int skillReferenceId, RoleAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var skill = await _attributeService.GetRoleSkillReferenceByIdAsync(skillReferenceId);
            if (skill == null || skill.RoleId != id)
            {
                return NotFound();
            }

            //make modifications to skill object here from values of the RoleAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateRoleSkillAttributeReferenceAsync(skill);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/productionroles/[id}/skills/{skillId}
        [Route("{id}/skills/{skillReferenceId}")]
        [ResponseType(typeof(RoleSkillReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionRoleSkill(int productionId, int Id, int skillReferenceId)
        {

            var skill = await _attributeService.GetRoleSkillReferenceByIdAsync(skillReferenceId);
            if (skill == null || skill.RoleId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteRoleSkillAttributeReferenceAsync(skill);

            return Ok(skill);

        }

        #endregion

        #endregion

    }
}
