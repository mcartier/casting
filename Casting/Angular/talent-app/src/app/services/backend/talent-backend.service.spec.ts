import { TestBed } from '@angular/core/testing';

import { TalentBackendService } from './talent-backend.service';

describe('TalentBackendService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TalentBackendService = TestBed.get(TalentBackendService);
    expect(service).toBeTruthy();
  });
});
