﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Talents;
using Microsoft.AspNet.Identity;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Enums;
using Casting.Web.Helpers;
using Casting.Web.Models.Talent;

namespace Casting.Web.Controllers.Agents
{
    public class TalentsController : BaseApiController
    {
        private ITalentService _talentService;
        private readonly UserManager<ApplicationUser> _userManager;
          public TalentsController(UserManager<ApplicationUser> userManager, ITalentService talentService, ICastingContext castingContext) : base(castingContext)
        {
            _talentService = talentService;// castingContext;
            _userManager = userManager;
        }


        // GET: api/Talents
        [ResponseType(typeof(IEnumerable<Talent>))]
        public async Task<IHttpActionResult> GetTalents()
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var talents = await _talentService.GetTalentsByAgentIdAsync(user.BaseUserId);
            
            return Ok(talents);
        }

        // GET: api/Talents/5
        [ResponseType(typeof(Talent))]
        public async Task<IHttpActionResult> GetTalent(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentTalents = await 
            var talent = await _talentService.GetTalentByIdWithAllAgentsReferencesAsync(id);
            if (talent == null)
            {
                return NotFound();
            }
            if (talent.MainAgentId != user.BaseUserId || !talent.AgentReferences.Any(r => r.AgentId == user.BaseUserId))
            {
                return Unauthorized();
            }
             return Ok(talent);
        }

        // PUT: api/Talents/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTalent(int id, TalentBindingModels talent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentTalents = await 
            var tempTalent = await _talentService.GetTalentByIdWithAllAgentsReferencesAsync(id);
            if (tempTalent == null)
            {
                return NotFound();
            }
            if (tempTalent.MainAgentId != user.BaseUserId || !tempTalent.AgentReferences.Any(r => r.AgentId == user.BaseUserId))
            {
                return Unauthorized();
            }

            tempTalent.FirstNames = talent.FirstNames;
            tempTalent.LastNames = talent.LastNames;
           
            try
            {
                await _talentService.UpdateTalentAsync(tempTalent);
            }
            catch (DbUpdateConcurrencyException)
            {
               
                    throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Talents
        [ResponseType(typeof(Talent))]
        public async Task<IHttpActionResult> PostTalent(Talent talent)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            HouseTalent newTalent = new HouseTalent();
            newTalent.FirstNames = talent.FirstNames;
            newTalent.LastNames = talent.LastNames;
            newTalent.MainAgentId = user.BaseUserId;
            newTalent.AgentReferences.Add(new AgentTalentReference() { AgentId = user.BaseUserId});

            await _talentService.AddNewTalentAsync(newTalent);

            return CreatedAtRoute("DefaultApi", new { id = newTalent.Id }, newTalent);
        }

        // DELETE: api/Talents/5
        [ResponseType(typeof(Talent))]
        public async Task<IHttpActionResult> DeleteTalent(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentTalents = await 
            var talent = await _talentService.GetTalentByIdWithAllAgentsReferencesAsync(id);
            if (talent == null)
            {
                return NotFound();
            }
            if (talent.MainAgentId != user.BaseUserId || !talent.AgentReferences.Any(r => r.AgentId == user.BaseUserId))
            {
                return Unauthorized();
            }

            await _talentService.DeleteTalent(talent);

            return Ok(talent);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               
            }
            base.Dispose(disposing);
        }

      
    }
}