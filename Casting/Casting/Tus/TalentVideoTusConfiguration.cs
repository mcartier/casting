﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Casting.Web.Tus.Extensions;
using tusdotnet.Models;
using tusdotnet.Models.Concatenation;
using tusdotnet.Models.Configuration;
using tusdotnet.Models.Expiration;
using tusdotnet.Stores;
using tusdotnet;
using tusdotnet.Interfaces;
using System.Threading.Tasks;
using System.Net;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using Casting.Web.DependencyResolution;
using Casting.BLL.CastingUsers.Interfaces;
using System.Text;
using Casting.Web.Helpers;
using tusdotnet.Extensions;
using System.IO;
using System.Threading;

namespace Casting.Web.Tus
{
    public static class TalentVideoTusConfiguration
    {
        public static DefaultTusConfiguration CreateTusConfiguration(bool enableAuthorize)
        {
            return new DefaultTusConfiguration
            {
                Store = new TusDiskStore(@"C:\tusfiles\", true),
                UrlPath = "/fileso",
                Events = new Events
                {

                    OnAuthorizeAsync = ctx =>
                    {
                        var completedTask = Task.FromResult(0);

                        if (!enableAuthorize)
                            return completedTask;

                        if (ctx.OwinContext.Authentication.User?.Identity?.IsAuthenticated != true)
                        {
                            string[] header = new string[1];
                            header[0] = "Basic realm=\"tusdotnet-test-owin\"";
                            ctx.OwinContext.Response.Headers.Add("WWW-Authenticate", header);
                            ctx.FailRequest(HttpStatusCode.Unauthorized);
                            return completedTask;
                        }

                        var userManager = IoC.Initialize().GetInstance<UserManager<ApplicationUser>>();
                        var talentService = IoC.Initialize().GetInstance<ITalentService>();
                        //var foo = container.ObjectFactory()
                        //   var userManager = context.OwinContext.GetUserManager<ApplicationUserManager>();
                        ApplicationUser user = userManager.FindByName(ctx.OwinContext.Authentication.User.Identity.Name);
                        //var talent = talentService.
                        // Do other verification on the user; claims, roles, etc.

                        // Verify different things depending on the intent of the request.
                        // E.g.:
                        //   Does the file about to be written belong to this user?
                        //   Is the current user allowed to create new files or have they reached their quota?
                        //   etc etc
                        switch (ctx.Intent)
                        {
                            case IntentType.CreateFile:
                                break;
                            case IntentType.ConcatenateFiles:
                                break;
                            case IntentType.WriteFile:
                                var file = AsyncHelper.RunSync<ITusFile>(() =>
                                    ((ITusReadableStore)ctx.Store).GetFileAsync(ctx.FileId, ctx.CancellationToken)
                                    );

                                break;
                            case IntentType.DeleteFile:
                                break;
                            case IntentType.GetFileInfo:
                                break;
                            case IntentType.GetOptions:
                                break;
                            default:
                                break;
                        }

                        return completedTask;
                    },

                    OnBeforeCreateAsync =  ctx =>
                    {
                        // Partial files are not complete so we do not need to validate
                        // the metadata in our example.
                        if (ctx.FileConcatenation is FileConcatPartial)
                        {
                            return Task.CompletedTask;
                        }

                        if (!ctx.Metadata.ContainsKey("name"))
                        {
                            ctx.FailRequest("name metadata must be specified. ");
                        }

                        if (!ctx.Metadata.ContainsKey("type"))
                        {
                            ctx.FailRequest("type metadata must be specified. ");
                        }
                        if (!ctx.Metadata.ContainsKey("contentType"))
                        {
                            ctx.FailRequest("type metadata must be specified. ");
                        }
                       // var talentService = IoC.Initialize().GetInstance<ITalentService>();
                       // return talentService.GetTalentByIdAsync(1).;
                        return Task.CompletedTask;
                    },
                    OnCreateCompleteAsync = ctx =>
                    {
                        Console.WriteLine($"Created file {ctx.FileId} using {ctx.Store.GetType().FullName}");
                        return Task.CompletedTask;
                    },
                    OnBeforeDeleteAsync = ctx =>
                    {
                        // Can the file be deleted? If not call ctx.FailRequest(<message>);
                        return Task.CompletedTask;
                    },
                    OnDeleteCompleteAsync = ctx =>
                    {
                        Console.WriteLine($"Deleted file {ctx.FileId} using {ctx.Store.GetType().FullName}");
                        return Task.CompletedTask;
                    },
                    OnFileCompleteAsync = async ctx =>
                    {
                        Console.WriteLine(
                            $"Upload of {ctx.FileId} is complete. Callback also got a store of type {ctx.Store.GetType().FullName}");
                        // If the store implements ITusReadableStore one could access the completed file here.
                        // The default TusDiskStore implements this interface:
                        var file = await ((ITusReadableStore)ctx.Store).GetFileAsync(ctx.FileId, ctx.CancellationToken);
                         using (FileStream sourceStream = new FileStream(@"C:\processedfiles\" + file.Id + ".webm",
                            FileMode.Append, FileAccess.Write, FileShare.None,
                            bufferSize: 4096, useAsync: true))
                        {

                            using (var fileStream = await file.GetContentAsync(ctx.CancellationToken))
                            {
                                await fileStream.CopyToAsync(sourceStream, 4096, ctx.CancellationToken);
                               
                            }
                              }

                         await ((ITusTerminationStore) ctx.Store).DeleteFileAsync(file.Id, ctx.CancellationToken);
                      /*  var fileName = file.Id;
                        var fullpath = @"c:\tusfiles\";

                        //deleting code starts here
                        string[] files = System.IO.Directory.GetFiles(fullpath, fileName + ".*");
                        foreach (string f in files)
                        {
                            System.IO.File.Delete(f);
                        }
                        */
                        //return Task;
                    }
                },
                // Set an expiration time where incomplete files can no longer be updated.
                // This value can either be absolute or sliding.
                // Absolute expiration will be saved per file on create
                // Sliding expiration will be saved per file on create and updated on each patch/update.
                Expiration = new AbsoluteExpiration(TimeSpan.FromMinutes(5))
            };
        }

    }
}