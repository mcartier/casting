﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Baskets
{
    public class Basket : Logger
    {
        [Key]
        public int Id { get; set; }

        public DateTime DOB { get; set; }
         public ICollection<BasketVideoReference> VideoReferences { get; set; }
         public ICollection<BasketAudioReference> AudioReferences { get; set; }
         public ICollection<BasketImageReference> ImageReferences { get; set; }
         
        public ICollection<BasketResumeReference> ResumeReferences { get; set; }
        public OrderByValuesEnum ResumeReferencesSortOrderI { get; set; }
        public OrderByValuesEnum VideoReferencesSortOrderI { get; set; }
        public OrderByValuesEnum ImageReferencesSortOrderI { get; set; }

        public OrderByValuesEnum AudioReferencesSortOrderI { get; set; }

    }
}
