﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Talents;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas
{
    public class HouseTalentPersona : Persona
    {
        public HouseTalentPersona()
        {
            base.PersonaType = PersonaTypeEnum.HouseTalentPersona;
        }
        public int TalentId { get; set; }
        public HouseTalent Talent { get; set; }
    }
}
