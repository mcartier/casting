﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Casting.Model.Entities.Metadata;

using Casting.Model.Entities.Attributes;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IMetadataAsyncRepository
    {
        Task<Union> GetUnionByIdAsync(int id);
        Task<IEnumerable<Union>> GetUnionsAsync();
        Task<int> AddNewUnionAsync(Union union);
        Task<int> DeleteUnionAsync(Union union);
        Task<int> UpdateUnionAsync(Union union);
        Task<EthnicStereoType> GetEthnicStereoTypeByIdAsync(int id);
        Task<IEnumerable<EthnicStereoType>> GetEthnicStereoTypesAsync();
        Task<int> AddNewEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType);
        Task<int> DeleteEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType);
        Task<int> UpdateEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType);
        Task<Ethnicity> GetEthnicityByIdAsync(int id);
        Task<IEnumerable<Ethnicity>> GetEthnicitiesAsync();
        Task<int> AddNewEthnicityAsync(Ethnicity ethnicity);
        Task<int> DeleteEthnicityAsync(Ethnicity ethnicity);
        Task<int> UpdateEthnicityAsync(Ethnicity ethnicity);
        Task<StereoType> GetStereoTypeByIdAsync(int id);
        Task<IEnumerable<StereoType>> GetStereoTypesAsync();
        Task<int> AddNewStereoTypeAsync(StereoType stereoType);
        Task<int> DeleteStereoTypeAsync(StereoType stereoType);
        Task<int> UpdateStereoTypeAsync(StereoType stereoType);
        Task<Accent> GetAccentByIdAsync(int id);
        Task<IEnumerable<Accent>> GetAccentsAsync();
        Task<int> AddNewAccentAsync(Accent accent);
        Task<int> DeleteAccentAsync(Accent accent);
        Task<int> UpdateAccentAsync(Accent accent);
        Task<Language> GetLanguageByIdAsync(int id);
        Task<IEnumerable<Language>> GetLanguagesAsync();
        Task<int> AddNewLanguageAsync(Language language);
        Task<int> DeleteLanguageAsync(Language language);
        Task<int> UpdateLanguageAsync(Language language);
        Task<HairColor> GetHairColorByIdAsync(int id);
        Task<IEnumerable<HairColor>> GetHairColorsAsync();
        Task<int> AddNewHairColorAsync(HairColor hairColor);
        Task<int> DeleteHairColorAsync(HairColor hairColor);
        Task<int> UpdateHairColorAsync(HairColor hairColor);
        Task<EyeColor> GetEyeColorByIdAsync(int id);
        Task<IEnumerable<EyeColor>> GetEyeColorsAsync();
        Task<int> AddNewEyeColorAsync(EyeColor eyeColor);
        Task<int> DeleteEyeColorAsync(EyeColor eyeColor);
        Task<int> UpdateEyeColorAsync(EyeColor eyeColor);
        Task<BodyType> GetBodyTypeByIdAsync(int id);
        Task<IEnumerable<BodyType>> GetBodyTypesAsync();
        Task<int> AddNewBodyTypeAsync(BodyType bodyType);
        Task<int> DeleteBodyTypeAsync(BodyType bodyType);
        Task<int> UpdateBodyTypeAsync(BodyType bodyType);

        Task<Skill> GetSkillByIdAsync(int id,bool includeCategory);
        Task<IEnumerable<Skill>> GetSkillsAsync(bool includeCategory);
        Task<int> AddNewSkillAsync(Skill skill);
        Task<int> DeleteSkillAsync(Skill skill);
        Task<int> UpdateSkillAsync(Skill skill);

        Task<SkillCategory> GetSkillCategoryByIdAsync(int id,bool includeSkills);
        Task<IEnumerable<SkillCategory>> GetSkillCategoriesAsync(bool includeSkills);
        Task<int> AddNewSkillCategoryAsync(SkillCategory skillCategory);
        Task<int> DeleteSkillCategoryAsync(SkillCategory skillCategory);
        Task<int> UpdateSkillCategoryAsync(SkillCategory skillCategory);

        Task<RoleType> GetRoleTypeByIdAsync(int id);
        Task<IEnumerable<RoleType>> GetRoleTypesAsync();
        Task<int> AddNewRoleTypeAsync(RoleType roleType);
        Task<int> DeleteRoleTypeAsync(RoleType roleType);
        Task<int> UpdateRoleTypeAsync(RoleType roleType);
        Task<RoleGenderType> GetRoleGenderTypeByIdAsync(int id);
        Task<IEnumerable<RoleGenderType>> GetRoleGenderTypesAsync();
        Task<int> AddNewRoleGenderTypeAsync(RoleGenderType roleGenderType);
        Task<int> DeleteRoleGenderTypeAsync(RoleGenderType roleGenderType);
        Task<int> UpdateRoleGenderTypeAsync(RoleGenderType roleGenderType);
        Task<CastingCallType> GetCastingCallTypeByIdAsync(int id);
        Task<IEnumerable<CastingCallType>> GetCastingCallTypesAsync();
        Task<IEnumerable<CastingCallType>> GetCastingCallTypesByCastingCallAsync(int castingCallId);
        Task<int> AddNewCastingCallTypeAsync(CastingCallType castingCallType);
        Task<int> DeleteCastingCallTypeAsync(CastingCallType castingCallType);
        Task<int> UpdateCastingCallTypeAsync(CastingCallType castingCallType);
        Task<CastingCallViewType> GetCastingCallViewTypeByIdAsync(int id);
        Task<IEnumerable<CastingCallViewType>> GetCastingCallViewTypesAsync();
        Task<IEnumerable<CastingCallViewType>> GetCastingCallViewTypesByCastingCallAsync(int castingCallId);
        Task<int> AddNewCastingCallViewTypeAsync(CastingCallViewType castingCallViewType);
        Task<int> DeleteCastingCallViewTypeAsync(CastingCallViewType castingCallViewType);
        Task<int> UpdateCastingCallViewTypeAsync(CastingCallViewType castingCallViewType);
        Task<CastingCallPrivacy> GetCastingCallPrivacyByIdAsync(int id);
        Task<IEnumerable<CastingCallPrivacy>> GetCastingCallPrivaciesAsync();
        Task<IEnumerable<CastingCallPrivacy>> GetCastingCallPrivaciesByCastingCallAsync(int castingCallId);
        Task<int> AddNewCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy);
        Task<int> DeleteCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy);
        Task<int> UpdateCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy);
        Task<ProductionType> GetProductionTypeByIdAsync(int id);
        Task<IEnumerable<ProductionType>> GetProductionTypesAsync();
        Task<IEnumerable<ProductionType>> GetProductionTypesByProductionTypeCatigoryAsync(int productionTypeCategoryId);
        Task<IEnumerable<ProductionType>> GetProductionTypesByProductionAsync(int productionId);
        Task<int> AddNewProductionTypeAsync(ProductionType productionType);
        Task<int> DeleteProductionTypeAsync(ProductionType productionType);
        Task<int> UpdateProductionTypeAsync(ProductionType productionType);
        Task<ProductionTypeCategory> GetProductionTypeCategoryByIdAsync(int id);
        Task<IEnumerable<ProductionTypeCategory>> GetProductionTypeCategoriesAsync();
        Task<IEnumerable<ProductionTypeCategory>> GetCompleteProductionTypeCategoriesAsync();
        Task<int> AddNewProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory);
        Task<int> DeleteProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory);
        Task<int> UpdateProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory);
       
    }
}