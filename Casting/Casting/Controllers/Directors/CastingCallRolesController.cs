﻿using Casting.BLL.CastingCalls;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.CastingCalls;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingCalls;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using Casting.DAL;
using Casting.Model.Entities.AttributeReferences;
using System.Data.Entity;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingcallroles")]
   // [Authorize]
    public class CastingCallRolesController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingCallRoleService _castingCallRoleService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public CastingCallRolesController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, ICastingCallRoleService castingCallRoleService, IProductionService productionService, ICastingContext castingContext) : base(castingContext)
        {
            _productionService = productionService;
            _castingCallRoleService = castingCallRoleService;
            _metadataService = metadataService;
            _userManager = userManager;
        }


        // GET: api/CastingCallRoles
        [ResponseType(typeof(IEnumerable<CastingCallRole>))]
        public async Task<IHttpActionResult> GetCastingCallRoles(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallRoles =
                await _castingCallRoleService.GetCastingCallRolesByCastingCallIdAsync(0);

            return Ok(castingCallRoles);
        }

        // GET: api/CastingCallRoles
        [System.Web.Http.ActionName("castingCall")]
        [Route("castingCall/{castingCallId}")]
        [ResponseType(typeof(IEnumerable<CastingCallRole>))]
        public async Task<IHttpActionResult> GetCastingCallRolesForCastingCall(int castingCallId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallRoles =
                await _castingCallRoleService.GetCastingCallRolesByCastingCallIdWithPersonasAndAssetsAsync(castingCallId);

            return Ok(castingCallRoles);
        }

        // GET: api/CastingCallRoles/5
        [ResponseType(typeof(CastingCallRole))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var castingCallRole =
                await _castingCallRoleService.GetCastingCallRoleByIdAsync(id);

            return Ok(castingCallRole);
        }

        // POST: api/CastingCallRoles
        [ResponseType(typeof(CastingCallRole))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewCastingCallRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
             /* var castingCallRole = new CastingCallRole()
            {
                DirectorId = user.BaseUserId,
                Name = name
            };*/
            var castingCallRole = new CastingCallRole()
            {
                CastingCallId = value.CastingCallId,
                RoleId = value.RoleId,
                RequiresHeadshot = value.RequiresHeadshot,
                HeadshotMaxCount = value.HeadshotMaxCount,
                RequiresVideo = value.RequiresVideo,
                VideoMaxCount = value.VideoMaxCount,
                RequiresAudio = value.RequiresAudio,
                AudioMaxCount = value.AudioMaxCount,
                RequiresCoverLetter = value.RequiresCoverLetter,


            };
            foreach (var unionId in value.Unions)
            {
                var union = await _metadataService.GetUnionByIdAsync(unionId);
                if (union != null)
                {
                    castingCallRole.Unions.Add(new CastingCallRoleUnionReference() { Union = union });
                }
            }

            var ret = await _castingCallRoleService.AddNewCastingCallRoleAsync(true, castingCallRole);
            return CreatedAtRoute("DefaultApi", new { id = castingCallRole.Id }, castingCallRole);
        }

   
        // PUT: api/CastingCallRoles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateCastingCallRoleBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingCallRole = await _castingCallRoleService.GetCastingCallRoleByIdAsync(id);
            if (castingCallRole == null)
            {
                return NotFound();
            }
            /*if (castingCallRole.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/

            castingCallRole.RequiresHeadshot = value.RequiresHeadshot;
            castingCallRole.HeadshotMaxCount = value.HeadshotMaxCount;
            castingCallRole.RequiresVideo = value.RequiresVideo;
            castingCallRole.VideoMaxCount = value.VideoMaxCount;
            castingCallRole.RequiresAudio = value.RequiresAudio;
            castingCallRole.AudioMaxCount = value.AudioMaxCount;
            castingCallRole.RequiresCoverLetter = value.RequiresCoverLetter;
            foreach (var stereoType in castingCallRole.Unions.OfType<CastingCallRoleUnionReference>().ToList())
            {
                if (!value.Unions.Any(e => e == stereoType.Id))
                {
                    _castingContext.SetState(stereoType, EntityState.Deleted);
                    castingCallRole.Unions.Remove(stereoType);
                }
            }
            foreach (var unionId in value.Unions)
            {
                if (!castingCallRole.Unions.Any(s => s.Id == unionId))
                {
                    var union = await _metadataService.GetUnionByIdAsync(unionId);
                    if (union != null)
                    {
                        castingCallRole.Unions.Add(new CastingCallRoleUnionReference() { Union = union });
                    }
                }
            }

            try
            {
                await _castingCallRoleService.UpdateCastingCallRoleAsync(castingCallRole);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/CastingCallRoles/5
        [ResponseType(typeof(CastingCallRole))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingCallRole = await _castingCallRoleService.GetCastingCallRoleByIdAsync(id);
            if (castingCallRole == null)
            {
                return NotFound();
            }
            /*if (castingCallRole.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }*/



            await _castingCallRoleService.DeleteCastingCallRole(castingCallRole);

            return Ok(castingCallRole);
        }
    }
}
