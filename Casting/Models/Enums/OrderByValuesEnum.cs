﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum OrderByValuesEnum
    {
        CreatedDateAscending,
        CreatedDateDescending,
        NameAscending,
        NameDescending,
        Custom
    }
}
