import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { ProductionDialogService } from '../../services/production-dialog.service';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MessageService } from 'primeng/api';
import { ConfirmDecisionSubscriptionService } from '../../../../services/confirm-decision-subscription.service';

@Component({
  selector: 'app-sides-list-item',
  templateUrl: './sides-list-item.component.html',
  styleUrls: ['./sides-list-item.component.scss']
})
export class SidesListItemComponent implements OnInit {

  constructor(
    private productionDialogService: ProductionDialogService,
    private productionExtraService: ProductionExtraService,
    private messageDispatchService: MessageDispatchService,
    private messageService: MessageService,
    private decisionDispatcher: ConfirmDecisionSubscriptionService
  ) { }

  private decisionDispatch: Subscription;
  @Input() detail: Side;

  ngOnInit() {
  }

  ngOnDestroy() {

    if (this.decisionDispatch) {
      this.decisionDispatch.unsubscribe();

    }

  }
  private viewDetails() {
    this.productionDialogService.showSidesDialog(this.detail);

  }
  private deleteDetails() {
    this.decisionDispatcher.poseConfirmDecision('Delete side ' + this.detail.name + '?', 'Confirm to proceed');
    this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
      .subscribe(
        (decision: boolean) => {
          this.decisionDispatch.unsubscribe();

          if (decision) {
            this.productionExtraService.deleteSide(this.detail.id, this.detail.productionId).subscribe(
              (shootDetail: Side) => {
                //this.shootDetail = shootDetail;
                this.messageDispatchService.dispatchSuccesMessage("Side '" + this.detail.name + "' was deleted successfully",
                  "Side Deleted");

              },
              (error) => {
                this.messageDispatchService.dispatchErrorMessage(
                  "Error saving side '" + this.detail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                  "Side Save Error");
              });

          }
        });


  }
}
