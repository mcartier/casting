﻿using Casting.BLL.CastingCalls;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.CastingCalls;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.CastingCalls;
using System.Data.Entity.Infrastructure;
using System.Web.Http.Results;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Productions.Interfaces;
using WebGrease.Css.Extensions;
using Casting.DAL;
using System.Data.Entity;
using Casting.Model.Entities.AttributeReferences;
using Casting.Web.Models;
using Casting.BLL.AttributeReferences.Interfaces;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/castingcalls")]
   // [Authorize]
    public class CastingCallsController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingCallService _castingCallService;
        private ICastingCallRoleService _castingCallRoleService;
        private IAttributeReferenceService _attributeService;
        private IMetadataService _metadataService;
        private readonly UserManager<ApplicationUser> _userManager;

        public CastingCallsController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, IAttributeReferenceService attributeService,
            ICastingCallService castingCallService, ICastingCallRoleService castingCallRoleService, IProductionService productionService, ICastingContext castingContext) : base(castingContext)
        {
            _productionService = productionService;
            _castingCallService = castingCallService;
            _castingCallRoleService = castingCallRoleService;
            _attributeService = attributeService;
            _metadataService = metadataService;
            _userManager = userManager;
        }


        // GET: api/CastingCalls
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCalls(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCalls =
                await _castingCallService.GetCastingCallsAsync(CastingCallParameterIncludeInQueryEnum.CastingCalls, pageIndex, pageSize);

            return Ok(castingCalls);
        }

        /*
        #### replaced by method in the production controller
        
        // GET: api/CastingCalls
        [System.Web.Http.ActionName("production")]
        [Route("production/{productionId}")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetCastingCallsForProduction(int productionId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
           
            var castingCalls =
                await _castingCallService.GetCastingCallsByProductionIdWithRolesPersonasAsync(productionId);

            return Ok(castingCalls);
        }
    */
        // GET: api/CastingCalls/5
        [ResponseType(typeof(CastingCall))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var castingCall =
                await _castingCallService.GetCastingCallByIdAsync(id);

            return Ok(castingCall);
        }

        // GET: api/CastingCalls/[id}/Roles
        [Route("{id}/roles")]
        [ResponseType(typeof(IEnumerable<CastingCallRole>))]
        public async Task<IHttpActionResult> GetCastingCallRoles(int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCallRoles =
                await _castingCallRoleService.GetCastingCallRolesByCastingCallIdWithPersonasAndAssetsAsync(Id);

            return Ok(castingCallRoles);
        }



        // POST: api/CastingCalls
        [ResponseType(typeof(CastingCall))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewCastingCallBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
             /* var castingCall = new CastingCall()
            {
                DirectorId = user.BaseUserId,
                Name = name
            };*/
             var production = await _productionService.GetProductionByIdWithRolesAsync(value.ProductionId);
             if (production == null)
             {
                return BadRequest("Production with Id " + value.ProductionId + " does not exist");
             }

            // foreach (var roleId in value.Roles)
            // {
            //     if (!production.Roles.Any(r => r.Id == roleId))
            //     {
            //         return BadRequest("Role with Id " + roleId + " was not found in the given production");
           //      }
           //  }

             var castingCall = new CastingCall()
            {
                ProductionId = value.ProductionId,
                CastingCallType = value.CastingCallType,
               // ViewTypeId = value.ViewTypeId,
                TalentSourceLocation = value.TalentSourceLocation,
                AcceptingSubmissions = value.AcceptingSubmissions,
                Instructions = value.Instructions,
                Expires = value.Expires,
                LocationType = value.LocationType,
                PostedDate = value.PostedDate,
                RequiresHeadshot = value.RequiresHeadshot,
                HeadshotMaxCount = value.HeadshotMaxCount,
                RequiresVideo = value.RequiresVideo,
                VideoMaxCount = value.VideoMaxCount,
                RequiresAudio = value.RequiresAudio,
                AudioMaxCount = value.AudioMaxCount,
                RequiresCoverLetter = value.RequiresCoverLetter,

             };
             foreach (var unionId in value.Unions)
             {
                 var union = await _metadataService.GetUnionByIdAsync(unionId);
                 if (union != null)
                 {
                     castingCall.Unions.Add(new CastingCallUnionReference() { Union = union });
                 }
             }

           // foreach (var roleId in value.Roles)
            //{
            // castingCall.Roles.Add(new CastingCallRole()
           //  {
            //     RoleId = roleId
           //  });
         //   }

            var ret = await _castingCallService.AddNewCastingCallAsync(castingCall);
            return CreatedAtRoute("DefaultApi", new { id = castingCall.Id }, castingCall);
        }

       
        // PUT: api/CastingCalls/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateCastingCallBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingCall = await _castingCallService.GetCastingCallByIdAsync(id);
            if (castingCall == null)
            {
                return NotFound();
            }
            if (castingCall.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }

            castingCall.ProductionId = value.ProductionId;
            castingCall.CastingCallType = value.CastingCallType;
           // castingCall.ViewTypeId = value.ViewTypeId;
            castingCall.TalentSourceLocation = value.TalentSourceLocation;
            castingCall.AcceptingSubmissions = value.AcceptingSubmissions;
            castingCall.Instructions = value.Instructions;
            castingCall.Expires = value.Expires;
            castingCall.LocationType = value.LocationType;
            castingCall.PostedDate = value.PostedDate;
            castingCall.RequiresHeadshot = value.RequiresHeadshot;
            castingCall.HeadshotMaxCount = value.HeadshotMaxCount;
            castingCall.RequiresVideo = value.RequiresVideo;
            castingCall.VideoMaxCount = value.VideoMaxCount;
            castingCall.RequiresAudio = value.RequiresAudio;
            castingCall.AudioMaxCount = value.AudioMaxCount;
            castingCall.RequiresCoverLetter = value.RequiresCoverLetter;
            foreach (var stereoType in castingCall.Unions.OfType<CastingCallUnionReference>().ToList())
            {
                if (!value.Unions.Any(e => e == stereoType.Id))
                {
                    _castingContext.SetState(stereoType, EntityState.Deleted);
                    castingCall.Unions.Remove(stereoType);
                }
            }
            foreach (var unionId in value.Unions)
            {
                if (!castingCall.Unions.Any(s => s.Id == unionId))
                {
                    var union = await _metadataService.GetUnionByIdAsync(unionId);
                    if (union != null)
                    {
                        castingCall.Unions.Add(new CastingCallUnionReference() { Union = union });
                    }
                }
            }

            try
            {
                await _castingCallService.UpdateCastingCallAsync(castingCall);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/CastingCalls/5
        [ResponseType(typeof(CastingCall))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var castingCall = await _castingCallService.GetCastingCallByIdAsync(id);
            if (castingCall == null)
            {
                return NotFound();
            }
            if (castingCall.Production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }

            await _castingCallService.DeleteCastingCall(castingCall);

            return Ok(castingCall);
        }

        #region CastingCall attribute references
        // GET: api/castingcalls/[id}/unions
        [Route("{id}/unions")]
        [ResponseType(typeof(IEnumerable<CastingCallUnionReference>))]
        public async Task<IHttpActionResult> GetCastingCallUnions(int Id)
        {

            var castingCallUnionss =
                await _attributeService.GetCastingCallUnionReferencesForParentAsync(Id, true);

            return Ok(castingCallUnionss);
        }

        // POST: api/castingcalls/[id}/unions
        [Route("{id}/unions")]
        [ResponseType(typeof(CastingCallUnionReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostCastingCallUnion(int Id, CastingCallAttributeReferenceBindingModel union)
        {
            var attribureReference = new CastingCallUnionReference() { CastingCallId = Id, UnionId = union.AttributeId };
            var ret =
                await _attributeService.AddNewCastingCallUnionAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }
        // PUT: api/castingcalls/{id}/unions/{unionReferenceId}
        [Route("{id}/unions/{unionReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutCastingCallUnion(int id, int unionReferenceId, CastingCallAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var union = await _attributeService.GetCastingCallUnionReferenceByIdAsync(unionReferenceId);
            if (union == null || union.CastingCallId != id)
            {
                return NotFound();
            }

            //make modifications to union object here from values of the CastingCallAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateCastingCallUnionAttributeReferenceAsync(union);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/castingcalls/[id}/unions/{unionId}
        [Route("{id}/unions/{unionReferenceId}")]
        [ResponseType(typeof(CastingCallUnionReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCastingCallUnion(int Id, int unionReferenceId)
        {

            var union = await _attributeService.GetCastingCallUnionReferenceByIdAsync(unionReferenceId);
            if (union == null || union.CastingCallId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteCastingCallUnionAttributeReferenceAsync(union);

            return Ok(union);

        }


        #endregion
    }
}
