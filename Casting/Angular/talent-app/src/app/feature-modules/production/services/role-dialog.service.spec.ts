import { TestBed } from '@angular/core/testing';

import { RoleDialogService } from './role-dialog.service';

describe('RoleDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RoleDialogService = TestBed.get(RoleDialogService);
    expect(service).toBeTruthy();
  });
});
