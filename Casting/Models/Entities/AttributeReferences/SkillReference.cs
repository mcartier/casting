﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Attributes;
using Casting.Model.Enums;

namespace Casting.Model.Entities.AttributeReferences
{
    public abstract class SkillReference : Logger
    {
        public int Id { get; set; }
       
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
        public int LevelId { get; set; }
        public AttributeReferenceTypeEnum AttributeReferenceTypeEnum { get; set; }

        public int OrderIndex { get; set; }
    }
}