import { TestBed } from '@angular/core/testing';

import { ProductionDialogService } from './production-dialog.service';

describe('ProductionDialogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductionDialogService = TestBed.get(ProductionDialogService);
    expect(service).toBeTruthy();
  });
});
