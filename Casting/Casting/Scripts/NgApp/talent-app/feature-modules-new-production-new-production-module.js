(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["feature-modules-new-production-new-production-module"],{

/***/ "./src/app/feature-modules/new-production/components/add-role/add-role.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/add-role/add-role.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  add-role works!\n</p>\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/add-role/add-role.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/add-role/add-role.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9uZXctcHJvZHVjdGlvbi9jb21wb25lbnRzL2FkZC1yb2xlL2FkZC1yb2xlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/add-role/add-role.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/add-role/add-role.component.ts ***!
  \******************************************************************************************/
/*! exports provided: AddRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddRoleComponent", function() { return AddRoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AddRoleComponent = /** @class */ (function () {
    function AddRoleComponent() {
    }
    AddRoleComponent.prototype.ngOnInit = function () {
    };
    AddRoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-role',
            template: __webpack_require__(/*! ./add-role.component.html */ "./src/app/feature-modules/new-production/components/add-role/add-role.component.html"),
            styles: [__webpack_require__(/*! ./add-role.component.scss */ "./src/app/feature-modules/new-production/components/add-role/add-role.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AddRoleComponent);
    return AddRoleComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/new-production/new-production.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/new-production/new-production.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"p-grid\">\r\n    <div class=\"p-col-9\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n            <p-card class=\"p-col\" header=\"Create a new project\">\r\n               <p-header>\r\n                <div #basiccard></div>\r\n                </p-header>\r\n                <form [formGroup]=\"basicProductionInfo\">\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Title of Production</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"e.g. 'Batman lives'\" #name formControlName=\"name\" required>\r\n                                <p-message severity=\"error\" text=\"Production Title is <strong>required</strong>\" class=\"p-col\" *ngIf=\"basicProductionInfo.controls['name'].hasError('required') && basicProductionInfo.controls['name'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Production Type</label>\r\n                                <p-dropdown [options]=\"productionTypes\" [group]=\"true\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #productiontype placeholder=\"Select a Type of Production\" formControlName=\"productionTypeId\" required>\r\n                                  <ng-template let-item pTemplate=\"selectedItem\"> \r\n                                    {{item.label}} <!-- highlighted selected item -->\r\n                                  </ng-template> \r\n                                  <ng-template let-item pTemplate=\"item\"> \r\n                                    {{item.label}}\r\n                                  </ng-template>\r\n                                  <ng-template let-group pTemplate=\"group\">\r\n                                    <span style=\"margin-left:.25em\">{{group.label}}</span>\r\n                                  </ng-template>\r\n                                  </p-dropdown>\r\n                                <!-- <mat-select placeholder=\"Select a Type of Production\" formControlName=\"productionTypeId\" required>\r\n                                     <mat-optgroup *ngFor=\"let cat of meta.productionTypeCategories\" [label]=\"cat.name\">\r\n                                         <mat-option *ngFor=\"let prodType of cat.productionTypes\" [value]=\"prodType.id\">\r\n                                                </mat-option>\r\n\r\n                                     </mat-optgroup>\r\n\r\n                                 </mat-select>-->\r\n                                <p-message severity=\"error\" text=\"Production Type is <strong>required</strong>\" class=\"p-col\" *ngIf=\"basicProductionInfo.controls['productionTypeId'].hasError('required') && basicProductionInfo.controls['productionTypeId'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Production</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Production Company</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Production Company Name\" formControlName=\"productionCompany\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. Starlight Productions</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Production Description</label>\r\n                                <textarea rows=\"3\" class=\"p-col\" pInputTextarea #description placeholder=\"Casting 'Batman Lives', a new CBS TV series based on the classic legends but taking place in a modern-day Gotham City.\" formControlName=\"description\" required></textarea>\r\n                                <p-message severity=\"error\" text=\"Production Description is <strong>required</strong>\" class=\"p-col\" *ngIf=\"basicProductionInfo.controls['description'].hasError('required') && basicProductionInfo.controls['description'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Casting 'Batman Lives', a new CBS TV series based on the classic legends but taking place in a modern-day Gotham City.</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Union Status</label>\r\n                              <div class=\"p-col\">\r\n                                <div class=\"p-grid p-justify-around\">\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"7\" label=\"Union and Non-Union\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"3\" label=\"Union\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"1\" label=\"Non-Union\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"15\" label=\"N/A\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-start\">\r\n                                <label class=\"p-col\">Will the talent be paid?</label>\r\n                              <div class=\"p-col\">\r\n                                <div class=\"p-grid p-justify-around\">\r\n                                <p-radioButton class=\"p-col\" name=\"pays\" value=\"1\" label=\"No\" formControlName=\"compensationType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"pays\" value=\"3\" label=\"Some\" formControlName=\"compensationType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"pays\" value=\"7\" label=\"Yes\" formControlName=\"compensationType\"></p-radioButton>\r\n                                </div>\r\n                              </div>\r\n\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Compensation & Union Contract Details</label>\r\n                                <textarea rows=\"3\" class=\"p-col\" pInputTextarea placeholder=\"e.g., Pays $100/day, plus travel and meals provided. No participation fees required. The producers plan to apply for a SAG-AFTRA New Media Agreement.\" formControlName=\"compensationContractDetails\"></textarea>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g., Pays $100/day, plus travel and meals provided. No participation fees required. The producers plan to apply for a SAG-AFTRA New Media Agreement.</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </p-card>\r\n\r\n            <p-card header=\"Roles\" class=\"p-col\">\r\n              <p-header>\r\n                <div #rolescard></div>\r\n              </p-header>\r\n\r\n\r\n                <app-role-list [production]=\"production\" [roles]=\"production.roles\">\r\n\r\n                </app-role-list>\r\n\r\n\r\n                <button (click)=\"addRoleClick()\" label=\"Add Role\" pButton></button>\r\n            </p-card>\r\n            <p-card header=\"Casting\" class=\"p-col\">\r\n              <p-header>\r\n                <div #castingcard></div>\r\n              </p-header>\r\n\r\n                <form [formGroup]=\"castingDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Casting Director</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Casting Director\" formControlName=\"castingDirector\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Casting Director</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Casting Assistant</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Casting Assistant\" formControlName=\"castingAssistant\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Casting Assistant</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Casting Associate</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Casting Associate\" formControlName=\"castingAssociate\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Casting Associate</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Assistant Casting Associate</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Assistant Casting Associate\" formControlName=\"assistantCastingAssociate\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Assistant Casting Associate</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n\r\n            </p-card>\r\n            <p-card header=\"Producing\" class=\"p-col\">\r\n              <p-header>\r\n                <div #productioncard></div>\r\n              </p-header>\r\n\r\n                <form [formGroup]=\"productionDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Executive Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Executive Producer\" formControlName=\"executiveProducer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Executive Producer</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Co Executive Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Co Executive Producer\" formControlName=\"coExecutiveProducer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Co Executive Producer</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Producer\" formControlName=\"producer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Producer</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Co Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Co Producer\" formControlName=\"coProducer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Co Producer</div>\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </form>\r\n\r\n            </p-card>\r\n            <p-card header=\"Artistic\" class=\"p-col\">\r\n              <p-header>\r\n                <div #artisticcard></div>\r\n              </p-header>\r\n                <form [formGroup]=\"artisticDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Artistic Director</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Artistic Director\" formControlName=\"artisticDirector\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Artistic Director</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Music Director</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Music Director\" formControlName=\"musicDirector\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Music Director</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Choreographer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Choreographer\" formControlName=\"choreographer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Choreographer</div>\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </form>\r\n            </p-card>\r\n            <p-card header=\"Other\" class=\"p-col\">\r\n              <p-header>\r\n                <div #othercard></div>\r\n              </p-header>\r\n                <form [formGroup]=\"extraDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">TV Network</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the TV Network\" formControlName=\"TVNetwork\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the TV Network</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Venue</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Venue\" formControlName=\"venue\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Venue</div>\r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </p-card>\r\n            <div class=\"p-grid p-justify-end p-align-center\">\r\n                <button pButton (click)=\"cancelEditProductionClick()\" label=\"Cancel\"></button>\r\n                <button pButton color=\"primary\" (click)=\"saveProductionClick()\" label=\"Save\"></button>\r\n\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n    <div class=\"p-col-3\">\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n            <button class=\"p-col\" pButton (click)=\"scrollToBasic()\">go Basic</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToRoles()\">go Roles</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToCasting()\">go Casting</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToProduction()\">go Production</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToArtistic()\">go Artistic</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToOther()\">go Other</button>\r\n            <button class=\"p-col\" pButton color=\"primary\" (click)=\"saveProductionClick()\">Save</button>\r\n            <button class=\"p-col\" pButton color=\"primary\" (click)=\"deleteProductionClick()\">Delete</button>\r\n            <button class=\"p-col\" pButton color=\"primary\" (click)=\"cancelEditProductionClick()\">Cancel</button>\r\n        </div>\r\n\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/new-production/new-production.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/new-production/new-production.component.scss ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Absolute Center Spinner */\n/*\r\n@import '~@angular/material/prebuilt-themes/deeppurple-amber.css';\r\n@mixin primary-color($theme) {\r\n\r\n  $primary: map-get($theme, primary);\r\n\r\n  .mat-card-header, mat-card-header {\r\n        background: mat-color($primary) !important;\r\n       }\r\n\r\n}*/\n/*\r\n@import '../../../../../_variables.scss';\r\n*/\n:host {\n  height: 100%; }\n.production_card {\n  margin-left: 16px;\n  margin-right: 16px; }\n/*\r\nmat-card-header {\r\n  background-color: $primary;\r\n}*/\nmat-form-field, .mat-form-field {\n  width: 100%; }\n.my-form-field {\n  /*margin-bottom:24px;*/ }\n.mokformfield {\n  width: 100%;\n  padding-bottom: 1.34375em; }\n.mokformfield mat-label, .mokformfield mat-radion-group {\n  display: block; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXByb2R1Y3Rpb24vRTpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xcbmV3LXByb2R1Y3Rpb25cXGNvbXBvbmVudHNcXG5ldy1wcm9kdWN0aW9uXFxuZXctcHJvZHVjdGlvbi5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXByb2R1Y3Rpb24vbmV3LXByb2R1Y3Rpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNEJBQUE7QUFJQTs7Ozs7Ozs7OztFQ09FO0FESUY7O0NDREM7QURJRDtFQUNFLFlBQVcsRUFBQTtBQUliO0VBQ0UsaUJBQWdCO0VBQ2hCLGtCQUFpQixFQUFBO0FBRW5COzs7RUNGRTtBRE1GO0VBRUUsV0FBVSxFQUFBO0FBRVo7RUFDRSxzQkFBQSxFQUF1QjtBQUV6QjtFQUVFLFdBQVU7RUFDVix5QkFBd0IsRUFBQTtBQUUxQjtFQUVDLGNBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9uZXctcHJvZHVjdGlvbi9jb21wb25lbnRzL25ldy1wcm9kdWN0aW9uL25ldy1wcm9kdWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogQWJzb2x1dGUgQ2VudGVyIFNwaW5uZXIgKi9cclxuXHJcblxyXG5cclxuLypcclxuQGltcG9ydCAnfkBhbmd1bGFyL21hdGVyaWFsL3ByZWJ1aWx0LXRoZW1lcy9kZWVwcHVycGxlLWFtYmVyLmNzcyc7XHJcbkBtaXhpbiBwcmltYXJ5LWNvbG9yKCR0aGVtZSkge1xyXG5cclxuICAkcHJpbWFyeTogbWFwLWdldCgkdGhlbWUsIHByaW1hcnkpO1xyXG5cclxuICAubWF0LWNhcmQtaGVhZGVyLCBtYXQtY2FyZC1oZWFkZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IG1hdC1jb2xvcigkcHJpbWFyeSkgIWltcG9ydGFudDtcclxuICAgICAgIH1cclxuXHJcbn0qL1xyXG4vKlxyXG5AaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi9fdmFyaWFibGVzLnNjc3MnO1xyXG4qL1xyXG46aG9zdCB7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbn1cclxuXHJcblxyXG4ucHJvZHVjdGlvbl9jYXJkIHtcclxuICBtYXJnaW4tbGVmdDoxNnB4O1xyXG4gIG1hcmdpbi1yaWdodDoxNnB4O1xyXG59XHJcbi8qXHJcbm1hdC1jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XHJcbn0qL1xyXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkXHJcbntcclxuICB3aWR0aDoxMDAlO1xyXG59XHJcbi5teS1mb3JtLWZpZWxkIHtcclxuICAvKm1hcmdpbi1ib3R0b206MjRweDsqL1xyXG59XHJcbi5tb2tmb3JtZmllbGRcclxue1xyXG4gIHdpZHRoOjEwMCU7XHJcbiAgcGFkZGluZy1ib3R0b206MS4zNDM3NWVtO1xyXG59XHJcbi5tb2tmb3JtZmllbGQgbWF0LWxhYmVsLCAubW9rZm9ybWZpZWxkIG1hdC1yYWRpb24tZ3JvdXBcclxue1xyXG4gZGlzcGxheTpibG9jaztcclxufSIsIi8qIEFic29sdXRlIENlbnRlciBTcGlubmVyICovXG4vKlxyXG5AaW1wb3J0ICd+QGFuZ3VsYXIvbWF0ZXJpYWwvcHJlYnVpbHQtdGhlbWVzL2RlZXBwdXJwbGUtYW1iZXIuY3NzJztcclxuQG1peGluIHByaW1hcnktY29sb3IoJHRoZW1lKSB7XHJcblxyXG4gICRwcmltYXJ5OiBtYXAtZ2V0KCR0aGVtZSwgcHJpbWFyeSk7XHJcblxyXG4gIC5tYXQtY2FyZC1oZWFkZXIsIG1hdC1jYXJkLWhlYWRlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogbWF0LWNvbG9yKCRwcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgICAgfVxyXG5cclxufSovXG4vKlxyXG5AaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi9fdmFyaWFibGVzLnNjc3MnO1xyXG4qL1xuOmhvc3Qge1xuICBoZWlnaHQ6IDEwMCU7IH1cblxuLnByb2R1Y3Rpb25fY2FyZCB7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xuICBtYXJnaW4tcmlnaHQ6IDE2cHg7IH1cblxuLypcclxubWF0LWNhcmQtaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeTtcclxufSovXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7IH1cblxuLm15LWZvcm0tZmllbGQge1xuICAvKm1hcmdpbi1ib3R0b206MjRweDsqLyB9XG5cbi5tb2tmb3JtZmllbGQge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy1ib3R0b206IDEuMzQzNzVlbTsgfVxuXG4ubW9rZm9ybWZpZWxkIG1hdC1sYWJlbCwgLm1va2Zvcm1maWVsZCBtYXQtcmFkaW9uLWdyb3VwIHtcbiAgZGlzcGxheTogYmxvY2s7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/new-production/new-production.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/new-production/new-production.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: NewProductionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProductionComponent", function() { return NewProductionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/new-production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/production-component.service */ "./src/app/feature-modules/new-production/services/production-component.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _animations_router_animation__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../animations/router.animation */ "./src/app/animations/router.animation.ts");











//import { NewRoleComponent } from '../new-role/new-role.component'

var NewProductionComponent = /** @class */ (function () {
    function NewProductionComponent(ProductionRoleService, _formBuilder, productionService, stateService, transitionService, transition, roleDialogService, productionComponentService) {
        this.ProductionRoleService = ProductionRoleService;
        this._formBuilder = _formBuilder;
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.roleDialogService = roleDialogService;
        this.productionComponentService = productionComponentService;
        this.compensationType = _models_production_models__WEBPACK_IMPORTED_MODULE_4__["CompensationTypeEnum"].notPaid;
        //private production: Production;
        this.showHints = false;
        this.productionTypes = [];
    }
    NewProductionComponent.prototype.ngOnInit = function () {
        var _this = this;
        for (var i = 0; i < this.metadata.productionTypeCategories.length; i++) {
            var prodType = {
                label: this.metadata.productionTypeCategories[i].name,
                // value: this.metadata.productionTypeCategories[i].id,
                items: []
            };
            for (var x = 0; x < this.metadata.productionTypeCategories[i].productionTypes.length; x++) {
                prodType.items.push({
                    label: this.metadata.productionTypeCategories[i].productionTypes[x].name,
                    value: this.metadata.productionTypeCategories[i].productionTypes[x].id
                });
            }
            this.productionTypes.push(prodType);
        }
        this.deleteRoleSubscription = this.ProductionRoleService.subscribeToDeleteRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles.splice(_this.production.roles.indexOf(role), 1);
            }
        });
        this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles[_this.production.roles.indexOf(role)] = role;
            }
        });
        this.deleteProductionIntentSubscription = this.productionComponentService.subscribeToDeleteProductionIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToProductionsView();
            }
        });
        this.cancelEditProductionIntentSubscription = this.productionComponentService.subscribeToCancelEditProductionIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToProductionsView();
            }
        });
        this.saveProductionIntentSubscription = this.productionComponentService.subscribeToSaveProductionIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.saveProductionClick();
            }
        });
        if (this.transition.params().popRole) {
            this.popRole = this.transition.params().popRole;
        }
        if (this.transition.params().isDraft) {
            this.isDraft = this.transition.params().isDraft;
        }
        if (this.transition.params().isEdit) {
            this.isEdit = this.transition.params().isEdit;
        }
        if (this.transition.params().production) {
            this.production = this.transition.params().production;
        }
        if (this.production) {
            this.basicProductionInfo = this._formBuilder.group({
                name: [this.production.name, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionTypeId: [this.metadata.productionTypes[this.metadata.productionTypes.findIndex(function (t) { return t.id == _this.production.productionTypeId; })], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionCompany: [this.production.productionCompany],
                description: [this.production.description, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                compensationType: [this.production.compensationType],
                compensationContractDetails: [this.production.compensationContractDetails],
                productionUnionStatus: [this.production.productionUnionStatus]
            }, { updateOn: 'blur' });
            this.castingDetails = this._formBuilder.group({
                castingDirector: [this.production.castingDirector],
                castingAssistant: [this.production.castingAssistant],
                castingAssociate: [this.production.castingAssociate],
                assistantCastingAssociate: [this.production.assistantCastingAssociate]
            }, { updateOn: 'blur' });
            this.productionDetails = this._formBuilder.group({
                executiveProducer: [this.production.executiveProducer],
                coExecutiveProducer: [this.production.coExecutiveProducer],
                producer: [this.production.producer],
                coProducer: [this.production.coProducer]
            }, { updateOn: 'blur' });
            this.artisticDetails = this._formBuilder.group({
                artisticDirector: [this.production.artisticDirector],
                musicDirector: [this.production.musicDirector],
                choreographer: [this.production.choreographer]
            }, { updateOn: 'blur' });
            this.extraDetails = this._formBuilder.group({
                TVNetwork: [this.production.tVNetwork],
                venue: [this.production.venue],
            }, { updateOn: 'blur' });
        }
        else {
            this.production = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"]();
            this.basicProductionInfo = this._formBuilder.group({
                name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionTypeId: [{}, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionCompany: [''],
                description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                compensationType: [0],
                compensationContractDetails: [''],
                productionUnionStatus: [0]
            }, { updateOn: 'blur' });
            this.castingDetails = this._formBuilder.group({
                castingDirector: [''],
                castingAssistant: [''],
                castingAssociate: [''],
                assistantCastingAssociate: ['']
            }, { updateOn: 'blur' });
            this.productionDetails = this._formBuilder.group({
                executiveProducer: [''],
                coExecutiveProducer: [''],
                producer: [''],
                coProducer: ['']
            }, { updateOn: 'blur' });
            this.artisticDetails = this._formBuilder.group({
                artisticDirector: [''],
                musicDirector: [''],
                choreographer: ['']
            }, { updateOn: 'blur' });
            this.extraDetails = this._formBuilder.group({
                TVNetwork: [''],
                venue: [''],
            }, { updateOn: 'blur' });
        }
    };
    NewProductionComponent.prototype.ngOnDestroy = function () {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();
        }
        if (this.saveProductionIntentSubscription) {
            this.saveProductionIntentSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    NewProductionComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
        this.productionComponentService.registerProductionScrollElement(this.productionCard);
        this.productionComponentService.registerArtisticScrollElement(this.artisticCard);
        this.productionComponentService.registerCastingScrollElement(this.castingCard);
        this.productionComponentService.registerRolesScrollElement(this.rolesCard);
        this.productionComponentService.registerBasicScrollElement(this.basicCard);
        this.productionComponentService.registerOtherScrollElement(this.otherCard);
        if (this.popRole) {
            if (true) {
                this.stateService.go("draftproductionnewrole", { production: this.production, productionId: this.production.id, draftId: this.production.id });
            }
            else {}
        }
    };
    NewProductionComponent.prototype.moveToProductionView = function (productionId) {
        this.stateService.go('production', { productionId: productionId });
    };
    NewProductionComponent.prototype.moveToProductionsView = function () {
        this.stateService.go('productions');
    };
    NewProductionComponent.prototype.scrollToProduction = function () {
        //  this.basicProductionInfo.reset();
        this.productionCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToProduction();
    };
    NewProductionComponent.prototype.scrollToArtistic = function () {
        //  this.basicProductionInfo.reset();
        this.artisticCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToArtistic();
    };
    NewProductionComponent.prototype.scrollToCasting = function () {
        //   this.basicProductionInfo.reset();
        this.castingCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToCasting();
    };
    NewProductionComponent.prototype.scrollToRoles = function () {
        //    this.basicProductionInfo.reset();
        this.rolesCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToRoles();
    };
    NewProductionComponent.prototype.scrollToBasic = function () {
        //   this.basicProductionInfo.reset();
        this.basicCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToBasic();
    };
    NewProductionComponent.prototype.scrollToOther = function () {
        //   this.basicProductionInfo.reset();
        this.otherCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToOther();
    };
    NewProductionComponent.prototype.deleteProductionClick = function () {
        //   this.basicProductionInfo.reset();
        this.productionComponentService.tryDeleteProduction();
    };
    NewProductionComponent.prototype.cancelEditProductionClick = function () {
        //   this.basicProductionInfo.reset();
        this.moveToProductionsView();
    };
    NewProductionComponent.prototype.getSubmissionObject = function () {
        var production = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["BaseProduction"];
        production.productionCompany = this.basicProductionInfo.get('productionCompany').value;
        production.productionTypeId = this.basicProductionInfo.get('productionTypeId').value;
        production.name = this.basicProductionInfo.get('name').value;
        production.description = this.basicProductionInfo.get('description').value;
        production.compensationType = this.basicProductionInfo.get('compensationType').value;
        production.compensationContractDetails = this.basicProductionInfo.get('compensationContractDetails').value;
        production.productionUnionStatus = this.basicProductionInfo.get('productionUnionStatus').value;
        production.castingDirector = this.castingDetails.get('castingDirector').value;
        production.castingAssistant = this.castingDetails.get('castingAssistant').value;
        production.castingAssociate = this.castingDetails.get('castingAssociate').value;
        production.assistantCastingAssociate = this.castingDetails.get('assistantCastingAssociate').value;
        production.executiveProducer = this.productionDetails.get('executiveProducer').value;
        production.coExecutiveProducer = this.productionDetails.get('coExecutiveProducer').value;
        production.producer = this.productionDetails.get('producer').value;
        production.coProducer = this.productionDetails.get('coProducer').value;
        production.artisticDirector = this.artisticDetails.get('artisticDirector').value;
        production.musicDirector = this.artisticDetails.get('musicDirector').value;
        production.choreographer = this.artisticDetails.get('choreographer').value;
        production.tVNetwork = this.extraDetails.get('TVNetwork').value;
        production.venue = this.extraDetails.get('venue').value;
        return production;
    };
    NewProductionComponent.prototype.getUpdateSubmissionObject = function () {
        var production = Object.assign(this.getSubmissionObject(), { id: this.production.id });
        return production;
    };
    NewProductionComponent.prototype.getNewSubmissionObject = function () {
        var production = Object.assign(this.getSubmissionObject());
        return production;
    };
    NewProductionComponent.prototype.saveProductionClick = function () {
        var _this = this;
        if (this.basicProductionInfo.valid) {
            //this.role = this.basicRoleInfo.value;
            //submissionProduction = Object.assign(this.basicProductionInfo.value, this.castingDetails.value, this.productionDetails.value, this.artisticDetails.value, this.extraDetails.value);
            if (this.production.id && this.production.id !== 0) {
                var submissionProduction = this.getUpdateSubmissionObject();
                debugger;
                this.productionService.saveProduction(this.production.id, submissionProduction).subscribe(function (production) {
                    _this.moveToProductionView(_this.production.id);
                    //this.production = production;
                }, function (error) {
                    console.log("error " + error);
                });
            }
            else {
                var submissionProduction = this.getNewSubmissionObject();
                debugger;
                this.productionService.createProduction(submissionProduction).subscribe(function (production) {
                    _this.production = production;
                    _this.moveToProductionView(_this.production.id);
                }, function (error) {
                    console.log("error " + error);
                });
            }
        }
        else {
            if (!this.basicProductionInfo.get('name').valid) {
                this.basicProductionInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicProductionInfo.get('productionTypeId').valid) {
                this.basicProductionInfo.get('productionTypeId').markAsDirty();
                this.productionTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.productionTypeField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicProductionInfo.get('description').valid) {
                this.basicProductionInfo.get('description').markAsDirty();
                this.descriptionField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.descriptionField.nativeElement.focus();
            }
        }
    };
    NewProductionComponent.prototype.addRoleClick = function () {
        var _this = this;
        // this.basicProductionInfo.reset();
        if (this.production.id && this.production.id !== 0) {
            //setTimeout(() => {
            if (true) {
                this.stateService.go("draftproductionnewrole", { production: this.production, productionId: this.production.id, draftId: this.production.id });
            }
            else {}
            // });
        }
        else {
            var submissionObject = Object.assign(this.basicProductionInfo.value, this.castingDetails.value, this.productionDetails.value, this.artisticDetails.value, this.extraDetails.value);
            this.productionService.createProduction(submissionObject).subscribe(function (production) {
                _this.isLoading = false;
                _this.stateService.go('draftproduction', { production: production, draftId: production.id, popRole: true });
            }, function (error) {
            });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_3__["MetadataModel"])
    ], NewProductionComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"])
    ], NewProductionComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('description'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "descriptionField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('productiontype'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__["Dropdown"])
    ], NewProductionComponent.prototype, "productionTypeField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('productioncard'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "productionCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('othercard'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "otherCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('artisticcard'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "artisticCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('castingcard'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "castingCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('rolescard'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "rolesCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('basiccard'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "basicCard", void 0);
    NewProductionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-production',
            template: __webpack_require__(/*! ./new-production.component.html */ "./src/app/feature-modules/new-production/components/new-production/new-production.component.html"),
            // make fade in animation available to this component
            animations: [_animations_router_animation__WEBPACK_IMPORTED_MODULE_11__["fadeInAnimation"]],
            // attach the fade in animation to the host (root) element of this component
            host: { '[@fadeInAnimation]': '' },
            styles: [__webpack_require__(/*! ./new-production.component.scss */ "./src/app/feature-modules/new-production/components/new-production/new-production.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_role_service__WEBPACK_IMPORTED_MODULE_9__["ProductionRoleService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _services_production_service__WEBPACK_IMPORTED_MODULE_7__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_6__["RoleDialogService"],
            _services_production_component_service__WEBPACK_IMPORTED_MODULE_8__["ProductionComponentService"]])
    ], NewProductionComponent);
    return NewProductionComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/new-role/new-role.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/new-role/new-role.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"p-grid p-dir-col\">\r\n\r\n\r\n\r\n    <p-card header=\"Enter the details about the roles you are casting\" class=\"p-col\">\r\n\r\n        <form [formGroup]=\"basicRoleInfo\">\r\n            <div class=\"p-grid p-dir-col p-justify-around\">\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n\r\n                        <label class=\"p-col\">Role Name</label>\r\n                        <input class=\"p-col\" pInputText #name  placeholder=\"Role Name\" formControlName=\"name\" required>\r\n                        <p-message class=\"p-col\" severity=\"error\" text=\"Role Name is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['name'].hasError('required') && basicRoleInfo.controls['name'].dirty\"></p-message>\r\n                        <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. 'Batman'</div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Role Type</label>\r\n                        <p-dropdown class=\"p-col\" [options]=\"metadata.roleTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" #roletype placeholder=\"Select a Type of Role\" formControlName=\"roleTypeId\" required>\r\n                        </p-dropdown>\r\n                        <p-message class=\"p-col\" severity=\"error\" text=\"Role Type is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['roleTypeId'].hasError('required') && basicRoleInfo.controls['roleTypeId'].dirty\"></p-message>\r\n                        <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Role</div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-align-start\">\r\n\r\n                        <div class=\"p-col-12 p-md-6\">\r\n                            <div class=\"p-grid p-dir-col\">\r\n                                <label class=\"p-col\">Gender (optional)</label>\r\n                                <p-dropdown class=\"p-col\" [options]=\"metadata.roleGenderTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" #gendertype placeholder=\"Select Gender\" formControlName=\"roleGenderTypeId\" required>\r\n                                </p-dropdown>\r\n                                <p-message class=\"p-col\" severity=\"error\" text=\"Gender Type is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['roleGenderTypeId'].hasError('required') && basicRoleInfo.controls['roleGenderTypeId'].dirty\"></p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Gender</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col-12 p-md-6\">\r\n                            <div class=\"p-grid p-align-start switchoffset\">\r\n                                <p-inputSwitch class=\"p-col\" formControlName=\"transgender\"></p-inputSwitch>\r\n                                <label class=\"p-col\">Transgender</label>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Role Description (optional)</label>\r\n                        <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Role Description\" formControlName=\"description\"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\" class=\"p-col\">Age Range (optional): {{minAge}} to {{maxAge}}</label>\r\n                        <p-slider class=\"p-col\" formControlName=\"age\" (onChange)=\"handleAgeSliderChange($event)\" [style]=\"{'width':'100%'}\" [min]=\"0\" [max]=\"100\" [range]=\"true\"></p-slider>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-justify-between p-align-end\">\r\n                                <label>{{minAge}}</label><label>{{maxAge}}</label>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-justify-between\">\r\n                        <p-inputSwitch class=\"p-col\" formControlName=\"requiresNudity\"></p-inputSwitch>\r\n                        <label class=\"p-col\">This role requires nudity</label>\r\n\r\n                    </div>\r\n                </div>\r\n\r\n\r\n            </div>\r\n        </form>\r\n\r\n\r\n\r\n        <div class=\"p-grid p-dir-col p-justify-around\">\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                    <label class=\"p-col\">Languages (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"languages\" [options]=\"metadata.languages\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Languages\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Languages</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">Accents (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"accents\" [options]=\"metadata.accents\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Accents\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Accents</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">StereoTypes (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"stereoTypes\" [options]=\"metadata.stereoTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select StereoTypes\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select StereoTypes</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">Ethnicities (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"ethnicities\" [options]=\"metadata.ethnicities\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Ethnicities\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Ethnicities</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">Ethnic Stereotypes (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"ethnicStereoTypes\" [options]=\"metadata.ethnicStereoTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Ethnic Stereotypes\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Ethnic Stereotypes</div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n        <p-footer class=\"p-grid p-justify-end\">\r\n            <button pButton type=\"button\" label=\"Cancel\" (click)=\"onNoClick()\" class=\"ui-button-secondary\"></button>\r\n            <button pButton type=\"button\" label=\"Save Role\" (click)=\"saveRoleClick()\"></button>\r\n        </p-footer>\r\n\r\n    </p-card>\r\n\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/new-role/new-role.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/new-role/new-role.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mat-dialog-content {\n  overflow: visible !important; }\n\n.role_card {\n  /* margin-left:16px;\r\n  margin-right:16px;*/ }\n\n/*mat-card-header {\r\n  background-color: $primary;\r\n}*/\n\nmat-form-field, .mat-form-field {\n  width: 100%; }\n\nmat-slider {\n  display: block; }\n\n.smallage {\n  width: 90%; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXJvbGUvRTpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xcbmV3LXByb2R1Y3Rpb25cXGNvbXBvbmVudHNcXG5ldy1yb2xlXFxuZXctcm9sZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXJvbGUvbmV3LXJvbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSw0QkFBMkIsRUFBQTs7QUFFL0I7RUFDQztxQkNBb0IsRURDQzs7QUFFdEI7O0VDQ0U7O0FERUY7RUFFRSxXQUFVLEVBQUE7O0FBRVo7RUFDSSxjQUFhLEVBQUE7O0FBRWpCO0VBQ0ksVUFBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXJvbGUvbmV3LXJvbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxubWF0LWRpYWxvZy1jb250ZW50IHtcclxuICAgIG92ZXJmbG93OnZpc2libGUgIWltcG9ydGFudDtcclxufVxyXG4ucm9sZV9jYXJkIHtcclxuIC8qIG1hcmdpbi1sZWZ0OjE2cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OjE2cHg7Ki9cclxufVxyXG4vKm1hdC1jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XHJcbn0qL1xyXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkXHJcbntcclxuICB3aWR0aDoxMDAlO1xyXG59XHJcbm1hdC1zbGlkZXIge1xyXG4gICAgZGlzcGxheTpibG9jaztcclxufVxyXG4uc21hbGxhZ2V7XHJcbiAgICB3aWR0aDo5MCU7XHJcbn0gIiwibWF0LWRpYWxvZy1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDsgfVxuXG4ucm9sZV9jYXJkIHtcbiAgLyogbWFyZ2luLWxlZnQ6MTZweDtcclxuICBtYXJnaW4tcmlnaHQ6MTZweDsqLyB9XG5cbi8qbWF0LWNhcmQtaGVhZGVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeTtcclxufSovXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7IH1cblxubWF0LXNsaWRlciB7XG4gIGRpc3BsYXk6IGJsb2NrOyB9XG5cbi5zbWFsbGFnZSB7XG4gIHdpZHRoOiA5MCU7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/new-role/new-role.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/new-role/new-role.component.ts ***!
  \******************************************************************************************/
/*! exports provided: NewRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRoleComponent", function() { return NewRoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__);








var NewRoleComponent = /** @class */ (function () {
    function NewRoleComponent(_formBuilder, ProductionRoleService, stateService, transitionService, transition) {
        this._formBuilder = _formBuilder;
        this.ProductionRoleService = ProductionRoleService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.minMinAge = 0;
        this.minMaxAge = 49;
        // private minAge: number = 0;
        // private maxAge: number = 50;
        this.maxMinAge = 1;
        this.maxMaxAge = 100;
        this.showHints = false;
        this.age = [0, 100];
        this.minAge = 0;
        this.maxAge = 0;
        this.languages = [];
        this.accents = [];
        this.stereoTypes = [];
        this.ethnicStereoTypes = [];
        this.ethnicities = [];
    }
    NewRoleComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
    };
    NewRoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.role) {
            this.age = [this.role.minAge, this.role.maxAge];
            this.minAge = this.role.minAge;
            this.maxAge = this.role.maxAge;
            this.languages = this.role.languages.map(function (l) { return l.language; });
            this.accents = this.role.accents.map(function (l) { return l.accent; });
            this.ethnicities = this.role.ethnicities.map(function (l) { return l.ethnicity; });
            this.stereoTypes = this.role.stereoTypes.map(function (l) { return l.stereoType; });
            this.ethnicStereoTypes = this.role.ethnicStereoTypes.map(function (l) { return l.ethnicStereoType; });
            this.basicRoleInfo = this._formBuilder.group({
                name: [this.role.name, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleTypeId: [this.metadata.roleTypes[this.metadata.roleTypes.findIndex(function (t) { return t.id == _this.role.roleTypeId; })], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleGenderTypeId: [this.metadata.roleGenderTypes[this.metadata.roleGenderTypes.findIndex(function (t) { return t.id == _this.role.roleGenderTypeId; })], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                transgender: [this.role.transgender],
                age: [this.age],
                //minAge: [this.role.minAge],
                //maxAge: [this.role.maxAge],
                description: [this.role.description],
                //accents: [this.role.accents.map(a => a.accentId)],
                //languages: [this.role.languages],
                //ethnicities: [this.role.ethnicities.map(a => a.ethnicityId)],
                //ethnicStereoTypes: [this.role.ethnicStereoTypes.map(a => a.ethnicStereoTypeId)],
                //stereoTypes: [this.role.stereoTypes.map(a => a.stereoTypeId)],
                requiresNudity: [this.role.requiresNudity],
            }, { updateOn: 'blur' });
        }
        else {
            this.role = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"];
            this.basicRoleInfo = this._formBuilder.group({
                name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleTypeId: [{}, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleGenderTypeId: [{}, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                transgender: [false],
                age: [this.age],
                //minAge: [0],
                //maxAge: [100],
                description: [''],
                //accents: [[]],
                //languages: [[]],
                //ethnicities: [[]],
                //ethnicStereoTypes: [[]],
                // stereoTypes: [[]],
                requiresNudity: [false],
            }, { updateOn: 'blur' });
        }
        /* this.basicRoleInfo.get('requiresCover').valueChanges.subscribe(
 (value) => {
});*/
    };
    NewRoleComponent.prototype.onNoClick = function () {
        this.stateService.go(this.transition.from());
        //this.dialogRef.close();
    };
    NewRoleComponent.prototype.handleAgeSliderChange = function (e) {
        this.minAge = e.values[0];
        this.maxAge = e.values[1];
    };
    NewRoleComponent.prototype.onMinSliderChange = function ($event) {
        this.minAge = $event.value;
        this.maxMaxAge = this.minAge + 1;
        this.basicRoleInfo.get('minAge').setValue($event.value);
    };
    NewRoleComponent.prototype.onMaxSliderChange = function ($event) {
        this.maxAge = $event.value;
        this.minMaxAge = this.maxAge - 1;
        this.basicRoleInfo.get('maxAge').setValue($event.value);
    };
    NewRoleComponent.prototype.getSubmissionObject = function () {
        var role = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"];
        role.productionId = this.production.id;
        role.name = this.basicRoleInfo.get('name').value;
        role.transgender = this.basicRoleInfo.get('transgender').value;
        role.minAge = this.basicRoleInfo.get('age').value[0];
        role.maxAge = this.basicRoleInfo.get('age').value[1];
        role.description = this.basicRoleInfo.get('description').value;
        role.languages = this.languages.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.accents = this.accents.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.ethnicities = this.ethnicities.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.ethnicStereoTypes = this.ethnicStereoTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.stereoTypes = this.stereoTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        //role.accents = this.basicRoleInfo.get('accents').value;
        // role.ethnicities= this.basicRoleInfo.get('ethnicities').value;
        //  role.ethnicStereoTypes= this.basicRoleInfo.get('ethnicStereoTypes').value;
        //  role.stereoTypes= this.basicRoleInfo.get('stereoTypes').value;
        role.requiresNudity = this.basicRoleInfo.get('requiresNudity').value;
        role.roleTypeId = this.basicRoleInfo.get('roleTypeId').value.id;
        role.roleGenderTypeId = this.basicRoleInfo.get('roleGenderTypeId').value.id;
        //this.role = this.basicRoleInfo.value;
        return role;
    };
    NewRoleComponent.prototype.saveRoleClick = function () {
        var _this = this;
        if (this.basicRoleInfo.valid) {
            var submissionRole = this.getSubmissionObject();
            if (this.role && this.role.id && this.role.id !== 0) {
                this.ProductionRoleService.saveRole(this.role.id, this.production.id, submissionRole).subscribe(function (role) {
                    _this.production.roles[_this.production.roles.findIndex(function (r) { return r.id == role.id; })] = role;
                    var params = {
                        productionId: _this.production.id,
                        production: _this.production,
                        draftId: _this.production.id
                    };
                    if (_this.transition.from().name != "draftproduction" &&
                        _this.transition.from().name != "editproduction") {
                        _this.stateService.go("production", params);
                    }
                    else {
                        _this.stateService.go(_this.transition.from(), params);
                    }
                }, function (error) {
                    console.log("error " + error);
                });
            }
            else {
                this.ProductionRoleService.createRole(this.production.id, submissionRole).subscribe(function (role) {
                    if (role != null) {
                        _this.production.roles.push(role);
                    }
                    var params = {
                        productionId: _this.production.id,
                        production: _this.production,
                        draftId: _this.production.id
                    };
                    if (_this.transition.from().name != "draftproduction" &&
                        _this.transition.from().name != "editproduction") {
                        _this.stateService.go("production", params);
                    }
                    else {
                        _this.stateService.go(_this.transition.from(), params);
                    }
                }, function (error) {
                    console.log("error " + error);
                });
            }
        }
        else {
            if (!this.basicRoleInfo.get('name').valid) {
                this.basicRoleInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicRoleInfo.get('roleTypeId').valid) {
                this.basicRoleInfo.get('roleTypeId').markAsDirty();
                this.roleTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.roleTypeField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicRoleInfo.get('roleGenderTypeId').valid) {
                this.basicRoleInfo.get('roleGenderTypeId').markAsDirty();
                this.genderTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.genderTypeField.focusViewChild.nativeElement.focus();
            }
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"])
    ], NewRoleComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_3__["MetadataModel"])
    ], NewRoleComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"])
    ], NewRoleComponent.prototype, "role", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewRoleComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roletype'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__["Dropdown"])
    ], NewRoleComponent.prototype, "roleTypeField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('gendertype'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_7__["Dropdown"])
    ], NewRoleComponent.prototype, "genderTypeField", void 0);
    NewRoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-role',
            template: __webpack_require__(/*! ./new-role.component.html */ "./src/app/feature-modules/new-production/components/new-role/new-role.component.html"),
            styles: [__webpack_require__(/*! ./new-role.component.scss */ "./src/app/feature-modules/new-production/components/new-role/new-role.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"]])
    ], NewRoleComponent);
    return NewRoleComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.html":
/*!********************************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.html ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-card header=\"{{production.name}}\">\r\n\r\n    <div class=\"p-grid p-dir-col p-justify-between\">\r\n        <div class=\"p-col\">\r\n            <div class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>Production Type:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.productionType.name}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>Role Count:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.roles.length}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <div class=\"p-col\">\r\n            <div class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>CastingCall Count:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.castingCalls.length}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>Casting Session Count:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.castingSessions.length}}</span>\r\n                         </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <p-footer>\r\n        <a uiSref=\"production\" [uiParams]=\"{ productionId:production.id }\">{{production.name}}</a>\r\n        <a pButton uiSref=\"editproduction\" [uiParams]=\"{ productionId:production.id }\">Edit</a>\r\n        <a pButton uiSref=\"production\" [uiParams]=\"{ productionId:production.id }\">View</a>\r\n    </p-footer>\r\n</p-card>\r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.scss":
/*!********************************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.scss ***!
  \********************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9uZXctcHJvZHVjdGlvbi9jb21wb25lbnRzL3Byb2R1Y3Rpb24tbGlzdC1pdGVtL3Byb2R1Y3Rpb24tbGlzdC1pdGVtLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.ts":
/*!******************************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.ts ***!
  \******************************************************************************************************************/
/*! exports provided: ProductionListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionListItemComponent", function() { return ProductionListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");



var ProductionListItemComponent = /** @class */ (function () {
    function ProductionListItemComponent() {
    }
    ProductionListItemComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["Production"])
    ], ProductionListItemComponent.prototype, "production", void 0);
    ProductionListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-production-list-item',
            template: __webpack_require__(/*! ./production-list-item.component.html */ "./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.html"),
            host: { 'class': 'p-col' },
            styles: [__webpack_require__(/*! ./production-list-item.component.scss */ "./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductionListItemComponent);
    return ProductionListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production-list/production-list.component.html":
/*!**********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production-list/production-list.component.html ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n                <app-production-list-item *ngFor=\"let production of productions\" [production]=\"production\">\r\n              </app-production-list-item>\r\n\r\n           \r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production-list/production-list.component.scss":
/*!**********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production-list/production-list.component.scss ***!
  \**********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".production_list_wrapper {\n  height: 400px; }\n\n.production_list {\n  padding: 8px 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi1saXN0L0U6XFx3b3JrXFxjYXN0aW5nXFxDYXN0aW5nXFxBbmd1bGFyXFx0YWxlbnQtYXBwL3NyY1xcYXBwXFxmZWF0dXJlLW1vZHVsZXNcXG5ldy1wcm9kdWN0aW9uXFxjb21wb25lbnRzXFxwcm9kdWN0aW9uLWxpc3RcXHByb2R1Y3Rpb24tbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQVksRUFBQTs7QUFFaEI7RUFDSSxjQUFhLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9mZWF0dXJlLW1vZHVsZXMvbmV3LXByb2R1Y3Rpb24vY29tcG9uZW50cy9wcm9kdWN0aW9uLWxpc3QvcHJvZHVjdGlvbi1saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnByb2R1Y3Rpb25fbGlzdF93cmFwcGVye1xyXG4gICAgaGVpZ2h0OjQwMHB4O1xyXG59XHJcbi5wcm9kdWN0aW9uX2xpc3R7XHJcbiAgICBwYWRkaW5nOjhweCAwO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production-list/production-list.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production-list/production-list.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: ProductionListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionListComponent", function() { return ProductionListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");




var ProductionListComponent = /** @class */ (function () {
    function ProductionListComponent(
    // private metadataService: MetadataService,
    productionService, stateService, transitionService, transition) {
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [5, 10, 25, 100];
    }
    ProductionListComponent.prototype.ngOnInit = function () {
    };
    ProductionListComponent.prototype.changePageSizeOption = function ($event) {
        var _this = this;
        this.length = $event.length;
        this.pageSize = $event.pageSize;
        this.length = $event.length;
        this.productionService.getProductions($event.pageIndex, $event.pageSize)
            .subscribe(function (productions) {
            _this.productions = productions;
        });
    };
    ProductionListComponent.prototype.createNewProduction = function () {
        this.stateService.go('newproduction');
        //this.productionComponentService.scrollToProduction();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductionListComponent.prototype, "productions", void 0);
    ProductionListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-production-list',
            template: __webpack_require__(/*! ./production-list.component.html */ "./src/app/feature-modules/new-production/components/production-list/production-list.component.html"),
            host: { 'class': 'p-grid p-dir-col' },
            styles: [__webpack_require__(/*! ./production-list.component.scss */ "./src/app/feature-modules/new-production/components/production-list/production-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_service__WEBPACK_IMPORTED_MODULE_3__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"]])
    ], ProductionListComponent);
    return ProductionListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production/production.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production/production.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-sides-dialog [side]=side [productionId]=production.id></app-sides-dialog>\r\n<app-audition-details-dialog [productionId]=production.id></app-audition-details-dialog>\r\n<app-shoot-details-dialog [productionId]=production.id></app-shoot-details-dialog>\r\n<app-pay-details-dialog [productionId]=production.id></app-pay-details-dialog>\r\n\r\n<div class=\"p-grid p-dir-col p-justify-between\">\r\n    <div class=\"p-col\">\r\n\r\n        <div class=\"p-grid p-align-start\">\r\n            <div class=\"p-col-6\">\r\n                <span>Production:</span>\r\n            </div>\r\n            <div class=\"p-col-6\">\r\n                <span class=\"valuelabel\">{{production.name}}</span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n\r\n    <div class=\"p-col\">\r\n        <div class=\"p-grid p-justify-around p-align-start\">\r\n            <div class=\"p-col-6\">\r\n                <div class=\"p-grid p-dir-col\">\r\n\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" toggler=\"header\" header=\"Roles ({{production.roles.length}})\">\r\n                        <p-header>\r\n                            <div class=\"ui-helper-clearfix\">\r\n                                <p-button [style]=\"{'float':'right'}\" label=\"New Role\" (click)=\"createNewRole()\"></p-button>\r\n                            </div>\r\n                        </p-header>\r\n                        <p-scrollPanel [style]=\"{'width': '100%', 'max-height': '400px'}\">\r\n                            <app-role-list [production]=\"production\" [roles]=\"production.roles\">\r\n                            </app-role-list>\r\n                        </p-scrollPanel>\r\n                        <p-footer>\r\n                            <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addSide()\" style=\"margin-right: .25em\"></button>\r\n                            <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n\r\n\r\n\r\n\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col-3\">\r\n                <div class=\"p-grid p-dir-col\">\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"CastingCalls ({{production.castingCalls.length}})\">\r\n\r\n                        <p-footer>\r\n                            <a pButton uiSref=\"newcastingCall\" icon=\"pi pi-plus\" label=\"New\" [uiParams]=\"{ productionId:production.id, production:production }\" class=\"ui-button-info\"></a>\r\n                            <a pButton uiSref=\"productioncastingCalls\" [uiParams]=\"{ productionId:production.id }\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></a>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Casting Sessions ({{production.castingSessions.length}})\">\r\n                        <p-footer>\r\n                            <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addSide()\" style=\"margin-right: .25em\"></button>\r\n                            <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Sides ({{production.sides.length}})\">\r\n                        <div class=\"p-grid p-dir-col p-justify-around\">\r\n                                 <p-card class=\"p-col\" *ngFor=\"let detail of production.sides\">\r\n                                   <div class=\"p-grid p-dir-col\">\r\n                                     <div class=\"p-col\">\r\n                                       <a (click)=\"viewSides(detail)\">{{detail.name}}</a>\r\n                                     </div>\r\n                                   </div>\r\n                                   <p-footer>\r\n                                     <a class=\"pi pi-pencil\"></a>\r\n                                     <a class=\"pi pi-trash\"></a>\r\n                                   </p-footer>\r\n\r\n                                </p-card>\r\n                         </div>\r\n                        <p-footer>\r\n                            <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addSide()\" style=\"margin-right: .25em\"></button>\r\n                            <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n\r\n\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col-3\">\r\n\r\n                <div class=\"p-grid p-dir-col\">\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Shoot Details ({{production.shootDetails.length}})\">\r\n\r\n                        <div class=\"p-grid p-dir-col p-justify-around\">\r\n                            <p-card class=\"p-col\" *ngFor=\"let detail of production.shootDetails\">\r\n                                <div class=\"p-grid p-dir-col\">\r\n                                    <div class=\"p-col\">\r\n                                        <a (click)=\"viewShootDetails(detail)\">{{detail.name}}</a>\r\n                                    </div>\r\n                                    <div class=\"p-col\">\r\n                                        <div class=\"p-grid p-align-start\">\r\n                                            <div class=\"p-col\">Location</div>\r\n                                            <div class=\"p-col\">{{detail.location}}</div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"p-col\">\r\n                                        <div class=\"p-grid p-align-start\">\r\n                                            <div class=\"p-col\">{{detail.startDate}}</div>\r\n                                            <div class=\"p-col\">{{detail.endDate}}</div>\r\n                                        </div>\r\n                                    </div>\r\n                                       </div>\r\n                              <p-footer>\r\n                                <a class=\"pi pi-pencil\"></a>\r\n                                <a class=\"pi pi-trash\"></a>\r\n                              </p-footer>\r\n                            </p-card>\r\n                        </div>\r\n                        <p-footer>\r\n                            <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addShootDetails()\" style=\"margin-right: .25em\"></button>\r\n                            <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Audition Details ({{production.auditionDetails.length}})\">\r\n                        <div class=\"p-grid p-dir-col p-justify-around\">\r\n                            <p-card class=\"p-col\" *ngFor=\"let detail of production.auditionDetails\">\r\n                                <div class=\"p-grid p-dir-col\">\r\n                                    <div class=\"p-col\">\r\n                                        <a (click)=\"viewAuditionDetails(detail)\">{{detail.name}}</a>\r\n                                    </div>\r\n                                    <div class=\"p-col\">\r\n                                        <div class=\"p-grid p-align-start\">\r\n                                            <div class=\"p-col\">Location</div>\r\n                                            <div class=\"p-col\">{{detail.location}}</div>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"p-col\">\r\n                                        <div class=\"p-grid p-align-start\">\r\n                                            <div class=\"p-col\">{{detail.startDate}}</div>\r\n                                            <div class=\"p-col\">{{detail.endDate}}</div>\r\n                                        </div>\r\n                                    </div>\r\n                                        </div>\r\n                              <p-footer>\r\n                                <a class=\"pi pi-pencil\"></a>\r\n                                <a class=\"pi pi-trash\"></a>\r\n                              </p-footer>\r\n                            </p-card>\r\n                        </div>\r\n                        <p-footer>\r\n                            <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addAuditionDetails()\" style=\"margin-right: .25em\"></button>\r\n                            <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n                    <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Pay Details ({{production.payDetails.length}})\">\r\n                        <div class=\"p-grid p-dir-col p-justify-around\">\r\n                            <p-card class=\"p-col\" *ngFor=\"let detail of production.payDetails\">\r\n                              <div class=\"p-grid p-dir-col\">\r\n                              <div class=\"p-col\">\r\n                                <a (click)=\"viewPayDetails(detail)\">{{detail.name}}</a>\r\n                                </div>\r\n                                    </div>\r\n                              <p-footer>\r\n                                <a class=\"pi pi-pencil\"></a>\r\n                                <a class=\"pi pi-trash\"></a>\r\n                              </p-footer>\r\n                            </p-card>\r\n                        </div>\r\n                        <p-footer>\r\n                            <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addPayDetails()\" style=\"margin-right: .25em\"></button>\r\n                            <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n                        </p-footer>\r\n                    </p-panel>\r\n\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production/production.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production/production.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".production_wrapper {\n  height: 400px; }\n\n.production {\n  padding: 8px 0; }\n\n.role_list_wrapper {\n  height: 400px;\n  width: 100%; }\n\n.role_list {\n  padding: 8px 0; }\n\n.bobo {\n  background-color: red;\n  color: red; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi9FOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcZmVhdHVyZS1tb2R1bGVzXFxuZXctcHJvZHVjdGlvblxcY29tcG9uZW50c1xccHJvZHVjdGlvblxccHJvZHVjdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQVksRUFBQTs7QUFFaEI7RUFDSSxjQUFhLEVBQUE7O0FBR2pCO0VBQ0ksYUFBWTtFQUNaLFdBQVUsRUFBQTs7QUFFZDtFQUNJLGNBQWEsRUFBQTs7QUFHakI7RUFDSSxxQkFBb0I7RUFDcEIsVUFBUyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi9wcm9kdWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnByb2R1Y3Rpb25fd3JhcHBlcntcclxuICAgIGhlaWdodDo0MDBweDtcclxufVxyXG4ucHJvZHVjdGlvbntcclxuICAgIHBhZGRpbmc6OHB4IDA7XHJcbn1cclxuXHJcbi5yb2xlX2xpc3Rfd3JhcHBlcntcclxuICAgIGhlaWdodDo0MDBweDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbn1cclxuLnJvbGVfbGlzdHtcclxuICAgIHBhZGRpbmc6OHB4IDA7XHJcbn1cclxuXHJcbi5ib2JvIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6cmVkO1xyXG4gICAgY29sb3I6cmVkO1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/production/production.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/production/production.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: ProductionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionComponent", function() { return ProductionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/new-production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../services/production-dialog.service */ "./src/app/services/production-dialog.service.ts");








//import { MatDialog } from '@angular/material';



var ProductionComponent = /** @class */ (function () {
    function ProductionComponent(ProductionRoleService, productionExtraService, metadataService, productionService, stateService, transitionService, transition, roleDialogService, productionDialogService) {
        this.ProductionRoleService = ProductionRoleService;
        this.productionExtraService = productionExtraService;
        this.metadataService = metadataService;
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.roleDialogService = roleDialogService;
        this.productionDialogService = productionDialogService;
        this.meta = new _models_metadata__WEBPACK_IMPORTED_MODULE_4__["MetadataModel"]();
    }
    ProductionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deleteRoleSubscription = this.ProductionRoleService.subscribeToDeleteRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles.splice(_this.production.roles.indexOf(role), 1);
            }
        });
        this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles[_this.production.roles.indexOf(role)] = role;
            }
        });
        this.editShootDetailSubscription = this.productionExtraService.subscribeToEditShootDetailObs()
            .subscribe(function (item) {
            if (item) {
                _this.production.shootDetails[_this.production.shootDetails.indexOf(_this.production.shootDetails.find(function (d) { return d.id == item.id; }))] = item;
            }
        });
        this.newShootDetailSubscription = this.productionExtraService.subscribeToCreateShootDetailObs()
            .subscribe(function (item) {
            if (item) {
                _this.production.shootDetails.push(item);
            }
        });
    };
    ProductionComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    ProductionComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (false) {}
    };
    ProductionComponent.prototype.addSide = function () {
        this.productionDialogService.showSidesDialog(null);
    };
    ProductionComponent.prototype.addAuditionDetails = function () {
        this.productionDialogService.showAuditionDetailsDialog(null);
    };
    ProductionComponent.prototype.addShootDetails = function () {
        this.productionDialogService.showShootDetailsDialog(null);
    };
    ProductionComponent.prototype.addPayDetails = function () {
        this.productionDialogService.showPayDetailsDialog(null);
    };
    ProductionComponent.prototype.viewShootDetails = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showShootDetailsDialog(detail);
    };
    ProductionComponent.prototype.viewAuditionDetails = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showAuditionDetailsDialog(detail);
    };
    ProductionComponent.prototype.viewPayDetails = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showPayDetailsDialog(detail);
    };
    ProductionComponent.prototype.viewSides = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showSidesDialog(detail);
    };
    ProductionComponent.prototype.createNewProduction = function () {
    };
    ProductionComponent.prototype.createNewRole = function () {
        var _this = this;
        if (true) {
            this.stateService.go("productionnewrole", { production: this.production, productionId: this.production.id });
        }
        else {}
    };
    ProductionComponent.prototype.createNewCastingCall = function () {
        this.stateService.go("newcastingCall", { productionId: this.production.id });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_5__["Production"])
    ], ProductionComponent.prototype, "production", void 0);
    ProductionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-production',
            template: __webpack_require__(/*! ./production.component.html */ "./src/app/feature-modules/new-production/components/production/production.component.html"),
            styles: [__webpack_require__(/*! ./production.component.scss */ "./src/app/feature-modules/new-production/components/production/production.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_role_service__WEBPACK_IMPORTED_MODULE_8__["ProductionRoleService"], _services_production_extra_service__WEBPACK_IMPORTED_MODULE_7__["ProductionExtraService"], _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"], _services_production_service__WEBPACK_IMPORTED_MODULE_6__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_9__["RoleDialogService"],
            _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_10__["ProductionDialogService"]])
    ], ProductionComponent);
    return ProductionComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/productions/productions.component.html":
/*!**************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/productions/productions.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"p-grid p-justify-center\">\r\n    <div class=\"p-col-9\">\r\n       <!-- <div fxLayout=\"column\">\r\n            <mat-card fxFlex class=\"production_card\">\r\n                <mat-card-header>\r\n                    <mat-card-title>Your Projects</mat-card-title>\r\n                </mat-card-header>\r\n                <mat-card-content>\r\n                    <div fxLayout=\"column\">\r\n                        <mat-paginator [length]=\"length\"\r\n                                       [pageSize]=\"pageSize\"\r\n                                       [pageSizeOptions]=\"pageSizeOptions\"\r\n                                       (page)=\"changePageSizeOption($event)\">\r\n                        </mat-paginator>\r\n                        <div fxFlex=\"1 1 auto\" fxLayout=\"column\" class=\"production_list_wrapper scroll_me_wrapper\" fxLayoutAlign=\"start stretch\">\r\n                            <div fxFlex=\"1 1 auto\" class=\"production_list scroll_me\">-->\r\n                                <app-production-list [productions]=\"productions\">\r\n                                </app-production-list>\r\n    <!--    </div>\r\n                        </div>\r\n                        <mat-paginator [length]=\"length\"\r\n                                       [pageSize]=\"pageSize\"\r\n                                       [pageSizeOptions]=\"pageSizeOptions\"\r\n                                       (page)=\"changePageSizeOption($event)\">\r\n                        </mat-paginator>\r\n\r\n                    </div>\r\n                </mat-card-content>\r\n            </mat-card>\r\n\r\n        </div>-->\r\n    </div>\r\n    <div class=\"p-col-3\">\r\n        <div class=\"p-grid\">\r\n\r\n            <button class=\"p-col\" pButton label=\"Create New Project\" (click)=\"createNewProduction()\"></button>\r\n            <button class=\"p-col\" pButton label=\"go Roles\"></button>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/productions/productions.component.scss":
/*!**************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/productions/productions.component.scss ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9uZXctcHJvZHVjdGlvbi9jb21wb25lbnRzL3Byb2R1Y3Rpb25zL3Byb2R1Y3Rpb25zLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/productions/productions.component.ts":
/*!************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/productions/productions.component.ts ***!
  \************************************************************************************************/
/*! exports provided: ProductionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionsComponent", function() { return ProductionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/production-component.service */ "./src/app/feature-modules/new-production/services/production-component.service.ts");
/* harmony import */ var _animations_router_animation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../animations/router.animation */ "./src/app/animations/router.animation.ts");







var ProductionsComponent = /** @class */ (function () {
    function ProductionsComponent(metadataService, productionService, stateService, transitionService, transition, productionComponentService) {
        this.metadataService = metadataService;
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.productionComponentService = productionComponentService;
    }
    ProductionsComponent.prototype.ngOnInit = function () {
    };
    ProductionsComponent.prototype.createNewProduction = function () {
        this.stateService.go('newproduction');
        //this.productionComponentService.scrollToProduction();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductionsComponent.prototype, "productions", void 0);
    ProductionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-productions',
            template: __webpack_require__(/*! ./productions.component.html */ "./src/app/feature-modules/new-production/components/productions/productions.component.html"),
            // make fade in animation available to this component
            animations: [_animations_router_animation__WEBPACK_IMPORTED_MODULE_6__["fadeInAnimation"]],
            // attach the fade in animation to the host (root) element of this component
            host: { '[@fadeInAnimation]': '' },
            styles: [__webpack_require__(/*! ./productions.component.scss */ "./src/app/feature-modules/new-production/components/productions/productions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"],
            _services_production_service__WEBPACK_IMPORTED_MODULE_4__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"],
            _services_production_component_service__WEBPACK_IMPORTED_MODULE_5__["ProductionComponentService"]])
    ], ProductionsComponent);
    return ProductionsComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.html":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.html ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p-card header=\"{{role.name}}\">\r\n\r\n    <div class=\"p-grid p-dir-col p-justify-between\">\r\n        <div class=\"p-col\" *ngIf=\"role.roleGenderTypeId !== 0 || role.roleTypeId !== 0 || role.requiresNudity || role.transgender\">\r\n           <!-- <mat-chip-list fxLayout=\"row\" fxLayoutAlign=\"end\">\r\n                <mat-chip *ngIf=\"role.requiresNudity\" color=\"warn\" selected=\"true\">Requires Nudity</mat-chip>\r\n                <mat-chip *ngIf=\"role.transgender\" color=\"accent\" selected=\"true\">Transgendered</mat-chip>\r\n                <mat-chip *ngIf=\"role.roleGenderTypeId !== 0\" color=\"primary\" selected=\"true\">{{role.roleGenderType.name}}</mat-chip>\r\n                <mat-chip *ngIf=\"role.roleTypeId !== 0\" color=\"primary\" selected=\"true\">{{role.roleType.name}}</mat-chip>\r\n            </mat-chip-list>-->\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.languages.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Languages:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let language of role.languages; last as islast\">{{language.language.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.accents.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Accents:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let accent of role.accents; last as islast\">{{accent.accent.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.ethnicities.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Ethicity:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let ethnicity of role.ethnicities; last as islast\">{{ethnicity.ethnicity.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.stereoTypes.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Stereotypes:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let stereoType of role.stereoTypes; last as islast\">{{stereoType.stereoType.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.ethnicStereoTypes.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Ethic Stereotypes:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let ethnicStereoType of role.ethnicStereoTypes; last as islast\">{{ethnicStereoType.ethnicStereoType.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <p-footer>\r\n        <button pButton (click)=\"deleteRoleClick(role)\" class=\"ui-button-secondary\" label=\"Delete Role\"></button>\r\n        <button pButton (click)=\"editRoleClick(role)\" label=\"Edit Role\"></button>\r\n    </p-footer>\r\n</p-card>\r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.scss ***!
  \********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9uZXctcHJvZHVjdGlvbi9jb21wb25lbnRzL3JvbGUtbGlzdC1pdGVtL3JvbGUtbGlzdC1pdGVtLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: RoleListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleListItemComponent", function() { return RoleListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/new-production/services/role-dialog.service.ts");
/* harmony import */ var _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/confirmation-dialog.service */ "./src/app/services/confirmation-dialog.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");







var RoleListItemComponent = /** @class */ (function () {
    function RoleListItemComponent(ProductionRoleService, confirmationService, roleDialogService, stateService, transitionService, transition) {
        this.ProductionRoleService = ProductionRoleService;
        this.confirmationService = confirmationService;
        this.roleDialogService = roleDialogService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
    }
    RoleListItemComponent.prototype.ngOnInit = function () {
    };
    RoleListItemComponent.prototype.ngOnDestroy = function () {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    RoleListItemComponent.prototype.editRoleClick = function (role) {
        var _this = this;
        if (true) {
            this.stateService.go("editrole", { production: this.production, productionId: this.production.id, role: role, roleId: role.id });
        }
        else {}
    };
    RoleListItemComponent.prototype.deleteRoleClick = function (role) {
        var _this = this;
        //setTimeout(() => {
        this.deleteRoleSubscription = this.confirmationService.confirmAction("Do you really want to delete role '" + this.role.name + "' from production '" + this.production.name + "'?")
            .subscribe(function (result) {
            if (result) {
                _this.ProductionRoleService.deleteRole(_this.role.id, _this.role.productionId).subscribe(function (role) {
                }, function (error) {
                    console.log("error " + error);
                });
            }
            _this.deleteRoleSubscription.unsubscribe();
        });
        //});
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_3__["Production"])
    ], RoleListItemComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], RoleListItemComponent.prototype, "role", void 0);
    RoleListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-list-item',
            template: __webpack_require__(/*! ./role-list-item.component.html */ "./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.html"),
            host: { 'class': 'p-col' },
            styles: [__webpack_require__(/*! ./role-list-item.component.scss */ "./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"], _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmationDialogService"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_4__["RoleDialogService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"]])
    ], RoleListItemComponent);
    return RoleListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/role-list/role-list.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/role-list/role-list.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <app-role-list-item *ngFor=\"let role of production.roles\" [role]=\"role\" [production]=\"production\">\r\n\r\n    </app-role-list-item>\r\n \r\n"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/role-list/role-list.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/role-list/role-list.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9uZXctcHJvZHVjdGlvbi9jb21wb25lbnRzL3JvbGUtbGlzdC9yb2xlLWxpc3QuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/role-list/role-list.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/role-list/role-list.component.ts ***!
  \********************************************************************************************/
/*! exports provided: RoleListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleListComponent", function() { return RoleListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/new-production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");





var RoleListComponent = /** @class */ (function () {
    function RoleListComponent(productionService, roleDialogService) {
        this.productionService = productionService;
        this.roleDialogService = roleDialogService;
        this.roles = [];
    }
    RoleListComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["Production"])
    ], RoleListComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], RoleListComponent.prototype, "roles", void 0);
    RoleListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-list',
            template: __webpack_require__(/*! ./role-list.component.html */ "./src/app/feature-modules/new-production/components/role-list/role-list.component.html"),
            host: { 'class': 'p-grid p-dir-col' },
            styles: [__webpack_require__(/*! ./role-list.component.scss */ "./src/app/feature-modules/new-production/components/role-list/role-list.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_service__WEBPACK_IMPORTED_MODULE_4__["ProductionService"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_3__["RoleDialogService"]])
    ], RoleListComponent);
    return RoleListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.html":
/*!************************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"column\" fxLayoutAlign=\"start stretch\">\r\n  <button mat-raised-button (click)=\"scrollToBasic()\">go Basic</button>\r\n  <button mat-raised-button (click)=\"scrollToRoles()\">go Roles</button>\r\n  <button mat-raised-button (click)=\"scrollToCasting()\">go Casting</button>\r\n  <button mat-raised-button (click)=\"scrollToProduction()\">go Production</button>\r\n  <button mat-raised-button (click)=\"scrollToArtistic()\">go Artistic</button>\r\n  <button mat-raised-button (click)=\"scrollToOther()\">go Other</button>\r\n  <button mat-raised-button (click)=\"saveProductionClick()\">Save</button>\r\n  <button mat-raised-button (click)=\"deleteProductionClick()\">Delete</button>\r\n  <button mat-raised-button (click)=\"cancelEditProductionClick()\">Cancel</button>\r\n\r\n     \r\n</div>"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.scss":
/*!************************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.scss ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".fixed {\n  position: fixed;\n  top: 40px;\n  z-index: 999; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2lkZS1xdWljay1saW5rcy9FOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcZmVhdHVyZS1tb2R1bGVzXFxuZXctcHJvZHVjdGlvblxcY29tcG9uZW50c1xcc2lkZS1xdWljay1saW5rc1xcc2lkZS1xdWljay1saW5rcy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQWU7RUFDZixTQUFTO0VBQ1QsWUFBWSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL25ldy1wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2lkZS1xdWljay1saW5rcy9zaWRlLXF1aWNrLWxpbmtzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZpeGVkIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgdG9wOiA0MHB4O1xyXG4gIHotaW5kZXg6IDk5OTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: SideQuickLinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideQuickLinksComponent", function() { return SideQuickLinksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/production-component.service */ "./src/app/feature-modules/new-production/services/production-component.service.ts");



var SideQuickLinksComponent = /** @class */ (function () {
    function SideQuickLinksComponent(productionComponentService) {
        this.productionComponentService = productionComponentService;
        this.fixed = false;
    }
    SideQuickLinksComponent.prototype.onWindowScroll = function ($event) {
        /* let top = window.scrollY;
         if (top > 50) {
           this.fixed = true;
           console.log(top);
         } else if (this.fixed && top < 5) {
           this.fixed = false;
           console.log(top);
         }*/
        // $event.stopPropagation();
    };
    SideQuickLinksComponent.prototype.ngOnInit = function () {
    };
    SideQuickLinksComponent.prototype.scrollToProduction = function () {
        this.productionComponentService.scrollToProduction();
    };
    SideQuickLinksComponent.prototype.scrollToArtistic = function () {
        this.productionComponentService.scrollToArtistic();
    };
    SideQuickLinksComponent.prototype.scrollToCasting = function () {
        this.productionComponentService.scrollToCasting();
    };
    SideQuickLinksComponent.prototype.scrollToRoles = function () {
        this.productionComponentService.scrollToRoles();
    };
    SideQuickLinksComponent.prototype.scrollToBasic = function () {
        this.productionComponentService.scrollToBasic();
    };
    SideQuickLinksComponent.prototype.scrollToOther = function () {
        this.productionComponentService.scrollToOther();
    };
    SideQuickLinksComponent.prototype.saveProductionClick = function () {
        this.productionComponentService.trySaveProduction();
    };
    SideQuickLinksComponent.prototype.deleteProductionClick = function () {
        this.productionComponentService.tryDeleteProduction();
    };
    SideQuickLinksComponent.prototype.cancelEditProductionClick = function () {
        this.productionComponentService.tryCancelEditProduction();
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("window:scroll", ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SideQuickLinksComponent.prototype, "onWindowScroll", null);
    SideQuickLinksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-side-quick-links',
            template: __webpack_require__(/*! ./side-quick-links.component.html */ "./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.html"),
            styles: [__webpack_require__(/*! ./side-quick-links.component.scss */ "./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_component_service__WEBPACK_IMPORTED_MODULE_2__["ProductionComponentService"]])
    ], SideQuickLinksComponent);
    return SideQuickLinksComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/new-production.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/new-production.module.ts ***!
  \*************************************************************************/
/*! exports provided: NewProductionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProductionModule", function() { return NewProductionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../interceptors/errorInterceptor */ "./src/app/interceptors/errorInterceptor.ts");
/* harmony import */ var _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../interceptors/jwtInterceptor */ "./src/app/interceptors/jwtInterceptor.ts");
/* harmony import */ var _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/new-production/new-production.component */ "./src/app/feature-modules/new-production/components/new-production/new-production.component.ts");
/* harmony import */ var _components_production_production_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/production/production.component */ "./src/app/feature-modules/new-production/components/production/production.component.ts");
/* harmony import */ var _new_production_states__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./new-production.states */ "./src/app/feature-modules/new-production/new-production.states.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/role-dialog.service */ "./src/app/feature-modules/new-production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/production-component.service */ "./src/app/feature-modules/new-production/services/production-component.service.ts");
/* harmony import */ var _components_add_role_add_role_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/add-role/add-role.component */ "./src/app/feature-modules/new-production/components/add-role/add-role.component.ts");
/* harmony import */ var _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/new-role/new-role.component */ "./src/app/feature-modules/new-production/components/new-role/new-role.component.ts");
/* harmony import */ var _components_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/role-list/role-list.component */ "./src/app/feature-modules/new-production/components/role-list/role-list.component.ts");
/* harmony import */ var _components_role_list_item_role_list_item_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/role-list-item/role-list-item.component */ "./src/app/feature-modules/new-production/components/role-list-item/role-list-item.component.ts");
/* harmony import */ var _components_production_list_production_list_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/production-list/production-list.component */ "./src/app/feature-modules/new-production/components/production-list/production-list.component.ts");
/* harmony import */ var _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/productions/productions.component */ "./src/app/feature-modules/new-production/components/productions/productions.component.ts");
/* harmony import */ var _components_side_quick_links_side_quick_links_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/side-quick-links/side-quick-links.component */ "./src/app/feature-modules/new-production/components/side-quick-links/side-quick-links.component.ts");
/* harmony import */ var _components_production_list_item_production_list_item_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/production-list-item/production-list-item.component */ "./src/app/feature-modules/new-production/components/production-list-item/production-list-item.component.ts");























//import { SidesDialogComponent } from '../../components/sides-dialog/sides-dialog.component';
//import { DropFileDirective } from '../../directives/drop-file.directive';
var NewProductionModule = /** @class */ (function () {
    function NewProductionModule() {
    }
    NewProductionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_9__["NewProductionComponent"],
                _components_add_role_add_role_component__WEBPACK_IMPORTED_MODULE_14__["AddRoleComponent"],
                _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_15__["NewRoleComponent"],
                _components_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_16__["RoleListComponent"],
                _components_role_list_item_role_list_item_component__WEBPACK_IMPORTED_MODULE_17__["RoleListItemComponent"],
                _components_production_production_component__WEBPACK_IMPORTED_MODULE_10__["ProductionComponent"],
                _components_production_list_production_list_component__WEBPACK_IMPORTED_MODULE_18__["ProductionListComponent"],
                _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_19__["ProductionsComponent"],
                _components_side_quick_links_side_quick_links_component__WEBPACK_IMPORTED_MODULE_20__["SideQuickLinksComponent"],
                _components_production_list_item_production_list_item_component__WEBPACK_IMPORTED_MODULE_21__["ProductionListItemComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__["UIRouterModule"].forChild({ states: _new_production_states__WEBPACK_IMPORTED_MODULE_11__["NEWPRODUCTION_STATES"] }),
                _global_global_module__WEBPACK_IMPORTED_MODULE_5__["GlobalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            ],
            exports: [
            // DropFileDirective,
            ],
            entryComponents: [
                _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_15__["NewRoleComponent"]
            ],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"], useClass: _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_8__["JwtInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"], useClass: _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_7__["ErrorInterceptor"], multi: true },
                _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_12__["RoleDialogService"],
                _services_production_component_service__WEBPACK_IMPORTED_MODULE_13__["ProductionComponentService"]
            ],
        })
    ], NewProductionModule);
    return NewProductionModule;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/new-production.states.ts":
/*!*************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/new-production.states.ts ***!
  \*************************************************************************/
/*! exports provided: getMetadataFromServer, getEmptyProduction, getProductionFromTransactionOrServer, getProductionsFromServer, getEmptyRole, getRoleFromTransactionOrServer, getRolesFromServer, productionListState, productionState, newproductionState, draftproductionState, editproductionState, productionNewRoleState, draftproductionNewRoleState, editRoleState, NEWPRODUCTION_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMetadataFromServer", function() { return getMetadataFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyProduction", function() { return getEmptyProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionFromTransactionOrServer", function() { return getProductionFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionsFromServer", function() { return getProductionsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyRole", function() { return getEmptyRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRoleFromTransactionOrServer", function() { return getRoleFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRolesFromServer", function() { return getRolesFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionListState", function() { return productionListState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionState", function() { return productionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newproductionState", function() { return newproductionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draftproductionState", function() { return draftproductionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editproductionState", function() { return editproductionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionNewRoleState", function() { return productionNewRoleState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draftproductionNewRoleState", function() { return draftproductionNewRoleState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editRoleState", function() { return editRoleState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NEWPRODUCTION_STATES", function() { return NEWPRODUCTION_STATES; });
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _components_production_production_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/production/production.component */ "./src/app/feature-modules/new-production/components/production/production.component.ts");
/* harmony import */ var _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/new-role/new-role.component */ "./src/app/feature-modules/new-production/components/new-role/new-role.component.ts");
/* harmony import */ var _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/productions/productions.component */ "./src/app/feature-modules/new-production/components/productions/productions.component.ts");
/* harmony import */ var _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/new-production/new-production.component */ "./src/app/feature-modules/new-production/components/new-production/new-production.component.ts");
/* harmony import */ var _services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/state-transition-handler.service */ "./src/app/services/state-transition-handler.service.ts");






function getMetadataFromServer(stateTransitionService, transition) {
    var metaDate;
    var promise;
    return stateTransitionService.getMetadataFromServer(transition);
}
function getEmptyProduction() {
    var prod;
    return prod;
}
function getProductionFromTransactionOrServer(stateTransitionService, transition) {
    var productionId;
    var production;
    var promise;
    return stateTransitionService.getProductionFromTransactionOrServer(transition);
}
function getProductionsFromServer(stateTransitionService, transition) {
    var productions;
    var promise;
    return stateTransitionService.getProductionsFromServer(transition);
}
function getEmptyRole() {
    var prod;
    return prod;
}
function getRoleFromTransactionOrServer(stateTransitionService, transition) {
    var roleId;
    var role;
    var promise;
    return stateTransitionService.getRoleFromTransactionOrServer(transition);
}
function getRolesFromServer(stateTransitionService, transition) {
    var roles;
    var promise;
    return stateTransitionService.getRolesFromServer(transition);
}
var productionListState = {
    name: 'productions',
    url: '/productions',
    parent: "mainPulloutScrollContainer",
    views: {
        "main@mainPulloutScrollContainer": { component: _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_3__["ProductionsComponent"] }
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'productions', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionsFromServer
        }
    ]
};
var productionState = {
    name: 'production',
    url: '/productions/:productionId',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_production_production_component__WEBPACK_IMPORTED_MODULE_1__["ProductionComponent"] }
    },
    params: {
        production: null
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        }
    ]
};
var newproductionState = {
    name: 'newproduction',
    url: '/productions/new',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_4__["NewProductionComponent"] },
    },
    params: {
        production: null,
        popRole: false,
        isDraft: false,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ]
};
var draftproductionState = {
    name: 'draftproduction',
    url: '/productions/draft/:draftId',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_4__["NewProductionComponent"] },
    },
    params: {
        production: null,
        popRole: false,
        isDraft: true,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var editproductionState = {
    name: 'editproduction',
    url: '/productions/edit/:productionId',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_4__["NewProductionComponent"] },
    },
    params: {
        production: null,
        popRole: false,
        isDraft: false,
        isEdit: true
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var productionNewRoleState = {
    name: 'productionnewrole',
    url: '/productions/:productionId/roles/new',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_2__["NewRoleComponent"] }
    },
    params: {},
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ]
};
var draftproductionNewRoleState = {
    name: 'draftproductionnewrole',
    url: '/productions/draft/:draftId/roles/new',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_2__["NewRoleComponent"] },
    },
    params: {
        // production: null,
        popRole: false,
        isDraft: true,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var editRoleState = {
    name: 'editrole',
    url: '/productions/:productionId/roles/:roleId/edit',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_2__["NewRoleComponent"] },
    },
    params: {},
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'role', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getRoleFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_5__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var NEWPRODUCTION_STATES = [
    productionListState,
    productionState,
    newproductionState,
    draftproductionState,
    editproductionState,
    productionNewRoleState,
    draftproductionNewRoleState,
    editRoleState,
];


/***/ }),

/***/ "./src/app/feature-modules/new-production/services/production-component.service.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/services/production-component.service.ts ***!
  \*****************************************************************************************/
/*! exports provided: ProductionComponentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionComponentService", function() { return ProductionComponentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var ProductionComponentService = /** @class */ (function () {
    function ProductionComponentService() {
        this._saveProductionIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.saveProductionIntentObs = this._saveProductionIntentObs.asObservable();
        this._cancelEditProductionIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.cancelEditProductionIntentObs = this._cancelEditProductionIntentObs.asObservable();
        this._deleteProductionIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.deleteProductionIntentObs = this._deleteProductionIntentObs.asObservable();
    }
    ProductionComponentService.prototype.subscribeToSaveProductionIntentObs = function () {
        return this.saveProductionIntentObs;
    };
    ProductionComponentService.prototype.trySaveProduction = function () {
        this._saveProductionIntentObs.next(true);
    };
    ProductionComponentService.prototype.subscribeToCancelEditProductionIntentObs = function () {
        return this.cancelEditProductionIntentObs;
    };
    ProductionComponentService.prototype.tryCancelEditProduction = function () {
        this._cancelEditProductionIntentObs.next(true);
    };
    ProductionComponentService.prototype.subscribeToDeleteProductionIntentObs = function () {
        return this.deleteProductionIntentObs;
    };
    ProductionComponentService.prototype.tryDeleteProduction = function () {
        this._deleteProductionIntentObs.next(true);
    };
    ProductionComponentService.prototype.registerProductionScrollElement = function (element) {
        this.productionElement = element;
    };
    ProductionComponentService.prototype.registerOtherScrollElement = function (element) {
        this.otherElement = element;
    };
    ProductionComponentService.prototype.registerArtisticScrollElement = function (element) {
        this.artisticElement = element;
    };
    ProductionComponentService.prototype.registerCastingScrollElement = function (element) {
        this.castingElement = element;
    };
    ProductionComponentService.prototype.registerRolesScrollElement = function (element) {
        this.rolesElement = element;
    };
    ProductionComponentService.prototype.registerBasicScrollElement = function (element) {
        this.basicElement = element;
    };
    //
    ProductionComponentService.prototype.scrollToProduction = function () {
        this.productionElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToOther = function () {
        this.otherElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToArtistic = function () {
        this.artisticElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToCasting = function () {
        this.castingElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToRoles = function () {
        this.rolesElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToBasic = function () {
        this.basicElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductionComponentService);
    return ProductionComponentService;
}());



/***/ }),

/***/ "./src/app/feature-modules/new-production/services/role-dialog.service.ts":
/*!********************************************************************************!*\
  !*** ./src/app/feature-modules/new-production/services/role-dialog.service.ts ***!
  \********************************************************************************/
/*! exports provided: RoleDialogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleDialogService", function() { return RoleDialogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var RoleDialogService = /** @class */ (function () {
    function RoleDialogService() {
        this._editRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.editRoleObs = this._editRoleObs.asObservable();
        this._newRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.newRoleObs = this._newRoleObs.asObservable();
    }
    RoleDialogService.prototype.subscribeToCreateRoleObs = function () {
        return this.newRoleObs;
    };
    RoleDialogService.prototype.subscribeToEditRoleObs = function () {
        return this.editRoleObs;
    };
    RoleDialogService.prototype.createRole = function (production) {
        var transferObject = new _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ProductionRoleTransferObject"];
        transferObject.production = production;
        /* const dialogRef = this.dialog.open(NewRoleComponent, {
           //width: '90%',
             data: transferObject,
           autoFocus: false,
           backdropClass: 'logindialog-overlay',
           panelClass: 'logindialog-panel'
         });
   
         dialogRef.afterClosed().subscribe(result => {
             let role: Role = result;
             this._newRoleObs.next(role);
           // this.animal = result;
         });*/
        return this.newRoleObs;
    };
    RoleDialogService.prototype.editRole = function (production, role) {
        var transferObject = new _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ProductionRoleTransferObject"];
        transferObject.production = production;
        transferObject.role = role;
        /* const dialogRef = this.dialog.open(NewRoleComponent, {
           //width: '90%',
           data: transferObject,
           autoFocus: false,
           backdropClass: 'logindialog-overlay',
           panelClass: 'logindialog-panel'
         });
   
           dialogRef.afterClosed().subscribe((result: Role) => {
          
          // let role: Role = result;
               this._editRoleObs.next(result);
           // this.animal = result;
         });*/
        return this.editRoleObs;
    };
    RoleDialogService.prototype.deleteRole = function (production, role) {
        var transferObject = new _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ProductionRoleTransferObject"];
        transferObject.production = production;
        transferObject.role = role;
        /*  const dialogRef = this.dialog.open(NewRoleComponent, {
            //width: '90%',
            data: transferObject,
            autoFocus: false,
            backdropClass: 'logindialog-overlay',
            panelClass: 'logindialog-panel'
          });
    
          dialogRef.afterClosed().subscribe(result => {
            let role: Role = result;
            this._editRoleObs.next(role);
            // this.animal = result;
          });*/
        return this.editRoleObs;
    };
    RoleDialogService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RoleDialogService);
    return RoleDialogService;
}());



/***/ })

}]);
//# sourceMappingURL=feature-modules-new-production-new-production-module.js.map