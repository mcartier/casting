(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"],{

/***/ "./src/app/animations/router.animation.ts":
/*!************************************************!*\
  !*** ./src/app/animations/router.animation.ts ***!
  \************************************************/
/*! exports provided: RouterAnimations, routeSlideAnimation, fadeInAnimation, slideInOutAnimation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouterAnimations", function() { return RouterAnimations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routeSlideAnimation", function() { return routeSlideAnimation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fadeInAnimation", function() { return fadeInAnimation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "slideInOutAnimation", function() { return slideInOutAnimation; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");


var RouterAnimations = /** @class */ (function () {
    function RouterAnimations() {
    }
    RouterAnimations.routeSlide = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('routeSlide', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* <=> *', [
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["group"])([
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(100%)' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('0.4s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(0%)' }))
                ], { optional: true }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(0%)' }),
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('0.4s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(-100%)' }))
                ], { optional: true }),
            ])
        ]),
    ]);
    return RouterAnimations;
}());

var routeSlideAnimation = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('routeSlide', [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* <=> *', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(100%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('0.4s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(0%)' }))
            ], { optional: true }),
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(0%)' }),
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('0.4s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ transform: 'translateX(-100%)' }))
            ], { optional: true }),
        ])
    ]),
]);
var fadeInAnimation = 
// trigger name for attaching this animation to an element using the [@triggerName] syntax
Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('fadeInAnimation', [
    // route 'enter' transition
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])(':enter', [
        // css styles at start of transition
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ opacity: 0 }),
        // animation and styles at end of transition
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('.3s', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ opacity: 1 }))
    ]),
]);
var slideInOutAnimation = 
// trigger name for attaching this animation to an element using the [@triggerName] syntax
Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('slideInOutAnimation', [
    // end state styles for route container (host)
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["state"])('*', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
        // the view covers the whole screen with a semi tranparent background
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.8)'
    })),
    // route 'enter' transition
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])(':enter', [
        // styles at start of transition
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
            // start with the content positioned off the right of the screen, 
            // -400% is required instead of -100% because the negative position adds to the width of the element
            right: '-400%',
            // start with background opacity set to 0 (invisible)
            backgroundColor: 'rgba(0, 0, 0, 0)'
        }),
        // animation and styles at end of transition
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
            // transition the right position to 0 which slides the content into view
            right: 0,
            // transition the background opacity to 0.8 to fade it in
            backgroundColor: 'rgba(0, 0, 0, 0.8)'
        }))
    ]),
    // route 'leave' transition
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])(':leave', [
        // animation and styles at end of transition
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('.5s ease-in-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({
            // transition the right position to -400% which slides the content out of view
            right: '-400%',
            // transition the background opacity to 0 to fade it out
            backgroundColor: 'rgba(0, 0, 0, 0)'
        }))
    ])
]);


/***/ }),

/***/ "./src/app/models/metadata.ts":
/*!************************************!*\
  !*** ./src/app/models/metadata.ts ***!
  \************************************/
/*! exports provided: MetadataItem, ProjectTypeCategoryMetadataItem, MetadataModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MetadataItem", function() { return MetadataItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectTypeCategoryMetadataItem", function() { return ProjectTypeCategoryMetadataItem; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MetadataModel", function() { return MetadataModel; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var MetadataItem = /** @class */ (function () {
    function MetadataItem() {
    }
    return MetadataItem;
}());

var ProjectTypeCategoryMetadataItem = /** @class */ (function () {
    function ProjectTypeCategoryMetadataItem() {
    }
    return ProjectTypeCategoryMetadataItem;
}());

var MetadataModel = /** @class */ (function () {
    function MetadataModel() {
    }
    return MetadataModel;
}());



/***/ }),

/***/ "./src/app/models/production.models.ts":
/*!*********************************************!*\
  !*** ./src/app/models/production.models.ts ***!
  \*********************************************/
/*! exports provided: ProductionRoleTransferObject, BaseProduction, Production, NewProduction, UpdateProduction, BaseRole, Role, NewRole, UpdateRole, BaseProductionExtra, AuditionDetail, ShootDetail, Side, PayDetail, CompensationTypeEnum, ProductionUnionStatusEnum, PayPackageTypeEnum, AuditionTypeEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionRoleTransferObject", function() { return ProductionRoleTransferObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseProduction", function() { return BaseProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Production", function() { return Production; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProduction", function() { return NewProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProduction", function() { return UpdateProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseRole", function() { return BaseRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Role", function() { return Role; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRole", function() { return NewRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateRole", function() { return UpdateRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseProductionExtra", function() { return BaseProductionExtra; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuditionDetail", function() { return AuditionDetail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShootDetail", function() { return ShootDetail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Side", function() { return Side; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayDetail", function() { return PayDetail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompensationTypeEnum", function() { return CompensationTypeEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionUnionStatusEnum", function() { return ProductionUnionStatusEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayPackageTypeEnum", function() { return PayPackageTypeEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuditionTypeEnum", function() { return AuditionTypeEnum; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var ProductionRoleTransferObject = /** @class */ (function () {
    function ProductionRoleTransferObject() {
    }
    return ProductionRoleTransferObject;
}());

var BaseProduction = /** @class */ (function () {
    function BaseProduction() {
    }
    return BaseProduction;
}());

var Production = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](Production, _super);
    function Production() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.productionTypes = [];
        _this.roles = [];
        _this.sides = [];
        _this.auditionDetails = [];
        _this.shootDetails = [];
        _this.payDetails = [];
        return _this;
    }
    return Production;
}(BaseProduction));

var NewProduction = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NewProduction, _super);
    function NewProduction() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewProduction;
}(BaseProduction));

var UpdateProduction = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateProduction, _super);
    function UpdateProduction() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateProduction;
}(BaseProduction));

var BaseRole = /** @class */ (function () {
    function BaseRole() {
    }
    return BaseRole;
}());

var Role = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](Role, _super);
    function Role() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Role;
}(BaseRole));

var NewRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NewRole, _super);
    function NewRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewRole;
}(BaseRole));

var UpdateRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateRole, _super);
    function UpdateRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateRole;
}(BaseRole));

var BaseProductionExtra = /** @class */ (function () {
    function BaseProductionExtra() {
    }
    return BaseProductionExtra;
}());

var AuditionDetail = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](AuditionDetail, _super);
    function AuditionDetail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return AuditionDetail;
}(BaseProductionExtra));

var ShootDetail = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ShootDetail, _super);
    function ShootDetail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ShootDetail;
}(BaseProductionExtra));

var Side = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](Side, _super);
    function Side() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Side;
}(BaseProductionExtra));

var PayDetail = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](PayDetail, _super);
    function PayDetail() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PayDetail;
}(BaseProductionExtra));

var CompensationTypeEnum;
(function (CompensationTypeEnum) {
    CompensationTypeEnum[CompensationTypeEnum["notPaid"] = 1] = "notPaid";
    CompensationTypeEnum[CompensationTypeEnum["somePaid"] = 3] = "somePaid";
    CompensationTypeEnum[CompensationTypeEnum["paid"] = 7] = "paid";
})(CompensationTypeEnum || (CompensationTypeEnum = {}));
var ProductionUnionStatusEnum;
(function (ProductionUnionStatusEnum) {
    ProductionUnionStatusEnum[ProductionUnionStatusEnum["notRelevan"] = 0] = "notRelevan";
    ProductionUnionStatusEnum[ProductionUnionStatusEnum["nonUnion"] = 1] = "nonUnion";
    ProductionUnionStatusEnum[ProductionUnionStatusEnum["union"] = 3] = "union";
    ProductionUnionStatusEnum[ProductionUnionStatusEnum["both"] = 7] = "both";
})(ProductionUnionStatusEnum || (ProductionUnionStatusEnum = {}));
var PayPackageTypeEnum;
(function (PayPackageTypeEnum) {
    PayPackageTypeEnum[PayPackageTypeEnum["money"] = 1] = "money";
    PayPackageTypeEnum[PayPackageTypeEnum["credit"] = 3] = "credit";
    PayPackageTypeEnum[PayPackageTypeEnum["gift"] = 7] = "gift";
})(PayPackageTypeEnum || (PayPackageTypeEnum = {}));
var AuditionTypeEnum;
(function (AuditionTypeEnum) {
    AuditionTypeEnum[AuditionTypeEnum["TapedSubmissionsOnly"] = 1] = "TapedSubmissionsOnly";
    AuditionTypeEnum[AuditionTypeEnum["TapedSubmissionAndInPersonAudition"] = 3] = "TapedSubmissionAndInPersonAudition";
    AuditionTypeEnum[AuditionTypeEnum["TapedSubmissionThenInPersonAudition"] = 7] = "TapedSubmissionThenInPersonAudition";
    AuditionTypeEnum[AuditionTypeEnum["InPersonAuditionOnly"] = 15] = "InPersonAuditionOnly";
})(AuditionTypeEnum || (AuditionTypeEnum = {}));


/***/ })

}]);
//# sourceMappingURL=common.js.map