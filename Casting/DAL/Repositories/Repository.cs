﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;

namespace Casting.DAL.Repositories
{
    public abstract class Repository : IRepository
    {
        private ICastingContext _castingContext;
        protected Repository(ICastingContext context)
        {
            _castingContext = context;
        }

        public IEnumerable<T> GetAll<T>() where T : class
        {
            return _castingContext.Set<T>().AsQueryable();
        }

        public T GetById<T>(int id) where T : class
        {
            return _castingContext.Set<T>().Find(id);

        }

        public int Add<T>(T entity) where T : class
        {
            _castingContext.Set<T>().Add(entity);
            return _castingContext.SaveChanges();
        }

        public int Delete<T>(T entity) where T : class
        {
            _castingContext.Set<T>().Remove(entity);
            return _castingContext.SaveChanges();
        }

        public int Update<T>(T entity) where T : class
        {
            _castingContext.SetModified(entity);
            return _castingContext.SaveChanges();
        }
        public void SetEntityState<T>(T entity, EntityState state) where T : class
        {
            _castingContext.SetState(entity, state);
        }
    }

}
