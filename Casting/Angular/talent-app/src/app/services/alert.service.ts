﻿import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { TransitionService, Transition } from '@uirouter/angular';
import { HttpErrorResponse } from '@angular/common/http';
import { IAlert, AlertType } from '../models/alertInterface'
@Injectable({
    providedIn: 'root'
})
export class AlertService {

    constructor(private transitionService: TransitionService) {
        transitionService.onBefore({ to: '*' }, (trans: Transition) => {
            if (this.keepAfterNavigationChange) {
                this.keepAfterNavigationChange = false;
            }
            else {
                this.message.next();
            }
        });


    }

    private message = new Subject<IAlert>();
    private keepAfterNavigationChange = false;

    success(message: string, keepAfterNavigationChange = true) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        const alertMessage: IAlert = {
            alertType: AlertType.success,
            message: message,
            status: 0,
            originalMessage: message,
            fieldValidationMessages: []
        }
        this.message.next(alertMessage);
    }

    error(error: HttpErrorResponse, message: string, extraValidation: string[] = [], keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        let fieldValidations: string[] = [];
        let alert: any;
        if (error.error) {
            if (error.error.modelState) {
                for (let key in error.error.modelState) {
                    if (error.error.modelState[key] && error.error.modelState[key] instanceof Array) {
                        for (var i = 0; i < error.error.modelState[key].length; i++) {
                            fieldValidations.push(error.error.modelState[key][i]);
                        }
                    }
                    else {
                        if (error.error.modelState[key]) { fieldValidations.push(error.error.modelState[key]) }
                    }

                }
            }
            extraValidation.forEach((validationString) => {
                fieldValidations.push(validationString);
            });
            const alertMessage: IAlert = {
                alertType: AlertType.error,
                message: (message !== null && message.length > 0) ? message : (error.error.message) ? error.error.message : error.statusText,
                status: (error.error instanceof ErrorEvent) ? 0 : error.status,
                originalMessage: (error.error.message) ? error.error.message : error.statusText,
                fieldValidationMessages: fieldValidations
            }
            alert = alertMessage;
        }
        this.message.next(alert);
    }

    getAlerts(): Observable<any> {
        return this.message.asObservable();
    }
}
