﻿using Casting.BLL.CastingCalls.Interface;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.CastingCalls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.BLL.CastingCalls
{
  

    public class CastingCallTalentLocationService : ICastingCallTalentLocationService
    {
        private ICastingCallAsyncRepository _castingCallRepository;

        public CastingCallTalentLocationService(ICastingCallAsyncRepository castingCallAsyncRepository)
        {
            _castingCallRepository = castingCallAsyncRepository;

        }
        public async Task<IEnumerable<CastingCallCityReference>> GetCastingCallTalentCityReferencesByCastingCallId(int castingCallId)
        {


            return await _castingCallRepository.GetCastingCallTalentCityReferencesByCastingCallId(castingCallId);
        }

        public async Task<CastingCallCityReference> GetCastingCallTalentCityReferenceById(int castingCallLocationReferenceId)
        {


            return await _castingCallRepository.GetCastingCallTalentCityReferenceById(castingCallLocationReferenceId);
        }


        public async Task<int> AddNewCityTalentLocationAsync(CastingCallCityReference castingCallCityReference)
        {
            return await _castingCallRepository.AddNewCityTalentLocationAsync(castingCallCityReference);
        }

        public async Task<int> DeleteCityTalentReferenceAsync(CastingCallCityReference castingCallCityReference)
        {
            return await _castingCallRepository.DeleteCityTalentReferenceAsync(castingCallCityReference);
        }
        public async Task<IEnumerable<CastingCallRegionReference>> GetCastingCallTalentRegionReferencesByCastingCallId(int castingCallId)
        {


            return await _castingCallRepository.GetCastingCallTalentRegionReferencesByCastingCallId(castingCallId);
        }

        public async Task<CastingCallRegionReference> GetCastingCallTalentRegionReferenceById(int castingCallLocationReferenceId)
        {


            return await _castingCallRepository.GetCastingCallTalentRegionReferenceById(castingCallLocationReferenceId);
        }

        public async Task<int> AddNewRegionTalentLocationAsync(CastingCallRegionReference castingCallRegionReference)
        {
            return await _castingCallRepository.AddNewRegionTalentLocationAsync(castingCallRegionReference);
        }

        public async Task<int> DeleteRegionTalentReferenceAsync(CastingCallRegionReference castingCallRegionReference)
        {
            return await _castingCallRepository.DeleteRegionTalentReferenceAsync(castingCallRegionReference);
        }
        public async Task<IEnumerable<CastingCallCountryReference>> GetCastingCallTalentCountryReferencesByCastingCallId(int castingCallId)
        {


            return await _castingCallRepository.GetCastingCallTalentCountryReferencesByCastingCallId(castingCallId);
        }

        public async Task<CastingCallCountryReference> GetCastingCallTalentCountryReferenceById(int castingCallLocationReferenceId)
        {


            return await _castingCallRepository.GetCastingCallTalentCountryReferenceById(castingCallLocationReferenceId);
        }

        public async Task<int> AddNewCountryTalentLocationAsync(CastingCallCountryReference castingCallCountryReference)
        {
            return await _castingCallRepository.AddNewCountryTalentLocationAsync(castingCallCountryReference);
        }

        public async Task<int> DeleteCountryTalentReferenceAsync(CastingCallCountryReference castingCallCountryReference)
        {
            return await _castingCallRepository.DeleteCountryTalentReferenceAsync(castingCallCountryReference);
        }
    }
}
