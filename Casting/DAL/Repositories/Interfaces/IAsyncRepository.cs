﻿using Casting.Model;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSW.Data.Entities;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IAsyncRepository
    {
        Task<IEnumerable<T>> GetAllAsync<T>() where T : class;
        Task<T> GetByIdAsync<T>(int id) where T : class;
        Task<int> AddAsync<T>(T entity) where T : class;
        Task<int> DeleteAsync<T>(T entity) where T : class;
        Task<int> UpdateAsync<T>(T entity) where T : class;
        void SetEntityState<T>(T entity, EntityState state) where T : class;
       

    }
}
