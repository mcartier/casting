﻿
import {
    HttpClient, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map, retry, catchError, publishLast, refCount } from 'rxjs/operators';
import { JSONReturnVM } from '../models/JSONReturn';
export class HttpHelpers {
    constructor(private _http: HttpClient) {
    }

    get haserror(): boolean {
        return this.errormsg != null;
    }

    errormsg: string;

    getAction<T>(path: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
       
        return this._http.get(path, httpOptions)
            .pipe(
            publishLast(),
                refCount(),
           );
    }
   
    postAction<T>(param: T, path: string) {
        this.errormsg = null;

        let body = JSON.stringify(param);
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        return this._http.post(path, body, httpOptions)
            .pipe(
            publishLast(),
            refCount(),
            
        );
    }
    putAction<T>(param: T, path: string) {
        this.errormsg = null;

        let body = JSON.stringify(param);
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        let t: any = 0;
        return this._http.put(path, body, httpOptions).pipe(
            publishLast(),
            refCount(),
        );
    }
    deleteAction<T>(path: string) {
        this.errormsg = null;
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };

        return this._http.delete(path, httpOptions).pipe(
            publishLast(),
            refCount(),
            //   catchError(this._handleError)
        );
    }

    private _handleError(error) {
       
        return throwError(
            'Something bad happened; please try again later.');
        //return Observable.throw(error.statusText + " at " + error.url || 'Server error');
    }
}