import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainNonScrollComponent } from './main-non-scroll.component';

describe('MainNonScrollComponent', () => {
  let component: MainNonScrollComponent;
  let fixture: ComponentFixture<MainNonScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainNonScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNonScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
