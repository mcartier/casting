import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuditionDetailsDialogComponent } from './audition-details-dialog.component';

describe('AuditionDetailsDialogComponent', () => {
  let component: AuditionDetailsDialogComponent;
  let fixture: ComponentFixture<AuditionDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuditionDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuditionDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
