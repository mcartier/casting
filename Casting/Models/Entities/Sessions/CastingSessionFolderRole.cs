﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casting.Model.Entities.Sessions
{
    public class CastingSessionFolderRole:Logger
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
          public int CastingSessionFolderId { get; set; }
        public CastingSessionFolder CastingSessionFolder { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int PersonasCount { get; set; }
        public ICollection<CastingSessionFolderRoleSelectionPersonaReference> Personas { get; set; }
        public int CastingSessionRoleId { get; set; }
        public CastingSessionRole CastingSessionRole { get; set; }
        public OrderByValuesEnum SelectedPersonasSortOrderI { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }

        public int OrderIndex { get; set; }
    }
}
