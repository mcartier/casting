﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;

namespace Casting.BLL.Personas
{
    public class PersonaService : BaseService
    {
        private IPersonaAsyncRepository _personaRepository;

        public PersonaService(IPersonaAsyncRepository personaAsyncRepository)
        {
            _personaRepository = personaAsyncRepository;
        }

        public async Task<Persona> GetPersonaByIdWithTalentAssetsAndAttributesAsync(bool includeTalentObject, bool includeAssets, bool includeAttributes, int personaId)
        {
            return await _personaRepository.GetPersonaByIdAsync(includeTalentObject, includeAssets, includeAttributes, true, personaId);
        }

        public async Task<IEnumerable<Persona>> GetPersonasWithAssetsByCastingCallFolderRoleIdAsync(int folderRoleId)
        {
            return await _personaRepository.GetPersonasByCastingCallFolderRoleIdAsync(false, true, true, true,
                folderRoleId);
        }
        public async Task<IEnumerable<Persona>> GetPersonasWithAssetsByCastingCallRoleIdAsync(int roleId)
        {
            return await _personaRepository.GetPersonasByCastingCallRoleIdAsync(false, true, true, true,
                roleId);
        }
        public async Task<IEnumerable<Persona>> GetPersonasWithAssetsBySessionFolderRoleIdAsync(int folderRoleId)
        {
            return await _personaRepository.GetPersonasBySessionFolderRoleIdAsync(false, true, true, true,
                folderRoleId);
        }
        public async Task<IEnumerable<Persona>> GetPersonasWithAssetsBySessionRoleIdAsync(int roleId)
        {
            return await _personaRepository.GetPersonasBySessionRoleIdAsync(false, true, true, true,
                roleId);
        }
        public async Task<int> AddNewPersonaReferenceAsync(PersonaReference personaReference)
        {
            return await _personaRepository.AddNewPersonaReferenceAsync(true, personaReference);
        }

        public async Task<int> AddNewPersonaReferenceAsync(int refererId, PersonaReference personaReference)
        {
            return await _personaRepository.AddNewPersonaReferenceAsync(true, refererId, personaReference);
        }


        public async Task<int> AddNewPersonaAsync(Persona persona)
        {
            return await _personaRepository.AddNewPersonaAsync(persona);
        }

        public async Task<int> UpdatePersonaReferenceAsync(int refererId, PersonaReference personaReference)
        {
            return await _personaRepository.UpdatePersonaReferenceAsync(refererId, personaReference);
        }
        public async Task<int> UpdatePersonaAsync(Persona persona)
        {
            return await _personaRepository.UpdatePersonaAsync(persona);
        }
        public async Task<int> DeletePersonaReference(int refererId, PersonaReference personaReference)
        {
            return await _personaRepository.DeletePersonaReferenceAsync(refererId, personaReference);
        }
        public async Task<int> DeletePersona(Persona persona)
        {
            return await _personaRepository.DeletePersonaAsync(persona);
        }
    }
}
