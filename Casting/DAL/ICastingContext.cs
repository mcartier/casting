﻿using Casting.Model;

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Sessions;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Entities.Users.Guests;
using Casting.Model.Entities.Users.Talents;

using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Entities.Attributes;

namespace Casting.DAL
{
    public interface ICastingContext
    {


        //locations

         DbSet<Country> Countries { get; set; }

         DbSet<City> Cities { get; set; }

         DbSet<Region> Regions { get; set; }

DbSet<Persona> Personas { get; set; }
        //ublic DbSet<HouseTalentPersona> HouseTalentPersonas { get; set; }
        //ublic DbSet<StudioTalentPersona> StudioTalentPersonas { get; set; }
         DbSet<BaseCastingUser> BaseUsers { get; set; }
       //  DbSet<UserReference> UserReferences { get; set; }
         DbSet<PersonaReference> PersonaReferences { get; set; }
          DbSet<CastingCallFolderRoleSubmissionPersonaReference> FolderRoleSubmissionPersonaReferences { get; set; }
         DbSet<CastingCallRoleSubmissionPersonaReference> RoleSubmissionPersonaReferences { get; set; }
         DbSet<CastingSessionFolderRoleSelectionPersonaReference> FolderRoleSelectionPersonaReferences { get; set; }
         DbSet<CastingSessionRoleSelectionPersonaReference> RoleSelectionPersonaReferences { get; set; }

        //  DbSet<TalentProfileReference> TalentProfileReferences { get; set; }
        DbSet<Profile> Profiles { get; set; }
        DbSet<ActorProfile> ActorProfiles { get; set; }
        DbSet<VoiceActorProfile> VoiceActorProfiles { get; set; }
        DbSet<Basket> Baskets { get; set; }
          DbSet<Guest> Guests { get; set; }
          DbSet<Director> Directors { get; set; }

          DbSet<Agent> Agents { get; set; }
          DbSet<Agency> Agencies { get; set; }
          DbSet<AgencyOffice> AgencyOffices { get; set; }
          DbSet<AgentCityReference> AgentCityReferences { get; set; }
           DbSet<AgentTalentReference> AgentTalentReferences { get; set; }

        //talents
        DbSet<Talent> Talents { get; set; }
        DbSet<HouseTalent> HouseTalents { get; set; }
        DbSet<StudioTalent> StudioTalents { get; set; }
        // DbSet<HouseTalentPersonaReference> HouseTalentPersonaReferences { get; set; }
        // DbSet<StudioTalentPersonaReference> StudioTalentPersonaReferences { get; set; }

        //assets
        DbSet<Audio> Audios { get; set; }
        DbSet<Video> Videos { get; set; }
        DbSet<Resume> Resumes { get; set; }
        DbSet<Image> Images { get; set; }

        //asset references
        DbSet<AudioReference> AudioReferences { get; set; }
        DbSet<VideoReference> VideoReferences { get; set; }
        DbSet<ResumeReference> ResumeReferences { get; set; }
        DbSet<ImageReference> ImageReferences { get; set; }

        //persona asset references
        DbSet<PersonaAudioReference> PersonaAudioReferences { get; set; }
        DbSet<PersonaVideoReference> PersonaVideoReferences { get; set; }
        DbSet<PersonaResumeReference> PersonaResumeReferences { get; set; }
        DbSet<PersonaImageReference> PersonaImageReferences { get; set; }

        //Basket asset references
        DbSet<BasketAudioReference> BasketAudioReferences { get; set; }
        DbSet<BasketVideoReference> BasketVideoReferences { get; set; }
        DbSet<BasketResumeReference> BasketResumeReferences { get; set; }
        DbSet<BasketImageReference> BasketImageReferences { get; set; }

        //Profile asset references
        DbSet<ProfileAudioReference> ProfileAudioReferences { get; set; }
        DbSet<ActorProfileVideoReference> ProfileVideoReferences { get; set; }
        DbSet<ProfileResumeReference> ProfileResumeReferences { get; set; }
        DbSet<ActorProfileImageReference> ProfileImageReferences { get; set; }

        //productions
        DbSet<Production> Productions { get; set; }
        DbSet<Role> Roles { get; set; }

         DbSet<ShootDetail> ShootDetails { get; set; }
         DbSet<AuditionDetail> AuditionDetails { get; set; }
         DbSet<PayDetail> PayDetails { get; set; }
         DbSet<Side> Sides { get; set; }
         DbSet<File> Files { get; set; }
        //production metadata
        DbSet<ProductionTypeCategory> ProductionTypeCategories { get; set; }

        DbSet<ProductionType> ProductionTypes { get; set; }
        DbSet<RoleType> RoleTypes { get; set; }
        DbSet<RoleGenderType> RoleGenderTypes { get; set; }

        //profile attributes
        DbSet<Union> Unions { get; set; }
        DbSet<EthnicStereoType> EthnicStereoTypes { get; set; }
        DbSet<Ethnicity> Ethnicities { get; set; }
        DbSet<StereoType> StereoTypes { get; set; }
        DbSet<Accent> Accents { get; set; }
        DbSet<Language> Languages { get; set; }

        DbSet<HairColor> HairColors { get; set; }
        DbSet<EyeColor> EyeColors { get; set; }
        DbSet<BodyType> BodyTypes { get; set; }
         DbSet<Skill> Skills { get; set; }
         DbSet<SkillCategory> SkillCategories { get; set; }

        // attribute references
         DbSet<UnionReference> UnionReferences { get; set; }
         DbSet<EthnicStereoTypeReference> EthnicStereoTypeReferences { get; set; }
         DbSet<EthnicityReference> EthnicityReferences { get; set; }
         DbSet<StereoTypeReference> StereoTypeReferences { get; set; }
         DbSet<AccentReference> AccentReferences { get; set; }
         DbSet<LanguageReference> LanguageReferences { get; set; }
         DbSet<EyeColorReference> EyeColorReferences { get; set; }
         DbSet<HairColorReference> HairColorReferences { get; set; }
         DbSet<BodyTypeReference> BodyTypeReferences { get; set; }
          DbSet<SkillReference> SkillReferences { get; set; }

        //profile attribute references
        DbSet<ProfileUnionReference> ProfileUnionReferences { get; set; }
        DbSet<ProfileEthnicStereoTypeReference> ProfileEthnicStereoTypeReferences { get; set; }
        DbSet<ProfileEthnicityReference> ProfileEthnicityReferences { get; set; }
        DbSet<ProfileStereoTypeReference> ProfileStereoTypeReferences { get; set; }
        DbSet<ProfileAccentReference> ProfileAccentReferences { get; set; }
        DbSet<ProfileLanguageReference> ProfileLanguageReferences { get; set; }
         DbSet<ProfileEyeColorReference> ProfileEyeColorReferences { get; set; }
         DbSet<ProfileHairColorReference> ProfileHairColorReferences { get; set; }
         DbSet<ProfileBodyTypeReference> ProfileBodyTypeReferences { get; set; }
         DbSet<ProfileSkillReference> ProfileSkillReferences { get; set; }


        //role attribute references
        DbSet<RoleEthnicStereoTypeReference> RoleEthnicStereoTypeReferences { get; set; }
        DbSet<RoleEthnicityReference> RoleEthnicityReferences { get; set; }
        DbSet<RoleStereoTypeReference> RoleStereoTypeReferences { get; set; }
        DbSet<RoleAccentReference> RoleAccentReferences { get; set; }
        DbSet<RoleLanguageReference> RoleLanguageReferences { get; set; }
         DbSet<RoleEyeColorReference> RoleEyeColorReferences { get; set; }
         DbSet<RoleHairColorReference> RoleHairColorReferences { get; set; }
         DbSet<RoleBodyTypeReference> RoleBodyTypeReferences { get; set; }
         DbSet<RoleSkillReference> RoleSkillReferences { get; set; }

        //castingCall attribute references
        DbSet<CastingCallUnionReference> CastingCallUnionReferences { get; set; }
         DbSet<CastingCallRoleUnionReference> CastingCallRoleUnionReferences { get; set; }

        //profile search creiteria attribute references
        /*  DbSet<ProfileSearchCriteriaUnionReference> ProfileSearchCriteriaUnionReferences { get; set; }
          DbSet<ProfileSearchCriteriaEthnicStereoTypeReference> ProfileSearchCriteriaEthnicStereoTypeReferences { get; set; }
          DbSet<ProfileSearchCriteriaEthnicityReference> ProfileSearchCriteriaEthnicityReferences { get; set; }
          DbSet<ProfileSearchCriteriaStereoTypeReference> ProfileSearchCriteriaStereoTypeReferences { get; set; }
          DbSet<ProfileSearchCriteriaAccentReference> ProfileSearchCriteriaAccentReferences { get; set; }
          DbSet<ProfileSearchCriteriaLanguageReference> ProfileSearchCriteriaLanguageReferences { get; set; }
          */
        //castingCalls
        DbSet<CastingCall> CastingCalls { get; set; }

         DbSet<CastingCallPrivacy> CastingCallPrivacies { get; set; }
         DbSet<CastingCallViewType> CastingCallViewTypes { get; set; }
         DbSet<CastingCallType> CastingCallTypes { get; set; }

        DbSet<CastingCallRole> CastingCallRoles { get; set; }
         DbSet<CastingCallFolder> CastingCallFolders { get; set; }

         DbSet<CastingCallFolderRole> CastingCallFolderRoles { get; set; }


        //casting sessions

        DbSet<CastingSession> CastingSessions { get; set; }

         DbSet<CastingSessionRole> CastingSessionRoles { get; set; }

         DbSet<CastingSessionFolder> CastingSessionFolders { get; set; }

         DbSet<CastingSessionFolderRole> CastingSessionFolderRoles { get; set; }

          DbSet<CastingCallCityReference> CastingCallCityReferences { get; set; }
          DbSet<CastingCallRegionReference> CastingCallRegionReferences { get; set; }
          DbSet<CastingCallCountryReference> CastingCallCountryReferences { get; set; }


        int SaveChanges();
        Task<int> SaveChangesAsync();
        void SetModified(object entity);
        void SetState(object entity, EntityState state);

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet Set(Type entityType);
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        DbEntityEntry Entry(object entity);
        System.Data.Entity.Database Database { get; set; }
        

        void Dispose();
    }
}
