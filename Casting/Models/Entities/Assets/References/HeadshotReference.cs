﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets.References
{
    public abstract class ImageReference : Logger
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int ImageId { get; set; }
        public Image Image { get; set; }

        public AssetReferenceTypeEnum AssetReferenceTypeEnum { get; set; }
        public int OrderIndex { get; set; }

    }
}
