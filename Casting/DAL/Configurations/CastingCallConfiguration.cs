﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Productions;


namespace Casting.DAL.Configurations
{
   
    public class CastingCallConfiguration : EntityTypeConfiguration<CastingCall>
    {
        public CastingCallConfiguration()
        {
            HasKey(x => x.Id);
            HasMany(s => s.Roles)
              .WithRequired(c => c.CastingCall)
             .HasForeignKey(k=>k.CastingCallId);
             HasRequired(p => p.Production)
                 .WithMany(c => c.CastingCalls)
                 .HasForeignKey(k=>k.ProductionId)
                 .WillCascadeOnDelete(false);
            HasMany(s => s.CastingSessions)
                 .WithRequired(s => s.CastingCall)
                 .HasForeignKey(k => k.CastingCallId);
             HasMany(c => c.CityTalentLocations)
                 .WithRequired(c => c.CastingCall)
                 .HasForeignKey(k => k.CastingCallId);
             HasMany(c => c.RegionTalentLocations)
                 .WithRequired(c => c.CastingCall)
                 .HasForeignKey(k => k.CastingCallId);
             HasMany(c => c.CountryTalentLocations)
                 .WithRequired(c => c.CastingCall)
                 .HasForeignKey(k => k.CastingCallId);
             HasOptional(a => a.AuditionDetail);
             HasOptional(a => a.PayDetail);
             HasOptional(a => a.ShootDetail);
        }
    }
    public class CastingCallRoleConfiguration : EntityTypeConfiguration<CastingCallRole>
    {
        public CastingCallRoleConfiguration()
        {
           HasKey(x => x.Id);
           HasRequired(r => r.Role)
               .WithMany(c => c.CastingCallRoles)
               .HasForeignKey(k => k.RoleId)
           .WillCascadeOnDelete(false);
           HasRequired(p => p.CastingCall)
               .WithMany(r => r.Roles)
               .HasForeignKey(k=>k.CastingCallId);
           HasMany(r => r.Personas)
               .WithRequired(r => r.CastingCallRole)
               .HasForeignKey(k => k.CastingCallRoleId);
           HasOptional(a => a.AuditionDetail);
           HasOptional(a => a.PayDetail);
           HasOptional(a => a.ShootDetail);
        }
    }

    public class CastingCallFolderConfiguration : EntityTypeConfiguration<CastingCallFolder>
    {
        public CastingCallFolderConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.CastingCall)
                .WithMany(f => f.Folders)
                .HasForeignKey(k => k.CastingCallId);
            HasMany(s => s.Roles)
                .WithRequired(c => c.CastingCallFolder)
                .HasForeignKey(k => k.CastingCallFolderId);


        }
    }
    public class CastingCallFolderRoleConfiguration : EntityTypeConfiguration<CastingCallFolderRole>
    {
        public CastingCallFolderRoleConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(r => r.CastingCallRole)
                .WithMany(c => c.FolderRoles)
                .HasForeignKey(k => k.CastingCallRoleId)
                .WillCascadeOnDelete(false);
            HasRequired(p => p.CastingCallFolder)
                .WithMany(r => r.Roles)
                .HasForeignKey(k => k.CastingCallFolderId);
            HasRequired(r => r.Role)
                .WithMany(c => c.CastingCallFolderRoles)
                .HasForeignKey(k => k.RoleId)
                .WillCascadeOnDelete(false);
            HasMany(r => r.Personas)
                .WithRequired(r => r.CastingCallFolderRole)
                .HasForeignKey(k => k.CastingCallFolderRoleId);

        }
    }
}
