import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidesDialogComponent } from './sides-dialog.component';

describe('SidesDialogComponent', () => {
  let component: SidesDialogComponent;
  let fixture: ComponentFixture<SidesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidesDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidesDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
