﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets
{
    public class Submission : Logger
    {
        public int Id { get; set; }
        public int CastingCallRoleId { get; set; }
        public CastingCallRole CastingCallRole { get; set; }
        public int CastingCallId { get; set; }
        public CastingCall CastingCall { get; set; }
        public int ProductionId { get; set; }
        public Production Production { get; set; }
        public int DirectorId { get; set; }
        public Director Director { get; set; }
        public int OwnerId { get; set; }
        public OwnerTypeEnum OwnerType { get; set; }
        public int PersonaId { get; set; }
        public Persona Persona { get; set; }
    }
}
