﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.BLL.CastingCalls.Interfaces;

namespace Casting.BLL.CastingCalls
{
    public class CastingCallService:BaseService, ICastingCallService
    {
        private ICastingCallAsyncRepository _castingCallRepository;

        public CastingCallService(ICastingCallAsyncRepository castingCallAsyncRepository)
        {
            _castingCallRepository = castingCallAsyncRepository;
           
        }

        public async Task<CastingCall> GetCastingCallByIdAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallByIdAsync(CastingCallParameterIncludeInQueryEnum.CastingCalls, castingCallId);
        }
        public async Task<CastingCall> GetCastingCallByIdWithRolesAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallByIdAsync(CastingCallParameterIncludeInQueryEnum.Roles, castingCallId);
        }
        public async Task<CastingCall> GetCastingCallByIdWithRolesAndPersonasAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallByIdAsync(CastingCallParameterIncludeInQueryEnum.RolesAndPersonas, castingCallId);
        }
        public async Task<CastingCall> GetCastingCallByIdWithRolesPersonasAndAssetsAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallByIdAsync(CastingCallParameterIncludeInQueryEnum.RolesPersonasAndAssets, castingCallId);
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdAsync(int productionId)
        {
            return await _castingCallRepository.GetCastingCallsByProductionIdAsync(CastingCallParameterIncludeInQueryEnum.CastingCalls, productionId);
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdWithRolesAsync(int productionId)
        {
            return await _castingCallRepository.GetCastingCallsByProductionIdAsync(CastingCallParameterIncludeInQueryEnum.Roles, productionId);
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdWithRolesPersonasAsync(int productionId)
        {
            return await _castingCallRepository.GetCastingCallsByProductionIdAsync(CastingCallParameterIncludeInQueryEnum.RolesAndPersonas, productionId);
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdWithRolesPersonasAssetsAsync(int productionId)
        {
            return await _castingCallRepository.GetCastingCallsByProductionIdAsync(CastingCallParameterIncludeInQueryEnum.RolesPersonasAndAssets, productionId);
        }

        public async Task<IEnumerable<CastingCall>> GetCastingCallsAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery,
            int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending)
        {
           return await _castingCallRepository.GetCastingCallsAsync(parameterIncludeInQuery,pageIndex, pageSize, orderBy);
        }

        public async Task<int> AddNewCastingCallAsync(CastingCall castingCall)
        {
            return await _castingCallRepository.AddNewCastingCallAsync(castingCall);
        }

        public async Task<int> UpdateCastingCallAsync(CastingCall castingCall)
        {
            return await _castingCallRepository.UpdateCastingCallAsync(castingCall);
        }
        public async Task<int> DeleteCastingCall(CastingCall castingCall)
        {
            return await _castingCallRepository.DeleteCastingCallAsync(castingCall);
        }
    }
}
