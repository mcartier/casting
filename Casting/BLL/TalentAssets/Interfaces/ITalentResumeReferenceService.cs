﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;

namespace Casting.BLL.TalentAssets.Interfaces

{
    public interface ITalentResumeReferenceService
    {
        Task<ResumeReference> GetResumeReferenceByIdAsync(int resumeId);
        Task<IEnumerable<ResumeReference>> GetProfileResumeReferencesForParentAsync(int profileId, bool withAssets);
        Task<IEnumerable<ResumeReference>> GetPersonaResumeReferencesForParentAsync(int personaId, bool withAssets);
        Task<IEnumerable<ResumeReference>> GetBasketResumeReferencesForParentAsync(int basketId, bool withAssets);

        Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            ResumeReference assetReference);

        Task<int> AddNewResumeAssetReferenceAsync(bool orderAtTheTop,
            int parentId, ResumeReference assetReference);

        Task<int> AddNewProfileResumeAssetReferenceAsync(bool orderAtTheTop, ProfileResumeReference assetReference);
        Task<int> AddNewPersonaResumeAssetReferenceAsync(bool orderAtTheTop, PersonaResumeReference assetReference);
        Task<int> AddNewBasketResumeAssetReferenceAsync(bool orderAtTheTop, BasketResumeReference assetReference);
        Task<int> DeleteResumeAssetReferenceAsync(int parentId, ResumeReference assetReference);
        Task<int> DeleteProfileResumeAssetReferenceAsync(ProfileResumeReference assetReference);
        Task<int> DeleteBasketResumeAssetReferenceAsync(BasketResumeReference assetReference);
        Task<int> DeletePersonaResumeAssetReferenceAsync(PersonaResumeReference assetReference);

        Task<int> UpdateResumeAssetReferenceAsync(int parentId,
            ResumeReference assetReference);

        Task<int> UpdateProfileResumeAssetReferenceAsync(ProfileResumeReference assetReference);
        Task<int> UpdateBasketResumeAssetReferenceAsync(BasketResumeReference assetReference);
        Task<int> UpdatePersonaResumeAssetReferenceAsync(PersonaResumeReference assetReference);

        Task<int> ReOrderResumeAssetAsync(int parentId,
            int desiredOrderIndex, ResumeReference assetReference);

        Task<int> ReOrderProfileResumeAssetAsync(int desiredOrderIndex, ProfileResumeReference assetReference);
        Task<int> ReOrderBasketResumeAssetAsync(int desiredOrderIndex, BasketResumeReference assetReference);
        Task<int> ReOrderPersonaResumeAssetAsync(int desiredOrderIndex, PersonaResumeReference assetReference);
    }
}