﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Users.Directors;


namespace Casting.DAL.Configurations
{
    class DirectorConfiguration : EntityTypeConfiguration<Director>
    {
        public DirectorConfiguration()
        {
            ToTable("Director");
            HasKey(x => x.Id);
            HasMany(s => s.Productions)
                .WithRequired(a => a.Director)
                .HasForeignKey(k=>k.DirectorId);
            HasMany(t => t.Profiles)
                .WithOptional(d => d.Director);
            //.HasForeignKey(k => k.DirectorId)
            // .WillCascadeOnDelete(false); ;
            // HasRequired(u => u.ApplicationUser)
            //     .WithMany(a => a.Directors)
            //     .HasForeignKey(k => k.UserId);
            //  Property(r => r.UserId).HasMaxLength(128).IsRequired();
        }
    }
}
;