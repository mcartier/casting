﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface ITalentAsyncRepository
    {
        Task<Talent> GetTalentByIdAsync(int talentId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<Talent> GetTalentByIdAsync(int talentId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<IEnumerable<Talent>> GetTalentsByAgentIdAsync(int agentId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<IEnumerable<Talent>> GetTalentsByAgentIdAsync(int agentId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<IEnumerable<Talent>> GetTalentsByMainAgentIdAsync(int agentId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<IEnumerable<Talent>> GetTalentsByMainAgentIdAsync(int agentId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<IEnumerable<Talent>> GetTalentsByDirectorIdAsync(int directorId, ProfileTypeEnum profileType, bool withProfile, bool withAssets, bool withAttributes, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<IEnumerable<Talent>> GetTalentsByDirectorIdAsync(int directorId, bool withAllAgentReferences, bool withAllAgents, bool withMainAgent);
        Task<int> AddNewTalentAsync(Talent talent);
        Task<int> DeleteTalentAsync(Talent talent);
        Task<int> UpdateTalentAsync(Talent talent);
    }
}