﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Talents;

namespace Casting.BLL.CastingUsers.Interfaces
{
    public interface ITalentAgentService
    {
        Task<AgentTalentReference> GetTalentAgentReferenceByIdAsync(int referenceId);
        Task<AgentTalentReference> GetTalentAgentReferenceByIdWithAgentAndTalentAsync(int referenceId);
        Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByAgentIdAsync(int agentId);
        Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByTalentIdAsync(int talenId);
        Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByAgentIdWithAgentAndTalentAsync(int agentId);
        Task<IEnumerable<AgentTalentReference>> GetTalentAgentsByTalentIdWithAgentAndTalentAsync(int talenId);
        Task<int> AddNewAgentTalentReferenceAsync(AgentTalentReference agentTalentReference);
        Task<int> UpdateAgentTalentReferenceAsync(AgentTalentReference agentTalentReference);
        Task<int> DeleteAgentTalentReference(AgentTalentReference agentTalentReference);
    }
}