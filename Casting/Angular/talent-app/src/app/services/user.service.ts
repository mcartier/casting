import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { Observable, of } from 'rxjs';
import { StateService } from '@uirouter/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    constructor() { }
    checkAuthenticated() {

      return !(localStorage.getItem("currentUser") === null);
    }
}
