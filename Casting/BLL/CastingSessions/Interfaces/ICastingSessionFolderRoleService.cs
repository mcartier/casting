﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Sessions;

namespace Casting.BLL.CastingSessions.Interfaces
{
    public interface ICastingSessionFolderRoleService
    {
        Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdAsync(int castingSessionFolderRoleId);
        Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdWithPersonasAsync(int castingSessionFolderRoleId);
        Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdWithPersonasAndAssetsAsync(int castingSessionFolderRoleId);
        Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdAsync(int castingSessionFolderId);
        Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAsync(int castingSessionFolderId);
        Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAndAssetsAsync(int castingSessionFolderId);
        Task<int> AddNewCastingSessionFolderRoleAsync(bool orderAtTheTop, CastingSessionFolderRole castingSessionFolderRole);
        Task<int> UpdateCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole);
        Task<int> DeleteCastingSessionFolderRole(CastingSessionFolderRole castingSessionFolderRole);
    }
}