import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSidePulloutScrollComponent } from './main-side-pullout-scroll.component';

describe('MainSidePulloutScrollComponent', () => {
  let component: MainSidePulloutScrollComponent;
  let fixture: ComponentFixture<MainSidePulloutScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainSidePulloutScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSidePulloutScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
