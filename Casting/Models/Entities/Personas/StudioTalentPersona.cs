﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Talents;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas
{
   public class StudioTalentPersona:Persona
    {
        public StudioTalentPersona()
        {
            base.PersonaType = PersonaTypeEnum.StudioTalentPersona;
        }

        public int TalentId { get; set; }
        public StudioTalent Talent { get; set; }
    }
}
