﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Talents;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
    public class StudioTalentPersonaReference : PersonaReference
    {
        public StudioTalentPersonaReference()
        {
            base.PersonaReferenceType = PersonaReferenceTypeEnum.StudioTalent;
        }
        public int TalentId { get; set; }
        public StudioTalent Talent { get; set; }
    }
}
