import { Component, OnInit, OnDestroy, Inject, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ProductionDialogService, AuditionDetailDialogObject } from '../../services/production-dialog.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { AuditionDetail, ShootDetail, PayDetail, Side, PayPackageTypeEnum } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { Dialog } from 'primeng/dialog';
import { SelectItem } from 'primeng/api';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { RadioButton } from 'primeng/radiobutton';

@Component({
    selector: 'app-pay-details-dialog',
    templateUrl: './pay-details-dialog.component.html',
    styleUrls: ['./pay-details-dialog.component.scss']
})
export class PayDetailsDialogComponent implements OnInit, OnDestroy, AfterViewInit {

    constructor(private productionDialogService: ProductionDialogService,
        private productionExtraService: ProductionExtraService,
        private _formBuilder: FormBuilder,
        public mediaMatcher: MediaMatcher,
        private messageDispatchService: MessageDispatchService) {
    }
    @Input() payDetail: PayDetail;
    @Input() productionId: number;
    @ViewChild('formDirective', {static: false}) private formDirective: NgForm;
    @ViewChild('name', {static: false}) nameField: ElementRef;
    @ViewChild('detaildialog', {static: false}) detaildialog: Dialog;
    @ViewChild('payPackageType', {static: false}) payPackageTypeField: RadioButton

   
    bigSizeMatcher: MediaQueryList;
    smallSizeMatcher: MediaQueryList;
    isSmallDevice: boolean = false;

    private messages: any[] = [];
    private saveIsDisabled: boolean = false;
        private cancelIsDisabled: boolean = false;


    payPackageTypeEnum = PayPackageTypeEnum;

    isVisible: boolean = false;
    private dialogVisibleStateSubscription: Subscription;
    private basicDialogInfo: FormGroup;

    ngOnInit() {
        this.isSmallDevice = window.innerWidth < 768;

        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));


        this.basicDialogInfo = this._formBuilder.group({
            name: ['', Validators.required],
            payPackageType: [0, Validators.required],
            details: [''],
        },
            { updateOn: 'blur' });

        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToPayDetailsDialogShowStateObs()
            .subscribe(
                dialogObject => {
                    this.clearMessages();
                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    if (dialogObject.isVisible && dialogObject.payDetail != null) {
                        this.payDetail = dialogObject.payDetail;
                        this.basicDialogInfo.patchValue({
                            name: this.payDetail.name,
                            details: this.payDetail.details,
                            payPackageType: this.payDetail.payPackageType,

                        });
                    } else {
                        this.payDetail = null;
                        this.basicDialogInfo.patchValue({
                            name: '',
                            details: '',
                            payPackageType: 0,

                        });

                    }
                    this.isVisible = dialogObject.isVisible;
                    // this.detaildialog.maximized = true;
                    //this.detaildialog.toggleMaximize();
                    this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                    this.nameField.nativeElement.focus();

                });


    }

    ngAfterViewInit() {


    }

    ngOnDestroy() {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();

        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);

    }
    private clearMessages() {
        this.messages = [];
    }

    mediaQueryBigChangeListener(event) {

        if (event.matches) {
            this.isSmallDevice = false;
         }
    }
    mediaQuerySmallChangeListener(event) {
        if (event.matches) {

            this.isSmallDevice = true;
        }
    }
    private payTypeOptionClick(payPackageType: PayPackageTypeEnum) {
      this.payTypeChange(payPackageType);

    }

    private payTypeChange(payPackageType: PayPackageTypeEnum) {
     
    }

    private onDialogShow(event: any, dialog: Dialog) {
       if (this.isSmallDevice) {
          setTimeout(() => {
                dialog.maximize();
            },
                0);
        }
    }
    private onDialogHide(event: any, dialog: Dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();
    }
    private cancelClick() {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
    }
    private resetDialog() {
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.messages = [];

    }

    private saveClick() {
        this.clearMessages();

        if (this.basicDialogInfo.valid) {
            this.saveIsDisabled = true;
            this.cancelIsDisabled = true;

            let tpayDetail: PayDetail = new PayDetail;
            tpayDetail.productionId = this.productionId;
            tpayDetail.name = this.basicDialogInfo.get('name').value;
            tpayDetail.details = this.basicDialogInfo.get('details').value;
            tpayDetail.payPackageType = this.basicDialogInfo.get('payPackageType').value;
            if (this.payDetail && this.payDetail.id && this.payDetail.id !== 0) {
              this.productionExtraService.savePayDetail(this.payDetail.id, this.productionId, tpayDetail).subscribe(
                (payDetail: PayDetail) => {
                  //this.payDetail = payDetail;
                  this.formDirective.resetForm();
                  this.basicDialogInfo.reset();
                  this.isVisible = false;
                  this.messageDispatchService.dispatchSuccesMessage(
                    "Pay detail '" + payDetail.name + "' was saved successfully",
                    "Pay Detail Saved");

                },
                (error) => {
                  this.saveIsDisabled = false;
                  this.cancelIsDisabled = false;

                  this.messageDispatchService.dispatchErrorMessage(
                    "Error saving pay detail '" + tpayDetail.name + "'. " + (error)
                    ? (error.error)
                    ? (error.error.exceptionMessage)
                    ? error.error.exceptionMessage
                    : error.error
                    : error
                    : "unknown error",
                    "Pay Detail Save Error");
                  this.messages.push({
                    severity: 'error',
                    summary: "Pay Detail Save Error",
                    detail: "Error saving pay detail '" + tpayDetail.name + "'. " + (error)
                      ? (error.error)
                      ? (error.error.exceptionMessage)
                      ? error.error.exceptionMessage
                      : error.error
                      : error
                      : "unknown error"
                  });
                });

            } else {
              this.productionExtraService.createPayDetail(this.productionId, tpayDetail).subscribe(
                (payDetail: PayDetail) => {
                  if (payDetail != null) {
                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    this.isVisible = false;
                    this.messageDispatchService.dispatchSuccesMessage(
                      "New pay detail '" + payDetail.name + "' was created successfully",
                      "Pay Detail Created");

                  }

                },
                (error) => {
                  this.saveIsDisabled = false;
                  this.cancelIsDisabled = false;

                  this.messageDispatchService.dispatchErrorMessage(
                    "Error creating pay detail '" + tpayDetail.name + "'. " + (error)
                    ? (error.error)
                    ? (error.error.exceptionMessage)
                    ? error.error.exceptionMessage
                    : error.error
                    : error
                    : "unknown error",
                    "Pay Detail Creation Error");
                  this.messages.push({
                    severity: 'error',
                    summary: "Pay Detail Creation Error",
                    detail: "Error creating pay detail '" + tpayDetail.name + "'. " + (error)
                      ? (error.error)
                      ? (error.error.exceptionMessage)
                      ? error.error.exceptionMessage
                      : error.error
                      : error
                      : "unknown error"
                  });
                });

            }

        } else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('payPackageType').valid) {
              this.basicDialogInfo.get('payPackageType').markAsDirty();
              this.payPackageTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
              this.payPackageTypeField.inputViewChild.nativeElement.focus();

            }

        }
    }
}
