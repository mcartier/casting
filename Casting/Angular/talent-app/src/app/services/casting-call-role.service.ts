import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';

import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { CastingCall, CastingCallRole, NewCastingCallRole, UpdateCastingCallRole, UpdateCastingCall, NewCastingCall } from '../models/casting-call.models';
import { WINDOW } from './browser.window.provider';
import { HttpHelpers } from '../utilities/HttpHelpers';
@Injectable({
    providedIn: 'root'
})
export class CastingCallRoleService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }
    private _deleteRoleObs: Subject<CastingCallRole> = new Subject<CastingCallRole>();
    public readonly deleteRoleObs: Observable<CastingCallRole> = this._deleteRoleObs.asObservable();

    private _editRoleObs: Subject<CastingCallRole> = new Subject<CastingCallRole>();
    public readonly editRoleObs: Observable<CastingCallRole> = this._editRoleObs.asObservable();

    private _newRoleObs: Subject<CastingCallRole> = new Subject<CastingCallRole>();
    public readonly newRoleObs: Observable<CastingCallRole> = this._newRoleObs.asObservable();

    public subscribeToCreateRoleObs(): Observable<CastingCallRole> {
      return this.newRoleObs;
    }
    public subscribeToEditRoleObs(): Observable<CastingCallRole> {
      return this.editRoleObs;
    }

    public subscribeToDeleteRoleObs(): Observable<CastingCallRole> {
      return this.deleteRoleObs;
    }

    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;
    private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/roles/";
        return "/api/castingcallroles/";
    }

    public createRole(id: number, role: CastingCallRole): Observable<CastingCallRole> {
      role.castingCallId = id;
        let controllerQuery = this.getBaseURL();
        return this.postAction<CastingCallRole>(role, controllerQuery)
            .pipe(
            map((res: CastingCallRole) => {
                    //do anything with the returned data here...
                this._newRoleObs.next(res);
                    return res
                })
            )
    };

    public saveRole(id: number, role: CastingCallRole): Observable<CastingCallRole> {
        let controllerQuery = this.getBaseURL() + id;
        return this.putAction<CastingCallRole>(role, controllerQuery)
          .pipe(
            map((res: CastingCallRole) => {
              //do anything with the returned data here...
              this._editRoleObs.next(res);
              return res;
            })
          );
    };


    public deleteRole(roleId: number): Observable<CastingCallRole> {
        let controllerQuery = this.getBaseURL() + roleId;
        return this.deleteAction<CastingCallRole>(controllerQuery)
          .pipe(
            map((res: CastingCallRole) => {
              //do anything with the returned data here...
              this._deleteRoleObs.next(res);
              return res;
            })
          );
    };



    public getRoles(orderByIndex: number): Observable<CastingCallRole[]> {
        let controllerQuery = this.getBaseURL() + "?orderBy=" + orderByIndex;
        return this.getAction<CastingCallRole[]>(controllerQuery)
            .pipe(
            map((res: CastingCallRole[]) => {
                    //do anything with the returned data here...
                    return res;
            })
            );
    };

    public getRole(roleId: number): Observable<CastingCallRole> {
        let controllerQuery = this.getBaseURL() + roleId;
        return this.getAction<CastingCallRole>(controllerQuery)
            .pipe(
            map((res: CastingCallRole) => {
                    //do anything with the returned data here...
                    return res;
            })
            );
    };
}
