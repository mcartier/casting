﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Casting.Web.Models
{
    // Models used as parameters to AccountController actions.

    public class RoleAttributeReferenceBindingModel
    {


        [Required]
        [Display(Name = "Attribute Id")]
        public int AttributeId { get; set; }

        [Required]
        [Display(Name = "Role Id")]
        public int RoleId { get; set; }


    }

    public class ProfileAttributeReferenceBindingModel
    {


        [Required]
        [Display(Name = "Attribute Id")]
        public int AttributeId { get; set; }

        [Required]
        [Display(Name = "Profile Id")]
        public int ProfileId { get; set; }


    }

    public class CastingCallAttributeReferenceBindingModel
    {


        [Required]
        [Display(Name = "Attribute Id")]
        public int AttributeId { get; set; }

        [Required]
        [Display(Name = "CastingCall Id")]
        public int CastingCallId { get; set; }


    }
}
