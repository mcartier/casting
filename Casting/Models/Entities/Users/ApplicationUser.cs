﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Security.Claims;
using System.Threading.Tasks;

using Casting.Model.Enums;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;


namespace Casting.Model.Entities.Users
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {

        }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public ApplicationUserTypeEnum ApplicationUserType { get; set; }
        [Key, Column(Order = 1), ForeignKey("BaseCastingUser")]
         public int BaseUserId { get; set; }

        private BaseCastingUser _baseCastingUser;
        public BaseCastingUser BaseCastingUser
        {
            get { return _baseCastingUser; }
            set
            {
                _baseCastingUser = value;
                if (_baseCastingUser != null)
                {
                    this.ApplicationUserType = _baseCastingUser.ApplicationUserType;
                }

            }
        }
      }


}