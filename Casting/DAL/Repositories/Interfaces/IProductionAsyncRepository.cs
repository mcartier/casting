﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IProductionAsyncRepository : IAsyncRepository, IDisposable
    {
        #region Productions

        Task<Production> GetProductionByIdAsync(bool withRoles, bool withRoleMetadata, bool withCastingCalls, bool withSessions, bool withAuditionData, int productionId);
        Task<int> AddNewProductionAsync(Production production);
        Task<int> DeleteProductionAsync(Production production);
        Task<int> UpdateProductionAsync(Production production);

        Task<IEnumerable<Production>> GetProductionsByDirectorIdAsync(bool withRoles, bool withRoleMetadata, bool withCastingCalls,
            bool withSessions, bool withAuditionData, int directorId, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending);
        Task<IEnumerable<Production>> GetProductionsByDirectorIdAsync(bool withRoles, bool withRoleMetadata, bool withCastingCalls,
            bool withSessions, bool withAuditionData, int directorId, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending);

        #endregion

        #region AuditionData
        Task<PayDetail> GetPayDetailByIdAsync(int payDetailId, bool withAttributes);
        Task<int> AddNewPayDetailAsync(PayDetail payDetail);
        Task<int> DeletePayDetailAsync(PayDetail payDetail);
        Task<int> UpdatePayDetailAsync(PayDetail payDetail);
        Task<IEnumerable<PayDetail>> GetPayDetailsByProductionIdAsync(int productionId, bool withAttributes);
        Task<Side> GetSideByIdAsync(int sideId, bool withAttributes);
        Task<int> AddNewSideAsync(Side side);
        Task<int> DeleteSideAsync(Side side);
        Task<int> UpdateSideAsync(Side side);
        Task<IEnumerable<Side>> GetSidesByProductionIdAsync(int productionId, bool withAttributes);
        Task<ShootDetail> GetShootDetailByIdAsync(int shootDetailId, bool withAttributes);
        Task<int> AddNewShootDetailAsync(ShootDetail shootDetail);
        Task<int> DeleteShootDetailAsync(ShootDetail shootDetail);
        Task<int> UpdateShootDetailAsync(ShootDetail shootDetail);
        Task<IEnumerable<ShootDetail>> GetShootDetailsByProductionIdAsync(int productionId, bool withAttributes);
        Task<AuditionDetail> GetAuditionDetailByIdAsync(int auditionDetailId, bool withAttributes);
        Task<int> AddNewAuditionDetailAsync(AuditionDetail auditionDetail);
        Task<int> DeleteAuditionDetailAsync(AuditionDetail auditionDetail);
        Task<int> UpdateAuditionDetailAsync(AuditionDetail auditionDetail);
        Task<IEnumerable<AuditionDetail>> GetAuditionDetailsByProductionIdAsync(int productionId, bool withAttributes);


      
        #endregion
        #region ProductionRoles
        Task<Role> GetRoleByIdAsync(int roleId, bool withAttributes);
        Task<int> AddNewRoleAsync(Role role);
        Task<int> DeleteRoleAsync(Role role);
        Task<int> UpdateRoleAsync(Role role);
        Task<IEnumerable<Role>> GetRolesByProductionIdAsync(int productionId, bool withAttributes);


        #endregion
    }

}
