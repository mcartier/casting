﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.BLL.CastingSessions.Interfaces;

namespace Casting.BLL.CastingSessionFolders
{
    public class CastingSessionFolderRoleService : BaseService, ICastingSessionFolderRoleService
    {
        private ICastingSessionAsyncRepository _castingSessionRepository;

        public CastingSessionFolderRoleService(ICastingSessionAsyncRepository castingSessionAsyncRepository)
        {
            _castingSessionRepository = castingSessionAsyncRepository;
        }

        public async Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdAsync(int castingSessionFolderRoleId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderRoleByIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum.Roles, castingSessionFolderRoleId);
        }
        public async Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdWithPersonasAsync(int castingSessionFolderRoleId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderRoleByIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum.Personas, castingSessionFolderRoleId);
        }
        public async Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdWithPersonasAndAssetsAsync(int castingSessionFolderRoleId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderRoleByIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum.PersonasAndAssets, castingSessionFolderRoleId);
        }

        public async Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderRolesByCastingSessionFolderIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum.Roles,  castingSessionFolderId);
        }

        public async Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderRolesByCastingSessionFolderIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum.Personas,  castingSessionFolderId);
        }

        public async Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdWithPersonasAndAssetsAsync(int castingSessionFolderId)
        {
            return await _castingSessionRepository.GetCastingSessionFolderRolesByCastingSessionFolderIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum.PersonasAndAssets,  castingSessionFolderId);
        }

        public async Task<int> AddNewCastingSessionFolderRoleAsync(bool orderAtTheTop, CastingSessionFolderRole castingSessionFolderRole)
        {
            return await _castingSessionRepository.AddNewCastingSessionFolderRoleAsync(orderAtTheTop, castingSessionFolderRole);
        }

        public async Task<int> UpdateCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole)
        {
            return await _castingSessionRepository.UpdateCastingSessionFolderRoleAsync(castingSessionFolderRole);
        }
        public async Task<int> DeleteCastingSessionFolderRole(CastingSessionFolderRole castingSessionFolderRole)
        {
            return await _castingSessionRepository.DeleteCastingSessionFolderRoleAsync(castingSessionFolderRole);
        }
    }
}
