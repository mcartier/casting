﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Microsoft.Owin;
using Owin;
using System.Threading.Tasks;
using Casting.Web.Tus.Extensions;
using tusdotnet.Models;
using tusdotnet.Models.Concatenation;
using tusdotnet.Models.Configuration;
using tusdotnet.Models.Expiration;
using tusdotnet.Stores;
using tusdotnet;
using tusdotnet.Interfaces;
using System.Text;
using Casting.Web.DependencyResolution;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using Casting.BLL.CastingUsers.Interfaces;
using StructureMap;
using System.Web.Mvc;
using Newtonsoft.Json.Serialization;

[assembly: OwinStartup(typeof(Casting.Web.Startup))]

namespace Casting.Web
{
    public partial class Startup
    {

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            var container = new Container();
            DependencyResolver.SetResolver(new StructureMapWebApiDependencyResolver(container));
            config.DependencyResolver = new StructureMapWebApiDependencyResolver(container);

            ConfigureAuth(app);
               // Web API routes
            config.MapHttpAttributeRoutes();
            
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;

            app.UseWebApi(config);
            ConfigureTus(app);
           // IContainer container = IoC.Initialize();
             

}

    }
}
