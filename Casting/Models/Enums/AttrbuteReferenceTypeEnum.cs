﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum AttributeReferenceTypeEnum
    {
        Profile = 1,
        Role = 3,
        Search = 7,
        CastingCall = 15,
        CastingCallRole = 31
    }
}
