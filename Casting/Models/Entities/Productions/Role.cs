﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions
{
   public class Role : Logger
    {
        public Role()
        {
            EyeColors = new List<RoleEyeColorReference>();
            HairColors = new List<RoleHairColorReference>();
            BodyTypes = new List<RoleBodyTypeReference>();
            Ethnicities = new List<RoleEthnicityReference>();
            EthnicStereoTypes = new List<RoleEthnicStereoTypeReference>();
            StereoTypes = new List<RoleStereoTypeReference>();
            Accents = new List<RoleAccentReference>();
           Languages = new List<RoleLanguageReference>();
            CastingCallRoles = new List<CastingCallRole>();
            CastingSessionFolderRoles = new List<CastingSessionFolderRole>();
            CastingSessionRoles = new List<CastingSessionRole>();
      Skills = new List<RoleSkillReference>();

}
    public int Id { get; set; }
      
        public int ProductionId { get; set; }
        public Production Production { get; set; }

        public string Description { get; set; }
        public string Name { get; set; }

        public long MaxAge { get; set; }

        public long MinAge { get; set; }
        public bool RequiresNudity { get; set; }
        public bool Transgender { get; set; }
        public int RoleTypeId { get; set; }
        public RoleType RoleType { get; set; }
        public int RoleGenderTypeId { get; set; }
        public RoleGenderType RoleGenderType { get; set; }
        public int? PayDetailId { get; set; }
        public PayDetail PayDetail { get; set; }
        //public Ethnicity Ethnicity { get; set; }
        //public int EthnicityId { get; set; }
        public ICollection<RoleSkillReference> Skills { get; set; }
        public ICollection<RoleBodyTypeReference> BodyTypes { get; set; }
        public ICollection<RoleHairColorReference> HairColors { get; set; }
        public ICollection<RoleEyeColorReference> EyeColors { get; set; }
        public ICollection<RoleAccentReference> Accents { get; set; }
        public ICollection<RoleLanguageReference> Languages { get; set; }
        public ICollection<RoleEthnicityReference> Ethnicities { get; set; }
        public ICollection<RoleStereoTypeReference> StereoTypes { get; set; }
        public ICollection<RoleEthnicStereoTypeReference> EthnicStereoTypes { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }
        [NotMapped]
        public int CastingCallRolesCount { get; set; }
        public ICollection<CastingCallRole> CastingCallRoles { get; set; }
        [NotMapped]
        public int CastingCallFolderRolesCount { get; set; }
        public ICollection<CastingCallFolderRole> CastingCallFolderRoles { get; set; }
        [NotMapped]
        public int CastingSessionRolesCount { get; set; }
        public ICollection<CastingSessionRole> CastingSessionRoles { get; set; }
        [NotMapped]
        public int CastingSessionFolderRolesCount { get; set; }
        public ICollection<CastingSessionFolderRole> CastingSessionFolderRoles { get; set; }
        public int OrderIndex { get; set; }
    }
}
