﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Productions;


namespace Casting.DAL.Configurations
{

    public class ProductionConfiguration : EntityTypeConfiguration<Production>
    {
        public ProductionConfiguration()
        {
            HasKey(x => x.Id);
            HasMany(s => s.Roles)
                .WithRequired(p => p.Production)
                .HasForeignKey(k => k.ProductionId);
            //HasMany(u=>u.UnionRequirements);
            HasRequired(t => t.ProductionType)
                .WithMany(p => p.Productions)
                .HasForeignKey(k => k.ProductionTypeId);
            HasMany(c => c.CastingCalls)
                .WithRequired(p => p.Production)
                .HasForeignKey(k => k.ProductionId);
            HasMany(c => c.CastingSessions)
                .WithRequired(c => c.Production)
                .HasForeignKey(k => k.ProductionId);
            HasRequired(d => d.Director)
                .WithMany(p => p.Productions);
            HasMany(s => s.Sides)
                .WithRequired(c => c.Production)
                .HasForeignKey(k => k.ProductionId);
            HasMany(s => s.AuditionDetails)
                .WithRequired(c => c.Production)
                .HasForeignKey(k => k.ProductionId);
            HasMany(s => s.PayDetails)
                .WithRequired(c => c.Production)
                .HasForeignKey(k => k.ProductionId);
            HasMany(s => s.ShootDetails)
                .WithRequired(c => c.Production)
                .HasForeignKey(k => k.ProductionId);
        }
    }
    public class RoleConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(p => p.Production)
                .WithMany(r => r.Roles)
                .HasForeignKey(k => k.ProductionId);
            HasRequired(g => g.RoleGenderType);
            HasRequired(r => r.RoleType);
            HasOptional(p => p.PayDetail);
            HasMany(e => e.Languages);
            HasMany(e => e.Accents);
            HasMany(e => e.Ethnicities);
            //.WithMany(r => r.Roles)
            // .HasForeignKey(k => k.EthnicityId);

            HasMany(s => s.StereoTypes);
            //  .WithMany(r => r.Roles);
            HasMany(e => e.EthnicStereoTypes);
            HasMany(e => e.HairColors);
            HasMany(e => e.EyeColors);
            HasMany(e => e.BodyTypes);
            HasMany(e => e.Skills);
            // .WithMany(r => r.Roles);

        }
    }

    public class SideConfiguration : EntityTypeConfiguration<Side>
    {
        public SideConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(p => p.Production)
                .WithMany(r => r.Sides)
                .HasForeignKey(k => k.ProductionId);

        }
    }
    public class AuditionDetailConfiguration : EntityTypeConfiguration<AuditionDetail>
    {
        public AuditionDetailConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(p => p.Production)
                .WithMany(r => r.AuditionDetails)
                .HasForeignKey(k => k.ProductionId);

        }
    }
    public class PayDetailConfiguration : EntityTypeConfiguration<PayDetail>
    {
        public PayDetailConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(p => p.Production)
                .WithMany(r => r.PayDetails)
                .HasForeignKey(k => k.ProductionId);

        }
    }
    public class ShootDetailConfiguration : EntityTypeConfiguration<ShootDetail>
    {
        public ShootDetailConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(p => p.Production)
                .WithMany(r => r.ShootDetails)
                .HasForeignKey(k => k.ProductionId);

        }
    }

    
    /*class ProductionProductionTypeReferenceConfiguration : EntityTypeConfiguration<ProductionProductionTypeReference>
        {
            public ProductionProductionTypeReferenceConfiguration()
            {
                HasKey(x => x.Id);
                HasRequired(t => t.Production)
                    .WithMany(a => a.ProductionTypeReferences)
                    .HasForeignKey(k => k.ProductionId);
                HasRequired(a => a.ProducuctionType)
                    .WithMany(t => t.ProductionReferences)
                    .HasForeignKey(k => k.ProductionTypeId);
            }
        }*/

}
