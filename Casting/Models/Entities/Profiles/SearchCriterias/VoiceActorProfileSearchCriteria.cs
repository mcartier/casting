﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Profiles.SearchCriterias
{
    public class VoiceActorProfileSearchCriteria: ProfileSearchCriteria
    {
        public int VoiceType { get; set; }

    }
}
