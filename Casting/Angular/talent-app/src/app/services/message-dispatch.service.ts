import { Injectable } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { Message } from 'primeng/api';
@Injectable({
  providedIn: 'root'
})
export class MessageDispatchService {

    constructor() { }

    private _messageDispatcherObs: Subject<Message[]> = new Subject<Message[]>();
    public readonly messageDispatcherObs: Observable<Message[]> = this._messageDispatcherObs.asObservable();

    public subscribeToMessageDispatcherObs(): Observable<Message[]> {
      return this._messageDispatcherObs;
    }

    public dispatchSuccesMessage(message: string, summary: string) {
      let messages: Message[] = [];
        messages.push({ severity: 'success', summary: summary, detail: message });
      this._messageDispatcherObs.next(messages);
    }

    public dispatchInfoMessage(message: string, summary: string) {
      let messages: Message[] = [];
      messages.push({ severity: 'info', summary: summary, detail: message });
      this._messageDispatcherObs.next(messages);
    }

    public dispatchWarningMessage(message: string, summary: string) {
      let messages: Message[] = [];
      messages.push({ severity: 'warn', summary: summary, detail: message });
      this._messageDispatcherObs.next(messages);
    }

    public dispatchErrorMessage(message: string, summary: string) {
      let messages: Message[] = [];
      messages.push({ severity: 'error', summary: summary, detail: message });
      this._messageDispatcherObs.next(messages);
    }

}
