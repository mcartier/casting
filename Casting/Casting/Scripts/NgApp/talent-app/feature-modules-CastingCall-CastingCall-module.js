(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["feature-modules-campaign-campaign-module"],{

/***/ "./src/app/feature-modules/campaign/campaign.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/feature-modules/campaign/campaign.module.ts ***!
  \*************************************************************/
/*! exports provided: CampaignModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignModule", function() { return CampaignModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../interceptors/errorInterceptor */ "./src/app/interceptors/errorInterceptor.ts");
/* harmony import */ var _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../interceptors/jwtInterceptor */ "./src/app/interceptors/jwtInterceptor.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _campaign_states__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./campaign.states */ "./src/app/feature-modules/campaign/campaign.states.ts");
/* harmony import */ var _services_campaign_component_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/campaign-component.service */ "./src/app/feature-modules/campaign/services/campaign-component.service.ts");
/* harmony import */ var _components_new_campaign_new_campaign_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/new-campaign/new-campaign.component */ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.ts");













var CampaignModule = /** @class */ (function () {
    function CampaignModule() {
    }
    CampaignModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_components_new_campaign_new_campaign_component__WEBPACK_IMPORTED_MODULE_11__["NewCampaignComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__["UIRouterModule"].forChild({ states: _campaign_states__WEBPACK_IMPORTED_MODULE_9__["CAMPAIGN_STATES"] }),
                _global_global_module__WEBPACK_IMPORTED_MODULE_5__["GlobalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            ],
            exports: [
            // DropFileDirective,
            ],
            entryComponents: [],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HTTP_INTERCEPTORS"], useClass: _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_7__["JwtInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HTTP_INTERCEPTORS"], useClass: _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_6__["ErrorInterceptor"], multi: true },
                _services_campaign_component_service__WEBPACK_IMPORTED_MODULE_10__["CampaignComponentService"]
            ],
        })
    ], CampaignModule);
    return CampaignModule;
}());



/***/ }),

/***/ "./src/app/feature-modules/campaign/campaign.states.ts":
/*!*************************************************************!*\
  !*** ./src/app/feature-modules/campaign/campaign.states.ts ***!
  \*************************************************************/
/*! exports provided: getMetadataFromServer, getEmptyCampaign, getCampaignFromTransactionOrServer, getCampaignsFromServer, getProductionCampaignsFromServer, getEmptyProduction, getProductionFromTransactionOrServer, getProductionsFromServer, newcampaignState, draftcampaignState, editcampaignState, CAMPAIGN_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMetadataFromServer", function() { return getMetadataFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyCampaign", function() { return getEmptyCampaign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCampaignFromTransactionOrServer", function() { return getCampaignFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCampaignsFromServer", function() { return getCampaignsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionCampaignsFromServer", function() { return getProductionCampaignsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyProduction", function() { return getEmptyProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionFromTransactionOrServer", function() { return getProductionFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionsFromServer", function() { return getProductionsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newcampaignState", function() { return newcampaignState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draftcampaignState", function() { return draftcampaignState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editcampaignState", function() { return editcampaignState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CAMPAIGN_STATES", function() { return CAMPAIGN_STATES; });
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/state-transition-handler.service */ "./src/app/services/state-transition-handler.service.ts");
/* harmony import */ var _components_new_campaign_new_campaign_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/new-campaign/new-campaign.component */ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.ts");



function getMetadataFromServer(stateTransitionService, transition) {
    var metaDate;
    var promise;
    return stateTransitionService.getMetadataFromServer(transition);
}
function getEmptyCampaign() {
    var prod;
    return prod;
}
function getCampaignFromTransactionOrServer(stateTransitionService, transition) {
    var campaignId;
    var campaign;
    var promise;
    return stateTransitionService.getCampaignFromTransactionOrServer(transition);
}
function getCampaignsFromServer(stateTransitionService, transition) {
    var campaigns;
    var promise;
    return stateTransitionService.getCampaignsFromServer(transition);
}
function getProductionCampaignsFromServer(stateTransitionService, transition) {
    var campaigns;
    var productionId;
    return stateTransitionService.getProductionCampaignsFromServer(transition);
}
function getEmptyProduction() {
    var prod;
    return prod;
}
function getProductionFromTransactionOrServer(stateTransitionService, transition) {
    var productionId;
    var production;
    var promise;
    return stateTransitionService.getProductionFromTransactionOrServer(transition);
}
function getProductionsFromServer(stateTransitionService, transition) {
    var productions;
    var promise;
    return stateTransitionService.getProductionsFromServer(transition);
}
/*
export const campaignListState = {
  name: 'campaigns',
  url: '/campaigns',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: CampaignsComponent }
  },
  resolve: [
    {
      token: 'campaigns', deps: [StateTransitionHandlerService, Transition], resolveFn: getCampaignsFromServer
    },
  ]


};
export const productionCampaignListState = {
  name: 'productioncampaigns',
  url: '/productions/:productionId/campaigns',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: CampaignsComponent }
  },
  params: {
    production: null
  },
  resolve: [
    {
      token: 'campaigns', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionCampaignsFromServer
    },
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
    },

  ]


};
export const campaignState = {
  name: 'campaign',
  url: '/productions/:productionId/campaigns/:campaignId',
  parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: CampaignComponent }
  },
  params: {
    campaign: null
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'campaign', deps: [StateTransitionHandlerService, Transition], resolveFn: getCampaignFromTransactionOrServer
    },
  ]
};
*/
var newcampaignState = {
    name: 'newcampaign',
    url: '/productions/:productionId/campaigns/new',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_campaign_new_campaign_component__WEBPACK_IMPORTED_MODULE_2__["NewCampaignComponent"] },
    },
    params: {
        //production: null,
        campaign: null,
        popRole: false,
        isDraft: false,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ]
};
var draftcampaignState = {
    name: 'draftcampaign',
    url: '/campaigns/draft/:draftId',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_campaign_new_campaign_component__WEBPACK_IMPORTED_MODULE_2__["NewCampaignComponent"] },
    },
    params: {
        campaign: null,
        popRole: false,
        isDraft: true,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'campaign', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getCampaignFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var editcampaignState = {
    name: 'editcampaign',
    url: '/productions/:productionId/campaigns/:campaignId/edit',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_campaign_new_campaign_component__WEBPACK_IMPORTED_MODULE_2__["NewCampaignComponent"] },
    },
    params: {
        campaign: null,
        popRole: false,
        isDraft: false,
        isEdit: true
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'campaign', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getCampaignFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_1__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var CAMPAIGN_STATES = [
    // productionCampaignListState,
    // campaignListState,
    // campaignState,
    newcampaignState,
    draftcampaignState,
    editcampaignState,
];


/***/ }),

/***/ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"p-grid\">\r\n    <div class=\"p-col-9\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n            <form [formGroup]=\"basicCampaignInfo\">\r\n\r\n                <p-card class=\"p-col\" header=\"Create a new campaign\">\r\n                    <p-header>\r\n                        <div #basiccard></div>\r\n                    </p-header>\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Name of Campaign</label>\r\n                                <input class=\"p-col\" pInput placeholder=\"{{namePlaceHolder}}\" #name formControlName=\"name\" required>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"Campaign Name is <strong>required</strong>\" *ngIf=\"basicCampaignInfo.controls['name'].hasError('required') && basicCampaignInfo.controls['name'].dirty\"></p-message>\r\n                               <div class=\"p-col\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n                            </div>\r\n                        </div>\r\n                      <div class=\"p-col\">\r\n                        <div class=\"p-grid p-dir-col p-justify-between\">\r\n                          <label class=\"p-col\">Campaign Type</label>\r\n                          <p-dropdown class=\"p-col\" [options]=\"metadata.campaignTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" #campaignTypeId placeholder=\"Select a Type of Campaign\" formControlName=\"campaignTypeId\" required>\r\n                          </p-dropdown>\r\n                          <p-message severity=\"error\" class=\"p-col\" text=\"Campaign Type is <strong>required</strong>\" *ngIf=\"basicCampaignInfo.controls['campaignTypeId'].hasError('required') && basicCampaignInfo.controls['campaignTypeId'].dirty\">\r\n                                    \r\n                          </p-message>\r\n                          <div class=\"p-col\" *ngIf=\"showHints\">Select a Type of Campaign</div>\r\n                        </div>\r\n                      </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Publish this campaign?</label>\r\n                                <p-radioButton #campaignViewType class=\"p-col\" name=\"campaignViewTypes\" value=\"7\" label=\"Make it public and visible to anyone\" formControlName=\"campaignViewType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"campaignViewTypes\" value=\"3\" label=\"Make it public and visible only to members\" formControlName=\"campaignViewType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"campaignViewTypes\" value=\"1\" label=\"Make it private and visible to only select members\" formControlName=\"campaignViewType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"campaignViewTypes\" value=\"15\" label=\"By invitation only\" formControlName=\"campaignViewType\"></p-radioButton>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"Campaign Type is <strong>required</strong>\" *ngIf=\"basicCampaignInfo.controls['campaignViewType'].hasError('required') && basicCampaignInfo.controls['campaignViewType'].dirty\">\r\n                                </p-message>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">When should this campaign expire?</label>\r\n                                <p-calendar [inputStyleClass]=\"p-col\" #expires required formControlName=\"expires\" (onSelect)=\"selectExpireDate($event)\" dataType=\"string\" placeholder=\"Chose an expiration date\" dateFormat=\"DD MM d yy\" [minDate]=\"minDate\" [showIcon]=\"true\" [readonlyInput]=\"true\"></p-calendar>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"Campaign Expiration Date is <strong>required</strong>\" *ngIf=\"basicCampaignInfo.controls['expires'].hasError('required') && basicCampaignInfo.controls['expires'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">This is the date at which this campaign will not be publicly visible any more</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Do you have any special submission or audition instructions?</label>\r\n                                <textarea rows=\"3\" class=\"p-col\" pInput placeholder=\"e.g., 'In your cover letter, note your availability. Include a video with your submission. For the auditions, be prepared to sing. For more info about the project, visit www.example.com.'\" formControlName=\"instructions\"></textarea>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">You don't need to include your email address here. Instead, you can manage your email settings in the next section.</div>\r\n                            </div>\r\n                        </div>\r\n                       \r\n                    </div>\r\n\r\n                </p-card>\r\n\r\n                <p-card header=\"Unions\" class=\"p-col\">\r\n                    <p-header>\r\n                        <div #unionscard></div>\r\n                    </p-header>\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Unions</label>\r\n                              <p-multiSelect class=\"p-col\" formControlName=\"unions\" (onChange)=\"changeUnionSelection($event)\" [options]=\"metadata.unions\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Unions\">\r\n                              </p-multiSelect>\r\n                                 <div class=\"p-col\" *ngIf=\"showHints\">Select Unions to include in the campaign</div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </p-card>\r\n\r\n              <p-card header=\"Roles\" class=\"p-col\">\r\n                <p-header>\r\n                  <div #rolescard></div>\r\n                </p-header>\r\n\r\n                <div class=\"p-grid p-dir-col p-justify-around\">\r\n                  <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                      <label class=\"p-col\">Roles</label>\r\n                      <p-multiSelect class=\"p-col\" formControlName=\"roles\" (onChange)=\"changeRoleSelection($event)\" [options]=\"production.roles\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Roles\">\r\n                      </p-multiSelect>\r\n                      <div class=\"p-col\" *ngIf=\"showHints\">Select Roles to include in the campaign</div>\r\n                    </div>\r\n\r\n                  </div>\r\n                </div>\r\n              </p-card>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-justify-end p-align-center\">\r\n                  <button pButton (click)=\"cancelEditCampaignClick()\">Cancel</button>\r\n                  <button pButton  (click)=\"saveCampaignClick()\">Save</button>\r\n\r\n                </div>\r\n              </div>\r\n            </form>\r\n\r\n            \r\n        </div>\r\n    </div>\r\n    <div class=\"p-col-3\">\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n\r\n            <button pButton class=\"p-col\" (click)=\"scrollToBasic()\">go Basic</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToRoles()\">go Roles</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToCasting()\">go Casting</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToCampaign()\">go Campaign</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToArtistic()\">go Artistic</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToOther()\">go Other</button>\r\n            <button pButton class=\"p-col\" (click)=\"saveCampaignClick()\">Save</button>\r\n            <button pButton class=\"p-col\" (click)=\"deleteCampaignClick()\">Delete</button>\r\n            <button pButton class=\"p-col\" (click)=\"cancelEditCampaignClick()\">Cancel</button>\r\n        </div>\r\n\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9jYW1wYWlnbi9jb21wb25lbnRzL25ldy1jYW1wYWlnbi9uZXctY2FtcGFpZ24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.ts ***!
  \********************************************************************************************/
/*! exports provided: NewCampaignComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCampaignComponent", function() { return NewCampaignComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_campaign_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/campaign.models */ "./src/app/models/campaign.models.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_campaign_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/campaign.service */ "./src/app/services/campaign.service.ts");
/* harmony import */ var _services_campaign_component_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/campaign-component.service */ "./src/app/feature-modules/campaign/services/campaign-component.service.ts");
/* harmony import */ var _services_campaign_role_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../services/campaign-role.service */ "./src/app/services/campaign-role.service.ts");
/* harmony import */ var _animations_router_animation__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../animations/router.animation */ "./src/app/animations/router.animation.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/dropdown.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/calendar.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(primeng_calendar__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/radiobutton.js");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(primeng_radiobutton__WEBPACK_IMPORTED_MODULE_14__);








//import { RoleDialogService } from '../../services/role-dialog.service';



//import { NewRoleComponent } from '../new-role/new-role.component'




var NewCampaignComponent = /** @class */ (function () {
    function NewCampaignComponent(CampaignRoleService, _formBuilder, metadataService, campaignService, stateService, transitionService, transition, campaignComponentService) {
        this.CampaignRoleService = CampaignRoleService;
        this._formBuilder = _formBuilder;
        this.metadataService = metadataService;
        this.campaignService = campaignService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.campaignComponentService = campaignComponentService;
        this.meta = new _models_metadata__WEBPACK_IMPORTED_MODULE_4__["MetadataModel"]();
        this.compensationType = _models_production_models__WEBPACK_IMPORTED_MODULE_6__["CompensationTypeEnum"].notPaid;
        //private campaign: Campaign;
        this.showHints = false;
    }
    NewCampaignComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deleteRoleSubscription = this.CampaignRoleService.subscribeToDeleteRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.campaign.roles.splice(_this.campaign.roles.indexOf(role), 1);
            }
        });
        /*this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(
                (role: CampaignRole) => {
                    if (role) {
                        this.campaign.roles[this.campaign.roles.indexOf(role)] = role;
                    }
                });*/
        this.deleteCampaignIntentSubscription = this.campaignComponentService.subscribeToDeleteCampaignIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToCampaignsView();
            }
        });
        this.cancelEditCampaignIntentSubscription = this.campaignComponentService.subscribeToCancelEditCampaignIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToCampaignsView();
            }
        });
        this.saveCampaignIntentSubscription = this.campaignComponentService.subscribeToSaveCampaignIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.saveCampaignClick();
            }
        });
        if (this.transition.params().popRole) {
            this.popRole = this.transition.params().popRole;
        }
        if (this.transition.params().isDraft) {
            this.isDraft = this.transition.params().isDraft;
        }
        if (this.transition.params().isEdit) {
            this.isEdit = this.transition.params().isEdit;
        }
        if (this.transition.params().campaign) {
            this.campaign = this.transition.params().campaign;
        }
        if (this.production) {
            this.namePlaceHolder = this.production.name;
        }
        if (this.campaign) {
            this.basicCampaignInfo = this._formBuilder.group({
                name: [this.campaign.name, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                instructions: [this.campaign.instructions],
                expires: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                campaignTypeId: [this.campaign.campaignType, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                campaignViewType: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                unions: [this.campaign.unions.map(function (a) { return a.unionId; })],
                roles: [this.campaign.roles.map(function (a) { return a.roleId; })]
                // campaignCompany: [this.campaign.campaignCompany],
                // description: [this.campaign.description, Validators.required],
                // compensationType: [this.campaign.compensationType],
                //   campaignUnionStatus: [this.campaign.campaignUnionStatus]
            }, { updateOn: 'blur' });
        }
        else {
            this.campaign = new _models_campaign_models__WEBPACK_IMPORTED_MODULE_5__["Campaign"]();
            this.basicCampaignInfo = this._formBuilder.group({
                name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                instructions: [''],
                expires: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                campaignTypeId: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                campaignViewType: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                //campaignViewType: [0, Validators.required],
                unions: [[]],
                roles: [[]],
            }, { updateOn: 'blur' });
        }
    };
    NewCampaignComponent.prototype.ngOnDestroy = function () {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();
        }
        if (this.saveCampaignIntentSubscription) {
            this.saveCampaignIntentSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    NewCampaignComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
        //    this.campaignComponentService.registerRolesScrollElement(this.rolesCard);
        // this.campaignComponentService.registerBasicScrollElement(this.basicCard);
        if (this.popRole) {
            /* setTimeout(() => {
                 this.newRoleSubscription = this.roleDialogService.createRole(this.campaign)
                     .subscribe(
                         role => {
                             if (role) {
                                 this.campaign.roles.push(role);
                             }
                             this.newRoleSubscription.unsubscribe();
                         });
 
 
             })*/
        }
    };
    NewCampaignComponent.prototype.setDate = function () {
        // Set today date using the patchValue function
        var date = new Date();
        this.basicCampaignInfo.patchValue({
            expires: {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate()
                }
            }
        });
    };
    NewCampaignComponent.prototype.clearDate = function () {
        // Clear the date using the patchValue function
        this.basicCampaignInfo.patchValue({ myDate: null });
    };
    NewCampaignComponent.prototype.moveToCampaignView = function (campaignId) {
        this.stateService.go('campaign', { campaign: this.campaign, campaignId: campaignId });
    };
    NewCampaignComponent.prototype.moveToCampaignsView = function () {
        this.stateService.go('campaigns');
    };
    NewCampaignComponent.prototype.scrollToRoles = function () {
        //    this.basicCampaignInfo.reset();
        // this.rolesCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
        //this.campaignComponentService.scrollToRoles();
    };
    NewCampaignComponent.prototype.scrollToBasic = function () {
        //   this.basicCampaignInfo.reset();
        //this.basicCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
        //this.campaignComponentService.scrollToBasic();
    };
    NewCampaignComponent.prototype.deleteCampaignClick = function () {
        //   this.basicCampaignInfo.reset();
        this.campaignComponentService.tryDeleteCampaign();
    };
    NewCampaignComponent.prototype.cancelEditCampaignClick = function () {
        //   this.basicCampaignInfo.reset();
        this.moveToCampaignsView();
    };
    NewCampaignComponent.prototype.selectExpireDate = function (date) {
        this.basicCampaignInfo.patchValue({
            expires: date,
        });
    };
    NewCampaignComponent.prototype.changeUnionSelection = function (event) {
        this.basicCampaignInfo.patchValue({
            unions: event.value,
        });
    };
    NewCampaignComponent.prototype.changeRoleSelection = function (event) {
        this.basicCampaignInfo.patchValue({
            roles: event.value,
        });
    };
    NewCampaignComponent.prototype.saveCampaignClick = function () {
        var _this = this;
        if (this.basicCampaignInfo.valid) {
            //this.role = this.basicRoleInfo.value;
            var submissionObject = Object.assign(this.basicCampaignInfo.value, {
                productionId: this.production.id
            });
            if (this.campaign.id && this.campaign.id !== 0) {
                this.campaignService.saveCampaign(this.campaign.id, this.production.id, submissionObject).subscribe(function (campaign) {
                    _this.moveToCampaignView(_this.campaign.id);
                    //this.campaign = campaign;
                }, function (error) {
                    console.log("error " + error);
                });
            }
            else {
                // submissionObject.productionId = this.productionId;
                this.campaignService.createCampaign(submissionObject, this.production.id).subscribe(function (campaign) {
                    _this.campaign = campaign;
                    _this.moveToCampaignView(_this.campaign.id);
                }, function (error) {
                    console.log("error " + error);
                });
            }
        }
        else {
            if (!this.basicCampaignInfo.get('name').valid) {
                this.basicCampaignInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicCampaignInfo.get('campaignTypeId').valid) {
                this.basicCampaignInfo.get('campaignTypeId').markAsDirty();
                this.campaignTypeIdField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.campaignTypeIdField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicCampaignInfo.get('campaignViewType').valid) {
                this.basicCampaignInfo.get('campaignViewType').markAsDirty();
                this.campaignViewTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.campaignViewTypeField.inputViewChild.nativeElement.focus();
            }
            else if (!this.basicCampaignInfo.get('expires').valid) {
                this.basicCampaignInfo.get('expires').markAsDirty();
                this.calendarInputField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.calendarInputField.inputfieldViewChild.nativeElement.focus();
            }
        }
    };
    NewCampaignComponent.prototype.addRoleClick = function () {
        var _this = this;
        // this.basicCampaignInfo.reset();
        if (this.campaign.id && this.campaign.id !== 0) {
            //setTimeout(() => {
            /*this.newRoleSubscription = this.roleDialogService.createRole(this.campaign)
                .subscribe(
                    role => {
                        if (role) {
                            this.campaign.roles.push(role);
                        }
                        this.newRoleSubscription.unsubscribe();
                    });*/
            // });
        }
        else {
            var submissionObject = Object.assign(this.basicCampaignInfo.value);
            this.campaignService.createCampaign(submissionObject, this.production.id).subscribe(function (campaign) {
                _this.isLoading = false;
                _this.stateService.go('draftcampaign', { campaign: campaign, draftId: campaign.id, popRole: true });
            }, function (error) {
            });
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], NewCampaignComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_6__["Production"])
    ], NewCampaignComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_4__["MetadataModel"])
    ], NewCampaignComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], NewCampaignComponent.prototype, "campaignId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_campaign_models__WEBPACK_IMPORTED_MODULE_5__["Campaign"])
    ], NewCampaignComponent.prototype, "campaign", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewCampaignComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('expires'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_calendar__WEBPACK_IMPORTED_MODULE_13__["Calendar"])
    ], NewCampaignComponent.prototype, "calendarInputField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('campaignTypeId'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__["Dropdown"])
    ], NewCampaignComponent.prototype, "campaignTypeIdField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('campaignViewType'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_radiobutton__WEBPACK_IMPORTED_MODULE_14__["RadioButton"])
    ], NewCampaignComponent.prototype, "campaignViewTypeField", void 0);
    NewCampaignComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-campaign',
            template: __webpack_require__(/*! ./new-campaign.component.html */ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.html"),
            // make fade in animation available to this component
            animations: [_animations_router_animation__WEBPACK_IMPORTED_MODULE_11__["fadeInAnimation"]],
            // attach the fade in animation to the host (root) element of this component
            host: { '[@fadeInAnimation]': '' },
            styles: [__webpack_require__(/*! ./new-campaign.component.scss */ "./src/app/feature-modules/campaign/components/new-campaign/new-campaign.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_campaign_role_service__WEBPACK_IMPORTED_MODULE_10__["CampaignRoleService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"], _services_campaign_service__WEBPACK_IMPORTED_MODULE_8__["CampaignService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"],
            _services_campaign_component_service__WEBPACK_IMPORTED_MODULE_9__["CampaignComponentService"]])
    ], NewCampaignComponent);
    return NewCampaignComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/campaign/services/campaign-component.service.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/feature-modules/campaign/services/campaign-component.service.ts ***!
  \*********************************************************************************/
/*! exports provided: CampaignComponentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignComponentService", function() { return CampaignComponentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var CampaignComponentService = /** @class */ (function () {
    function CampaignComponentService() {
        this._saveCampaignIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.saveCampaignIntentObs = this._saveCampaignIntentObs.asObservable();
        this._cancelEditCampaignIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.cancelEditCampaignIntentObs = this._cancelEditCampaignIntentObs.asObservable();
        this._deleteCampaignIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.deleteCampaignIntentObs = this._deleteCampaignIntentObs.asObservable();
    }
    CampaignComponentService.prototype.subscribeToSaveCampaignIntentObs = function () {
        return this.saveCampaignIntentObs;
    };
    CampaignComponentService.prototype.trySaveCampaign = function () {
        this._saveCampaignIntentObs.next(true);
    };
    CampaignComponentService.prototype.subscribeToCancelEditCampaignIntentObs = function () {
        return this.cancelEditCampaignIntentObs;
    };
    CampaignComponentService.prototype.tryCancelEditCampaign = function () {
        this._cancelEditCampaignIntentObs.next(true);
    };
    CampaignComponentService.prototype.subscribeToDeleteCampaignIntentObs = function () {
        return this.deleteCampaignIntentObs;
    };
    CampaignComponentService.prototype.tryDeleteCampaign = function () {
        this._deleteCampaignIntentObs.next(true);
    };
    CampaignComponentService.prototype.registerCampaignScrollElement = function (element) {
        this.campaignElement = element;
    };
    CampaignComponentService.prototype.registerOtherScrollElement = function (element) {
        this.otherElement = element;
    };
    CampaignComponentService.prototype.registerArtisticScrollElement = function (element) {
        this.artisticElement = element;
    };
    CampaignComponentService.prototype.registerCastingScrollElement = function (element) {
        this.castingElement = element;
    };
    CampaignComponentService.prototype.registerRolesScrollElement = function (element) {
        this.rolesElement = element;
    };
    CampaignComponentService.prototype.registerBasicScrollElement = function (element) {
        this.basicElement = element;
    };
    //
    CampaignComponentService.prototype.scrollToCampaign = function () {
        this.campaignElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CampaignComponentService.prototype.scrollToOther = function () {
        this.otherElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CampaignComponentService.prototype.scrollToArtistic = function () {
        this.artisticElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CampaignComponentService.prototype.scrollToCasting = function () {
        this.castingElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CampaignComponentService.prototype.scrollToRoles = function () {
        this.rolesElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CampaignComponentService.prototype.scrollToBasic = function () {
        this.basicElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CampaignComponentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CampaignComponentService);
    return CampaignComponentService;
}());



/***/ }),

/***/ "./src/app/models/campaign.models.ts":
/*!*******************************************!*\
  !*** ./src/app/models/campaign.models.ts ***!
  \*******************************************/
/*! exports provided: CampaignRoleTransferObject, BaseCampaign, Campaign, NewCampaign, UpdateCampaign, BaseCampaignRole, CampaignRole, NewCampaignRole, UpdateCampaignRole, BaseCampaignFolder, CampaignFolder, NewCampaignFolder, UpdateCampaignFolder, CampaignFolderRole, NewCampaignFolderRole, CampaignTypeEnum, CampaignUnionStatusEnum, campaignLocationReference, LocationTypeEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignRoleTransferObject", function() { return CampaignRoleTransferObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCampaign", function() { return BaseCampaign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Campaign", function() { return Campaign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCampaign", function() { return NewCampaign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCampaign", function() { return UpdateCampaign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCampaignRole", function() { return BaseCampaignRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignRole", function() { return CampaignRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCampaignRole", function() { return NewCampaignRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCampaignRole", function() { return UpdateCampaignRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCampaignFolder", function() { return BaseCampaignFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignFolder", function() { return CampaignFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCampaignFolder", function() { return NewCampaignFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCampaignFolder", function() { return UpdateCampaignFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignFolderRole", function() { return CampaignFolderRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCampaignFolderRole", function() { return NewCampaignFolderRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignTypeEnum", function() { return CampaignTypeEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignUnionStatusEnum", function() { return CampaignUnionStatusEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "campaignLocationReference", function() { return campaignLocationReference; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationTypeEnum", function() { return LocationTypeEnum; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var CampaignRoleTransferObject = /** @class */ (function () {
    function CampaignRoleTransferObject() {
    }
    return CampaignRoleTransferObject;
}());

var BaseCampaign = /** @class */ (function () {
    function BaseCampaign() {
    }
    return BaseCampaign;
}());

var Campaign = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](Campaign, _super);
    function Campaign() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.roles = [];
        return _this;
    }
    return Campaign;
}(BaseCampaign));

var NewCampaign = /** @class */ (function () {
    function NewCampaign() {
    }
    return NewCampaign;
}());

var UpdateCampaign = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateCampaign, _super);
    function UpdateCampaign() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateCampaign;
}(BaseCampaign));

var BaseCampaignRole = /** @class */ (function () {
    function BaseCampaignRole() {
    }
    return BaseCampaignRole;
}());

var CampaignRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CampaignRole, _super);
    function CampaignRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CampaignRole;
}(BaseCampaignRole));

var NewCampaignRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NewCampaignRole, _super);
    function NewCampaignRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewCampaignRole;
}(BaseCampaignRole));

var UpdateCampaignRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateCampaignRole, _super);
    function UpdateCampaignRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateCampaignRole;
}(BaseCampaignRole));

var BaseCampaignFolder = /** @class */ (function () {
    function BaseCampaignFolder() {
    }
    return BaseCampaignFolder;
}());

var CampaignFolder = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CampaignFolder, _super);
    function CampaignFolder() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.folderRoles = [];
        return _this;
    }
    return CampaignFolder;
}(BaseCampaignFolder));

var NewCampaignFolder = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NewCampaignFolder, _super);
    function NewCampaignFolder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewCampaignFolder;
}(BaseCampaignFolder));

var UpdateCampaignFolder = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateCampaignFolder, _super);
    function UpdateCampaignFolder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateCampaignFolder;
}(BaseCampaignFolder));

var CampaignFolderRole = /** @class */ (function () {
    function CampaignFolderRole() {
    }
    return CampaignFolderRole;
}());

var NewCampaignFolderRole = /** @class */ (function () {
    function NewCampaignFolderRole() {
    }
    return NewCampaignFolderRole;
}());

var CampaignTypeEnum;
(function (CampaignTypeEnum) {
    CampaignTypeEnum[CampaignTypeEnum["publicAll"] = 1] = "publicAll";
    CampaignTypeEnum[CampaignTypeEnum["publicMembers"] = 3] = "publicMembers";
    CampaignTypeEnum[CampaignTypeEnum["privateSelectMembers"] = 7] = "privateSelectMembers";
})(CampaignTypeEnum || (CampaignTypeEnum = {}));
var CampaignUnionStatusEnum;
(function (CampaignUnionStatusEnum) {
    CampaignUnionStatusEnum[CampaignUnionStatusEnum["notRelevan"] = 0] = "notRelevan";
    CampaignUnionStatusEnum[CampaignUnionStatusEnum["nonUnion"] = 1] = "nonUnion";
    CampaignUnionStatusEnum[CampaignUnionStatusEnum["union"] = 3] = "union";
    CampaignUnionStatusEnum[CampaignUnionStatusEnum["both"] = 7] = "both";
})(CampaignUnionStatusEnum || (CampaignUnionStatusEnum = {}));
var campaignLocationReference = /** @class */ (function () {
    function campaignLocationReference() {
    }
    return campaignLocationReference;
}());

var LocationTypeEnum;
(function (LocationTypeEnum) {
    LocationTypeEnum[LocationTypeEnum["na"] = 0] = "na";
    LocationTypeEnum[LocationTypeEnum["city"] = 1] = "city";
    LocationTypeEnum[LocationTypeEnum["region"] = 3] = "region";
    LocationTypeEnum[LocationTypeEnum["country"] = 7] = "country";
})(LocationTypeEnum || (LocationTypeEnum = {}));


/***/ })

}]);
//# sourceMappingURL=feature-modules-campaign-campaign-module.js.map