import { Component, OnInit, Input } from '@angular/core';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { RoleDialogService } from '../../services/role-dialog.service';
import { ProductionService } from '../../../../services/production.service';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
    styleUrls: ['./role-list.component.scss'],
    host: { 'class': 'p-grid p-dir-col' }
})
export class RoleListComponent implements OnInit {

    constructor(private productionService: ProductionService, private roleDialogService: RoleDialogService) { }
    @Input()  production: Production;
    @Input()  roles: Role[] = [];

    ngOnInit() {
     
    }
  
 
}
