import { Component, OnInit, OnDestroy, Inject, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ProductionDialogService, AuditionDetailDialogObject } from '../../services/production-dialog.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { Dialog } from 'primeng/dialog';
import { SelectItem } from 'primeng/api';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';

@Component({
    selector: 'app-shoot-details-dialog',
    templateUrl: './shoot-details-dialog.component.html',
    styleUrls: ['./shoot-details-dialog.component.scss']
})
export class ShootDetailsDialogComponent implements OnInit, OnDestroy, AfterViewInit {

    constructor(private productionDialogService: ProductionDialogService,
        private productionExtraService: ProductionExtraService,
        private _formBuilder: FormBuilder,
        public mediaMatcher: MediaMatcher,
        private messageDispatchService: MessageDispatchService) {
    }
    @Input() shootDetail: ShootDetail;
    @Input() productionId: number;
    @ViewChild('formDirective', {static: false}) private formDirective: NgForm;
    @ViewChild('name', {static: false}) nameField: ElementRef;
    @ViewChild('location', {static: false}) locationField: ElementRef;
    @ViewChild('locationDetail', {static: false}) locationDetailField: ElementRef;
    @ViewChild('startDate', {static: false}) startDateField: any;
    @ViewChild('endDate', {static: false}) endDateField: any;
    @ViewChild('detaildialog', {static: false}) detaildialog: Dialog;
    bigSizeMatcher: MediaQueryList;
    smallSizeMatcher: MediaQueryList;
    isSmallDevice: boolean = false;

    private messages: any[] = [];
    private saveIsDisabled: boolean = false;
    private cancelIsDisabled: boolean = false;
    private isOneDayOnly:boolean = false;
    private datesToBeDecidedLater:boolean = false;


    isVisible: boolean = false;
    private dialogVisibleStateSubscription: Subscription;
    private basicDialogInfo: FormGroup;
    minDate: Date;
    maxDate: Date;
    invalidDates: Array<Date>
    dialogo: Dialog;


    ngOnInit() {
        this.isSmallDevice = window.innerWidth < 768;

        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));

        let today = new Date();
        let month = today.getMonth();
        let year = today.getFullYear();
        this.minDate = new Date();
        this.minDate.setDate(today.getDate() + 1);
        this.maxDate = new Date();
        this.maxDate.setDate(today.getDate() + 1);

        this.basicDialogInfo = this._formBuilder.group({
            location: ['', Validators.required],
            locationDetail: [''],
            isOneDayOnly: [false],
            details: [''],
            instructions: [''],
            startDate: ['', Validators.required],
            endDate: ['', Validators.required],
            name: ['', Validators.required],
            datesToBeDecidedLater: [false],
            datesAreTemptative: [false]

        },
            { updateOn: 'blur' });
        this.basicDialogInfo.patchValue({
            isOneDayOnly: false,
        });
        this.basicDialogInfo.controls['endDate'].enable();


        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToShootDetailsDialogShowStateObs()
            .subscribe(
                dialogObject => {
                    this.clearMessages();
                    this.formDirective.resetForm();
                    this.basicDialogInfo.reset();
                    if (dialogObject.isVisible && dialogObject.shootDetail != null) {
                        this.shootDetail = dialogObject.shootDetail;
                        this.basicDialogInfo.patchValue({
                            location: this.shootDetail.location,
                            locationDetail: this.shootDetail.locationDetail,
                            isOneDayOnly: this.shootDetail.isOneDayOnly,
                            details: this.shootDetail.details,
                            instructions: this.shootDetail.instructions,
                            startDate: new Date(this.shootDetail.startDate),
                            endDate: new Date(this.shootDetail.endDate),
                            name: this.shootDetail.name,
                            datesToBeDecidedLater: this.shootDetail.datesToBeDecidedLater,
                            datesAreTemptative: this.shootDetail.datesAreTemptative
                        });
                        if (this.shootDetail.datesToBeDecidedLater) {
                          this.dateDecidionChange(this.shootDetail.datesToBeDecidedLater);

                        } else {
                          //check if one day only
                          if (this.shootDetail.isOneDayOnly) {
                            this.oneDayAuditionChange(this.shootDetail.isOneDayOnly);
                          } else {
                            this.basicDialogInfo.controls['endDate'].enable();
                            this.basicDialogInfo.controls['startDate'].enable();
                          }

                        }
                    } else {
                        this.shootDetail = null;
                        this.basicDialogInfo.patchValue({
                          location: '',
                           locationDetail: '',
                          isOneDayOnly: false,
                          details: '',
                          instructions: '',
                          startDate: '',
                          endDate: '',
                          name: '',
                          datesToBeDecidedLater: false,
                          datesAreTemptative: false
                        });
                        this.dateDecidionChange(false);
                        this.oneDayAuditionChange(false);
 
                    }
                    this.isVisible = dialogObject.isVisible;
                    // this.detaildialog.maximized = true;
                    //this.detaildialog.toggleMaximize();
                    this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                    this.nameField.nativeElement.focus();

                });


    }

    ngAfterViewInit() {


    }

    ngOnDestroy() {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();

        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);

    }
    private clearMessages() {
        this.messages = [];
    }

    mediaQueryBigChangeListener(event) {

        if (event.matches) {
            this.isSmallDevice = false;
        }
    }
    mediaQuerySmallChangeListener(event) {
        if (event.matches) {

            this.isSmallDevice = true;
         }
    }
    private selectStartDate(date: any) {
        this.basicDialogInfo.patchValue({
            startDate: date,
        });
    }
    private selectEndDate(date: any) {
        this.basicDialogInfo.patchValue({
            endDate: date,
        });
    }

  
    private oneDayAuditionChange(value) {
      this.isOneDayOnly = value;
      if (value) {
        this.basicDialogInfo.controls['endDate'].disable();
      } else {
        if (!this.datesToBeDecidedLater) {
          this.basicDialogInfo.controls['endDate'].enable();
        }
      }
      this.basicDialogInfo.patchValue({
        endDate: '',
      });
    }
    private clickOneDaySwitch(selection: any) {
      this.oneDayAuditionChange(selection.checked);
    }
    private dateDecidionChange(value) {
      this.datesToBeDecidedLater = value;
      if (value) {
        this.basicDialogInfo.controls['endDate'].disable();
        this.basicDialogInfo.controls['startDate'].disable();
      } else {
        this.basicDialogInfo.controls['endDate'].enable();
        if (!this.isOneDayOnly) {
          this.basicDialogInfo.controls['startDate'].enable();
        }
      }
      this.basicDialogInfo.patchValue({
        endDate: '',
      });
      this.basicDialogInfo.patchValue({
        startDate: '',
      });


    }
    private clickDateDecisionSwitch(selection: any) {
      this.dateDecidionChange(selection.checked);

    }
  private onDialogShow(event: any, dialog: Dialog) {
        if (this.isSmallDevice) {
           setTimeout(() => {
                dialog.maximize();
            },
                0);
        }
    }
    private onDialogHide(event: any, dialog: Dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();

    }
    private cancelClick() {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();

    }
    private resetDialog() {
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.messages = [];

    }

    private saveClick() {
        this.clearMessages();

        if (this.basicDialogInfo.valid) {
            this.saveIsDisabled = true;
            this.cancelIsDisabled = true;

            let tshootDetail: ShootDetail = new ShootDetail;
            tshootDetail.productionId = this.productionId;
            tshootDetail.name = this.basicDialogInfo.get('name').value;
            tshootDetail.location = this.basicDialogInfo.get('location').value;
            tshootDetail.locationDetail = this.basicDialogInfo.get('locationDetail').value;
            tshootDetail.details = this.basicDialogInfo.get('details').value;
            tshootDetail.instructions = this.basicDialogInfo.get('instructions').value;
            tshootDetail.startDate = this.basicDialogInfo.get('startDate').value;
            tshootDetail.endDate = this.basicDialogInfo.get('endDate').value;
            tshootDetail.isOneDayOnly = this.basicDialogInfo.get('isOneDayOnly').value;
            tshootDetail.datesToBeDecidedLater = this.basicDialogInfo.get('datesToBeDecidedLater').value;
            tshootDetail.datesAreTemptative = this.basicDialogInfo.get('datesAreTemptative').value;
            if (this.shootDetail && this.shootDetail.id && this.shootDetail.id !== 0) {
                this.productionExtraService.saveShootDetail(this.shootDetail.id, this.productionId, tshootDetail).subscribe(
                    (shootDetail: ShootDetail) => {
                        //this.shootDetail = shootDetail;
                        this.formDirective.resetForm();
                        this.basicDialogInfo.reset();
                        this.isVisible = false;
                        this.messageDispatchService.dispatchSuccesMessage("Shoot detail '" + shootDetail.name + "' was saved successfully",
                            "Shoot Detail Saved");

                    },
                    (error) => {
                        this.saveIsDisabled = false;
                        this.cancelIsDisabled = false;

                        this.messageDispatchService.dispatchErrorMessage(
                            "Error saving shoot detail '" + tshootDetail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                            "Shoot Detail Save Error");
                        this.messages.push({
                            severity: 'error',
                            summary: "Shoot Detail Save Error",
                            detail: "Error saving shoot detail '" + tshootDetail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                        });
                    })

            } else {
                this.productionExtraService.createShootDetail(this.productionId, tshootDetail).subscribe(
                    (shootDetail: ShootDetail) => {
                        if (shootDetail != null) {
                            this.formDirective.resetForm();
                            this.basicDialogInfo.reset();
                            this.isVisible = false;
                            this.messageDispatchService.dispatchSuccesMessage("New shoot detail '" + shootDetail.name + "' was created successfully",
                                "Shoot Detail Created");

                        }

                    },
                    (error) => {
                        this.saveIsDisabled = false;
                        this.cancelIsDisabled = false;

                        this.messageDispatchService.dispatchErrorMessage(
                            "Error creating shoot detail '" + tshootDetail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                            "Shoot Detail Creation Error");
                        this.messages.push({
                            severity: 'error',
                            summary: "Shoot Detail Creation Error",
                            detail: "Error creating shoot detail '" + tshootDetail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                        });
                    })

            }

        } else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.nameField.nativeElement.focus();
            } else if (!this.basicDialogInfo.get('location').valid) {
                this.basicDialogInfo.get('location').markAsDirty();
                this.locationField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.locationField.nativeElement.focus();
            } else if (!this.basicDialogInfo.get('startDate').valid) {
                this.basicDialogInfo.get('startDate').markAsDirty();
                this.startDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.startDateField.inputfieldViewChild.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('endDate').valid) {
                this.basicDialogInfo.get('endDate').markAsDirty();
                this.endDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.endDateField.inputfieldViewChild.nativeElement.focus();
            }


        }
    }
}
