﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SSW.Data.Entities;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IBaseRepository
    {
        IQueryable<T> GetAllQueryable<T>() where T : class;
    }
}
