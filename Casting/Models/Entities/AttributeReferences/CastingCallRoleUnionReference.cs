﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Casting.Model.Entities.Personas;
using Casting.Model.Entities.CastingCalls;


using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.AttributeReferences
{
    public class CastingCallRoleUnionReference: UnionReference
    {
        public CastingCallRoleUnionReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.CastingCallRole;
        }
        public int RoleId { get; set; }
        public CastingCallRole Role { get; set; }

    }
}
