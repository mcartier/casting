(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-toast [baseZIndex]=\"300000\" [style]=\"{marginTop: '80px'}\"></p-toast>\r\n<p-toast position=\"center\" key=\"confirm\" (onClose)=\"onDecisionReject()\" [modal]=\"true\" [baseZIndex]=\"50000\">\r\n  <ng-template let-message pTemplate=\"message\">\r\n    <div style=\"text-align: center\">\r\n      <i class=\"pi pi-exclamation-triangle\" style=\"font-size: 3em\"></i>\r\n      <h3>{{message.summary}}</h3>\r\n      <p>{{message.detail}}</p>\r\n    </div>\r\n    <div class=\"ui-g ui-fluid\">\r\n      <div class=\"ui-g-6\">\r\n        <button type=\"button\" pButton (click)=\"onDecisionConfirm()\" label=\"Yes\" class=\"ui-button-success\"></button>\r\n      </div>\r\n      <div class=\"ui-g-6\">\r\n        <button type=\"button\" pButton (click)=\"onDecisionReject()\" label=\"No\" class=\"ui-button-secondary\"></button>\r\n      </div>\r\n    </div>\r\n  </ng-template>\r\n</p-toast>\r\n\r\n<div>\r\n\r\n  <ui-view #mainView></ui-view>\r\n  \r\n  <div\r\n    *ngIf=\"isShowingRouteLoadIndicator\"\r\n    class=\"router-load-indicator\">\r\n    Loading Module\r\n  </div>\r\n</div>\r\n\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/confirmation-dialog/confirmation-dialog.component.html":
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/confirmation-dialog/confirmation-dialog.component.html ***!
  \*************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<h1>{{data.title}}</h1>\r\n<div>{{data.confirmationMessage}}</div>\r\n<div>\r\n  <button style=\"color: #fff;background-color: #153961;\">{{data.yesText}}</button>\r\n  <button (click)=\"dialogRef.close(false)\">{{data.noText}}</button>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-non-scroll/main-non-scroll.component.html":
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-non-scroll/main-non-scroll.component.html ***!
  \****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"p-grid p-dir-col\">\r\n  <div class=\"p-col\">\r\n              <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n            </div>\r\n          </div>\r\n  ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.html ***!
  \********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col\">\r\n  <div class=\"p-col\">\r\n              <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n            </div>\r\n          </div>\r\n      \r\n ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.html":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"p-grid p-dir-col\">\r\n  <div class=\"p-col\">\r\n                                <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-scroll/main-scroll.component.html":
/*!********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-scroll/main-scroll.component.html ***!
  \********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col\">\r\n  <div class=\"p-col\">\r\n\r\n              <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n\r\n  </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.html":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.html ***!
  \******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"p-grid p-dir-col\">\r\n  <div class=\"p-col\">\r\n     <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n  </div>\r\n</div>\r\n<!--  <ui-view #sidePulloutContainerView name=\"side\"></ui-view>\r\n          </div>\r\n        </div>-->\r\n\r\n   ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.html ***!
  \**********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n                <div class=\"p-grid p-dir-col\">\r\n                    <div class=\"p-col\">\r\n                      <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n                    </div>\r\n                </div>\r\n     <!-- <div class=\"p-grid p-dir-col\" fxFlex=\"1 1 auto\" fxLayout=\"column\" class=\"scroll_me_wrapper\" fxLayoutAlign=\"start stretch\">\r\n       <div class=\"scroll_me\" fxFlex=\"1 1 auto\">\r\n         <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n       </div>\r\n     </div>\r\n     -->");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/no-pullout/no-pullout.component.html":
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/no-pullout/no-pullout.component.html ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col\">\r\n<div class=\"p-col\">\r\n    <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n\r\n</div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/pullout/pullout.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/pullout/pullout.component.html ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-sidebar [(visible)]=\"display\">\r\n  Content\r\n  <ui-view #sidePulloutContainerView name=\"nav\"></ui-view>\r\n\r\n</p-sidebar>\r\n<div class=\"p-grid p-dir-col\">\r\n  <header class=\"p-col\">\r\n       <div class=\"casting_wrapper\">\r\n         <button type=\"text\" (click)=\"display = true\" pButton icon=\"pi pi-plus\" label=\"Show\"></button>\r\n\r\n        <span>My Application</span>\r\n      </div>\r\n\r\n    <ngx-loading-bar></ngx-loading-bar>\r\n  </header>\r\n  <main class=\"p-col\"><!-- fxFlex=\"1 1 calc(100% - 138.626px)\"-->\r\n    <ui-view #sidePulloutContainerView name=\"main\"></ui-view>\r\n  \r\n  </main>\r\n\r\n \r\n  <footer class=\"p-col\">\r\n       <div class=\"casting_wrapper\">\r\n        <span>My Application</span>\r\n      </div>\r\n   </footer>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/home/home.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/home/home.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n\r\n<!--<uppy [plugins]=\"uppyPlugins\" #uppyComponent [restrictions]=\"uppyRestrictions\" [on]=\"uppyEvent\"></uppy>-->\r\n<button (click)=\"onclick()\">clickme</button>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-card header=\"Login to Casting WEb Book\">\r\n    <form [formGroup]=\"loginForm\" novalidate (ngSubmit)=\"onSubmit()\">\r\n      <div class=\"p-grid p-dir-col p-justify-around\">\r\n      <div class=\"p-col\">\r\n\r\n      <div class=\"p-grid p-dir-col p-justify-between email\">\r\n                        <input class=\"p-col\" pInput placeholder=\"Enter Username\" formControlName=\"userName\">\r\n                        <p-message class=\"p-col\" text=\"Username is <strong>required</strong>\" *ngIf=\"loginForm.controls['userName'].hasError('required') && loginForm.controls['userName'].dirty\">\r\n                            \r\n                        </p-message>\r\n                        <div class=\"p-col p-align-end\">e.g. name@company.com</div>\r\n      </div>\r\n      </div>\r\n          <div class=\"p-col\">\r\n\r\n\r\n              <div class=\"p-grid p-dir-col p-justify-between names\">\r\n                  <input class=\"p-col\" pInput type=\"password\" placeholder=\"Password\" formControlName=\"password\">\r\n                  <p-message class=\"p-col\" text=\"Password is <strong>required</strong>\" *ngIf=\"loginForm.controls['password'].hasError('required') && loginForm.controls['password'].dirty\">\r\n                      \r\n                  </p-message>\r\n              </div>\r\n      </div>\r\n          <div class=\"p-col\">\r\n\r\n\r\n              <div class=\"p-grid p-dir-col p-justify-between names\">\r\n                  <div [hidden]=\"!errorMessage\" class=\"well error\">{{ errorMessage }}</div>\r\n\r\n                  <button pButton type=\"submit\">Submit</button>\r\n              </div>\r\n          </div>\r\n      </div>\r\n\r\n    </form>\r\n</p-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/logout/logout.component.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/logout/logout.component.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/record-rtc/record-rtc.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/record-rtc/record-rtc.component.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row\">\r\n  <div class=\"col-xs-12\">\r\n    <video #video class=\"video\"></video>\r\n  </div>\r\n</div>\r\n{{data}}\r\n<div class=\"row\">\r\n  <div class=\"col-xs-12\">\r\n    <div mat-dialog-actions>\r\n      <button mat-button (click)=\"onNoClick()\">No Thanks</button>\r\n      <button mat-button (click)=\"saveMe()\" cdkFocusInitial>Ok</button>\r\n    </div>\r\n  </div>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/register/register.component.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/register/register.component.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<p-steps [model]=\"formSteps\" [readonly]=\"false\" [(activeIndex)]=\"activeStepIndex\"></p-steps>\r\n\r\n<p-card *ngIf=\"activeStepIndex == 1\" header=\"Fill out your basic user information\">\r\n\r\n    <form [formGroup]=\"basicUserInfo\">\r\n        <div class=\"p-grid p-dir-col p-justify-around\">\r\n            <div class=\"p-col\">\r\n\r\n                <div class=\"p-grid p-dir-col p-justify-between email\">\r\n                    <label class=\"p-col\">Your email</label>\r\n                    <div class=\"ui-inputgroup\">\r\n                        <input pInput placeholder=\"Email\" type=\"email\" formControlName=\"email\">\r\n                        <p-progressSpinner *ngIf=\"isSearchingEmail\" [style]=\"{width: '20px', height: '20px'}\" strokeWidth=\"8\" fill=\"#EEEEEE\" animationDuration=\".5s\"></p-progressSpinner>\r\n                        <i class=\"pi pi-check-circle\" *ngIf=\"(emailHasBeenChecked && emailIsAvailable && !isSearchingEmail)\"></i>\r\n                        <i class=\"pi pi-times-circle\" *ngIf=\"(emailHasBeenChecked && !emailIsAvailable && !isSearchingEmail)\"></i>\r\n                    </div>\r\n                    <p-message text=\"Email is <strong>already in use</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['email'].hasError('emailTaken') && basicUserInfo.controls['email'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <p-message text=\"Email is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['email'].hasError('required') && basicUserInfo.controls['email'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <p-message text=\"Email format is <strong>incorrect</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['email'].hasError('email') && basicUserInfo.controls['email'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">e.g. name@company.com</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n\r\n\r\n                <div class=\"p-grid p-dir-col p-justify-between names\">\r\n                    <label class=\"p-col\">Your first names</label>\r\n                    <input class=\"p-col\" pInput placeholder=\"First names\" formControlName=\"firstNames\">\r\n                    <p-message text=\"Some first name is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['firstNames'].hasError('required') && basicUserInfo.controls['firstNames'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">e.g. Billy Ray Bob</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n\r\n                <div class=\"p-grid p-dir-col p-justify-between names\">\r\n                    <label class=\"p-col\">Your last names</label>\r\n                    <input class=\"p-col\" pInput placeholder=\"Last names\" formControlName=\"lastNames\">\r\n                    <p-message text=\"Some last name is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['lastNames'].hasError('required') && basicUserInfo.controls['lastNames'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">e.g. Cohen Johnson De La Renta</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n\r\n\r\n                <div class=\"p-grid p-dir-col p-justify-between roles\">\r\n                    <label class=\"p-col\">Your role in the production-role-casting industry</label>\r\n                    <p-dropdown class=\"p-col\" [options]=\"roleGroups\" [group]=\"true\" autoWidth=\"false\" #role placeholder=\"Your role\" formControlName=\"role\" required>\r\n                        <ng-template let-item pTemplate=\"selectedItem\">\r\n                            <b>{{item.label}}</b> <!-- highlighted selected item -->\r\n                        </ng-template>\r\n                        <ng-template let-item pTemplate=\"item\">\r\n                            {{item.label}}\r\n                        </ng-template>\r\n                        <ng-template let-group pTemplate=\"group\">\r\n                            <span style=\"margin-left:.25em\">{{group.label}}</span>\r\n                        </ng-template>\r\n                    </p-dropdown>\r\n\r\n                    <p-message text=\"Your role selection is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['role'].hasError('required') && basicUserInfo.controls['role'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">Select a role</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n\r\n                <div class=\"p-grid p-dir-col p-justify-between roleOther\" *ngIf=\"roleOtherIsShowed\">\r\n                    <label class=\"p-col\">Tell us what exactly</label>\r\n                    <input class=\"p-col\" pInput placeholder=\"Specify\" formControlName=\"roleOther\">\r\n                    <p-message text=\"Telling us something is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"basicUserInfo.controls['roleOther'].hasError('required') && basicUserInfo.controls['roleOther'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">Specify a bit more about what you do</div>\r\n                </div>\r\n\r\n            </div>\r\n           \r\n        </div>\r\n    </form>\r\n    <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n    <div>\r\n        <button (click)=\"moveToRegistterStep()\">Next</button>\r\n     </div>\r\n</p-card>\r\n\r\n\r\n\r\n\r\n<p-card *ngIf=\"activeStepIndex == 2\" header=\"Fill out your address\">\r\n    <form [formGroup]=\"userLoginInfo\">\r\n        <div class=\"p-grid p-dir-col p-justify-around\">\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between username\">\r\n                    <label class=\"p-col\">Choose your username</label>\r\n                    <div class=\"ui-inputgroup\">\r\n                        <input pInput placeholder=\"Username\" type=\"text\" formControlName=\"username\">\r\n                        <p-progressSpinner *ngIf=\"isSearchingUsername\" [style]=\"{width: '20px', height: '20px'}\" strokeWidth=\"8\" fill=\"#EEEEEE\" animationDuration=\".5s\"></p-progressSpinner>\r\n                        <i class=\"pi pi-check-circle\" *ngIf=\"(usernameHasBeenChecked && usernameIsAvailable && !isSearchingUsername)\"></i>\r\n                        <i class=\"pi pi-times-circle\" *ngIf=\"(usernameHasBeenChecked && !usernameIsAvailable && !isSearchingUsername)\"></i>\r\n                    </div>\r\n                    <p-message text=\"Username is <strong>already in use</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"userLoginInfo.controls['username'].hasError('usernameTaken') && userLoginInfo.controls['username'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <p-message text=\"Username is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"userLoginInfo.controls['username'].hasError('required') && userLoginInfo.controls['username'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">e.g. bobthepanther</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between password\">\r\n                    <label class=\"p-col\">Chose a password</label>\r\n                    <input class=\"p-col\" pInput placeholder=\"Your new password\" formControlName=\"password\">\r\n                    <p-message text=\"Password is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"userLoginInfo.controls['password'].hasError('required') && userLoginInfo.controls['password'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <p-message text=\"Password must be 5 characters long <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"userLoginInfo.controls['password'].hasError('minlength') && userLoginInfo.controls['password'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">Your new password</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between password\">\r\n                    <label class=\"p-col\">Re-enter your chosen password</label>\r\n                    <input class=\"p-col\" pInput placeholder=\"Re-enter password\" formControlName=\"passwordCompare\">\r\n                    <p-message text=\"Password repeat is <strong>required</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"userLoginInfo.controls['passwordCompare'].hasError('required') && userLoginInfo.controls['passwordCompare'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <p-message text=\"Passwords <strong>must match</strong>\" class=\"p-col\" severity=\"error\" *ngIf=\"userLoginInfo.controls['passwordCompare'].hasError('compare') && userLoginInfo.controls['passwordCompare'].dirty\">\r\n                        \r\n                    </p-message>\r\n                    <div class=\"p-col p-align-end\">Re-enter password</div>\r\n                </div>\r\n\r\n            </div>\r\n           \r\n        </div>\r\n    </form>\r\n    <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n    <div>\r\n        <button pButton (click)=\"moveToStep(1)\">Back</button>\r\n        <button pButton (click)=\"register()\">Register</button>\r\n    </div>\r\n</p-card>\r\n\r\n\r\n<p-card *ngIf=\"activeStepIndex == 3\" header=\"Done\">\r\n    You are now done.\r\n    <div>\r\n        <button pButton (click)=\"stepper.reset()\">Reset</button>\r\n    </div>\r\n</p-card>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/toolbar/toolbar.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/toolbar/toolbar.component.html ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\r\n  toolbar works!\r\n</p>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/testme/testme.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/testme/testme.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\r\n  testme works!\r\n</p>\r\n");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
    if (m) return m.call(o);
    return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}


/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./feature-modules/casting-call/casting-call.module": [
		"./src/app/feature-modules/casting-call/casting-call.module.ts",
		"common",
		"feature-modules-casting-call-casting-call-module"
	],
	"./feature-modules/production/production.module": [
		"./src/app/feature-modules/production/production.module.ts",
		"common",
		"feature-modules-production-production-module"
	],
	"./feature-modules/recording/recordingManager.module": [
		"./src/app/feature-modules/recording/recordingManager.module.ts",
		"feature-modules-recording-recordingManager-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ngx-loading-bar/http-client */ "./node_modules/@ngx-loading-bar/http-client/fesm5/ngx-loading-bar-http-client.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ui_router_states__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ui-router-states */ "./src/app/ui-router-states.ts");
/* harmony import */ var _ui_router_router_config__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ui-router/router.config */ "./src/app/ui-router/router.config.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _services_browser_window_provider__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var ng2_sticky_kit__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng2-sticky-kit */ "./node_modules/ng2-sticky-kit/ng2-sticky-kit.es5.js");
/* harmony import */ var _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./interceptors/errorInterceptor */ "./src/app/interceptors/errorInterceptor.ts");
/* harmony import */ var _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./interceptors/jwtInterceptor */ "./src/app/interceptors/jwtInterceptor.ts");
/* harmony import */ var _components_app_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/app.component */ "./src/app/components/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_logout_logout_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/logout/logout.component */ "./src/app/components/logout/logout.component.ts");
/* harmony import */ var _directives_email_availability_validator_directive__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./directives/email-availability-validator.directive */ "./src/app/directives/email-availability-validator.directive.ts");
/* harmony import */ var _services_video_blob_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./services/video-blob.service */ "./src/app/services/video-blob.service.ts");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _services_file_upload_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./services/file-upload.service */ "./src/app/services/file-upload.service.ts");
/* harmony import */ var _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./services/confirmation-dialog.service */ "./src/app/services/confirmation-dialog.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var _services_casting_call_service__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./services/casting-call.service */ "./src/app/services/casting-call.service.ts");
/* harmony import */ var _services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./services/state-transition-handler.service */ "./src/app/services/state-transition-handler.service.ts");
/* harmony import */ var _services_casting_call_role_service__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./services/casting-call-role.service */ "./src/app/services/casting-call-role.service.ts");
/* harmony import */ var _services_state_dependency_resolver_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./services/state-dependency-resolver.service */ "./src/app/services/state-dependency-resolver.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./services/confirm-decision-subscription.service */ "./src/app/services/confirm-decision-subscription.service.ts");
/* harmony import */ var _components_record_rtc_record_rtc_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./components/record-rtc/record-rtc.component */ "./src/app/components/record-rtc/record-rtc.component.ts");
/* harmony import */ var _components_toolbar_toolbar_component__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./components/toolbar/toolbar.component */ "./src/app/components/toolbar/toolbar.component.ts");
/* harmony import */ var _testme_testme_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./testme/testme.component */ "./src/app/testme/testme.component.ts");
/* harmony import */ var _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! ./components/confirmation-dialog/confirmation-dialog.component */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts");
/* harmony import */ var _components_containers_main_pullout_non_scroll_main_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! ./components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component */ "./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.ts");
/* harmony import */ var _components_containers_main_non_scroll_main_non_scroll_component__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! ./components/containers/main-non-scroll/main-non-scroll.component */ "./src/app/components/containers/main-non-scroll/main-non-scroll.component.ts");
/* harmony import */ var _components_containers_main_pullout_scroll_main_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! ./components/containers/main-pullout-scroll/main-pullout-scroll.component */ "./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.ts");
/* harmony import */ var _components_containers_main_scroll_main_scroll_component__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! ./components/containers/main-scroll/main-scroll.component */ "./src/app/components/containers/main-scroll/main-scroll.component.ts");
/* harmony import */ var _components_containers_main_side_pullout_scroll_main_side_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! ./components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component */ "./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.ts");
/* harmony import */ var _components_containers_main_side_pullout_non_scroll_main_side_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! ./components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component */ "./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.ts");
/* harmony import */ var _components_containers_pullout_pullout_component__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! ./components/containers/pullout/pullout.component */ "./src/app/components/containers/pullout/pullout.component.ts");
/* harmony import */ var _components_containers_no_pullout_no_pullout_component__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! ./components/containers/no-pullout/no-pullout.component */ "./src/app/components/containers/no-pullout/no-pullout.component.ts");











//import { ActivatedRoute } from '@angular/router';


//import { StickyNavModule } from 'ng2-sticky-nav';








//import { SidesDialogComponent } from './components/sides-dialog/sides-dialog.component';

//import { UppyModule } from './uppy/uppy.module';
//import { VideoRecorderModule } from './video-recorder/video-recorder.module';













//import { RecordingManagerModule } from './feature-modules/recording/recordingManager.module';
//import { RecordingsComponent } from './feature-modules/recording/components/recordings/recordings.component';
//import { RecordingItemComponent } from './feature-modules/recording/components/recording-item/recording-item.component';
//import { MatFileUploadQueueComponent } from './components/mat-file-upload-queue/mat-file-upload-queue.component';
//import { MatFileUploadComponent } from './components/mat-file-upload/mat-file-upload.component';
//import { FileUploadInputFor } from './directives/file-upload-input-for.directive';
//import { BytesPipePipe } from './pipes/bytes-pipe.pipe';
//import { VideoRecorderComponent } from './components/video-recorder/video-recorder.component';












//import { DropFileDirective } from './directives/drop-file.directive';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _components_app_component__WEBPACK_IMPORTED_MODULE_15__["AppComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
                _directives_email_availability_validator_directive__WEBPACK_IMPORTED_MODULE_20__["EmailAvailabilityValidatorDirective"],
                _components_record_rtc_record_rtc_component__WEBPACK_IMPORTED_MODULE_34__["RecordRTCComponent"],
                _components_toolbar_toolbar_component__WEBPACK_IMPORTED_MODULE_35__["ToolbarComponent"],
                _testme_testme_component__WEBPACK_IMPORTED_MODULE_36__["TestmeComponent"],
                _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_37__["ConfirmationDialogComponent"],
                _components_containers_main_pullout_non_scroll_main_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_38__["MainPulloutNonScrollComponent"],
                _components_containers_main_non_scroll_main_non_scroll_component__WEBPACK_IMPORTED_MODULE_39__["MainNonScrollComponent"],
                _components_containers_main_pullout_scroll_main_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_40__["MainPulloutScrollComponent"],
                _components_containers_main_scroll_main_scroll_component__WEBPACK_IMPORTED_MODULE_41__["MainScrollComponent"],
                _components_containers_main_side_pullout_scroll_main_side_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_42__["MainSidePulloutScrollComponent"],
                _components_containers_main_side_pullout_non_scroll_main_side_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_43__["MainSidePulloutNonScrollComponent"],
                _components_containers_pullout_pullout_component__WEBPACK_IMPORTED_MODULE_44__["PulloutComponent"],
                _components_containers_no_pullout_no_pullout_component__WEBPACK_IMPORTED_MODULE_45__["NoPulloutComponent"],
                _components_register_register_component__WEBPACK_IMPORTED_MODULE_17__["RegisterComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_18__["LoginComponent"],
                _components_logout_logout_component__WEBPACK_IMPORTED_MODULE_19__["LogoutComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _uirouter_angular__WEBPACK_IMPORTED_MODULE_10__["UIRouterModule"].forRoot({
                    states: _ui_router_states__WEBPACK_IMPORTED_MODULE_7__["APP_STATES"],
                    useHash: false,
                    otherwise: { state: 'home' },
                    config: _ui_router_router_config__WEBPACK_IMPORTED_MODULE_8__["routerConfigFn"],
                }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_5__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                _global_global_module__WEBPACK_IMPORTED_MODULE_3__["GlobalModule"],
                //UppyModule,
                //  VideoRecorderModule,
                // StickyNavModule,
                ng2_sticky_kit__WEBPACK_IMPORTED_MODULE_12__["StickyModule"],
                _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_4__["LoadingBarHttpClientModule"],
            ],
            entryComponents: [
                _components_record_rtc_record_rtc_component__WEBPACK_IMPORTED_MODULE_34__["RecordRTCComponent"],
                _components_confirmation_dialog_confirmation_dialog_component__WEBPACK_IMPORTED_MODULE_37__["ConfirmationDialogComponent"]
            ],
            exports: [
            //BytesPipePipe
            // DropFileDirective,
            ],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"], useClass: _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_14__["JwtInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"], useClass: _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_13__["ErrorInterceptor"], multi: true },
                _services_browser_window_provider__WEBPACK_IMPORTED_MODULE_11__["WINDOW_PROVIDERS"],
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModuleFactoryLoader"], useClass: _angular_core__WEBPACK_IMPORTED_MODULE_2__["SystemJsNgModuleLoader"] },
                _services_video_blob_service__WEBPACK_IMPORTED_MODULE_21__["VideoBlobService"],
                _services_metadata_service__WEBPACK_IMPORTED_MODULE_22__["MetadataService"],
                _services_file_upload_service__WEBPACK_IMPORTED_MODULE_23__["FileUploadService"],
                _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_24__["ConfirmationDialogService"],
                _services_production_service__WEBPACK_IMPORTED_MODULE_25__["ProductionService"],
                _services_production_role_service__WEBPACK_IMPORTED_MODULE_27__["ProductionRoleService"],
                _services_casting_call_service__WEBPACK_IMPORTED_MODULE_28__["CastingCallService"],
                _services_casting_call_role_service__WEBPACK_IMPORTED_MODULE_30__["CastingCallRoleService"],
                _services_state_dependency_resolver_service__WEBPACK_IMPORTED_MODULE_31__["StateDependencyResolverService"],
                _services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_29__["StateTransitionHandlerService"],
                _services_production_extra_service__WEBPACK_IMPORTED_MODULE_26__["ProductionExtraService"],
                _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_32__["MessageDispatchService"],
                _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_33__["ConfirmDecisionSubscriptionService"],
            ],
            bootstrap: [_uirouter_angular__WEBPACK_IMPORTED_MODULE_10__["UIView"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/app.component.scss":
/*!***********************************************!*\
  !*** ./src/app/components/app.component.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n\n.main_container {\n  /* height:100%;\n   position:relative;*/\n}\n\n.router-load-indicator {\n  background-color: #ffdc73;\n  border-radius: 5px 5px 5px 5px;\n  box-shadow: 0px 2px 2px fade(#000000, 20%);\n  color: #000000;\n  font-family: monospace;\n  font-size: 16px;\n  left: 50%;\n  padding: 7px 15px 7px 15px;\n  position: fixed;\n  text-transform: lowercase;\n  top: 10px;\n  transform: translateX(-50%);\n  z-index: 2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcY29tcG9uZW50c1xcYXBwLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2FwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUNDRjs7QURFQTtFQUNHO3NCQUFBO0FDRUg7O0FER0E7RUFJSSx5QkFBQTtFQUNBLDhCQUFBO0VBQ0EsMENBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsU0FBQTtFQUNBLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsVUFBQTtBQ0hKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9hcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbn1cclxuXHJcbi5tYWluX2NvbnRhaW5lciB7XHJcbiAgIC8qIGhlaWdodDoxMDAlO1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7Ki9cclxufVxyXG5cclxuXHJcbi5yb3V0ZXItbG9hZC1pbmRpY2F0b3Ige1xyXG4gICAgLy8gRGVsYXkgdGhlIGFuaW1hdGlvbiBmb3IgYSBmYXN0IG5ldHdvcmsgY29ubmVjdGlvbiAoc28gdXNlcnMgZG9uJ3Qgc2VlIHRoZSBsb2FkZXIpLlxyXG4gICBcclxuICAgIC8vIC0tXHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkYzczIDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweCA1cHggNXB4IDVweCA7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMnB4IDJweCBmYWRlKCAjMDAwMDAwLCAyMCUgKSA7XHJcbiAgICBjb2xvcjogIzAwMDAwMCA7XHJcbiAgICBmb250LWZhbWlseTogbW9ub3NwYWNlIDtcclxuICAgIGZvbnQtc2l6ZTogMTZweCA7XHJcbiAgICBsZWZ0OiA1MCUgO1xyXG4gICAgcGFkZGluZzogN3B4IDE1cHggN3B4IDE1cHggO1xyXG4gICAgcG9zaXRpb246IGZpeGVkIDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBsb3dlcmNhc2UgO1xyXG4gICAgdG9wOiAxMHB4IDtcclxuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCggLTUwJSApIDtcclxuICAgIHotaW5kZXg6IDIgO1xyXG59XHJcbiIsIjpob3N0IHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ubWFpbl9jb250YWluZXIge1xuICAvKiBoZWlnaHQ6MTAwJTtcbiAgIHBvc2l0aW9uOnJlbGF0aXZlOyovXG59XG5cbi5yb3V0ZXItbG9hZC1pbmRpY2F0b3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZkYzczO1xuICBib3JkZXItcmFkaXVzOiA1cHggNXB4IDVweCA1cHg7XG4gIGJveC1zaGFkb3c6IDBweCAycHggMnB4IGZhZGUoIzAwMDAwMCwgMjAlKTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIGZvbnQtZmFtaWx5OiBtb25vc3BhY2U7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGVmdDogNTAlO1xuICBwYWRkaW5nOiA3cHggMTVweCA3cHggMTVweDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0ZXh0LXRyYW5zZm9ybTogbG93ZXJjYXNlO1xuICB0b3A6IDEwcHg7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgei1pbmRleDogMjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/app.component.ts":
/*!*********************************************!*\
  !*** ./src/app/components/app.component.ts ***!
  \*********************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/confirm-decision-subscription.service */ "./src/app/services/confirm-decision-subscription.service.ts");






var AppComponent = /** @class */ (function () {
    function AppComponent(transitionService, messageDispatchService, messageService, confirmDecisionDispatcher) {
        //this.isShowingRouteLoadIndicator = false;
        this.transitionService = transitionService;
        this.messageDispatchService = messageDispatchService;
        this.messageService = messageService;
        this.confirmDecisionDispatcher = confirmDecisionDispatcher;
        this.title = 'talent-app';
        this.asyncLoadCount = 0;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.messageDispatcher = this.messageDispatchService.subscribeToMessageDispatcherObs()
            .subscribe(function (messages) {
            _this.messageService.addAll(messages);
        });
        this.decisionDispatcher = this.confirmDecisionDispatcher.subscribeToDecisionDispatcherObs()
            .subscribe(function (message) {
            _this.messageService.clear('confirm');
            _this.messageService.add(message);
        });
        // Make an editable copy of the pristineContact
        this.transitionService.onBefore({}, function (transition) {
            this.asyncLoadCount++;
            this.isShowingRouteLoadIndicator = !!this.asyncLoadCount;
        }, { priority: 10, bind: this });
        this.transitionService.onSuccess({}, function (transition) {
            this.asyncLoadCount--;
            this.isShowingRouteLoadIndicator = !!this.asyncLoadCount;
        }, { priority: 10, bind: this }); //(this.asyncLoadCount);
        this.transitionService.onError({}, function (transition) {
            this.asyncLoadCount--;
            this.isShowingRouteLoadIndicator = !!this.asyncLoadCount;
        }, { priority: 10, bind: this }); //(this.asyncLoadCount);
    };
    AppComponent.prototype.ngOnDestroy = function () {
        if (this.messageDispatcher) {
            this.messageDispatcher.unsubscribe();
        }
        if (this.decisionDispatcher) {
            this.decisionDispatcher.unsubscribe();
        }
    };
    AppComponent.prototype.onDecisionConfirm = function () {
        this.messageService.clear('confirm');
        this.confirmDecisionDispatcher.dispatchAgreeDecision();
    };
    AppComponent.prototype.onDecisionReject = function () {
        this.messageService.clear('confirm');
        this.confirmDecisionDispatcher.dispatchDeclineDecision();
    };
    AppComponent.ctorParameters = function () { return [
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"] },
        { type: primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"] },
        { type: _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmDecisionSubscriptionService"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/app.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.scss */ "./src/app/components/app.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"],
            primeng_api__WEBPACK_IMPORTED_MODULE_3__["MessageService"],
            _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmDecisionSubscriptionService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.scss ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29uZmlybWF0aW9uLWRpYWxvZy9jb25maXJtYXRpb24tZGlhbG9nLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/confirmation-dialog/confirmation-dialog.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/confirmation-dialog/confirmation-dialog.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ConfirmationDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationDialogComponent", function() { return ConfirmationDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConfirmationDialogComponent = /** @class */ (function () {
    function ConfirmationDialogComponent() {
    }
    ConfirmationDialogComponent.prototype.ngOnInit = function () {
    };
    ConfirmationDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirmation-dialog',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./confirmation-dialog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/confirmation-dialog/confirmation-dialog.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./confirmation-dialog.component.scss */ "./src/app/components/confirmation-dialog/confirmation-dialog.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConfirmationDialogComponent);
    return ConfirmationDialogComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/main-non-scroll/main-non-scroll.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/components/containers/main-non-scroll/main-non-scroll.component.scss ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tbm9uLXNjcm9sbC9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcY29tcG9uZW50c1xcY29udGFpbmVyc1xcbWFpbi1ub24tc2Nyb2xsXFxtYWluLW5vbi1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLW5vbi1zY3JvbGwvbWFpbi1ub24tc2Nyb2xsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tbm9uLXNjcm9sbC9tYWluLW5vbi1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbn1cclxuIiwiOmhvc3Qge1xuICBoZWlnaHQ6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/containers/main-non-scroll/main-non-scroll.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/components/containers/main-non-scroll/main-non-scroll.component.ts ***!
  \************************************************************************************/
/*! exports provided: MainNonScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainNonScrollComponent", function() { return MainNonScrollComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainNonScrollComponent = /** @class */ (function () {
    function MainNonScrollComponent() {
    }
    MainNonScrollComponent.prototype.ngOnInit = function () {
    };
    MainNonScrollComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-non-scroll',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-non-scroll.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-non-scroll/main-non-scroll.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-non-scroll.component.scss */ "./src/app/components/containers/main-non-scroll/main-non-scroll.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainNonScrollComponent);
    return MainNonScrollComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.scss":
/*!******************************************************************************************************!*\
  !*** ./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.scss ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tcHVsbG91dC1ub24tc2Nyb2xsL0Y6XFx3b3JrXFxjYXN0aW5nXFxDYXN0aW5nXFxBbmd1bGFyXFx0YWxlbnQtYXBwL3NyY1xcYXBwXFxjb21wb25lbnRzXFxjb250YWluZXJzXFxtYWluLXB1bGxvdXQtbm9uLXNjcm9sbFxcbWFpbi1wdWxsb3V0LW5vbi1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXB1bGxvdXQtbm9uLXNjcm9sbC9tYWluLXB1bGxvdXQtbm9uLXNjcm9sbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXB1bGxvdXQtbm9uLXNjcm9sbC9tYWluLXB1bGxvdXQtbm9uLXNjcm9sbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICBoZWlnaHQ6MTAwJTtcclxufVxyXG4iLCI6aG9zdCB7XG4gIGhlaWdodDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: MainPulloutNonScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPulloutNonScrollComponent", function() { return MainPulloutNonScrollComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainPulloutNonScrollComponent = /** @class */ (function () {
    function MainPulloutNonScrollComponent() {
    }
    MainPulloutNonScrollComponent.prototype.ngOnInit = function () {
    };
    MainPulloutNonScrollComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-pullout-non-scroll',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-pullout-non-scroll.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-pullout-non-scroll.component.scss */ "./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainPulloutNonScrollComponent);
    return MainPulloutNonScrollComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tcHVsbG91dC1zY3JvbGwvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNvbnRhaW5lcnNcXG1haW4tcHVsbG91dC1zY3JvbGxcXG1haW4tcHVsbG91dC1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXB1bGxvdXQtc2Nyb2xsL21haW4tcHVsbG91dC1zY3JvbGwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lcnMvbWFpbi1wdWxsb3V0LXNjcm9sbC9tYWluLXB1bGxvdXQtc2Nyb2xsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gIGhlaWdodDoxMDAlO1xyXG59XHJcbiIsIjpob3N0IHtcbiAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.ts ***!
  \********************************************************************************************/
/*! exports provided: MainPulloutScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainPulloutScrollComponent", function() { return MainPulloutScrollComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainPulloutScrollComponent = /** @class */ (function () {
    function MainPulloutScrollComponent() {
    }
    MainPulloutScrollComponent.prototype.ngOnInit = function () {
    };
    MainPulloutScrollComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-pullout-scroll',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-pullout-scroll.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-pullout-scroll.component.scss */ "./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainPulloutScrollComponent);
    return MainPulloutScrollComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/main-scroll/main-scroll.component.scss":
/*!******************************************************************************!*\
  !*** ./src/app/components/containers/main-scroll/main-scroll.component.scss ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tc2Nyb2xsL0Y6XFx3b3JrXFxjYXN0aW5nXFxDYXN0aW5nXFxBbmd1bGFyXFx0YWxlbnQtYXBwL3NyY1xcYXBwXFxjb21wb25lbnRzXFxjb250YWluZXJzXFxtYWluLXNjcm9sbFxcbWFpbi1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXNjcm9sbC9tYWluLXNjcm9sbC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXNjcm9sbC9tYWluLXNjcm9sbC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICBoZWlnaHQ6MTAwJTtcclxufVxyXG4iLCI6aG9zdCB7XG4gIGhlaWdodDogMTAwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/containers/main-scroll/main-scroll.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/components/containers/main-scroll/main-scroll.component.ts ***!
  \****************************************************************************/
/*! exports provided: MainScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainScrollComponent", function() { return MainScrollComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainScrollComponent = /** @class */ (function () {
    function MainScrollComponent() {
    }
    MainScrollComponent.prototype.ngOnInit = function () {
    };
    MainScrollComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-scroll',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-scroll.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-scroll/main-scroll.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-scroll.component.scss */ "./src/app/components/containers/main-scroll/main-scroll.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainScrollComponent);
    return MainScrollComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.scss":
/*!****************************************************************************************************************!*\
  !*** ./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.scss ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tc2lkZS1wdWxsb3V0LW5vbi1zY3JvbGwvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNvbnRhaW5lcnNcXG1haW4tc2lkZS1wdWxsb3V0LW5vbi1zY3JvbGxcXG1haW4tc2lkZS1wdWxsb3V0LW5vbi1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXNpZGUtcHVsbG91dC1ub24tc2Nyb2xsL21haW4tc2lkZS1wdWxsb3V0LW5vbi1zY3JvbGwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lcnMvbWFpbi1zaWRlLXB1bGxvdXQtbm9uLXNjcm9sbC9tYWluLXNpZGUtcHVsbG91dC1ub24tc2Nyb2xsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gIGhlaWdodDoxMDAlO1xyXG59XHJcbiIsIjpob3N0IHtcbiAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: MainSidePulloutNonScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainSidePulloutNonScrollComponent", function() { return MainSidePulloutNonScrollComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainSidePulloutNonScrollComponent = /** @class */ (function () {
    function MainSidePulloutNonScrollComponent() {
    }
    MainSidePulloutNonScrollComponent.prototype.ngOnInit = function () {
    };
    MainSidePulloutNonScrollComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-side-pullout-non-scroll',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-side-pullout-non-scroll.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-side-pullout-non-scroll.component.scss */ "./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainSidePulloutNonScrollComponent);
    return MainSidePulloutNonScrollComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.scss ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tc2lkZS1wdWxsb3V0LXNjcm9sbC9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcY29tcG9uZW50c1xcY29udGFpbmVyc1xcbWFpbi1zaWRlLXB1bGxvdXQtc2Nyb2xsXFxtYWluLXNpZGUtcHVsbG91dC1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9tYWluLXNpZGUtcHVsbG91dC1zY3JvbGwvbWFpbi1zaWRlLXB1bGxvdXQtc2Nyb2xsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL21haW4tc2lkZS1wdWxsb3V0LXNjcm9sbC9tYWluLXNpZGUtcHVsbG91dC1zY3JvbGwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbn1cclxuIiwiOmhvc3Qge1xuICBoZWlnaHQ6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: MainSidePulloutScrollComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainSidePulloutScrollComponent", function() { return MainSidePulloutScrollComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var MainSidePulloutScrollComponent = /** @class */ (function () {
    function MainSidePulloutScrollComponent() {
    }
    MainSidePulloutScrollComponent.prototype.ngOnInit = function () {
    };
    MainSidePulloutScrollComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-side-pullout-scroll',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./main-side-pullout-scroll.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./main-side-pullout-scroll.component.scss */ "./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MainSidePulloutScrollComponent);
    return MainSidePulloutScrollComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/no-pullout/no-pullout.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/components/containers/no-pullout/no-pullout.component.scss ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL25vLXB1bGxvdXQvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNvbnRhaW5lcnNcXG5vLXB1bGxvdXRcXG5vLXB1bGxvdXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9uby1wdWxsb3V0L25vLXB1bGxvdXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lcnMvbm8tcHVsbG91dC9uby1wdWxsb3V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gIGhlaWdodDoxMDAlO1xyXG59XHJcbiIsIjpob3N0IHtcbiAgaGVpZ2h0OiAxMDAlO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/containers/no-pullout/no-pullout.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/components/containers/no-pullout/no-pullout.component.ts ***!
  \**************************************************************************/
/*! exports provided: NoPulloutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoPulloutComponent", function() { return NoPulloutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NoPulloutComponent = /** @class */ (function () {
    function NoPulloutComponent() {
    }
    NoPulloutComponent.prototype.ngOnInit = function () {
    };
    NoPulloutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-no-pullout',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./no-pullout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/no-pullout/no-pullout.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./no-pullout.component.scss */ "./src/app/components/containers/no-pullout/no-pullout.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NoPulloutComponent);
    return NoPulloutComponent;
}());



/***/ }),

/***/ "./src/app/components/containers/pullout/pullout.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/containers/pullout/pullout.component.scss ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host {\n  height: 100%;\n}\n\n.sidenav-container {\n  height: 100%;\n}\n\n.sidenav {\n  width: 200px;\n}\n\n.sidenav .mat-toolbar {\n  background: inherit;\n}\n\n.mat-toolbar.mat-primary {\n  position: -webkit-sticky;\n  position: sticky;\n  top: 0;\n  z-index: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb250YWluZXJzL3B1bGxvdXQvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNvbnRhaW5lcnNcXHB1bGxvdXRcXHB1bGxvdXQuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvY29udGFpbmVycy9wdWxsb3V0L3B1bGxvdXQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxZQUFBO0FDRUY7O0FEQ0E7RUFDRSxZQUFBO0FDRUY7O0FEQ0E7RUFDRSxtQkFBQTtBQ0VGOztBRENBO0VBQ0Usd0JBQUE7RUFBQSxnQkFBQTtFQUNBLE1BQUE7RUFDQSxVQUFBO0FDRUYiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NvbnRhaW5lcnMvcHVsbG91dC9wdWxsb3V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gIGhlaWdodDoxMDAlO1xyXG59XHJcbi5zaWRlbmF2LWNvbnRhaW5lciB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2lkZW5hdiB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG59XHJcblxyXG4uc2lkZW5hdiAubWF0LXRvb2xiYXIge1xyXG4gIGJhY2tncm91bmQ6IGluaGVyaXQ7XHJcbn1cclxuXHJcbi5tYXQtdG9vbGJhci5tYXQtcHJpbWFyeSB7XHJcbiAgcG9zaXRpb246IHN0aWNreTtcclxuICB0b3A6IDA7XHJcbiAgei1pbmRleDogMTtcclxufSIsIjpob3N0IHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc2lkZW5hdi1jb250YWluZXIge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5zaWRlbmF2IHtcbiAgd2lkdGg6IDIwMHB4O1xufVxuXG4uc2lkZW5hdiAubWF0LXRvb2xiYXIge1xuICBiYWNrZ3JvdW5kOiBpbmhlcml0O1xufVxuXG4ubWF0LXRvb2xiYXIubWF0LXByaW1hcnkge1xuICBwb3NpdGlvbjogc3RpY2t5O1xuICB0b3A6IDA7XG4gIHotaW5kZXg6IDE7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/containers/pullout/pullout.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/containers/pullout/pullout.component.ts ***!
  \********************************************************************/
/*! exports provided: PulloutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PulloutComponent", function() { return PulloutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var PulloutComponent = /** @class */ (function () {
    function PulloutComponent(breakpointObserver) {
        this.breakpointObserver = breakpointObserver;
        this.isHandset$ = this.breakpointObserver.observe(_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["Breakpoints"].Handset)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (result) { return result.matches; }));
    }
    PulloutComponent.prototype.ngOnInit = function () {
    };
    PulloutComponent.ctorParameters = function () { return [
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"] }
    ]; };
    PulloutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pullout',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./pullout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/containers/pullout/pullout.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./pullout.component.scss */ "./src/app/components/containers/pullout/pullout.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["BreakpointObserver"]])
    ], PulloutComponent);
    return PulloutComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.scss ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.openDialog = function () {
        debugger;
    };
    HomeComponent.prototype.onclick = function () {
        this.openDialog();
    };
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent.prototype.ngOnDestroy = function () {
    };
    HomeComponent.prototype.ngAfterViewInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/home/home.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./home.component.scss */ "./src/app/components/home/home.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.scss":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".login-form {\n  width: 80%;\n  min-width: 120px;\n  margin: 20px auto;\n}\n\n.mat-radio-button {\n  display: block;\n  margin: 5px 0;\n}\n\n.row {\n  display: flex;\n  flex-direction: row;\n}\n\n.col {\n  flex: 1;\n  margin-right: 20px;\n}\n\n.col:last-child {\n  margin-right: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcY29tcG9uZW50c1xcbG9naW5cXGxvZ2luLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0UsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUNIRjs7QURNQTtFQUNFLGNBQUE7RUFDQSxhQUFBO0FDSEY7O0FETUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUNIRjs7QURNQTtFQUNFLE9BQUE7RUFDQSxrQkFBQTtBQ0hGOztBRE1BO0VBQ0UsZUFBQTtBQ0hGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mdWxsLXdpZHRoIHtcclxuIFxyXG59XHJcblxyXG4ubG9naW4tZm9ybSB7XHJcbiAgd2lkdGg6IDgwJTtcclxuICBtaW4td2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMjBweCBhdXRvO1xyXG59XHJcblxyXG4ubWF0LXJhZGlvLWJ1dHRvbiB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiA1cHggMDtcclxufVxyXG5cclxuLnJvdyB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4uY29sIHtcclxuICBmbGV4OiAxO1xyXG4gIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG5cclxuLmNvbDpsYXN0LWNoaWxkIHtcclxuICBtYXJnaW4tcmlnaHQ6IDA7XHJcbn1cclxuIiwiLmxvZ2luLWZvcm0ge1xuICB3aWR0aDogODAlO1xuICBtaW4td2lkdGg6IDEyMHB4O1xuICBtYXJnaW46IDIwcHggYXV0bztcbn1cblxuLm1hdC1yYWRpby1idXR0b24ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiA1cHggMDtcbn1cblxuLnJvdyB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG5cbi5jb2wge1xuICBmbGV4OiAxO1xuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XG59XG5cbi5jb2w6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1yaWdodDogMDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");





//import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(_formBuilder, authenticationService, stateService) {
        this._formBuilder = _formBuilder;
        this.authenticationService = authenticationService;
        this.stateService = stateService;
        this.submitted = false;
        this.credentials = { username: null, password: null };
    }
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.submitting = true;
        var returnToOriginalState = function () {
            var state = _this.returnTo.state();
            var params = _this.returnTo.params();
            var options = Object.assign({}, _this.returnTo.options(), { reload: true });
            if (state.name == 'logout') {
                _this.stateService.go('home');
            }
            else {
                _this.stateService.go(state, params, options);
            }
        };
        var showError = function (errorMessage) {
            return _this.errorMessage = errorMessage;
        };
        var stop = function () { return _this.submitting = false; };
        this.authenticationService.login(this.loginForm.value.userName, this.loginForm.value.password)
            .then(returnToOriginalState)
            .catch(showError)
            .then(stop, stop);
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this._formBuilder.group({
            userName: ['bobob', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['Brazil99?', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    LoginComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TargetState"])
    ], LoginComponent.prototype, "returnTo", void 0);
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/login/login.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.scss */ "./src/app/components/login/login.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/logout/logout.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/logout/logout.component.ts ***!
  \*******************************************************/
/*! exports provided: LogoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutComponent", function() { return LogoutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");




//import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
var LogoutComponent = /** @class */ (function () {
    function LogoutComponent(authenticationService, stateService) {
        this.authenticationService = authenticationService;
        this.stateService = stateService;
    }
    LogoutComponent.prototype.ngOnInit = function () {
        this.authenticationService.logout();
    };
    LogoutComponent.ctorParameters = function () { return [
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] }
    ]; };
    LogoutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./logout.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/logout/logout.component.html")).default
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"], _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"]])
    ], LogoutComponent);
    return LogoutComponent;
}());



/***/ }),

/***/ "./src/app/components/record-rtc/record-rtc.component.scss":
/*!*****************************************************************!*\
  !*** ./src/app/components/record-rtc/record-rtc.component.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".video {\n  box-shadow: 1px 6px 10px 2px rgba(35, 35, 35, 0.62);\n  height: 600px;\n  max-height: 800px;\n  width: 1067px;\n}\n\n.row {\n  margin-bottom: 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZWNvcmQtcnRjL0Y6XFx3b3JrXFxjYXN0aW5nXFxDYXN0aW5nXFxBbmd1bGFyXFx0YWxlbnQtYXBwL3NyY1xcYXBwXFxjb21wb25lbnRzXFxyZWNvcmQtcnRjXFxyZWNvcmQtcnRjLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL3JlY29yZC1ydGMvcmVjb3JkLXJ0Yy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLG1EQUFBO0VBQ0EsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtBQ0NGOztBREVBO0VBQ0UsbUJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmVjb3JkLXJ0Yy9yZWNvcmQtcnRjLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnZpZGVvIHtcclxuICBib3gtc2hhZG93OiAxcHggNnB4IDEwcHggMnB4IHJnYmEoMzUsIDM1LCAzNSwgMC42Mik7XHJcbiAgaGVpZ2h0OiA2MDBweDtcclxuICBtYXgtaGVpZ2h0OiA4MDBweDtcclxuICB3aWR0aDogMTA2N3B4O1xyXG59XHJcblxyXG4ucm93IHtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59IiwiLnZpZGVvIHtcbiAgYm94LXNoYWRvdzogMXB4IDZweCAxMHB4IDJweCByZ2JhKDM1LCAzNSwgMzUsIDAuNjIpO1xuICBoZWlnaHQ6IDYwMHB4O1xuICBtYXgtaGVpZ2h0OiA4MDBweDtcbiAgd2lkdGg6IDEwNjdweDtcbn1cblxuLnJvdyB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59Il19 */");

/***/ }),

/***/ "./src/app/components/record-rtc/record-rtc.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/record-rtc/record-rtc.component.ts ***!
  \***************************************************************/
/*! exports provided: RecordRTCComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecordRTCComponent", function() { return RecordRTCComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");


//let RecordRTC = require('recordrtc/RecordRTC.min');


var RecordRTCComponent = /** @class */ (function () {
    function RecordRTCComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
        // Do stuff
    }
    RecordRTCComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    RecordRTCComponent.prototype.saveMe = function () {
        //this.videoBlobService.addNewCreatedBlob(this.data);
        this.dialogRef.close();
    };
    RecordRTCComponent.prototype.ngAfterViewInit = function () {
        // set the initial state of the video
    };
    RecordRTCComponent.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: String, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    RecordRTCComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'record-rtc',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./record-rtc.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/record-rtc/record-rtc.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./record-rtc.component.scss */ "./src/app/components/record-rtc/record-rtc.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], String])
    ], RecordRTCComponent);
    return RecordRTCComponent;
}());



/***/ }),

/***/ "./src/app/components/register/register.component.scss":
/*!*************************************************************!*\
  !*** ./src/app/components/register/register.component.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mat-form-field.email {\n  /*   display: flex;\n  max-width: 350px;*/\n}\n\n.mat-form-field.names {\n  /*   max-width: 300px;*/\n}\n\n.mat-form-field.roleOther {\n  /*  max-width: 250px;*/\n}\n\n.mat-form-field.roles {\n  /* max-width: 250px;*/\n}\n\n.mat-form-field.password {\n  /* max-width: 250px;*/\n}\n\n.mat-form-field.username {\n  /* max-width: 250px;*/\n}\n\n.full-width {\n  width: 100%;\n  max-width: 350px;\n}\n\n.row {\n  display: flex;\n  flex-direction: row;\n}\n\n.col {\n  flex: 1;\n  margin-right: 20px;\n}\n\n.col:last-child {\n  margin-right: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9yZWdpc3Rlci9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcY29tcG9uZW50c1xccmVnaXN0ZXJcXHJlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL3JlZ2lzdGVyL3JlZ2lzdGVyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFDO0VBQ0U7b0JBQUE7QUNFSDs7QURDQztFQUNFLHVCQUFBO0FDRUg7O0FEQUM7RUFDRSxzQkFBQTtBQ0dIOztBREFDO0VBQ0UscUJBQUE7QUNHSDs7QURBQztFQUNFLHFCQUFBO0FDR0g7O0FEQUM7RUFDQyxxQkFBQTtBQ0dGOztBRENDO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0FDRUg7O0FEQ0M7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7QUNFSDs7QURDQztFQUNFLE9BQUE7RUFDQSxrQkFBQTtBQ0VIOztBREFDO0VBQ0UsZUFBQTtBQ0dIIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAubWF0LWZvcm0tZmllbGQuZW1haWwgIHtcclxuICAgLyogICBkaXNwbGF5OiBmbGV4O1xyXG4gbWF4LXdpZHRoOiAzNTBweDsqL1xyXG4gfVxyXG4gLm1hdC1mb3JtLWZpZWxkLm5hbWVzICB7XHJcbiAgIC8qICAgbWF4LXdpZHRoOiAzMDBweDsqL1xyXG4gfVxyXG4gLm1hdC1mb3JtLWZpZWxkLnJvbGVPdGhlciAge1xyXG4gICAvKiAgbWF4LXdpZHRoOiAyNTBweDsqL1xyXG4gfVxyXG4gXHJcbiAubWF0LWZvcm0tZmllbGQucm9sZXMgIHtcclxuICAgLyogbWF4LXdpZHRoOiAyNTBweDsqL1xyXG4gfVxyXG5cclxuIC5tYXQtZm9ybS1maWVsZC5wYXNzd29yZCAge1xyXG4gICAvKiBtYXgtd2lkdGg6IDI1MHB4OyovXHJcbiB9XHJcblxyXG4gLm1hdC1mb3JtLWZpZWxkLnVzZXJuYW1lICB7XHJcbiAgLyogbWF4LXdpZHRoOiAyNTBweDsqL1xyXG4gfVxyXG5cclxuIFxyXG4gLmZ1bGwtd2lkdGgge1xyXG4gICB3aWR0aDogMTAwJTtcclxuICAgbWF4LXdpZHRoOiAzNTBweDtcclxuIH1cclxuXHJcbiAucm93IHtcclxuICAgZGlzcGxheTogZmxleDtcclxuICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuIH1cclxuXHJcbiAuY29sIHtcclxuICAgZmxleDogMTtcclxuICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gfVxyXG4gLmNvbDpsYXN0LWNoaWxkIHtcclxuICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gfVxyXG4iLCIubWF0LWZvcm0tZmllbGQuZW1haWwge1xuICAvKiAgIGRpc3BsYXk6IGZsZXg7XG4gIG1heC13aWR0aDogMzUwcHg7Ki9cbn1cblxuLm1hdC1mb3JtLWZpZWxkLm5hbWVzIHtcbiAgLyogICBtYXgtd2lkdGg6IDMwMHB4OyovXG59XG5cbi5tYXQtZm9ybS1maWVsZC5yb2xlT3RoZXIge1xuICAvKiAgbWF4LXdpZHRoOiAyNTBweDsqL1xufVxuXG4ubWF0LWZvcm0tZmllbGQucm9sZXMge1xuICAvKiBtYXgtd2lkdGg6IDI1MHB4OyovXG59XG5cbi5tYXQtZm9ybS1maWVsZC5wYXNzd29yZCB7XG4gIC8qIG1heC13aWR0aDogMjUwcHg7Ki9cbn1cblxuLm1hdC1mb3JtLWZpZWxkLnVzZXJuYW1lIHtcbiAgLyogbWF4LXdpZHRoOiAyNTBweDsqL1xufVxuXG4uZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDM1MHB4O1xufVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cblxuLmNvbCB7XG4gIGZsZXg6IDE7XG4gIG1hcmdpbi1yaWdodDogMjBweDtcbn1cblxuLmNvbDpsYXN0LWNoaWxkIHtcbiAgbWFyZ2luLXJpZ2h0OiAwO1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/components/register/register.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/register/register.component.ts ***!
  \***********************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _rxweb_reactive_form_validators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @rxweb/reactive-form-validators */ "./node_modules/@rxweb/reactive-form-validators/fesm5/rxweb-reactive-form-validators.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");






var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(_formBuilder, messageDispatchService, authenticationService) {
        this._formBuilder = _formBuilder;
        this.messageDispatchService = messageDispatchService;
        this.authenticationService = authenticationService;
        this.activeStepIndex = 1;
        //email checking
        this.currentEmailValue = "";
        this.isSearchingEmail = false;
        this.emailIsAvailable = false;
        this.emailHasBeenChecked = false;
        this.roleOtherIsShowed = false;
        this.currentUsernameValue = "";
        this.isSearchingUsername = false;
        this.usernameIsAvailable = false;
        this.usernameHasBeenChecked = false;
        this.messages = [];
        this.roleGroups = [
            {
                label: "Talent",
                items: [
                    { label: 'Actor', value: 3 },
                    { label: 'Voice Actor', value: 4 },
                    { label: 'Model', value: 5 },
                    { label: 'Dancer', value: 6 }
                ]
            },
            {
                label: "Representation",
                items: [
                    { label: 'Agent', value: 7 }
                ]
            },
            {
                label: "Production",
                items: [
                    { label: 'Casting Director', value: 15 },
                    { label: 'Producer', value: 16 },
                    { label: 'Director', value: 17 }
                ]
            }
        ];
    }
    // @ViewChild('formDirective', {static: false}) private formDirective: NgForm;
    RegisterComponent.prototype.test = function () {
        debugger;
        var t = this.basicUserInfo;
    };
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.formSteps = [
            { label: 'Step 1' },
            { label: 'Step 2' },
            { label: 'Step 3' }
        ];
        //basic user info
        this.basicUserInfo = this._formBuilder.group({
            email: ['fdgld@kjtrtr.com', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, this.validateEmailNotTaken.bind(this)]],
            firstNames: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            lastNames: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            role: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            roleOther: [''],
        }, { updateOn: 'blur' });
        //userlogin info
        this.userLoginInfo = this._formBuilder.group({
            username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validateUsernameNotTaken.bind(this)]],
            //password: ['', [Validators.required, RxwebValidators.password({ validation: { maxLength: 10, minLength: 5, digit: true, specialCharacter: true } })]],
            password: ['Brazil99?', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, , _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]],
            passwordCompare: ['Brazil99?', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _rxweb_reactive_form_validators__WEBPACK_IMPORTED_MODULE_4__["RxwebValidators"].compare({ fieldName: 'password' })]]
        }, { updateOn: 'blur' });
        this.basicUserInfo.get('role').valueChanges.subscribe(function (value) {
            debugger;
            //this.basicUserInfo.get('roleOther').validator('required')
            _this.basicUserInfo.get('roleOther').clearValidators();
            _this.basicUserInfo.get('roleOther').reset();
            _this.basicUserInfo.get('roleOther').setErrors(null);
            _this.basicUserInfo.get('roleOther').markAsUntouched();
            _this.basicUserInfo.get('roleOther').markAsPristine();
            if (value && parseInt(value) === 1) {
                _this.basicUserInfo.get('roleOther').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
                _this.roleOtherIsShowed = true;
            }
            else {
                _this.roleOtherIsShowed = false;
            }
            _this.basicUserInfo.get('roleOther').reset();
            _this.basicUserInfo.get('roleOther').setErrors(null);
            _this.basicUserInfo.get('roleOther').markAsUntouched();
            _this.basicUserInfo.get('roleOther').markAsPristine();
        });
        //email availability check
        this.basicUserInfo.get('email').valueChanges.subscribe(function (value) {
            _this.clearMessages();
            //make sure the value really changed as we force the value change call at the end of this method
            if (_this.currentEmailValue !== _this.basicUserInfo.get('email').value) {
                if (!_this.basicUserInfo.get('email').hasError('required') &&
                    !_this.basicUserInfo.get('email').hasError('email')) {
                    _this.isSearchingEmail = true;
                    _this.authenticationService.checkEmailAvailability(_this.basicUserInfo.get('email').value)
                        .then(function (response) {
                        _this.currentEmailValue = _this.basicUserInfo.get('email').value;
                        _this.isSearchingEmail = false;
                        _this.emailHasBeenChecked = true;
                        _this.emailIsAvailable = response;
                        _this.basicUserInfo.get('email').updateValueAndValidity();
                        if (response === false) {
                        }
                        else {
                        }
                    })
                        .catch(function (error) {
                        _this.isSearchingEmail = false;
                        _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Email Verification Error");
                        _this.messages.push({
                            severity: 'error',
                            summary: "Email Verification Error",
                            detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                        });
                    });
                }
            }
        });
        //username availability check
        this.userLoginInfo.get('username').valueChanges.subscribe(function (value) {
            _this.clearMessages();
            //make sure the value really changed as we force the value change call at the end of this method
            if (_this.currentUsernameValue !== _this.userLoginInfo.get('username').value) {
                if (!_this.userLoginInfo.get('username').hasError('required')) {
                    _this.isSearchingUsername = true;
                    _this.authenticationService.checkUsernameAvailability(_this.userLoginInfo.get('username').value)
                        .then(function (response) {
                        _this.currentUsernameValue = _this.userLoginInfo.get('username').value;
                        _this.isSearchingUsername = false;
                        _this.usernameHasBeenChecked = true;
                        _this.usernameIsAvailable = response;
                        _this.userLoginInfo.get('username').updateValueAndValidity();
                        if (response === false) {
                        }
                        else {
                        }
                    })
                        .catch(function (error) {
                        _this.isSearchingUsername = false;
                        _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Username Verification Error");
                        _this.messages.push({
                            severity: 'error',
                            summary: "Username Verification Error",
                            detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                        });
                    });
                }
            }
        });
    };
    RegisterComponent.prototype.validateEmailNotTaken = function (control) {
        if (this.emailHasBeenChecked && !this.emailIsAvailable && (!this.basicUserInfo.get('email').hasError('required') &&
            !this.basicUserInfo.get('email').hasError('email'))) {
            return {
                emailTaken: true
            };
        }
        return null;
    };
    RegisterComponent.prototype.validateRoleOther = function (control) {
        // debugger;
        if (this.basicUserInfo.get('role').value && parseInt(this.basicUserInfo.get('role').value) === 1) {
            if (!this.basicUserInfo.get('roleOther').value ||
                this.basicUserInfo.get('roleOther').value === '') {
                return { required: true };
            }
        }
        return null;
    };
    //username checking
    RegisterComponent.prototype.validateUsernameNotTaken = function (control) {
        if (this.usernameHasBeenChecked && !this.usernameIsAvailable && !this.userLoginInfo.get('username').hasError('required')) {
            return {
                usernameTaken: true
            };
        }
        return null;
    };
    RegisterComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    RegisterComponent.prototype.moveToStep = function (stepIndex) {
        this.clearMessages();
        this.activeStepIndex = stepIndex;
    };
    RegisterComponent.prototype.moveToRegistterStep = function () {
        if (this.basicUserInfo.valid) {
            this.moveToStep(2);
        }
        else {
        }
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.clearMessages();
        if (this.basicUserInfo.invalid) {
            this.moveToStep(1);
        }
        else {
            if (this.userLoginInfo.valid && this.basicUserInfo.valid) {
                this.authenticationService.register(this.userLoginInfo.get('username').value, this.userLoginInfo.get('password').value, this.userLoginInfo.get('passwordCompare').value, this.basicUserInfo.get('email').value, this.basicUserInfo.get('firstNames').value, this.basicUserInfo.get('lastNames').value, parseInt(this.basicUserInfo.get('role').value), this.basicUserInfo.get('roleOther').value)
                    .then(function (r) {
                    _this.messageDispatchService.dispatchSuccesMessage("Accout:'" + _this.userLoginInfo.get('username').value + "' was created successfully", "Accout Created");
                    //        stepper.next();
                })
                    .catch(function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Accout Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Accout Creation Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
                //.then(stop, stop);
            }
        }
    };
    RegisterComponent.prototype.stepChanged = function (event, stepper) {
        this.basicUserInfo.get('roleOther').clearValidators();
        event.selectedStep.interacted = false;
        event.selectedStep.interacted = false;
        event.selectedStep.hasError = false;
        event.selectedStep.stepControl.markAsPristine();
        event.selectedStep.stepControl.markAsUntouched();
        //this.basicUserInfo.get('roleOther').setErrors(null);
        //this.basicUserInfo.get('roleOther').markAsUntouched();
        // this.basicUserInfo.get('roleOther').markAsPristine();
        //stepper.selected.interacted = false;
    };
    RegisterComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_5__["MessageDispatchService"] },
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] }
    ]; };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./register.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/register/register.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./register.component.scss */ "./src/app/components/register/register.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_5__["MessageDispatchService"],
            _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/components/toolbar/toolbar.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/toolbar/toolbar.component.scss ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdG9vbGJhci90b29sYmFyLmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/components/toolbar/toolbar.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/toolbar/toolbar.component.ts ***!
  \*********************************************************/
/*! exports provided: ToolbarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToolbarComponent", function() { return ToolbarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ToolbarComponent = /** @class */ (function () {
    function ToolbarComponent() {
    }
    ToolbarComponent.prototype.ngOnInit = function () {
    };
    ToolbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-toolbar',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./toolbar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/components/toolbar/toolbar.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./toolbar.component.scss */ "./src/app/components/toolbar/toolbar.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ToolbarComponent);
    return ToolbarComponent;
}());



/***/ }),

/***/ "./src/app/directives/drop-file.directive.ts":
/*!***************************************************!*\
  !*** ./src/app/directives/drop-file.directive.ts ***!
  \***************************************************/
/*! exports provided: DropFileDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropFileDirective", function() { return DropFileDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DropFileDirective = /** @class */ (function () {
    function DropFileDirective() {
        this.onFileDropped = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.background = '#f5fcff';
        this.opacity = '1';
    }
    //Dragover listener
    DropFileDirective.prototype.onDragOver = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.background = '#9ecbec';
        this.opacity = '0.8';
    };
    //Dragleave listener
    DropFileDirective.prototype.onDragLeave = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.background = '#f5fcff';
        this.opacity = '1';
    };
    //Drop listener
    DropFileDirective.prototype.ondrop = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.background = '#f5fcff';
        this.opacity = '1';
        var files = event.dataTransfer.files;
        if (files.length > 0) {
            this.onFileDropped.emit(files);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DropFileDirective.prototype, "onFileDropped", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('style.background-color'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DropFileDirective.prototype, "background", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('style.opacity'),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DropFileDirective.prototype, "opacity", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('dragover', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], DropFileDirective.prototype, "onDragOver", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('dragleave', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], DropFileDirective.prototype, "onDragLeave", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('drop', ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], DropFileDirective.prototype, "ondrop", null);
    DropFileDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: "[appDropFile]"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DropFileDirective);
    return DropFileDirective;
}());



/***/ }),

/***/ "./src/app/directives/email-availability-validator.directive.ts":
/*!**********************************************************************!*\
  !*** ./src/app/directives/email-availability-validator.directive.ts ***!
  \**********************************************************************/
/*! exports provided: EmailAvailabilityValidatorDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailAvailabilityValidatorDirective", function() { return EmailAvailabilityValidatorDirective; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var EmailAvailabilityValidatorDirective = /** @class */ (function () {
    function EmailAvailabilityValidatorDirective() {
    }
    EmailAvailabilityValidatorDirective = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"])({
            selector: '[appEmailAvailabilityValidator]'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], EmailAvailabilityValidatorDirective);
    return EmailAvailabilityValidatorDirective;
}());



/***/ }),

/***/ "./src/app/global/global.module.ts":
/*!*****************************************!*\
  !*** ./src/app/global/global.module.ts ***!
  \*****************************************/
/*! exports provided: GlobalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalModule", function() { return GlobalModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/esm5/scrolling.es5.js");
/* harmony import */ var _services_authentication_service_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/authentication.service.js */ "./src/app/services/authentication.service.js");
/* harmony import */ var _pipes_bytes_pipe_pipe__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../pipes/bytes-pipe.pipe */ "./src/app/pipes/bytes-pipe.pipe.ts");
/* harmony import */ var _pipes_linebreak_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pipes/linebreak.pipe */ "./src/app/pipes/linebreak.pipe.ts");
/* harmony import */ var primeng_messages__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/messages */ "./node_modules/primeng/fesm5/primeng-messages.js");
/* harmony import */ var primeng_message__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/message */ "./node_modules/primeng/fesm5/primeng-message.js");
/* harmony import */ var primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/inputtext */ "./node_modules/primeng/fesm5/primeng-inputtext.js");
/* harmony import */ var primeng_chips__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/chips */ "./node_modules/primeng/fesm5/primeng-chips.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/fesm5/primeng-calendar.js");
/* harmony import */ var primeng_keyfilter__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/keyfilter */ "./node_modules/primeng/fesm5/primeng-keyfilter.js");
/* harmony import */ var primeng_listbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! primeng/listbox */ "./node_modules/primeng/fesm5/primeng-listbox.js");
/* harmony import */ var primeng_multiselect__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! primeng/multiselect */ "./node_modules/primeng/fesm5/primeng-multiselect.js");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/fesm5/primeng-radiobutton.js");
/* harmony import */ var primeng_slider__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! primeng/slider */ "./node_modules/primeng/fesm5/primeng-slider.js");
/* harmony import */ var primeng_selectbutton__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! primeng/selectbutton */ "./node_modules/primeng/fesm5/primeng-selectbutton.js");
/* harmony import */ var primeng_tristatecheckbox__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! primeng/tristatecheckbox */ "./node_modules/primeng/fesm5/primeng-tristatecheckbox.js");
/* harmony import */ var primeng_autocomplete__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! primeng/autocomplete */ "./node_modules/primeng/fesm5/primeng-autocomplete.js");
/* harmony import */ var primeng_checkbox__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! primeng/checkbox */ "./node_modules/primeng/fesm5/primeng-checkbox.js");
/* harmony import */ var primeng_colorpicker__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! primeng/colorpicker */ "./node_modules/primeng/fesm5/primeng-colorpicker.js");
/* harmony import */ var primeng_inputswitch__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! primeng/inputswitch */ "./node_modules/primeng/fesm5/primeng-inputswitch.js");
/* harmony import */ var primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! primeng/inputtextarea */ "./node_modules/primeng/fesm5/primeng-inputtextarea.js");
/* harmony import */ var primeng_inputmask__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! primeng/inputmask */ "./node_modules/primeng/fesm5/primeng-inputmask.js");
/* harmony import */ var primeng_password__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! primeng/password */ "./node_modules/primeng/fesm5/primeng-password.js");
/* harmony import */ var primeng_rating__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! primeng/rating */ "./node_modules/primeng/fesm5/primeng-rating.js");
/* harmony import */ var primeng_spinner__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! primeng/spinner */ "./node_modules/primeng/fesm5/primeng-spinner.js");
/* harmony import */ var primeng_togglebutton__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! primeng/togglebutton */ "./node_modules/primeng/fesm5/primeng-togglebutton.js");
/* harmony import */ var primeng_button__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! primeng/button */ "./node_modules/primeng/fesm5/primeng-button.js");
/* harmony import */ var primeng_toolbar__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! primeng/toolbar */ "./node_modules/primeng/fesm5/primeng-toolbar.js");
/* harmony import */ var primeng_card__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! primeng/card */ "./node_modules/primeng/fesm5/primeng-card.js");
/* harmony import */ var primeng_panel__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! primeng/panel */ "./node_modules/primeng/fesm5/primeng-panel.js");
/* harmony import */ var primeng_tabview__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! primeng/tabview */ "./node_modules/primeng/fesm5/primeng-tabview.js");
/* harmony import */ var primeng_sidebar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! primeng/sidebar */ "./node_modules/primeng/fesm5/primeng-sidebar.js");
/* harmony import */ var primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! primeng/scrollpanel */ "./node_modules/primeng/fesm5/primeng-scrollpanel.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm5/primeng-dialog.js");
/* harmony import */ var primeng_fileupload__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! primeng/fileupload */ "./node_modules/primeng/fesm5/primeng-fileupload.js");
/* harmony import */ var primeng_toast__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! primeng/toast */ "./node_modules/primeng/fesm5/primeng-toast.js");
/* harmony import */ var primeng_menu__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! primeng/menu */ "./node_modules/primeng/fesm5/primeng-menu.js");
/* harmony import */ var primeng_progressspinner__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! primeng/progressspinner */ "./node_modules/primeng/fesm5/primeng-progressspinner.js");
/* harmony import */ var primeng_steps__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! primeng/steps */ "./node_modules/primeng/fesm5/primeng-steps.js");
/* harmony import */ var primeng_fieldset__WEBPACK_IMPORTED_MODULE_44__ = __webpack_require__(/*! primeng/fieldset */ "./node_modules/primeng/fesm5/primeng-fieldset.js");
/* harmony import */ var primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45__ = __webpack_require__(/*! primeng/overlaypanel */ "./node_modules/primeng/fesm5/primeng-overlaypanel.js");
/* harmony import */ var _directives_drop_file_directive__WEBPACK_IMPORTED_MODULE_46__ = __webpack_require__(/*! ../directives/drop-file.directive */ "./src/app/directives/drop-file.directive.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_47__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");









//import { TextMaskModule } from 'angular2-text-mask';
//primeNg components
















//import { EditorModule } from 'primeng/editor';
























var GlobalModule = /** @class */ (function () {
    function GlobalModule() {
    }
    GlobalModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _pipes_bytes_pipe_pipe__WEBPACK_IMPORTED_MODULE_6__["BytesPipePipe"],
                _directives_drop_file_directive__WEBPACK_IMPORTED_MODULE_46__["DropFileDirective"],
                _pipes_linebreak_pipe__WEBPACK_IMPORTED_MODULE_7__["LinebreakPipe"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_4__["ScrollDispatchModule"],
                // BrowserAnimationsModule,
                // TextMaskModule,
                //primeNg components
                primeng_dialog__WEBPACK_IMPORTED_MODULE_38__["DialogModule"],
                primeng_messages__WEBPACK_IMPORTED_MODULE_8__["MessagesModule"],
                primeng_message__WEBPACK_IMPORTED_MODULE_9__["MessageModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__["InputTextModule"],
                primeng_chips__WEBPACK_IMPORTED_MODULE_11__["ChipsModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__["DropdownModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_13__["CalendarModule"],
                primeng_keyfilter__WEBPACK_IMPORTED_MODULE_14__["KeyFilterModule"],
                primeng_listbox__WEBPACK_IMPORTED_MODULE_15__["ListboxModule"],
                primeng_multiselect__WEBPACK_IMPORTED_MODULE_16__["MultiSelectModule"],
                primeng_radiobutton__WEBPACK_IMPORTED_MODULE_17__["RadioButtonModule"],
                primeng_slider__WEBPACK_IMPORTED_MODULE_18__["SliderModule"],
                primeng_selectbutton__WEBPACK_IMPORTED_MODULE_19__["SelectButtonModule"],
                primeng_tristatecheckbox__WEBPACK_IMPORTED_MODULE_20__["TriStateCheckboxModule"],
                primeng_autocomplete__WEBPACK_IMPORTED_MODULE_21__["AutoCompleteModule"],
                primeng_checkbox__WEBPACK_IMPORTED_MODULE_22__["CheckboxModule"],
                primeng_colorpicker__WEBPACK_IMPORTED_MODULE_23__["ColorPickerModule"],
                // EditorModule,
                primeng_inputswitch__WEBPACK_IMPORTED_MODULE_24__["InputSwitchModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_25__["InputTextareaModule"],
                primeng_inputmask__WEBPACK_IMPORTED_MODULE_26__["InputMaskModule"],
                primeng_password__WEBPACK_IMPORTED_MODULE_27__["PasswordModule"],
                primeng_rating__WEBPACK_IMPORTED_MODULE_28__["RatingModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_29__["SpinnerModule"],
                primeng_togglebutton__WEBPACK_IMPORTED_MODULE_30__["ToggleButtonModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_31__["ButtonModule"],
                primeng_toolbar__WEBPACK_IMPORTED_MODULE_32__["ToolbarModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_33__["CardModule"],
                primeng_panel__WEBPACK_IMPORTED_MODULE_34__["PanelModule"],
                primeng_tabview__WEBPACK_IMPORTED_MODULE_35__["TabViewModule"],
                primeng_sidebar__WEBPACK_IMPORTED_MODULE_36__["SidebarModule"],
                primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_37__["ScrollPanelModule"],
                primeng_fileupload__WEBPACK_IMPORTED_MODULE_39__["FileUploadModule"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_40__["ToastModule"],
                primeng_menu__WEBPACK_IMPORTED_MODULE_41__["MenuModule"],
                primeng_progressspinner__WEBPACK_IMPORTED_MODULE_42__["ProgressSpinnerModule"],
                primeng_steps__WEBPACK_IMPORTED_MODULE_43__["StepsModule"],
                primeng_fieldset__WEBPACK_IMPORTED_MODULE_44__["FieldsetModule"],
                primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45__["OverlayPanelModule"],
            ],
            exports: [
                _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_4__["ScrollDispatchModule"],
                _pipes_bytes_pipe_pipe__WEBPACK_IMPORTED_MODULE_6__["BytesPipePipe"],
                _pipes_linebreak_pipe__WEBPACK_IMPORTED_MODULE_7__["LinebreakPipe"],
                // TextMaskModule,
                //primeNg components
                primeng_messages__WEBPACK_IMPORTED_MODULE_8__["MessagesModule"],
                primeng_message__WEBPACK_IMPORTED_MODULE_9__["MessageModule"],
                primeng_inputtext__WEBPACK_IMPORTED_MODULE_10__["InputTextModule"],
                primeng_chips__WEBPACK_IMPORTED_MODULE_11__["ChipsModule"],
                primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__["DropdownModule"],
                primeng_calendar__WEBPACK_IMPORTED_MODULE_13__["CalendarModule"],
                primeng_keyfilter__WEBPACK_IMPORTED_MODULE_14__["KeyFilterModule"],
                primeng_listbox__WEBPACK_IMPORTED_MODULE_15__["ListboxModule"],
                primeng_multiselect__WEBPACK_IMPORTED_MODULE_16__["MultiSelectModule"],
                primeng_radiobutton__WEBPACK_IMPORTED_MODULE_17__["RadioButtonModule"],
                primeng_slider__WEBPACK_IMPORTED_MODULE_18__["SliderModule"],
                primeng_selectbutton__WEBPACK_IMPORTED_MODULE_19__["SelectButtonModule"],
                primeng_tristatecheckbox__WEBPACK_IMPORTED_MODULE_20__["TriStateCheckboxModule"],
                primeng_autocomplete__WEBPACK_IMPORTED_MODULE_21__["AutoCompleteModule"],
                primeng_checkbox__WEBPACK_IMPORTED_MODULE_22__["CheckboxModule"],
                primeng_colorpicker__WEBPACK_IMPORTED_MODULE_23__["ColorPickerModule"],
                //EditorModule,
                primeng_inputswitch__WEBPACK_IMPORTED_MODULE_24__["InputSwitchModule"],
                primeng_inputtextarea__WEBPACK_IMPORTED_MODULE_25__["InputTextareaModule"],
                primeng_inputmask__WEBPACK_IMPORTED_MODULE_26__["InputMaskModule"],
                primeng_password__WEBPACK_IMPORTED_MODULE_27__["PasswordModule"],
                primeng_rating__WEBPACK_IMPORTED_MODULE_28__["RatingModule"],
                primeng_spinner__WEBPACK_IMPORTED_MODULE_29__["SpinnerModule"],
                primeng_togglebutton__WEBPACK_IMPORTED_MODULE_30__["ToggleButtonModule"],
                primeng_button__WEBPACK_IMPORTED_MODULE_31__["ButtonModule"],
                primeng_toolbar__WEBPACK_IMPORTED_MODULE_32__["ToolbarModule"],
                primeng_card__WEBPACK_IMPORTED_MODULE_33__["CardModule"],
                primeng_panel__WEBPACK_IMPORTED_MODULE_34__["PanelModule"],
                primeng_tabview__WEBPACK_IMPORTED_MODULE_35__["TabViewModule"],
                primeng_sidebar__WEBPACK_IMPORTED_MODULE_36__["SidebarModule"],
                primeng_scrollpanel__WEBPACK_IMPORTED_MODULE_37__["ScrollPanelModule"],
                primeng_dialog__WEBPACK_IMPORTED_MODULE_38__["DialogModule"],
                primeng_fileupload__WEBPACK_IMPORTED_MODULE_39__["FileUploadModule"],
                _directives_drop_file_directive__WEBPACK_IMPORTED_MODULE_46__["DropFileDirective"],
                primeng_toast__WEBPACK_IMPORTED_MODULE_40__["ToastModule"],
                primeng_menu__WEBPACK_IMPORTED_MODULE_41__["MenuModule"],
                primeng_progressspinner__WEBPACK_IMPORTED_MODULE_42__["ProgressSpinnerModule"],
                primeng_steps__WEBPACK_IMPORTED_MODULE_43__["StepsModule"],
                primeng_fieldset__WEBPACK_IMPORTED_MODULE_44__["FieldsetModule"],
                primeng_overlaypanel__WEBPACK_IMPORTED_MODULE_45__["OverlayPanelModule"],
            ],
            providers: [
                //WINDOW_PROVIDERS,
                primeng_api__WEBPACK_IMPORTED_MODULE_47__["MessageService"],
                _services_authentication_service_js__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"],
            ]
        })
    ], GlobalModule);
    return GlobalModule;
}());



/***/ }),

/***/ "./src/app/interceptors/errorInterceptor.ts":
/*!**************************************************!*\
  !*** ./src/app/interceptors/errorInterceptor.ts ***!
  \**************************************************/
/*! exports provided: ErrorInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorInterceptor", function() { return ErrorInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/authentication.service */ "./src/app/services/authentication.service.ts");






var ErrorInterceptor = /** @class */ (function () {
    function ErrorInterceptor(authenticationService, stateService) {
        this.authenticationService = authenticationService;
        this.stateService = stateService;
    }
    ErrorInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["catchError"])(function (err) {
            if (err.status === 401) {
                // auto logout if 401 response returned from api
                _this.authenticationService.logout();
                _this.stateService.go("login");
                // location.reload(true);
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])(err);
        }));
    };
    ErrorInterceptor.ctorParameters = function () { return [
        { type: _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_4__["StateService"] }
    ]; };
    ErrorInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"], _uirouter_core__WEBPACK_IMPORTED_MODULE_4__["StateService"]])
    ], ErrorInterceptor);
    return ErrorInterceptor;
}());



/***/ }),

/***/ "./src/app/interceptors/jwtInterceptor.ts":
/*!************************************************!*\
  !*** ./src/app/interceptors/jwtInterceptor.ts ***!
  \************************************************/
/*! exports provided: JwtInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtInterceptor", function() { return JwtInterceptor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var JwtInterceptor = /** @class */ (function () {
    function JwtInterceptor() {
    }
    JwtInterceptor.prototype.intercept = function (request, next) {
        // add authorization header with jwt token if available
        //
        if (localStorage.getItem('currentUser')) {
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.access_token) {
                request = request.clone({
                    setHeaders: {
                        Authorization: "Bearer " + currentUser.access_token.replace(" ", "")
                    }
                });
            }
        }
        return next.handle(request);
    };
    JwtInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], JwtInterceptor);
    return JwtInterceptor;
}());



/***/ }),

/***/ "./src/app/models/alertInterface.ts":
/*!******************************************!*\
  !*** ./src/app/models/alertInterface.ts ***!
  \******************************************/
/*! exports provided: AlertType */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertType", function() { return AlertType; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var AlertType;
(function (AlertType) {
    AlertType[AlertType["success"] = 0] = "success";
    AlertType[AlertType["error"] = 1] = "error";
    AlertType[AlertType["info"] = 2] = "info";
})(AlertType || (AlertType = {}));


/***/ }),

/***/ "./src/app/models/blobs.ts":
/*!*********************************!*\
  !*** ./src/app/models/blobs.ts ***!
  \*********************************/
/*! exports provided: uploadDestinationEnum, CreatedBlob, StudioTalentBlob, ProductionSideFile, BlobUploadState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uploadDestinationEnum", function() { return uploadDestinationEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreatedBlob", function() { return CreatedBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StudioTalentBlob", function() { return StudioTalentBlob; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionSideFile", function() { return ProductionSideFile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlobUploadState", function() { return BlobUploadState; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _utilities_blobFileHelper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utilities/blobFileHelper */ "./src/app/utilities/blobFileHelper.ts");



var uploadDestinationEnum;
(function (uploadDestinationEnum) {
    uploadDestinationEnum[uploadDestinationEnum["Talent"] = 1] = "Talent";
    uploadDestinationEnum[uploadDestinationEnum["Basket"] = 3] = "Basket";
})(uploadDestinationEnum || (uploadDestinationEnum = {}));
var CreatedBlob = /** @class */ (function () {
    function CreatedBlob(_id, _blob, _uploadUrl, _headers, _metadata) {
        this.blob = _blob;
        this.id = _id;
        this.uploadState = new BlobUploadState(_utilities_blobFileHelper__WEBPACK_IMPORTED_MODULE_2__["blobFileHelper"].blobToFile(_blob, "video.webm"), _uploadUrl, _headers, _metadata);
    }
    return CreatedBlob;
}());

var StudioTalentBlob = /** @class */ (function () {
    function StudioTalentBlob(_id, _blob, _talentId, _sessionId, _roleId, _talentName, _roleName, _sessionName) {
        if (_talentName === void 0) { _talentName = "studio_Talent"; }
        if (_roleName === void 0) { _roleName = "Role"; }
        if (_sessionName === void 0) { _sessionName = "session"; }
        this.uploadDestination = uploadDestinationEnum.Talent;
        this.isUploading = false;
        this.uploadedSize = 0;
        this.percentageUploaded = 0;
        this.isComplete = false;
        this.isSuccess = false;
        this.isError = false;
        this.uploadUrl = "https://cast.com/castingbasketvideoupload";
        this.file = _utilities_blobFileHelper__WEBPACK_IMPORTED_MODULE_2__["blobFileHelper"].blobToFile(_blob, _talentName + '_' + _roleName + '.webm');
        this.blob = _blob;
        this.totalSize = this.file.size;
        this.file = this.file;
        this.talentId = _talentId;
        this.roleId = _roleId;
        this.sessionId = _sessionId;
        this.talentName = _talentName;
        this.roleName = _roleName;
        this.sessionName = _sessionName;
        this.id = _id;
        this.obs = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    StudioTalentBlob.prototype.buildMetadata = function () {
        this.metadata = {
            name: this.talentName + '_' + this.roleName + '.webm',
            type: "webm/video",
            contentType: "video/*",
            talentId: this.talentId,
            roleId: this.roleId,
            sessionId: this.sessionId
        };
    };
    ;
    StudioTalentBlob.prototype.sartUpload = function () {
        if (this.upload) {
            this.isUploading = true;
            this.upload.start();
            this.obs.next(this);
        }
    };
    ;
    StudioTalentBlob.prototype.successCallback = function (file) {
        this.isUploading = false;
        this.isComplete = true;
        this.isSuccess = true;
        if (this.upload) {
            //this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
        this.obs.complete();
    };
    ;
    StudioTalentBlob.prototype.errorCallback = function (file, error) {
        this.isUploading = false;
        this.isComplete = true;
        this.isError = true;
        this.error = error;
        if (this.upload) {
            this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
    };
    ;
    StudioTalentBlob.prototype.progressCallback = function (file, bytesUploaded, bytesTotal, percentage) {
        //this.isUploading = true;
        this.uploadedSize = bytesUploaded;
        this.totalSize = bytesTotal;
        this.percentageUploaded = percentage;
        this.obs.next(this);
    };
    ;
    return StudioTalentBlob;
}());

var ProductionSideFile = /** @class */ (function () {
    function ProductionSideFile(_id, _file, _sideId, _productionId, _sideName, _productionName) {
        if (_sideName === void 0) { _sideName = "side name"; }
        if (_productionName === void 0) { _productionName = "production"; }
        this.uploadDestination = uploadDestinationEnum.Talent;
        this.isUploading = false;
        this.uploadedSize = 0;
        this.percentageUploaded = 0;
        this.isComplete = false;
        this.isSuccess = false;
        this.isError = false;
        this.uploadUrl = "//cast.com/castingsideupload";
        this.file = _file;
        this.totalSize = this.file.size;
        this.productionId = _productionId;
        this.productionName = _productionName;
        this.sideId = _sideId;
        this.sideName = _sideName;
        this.id = _id;
        this.obs = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    ProductionSideFile.prototype.buildMetadata = function () {
        this.metadata = {
            name: this.productionName + '_' + this.sideName + '.pdf',
            productionId: this.productionId,
            sideName: "my side",
            realName: this.file.name,
            contentType: "application/pdf",
            sideId: this.sideId,
            type: this.file.type,
        };
    };
    ;
    ProductionSideFile.prototype.sartUpload = function () {
        if (this.upload) {
            this.isUploading = true;
            this.upload.start();
            this.obs.next(this);
        }
    };
    ;
    ProductionSideFile.prototype.successCallback = function (file) {
        this.isUploading = false;
        this.isComplete = true;
        this.isSuccess = true;
        if (this.upload) {
            //this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
        this.obs.complete();
    };
    ;
    ProductionSideFile.prototype.errorCallback = function (file, error) {
        this.isUploading = false;
        this.isComplete = true;
        this.isError = true;
        this.error = error;
        if (this.upload) {
            this.upload.abort();
        }
        this.upload = null;
        this.obs.next(this);
    };
    ;
    ProductionSideFile.prototype.progressCallback = function (file, bytesUploaded, bytesTotal, percentage) {
        //this.isUploading = true;
        this.uploadedSize = bytesUploaded;
        this.totalSize = bytesTotal;
        this.percentageUploaded = percentage;
        this.obs.next(this);
    };
    ;
    return ProductionSideFile;
}());

var BlobUploadState = /** @class */ (function () {
    function BlobUploadState(_file, _uploadUrl, _headers, _metadata) {
        this._file = _file;
        this.uploadedSize = 0;
        this.percentageUploaded = 0;
        this.totalSize = _file.size;
        this.file = _file;
        this.uploadUrl = _uploadUrl;
        this.metadata = _metadata;
        this.headers = _headers;
        this.obs = new rxjs__WEBPACK_IMPORTED_MODULE_1__["Subject"]();
    }
    BlobUploadState.prototype.buildMetadata = function () {
    };
    ;
    BlobUploadState.prototype.sartUpload = function () {
        this.isUploading = true;
        this.obs.next(this);
    };
    ;
    BlobUploadState.prototype.successCallback = function (file) {
        this.isUploading = false;
        this.isComplete = true;
        this.isSuccess = true;
        this.obs.next(this);
    };
    ;
    BlobUploadState.prototype.errorCallback = function (file, error) {
        this.isUploading = false;
        this.isComplete = true;
        this.isError = true;
        this.error = error;
        this.obs.next(this);
    };
    ;
    BlobUploadState.prototype.progressCallback = function (file, bytesUploaded, bytesTotal, percentage) {
        this.isUploading = true;
        this.uploadedSize = bytesUploaded;
        this.totalSize = bytesTotal;
        this.percentageUploaded = percentage;
        this.obs.next(this);
    };
    ;
    return BlobUploadState;
}());



/***/ }),

/***/ "./src/app/models/confirmation.models.ts":
/*!***********************************************!*\
  !*** ./src/app/models/confirmation.models.ts ***!
  \***********************************************/
/*! exports provided: MessageInterface */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageInterface", function() { return MessageInterface; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var MessageInterface = /** @class */ (function () {
    function MessageInterface() {
    }
    return MessageInterface;
}());



/***/ }),

/***/ "./src/app/pipes/bytes-pipe.pipe.ts":
/*!******************************************!*\
  !*** ./src/app/pipes/bytes-pipe.pipe.ts ***!
  \******************************************/
/*! exports provided: BytesPipePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BytesPipePipe", function() { return BytesPipePipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var BytesPipePipe = /** @class */ (function () {
    function BytesPipePipe() {
    }
    BytesPipePipe.prototype.transform = function (bytes) {
        if (isNaN(parseFloat('' + bytes)) || !isFinite(bytes))
            return '-';
        if (bytes <= 0)
            return '0';
        var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(1) + ' ' + units[number];
    };
    BytesPipePipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({ name: 'bytes' })
    ], BytesPipePipe);
    return BytesPipePipe;
}());



/***/ }),

/***/ "./src/app/pipes/linebreak.pipe.ts":
/*!*****************************************!*\
  !*** ./src/app/pipes/linebreak.pipe.ts ***!
  \*****************************************/
/*! exports provided: LinebreakPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinebreakPipe", function() { return LinebreakPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LinebreakPipe = /** @class */ (function () {
    function LinebreakPipe() {
    }
    LinebreakPipe.prototype.transform = function (text) {
        return text.replace("/n", "<br/>");
    };
    LinebreakPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'linebreak'
        })
    ], LinebreakPipe);
    return LinebreakPipe;
}());



/***/ }),

/***/ "./src/app/services/alert.service.js":
/*!*******************************************!*\
  !*** ./src/app/services/alert.service.js ***!
  \*******************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _models_alertInterface__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/alertInterface */ "./src/app/models/alertInterface.ts");





var AlertService = /** @class */ (function () {
    function AlertService(transitionService) {
        var _this = this;
        this.transitionService = transitionService;
        this.message = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.keepAfterNavigationChange = false;
        transitionService.onBefore({ to: '*' }, function (trans) {
            if (_this.keepAfterNavigationChange) {
                _this.keepAfterNavigationChange = false;
            }
            else {
                _this.message.next();
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = true; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        var alertMessage = {
            alertType: _models_alertInterface__WEBPACK_IMPORTED_MODULE_4__["AlertType"].success,
            message: message,
            status: 0,
            originalMessage: message,
            fieldValidationMessages: []
        };
        this.message.next(alertMessage);
    };
    AlertService.prototype.error = function (error, message, extraValidation, keepAfterNavigationChange) {
        if (extraValidation === void 0) { extraValidation = []; }
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        var fieldValidations = [];
        var alert;
        if (error.error) {
            if (error.error.modelState) {
                for (var key in error.error.modelState) {
                    if (error.error.modelState[key] && error.error.modelState[key] instanceof Array) {
                        for (var i = 0; i < error.error.modelState[key].length; i++) {
                            fieldValidations.push(error.error.modelState[key][i]);
                        }
                    }
                    else {
                        if (error.error.modelState[key]) {
                            fieldValidations.push(error.error.modelState[key]);
                        }
                    }
                }
            }
            extraValidation.forEach(function (validationString) {
                fieldValidations.push(validationString);
            });
            var alertMessage = {
                alertType: _models_alertInterface__WEBPACK_IMPORTED_MODULE_4__["AlertType"].error,
                message: (message !== null && message.length > 0) ? message : (error.error.message) ? error.error.message : error.statusText,
                status: (error.error instanceof ErrorEvent) ? 0 : error.status,
                originalMessage: (error.error.message) ? error.error.message : error.statusText,
                fieldValidationMessages: fieldValidations
            };
            alert = alertMessage;
        }
        this.message.next(alert);
    };
    AlertService.prototype.getAlerts = function () {
        return this.message.asObservable();
    };
    AlertService.ctorParameters = function () { return [
        { type: _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__["TransitionService"] }
    ]; };
    AlertService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_uirouter_angular__WEBPACK_IMPORTED_MODULE_3__["TransitionService"]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ "./src/app/services/authentication.service.js":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.js ***!
  \****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _alert_service_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./alert.service.js */ "./src/app/services/alert.service.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");







var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(alertService, http, stateService, window) {
        this.alertService = alertService;
        this.http = http;
        this.stateService = stateService;
        this.window = window;
        this._logedInStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](this.checkAuthenticated());
        this.LogedInStateObs = this._logedInStateObs.asObservable();
    }
    AuthenticationService.prototype.register = function (username, password, confirmpassword, email, firstNames, lastNames, industryRole, rolesOther) {
        var _this = this;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var data = {
            firstNames: firstNames,
            lastNames: lastNames,
            industryRole: industryRole,
            rolesOther: rolesOther,
            username: username,
            password: password,
            confirmpassword: confirmpassword,
            email: email
        };
        return this.http.post("/api/account/register", data, httpOptions).toPromise()
            .then(function () {
            _this.alertService.success("User " + username + " successfuly registered");
            _this.login(username, password)
                .then(function () {
                _this.stateService.go('home');
            })
                .catch(function (error) {
            });
        }).catch(function (error) {
            _this._logedInStateObs.next(false);
            localStorage.removeItem('currentUser');
            _this.alertService.error(error, "Registration for user " + username + " failed");
        });
    };
    AuthenticationService.prototype.checkUsernameAvailability = function (username) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var data = {
            username: username
        };
        return this.http.post("/api/account/checkusername", data, httpOptions).toPromise()
            .then(function (response) {
            // this.alertService.success("User " + username + " successfuly registered")
            return response;
        }).catch(function (error) {
            //this._logedInStateObs.next(false);
            //localStorage.removeItem('currentUser');
            // this.alertService.error(error, "Registration for user " + username + " failed");
        });
    };
    AuthenticationService.prototype.checkEmailAvailability = function (email) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var data = {
            email: email
        };
        return this.http.post("/api/account/checkemail", data, httpOptions).toPromise()
            .then(function (response) {
            // this.alertService.success("User " + username + " successfuly registered")
            return response;
        }).catch(function (error) {
            //this._logedInStateObs.next(false);
            //localStorage.removeItem('currentUser');
            // this.alertService.error(error, "Registration for user " + username + " failed");
        });
    };
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        debugger;
        return this.http.post(window.location.protocol + "//cast.com/oauth2/token", "username=" + username + "&password=" + password + "&grant_type=password&client_id=099153c2625149bc8ecb3e85e03f0022").toPromise()
            .then(function (user) {
            if (user && user.access_token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                var currentUser = JSON.parse(localStorage.getItem('currentUser'));
                _this._logedInStateObs.next(true);
            }
        }).catch(function (error) {
            _this._logedInStateObs.next(false);
            localStorage.removeItem('currentUser');
            _this.alertService.error(error, "Login for " + username + " failed", [(error.error.error_description) ? error.error.error_description : error.error.error], false);
        });
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this._logedInStateObs.next(false);
        /*this.http.post<any>(
            "/api/account/logout",
            {  }
          ).toPromise()
          .then((response) => {
          }).catch((error) => {
       });*/
        //const $state = this.trans.router.stateService;
        this.stateService.go('login', undefined, undefined);
    };
    AuthenticationService.prototype.checkAuthenticated = function () {
        return !(localStorage.getItem("currentUser") === null);
    };
    AuthenticationService.ctorParameters = function () { return [
        { type: _alert_service_js__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_3__["StateService"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_6__["WINDOW"],] }] }
    ]; };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_6__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alert_service_js__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_3__["StateService"],
            Window])
    ], AuthenticationService);
    return AuthenticationService;
}());

//# sourceMappingURL=authentication.service.js.map

/***/ }),

/***/ "./src/app/services/authentication.service.ts":
/*!****************************************************!*\
  !*** ./src/app/services/authentication.service.ts ***!
  \****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _alert_service_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./alert.service.js */ "./src/app/services/alert.service.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");







var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(alertService, http, stateService, window) {
        this.alertService = alertService;
        this.http = http;
        this.stateService = stateService;
        this.window = window;
        this._logedInStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_4__["BehaviorSubject"](this.checkAuthenticated());
        this.LogedInStateObs = this._logedInStateObs.asObservable();
    }
    AuthenticationService.prototype.register = function (username, password, confirmpassword, email, firstNames, lastNames, industryRole, rolesOther) {
        var _this = this;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var data = {
            firstNames: firstNames,
            lastNames: lastNames,
            industryRole: industryRole,
            rolesOther: rolesOther,
            username: username,
            password: password,
            confirmpassword: confirmpassword,
            email: email
        };
        return this.http.post("/api/account/register", data, httpOptions).toPromise()
            .then(function () {
            _this.alertService.success("User " + username + " successfuly registered");
            _this.login(username, password)
                .then(function () {
                _this.stateService.go('home');
            })
                .catch(function (error) {
            });
        }).catch(function (error) {
            _this._logedInStateObs.next(false);
            localStorage.removeItem('currentUser');
            _this.alertService.error(error, "Registration for user " + username + " failed");
        });
    };
    AuthenticationService.prototype.checkUsernameAvailability = function (username) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var data = {
            username: username
        };
        return this.http.post("/api/account/checkusername", data, httpOptions).toPromise()
            .then(function (response) {
            // this.alertService.success("User " + username + " successfuly registered")
            return response;
        }).catch(function (error) {
            //this._logedInStateObs.next(false);
            //localStorage.removeItem('currentUser');
            // this.alertService.error(error, "Registration for user " + username + " failed");
        });
    };
    AuthenticationService.prototype.checkEmailAvailability = function (email) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var data = {
            email: email
        };
        return this.http.post("/api/account/checkemail", data, httpOptions).toPromise()
            .then(function (response) {
            // this.alertService.success("User " + username + " successfuly registered")
            return response;
        }).catch(function (error) {
            //this._logedInStateObs.next(false);
            //localStorage.removeItem('currentUser');
            // this.alertService.error(error, "Registration for user " + username + " failed");
        });
    };
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        debugger;
        return this.http.post(window.location.protocol + "//cast.com/oauth2/token", "username=" + username + "&password=" + password + "&grant_type=password&client_id=099153c2625149bc8ecb3e85e03f0022").toPromise()
            .then(function (user) {
            if (user && user.access_token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                var currentUser = JSON.parse(localStorage.getItem('currentUser'));
                _this._logedInStateObs.next(true);
            }
        }).catch(function (error) {
            _this._logedInStateObs.next(false);
            localStorage.removeItem('currentUser');
            _this.alertService.error(error, "Login for " + username + " failed", [(error.error.error_description) ? error.error.error_description : error.error.error], false);
        });
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this._logedInStateObs.next(false);
        /*this.http.post<any>(
            "/api/account/logout",
            {  }
          ).toPromise()
          .then((response) => {
          }).catch((error) => {
       });*/
        //const $state = this.trans.router.stateService;
        this.stateService.go('login', undefined, undefined);
    };
    AuthenticationService.prototype.checkAuthenticated = function () {
        return !(localStorage.getItem("currentUser") === null);
    };
    AuthenticationService.ctorParameters = function () { return [
        { type: _alert_service_js__WEBPACK_IMPORTED_MODULE_5__["AlertService"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_3__["StateService"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_6__["WINDOW"],] }] }
    ]; };
    AuthenticationService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_6__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_alert_service_js__WEBPACK_IMPORTED_MODULE_5__["AlertService"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_3__["StateService"],
            Window])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/services/browser.window.provider.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/browser.window.provider.ts ***!
  \*****************************************************/
/*! exports provided: WINDOW, WINDOW_PROVIDERS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WINDOW", function() { return WINDOW; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WINDOW_PROVIDERS", function() { return WINDOW_PROVIDERS; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var WINDOW = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["InjectionToken"]('window');
var windowProvider = {
    provide: WINDOW,
    useFactory: function () { return window; }
};
var WINDOW_PROVIDERS = [
    windowProvider
];


/***/ }),

/***/ "./src/app/services/casting-call-role.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/casting-call-role.service.ts ***!
  \*******************************************************/
/*! exports provided: CastingCallRoleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallRoleService", function() { return CastingCallRoleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var _utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utilities/HttpHelpers */ "./src/app/utilities/HttpHelpers.ts");







var CastingCallRoleService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CastingCallRoleService, _super);
    function CastingCallRoleService(http, window) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.window = window;
        _this._deleteRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteRoleObs = _this._deleteRoleObs.asObservable();
        _this._editRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editRoleObs = _this._editRoleObs.asObservable();
        _this._newRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newRoleObs = _this._newRoleObs.asObservable();
        _this.host = window.location.host;
        _this.hostName = window.location.hostname;
        _this.protocol = window.location.protocol;
        _this.port = window.location.port;
        return _this;
    }
    CastingCallRoleService.prototype.subscribeToCreateRoleObs = function () {
        return this.newRoleObs;
    };
    CastingCallRoleService.prototype.subscribeToEditRoleObs = function () {
        return this.editRoleObs;
    };
    CastingCallRoleService.prototype.subscribeToDeleteRoleObs = function () {
        return this.deleteRoleObs;
    };
    CastingCallRoleService.prototype.getBaseURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/roles/";
        return "/api/castingcallroles/";
    };
    CastingCallRoleService.prototype.createRole = function (id, role) {
        var _this = this;
        role.castingCallId = id;
        var controllerQuery = this.getBaseURL();
        return this.postAction(role, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newRoleObs.next(res);
            return res;
        }));
    };
    ;
    CastingCallRoleService.prototype.saveRole = function (id, role) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + id;
        return this.putAction(role, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editRoleObs.next(res);
            return res;
        }));
    };
    ;
    CastingCallRoleService.prototype.deleteRole = function (roleId) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + roleId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteRoleObs.next(res);
            return res;
        }));
    };
    ;
    CastingCallRoleService.prototype.getRoles = function (orderByIndex) {
        var controllerQuery = this.getBaseURL() + "?orderBy=" + orderByIndex;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    CastingCallRoleService.prototype.getRole = function (roleId) {
        var controllerQuery = this.getBaseURL() + roleId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    CastingCallRoleService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"],] }] }
    ]; };
    CastingCallRoleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], Window])
    ], CastingCallRoleService);
    return CastingCallRoleService;
}(_utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__["HttpHelpers"]));



/***/ }),

/***/ "./src/app/services/casting-call.service.ts":
/*!**************************************************!*\
  !*** ./src/app/services/casting-call.service.ts ***!
  \**************************************************/
/*! exports provided: CastingCallService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallService", function() { return CastingCallService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var _utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utilities/HttpHelpers */ "./src/app/utilities/HttpHelpers.ts");







var CastingCallService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CastingCallService, _super);
    function CastingCallService(http, window) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.window = window;
        _this._deleteCastingCallObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteCastingCallObs = _this._deleteCastingCallObs.asObservable();
        _this._editCastingCallObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editCastingCallObs = _this._editCastingCallObs.asObservable();
        _this._newCastingCallObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newCastingCallObs = _this._newCastingCallObs.asObservable();
        _this.host = window.location.host;
        _this.hostName = window.location.hostname;
        _this.protocol = window.location.protocol;
        _this.port = window.location.port;
        return _this;
    }
    CastingCallService.prototype.subscribeToCreateCastingCallObs = function () {
        return this.newCastingCallObs;
    };
    CastingCallService.prototype.subscribeToEditCastingCallObs = function () {
        return this.editCastingCallObs;
    };
    CastingCallService.prototype.subscribeToDeleteCastingCallObs = function () {
        return this.deleteCastingCallObs;
    };
    CastingCallService.prototype.getBaseURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/casting-calls/";
        return "/api/productions/";
    };
    CastingCallService.prototype.createCastingCall = function (castingCall, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + productionId + "/castingcalls";
        return this.postAction(castingCall, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newCastingCallObs.next(res);
            return res;
        }));
    };
    ;
    CastingCallService.prototype.saveCastingCall = function (id, productionId, castingCall) {
        var _this = this;
        castingCall.id = id;
        var controllerQuery = this.getBaseURL() + productionId + "/castingcalls/" + id;
        return this.putAction(castingCall, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editCastingCallObs.next(res);
            return res;
        }));
    };
    ;
    CastingCallService.prototype.deleteCastingCall = function (castingCallId, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + productionId + "/castingcalls/" + castingCallId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteCastingCallObs.next(res);
            return res;
        }));
    };
    ;
    CastingCallService.prototype.getCastingCalls = function (pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    CastingCallService.prototype.getProductionCastingCalls = function (productionId, pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL() + "production/" + productionId + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    CastingCallService.prototype.getCastingCall = function (castingCallId, productionId) {
        var controllerQuery = this.getBaseURL() + productionId + "/castingcalls/" + castingCallId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    CastingCallService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"],] }] }
    ]; };
    CastingCallService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], Window])
    ], CastingCallService);
    return CastingCallService;
}(_utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__["HttpHelpers"]));



/***/ }),

/***/ "./src/app/services/confirm-decision-subscription.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/confirm-decision-subscription.service.ts ***!
  \*******************************************************************/
/*! exports provided: ConfirmDecisionSubscriptionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmDecisionSubscriptionService", function() { return ConfirmDecisionSubscriptionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var ConfirmDecisionSubscriptionService = /** @class */ (function () {
    function ConfirmDecisionSubscriptionService() {
        this._confirmDecisionDispatcherObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.confirmDecisionDispatcherObs = this._confirmDecisionDispatcherObs.asObservable();
        this._decisionDispatcherObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.decisionDispatcherObs = this._decisionDispatcherObs.asObservable();
    }
    ConfirmDecisionSubscriptionService.prototype.subscribeToConfirmDecisionDispatcherObs = function () {
        return this._confirmDecisionDispatcherObs;
    };
    ConfirmDecisionSubscriptionService.prototype.subscribeToDecisionDispatcherObs = function () {
        return this._decisionDispatcherObs;
    };
    ConfirmDecisionSubscriptionService.prototype.poseConfirmDecision = function (summary, detail) {
        var message = { key: 'confirm', sticky: true, severity: 'warn', summary: summary, detail: detail };
        this._decisionDispatcherObs.next(message);
    };
    ConfirmDecisionSubscriptionService.prototype.dispatchAgreeDecision = function () {
        this._confirmDecisionDispatcherObs.next(true);
    };
    ConfirmDecisionSubscriptionService.prototype.dispatchDeclineDecision = function () {
        this._confirmDecisionDispatcherObs.next(false);
    };
    ConfirmDecisionSubscriptionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConfirmDecisionSubscriptionService);
    return ConfirmDecisionSubscriptionService;
}());



/***/ }),

/***/ "./src/app/services/confirmation-dialog.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/confirmation-dialog.service.ts ***!
  \*********************************************************/
/*! exports provided: ConfirmationDialogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmationDialogService", function() { return ConfirmationDialogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _models_confirmation_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/confirmation.models */ "./src/app/models/confirmation.models.ts");




var ConfirmationDialogService = /** @class */ (function () {
    function ConfirmationDialogService() {
        this._confirmationResultObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.confirmationResultObs = this._confirmationResultObs.asObservable();
    }
    ConfirmationDialogService.prototype.subscribeToConfirmationResultObs = function () {
        return this._confirmationResultObs;
    };
    ConfirmationDialogService.prototype.confirmAction = function (confirmationMessage, title, yesText, noText) {
        if (title === void 0) { title = "Confirm"; }
        if (yesText === void 0) { yesText = "Confirm"; }
        if (noText === void 0) { noText = "Cancel"; }
        var transferObject = new _models_confirmation_models__WEBPACK_IMPORTED_MODULE_3__["MessageInterface"];
        transferObject.title = title;
        transferObject.confirmationMessage = confirmationMessage;
        transferObject.yesText = yesText;
        transferObject.noText = noText;
        /* const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
         //width: '90%',
         data: transferObject,
         autoFocus: false,
        // backdropClass: 'logindialog-overlay',
        // panelClass: 'logindialog-panel'
       });
 
       dialogRef.afterClosed().subscribe(result => {
         let confirmationResult: boolean = result;
           this._confirmationResultObs.next(confirmationResult);
         // this.animal = result;
       });*/
        return this._confirmationResultObs;
    };
    ConfirmationDialogService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConfirmationDialogService);
    return ConfirmationDialogService;
}());



/***/ }),

/***/ "./src/app/services/file-upload.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/file-upload.service.ts ***!
  \*************************************************/
/*! exports provided: FileUploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FileUploadService", function() { return FileUploadService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var tus_js_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tus-js-client */ "./node_modules/tus-js-client/lib.es5/index.js");
/* harmony import */ var tus_js_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(tus_js_client__WEBPACK_IMPORTED_MODULE_3__);




var FileUploadService = /** @class */ (function () {
    function FileUploadService(window) {
        this.window = window;
    }
    FileUploadService.prototype.uploadFiles = function (uploadObjects) {
        var headers;
        if (localStorage.getItem('currentUser')) {
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.access_token) {
                headers =
                    {
                        Authorization: "Bearer " + currentUser.access_token.replace(" ", "")
                    };
            }
        }
        uploadObjects.forEach(function (blobUploadState) {
            blobUploadState.buildMetadata();
            var upload = new tus_js_client__WEBPACK_IMPORTED_MODULE_3__["Upload"](blobUploadState.file, {
                uploadUrl: window.location.protocol + blobUploadState.uploadUrl,
                endpoint: blobUploadState.uploadUrl,
                retryDelays: [0, 1000, 3000, 5000],
                resume: true,
                // chunkSize:1000000,
                metadata: blobUploadState.metadata,
                headers: headers,
                onError: function (error) {
                    blobUploadState.errorCallback(blobUploadState.file, error);
                    console.log('Failed: ' + blobUploadState.file.name + error);
                },
                onProgress: function (bytesUploaded, bytesTotal) {
                    var percentage = ((bytesUploaded / bytesTotal) * 100).toFixed(2);
                    blobUploadState.progressCallback(blobUploadState.file, bytesUploaded, bytesTotal, percentage);
                },
                onSuccess: function () {
                    if (upload.url.endsWith('/')) {
                        upload.url = upload.url.slice(0, -1);
                    }
                    var fileId = upload.url.split('/').pop();
                    blobUploadState.uploadedFileId = fileId;
                    console.log('Download ' + blobUploadState.uploadedFileId + ' from ' + upload.url + ' ' + blobUploadState.file.name);
                    blobUploadState.successCallback(blobUploadState.file);
                },
            });
            blobUploadState.upload = upload;
            blobUploadState.sartUpload();
        });
        return true;
    };
    FileUploadService.ctorParameters = function () { return [
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_2__["WINDOW"],] }] }
    ]; };
    FileUploadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_2__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Window])
    ], FileUploadService);
    return FileUploadService;
}());



/***/ }),

/***/ "./src/app/services/message-dispatch.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/message-dispatch.service.ts ***!
  \******************************************************/
/*! exports provided: MessageDispatchService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessageDispatchService", function() { return MessageDispatchService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var MessageDispatchService = /** @class */ (function () {
    function MessageDispatchService() {
        this._messageDispatcherObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.messageDispatcherObs = this._messageDispatcherObs.asObservable();
    }
    MessageDispatchService.prototype.subscribeToMessageDispatcherObs = function () {
        return this._messageDispatcherObs;
    };
    MessageDispatchService.prototype.dispatchSuccesMessage = function (message, summary) {
        var messages = [];
        messages.push({ severity: 'success', summary: summary, detail: message });
        this._messageDispatcherObs.next(messages);
    };
    MessageDispatchService.prototype.dispatchInfoMessage = function (message, summary) {
        var messages = [];
        messages.push({ severity: 'info', summary: summary, detail: message });
        this._messageDispatcherObs.next(messages);
    };
    MessageDispatchService.prototype.dispatchWarningMessage = function (message, summary) {
        var messages = [];
        messages.push({ severity: 'warn', summary: summary, detail: message });
        this._messageDispatcherObs.next(messages);
    };
    MessageDispatchService.prototype.dispatchErrorMessage = function (message, summary) {
        var messages = [];
        messages.push({ severity: 'error', summary: summary, detail: message });
        this._messageDispatcherObs.next(messages);
    };
    MessageDispatchService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MessageDispatchService);
    return MessageDispatchService;
}());



/***/ }),

/***/ "./src/app/services/metadata.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/metadata.service.ts ***!
  \**********************************************/
/*! exports provided: MetadataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MetadataService", function() { return MetadataService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utilities/HttpHelpers */ "./src/app/utilities/HttpHelpers.ts");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");





var MetadataService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](MetadataService, _super);
    function MetadataService(http, window) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.window = window;
        _this.host = window.location.host;
        _this.hostName = window.location.hostname;
        _this.protocol = window.location.protocol;
        _this.port = window.location.port;
        return _this;
    }
    MetadataService.prototype.getBaseURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/talents/";
        return "/api/metadata/";
    };
    MetadataService.prototype.getMetadata = function () {
        var _this = this;
        var prom;
        if (this._metadata) {
            prom = new Promise(function (resolve, reject) {
                resolve(_this._metadata);
            });
        }
        else {
            var controllerQuery_1 = this.getBaseURL() + "metadata";
            prom = new Promise(function (resolve, reject) {
                _this.getAction(controllerQuery_1)
                    .toPromise()
                    .then(function (res) {
                    _this._metadata = res;
                    resolve(_this._metadata);
                })
                    .catch(function (error) { return reject(error); });
            });
        }
        return prom;
    };
    MetadataService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_4__["WINDOW"],] }] }
    ]; };
    MetadataService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_4__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], Window])
    ], MetadataService);
    return MetadataService;
}(_utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_3__["HttpHelpers"]));



/***/ }),

/***/ "./src/app/services/production-extra.service.ts":
/*!******************************************************!*\
  !*** ./src/app/services/production-extra.service.ts ***!
  \******************************************************/
/*! exports provided: ProductionExtraService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionExtraService", function() { return ProductionExtraService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var _utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utilities/HttpHelpers */ "./src/app/utilities/HttpHelpers.ts");







var ProductionExtraService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ProductionExtraService, _super);
    function ProductionExtraService(http, window) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.window = window;
        /*
        private _sideCreatedObs: Subject<Side> = new Subject<Side>();
        private _payDetailCreatedObs: Subject<PayDetail> = new Subject<PayDetail>();
        private _shootDetailCreatedObs: Subject<ShootDetail> = new Subject<ShootDetail>();
        private _auditionDetailCreatedObs: Subject<AuditionDetail> = new Subject<AuditionDetail>();
    
        public readonly auditionDetailCreatedObs: Observable<AuditionDetail> = this._auditionDetailCreatedObs.asObservable();
    
        public subscribeToAuditionDetailCreatedObs(): Observable<AuditionDetail> {
          return this._auditionDetailCreatedObs;
        }
        public readonly shootDetailCreatedObs: Observable<ShootDetail> = this._shootDetailCreatedObs.asObservable();
    
        public subscribeToShootDetailCreatedObs(): Observable<ShootDetail> {
          return this._shootDetailCreatedObs;
        }
        public readonly payDetailCreatedObs: Observable<PayDetail> = this._payDetailCreatedObs.asObservable();
    
        public subscribeToPayDetailCreatedObs(): Observable<PayDetail> {
          return this._payDetailCreatedObs;
        }
        public readonly sideCreatedObs: Observable<Side> = this._sideCreatedObs.asObservable();
    
        public subscribeToSideCreatedObs(): Observable<Side> {
          return this._sideCreatedObs;
        }
    
        private _sideUpdatedObs: BehaviorSubject<Side> = new BehaviorSubject<Side>();
        private _payDetailUpdatedObs: BehaviorSubject<PayDetail> = new BehaviorSubject<PayDetail>();
        private _shootDetailUpdatedObs: BehaviorSubject<ShootDetail> = new BehaviorSubject<ShootDetail>();
        private _auditionDetailUpdatedObs: BehaviorSubject<AuditionDetail> = new BehaviorSubject<AuditionDetail>();
    
        public readonly auditionDetailUpdatedObs: Observable<AuditionDetail> = this._auditionDetailUpdatedObs.asObservable();
    
        public subscribeToAuditionDetailUpdatedObs(): Observable<AuditionDetail> {
          return this._auditionDetailUpdatedObs;
        }
        public readonly shootDetailUpdatedObs: Observable<ShootDetail> = this._shootDetailUpdatedObs.asObservable();
    
        public subscribeToShootDetailUpdatedObs(): Observable<ShootDetail> {
          return this._shootDetailUpdatedObs;
        }
        public readonly payDetailUpdatedObs: Observable<PayDetail> = this._payDetailUpdatedObs.asObservable();
    
        public subscribeToPayDetailUpdatedObs(): Observable<PayDetail> {
          return this._payDetailUpdatedObs;
        }
        public readonly sideUpdatedObs: Observable<Side> = this._sideUpdatedObs.asObservable();
    
        public subscribeToSideUpdatedObs(): Observable<Side> {
          return this._sideUpdatedObs;
        }
        private _sideDeletedObs: BehaviorSubject<Side> = new BehaviorSubject<Side>();
        private _payDetailDeletedObs: BehaviorSubject<PayDetail> = new BehaviorSubject<PayDetail>();
        private _shootDetailDeletedObs: BehaviorSubject<ShootDetail> = new BehaviorSubject<ShootDetail>();
        private _auditionDetailDeletedObs: BehaviorSubject<AuditionDetail> = new BehaviorSubject<AuditionDetail>();
    
        public readonly auditionDetailDeletedObs: Observable<AuditionDetail> = this._auditionDetailDeletedObs.asObservable();
    
        public subscribeToAuditionDetailDeletedObs(): Observable<AuditionDetail> {
          return this._auditionDetailDeletedObs;
        }
        public readonly shootDetailDeletedObs: Observable<ShootDetail> = this._shootDetailDeletedObs.asObservable();
    
        public subscribeToShootDetailDeletedObs(): Observable<ShootDetail> {
          return this._shootDetailDeletedObs;
        }
        public readonly payDetailDeletedObs: Observable<PayDetail> = this._payDetailDeletedObs.asObservable();
    
        public subscribeToPayDetailDeletedObs(): Observable<PayDetail> {
          return this._payDetailDeletedObs;
        }
        public readonly sideDeletedObs: Observable<Side> = this._sideDeletedObs.asObservable();
    
        public subscribeToSideDeletedObs(): Observable<Side> {
          return this._sideDeletedObs;
        }
         */
        _this._deleteAuditionDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteAuditionDetailObs = _this._deleteAuditionDetailObs.asObservable();
        _this._editAuditionDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editAuditionDetailObs = _this._editAuditionDetailObs.asObservable();
        _this._newAuditionDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newAuditionDetailObs = _this._newAuditionDetailObs.asObservable();
        _this._deleteShootDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteShootDetailObs = _this._deleteShootDetailObs.asObservable();
        _this._editShootDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editShootDetailObs = _this._editShootDetailObs.asObservable();
        _this._newShootDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newShootDetailObs = _this._newShootDetailObs.asObservable();
        _this._deletePayDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deletePayDetailObs = _this._deletePayDetailObs.asObservable();
        _this._editPayDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editPayDetailObs = _this._editPayDetailObs.asObservable();
        _this._newPayDetailObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newPayDetailObs = _this._newPayDetailObs.asObservable();
        _this._deleteSideObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteSideObs = _this._deleteSideObs.asObservable();
        _this._editSideObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editSideObs = _this._editSideObs.asObservable();
        _this._newSideObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newSideObs = _this._newSideObs.asObservable();
        _this.host = window.location.host;
        _this.hostName = window.location.hostname;
        _this.protocol = window.location.protocol;
        _this.port = window.location.port;
        return _this;
    }
    ProductionExtraService.prototype.getBaseURL = function (productionId) {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/productions/";
        return "/api/productions/" + productionId + "/extras";
    };
    ProductionExtraService.prototype.subscribeToCreateAuditionDetailObs = function () {
        return this.newAuditionDetailObs;
    };
    ProductionExtraService.prototype.subscribeToEditAuditionDetailObs = function () {
        return this.editAuditionDetailObs;
    };
    ProductionExtraService.prototype.subscribeToDeleteAuditionDetailObs = function () {
        return this.deleteAuditionDetailObs;
    };
    ProductionExtraService.prototype.getBaseAuditionDetailURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/auditionDetails/";
        return "/auditiondetails/";
    };
    ProductionExtraService.prototype.createAuditionDetail = function (productionId, auditionDetail) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL();
        return this.postAction(auditionDetail, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newAuditionDetailObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.saveAuditionDetail = function (id, productionId, auditionDetail) {
        var _this = this;
        auditionDetail.id = id;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + id;
        return this.putAction(auditionDetail, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editAuditionDetailObs.next(auditionDetail);
            return auditionDetail;
        }));
    };
    ;
    ProductionExtraService.prototype.deleteAuditionDetail = function (auditionDetailId, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + auditionDetailId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteAuditionDetailObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getAuditionDetails = function (productionId, pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getAuditionDetail = function (auditionDetailId, productionId) {
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseAuditionDetailURL() + auditionDetailId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.subscribeToCreateShootDetailObs = function () {
        return this.newShootDetailObs;
    };
    ProductionExtraService.prototype.subscribeToEditShootDetailObs = function () {
        return this.editShootDetailObs;
    };
    ProductionExtraService.prototype.subscribeToDeleteShootDetailObs = function () {
        return this.deleteShootDetailObs;
    };
    ProductionExtraService.prototype.getBaseShootDetailURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/shootDetails/";
        return "/shootdetails/";
    };
    ProductionExtraService.prototype.createShootDetail = function (productionId, shootDetail) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL();
        return this.postAction(shootDetail, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newShootDetailObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.saveShootDetail = function (id, productionId, shootDetail) {
        var _this = this;
        shootDetail.id = id;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + id;
        return this.putAction(shootDetail, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editShootDetailObs.next(shootDetail);
            return shootDetail;
        }));
    };
    ;
    ProductionExtraService.prototype.deleteShootDetail = function (shootDetailId, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + shootDetailId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteShootDetailObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getShootDetails = function (productionId, pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getShootDetail = function (shootDetailId, productionId) {
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseShootDetailURL() + shootDetailId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.subscribeToCreatePayDetailObs = function () {
        return this.newPayDetailObs;
    };
    ProductionExtraService.prototype.subscribeToEditPayDetailObs = function () {
        return this.editPayDetailObs;
    };
    ProductionExtraService.prototype.subscribeToDeletePayDetailObs = function () {
        return this.deletePayDetailObs;
    };
    ProductionExtraService.prototype.getBasePayDetailURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/payDetails/";
        return "/paydetails/";
    };
    ProductionExtraService.prototype.createPayDetail = function (productionId, payDetail) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL();
        return this.postAction(payDetail, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newPayDetailObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.savePayDetail = function (id, productionId, payDetail) {
        var _this = this;
        payDetail.id = id;
        var controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + id;
        return this.putAction(payDetail, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editPayDetailObs.next(payDetail);
            return payDetail;
        }));
    };
    ;
    ProductionExtraService.prototype.deletePayDetail = function (payDetailId, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + payDetailId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deletePayDetailObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getPayDetails = function (productionId, pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getPayDetail = function (payDetailId, productionId) {
        var controllerQuery = this.getBaseURL(productionId) + this.getBasePayDetailURL() + payDetailId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.subscribeToCreateSideObs = function () {
        return this.newSideObs;
    };
    ProductionExtraService.prototype.subscribeToEditSideObs = function () {
        return this.editSideObs;
    };
    ProductionExtraService.prototype.subscribeToDeleteSideObs = function () {
        return this.deleteSideObs;
    };
    ProductionExtraService.prototype.getBaseSideURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/sides/";
        return "/sides/";
    };
    ProductionExtraService.prototype.createSide = function (productionId, side) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL();
        return this.postAction(side, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newSideObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.saveSide = function (id, productionId, side) {
        var _this = this;
        side.id = id;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + id;
        return this.putAction(side, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editSideObs.next(side);
            return side;
        }));
    };
    ;
    ProductionExtraService.prototype.deleteSide = function (sideId, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + sideId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteSideObs.next(res);
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getSides = function (productionId, pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.prototype.getSide = function (sideId, productionId) {
        var controllerQuery = this.getBaseURL(productionId) + this.getBaseSideURL() + sideId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionExtraService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"],] }] }
    ]; };
    ProductionExtraService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], Window])
    ], ProductionExtraService);
    return ProductionExtraService;
}(_utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__["HttpHelpers"]));



/***/ }),

/***/ "./src/app/services/production-role.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/production-role.service.ts ***!
  \*****************************************************/
/*! exports provided: ProductionRoleService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionRoleService", function() { return ProductionRoleService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var _utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utilities/HttpHelpers */ "./src/app/utilities/HttpHelpers.ts");







var ProductionRoleService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ProductionRoleService, _super);
    function ProductionRoleService(http, window) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.window = window;
        _this._deleteRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteRoleObs = _this._deleteRoleObs.asObservable();
        _this._editRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editRoleObs = _this._editRoleObs.asObservable();
        _this._newRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newRoleObs = _this._newRoleObs.asObservable();
        _this.host = window.location.host;
        _this.hostName = window.location.hostname;
        _this.protocol = window.location.protocol;
        _this.port = window.location.port;
        return _this;
    }
    ProductionRoleService.prototype.subscribeToCreateRoleObs = function () {
        return this.newRoleObs;
    };
    ProductionRoleService.prototype.subscribeToEditRoleObs = function () {
        return this.editRoleObs;
    };
    ProductionRoleService.prototype.subscribeToDeleteRoleObs = function () {
        return this.deleteRoleObs;
    };
    ProductionRoleService.prototype.getBaseURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/roles/";
        return "/api/productions/";
    };
    ProductionRoleService.prototype.createRole = function (productionId, role) {
        var _this = this;
        role.productionId = productionId;
        var controllerQuery = this.getBaseURL() + productionId + "/roles/";
        return this.postAction(role, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newRoleObs.next(res);
            return res;
        }));
    };
    ;
    ProductionRoleService.prototype.saveRole = function (id, productionId, role) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + productionId + "/roles/" + id;
        return this.putAction(role, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editRoleObs.next(res);
            return res;
        }));
    };
    ;
    ProductionRoleService.prototype.deleteRole = function (roleId, productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + productionId + "/roles/" + roleId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteRoleObs.next(res);
            return res;
        }));
    };
    ;
    ProductionRoleService.prototype.getRoles = function (orderByIndex) {
        var controllerQuery = this.getBaseURL() + "?orderBy=" + orderByIndex;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionRoleService.prototype.getRole = function (roleId, productionId) {
        // let controllerQuery = this.getBaseURL() + roleId;
        var controllerQuery = this.getBaseURL() + productionId + "/roles/" + roleId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionRoleService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"],] }] }
    ]; };
    ProductionRoleService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], Window])
    ], ProductionRoleService);
    return ProductionRoleService;
}(_utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__["HttpHelpers"]));



/***/ }),

/***/ "./src/app/services/production.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/production.service.ts ***!
  \************************************************/
/*! exports provided: ProductionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionService", function() { return ProductionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _browser_window_provider__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./browser.window.provider */ "./src/app/services/browser.window.provider.ts");
/* harmony import */ var _utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utilities/HttpHelpers */ "./src/app/utilities/HttpHelpers.ts");







var ProductionService = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ProductionService, _super);
    function ProductionService(http, window) {
        var _this = _super.call(this, http) || this;
        _this.http = http;
        _this.window = window;
        _this._deleteProductionObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.deleteProductionObs = _this._deleteProductionObs.asObservable();
        _this._editProductionObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.editProductionObs = _this._editProductionObs.asObservable();
        _this._newProductionObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        _this.newProductionObs = _this._newProductionObs.asObservable();
        _this.host = window.location.host;
        _this.hostName = window.location.hostname;
        _this.protocol = window.location.protocol;
        _this.port = window.location.port;
        return _this;
    }
    ProductionService.prototype.subscribeToCreateProductionObs = function () {
        return this.newProductionObs;
    };
    ProductionService.prototype.subscribeToEditProductionObs = function () {
        return this.editProductionObs;
    };
    ProductionService.prototype.subscribeToDeleteProductionObs = function () {
        return this.deleteProductionObs;
    };
    ProductionService.prototype.getBaseURL = function () {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/productions/";
        return "/api/productions/";
    };
    ProductionService.prototype.createProduction = function (production) {
        var _this = this;
        var controllerQuery = this.getBaseURL();
        return this.postAction(production, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._newProductionObs.next(res);
            return res;
        }));
    };
    ;
    ProductionService.prototype.saveProduction = function (id, production) {
        var _this = this;
        production.id = id;
        var controllerQuery = this.getBaseURL() + id;
        return this.putAction(production, controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._editProductionObs.next(res);
            return res;
        }));
    };
    ;
    ProductionService.prototype.deleteProduction = function (productionId) {
        var _this = this;
        var controllerQuery = this.getBaseURL() + productionId;
        return this.deleteAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            _this._deleteProductionObs.next(res);
            return res;
        }));
    };
    ;
    ProductionService.prototype.getProductions = function (pageIndex, pageSize) {
        if (pageIndex === void 0) { pageIndex = 0; }
        if (pageSize === void 0) { pageSize = 24; }
        var controllerQuery = this.getBaseURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionService.prototype.getProduction = function (productionId) {
        var controllerQuery = this.getBaseURL() + productionId;
        return this.getAction(controllerQuery)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            //do anything with the returned data here...
            return res;
        }));
    };
    ;
    ProductionService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"] },
        { type: Window, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"], args: [_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"],] }] }
    ]; };
    ProductionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_browser_window_provider__WEBPACK_IMPORTED_MODULE_5__["WINDOW"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClient"], Window])
    ], ProductionService);
    return ProductionService;
}(_utilities_HttpHelpers__WEBPACK_IMPORTED_MODULE_6__["HttpHelpers"]));



/***/ }),

/***/ "./src/app/services/state-dependency-resolver.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/state-dependency-resolver.service.ts ***!
  \***************************************************************/
/*! exports provided: StateDependencyResolverService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateDependencyResolverService", function() { return StateDependencyResolverService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StateDependencyResolverService = /** @class */ (function () {
    function StateDependencyResolverService() {
    }
    StateDependencyResolverService.prototype.getEmptyCastingCall = function () {
        var prod;
        return prod;
    };
    StateDependencyResolverService.prototype.getCastingCallFromTransactionOrServer = function (castingCallService, transition) {
        var castingCallId;
        var castingCall;
        var productionId;
        var promise;
        if (transition.params().castingCall) {
            return new Promise(function (resolve) {
                resolve(transition.params().castingCall);
            });
        }
        else {
            if (transition.params().castingCallId && transition.params().castingCallId !== 0) {
                castingCallId = transition.params().castingCallId;
            }
            else if (transition.params().draftId && transition.params().draftId !== 0) {
                castingCallId = transition.params().draftId;
            }
            if (transition.params().productionId && transition.params().productionId !== 0) {
                productionId = transition.params().productionId;
            }
            if (castingCallId !== 0) {
                return new Promise(function (resolve) {
                    resolve(castingCallService.getCastingCall(castingCallId, productionId).toPromise());
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve(null);
                });
            }
        }
    };
    StateDependencyResolverService.prototype.getCastingCallsFromServer = function (castingCallService, transition) {
        var castingCalls;
        var promise;
        return new Promise(function (resolve) {
            resolve(castingCallService.getCastingCalls().toPromise());
        });
    };
    StateDependencyResolverService.prototype.getEmptyProduction = function () {
        var prod;
        return prod;
    };
    StateDependencyResolverService.prototype.getProductionFromTransactionOrServer = function (productionService, transition) {
        var productionId;
        var production;
        var promise;
        if (transition.params().production) {
            return new Promise(function (resolve) {
                resolve(transition.params().production);
            });
        }
        else {
            if (transition.params().productionId && transition.params().productionId !== 0) {
                productionId = transition.params().productionId;
            }
            else if (transition.params().draftId && transition.params().draftId !== 0) {
                productionId = transition.params().draftId;
            }
            if (productionId !== 0) {
                return new Promise(function (resolve) {
                    resolve(productionService.getProduction(productionId).toPromise());
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve(null);
                });
            }
        }
    };
    StateDependencyResolverService.prototype.getProductionsFromServer = function (productionService, transition) {
        var productions;
        var promise;
        return new Promise(function (resolve) {
            resolve(productionService.getProductions().toPromise());
        });
    };
    StateDependencyResolverService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], StateDependencyResolverService);
    return StateDependencyResolverService;
}());



/***/ }),

/***/ "./src/app/services/state-transition-handler.service.ts":
/*!**************************************************************!*\
  !*** ./src/app/services/state-transition-handler.service.ts ***!
  \**************************************************************/
/*! exports provided: StateTransitionHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StateTransitionHandlerService", function() { return StateTransitionHandlerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _metadata_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _production_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _production_role_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var _casting_call_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./casting-call.service */ "./src/app/services/casting-call.service.ts");






var StateTransitionHandlerService = /** @class */ (function () {
    function StateTransitionHandlerService(productionService, roleService, castingCallService, metaDataService) {
        this.productionService = productionService;
        this.roleService = roleService;
        this.castingCallService = castingCallService;
        this.metaDataService = metaDataService;
        //  this.productionService = productionService;
        //  this.castingCallService = castingCallService;
    }
    StateTransitionHandlerService.prototype.getMetadataFromServer = function (transition) {
        var _this = this;
        var metadataId;
        var metadata;
        return new Promise(function (resolve) {
            resolve(_this.metaDataService.getMetadata());
        });
    };
    StateTransitionHandlerService.prototype.getEmptyProduction = function () {
        var prod;
        return prod;
    };
    StateTransitionHandlerService.prototype.getProductionFromTransactionOrServer = function (transition) {
        var _this = this;
        var productionId;
        var production;
        var promise;
        if (transition.params().production) {
            return new Promise(function (resolve) {
                resolve(transition.params().production);
            });
        }
        else {
            if (transition.params().productionId && transition.params().productionId !== 0) {
                productionId = transition.params().productionId;
            }
            else if (transition.params().draftId && transition.params().draftId !== 0) {
                productionId = transition.params().draftId;
            }
            if (productionId !== 0) {
                return new Promise(function (resolve) {
                    resolve(_this.productionService.getProduction(productionId).toPromise());
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve(null);
                });
            }
        }
    };
    StateTransitionHandlerService.prototype.getProductionsFromServer = function (transition) {
        var _this = this;
        var productions;
        var promise;
        return new Promise(function (resolve) {
            resolve(_this.productionService.getProductions().toPromise());
        });
    };
    StateTransitionHandlerService.prototype.getEmptyRole = function () {
        var prod;
        return prod;
    };
    StateTransitionHandlerService.prototype.getRoleFromTransactionOrServer = function (transition) {
        var _this = this;
        var roleId;
        var productionId;
        var role;
        var promise;
        if (transition.params().role) {
            return new Promise(function (resolve) {
                resolve(transition.params().role);
            });
        }
        else {
            if (transition.params().roleId && transition.params().roleId !== 0) {
                roleId = transition.params().roleId;
            }
            if (transition.params().productionId && transition.params().productionId !== 0) {
                productionId = transition.params().productionId;
            }
            if (roleId !== 0 && productionId !== 0) {
                return new Promise(function (resolve) {
                    resolve(_this.roleService.getRole(roleId, productionId).toPromise());
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve(null);
                });
            }
        }
    };
    StateTransitionHandlerService.prototype.getRolesFromServer = function (transition) {
        var _this = this;
        var roles;
        var promise;
        return new Promise(function (resolve) {
            resolve(_this.roleService.getRoles(0).toPromise());
        });
    };
    StateTransitionHandlerService.prototype.getEmptyCastingCall = function () {
        var prod;
        return prod;
    };
    StateTransitionHandlerService.prototype.getCastingCallFromTransactionOrServer = function (transition) {
        var _this = this;
        var castingCallId;
        var castingCall;
        var productionId;
        var promise;
        if (transition.params().castingCall) {
            return new Promise(function (resolve) {
                resolve(transition.params().castingCall);
            });
        }
        else {
            if (transition.params().castingCallId && transition.params().castingCallId !== 0) {
                castingCallId = transition.params().castingCallId;
            }
            else if (transition.params().draftId && transition.params().draftId !== 0) {
                castingCallId = transition.params().draftId;
            }
            if (transition.params().productionId && transition.params().productionId !== 0) {
                productionId = transition.params().productionId;
            }
            if (castingCallId !== 0) {
                return new Promise(function (resolve) {
                    resolve(_this.castingCallService.getCastingCall(castingCallId, productionId).toPromise());
                });
            }
            else {
                return new Promise(function (resolve) {
                    resolve(null);
                });
            }
        }
    };
    StateTransitionHandlerService.prototype.getCastingCallsFromServer = function (transition) {
        var _this = this;
        var castingCalls;
        var promise;
        return new Promise(function (resolve) {
            resolve(_this.castingCallService.getCastingCalls().toPromise());
        });
    };
    StateTransitionHandlerService.prototype.getProductionCastingCallsFromServer = function (transition) {
        var _this = this;
        var castingCalls;
        var productionId;
        if (transition.params().productionId && transition.params().productionId !== 0) {
            productionId = transition.params().productionId;
        }
        if (productionId !== 0) {
            return new Promise(function (resolve) {
                resolve(_this.castingCallService.getProductionCastingCalls(productionId).toPromise());
            });
        }
        else {
            return new Promise(function (resolve) {
                resolve(null);
            });
        }
    };
    StateTransitionHandlerService.ctorParameters = function () { return [
        { type: _production_service__WEBPACK_IMPORTED_MODULE_3__["ProductionService"] },
        { type: _production_role_service__WEBPACK_IMPORTED_MODULE_4__["ProductionRoleService"] },
        { type: _casting_call_service__WEBPACK_IMPORTED_MODULE_5__["CastingCallService"] },
        { type: _metadata_service__WEBPACK_IMPORTED_MODULE_2__["MetadataService"] }
    ]; };
    StateTransitionHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_production_service__WEBPACK_IMPORTED_MODULE_3__["ProductionService"], _production_role_service__WEBPACK_IMPORTED_MODULE_4__["ProductionRoleService"], _casting_call_service__WEBPACK_IMPORTED_MODULE_5__["CastingCallService"],
            _metadata_service__WEBPACK_IMPORTED_MODULE_2__["MetadataService"]])
    ], StateTransitionHandlerService);
    return StateTransitionHandlerService;
}());



/***/ }),

/***/ "./src/app/services/user.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/user.service.ts ***!
  \******************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserService = /** @class */ (function () {
    function UserService() {
    }
    UserService.prototype.checkAuthenticated = function () {
        return !(localStorage.getItem("currentUser") === null);
    };
    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/services/video-blob.service.ts":
/*!************************************************!*\
  !*** ./src/app/services/video-blob.service.ts ***!
  \************************************************/
/*! exports provided: VideoBlobService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoBlobService", function() { return VideoBlobService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _models_blobs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/blobs */ "./src/app/models/blobs.ts");
/* harmony import */ var angular2_uuid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! angular2-uuid */ "./node_modules/angular2-uuid/index.js");
/* harmony import */ var angular2_uuid__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(angular2_uuid__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _file_upload_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./file-upload.service */ "./src/app/services/file-upload.service.ts");







var VideoBlobService = /** @class */ (function () {
    function VideoBlobService(fileUploadService) {
        this.fileUploadService = fileUploadService;
        this._blobs = [];
        this.videoUrls = [];
        this._newBlogRecordingObserverCallback = null;
        this._createdBlobObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._blobsObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]([]);
        this.createdblobObs = this._createdBlobObs.asObservable();
        this.blobsObs = this._blobsObs.asObservable();
        this._blobObsArray = {};
        if (localStorage.getItem('currentUser')) {
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.access_token) {
                this.headers =
                    {
                        Authorization: "Bearer " + currentUser.access_token.replace(" ", "")
                    };
            }
        }
    }
    VideoBlobService.prototype.norifyNewBlogRecordingObserver = function (newBlob) {
        if (this._newBlogRecordingObserverCallback) {
            this._newBlogRecordingObserverCallback(newBlob);
        }
    };
    VideoBlobService.prototype.registerNewBlogRecordingObserver = function (observer) {
        this._newBlogRecordingObserverCallback = observer;
    };
    VideoBlobService.prototype.unRegisterNewBlogRecordingObserver = function () {
        this._newBlogRecordingObserverCallback = null;
    };
    VideoBlobService.prototype.subscribeToCreatedBlobObs = function () {
        return this.createdblobObs;
    };
    VideoBlobService.prototype.subscribeToBlobsObs = function () {
        return this.blobsObs;
    };
    VideoBlobService.prototype.newBlobRecorded = function (newBlob) {
        if (newBlob) {
            this._newBlob = newBlob;
            var newCreatedBlob = new _models_blobs__WEBPACK_IMPORTED_MODULE_4__["StudioTalentBlob"](angular2_uuid__WEBPACK_IMPORTED_MODULE_5__["UUID"].UUID(), newBlob, 1, 1, 1);
            this.norifyNewBlogRecordingObserver(newCreatedBlob);
            this._createdBlobObs.next(newCreatedBlob);
        }
    };
    VideoBlobService.prototype.addNewCreatedBlob = function (newBlob) {
        //let newCreatedBlob: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
        // let newCreatedBlob2: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
        //let newCreatedBlob3: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
        if (!this._blobs.some(function (s) { return s.id == newBlob.id; })) {
            var blobsCol = [newBlob];
            this._blobs.push(newBlob);
            // this._blobs = this._blobs;
        }
        else {
            this._blobs[this._blobs.findIndex(function (s) { return s.id == newBlob.id; })] = newBlob;
        }
        this._blobsObs.next(this._blobs);
        // this.fileUploadService.uploadFiles(blobsCol);
        //this._blobsObs.next(this._blobs);
    };
    VideoBlobService.prototype.SubscribeToBlobObs = function (blobId) {
        return this._getOrCreateBlobObsInArray(blobId).asObservable();
    };
    VideoBlobService.prototype.makeString = function (value) {
        return value.toString();
    };
    ;
    VideoBlobService.prototype._getOrCreateBlobObsInArray = function (blobId) {
        var blobId$ = this.makeString(blobId);
        if (!(blobId$ in this._blobObsArray)) {
            // this._blobObsArray[blobId$] = new BehaviorSubject<CreatedBlob>(new Blob());
        }
        return this._blobObsArray[blobId$];
    };
    VideoBlobService.prototype._removeBlobObsFromArray = function (blobId) {
        var blobId$ = this.makeString(blobId);
        if (blobId$ in this._blobObsArray) {
            delete this._blobObsArray[blobId$];
        }
    };
    VideoBlobService.prototype._isBlobIsBlobArray = function (blobId) {
        return this._blobs.some(function (s) { return s.id == blobId; });
    };
    VideoBlobService.prototype._getBlobs = function () {
        if (this._blobs !== null && this._blobs.length > 0) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this._blobs)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
                return res;
            }));
        }
        else {
        }
    };
    VideoBlobService.prototype._getBlob = function (blobId) {
        if (this._isBlobIsBlobArray(blobId)) {
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this._blobs.find(function (s) { return s.id == blobId; }))
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
                return res;
            }));
        }
        else {
        }
    };
    ;
    VideoBlobService.prototype.getBlobs = function () {
        var _this = this;
        return this._getBlobs()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            _this._blobsObs.next(_this._blobs);
            return res;
        }));
    };
    ;
    VideoBlobService.prototype.getBlob = function (blobId) {
        var _this = this;
        return this._getBlob(blobId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (res) {
            _this._getOrCreateBlobObsInArray(blobId).next(res);
            return res;
        }));
    };
    ;
    VideoBlobService.prototype.addBlob = function (blob) {
        //do anything with the returned data here...
        if (!this._isBlobIsBlobArray(blob.id)) {
            this._blobs.push(blob);
            this._blobsObs.next(this._blobs);
        }
        ;
        //
        +this._getOrCreateBlobObsInArray(blob.id).next(blob);
        this._blobsObs.next(this._blobs);
        return this._blobsObs;
    };
    ;
    VideoBlobService.prototype.saveBlob = function (blob) {
        //do anything with the returned data here...
        if (!this._isBlobIsBlobArray(blob.id)) {
            this._blobs.push(blob);
        }
        else {
            this._blobs[this._blobs.findIndex(function (s) { return s.id == blob.id; })] = blob;
        }
        this._blobsObs.next(this._blobs);
        this._getOrCreateBlobObsInArray(blob.id).next(blob);
        return blob;
    };
    ;
    VideoBlobService.prototype.deleteBlob = function (blobId) {
        //do anything with the returned data here...
        if (this._blobs.some(function (s) { return s.id == blobId; })) {
            this._blobs.splice(this._blobs.findIndex(function (s) { return s.id == blobId; }), 1);
            //this._getOrCreateBlobObsInArray(blobId).complete();
            /// this._removeBlobObsFromArray(blobId);
            this._blobsObs.next(this._blobs);
        }
        return true;
    };
    ;
    VideoBlobService.prototype._handleSuccessError = function (message, dispatchAlert) {
    };
    VideoBlobService.ctorParameters = function () { return [
        { type: _file_upload_service__WEBPACK_IMPORTED_MODULE_6__["FileUploadService"] }
    ]; };
    VideoBlobService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_file_upload_service__WEBPACK_IMPORTED_MODULE_6__["FileUploadService"]])
    ], VideoBlobService);
    return VideoBlobService;
}());



/***/ }),

/***/ "./src/app/testme/testme.component.scss":
/*!**********************************************!*\
  !*** ./src/app/testme/testme.component.scss ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Rlc3RtZS90ZXN0bWUuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/testme/testme.component.ts":
/*!********************************************!*\
  !*** ./src/app/testme/testme.component.ts ***!
  \********************************************/
/*! exports provided: TestmeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestmeComponent", function() { return TestmeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TestmeComponent = /** @class */ (function () {
    function TestmeComponent() {
    }
    TestmeComponent.prototype.ngOnInit = function () {
    };
    TestmeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-testme',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./testme.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/testme/testme.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./testme.component.scss */ "./src/app/testme/testme.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TestmeComponent);
    return TestmeComponent;
}());



/***/ }),

/***/ "./src/app/ui-router-states.ts":
/*!*************************************!*\
  !*** ./src/app/ui-router-states.ts ***!
  \*************************************/
/*! exports provided: rootState, pulloutContainerState, noPulloutContainerState, mainScrollContainerState, mainNonScrollContainerState, mainPulloutNonScrollContainerState, mainPulloutScrollContainerState, mainSidePulloutNonScrollContainerState, mainSidePulloutScrollContainerState, returnTo, homeState, loginState, registerState, logoutState, talentFutureState, recordFutureState, newProductionFutureState, productionCastingCallsFutureState, newCastingCallFutureState, castingCallsFutureState, prefsFutureState, mymessagesFutureState, APP_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rootState", function() { return rootState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pulloutContainerState", function() { return pulloutContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "noPulloutContainerState", function() { return noPulloutContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainScrollContainerState", function() { return mainScrollContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainNonScrollContainerState", function() { return mainNonScrollContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainPulloutNonScrollContainerState", function() { return mainPulloutNonScrollContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainPulloutScrollContainerState", function() { return mainPulloutScrollContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainSidePulloutNonScrollContainerState", function() { return mainSidePulloutNonScrollContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mainSidePulloutScrollContainerState", function() { return mainSidePulloutScrollContainerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "returnTo", function() { return returnTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "homeState", function() { return homeState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginState", function() { return loginState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registerState", function() { return registerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logoutState", function() { return logoutState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "talentFutureState", function() { return talentFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recordFutureState", function() { return recordFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newProductionFutureState", function() { return newProductionFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionCastingCallsFutureState", function() { return productionCastingCallsFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newCastingCallFutureState", function() { return newCastingCallFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "castingCallsFutureState", function() { return castingCallsFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "prefsFutureState", function() { return prefsFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mymessagesFutureState", function() { return mymessagesFutureState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "APP_STATES", function() { return APP_STATES; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _components_app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/app.component */ "./src/app/components/app.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_logout_logout_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/logout/logout.component */ "./src/app/components/logout/logout.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/components/register/register.component.ts");
/* harmony import */ var _components_containers_main_side_pullout_scroll_main_side_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component */ "./src/app/components/containers/main-side-pullout-scroll/main-side-pullout-scroll.component.ts");
/* harmony import */ var _components_containers_main_side_pullout_non_scroll_main_side_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component */ "./src/app/components/containers/main-side-pullout-non-scroll/main-side-pullout-non-scroll.component.ts");
/* harmony import */ var _components_containers_main_pullout_scroll_main_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/containers/main-pullout-scroll/main-pullout-scroll.component */ "./src/app/components/containers/main-pullout-scroll/main-pullout-scroll.component.ts");
/* harmony import */ var _components_containers_main_pullout_non_scroll_main_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component */ "./src/app/components/containers/main-pullout-non-scroll/main-pullout-non-scroll.component.ts");
/* harmony import */ var _components_containers_pullout_pullout_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/containers/pullout/pullout.component */ "./src/app/components/containers/pullout/pullout.component.ts");
/* harmony import */ var _components_containers_no_pullout_no_pullout_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/containers/no-pullout/no-pullout.component */ "./src/app/components/containers/no-pullout/no-pullout.component.ts");
/* harmony import */ var _components_containers_main_scroll_main_scroll_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/containers/main-scroll/main-scroll.component */ "./src/app/components/containers/main-scroll/main-scroll.component.ts");
/* harmony import */ var _components_containers_main_non_scroll_main_non_scroll_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/containers/main-non-scroll/main-non-scroll.component */ "./src/app/components/containers/main-non-scroll/main-non-scroll.component.ts");















/** States */
var rootState = {
    name: 'root',
    redirectTo: 'home',
    url: '',
    component: _components_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
};
var pulloutContainerState = {
    parent: 'root',
    name: 'pulloutContainer',
    //redirectTo: 'home',
    url: '',
    //views: {
    //     "main@root": { component: MainContainerComponent }
    //   },
    component: _components_containers_pullout_pullout_component__WEBPACK_IMPORTED_MODULE_11__["PulloutComponent"]
};
var noPulloutContainerState = {
    parent: 'root',
    name: 'noPulloutContainer',
    //redirectTo: 'home',
    url: '',
    //views: {
    //     "main@root": { component: MainContainerComponent }
    //   },
    component: _components_containers_no_pullout_no_pullout_component__WEBPACK_IMPORTED_MODULE_12__["NoPulloutComponent"]
};
var mainScrollContainerState = {
    parent: 'noPulloutContainer',
    name: 'mainScrollContainer',
    //redirectTo: 'home',
    url: '',
    views: {
        "main@noPulloutContainer": { component: _components_containers_main_scroll_main_scroll_component__WEBPACK_IMPORTED_MODULE_13__["MainScrollComponent"] }
    },
};
var mainNonScrollContainerState = {
    parent: 'noPulloutContainer',
    name: 'mainNonScrollContainer',
    //redirectTo: 'home',
    url: '',
    views: {
        "main@noPulloutContainer": { component: _components_containers_main_non_scroll_main_non_scroll_component__WEBPACK_IMPORTED_MODULE_14__["MainNonScrollComponent"] }
    },
};
var mainPulloutNonScrollContainerState = {
    parent: 'pulloutContainer',
    name: 'mainPulloutNonScrollContainer',
    //redirectTo: 'home',
    url: '',
    views: {
        "main@pulloutContainer": { component: _components_containers_main_pullout_non_scroll_main_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_10__["MainPulloutNonScrollComponent"] }
    },
};
var mainPulloutScrollContainerState = {
    parent: 'pulloutContainer',
    name: 'mainPulloutScrollContainer',
    views: {
        "main@pulloutContainer": { component: _components_containers_main_pullout_scroll_main_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_9__["MainPulloutScrollComponent"] }
    },
    //component: MainPulloutScrollComponent,
    //redirectTo: 'home',
    url: '',
    // component: SidePulloutContainerComponent
    data: { requiresAuth: true }
};
var mainSidePulloutNonScrollContainerState = {
    parent: 'pulloutContainer',
    name: 'mainSidePulloutNonScrollContainer',
    //redirectTo: 'home',
    url: '',
    views: {
        "main@pulloutContainer": { component: _components_containers_main_side_pullout_non_scroll_main_side_pullout_non_scroll_component__WEBPACK_IMPORTED_MODULE_8__["MainSidePulloutNonScrollComponent"] }
    },
};
var mainSidePulloutScrollContainerState = {
    parent: 'pulloutContainer',
    name: 'mainSidePulloutScrollContainer',
    views: {
        "main@pulloutContainer": { component: _components_containers_main_side_pullout_scroll_main_side_pullout_scroll_component__WEBPACK_IMPORTED_MODULE_7__["MainSidePulloutScrollComponent"] }
    },
    //component: MainSidePulloutScrollComponent,
    //redirectTo: 'home',
    url: '',
};
function returnTo($transition$) {
    if ($transition$.redirectedFrom() != null) {
        // The user was redirected to the login state (e.g., via the requiresAuth hook when trying to activate contacts)
        // Return to the original attempted target state (e.g., contacts)
        return $transition$.redirectedFrom().targetState();
    }
    var $state = $transition$.router.stateService;
    // The user was not redirected to the login state; they directly activated the login state somehow.
    // Return them to the state they came from.
    if ($transition$.from().name !== '') {
        return $state.target($transition$.from(), $transition$.params('from'));
    }
    // If the fromState's name is empty, then this was the initial transition. Just return them to the home state
    return $state.target('home');
}
var homeState = {
    name: "home",
    url: '/home',
    parent: "mainPulloutScrollContainer",
    views: {
        "main@mainPulloutScrollContainer": { component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] }
    },
};
/**
 * This is the login state.  It is activated when the user navigates to /login, or if a unauthenticated
 * user attempts to access a protected state (or substate) which requires authentication. (see routerhooks/requiresAuth.js)
 *
 * It shows a fake login dialog and prompts the user to authenticate.  Once the user authenticates, it then
 * reactivates the state that the user originally came from.
 */
var loginState = {
    name: 'login',
    url: '/login',
    parent: "mainScrollContainer",
    views: {
        "main@mainScrollContainer": { component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"] }
    },
    resolve: [
        { token: 'returnTo', deps: [_uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: returnTo },
    ]
};
var registerState = {
    name: 'register',
    url: '/register',
    parent: "mainScrollContainer",
    views: {
        "main@mainScrollContainer": { component: _components_register_register_component__WEBPACK_IMPORTED_MODULE_6__["RegisterComponent"] }
    },
    resolve: [
        { token: 'returnTo', deps: [_uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: returnTo },
    ]
};
var logoutState = {
    name: "logout",
    url: '/logout',
    component: _components_logout_logout_component__WEBPACK_IMPORTED_MODULE_4__["LogoutComponent"]
};
// This future state is a placeholder for all the lazy loaded feature module states
// The feature modules are not loaded until their link is activated
var talentFutureState = {
    name: 'talent.**',
    url: '/talent',
    loadChildren: './feature-modules/talent/talent.module#TalentModule'
};
var recordFutureState = {
    name: 'record.**',
    url: '/record',
    loadChildren: './feature-modules/recording/recordingManager.module#RecordingManagerModule'
};
var newProductionFutureState = {
    name: 'productions.**',
    url: '/productions',
    loadChildren: './feature-modules/production/production.module#NewProductionModule'
};
var productionCastingCallsFutureState = {
    name: 'productioncastingCalls.**',
    url: '/productions/:productionId/casting-calls',
    loadChildren: './feature-modules/casting-call/casting-call.module#CastingCallModule'
};
var newCastingCallFutureState = {
    name: 'newcastingCall.**',
    url: '/productions/:productionId/casting-calls/new',
    loadChildren: './feature-modules/casting-call/casting-call.module#CastingCallModule'
};
var castingCallsFutureState = {
    name: 'castingCalls.**',
    url: '/casting-calls',
    loadChildren: './feature-modules/casting-call/casting-call.module#CastingCallModule'
};
// This future state is a placeholder for the lazy loaded Prefs states
var prefsFutureState = {
    name: 'prefs.**',
    url: '/prefs',
    loadChildren: './prefs/prefs.module#PrefsModule'
};
// This future state is a placeholder for the lazy loaded My Messages feature module
var mymessagesFutureState = {
    name: 'mymessages.**',
    url: '/mymessages',
    loadChildren: './mymessages/mymessages.module#MymessagesModule'
};
var APP_STATES = [
    rootState,
    pulloutContainerState,
    noPulloutContainerState,
    mainScrollContainerState,
    mainPulloutScrollContainerState,
    mainSidePulloutScrollContainerState,
    mainNonScrollContainerState,
    mainPulloutNonScrollContainerState,
    mainSidePulloutNonScrollContainerState,
    homeState,
    logoutState,
    registerState,
    loginState,
    recordFutureState,
    newProductionFutureState,
    castingCallsFutureState,
    productionCastingCallsFutureState,
    newCastingCallFutureState
];


/***/ }),

/***/ "./src/app/ui-router/requiresAuthenticationHook.ts":
/*!*********************************************************!*\
  !*** ./src/app/ui-router/requiresAuthenticationHook.ts ***!
  \*********************************************************/
/*! exports provided: requiresAuthenticationHook */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "requiresAuthenticationHook", function() { return requiresAuthenticationHook; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/user.service */ "./src/app/services/user.service.ts");


/**
 * This file contains a Transition Hook which protects a
 * route that requires authentication.
 *
 * This hook redirects to /login when both:
 * - The user is not authenticated
 * - The user is navigating to a state that requires authentication
 */
function requiresAuthenticationHook(transitionService) {
    // Matches if the destination state's data property has a truthy 'requiresAuth' property
    var requiresAuthCriteria = {
        to: function (state) { return state.data && state.data.requiresAuth; }
    };
    // Function that returns a redirect for the current transition to the login state
    // if the user is not currently authenticated (according to the AuthService)
    var redirectToLogin = function (transition) {
        var authenticationService = transition.injector().get(_services_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]);
        var $state = transition.router.stateService;
        if (!authenticationService.checkAuthenticated()) {
            return $state.target('login', undefined, { location: false });
        }
    };
    // Register the "requires auth" hook with the TransitionsService
    transitionService.onBefore(requiresAuthCriteria, redirectToLogin, { priority: 10 });
}


/***/ }),

/***/ "./src/app/ui-router/router.config.ts":
/*!********************************************!*\
  !*** ./src/app/ui-router/router.config.ts ***!
  \********************************************/
/*! exports provided: routerConfigFn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routerConfigFn", function() { return routerConfigFn; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _requiresAuthenticationHook__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./requiresAuthenticationHook */ "./src/app/ui-router/requiresAuthenticationHook.ts");


/** UIRouter Config  */
function routerConfigFn(router, injector) {
    var transitionService = router.transitionService;
    Object(_requiresAuthenticationHook__WEBPACK_IMPORTED_MODULE_1__["requiresAuthenticationHook"])(transitionService);
    //googleAnalyticsHook(transitionService);
    //StateTree.create(router, document.getElementById('statetree'), { width: 200, height: 100 });
    //var pluginInstance = router.plugin(Visualizer);
    //router.trace.enable(Category.TRANSITION);
    //router.plugin(Visualizer);
}


/***/ }),

/***/ "./src/app/utilities/HttpHelpers.ts":
/*!******************************************!*\
  !*** ./src/app/utilities/HttpHelpers.ts ***!
  \******************************************/
/*! exports provided: HttpHelpers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpHelpers", function() { return HttpHelpers; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");




var HttpHelpers = /** @class */ (function () {
    function HttpHelpers(_http) {
        this._http = _http;
    }
    Object.defineProperty(HttpHelpers.prototype, "haserror", {
        get: function () {
            return this.errormsg != null;
        },
        enumerable: true,
        configurable: true
    });
    HttpHelpers.prototype.getAction = function (path) {
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        return this._http.get(path, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["publishLast"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["refCount"])());
    };
    HttpHelpers.prototype.postAction = function (param, path) {
        this.errormsg = null;
        var body = JSON.stringify(param);
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        return this._http.post(path, body, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["publishLast"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["refCount"])());
    };
    HttpHelpers.prototype.putAction = function (param, path) {
        this.errormsg = null;
        var body = JSON.stringify(param);
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        var t = 0;
        return this._http.put(path, body, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["publishLast"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["refCount"])());
    };
    HttpHelpers.prototype.deleteAction = function (path) {
        this.errormsg = null;
        var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        return this._http.delete(path, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["publishLast"])(), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["refCount"])());
    };
    HttpHelpers.prototype._handleError = function (error) {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["throwError"])('Something bad happened; please try again later.');
        //return Observable.throw(error.statusText + " at " + error.url || 'Server error');
    };
    return HttpHelpers;
}());



/***/ }),

/***/ "./src/app/utilities/blobFileHelper.ts":
/*!*********************************************!*\
  !*** ./src/app/utilities/blobFileHelper.ts ***!
  \*********************************************/
/*! exports provided: blobFileHelper */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "blobFileHelper", function() { return blobFileHelper; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var blobFileHelper = /** @class */ (function () {
    function blobFileHelper() {
    }
    blobFileHelper.blobToFile = function (theBlob, fileName) {
        var b = theBlob;
        //A Blob() is almost a File() - it's just missing the two properties below which we will add
        b.lastModifiedDate = new Date();
        b.name = fileName;
        //Cast to a File() type
        return theBlob;
    };
    return blobFileHelper;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"], { preserveWhitespaces: true })
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! F:\work\casting\Casting\Angular\talent-app\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map