﻿using Casting.BLL.Productions;
using Casting.BLL.Productions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Productions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.Production;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.DAL;

namespace Casting.Web.Controllers.Directors
{ 
    [RoutePrefix("api/productions")]
    [Authorize]
    public class ProductionsController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingCallService _castingCallService;
        private IProductionRoleService _productionRoleService;
        private IMetadataService _metadataService;


        private IAuditionDetailService _auditionDetailService;
        private IPayDetailService _payDetailService;
        private IShootDetailService _shootDetailService;
        private ISideService _sideService;

        private readonly UserManager<ApplicationUser> _userManager;

        public ProductionsController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, IProductionService productionService, IProductionRoleService productionRoleService, ICastingCallService castingCallService, ICastingContext castingContext
            , IAuditionDetailService auditionDetailService
            , IPayDetailService payDetailService
            , IShootDetailService shootDetailService
            , ISideService sideService) : base(castingContext)
        {
            _productionService = productionService;
            _auditionDetailService = auditionDetailService;
            _payDetailService = payDetailService;
            _shootDetailService = shootDetailService;
            _sideService = sideService;
            _castingCallService = castingCallService;
            _productionRoleService = productionRoleService;
            _metadataService = metadataService;
            _userManager = userManager;
        }
      
        // GET: api/Productions
        [ResponseType(typeof(IEnumerable<Production>))]
        public async Task<IHttpActionResult> GetProductions(int pageIndex = 0, int pageSize = 24)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
             var productions =
                await _productionService.GetProductionsByDirectorIdAsync(user.BaseUserId, pageIndex, pageSize);
               return Ok(productions);
        }

        // GET: api/Productions/5
        [ResponseType(typeof(Production))]
        public async Task<IHttpActionResult> Get(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            var production =
                await _productionService.GetProductionByIdWithRolesCastingCallsSessionsAsync(id);

            return Ok(production);
        }

        // GET: api/productions/[id}/Roles
       /* [Route("{id}/roles")]
        [ResponseType(typeof(IEnumerable<Role>))]
        public async Task<IHttpActionResult> GetProductionRoles(int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
                  var productionRoles =
                await _productionRoleService.GetRolesByProductionIdAsync(Id);

            return Ok(productionRoles);
        }
    */

        // GET: api/productions/{id}/castingcalls
        [Route("{id}/castingcalls")]
        [ResponseType(typeof(IEnumerable<CastingCall>))]
        public async Task<IHttpActionResult> GetProductionCastingCalls(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/

            var castingCalls =
                await _castingCallService.GetCastingCallsByProductionIdWithRolesPersonasAsync(id);

            return Ok(castingCalls);
        }

        // POST: api/Productions
        [ResponseType(typeof(Production))]
        [HttpPost]
        public async Task<IHttpActionResult> Post(NewProductionBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
           {

               return BadRequest("User is not an Director");

           }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            var name = "Untitled Project";
            if (!String.IsNullOrEmpty(value.Name))
            {
                name = value.Name;
            }
             var production = new Production()
            {
                ProductionTypeId = value.ProductionTypeId,
                DirectorId = user.BaseUserId,
                ProductionCompany = value.ProductionCompany,
                Name = name,
                Description = value.Description,
                ProductionInfo = value.ProductionInfo,
                ExecutiveProducer = value.ExecutiveProducer,
                CoExecutiveProducer = value.CoExecutiveProducer,
                Producer = value.Producer,
                CoProducer = value.CoProducer,
                CastingDirector = value.CastingDirector,
                CastingAssistant = value.CastingAssistant,
                CastingAssociate = value.CastingAssociate,
                AssistantCastingAssociate = value.AssistantCastingAssociate,
                ArtisticDirector = value.ArtisticDirector,
                MusicDirector = value.MusicDirector,
                Choreographer = value.Choreographer,
                TVNetwork = value.TVNetwork,
                Venue = value.Venue,
                CompensationType = value.CompensationType,
                CompensationContractDetails = value.CompensationContractDetails,
                ProductionUnionStatus = value.ProductionUnionStatus

            };
            var ret = await _productionService.AddNewProductionAsync(production);
            return Created("api/productions/" + production.Id, production);
        }

        //removed the below. now quickly creating a default production instead
      /*  public async Task<IHttpActionResult> Post(NewProductionBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }
            var production = new Production()
            {
                DirectorId = user.BaseUserId,
                ProductionCompany = value.ProductionCompany,
                Name = value.Name,
                Description = value.Description,
                ProductionInfo = value.ProductionInfo,
                ExecutiveProducer = value.ExecutiveProducer,
                CoExecutiveProducer = value.CoExecutiveProducer,
                Producer = value.Producer,
                CoProducer = value.CoProducer,
                CastingDirector = value.CastingDirector,
                CastingAssistant = value.CastingAssistant,
                CastingAssociate = value.CastingAssociate,
                AssistantCastingAssociate = value.AssistantCastingAssociate,
                ArtisticDirector = value.ArtisticDirector,
                MusicDirector = value.MusicDirector,
                Choreographer = value.Choreographer,
                TVNetwork = value.TVNetwork,
                Venue = value.Venue,
                CompensationType = value.CompensationType,
                CompensationContractDetails = value.CompensationContractDetails,
                ProductionUnionStatus = value.ProductionUnionStatus

            };
            foreach (var newRole in value.Roles)
            {
                var role = new Role()
                {
                    Description = newRole.Description,
                    Name = newRole.Name,
                    MaxAge = newRole.MaxAge,
                    MinAge = newRole.MinAge,
                    RequiresNudity = newRole.RequiresNudity,
                    RequiresHeadshot = newRole.RequiresHeadshot,
                    HeadshotMaxCount = newRole.HeadshotMaxCount,
                    RequiresVideo = newRole.RequiresVideo,
                    VideoMaxCount = newRole.VideoMaxCount,
                    RequiresAudio = newRole.RequiresAudio,
                    AudioMaxCount = newRole.AudioMaxCount,
                    RequiresCoverLetter = newRole.RequiresCoverLetter,
                    Transgender = newRole.Transgender,
                    RoleTypeId = newRole.RoleTypeId,
                    RoleGenderTypeId = newRole.RoleGenderTypeId
                };
                foreach (var etnicityId in newRole.Ethnicities)
                {
                    var ethnicity = await _metadataService.GetEthnicityByIdAsync(etnicityId);
                    if (ethnicity != null)
                    {
                        role.Ethnicities.Add(ethnicity);
                    }
                }
                foreach (var stereotypeId in newRole.StereoTypes)
                {
                    var stereotype = await _metadataService.GetStereoTypeByIdAsync(stereotypeId);
                    if (stereotype != null)
                    {
                        role.StereoTypes.Add(stereotype);
                    }
                }
                foreach (var etnicitystereotypeId in newRole.EthnicStereoTypes)
                {
                    var ethnicitystereotype = await _metadataService.GetEthnicStereoTypeByIdAsync(etnicitystereotypeId);
                    if (ethnicitystereotype != null)
                    {
                        role.EthnicStereoTypes.Add(ethnicitystereotype);
                    }
                }
                production.Roles.Add(role);

            }
            var ret = await _productionService.AddNewProductionAsync(production);
            return CreatedAtRoute("DefaultApi", new { id = production.Id }, production);
        } */

        // PUT: api/Productions/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> Put(int id, UpdateProductionBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/

            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            var production = await _productionService.GetProductionByIdAsync(id);
            if (production == null)
            {
                return NotFound();
            }
            if (production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }

            production.IsSaved = true;
            production.ProductionTypeId = value.ProductionTypeId;
            production.ProductionCompany = value.ProductionCompany;
            production.Name = value.Name;
            production.Description = value.Description;
            production.ProductionInfo = value.ProductionInfo;
            production.ExecutiveProducer = value.ExecutiveProducer;
            production.CoExecutiveProducer = value.CoExecutiveProducer;
            production.Producer = value.Producer;
            production.CoProducer = value.CoProducer;
            production.CastingDirector = value.CastingDirector;
            production.CastingAssistant = value.CastingAssistant;
            production.CastingAssociate = value.CastingAssociate;
            production.AssistantCastingAssociate = value.AssistantCastingAssociate;
            production.ArtisticDirector = value.ArtisticDirector;
            production.MusicDirector = value.MusicDirector;
            production.Choreographer = value.Choreographer;
            production.TVNetwork = value.TVNetwork;
            production.Venue = value.Venue;
            production.CompensationType = value.CompensationType;
            production.CompensationContractDetails = value.CompensationContractDetails;
            production.ProductionUnionStatus = value.ProductionUnionStatus;


            try
            {
                await _productionService.UpdateProductionAsync(production);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/Productions/5
        [ResponseType(typeof(Production))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/


            var production = await _productionService.GetProductionByIdAsync(id);
            if (production == null)
            {
                return NotFound();
            }
            if (production.DirectorId != user.BaseUserId)
            {
                return Unauthorized();
            }

            await _productionService.DeleteProduction(production);

            return Ok(production);
        }

    
    }
}
