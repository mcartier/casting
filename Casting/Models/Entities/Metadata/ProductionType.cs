﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;
using System.Runtime.Serialization;

namespace Casting.Model.Entities.Metadata
{
    public class ProductionType : Logger
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ProductionTypeCategoryId { get; set; }
        [IgnoreDataMember]
        public ProductionTypeCategory ProductionTypeCategory { get; set; }
       // public ICollection<ProductionProductionTypeReference> ProductionReferences { get; set; }
        public ICollection<Production> Productions { get; set; }
    }
}
