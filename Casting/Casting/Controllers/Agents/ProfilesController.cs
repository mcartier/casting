﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;

using Microsoft.AspNet.Identity;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.BLL.Profiles.Interfaces;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Enums;
using Casting.Web.Helpers;

using Casting.Model.Entities.Attributes;
using Casting.Model.Entities.AttributeReferences;
using Casting.BLL.AttributeReferences.Interfaces;
using Casting.Web.Models;

namespace Casting.Web.Controllers.Agents
{
    public class ProfilesController : BaseApiController
    {
        private IProfileService _profileService;
        private IAttributeReferenceService _attributeService;
           private readonly UserManager<ApplicationUser> _userManager;
        public ProfilesController(UserManager<ApplicationUser> userManager, IProfileService profileService, ICastingContext castingContext,
            IAttributeReferenceService attributeService)
          : base(castingContext)
        {
            _profileService = profileService;// castingContext;
            _attributeService = attributeService;
                _userManager = userManager;
        }


        // GET: api/Profiles
        [ResponseType(typeof(IEnumerable<Profile>))]
        public async Task<IHttpActionResult> GetProfiles()
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profiles = await _profileService.GetProfileByIdAsync(user.BaseUserId);

            return Ok(profiles);
        }

        // GET: api/Profiles/5
        [ResponseType(typeof(Profile))]
        public async Task<IHttpActionResult> GetProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profile = await _profileService.GetProfileByIdAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

        // GET: api/Profiles/5/2
        [ResponseType(typeof(Profile))]
        public async Task<IHttpActionResult> GetTypedProfile(int id, ProfileTypeEnum type)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }

            Profile profile;
            switch (type)
            {

                case ProfileTypeEnum.Actor:
                    profile = await _profileService.GetCompleteActorProfileByIdAsync(id);
                    break;
                case ProfileTypeEnum.VoiceActor:
                    profile = await _profileService.GetCompleteVoiceActorProfileByIdAsync(id);
                    break;
                default:
                    profile = await _profileService.GetProfileByIdAsync(id);
                    break;
            }

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }
        // GET: api/ActorProfiles/5
        [ResponseType(typeof(IEnumerable<Union>))]
        [Route("{id}/unions")]
        public async Task<IHttpActionResult> GetActorProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profile = await _profileService.GetCompleteActorProfileByIdAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }



        // DELETE: api/Profiles/5
        [ResponseType(typeof(Profile))]
        public async Task<IHttpActionResult> DeleteProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentProfiles = await 
            var profile = await _profileService.GetProfileByIdAsync(id);
            if (profile == null)
            {
                return NotFound();
            }


            await _profileService.DeleteProfile(profile);

            return Ok(profile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
            base.Dispose(disposing);
        }

        #region profile attribute references

        // GET: api/profiles/[id}/unions
        [Route("{id}/unions")]
        [ResponseType(typeof(IEnumerable<ProfileUnionReference>))]
        public async Task<IHttpActionResult> GetProfileUnions(int Id)
        {

            var profileUnionss =
                await _attributeService.GetProfileUnionReferencesForParentAsync(Id, true);

            return Ok(profileUnionss);
        }
        // GET: api/profiles/[id}/ethnicities
        [Route("{id}/ethnicities")]
        [ResponseType(typeof(IEnumerable<ProfileEthnicityReference>))]
        public async Task<IHttpActionResult> GetProfileEthnicities(int Id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
             {

                 return BadRequest("User is not an Director");

             }*/
            var profileEthnicitiess =
                await _attributeService.GetProfileEthnicityReferencesForParentAsync(Id, true);

            return Ok(profileEthnicitiess);
        }

        // GET: api/profiles/[id}/stereoTypes
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(IEnumerable<ProfileStereoTypeReference>))]
        public async Task<IHttpActionResult> GetProfileStereoTypes(int Id)
        {

            var profileStereoTypess =
                await _attributeService.GetProfileStereoTypeReferencesForParentAsync(Id, true);

            return Ok(profileStereoTypess);
        }
        // GET: api/profiles/[id}/ethnicStereoTypes
        [Route("{id}/ethnicstereotypes")]
        [ResponseType(typeof(IEnumerable<ProfileEthnicStereoTypeReference>))]
        public async Task<IHttpActionResult> GetProfileEthnicStereoTypes(int Id)
        {

            var profileEthnicStereoTypess =
                await _attributeService.GetProfileEthnicStereoTypeReferencesForParentAsync(Id, true);

            return Ok(profileEthnicStereoTypess);
        }

        // GET: api/profiles/[id}/languages
        [Route("{id}/languages")]
        [ResponseType(typeof(IEnumerable<ProfileLanguageReference>))]
        public async Task<IHttpActionResult> GetProfileLanguages(int Id)
        {

            var profileLanguagess =
                await _attributeService.GetProfileLanguageReferencesForParentAsync(Id, true);

            return Ok(profileLanguagess);
        }

        // GET: api/profiles/[id}/accents
        [Route("{id}/accents")]
        [ResponseType(typeof(IEnumerable<ProfileAccentReference>))]
        public async Task<IHttpActionResult> GetProfileAccents(int Id)
        {

            var profileAccentss =
                await _attributeService.GetProfileAccentReferencesForParentAsync(Id, true);

            return Ok(profileAccentss);
        }


        // POST: api/profiles/[id}/unions
        [Route("{id}/unions")]
        [ResponseType(typeof(ProfileUnionReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProfileUnion(int Id, ProfileAttributeReferenceBindingModel union)
        {
            var attribureReference = new ProfileUnionReference() { ProfileId = Id, UnionId = union.AttributeId };
            var ret =
                await _attributeService.AddNewProfileUnionAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }

        // POST: api/profiles/[id}/stereoTypes
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(ProfileStereoTypeReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProfileStereoType(int Id, ProfileAttributeReferenceBindingModel stereoType)
        {
            var attribureReference = new ProfileStereoTypeReference() { ProfileId = Id, StereoTypeId = stereoType.AttributeId };
            var ret =
                await _attributeService.AddNewProfileStereoTypeAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }


        // POST: api/profiles/[id}/ethnicEthnicStereoTypes
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(ProfileEthnicStereoTypeReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProfileEthnicStereoType(int Id, ProfileAttributeReferenceBindingModel ethnicEthnicStereoType)
        {
            var attribureReference = new ProfileEthnicStereoTypeReference() { ProfileId = Id, EthnicStereoTypeId = ethnicEthnicStereoType.AttributeId };
            var ret =
                await _attributeService.AddNewProfileEthnicStereoTypeAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }


        // POST: api/profiles/[id}/ethnicitys
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(ProfileEthnicityReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProfileEthnicity(int Id, ProfileAttributeReferenceBindingModel ethnicity)
        {
            var attribureReference = new ProfileEthnicityReference() { ProfileId = Id, EthnicityId = ethnicity.AttributeId };
            var ret =
                await _attributeService.AddNewProfileEthnicityAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }


        // POST: api/profiles/[id}/languages
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(ProfileLanguageReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProfileLanguage(int Id, ProfileAttributeReferenceBindingModel language)
        {
            var attribureReference = new ProfileLanguageReference() { ProfileId = Id, LanguageId = language.AttributeId };
            var ret =
                await _attributeService.AddNewProfileLanguageAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }


        // POST: api/profiles/[id}/accents
        [Route("{id}/stereotypes")]
        [ResponseType(typeof(ProfileAccentReference))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProfileAccent(int Id, ProfileAttributeReferenceBindingModel accent)
        {
            var attribureReference = new ProfileAccentReference() { ProfileId = Id, AccentId = accent.AttributeId };
            var ret =
                await _attributeService.AddNewProfileAccentAttributeReferenceAsync(false, attribureReference);

            return CreatedAtRoute("DefaultApi", new { id = attribureReference.Id }, attribureReference);
        }

        // PUT: api/profiles/{id}/unions/{unionReferenceId}
        [Route("{id}/unions/{unionReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProfileUnion(int id, int unionReferenceId, ProfileAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var union = await _attributeService.GetProfileUnionReferenceByIdAsync(unionReferenceId);
            if (union == null || union.ProfileId != id)
            {
                return NotFound();
            }

            //make modifications to union object here from values of the ProfileAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateProfileUnionAttributeReferenceAsync(union);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        // PUT: api/profiles/{id}/stereotypes/{stereoTypeReferenceId}
        [Route("{id}/stereotypes/{stereoTypeReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProfileStereoType(int id, int stereoTypeReferenceId, ProfileAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var stereoType = await _attributeService.GetProfileStereoTypeReferenceByIdAsync(stereoTypeReferenceId);
            if (stereoType == null || stereoType.ProfileId != id)
            {
                return NotFound();
            }

            //make modifications to stereoType object here from values of the ProfileAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateProfileStereoTypeAttributeReferenceAsync(stereoType);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/profiles/{id}/stereotypes/{ethnicStereoTypeReferenceId}
        [Route("{id}/stereotypes/{ethnicStereoTypeReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProfileEthnicStereoType(int id, int ethnicStereoTypeReferenceId, ProfileAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var ethnicStereoType = await _attributeService.GetProfileEthnicStereoTypeReferenceByIdAsync(ethnicStereoTypeReferenceId);
            if (ethnicStereoType == null || ethnicStereoType.ProfileId != id)
            {
                return NotFound();
            }

            //make modifications to ethnicStereoType object here from values of the ProfileAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateProfileEthnicStereoTypeAttributeReferenceAsync(ethnicStereoType);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/profiles/{id}/stereotypes/{ethnicityReferenceId}
        [Route("{id}/stereotypes/{ethnicityReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProfileEthnicity(int id, int ethnicityReferenceId, ProfileAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var ethnicity = await _attributeService.GetProfileEthnicityReferenceByIdAsync(ethnicityReferenceId);
            if (ethnicity == null || ethnicity.ProfileId != id)
            {
                return NotFound();
            }

            //make modifications to ethnicity object here from values of the ProfileAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateProfileEthnicityAttributeReferenceAsync(ethnicity);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/profiles/{id}/stereotypes/{languageReferenceId}
        [Route("{id}/stereotypes/{languageReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProfileLanguage(int id, int languageReferenceId, ProfileAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var language = await _attributeService.GetProfileLanguageReferenceByIdAsync(languageReferenceId);
            if (language == null || language.ProfileId != id)
            {
                return NotFound();
            }

            //make modifications to language object here from values of the ProfileAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateProfileLanguageAttributeReferenceAsync(language);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // PUT: api/profiles/{id}/stereotypes/{accentReferenceId}
        [Route("{id}/stereotypes/{accentReferenceId}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProfileAccent(int id, int accentReferenceId, ProfileAttributeReferenceBindingModel value)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            var accent = await _attributeService.GetProfileAccentReferenceByIdAsync(accentReferenceId);
            if (accent == null || accent.ProfileId != id)
            {
                return NotFound();
            }

            //make modifications to accent object here from values of the ProfileAttributeReferenceBindingModel
            try
            {
                await _attributeService.UpdateProfileAccentAttributeReferenceAsync(accent);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        // DELETE: api/profiles/[id}/unions/{unionId}
        [Route("{id}/unions/{unionReferenceId}")]
        [ResponseType(typeof(ProfileUnionReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProfileUnion(int Id, int unionReferenceId)
        {

            var union = await _attributeService.GetProfileUnionReferenceByIdAsync(unionReferenceId);
            if (union == null || union.ProfileId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteProfileUnionAttributeReferenceAsync(union);

            return Ok(union);

        }

        // DELETE: api/profiles/[id}/stereoTypes/{stereoTypeId}
        [Route("{id}/stereotypes/{stereoTypeReferenceId}")]
        [ResponseType(typeof(ProfileStereoTypeReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProfileStereoType(int Id, int stereoTypeReferenceId)
        {

            var stereoType = await _attributeService.GetProfileStereoTypeReferenceByIdAsync(stereoTypeReferenceId);
            if (stereoType == null || stereoType.ProfileId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteProfileStereoTypeAttributeReferenceAsync(stereoType);

            return Ok(stereoType);

        }

        // DELETE: api/profiles/[id}/ethnicitys/{ethnicityId}
        [Route("{id}/stereotypes/{ethnicityReferenceId}")]
        [ResponseType(typeof(ProfileEthnicityReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProfileEthnicity(int Id, int ethnicityReferenceId)
        {

            var ethnicity = await _attributeService.GetProfileEthnicityReferenceByIdAsync(ethnicityReferenceId);
            if (ethnicity == null || ethnicity.ProfileId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteProfileEthnicityAttributeReferenceAsync(ethnicity);

            return Ok(ethnicity);

        }

        // DELETE: api/profiles/[id}/ethnicStereoTypes/{ethnicStereoTypeId}
        [Route("{id}/stereotypes/{ethnicStereoTypeReferenceId}")]
        [ResponseType(typeof(ProfileEthnicStereoTypeReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProfileEthnicStereoType(int Id, int ethnicStereoTypeReferenceId)
        {

            var ethnicStereoType = await _attributeService.GetProfileEthnicStereoTypeReferenceByIdAsync(ethnicStereoTypeReferenceId);
            if (ethnicStereoType == null || ethnicStereoType.ProfileId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteProfileEthnicStereoTypeAttributeReferenceAsync(ethnicStereoType);

            return Ok(ethnicStereoType);

        }

        // DELETE: api/profiles/[id}/languages/{languageId}
        [Route("{id}/stereotypes/{languageReferenceId}")]
        [ResponseType(typeof(ProfileLanguageReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProfileLanguage(int Id, int languageReferenceId)
        {

            var language = await _attributeService.GetProfileLanguageReferenceByIdAsync(languageReferenceId);
            if (language == null || language.ProfileId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteProfileLanguageAttributeReferenceAsync(language);

            return Ok(language);

        }

        // DELETE: api/profiles/[id}/accents/{accentId}
        [Route("{id}/stereotypes/{accentReferenceId}")]
        [ResponseType(typeof(ProfileAccentReference))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProfileAccent(int Id, int accentReferenceId)
        {

            var accent = await _attributeService.GetProfileAccentReferenceByIdAsync(accentReferenceId);
            if (accent == null || accent.ProfileId != Id)
            {
                return NotFound();
            }


            await _attributeService.DeleteProfileAccentAttributeReferenceAsync(accent);

            return Ok(accent);

        }



        #endregion
    }
}