﻿import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { StateService } from '@uirouter/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from "rxjs";
import { Observable, of } from 'rxjs';
import { AlertService } from "./alert.service.js";
import { WINDOW } from './browser.window.provider';
@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    constructor(private alertService: AlertService,
        private http: HttpClient,
        private stateService: StateService,
        @Inject(WINDOW) private window: Window) {

    }
    private _logedInStateObs: BehaviorSubject<boolean> = new BehaviorSubject(this.checkAuthenticated());
    public readonly LogedInStateObs: Observable<boolean> = this._logedInStateObs.asObservable();

    register(
        username: string,
        password: string,
        confirmpassword: string,
        email: string,
        firstNames: string,
        lastNames: string,
        industryRole: number,
        rolesOther: string,
    ) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        let data = {
            firstNames: firstNames,
            lastNames: lastNames,
            industryRole: industryRole,
            rolesOther: rolesOther,
            username: username,
            password: password,
            confirmpassword: confirmpassword,
            email: email
        };
        return this.http.post<any>(
            "/api/account/register",
            data,
            httpOptions
        ).toPromise()
            .then(() => {
                this.alertService.success("User " + username + " successfuly registered")
                this.login(username, password)
                    .then(() => {
                        this.stateService.go('home');
                    })
                    .catch((error) => {

                    });
            }).catch((error) => {
                this._logedInStateObs.next(false);
                localStorage.removeItem('currentUser');
                this.alertService.error(error, "Registration for user " + username + " failed");
            });

    }


    checkUsernameAvailability(username: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        let data = {
            username: username
        };
        return this.http.post<any>(
            "/api/account/checkusername",
            data,
            httpOptions
        ).toPromise()
            .then((response) => {
                // this.alertService.success("User " + username + " successfuly registered")
                return response;
            }).catch((error) => {
                //this._logedInStateObs.next(false);
                //localStorage.removeItem('currentUser');
                // this.alertService.error(error, "Registration for user " + username + " failed");
            });

    }
    checkEmailAvailability(email: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        let data = {
            email: email
        };
        return this.http.post<any>(
            "/api/account/checkemail",
            data,
            httpOptions
        ).toPromise()
            .then((response) => {
                // this.alertService.success("User " + username + " successfuly registered")
                return response;
            }).catch((error) => {
                //this._logedInStateObs.next(false);
                //localStorage.removeItem('currentUser');
                // this.alertService.error(error, "Registration for user " + username + " failed");
            });

    }


    login(username: string, password: string) {
        debugger;
        return this.http.post<any>(window.location.protocol + "//cast.com/oauth2/token", `username=${username}&password=${password}&grant_type=password&client_id=099153c2625149bc8ecb3e85e03f0022`
        ).toPromise()
            .then((user) => {
                if (user && user.access_token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
                    this._logedInStateObs.next(true);
                }
            }).catch((error) => {
                this._logedInStateObs.next(false);
                localStorage.removeItem('currentUser');
                this.alertService.error(error, "Login for " + username + " failed", [(error.error.error_description) ? error.error.error_description : error.error.error], false);

            })

    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this._logedInStateObs.next(false);
        /*this.http.post<any>(
            "/api/account/logout",
            {  }
          ).toPromise()
          .then((response) => {
          }).catch((error) => {
       });*/
        //const $state = this.trans.router.stateService;
        this.stateService.go('login', undefined, undefined);

    }

    checkAuthenticated() {

        return !(localStorage.getItem("currentUser") === null);
    }
}