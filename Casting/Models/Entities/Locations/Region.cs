﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Model.Entities.CastingCalls;

namespace Casting.Model.Entities.Locations
{
    public class Region:Logger
    {
        public Region()
        {
            CastingCalls = new List<CastingCallRegionReference>();
            Cities = new List<City>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public string  ShortName { get; set; }
        public RegionTypeEnum RegionType { get; set; }
        public ICollection<City> Cities { get; set; }
        public ICollection<CastingCallRegionReference> CastingCalls { get; set; }
        
    }
}
