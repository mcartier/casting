﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.BLL.CastingCalls.Interfaces;

namespace Casting.BLL.CastingCalls
{
    public class CastingCallRoleService : BaseService, ICastingCallRoleService
    {
        private ICastingCallAsyncRepository _castingCallRepository;

        public CastingCallRoleService(ICastingCallAsyncRepository castingCallAsyncRepository)
        {
            _castingCallRepository = castingCallAsyncRepository;
        }

        public async Task<CastingCallRole> GetCastingCallRoleByIdAsync(int castingCallRoleId)
        {
            return await _castingCallRepository.GetCastingCallRoleByIdAsync(CastingCallRoleParameterIncludeInQueryEnum.Roles, castingCallRoleId);
        }
        public async Task<CastingCallRole> GetCastingCallRoleByIdWithPersonasAsync(int castingCallRoleId)
        {
            return await _castingCallRepository.GetCastingCallRoleByIdAsync(CastingCallRoleParameterIncludeInQueryEnum.Personas, castingCallRoleId);
        }
        public async Task<CastingCallRole> GetCastingCallRoleByIdWithPersonasAndAssetsAsync(int castingCallRoleId)
        {
            return await _castingCallRepository.GetCastingCallRoleByIdAsync(CastingCallRoleParameterIncludeInQueryEnum.PersonasAndAssets, castingCallRoleId);
        }

        public async Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallRolesByCastingCallIdAsync(CastingCallRoleParameterIncludeInQueryEnum.Roles,  castingCallId);
        }

        public async Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdWithPersonasAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallRolesByCastingCallIdAsync(CastingCallRoleParameterIncludeInQueryEnum.Personas,  castingCallId);
        }

        public async Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdWithPersonasAndAssetsAsync(int castingCallId)
        {
            return await _castingCallRepository.GetCastingCallRolesByCastingCallIdAsync(CastingCallRoleParameterIncludeInQueryEnum.PersonasAndAssets,  castingCallId);
        }

        public async Task<int> AddNewCastingCallRoleAsync(bool orderAtTheTop, CastingCallRole castingCallRole)
        {
            return await _castingCallRepository.AddNewCastingCallRoleAsync(orderAtTheTop, castingCallRole);
        }

        public async Task<int> UpdateCastingCallRoleAsync(CastingCallRole castingCallRole)
        {
            return await _castingCallRepository.UpdateCastingCallRoleAsync(castingCallRole);
        }
        public async Task<int> DeleteCastingCallRole(CastingCallRole castingCallRole)
        {
            return await _castingCallRepository.DeleteCastingCallRoleAsync(castingCallRole);
        }
        
    }
}
