﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Sessions;

namespace Casting.BLL.CastingSessions.Interfaces
{
    public interface ICastingSessionFolderService
    {
        Task<CastingSessionFolder> GetCastingSessionFolderByIdWithRolesAsync(int castingSessionFolderId);
        Task<CastingSessionFolder> GetCastingSessionFolderByIdWithRolesAndPersonasAsync(int castingSessionFolderId);
        Task<CastingSessionFolder> GetCastingSessionFolderByIdWithRolesPersonasAndAssetsAsync(int castingSessionFolderId);
        Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdWithRolesAsync(int castingSessionId);
        Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdWithRolesAndPersonasAsync(int castingSessionId);
        Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdWithRolesPersonasAndAssetsAsync(int castingSessionId);
        Task<int> AddNewCastingSessionFolderAsync(bool orderAtTheTop, CastingSessionFolder castingSessionFolder);
        Task<int> UpdateCastingSessionFolderAsync(CastingSessionFolder castingSessionFolder);
        Task<int> DeleteCastingSessionFolder(CastingSessionFolder castingSessionFolder);
    }
}