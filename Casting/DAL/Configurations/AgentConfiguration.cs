﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Users.Agents;


namespace Casting.DAL.Configurations
{
    class AgentConfiguration : EntityTypeConfiguration<Agent>
    {
        public AgentConfiguration()
        {
            ToTable("Agent");
            HasKey(x => x.Id);
            HasMany(s => s.TalentReferences)
                .WithRequired(a => a.Agent)
                .HasForeignKey(k=>k.AgentId);
            HasMany(mt => mt.ManagedTalents)
                .WithOptional(ma => ma.MainAgent)
                .HasForeignKey(k => k.MainAgentId);
            HasMany(s => s.CityReferences)
                .WithRequired(a => a.Agent)
                .HasForeignKey(k => k.AgentId);
            HasRequired(o => o.AgencyOffice)
                .WithMany(a => a.Agents)
                .HasForeignKey(k => k.AgencyOfficeId);
            // HasRequired(u => u.ApplicationUser)
            //     .WithMany(a=>a.Agents)
            //     .HasForeignKey(k=>k.UserId);
            //   Property(r => r.UserId).HasMaxLength(128).IsRequired();
        }
    }

    class AgencyConfiguration : EntityTypeConfiguration<Agency>
    {
        public AgencyConfiguration()
        {
            HasKey(x => x.Id);
             HasMany(s => s.Offices)
                .WithRequired(a => a.Agency)
                .HasForeignKey(k => k.AgencyId);

        }
    }

    class AgencyAddressConfiguration : EntityTypeConfiguration<AgencyOffice>
    {
        public AgencyAddressConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(a => a.Agency)
                .WithMany(o => o.Offices)
                .HasForeignKey(k => k.AgencyId);
            HasRequired(c => c.City)
                .WithMany(o => o.AgencyOffices)
                .HasForeignKey(c => c.CityId)
                .WillCascadeOnDelete(false);
            HasMany(s => s.Agents)
                .WithRequired(a => a.AgencyOffice)
                .HasForeignKey(k => k.AgencyOfficeId);

        }
    }

    class AgentTalentReferenceConfiguration : EntityTypeConfiguration<AgentTalentReference>
    {
        public AgentTalentReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(t => t.Talent)
                .WithMany(a => a.AgentReferences)
                .HasForeignKey(k => k.TalentId);
            HasRequired(a => a.Agent)
                .WithMany(t => t.TalentReferences)
                .HasForeignKey(k => k.AgentId);
        }
    }

    class AgentCityReferenceConfiguration : EntityTypeConfiguration<AgentCityReference>
    {
        public AgentCityReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(t => t.City)
                .WithMany(a => a.CoveringAgentReferences)
                .HasForeignKey(k => k.CityId);
            HasRequired(a => a.Agent)
                .WithMany(t => t.CityReferences)
                .HasForeignKey(k => k.AgentId);
        }
    }
}
;