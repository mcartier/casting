import { Component, OnDestroy, OnInit, Input, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { Production, BaseProduction, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { RoleDialogService } from '../../services/role-dialog.service';
import { ProductionService } from '../../../../services/production.service';
import { ProductionComponentService } from '../../services/production-component.service';
import { ProductionRoleService } from '../../../../services/production-role.service';
import { SelectItem, SelectItemGroup } from 'primeng/api';
import { Dropdown } from 'primeng/dropdown';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';

//import { NewRoleComponent } from '../new-role/new-role.component'
import { fadeInAnimation } from '../../../../animations/router.animation';
@Component({
    selector: 'app-new-production',
    templateUrl: './new-production.component.html',
    styleUrls: ['./new-production.component.scss'],

    // make fade in animation available to this component
    animations: [fadeInAnimation],

    // attach the fade in animation to the host (root) element of this component
    host: { '[@fadeInAnimation]': '' }
})
export class NewProductionComponent implements OnInit, AfterViewInit, OnDestroy {

    constructor(private ProductionRoleService: ProductionRoleService,
        private _formBuilder: FormBuilder,
        private productionService: ProductionService,
        public stateService: StateService,
        public transitionService: TransitionService,
        private messageDispatchService: MessageDispatchService,
        public transition: Transition, private roleDialogService: RoleDialogService,
        private productionComponentService: ProductionComponentService) { }

    @Input() private metadata: MetadataModel;
    @Input() production: Production;

    @ViewChild('name', {static: false}) nameField: ElementRef;
    @ViewChild('description', {static: false}) descriptionField: ElementRef;
    @ViewChild('productiontype', {static: false}) productionTypeField: Dropdown;
    @ViewChild('productioncard', {static: false}) productionCard: ElementRef;
    @ViewChild('othercard', {static: false}) otherCard: ElementRef;
    @ViewChild('artisticcard', {static: false}) artisticCard: ElementRef;
    @ViewChild('castingcard', {static: false}) castingCard: ElementRef;
    @ViewChild('rolescard', {static: false}) rolesCard: ElementRef;
    @ViewChild('basiccard', {static: false}) basicCard: ElementRef;
   
    private basicProductionInfo: FormGroup;
    private castingDetails: FormGroup;
    private productionDetails: FormGroup;
    private artisticDetails: FormGroup;
    private extraDetails: FormGroup;
    private compensationType: CompensationTypeEnum = CompensationTypeEnum.notPaid;
    //private production: Production;
    private showHints: boolean = false;
    private isDraft: boolean;
    private isEdit: boolean;
    private popRole: boolean;
    private isLoading: boolean;
    private newRoleSubscription: Subscription;
    private deleteRoleSubscription: Subscription;
    private editRoleSubscription: Subscription;
    private saveProductionIntentSubscription: Subscription;
    private deleteProductionIntentSubscription: Subscription;
    private cancelEditProductionIntentSubscription: Subscription;
    private progressIndicatorId: any;
    private productionTypes: SelectItemGroup[] = [];

    private messages: any[] = [];

    ngOnInit() {

        for (var i: number = 0; i < this.metadata.productionTypeCategories.length; i++) {
            var prodType: SelectItemGroup = {
                label: this.metadata.productionTypeCategories[i].name,
               // value: this.metadata.productionTypeCategories[i].id,
                items:[]
            }
            for (var x: number = 0; x < this.metadata.productionTypeCategories[i].productionTypes.length; x++) {
              prodType.items.push({
                  label: this.metadata.productionTypeCategories[i].productionTypes[x].name,
                  value: this.metadata.productionTypeCategories[i].productionTypes[x].id
              })
            }
            this.productionTypes.push(prodType)
        }
         this.deleteRoleSubscription = this.ProductionRoleService.subscribeToDeleteRoleObs()
        .subscribe(
          role => {
            if (role) {
              this.production.roles.splice(this.production.roles.indexOf(role),1);
            }
          });
      this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
        .subscribe(
          (role: Role) => {
            if (role) {
               this.production.roles[this.production.roles.indexOf(role)] = role;
            }
          });
        this.deleteProductionIntentSubscription = this.productionComponentService.subscribeToDeleteProductionIntentObs()
        .subscribe(
          value => {
            if (value) {
                this.moveToProductionsView();
            }
          });

        this.cancelEditProductionIntentSubscription = this.productionComponentService.subscribeToCancelEditProductionIntentObs()
        .subscribe(
          value => {
            if (value) {
                this.moveToProductionsView();
            }
          });

      this.saveProductionIntentSubscription = this.productionComponentService.subscribeToSaveProductionIntentObs()
        .subscribe(
          value => {
            if (value) {
              this.saveProductionClick();
            }
          });

        if (this.transition.params().popRole) {
            this.popRole = this.transition.params().popRole;
        }
        if (this.transition.params().isDraft) {
            this.isDraft = this.transition.params().isDraft;
        }
        if (this.transition.params().isEdit) {
            this.isEdit = this.transition.params().isEdit;
        }
        if (this.transition.params().production) {
            this.production = this.transition.params().production;
        }

        if (this.production) {
            this.basicProductionInfo = this._formBuilder.group({
                name: [this.production.name, Validators.required],
                productionTypeId: [this.production.productionTypeId, Validators.required],
                productionCompany: [this.production.productionCompany],
                description: [this.production.description, Validators.required],
                compensationType: [this.production.compensationType],
                compensationContractDetails: [this.production.compensationContractDetails],
                productionUnionStatus: [this.production.productionUnionStatus]
            }, { updateOn: 'blur' });

            this.castingDetails = this._formBuilder.group({
                castingDirector: [this.production.castingDirector],
                castingAssistant: [this.production.castingAssistant],
                castingAssociate: [this.production.castingAssociate],
                assistantCastingAssociate: [this.production.assistantCastingAssociate]
            }, { updateOn: 'blur' });

            this.productionDetails = this._formBuilder.group({
               executiveProducer: [this.production.executiveProducer],
                coExecutiveProducer: [this.production.coExecutiveProducer],
                producer: [this.production.producer],
                coProducer: [this.production.coProducer]
            }, { updateOn: 'blur' });

            this.artisticDetails = this._formBuilder.group({
                artisticDirector: [this.production.artisticDirector],
                musicDirector: [this.production.musicDirector],
                choreographer: [this.production.choreographer]
            }, { updateOn: 'blur' });

            this.extraDetails = this._formBuilder.group({
                TVNetwork: [this.production.tVNetwork],
                venue: [this.production.venue],
            }, { updateOn: 'blur' });

        } else {
            this.production = new Production();
            this.basicProductionInfo = this._formBuilder.group({
                name: ['', Validators.required],
                productionTypeId: [null, Validators.required],
                productionCompany: [''],
                description: ['', Validators.required],
                compensationType: [0],
                compensationContractDetails: [''],
                productionUnionStatus: [0]
            }, { updateOn: 'blur' });

            this.castingDetails = this._formBuilder.group({
                castingDirector: [''],
                castingAssistant: [''],
                castingAssociate: [''],
                assistantCastingAssociate: ['']
            }, { updateOn: 'blur' });

            this.productionDetails = this._formBuilder.group({
                executiveProducer: [''],
                coExecutiveProducer: [''],
                producer: [''],
                coProducer: ['']
            }, { updateOn: 'blur' });

            this.artisticDetails = this._formBuilder.group({
                artisticDirector: [''],
                musicDirector: [''],
                choreographer: ['']
            }, { updateOn: 'blur' });

            this.extraDetails = this._formBuilder.group({
                TVNetwork: [''],
                venue: [''],
            }, { updateOn: 'blur' });

        }



    }
    ngOnDestroy() {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();

        }
        if (this.newRoleSubscription) {
          this.newRoleSubscription.unsubscribe();

        }
        if (this.saveProductionIntentSubscription) {
          this.saveProductionIntentSubscription.unsubscribe();

        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();

        }
        
    }
    
    ngAfterViewInit() {
        setTimeout(() => this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' }));
        this.productionComponentService.registerProductionScrollElement(this.productionCard);
        this.productionComponentService.registerArtisticScrollElement(this.artisticCard);
        this.productionComponentService.registerCastingScrollElement(this.castingCard);
        this.productionComponentService.registerRolesScrollElement(this.rolesCard);
        this.productionComponentService.registerBasicScrollElement(this.basicCard);
        this.productionComponentService.registerOtherScrollElement(this.otherCard);
        if (this.popRole) {
          if (1 == 1) {
              this.stateService.go("draftproductionnewrole",
              { production: this.production, productionId: this.production.id, draftId:this.production.id });

          } else {
            setTimeout(() => {
              this.newRoleSubscription = this.roleDialogService.createRole(this.production)
                .subscribe(
                  role => {
                    if (role) {
                      this.production.roles.push(role);
                    }
                    this.newRoleSubscription.unsubscribe();
                  });


            })
          }
        }

    }
    private moveToProductionView(productionId: number) {
      this.stateService.go('production', { productionId: productionId });

    }
    private moveToProductionsView() {
      this.stateService.go('productions');

    }
    
    private scrollToProduction() {
    //  this.basicProductionInfo.reset();
    this.productionCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
      //this.productionComponentService.scrollToProduction();
    }
    private scrollToArtistic() {
    //  this.basicProductionInfo.reset();
    this.artisticCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
      //this.productionComponentService.scrollToArtistic();
    }
    private scrollToCasting() {
   //   this.basicProductionInfo.reset();
    this.castingCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
      //this.productionComponentService.scrollToCasting();
    }
    private scrollToRoles() {
  //    this.basicProductionInfo.reset();
    this.rolesCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
      //this.productionComponentService.scrollToRoles();
    }
    private scrollToBasic() {
   //   this.basicProductionInfo.reset();
    this.basicCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
      //this.productionComponentService.scrollToBasic();
    }
    private scrollToOther() {
   //   this.basicProductionInfo.reset();
    this.otherCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
      //this.productionComponentService.scrollToOther();
    }
    private deleteProductionClick() {
   //   this.basicProductionInfo.reset();
      this.productionComponentService.tryDeleteProduction();
    }

    private cancelEditProductionClick() {
   //   this.basicProductionInfo.reset();
       this.moveToProductionsView();
    }

    private getSubmissionObject(): BaseProduction {
        let production: BaseProduction = new BaseProduction;
       production.productionCompany = this.basicProductionInfo.get('productionCompany').value
      production.productionTypeId = this.basicProductionInfo.get('productionTypeId').value;
      production.name = this.basicProductionInfo.get('name').value
      production.description = this.basicProductionInfo.get('description').value
      production.compensationType = this.basicProductionInfo.get('compensationType').value;
      production.compensationContractDetails = this.basicProductionInfo.get('compensationContractDetails').value
      production.productionUnionStatus = this.basicProductionInfo.get('productionUnionStatus').value;

      production.castingDirector = this.castingDetails.get('castingDirector').value
      production.castingAssistant = this.castingDetails.get('castingAssistant').value
      production.castingAssociate = this.castingDetails.get('castingAssociate').value
      production.assistantCastingAssociate = this.castingDetails.get('assistantCastingAssociate').value

      production.executiveProducer = this.productionDetails.get('executiveProducer').value
      production.coExecutiveProducer = this.productionDetails.get('coExecutiveProducer').value
      production.producer = this.productionDetails.get('producer').value
      production.coProducer = this.productionDetails.get('coProducer').value

      production.artisticDirector = this.artisticDetails.get('artisticDirector').value
      production.musicDirector = this.artisticDetails.get('musicDirector').value
      production.choreographer = this.artisticDetails.get('choreographer').value
      production.tVNetwork = this.extraDetails.get('TVNetwork').value
      production.venue = this.extraDetails.get('venue').value

      return production;
    }

    private getUpdateSubmissionObject(): UpdateProduction {
      let production: UpdateProduction = Object.assign(this.getSubmissionObject(), { id: this.production.id });
      return production;
    }
    private getNewSubmissionObject(): NewProduction {
        let production: NewProduction = Object.assign(this.getSubmissionObject());
      return production;
    }

    private clearMessages() {
        this.messages = [];
    }

    private saveProductionClick() {
        this.clearMessages();
        if (this.basicProductionInfo.valid) {
            //this.role = this.basicRoleInfo.value;
            //submissionProduction = Object.assign(this.basicProductionInfo.value, this.castingDetails.value, this.productionDetails.value, this.artisticDetails.value, this.extraDetails.value);
             if (this.production.id && this.production.id !== 0) {
                 let submissionProduction: UpdateProduction = this.getUpdateSubmissionObject();
                this.productionService.saveProduction(this.production.id, submissionProduction).subscribe((production: Production) => {
                    this.messageDispatchService.dispatchSuccesMessage("Production:'" + production.name + "' was saved successfully",
                        "Production Saved");

                this.moveToProductionView(this.production.id);
                    //this.production = production;
                  },
                    (error) => {
                        this.messageDispatchService.dispatchErrorMessage(
                            "Error saving production:'" + submissionProduction.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                            "Production Save Error");
                        this.messages.push({
                            severity: 'error',
                            summary: "Production Save Error",
                            detail: "Error saving production:'" + submissionProduction.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                        });

                    })

            } else {
               let submissionProduction: NewProduction = this.getNewSubmissionObject();
                this.productionService.createProduction(submissionProduction).subscribe((production: Production) => {
                    this.production = production;
                    this.messageDispatchService.dispatchSuccesMessage("New production:'" + production.name + "' was created successfully",
                        "Production Created");
                          this.moveToProductionView(this.production.id);

                  
                },
                    (error) => {
                        this.messageDispatchService.dispatchErrorMessage(
                            "Error creating production:'" + submissionProduction.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                            "Production Creation Error");
                        this.messages.push({
                            severity: 'error',
                            summary: "Production Creation Error",
                            detail: "Error creating production:'" + submissionProduction.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                        });
 })

            }
        } else {
            if (!this.basicProductionInfo.get('name').valid) {
                this.basicProductionInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.nameField.nativeElement.focus();
           
            }
            else if (!this.basicProductionInfo.get('productionTypeId').valid) {
                this.basicProductionInfo.get('productionTypeId').markAsDirty();
                this.productionTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.productionTypeField.focusViewChild.nativeElement.focus();
              }
            else if (!this.basicProductionInfo.get('description').valid) {
                this.basicProductionInfo.get('description').markAsDirty();
                this.descriptionField.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
                this.descriptionField.nativeElement.focus();
            }

        }

    }
    private addRoleClick() {
     // this.basicProductionInfo.reset();
        if (this.production.id && this.production.id !== 0) {
            //setTimeout(() => {
            if (1 == 1) {
              this.stateService.go("draftproductionnewrole",
                { production: this.production, productionId: this.production.id, draftId: this.production.id });

            } else {
              this.newRoleSubscription = this.roleDialogService.createRole(this.production)
                .subscribe(
                  role => {
                    if (role) {
                      this.production.roles.push(role);
                    }
                    this.newRoleSubscription.unsubscribe();
                  });
            }
            // });

        } else {

             let submissionObject = Object.assign(this.basicProductionInfo.value, this.castingDetails.value, this.productionDetails.value, this.artisticDetails.value, this.extraDetails.value);
            this.productionService.createProduction(submissionObject).subscribe((production: Production) => {
                this.isLoading = false;
                this.stateService.go('draftproduction', { production: production, draftId: production.id, popRole: true });
            },
                (error) => {
                })
        
        }
    }

}
