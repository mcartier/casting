﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;

namespace Casting.DAL.Configurations
{
    class AudioConfiguration : EntityTypeConfiguration<Audio>
    {
        public AudioConfiguration()
        {
            HasKey(x => x.Id);
          
        }
    }
    class AudioReferenceConfiguration : EntityTypeConfiguration<AudioReference>
    {
        public AudioReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(m => m.Audio);

        }
    }
    class PersonaAudioReferenceConfiguration : EntityTypeConfiguration<PersonaAudioReference>
    {
        public PersonaAudioReferenceConfiguration()
        {
            ToTable("PersonaAudioReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Audio);
            HasRequired(p => p.Persona)
                .WithMany(r => r.AudioReferences)
                .HasForeignKey(k => k.PersonaId);


        }
    }
    class BasketAudioReferenceConfiguration : EntityTypeConfiguration<BasketAudioReference>
    {
        public BasketAudioReferenceConfiguration()
        {
            ToTable("BasketAudioReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Audio);
            HasRequired(p => p.Basket)
                .WithMany(r => r.AudioReferences)
                .HasForeignKey(k => k.BasketId);
        }
    }
    class ProfileAudioReferenceConfiguration : EntityTypeConfiguration<ProfileAudioReference>
    {
        public ProfileAudioReferenceConfiguration()
        {
            ToTable("ProfileAudioReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Audio);
            HasRequired(p => p.Profile)
                .WithMany(r => r.AudioReferences)
                .HasForeignKey(k => k.ProfileId);
        }
    }
}
;