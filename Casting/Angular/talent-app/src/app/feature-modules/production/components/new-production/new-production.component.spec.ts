import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewProductionComponent } from './new-production.component';

describe('NewProductionComponent', () => {
  let component: NewProductionComponent;
  let fixture: ComponentFixture<NewProductionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewProductionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewProductionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
