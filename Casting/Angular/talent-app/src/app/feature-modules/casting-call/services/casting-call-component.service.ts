import { Injectable } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CastingCallComponentService {

    constructor() { }

    private _saveCastingCallIntentObs: Subject<any> = new Subject();
    public readonly saveCastingCallIntentObs: Observable<any> = this._saveCastingCallIntentObs.asObservable();

    public subscribeToSaveCastingCallIntentObs(): Observable<any> {
        return this.saveCastingCallIntentObs;
    }
    public trySaveCastingCall() {
        this._saveCastingCallIntentObs.next(true);
    }
    private _cancelEditCastingCallIntentObs: Subject<any> = new Subject();
    public readonly cancelEditCastingCallIntentObs: Observable<any> = this._cancelEditCastingCallIntentObs.asObservable();

    public subscribeToCancelEditCastingCallIntentObs(): Observable<any> {
        return this.cancelEditCastingCallIntentObs;
    }
    public tryCancelEditCastingCall() {
        this._cancelEditCastingCallIntentObs.next(true);
    }
    private _deleteCastingCallIntentObs: Subject<any> = new Subject();
    public readonly deleteCastingCallIntentObs: Observable<any> = this._deleteCastingCallIntentObs.asObservable();

    public subscribeToDeleteCastingCallIntentObs(): Observable<any> {
        return this.deleteCastingCallIntentObs;
    }
    public tryDeleteCastingCall() {
        this._deleteCastingCallIntentObs.next(true);
    }

    private castingCallElement: ElementRef;
    private artisticElement: ElementRef;
    private castingElement: ElementRef;
    private rolesElement: ElementRef;
    private basicElement: ElementRef;
    private otherElement: ElementRef;

    public registerCastingCallScrollElement(element: ElementRef) {
        this.castingCallElement = element;
    }
    public registerOtherScrollElement(element: ElementRef) {
        this.otherElement = element;
    }
    public registerArtisticScrollElement(element: ElementRef) {
        this.artisticElement = element;
    }
    public registerCastingScrollElement(element: ElementRef) {
        this.castingElement = element;
    }
    public registerRolesScrollElement(element: ElementRef) {
        this.rolesElement = element;
    }
    public registerBasicScrollElement(element: ElementRef) {
        this.basicElement = element;
    }

    //

    public scrollToCastingCall() {
        this.castingCallElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToOther() {
        this.otherElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToArtistic() {
        this.artisticElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToCasting() {
        this.castingElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToRoles() {
        this.rolesElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToBasic() {
        this.basicElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }

}
