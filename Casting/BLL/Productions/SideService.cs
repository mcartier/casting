﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class SideService : BaseService, ISideService
    {
        private IProductionAsyncRepository _productionRepository;

        public SideService(IProductionAsyncRepository productionAsyncRepository)
        {
            _productionRepository = productionAsyncRepository;
        }


        public async Task<Side> GetSideByIdAsync(int sideId)
        {
            return await _productionRepository.GetSideByIdAsync(sideId, false);
        }
           public async Task<IEnumerable<Side>> GetSidesByProductionIdAsync(int productionId)
        {
            return await _productionRepository.GetSidesByProductionIdAsync(productionId,false);
        }
        
        public async Task<int> AddNewSideAsync(Side side)
        {
            return await _productionRepository.AddNewSideAsync(side);
        }

        public async Task<int> UpdateSideAsync(Side side)
        {
            return await _productionRepository.UpdateSideAsync(side);
        }

        public async Task<int> DeleteSideAsync(Side side)
        {
            return await _productionRepository.DeleteSideAsync(side);
        }

     
    }
}
