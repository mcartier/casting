﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Users.Talents;


namespace Casting.DAL.Configurations
{
    public class TalentConfiguration : EntityTypeConfiguration<Talent>
    {
        public TalentConfiguration()
        {
            ToTable("Talent");
            HasKey(x => x.Id);
               HasMany(p => p.Profiles)
                   .WithOptional(t=>t.Talent)
                   .HasForeignKey(k => k.TalentId);//.WithOptionalPrincipal(t=);
            // .WithOptionalDependent();

            //HasRequired(p => p.ProfileReference)
            //     .WithRequiredPrincipal(t => t.Talent);
            HasMany(s => s.AgentReferences)
                .WithRequired(a => a.Talent)
                .HasForeignKey(k=>k.TalentId);
            Property(p => p.MainAgentId).IsOptional();
            HasOptional(ma => ma.MainAgent)
                .WithMany(t => t.ManagedTalents)
                .HasForeignKey(k => k.MainAgentId);
         //   HasOptional(u => u.ApplicationUser)
             //   .WithMany(a => a.Talents)
             //   .HasForeignKey(k => k.UserId);
         //   Property(r => r.UserId).HasMaxLength(128);//.IsRequired();

            // HasMany(p => p.Personas)
            //    .WithRequired(t => t.Talent)
            //   .HasForeignKey(k => k.TalentId);
        }
    }
    public class HouseTalentConfiguration : EntityTypeConfiguration<HouseTalent>
    {
        public HouseTalentConfiguration()
        {
            ToTable("HouseTalent");
            HasKey(x => x.Id);

        }
    }
    public class StudioTalentConfiguration : EntityTypeConfiguration<StudioTalent>
    {
        public StudioTalentConfiguration()
        {
            ToTable("StudioTalent");
            HasKey(x => x.Id);
            //HasRequired(d => d.Director)
            //    .WithMany(t => t.StudioTalents)
            //    .HasForeignKey(k => k.DirectorId)
            //    .WillCascadeOnDelete(false);
            //  HasOptional(p => p.PersonaReference)
            //    .WithRequired(t => t.Talent);
        }
    }


}
