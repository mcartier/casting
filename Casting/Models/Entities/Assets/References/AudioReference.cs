﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets.References
{
   public abstract class AudioReference : Logger
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int AudioId { get; set; }
        public Audio Audio { get; set; }
       

        public AssetReferenceTypeEnum AssetReferenceTypeEnum { get; set; }
        public int OrderIndex { get; set; }
    }
}