import { Transition } from "@uirouter/angular";
import { SideQuickLinksComponent } from './components/side-quick-links/side-quick-links.component';
import { ProductionComponent } from './components/production/production.component';
import { NewRoleComponent } from './components/new-role/new-role.component';
import { ProductionsComponent } from './components/productions/productions.component';
import { ProductionListComponent } from './components/production-list/production-list.component';
import { NewProductionComponent } from './components/new-production/new-production.component';
import { StateTransitionHandlerService } from '../../services/state-transition-handler.service';
import { ProductionService } from '../../services/production.service';
import { StateDependencyResolverService } from '../../services/state-dependency-resolver.service';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../models/production.models';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../models/metadata';


export function getMetadataFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<MetadataModel> {
    let metaDate: MetadataModel;
    let promise: Promise<MetadataModel>;

    return stateTransitionService.getMetadataFromServer(transition);

}

export function getEmptyProduction() {
  let prod: Production;
  return prod;
}
export function getProductionFromTransactionOrServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<Production> {
  let productionId: number;
  let production: Production;
  let promise: Promise<Production>;

  return stateTransitionService.getProductionFromTransactionOrServer(transition);

}
export function getProductionsFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<Production[]> {
  let productions: Production[];
  let promise: Promise<Production>;

  return stateTransitionService.getProductionsFromServer(transition);

}
export function getEmptyRole() {
  let prod: Role;
  return prod;
}
export function getRoleFromTransactionOrServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<Role> {
  let roleId: number;
  let role: Role;
  let promise: Promise<Role>;

  return stateTransitionService.getRoleFromTransactionOrServer(transition);

}
export function getRolesFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<Role[]> {
  let roles: Role[];
  let promise: Promise<Role>;

  return stateTransitionService.getRolesFromServer(transition);

}


export const productionListState = {
  name: 'productions',
  url: '/productions',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: ProductionsComponent }
    },
  resolve: [
    // All the folders are fetched from the Folders service
    {
          token: 'productions', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionsFromServer
    }
  ]


};
export const productionState = {
  name: 'production',
    url: '/productions/:productionId',
    parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: ProductionComponent }
  },
  params: {
    production: null
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
    {
          token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
    }
      ]
};

export const newproductionState = {
  name: 'newproduction',
  url: '/productions/new',
    parent: "mainPulloutNonScrollContainer",
  views: {
    "main@mainPulloutNonScrollContainer": { component: NewProductionComponent },
     // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
    production: null,
    popRole: false,
    isDraft: false,
    isEdit: false
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
          token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ]
};
export const draftproductionState = {
  name: 'draftproduction',
    url: '/productions/draft/:draftId',
    parent: "mainPulloutNonScrollContainer",
  views: {
      "main@mainPulloutNonScrollContainer": { component: NewProductionComponent },
     // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
      production: null,
      popRole: false,
      isDraft: true,
      isEdit: false
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
          token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
      },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ],
};

export const editproductionState = {
  name: 'editproduction',
    url: '/productions/edit/:productionId',
    parent: "mainPulloutNonScrollContainer",
  views: {
      "main@mainPulloutNonScrollContainer": { component: NewProductionComponent },
     // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
      production: null,
      popRole: false,
      isDraft: false,
      isEdit: true
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
          token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
      },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ],
};

export const productionNewRoleState = {
  name: 'productionnewrole',
  url: '/productions/:productionId/roles/new',
  parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: NewRoleComponent }
  },
  params: {
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
      },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ]
};

export const draftproductionNewRoleState = {
  name: 'draftproductionnewrole',
  url: '/productions/draft/:draftId/roles/new',
    parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: NewRoleComponent },
    // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
   // production: null,
    popRole: false,
    isDraft: true,
    isEdit: false
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
      },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ],
};

export const editRoleState = {
  name: 'editrole',
    url: '/productions/:productionId/roles/:roleId/edit',
    parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: NewRoleComponent },
    // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
   
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
      },
    {
      token: 'role', deps: [StateTransitionHandlerService, Transition], resolveFn: getRoleFromTransactionOrServer
      },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ],
};




export const NEWPRODUCTION_STATES = [
  productionListState,
  productionState,
     newproductionState,
  draftproductionState,
    editproductionState,
    productionNewRoleState,
    draftproductionNewRoleState,
  editRoleState,
 
];

