﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.BLL.CastingSessions.Interfaces;

namespace Casting.BLL.CastingSessions
{
    public class CastingSessionRoleService : BaseService, ICastingSessionRoleService
    {
        private ICastingSessionAsyncRepository _castingSessionRepository;

        public CastingSessionRoleService(ICastingSessionAsyncRepository castingSessionAsyncRepository)
        {
            _castingSessionRepository = castingSessionAsyncRepository;
        }

        public async Task<CastingSessionRole> GetCastingSessionRoleByIdAsync(int castingSessionRoleId)
        {
            return await _castingSessionRepository.GetCastingSessionRoleByIdAsync(CastingSessionRoleParameterIncludeInQueryEnum.Roles, castingSessionRoleId);
        }
        public async Task<CastingSessionRole> GetCastingSessionRoleByIdWithPersonasAsync(int castingSessionRoleId)
        {
            return await _castingSessionRepository.GetCastingSessionRoleByIdAsync(CastingSessionRoleParameterIncludeInQueryEnum.Personas, castingSessionRoleId);
        }
        public async Task<CastingSessionRole> GetCastingSessionRoleByIdWithPersonasAndAssetsAsync(int castingSessionRoleId)
        {
            return await _castingSessionRepository.GetCastingSessionRoleByIdAsync(CastingSessionRoleParameterIncludeInQueryEnum.PersonasAndAssets, castingSessionRoleId);
        }

        public async Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionRolesByCastingSessionIdAsync(CastingSessionRoleParameterIncludeInQueryEnum.Roles,  castingSessionId);
        }

        public async Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdWithPersonasAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionRolesByCastingSessionIdAsync(CastingSessionRoleParameterIncludeInQueryEnum.Personas,  castingSessionId);
        }

        public async Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdWithPersonasAndAssetsAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionRolesByCastingSessionIdAsync(CastingSessionRoleParameterIncludeInQueryEnum.PersonasAndAssets,  castingSessionId);
        }

        public async Task<int> AddNewCastingSessionRoleAsync(bool orderAtTheTop, CastingSessionRole castingSessionRole)
        {
            return await _castingSessionRepository.AddNewCastingSessionRoleAsync(orderAtTheTop, castingSessionRole);
        }

        public async Task<int> UpdateCastingSessionRoleAsync(CastingSessionRole castingSessionRole)
        {
            return await _castingSessionRepository.UpdateCastingSessionRoleAsync(castingSessionRole);
        }
        public async Task<int> DeleteCastingSessionRole(CastingSessionRole castingSessionRole)
        {
            return await _castingSessionRepository.DeleteCastingSessionRoleAsync(castingSessionRole);
        }
        
    }
}
