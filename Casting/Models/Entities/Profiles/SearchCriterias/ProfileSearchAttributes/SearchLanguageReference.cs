﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.ProfileAttributes.References;
using Casting.Model.Enums;
using Casting.Model.Entities.Profiles.SearchCriterias;


namespace Casting.Model.Entities.SearchCriterias.ProfileAttributes
{
    public class ProfileSearchCriteriaLanguageReference: LanguageReference
    {
        public ProfileSearchCriteriaLanguageReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.Search;
        }
        public int ProfileSearchCriteriaId { get; set; }
        public ProfileSearchCriteria ProfileSearchCriteria { get; set; }

    }
}
