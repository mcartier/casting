// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using Casting.DAL;
using Casting.DAL.Repositories;
using Casting.DAL.Repositories.Interfaces;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using StructureMap;
using StructureMap.Configuration.DSL;
using StructureMap.Graph;
using System.Data.Entity;
using StructureMap.Pipeline;
using Casting.Model.Entities.Users;
using Casting.BLL.Profiles;
using Casting.BLL.CastingUsers;
using Casting.BLL.Profiles.Interfaces;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Metadata;
using Casting.BLL.Productions;
using Casting.BLL.Productions.Interfaces;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.BLL.CastingCalls;
using Casting.BLL.Location.Interfaces;
using Casting.BLL.Location;
using Casting.BLL.CastingCalls.Interface;
using Casting.BLL.AttributeReferences.Interfaces;
using Casting.BLL.AttributeReferences;
using Casting.BLL.TalentAssets.Interfaces;
using Casting.BLL.TalentAssets;





namespace Casting.Web.DependencyResolution {


    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                    scan.With(new ControllerConvention());
                });

            For<IUserStore<ApplicationUser>>()
                .Use<UserStore<ApplicationUser>>()
                .Ctor<DbContext>()
                .Is<CastingContext>(cfg => cfg.SelectConstructor(() => new CastingContext()).Ctor<string>().Is("casting"));

            ForConcreteType<UserManager<ApplicationUser>>()
                .Configure
                .SetProperty(userManager => userManager.PasswordValidator = new PasswordValidator
                {
                    RequiredLength = 6,
                    RequireNonLetterOrDigit = true,
                    RequireDigit = true,
                    RequireLowercase = true,
                    RequireUppercase = true
                })
                .SetProperty(userManager => userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
                {
                    RequireUniqueEmail = false,
                    AllowOnlyAlphanumericUserNames = true
                       
                });

            ForConcreteType<CastingContext>().Configure.SelectConstructor(() => new CastingContext()).Ctor<string>().Is("casting");
           // For<IUserStore<ApplicationUser>>().Use<UserStore<ApplicationUser>>();

            //dal
            For<ICastingContext>().Use<CastingContext>();

            For<ITalentAsyncRepository>().Use<TalentAsyncRepository>();
            For<IAgentAsyncRepository>().Use<AgentAsyncRepository>();
            For<IProfileAsyncRepository>().Use<ProfileAsyncRepository>();
            For<ITalentAssetAsyncRepository>().Use<TalentAssetAsyncRepository>();
            For<IAttributeReferenceAsyncRepository>().Use<AttributeReferenceAsyncRepository>();
            For<IPersonaAsyncRepository>().Use<PersonaAsyncRepository>();
            For<IProductionAsyncRepository>().Use<ProductionAsyncRepository>();
            For<ICastingCallAsyncRepository>().Use<CastingCallAsyncRepository>();
            For<ICastingSessionAsyncRepository>().Use<CastingSessionAsyncRepository>();
            For<IMetadataAsyncRepository>().Use<MetadataAsyncRepository>();
            For<IFileAsyncRepository>().Use<FileAsyncRepository>();

            For<IProfileService>().Use<ProfileService>();
            For<ITalentService>().Use<TalentService>();
            For<IAgentService>().Use<AgentService>();
            For<ITalentAgentService>().Use<TalentAgentService>();
            For<IMetadataService>().Use<MetadataService>();
            For<IProductionService>().Use<ProductionService>();
            For<IProductionRoleService>().Use<ProductionRoleService>();

            For<IAuditionDetailService>().Use<AuditionDetailService>();
            For<IPayDetailService>().Use<PayDetailService>();
            For<IShootDetailService>().Use<ShootDetailService>();
            For<ISideService>().Use<SideService>();

            For<IFileService>().Use<FileService>();

            For<ILocationsService>().Use<LocationsService>();

            For<ICastingCallService>().Use<CastingCallService>();
            For<ICastingCallFolderService>().Use<CastingCallFolderService>();
            For<ICastingCallRoleService>().Use<CastingCallRoleService>();
            For<ICastingCallFolderRoleService>().Use<CastingCallFolderRoleService>();
            For<ICastingCallTalentLocationService>().Use<CastingCallTalentLocationService>();

            For<ITalentImageReferenceService>().Use<TalentImageReferenceService>();
            For<ITalentResumeReferenceService>().Use<TalentResumeReferenceService>();
            For<ITalentVideoReferenceService>().Use<TalentVideoReferenceService>();
            For<ITalentAudioReferenceService>().Use<TalentAudioReferenceService>();

            For<IAttributeReferenceService>().Use<AttributeReferenceService>();

           }

        #endregion
    }
}