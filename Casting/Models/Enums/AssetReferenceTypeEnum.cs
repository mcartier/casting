﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum AssetReferenceTypeEnum
    {
       Persona = 1,
       Basket = 2,
       Profile = 3,
       ProfileMainImage = 4
    }
}
