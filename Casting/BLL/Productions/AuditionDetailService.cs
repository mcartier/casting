﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class AuditionDetailService : BaseService, IAuditionDetailService
    {
        private IProductionAsyncRepository _productionRepository;

        public AuditionDetailService(IProductionAsyncRepository productionAsyncRepository)
        {
            _productionRepository = productionAsyncRepository;
        }


        public async Task<AuditionDetail> GetAuditionDetailByIdAsync(int auditionDetailId)
        {
            return await _productionRepository.GetAuditionDetailByIdAsync(auditionDetailId, false);
        }
           public async Task<IEnumerable<AuditionDetail>> GetAuditionDetailsByProductionIdAsync(int productionId)
        {
            return await _productionRepository.GetAuditionDetailsByProductionIdAsync(productionId,false);
        }
        
        public async Task<int> AddNewAuditionDetailAsync(AuditionDetail auditionDetail)
        {
            return await _productionRepository.AddNewAuditionDetailAsync(auditionDetail);
        }

        public async Task<int> UpdateAuditionDetailAsync(AuditionDetail auditionDetail)
        {
            return await _productionRepository.UpdateAuditionDetailAsync(auditionDetail);
        }

        public async Task<int> DeleteAuditionDetailAsync(AuditionDetail auditionDetail)
        {
            return await _productionRepository.DeleteAuditionDetailAsync(auditionDetail);
        }
    }
}
