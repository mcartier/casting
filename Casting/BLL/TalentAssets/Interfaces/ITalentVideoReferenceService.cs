﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;

namespace Casting.BLL.TalentAssets.Interfaces
{
    public interface ITalentVideoReferenceService
    {
        Task<VideoReference> GetVideoReferenceByIdAsync(int videoId);
        Task<IEnumerable<VideoReference>> GetProfileVideoReferencesForParentAsync(int profileId, bool withAssets);
        Task<IEnumerable<VideoReference>> GetPersonaVideoReferencesForParentAsync(int personaId, bool withAssets);
        Task<IEnumerable<VideoReference>> GetBasketVideoReferencesForParentAsync(int basketId, bool withAssets);

        Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            VideoReference assetReference);

        Task<int> AddNewVideoAssetReferenceAsync(bool orderAtTheTop,
            int parentId, VideoReference assetReference);

        Task<int> AddNewProfileVideoAssetReferenceAsync(bool orderAtTheTop, ActorProfileVideoReference assetReference);
        Task<int> AddNewPersonaVideoAssetReferenceAsync(bool orderAtTheTop, PersonaVideoReference assetReference);
        Task<int> AddNewBasketVideoAssetReferenceAsync(bool orderAtTheTop, BasketVideoReference assetReference);
        Task<int> DeleteVideoAssetReferenceAsync(int parentId, VideoReference assetReference);
        Task<int> DeleteProfileVideoAssetReferenceAsync(ActorProfileVideoReference assetReference);
        Task<int> DeleteBasketVideoAssetReferenceAsync(BasketVideoReference assetReference);
        Task<int> DeletePersonaVideoAssetReferenceAsync(PersonaVideoReference assetReference);

        Task<int> UpdateVideoAssetReferenceAsync(int parentId,
            VideoReference assetReference);

        Task<int> UpdateProfileVideoAssetReferenceAsync(ActorProfileVideoReference assetReference);
        Task<int> UpdateBasketVideoAssetReferenceAsync(BasketVideoReference assetReference);
        Task<int> UpdatePersonaVideoAssetReferenceAsync(PersonaVideoReference assetReference);

        Task<int> ReOrderVideoAssetAsync(int parentId,
            int desiredOrderIndex, VideoReference assetReference);

        Task<int> ReOrderProfileVideoAssetAsync(int desiredOrderIndex, ActorProfileVideoReference assetReference);
        Task<int> ReOrderBasketVideoAssetAsync(int desiredOrderIndex, BasketVideoReference assetReference);
        Task<int> ReOrderPersonaVideoAssetAsync(int desiredOrderIndex, PersonaVideoReference assetReference);
    }
}