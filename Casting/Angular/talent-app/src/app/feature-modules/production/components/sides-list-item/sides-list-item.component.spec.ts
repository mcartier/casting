import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidesListItemComponent } from './sides-list-item.component';

describe('SidesListItemComponent', () => {
  let component: SidesListItemComponent;
  let fixture: ComponentFixture<SidesListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidesListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidesListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
