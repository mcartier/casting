﻿using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;
using System.Collections.Generic;

namespace Casting.BLL.CastingCalls.Interface
{
    public interface ICastingCallTalentLocationService
    {
        Task<IEnumerable<CastingCallCityReference>> GetCastingCallTalentCityReferencesByCastingCallId(int castingCallId);
        Task<CastingCallCityReference> GetCastingCallTalentCityReferenceById(int castingCallLocationReferenceId);
        Task<IEnumerable<CastingCallRegionReference>> GetCastingCallTalentRegionReferencesByCastingCallId(int castingCallId);
        Task<CastingCallRegionReference> GetCastingCallTalentRegionReferenceById(int castingCallLocationReferenceId);
        Task<IEnumerable<CastingCallCountryReference>> GetCastingCallTalentCountryReferencesByCastingCallId(int castingCallId);
        Task<CastingCallCountryReference> GetCastingCallTalentCountryReferenceById(int castingCallLocationReferenceId);
        Task<int> AddNewCityTalentLocationAsync(CastingCallCityReference castingCallCityReference);
        Task<int> DeleteCityTalentReferenceAsync(CastingCallCityReference castingCallCityReference);
        Task<int> AddNewRegionTalentLocationAsync(CastingCallRegionReference castingCallRegionReference);
        Task<int> DeleteRegionTalentReferenceAsync(CastingCallRegionReference castingCallRegionReference);
        Task<int> AddNewCountryTalentLocationAsync(CastingCallCountryReference castingCallCountryReference);
        Task<int> DeleteCountryTalentReferenceAsync(CastingCallCountryReference castingCallCountryReference);
    }
}