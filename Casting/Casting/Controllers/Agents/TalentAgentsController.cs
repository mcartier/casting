﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Enums;
using Casting.Web.Helpers;
using Casting.Web.Models.Talent;

namespace Casting.Web.Controllers.Agents
{
    public class AgentTalentsController : BaseAgentApiController
    {
        private ITalentAgentService _agentTalentService;
        private readonly UserManager<ApplicationUser> _userManager;
          public AgentTalentsController(UserManager<ApplicationUser> userManager, ITalentAgentService agentAgentTalentService, ICastingContext castingContext) : base(castingContext)
        {
            _agentTalentService = agentAgentTalentService;// castingContext;
            _userManager = userManager;
        }


        // GET: api/AgentTalents
        [ResponseType(typeof(IEnumerable<AgentTalentReference>))]
        public async Task<IHttpActionResult> GetTalentAgents(int talentId)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var agentAgentTalents = await _agentTalentService.GetTalentAgentsByTalentIdWithAgentAndTalentAsync(talentId);
            
            return Ok(agentAgentTalents);
        }

        // GET: api/TalentAgents/5
        [ResponseType(typeof(AgentTalentReference))]
        public async Task<IHttpActionResult> GetTalentAgentReference(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentAgentTalents = await 
            var agentAgentTalent = await _agentTalentService.GetTalentAgentReferenceByIdWithAgentAndTalentAsync(id);
            if (agentAgentTalent == null)
            {
                return NotFound();
            }
             return Ok(agentAgentTalent);
        }

        // POST: api/AgentTalents
        [ResponseType(typeof(AgentTalentReference))]
        public async Task<IHttpActionResult> PostAgentTalentReference(TalentAgentReferenceBindingModels talentAgentReference)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var agentAgentTalents = await _agentTalentService.GetTalentAgentsByTalentIdAsync(talentAgentReference.TalentId);
            if (agentAgentTalents.Any(a => a.AgentId == talentAgentReference.AgentId))
            {
                return Conflict();
            }
            AgentTalentReference newAgentTalent = new AgentTalentReference();
            newAgentTalent.AgentId = talentAgentReference.AgentId;
            newAgentTalent.TalentId = talentAgentReference.TalentId;
           

            await _agentTalentService.AddNewAgentTalentReferenceAsync(newAgentTalent);

            return CreatedAtRoute("DefaultApi", new { id = newAgentTalent.Id }, newAgentTalent);
        }

        // DELETE: api/AgentTalents/5
        [ResponseType(typeof(AgentTalentReference))]
        public async Task<IHttpActionResult> DeleteAgentTalentReference(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentAgentTalents = await 
            var agentTalentReference = await _agentTalentService.GetTalentAgentReferenceByIdAsync(id);
            if (agentTalentReference == null)
            {
                return NotFound();
            }
            if (agentTalentReference.AgentId != user.BaseUserId)
            {
                return Unauthorized();
            }

            await _agentTalentService.DeleteAgentTalentReference(agentTalentReference);

            return Ok(agentTalentReference);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               
            }
            base.Dispose(disposing);
        }

      
    }
}