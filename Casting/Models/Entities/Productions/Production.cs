﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;
using Casting.Model.Entities.CastingCalls;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace Casting.Model.Entities.Productions


{
    public class Production : Logger
    {
        public Production()
        {
            Roles = new List<Role>();
            CastingCalls = new List<CastingCall>();
            CastingSessions = new List<CastingSession>();
            ShootDetails = new List<ShootDetail>();
            AuditionDetails = new List<AuditionDetail>();
            Sides = new List<Side>();
            PayDetails = new List<PayDetail>();
        }
        public int Id { get; set; }
        public int DirectorId { get; set; }
        public Director Director { get; set; }
        public string ProductionCompany { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ProductionInfo { get; set; }
        public string ExecutiveProducer { get; set; }
        public bool IsSaved { get; set; }
        public string CoExecutiveProducer { get; set; }
        public string Producer { get; set; }
        public string CoProducer { get; set; }
        public string CastingDirector { get; set; }
        public string CastingAssistant { get; set; }
        public string CastingAssociate { get; set; }
        public string AssistantCastingAssociate { get; set; }
        public string ArtisticDirector { get; set; }
        public string MusicDirector { get; set; }
        public string Choreographer { get; set; }
        public string TVNetwork { get; set; }
        public string Venue { get; set; }
        public string CompensationContractDetails { get; set; }
        public ProductionUnionStatusEnum ProductionUnionStatus { get; set; }
        //public ICollection<Union> UnionRequirements { get; set; }
        //public ICollection<ProductionProductionTypeReference> ProductionTypeReferences { get; set; }

        public ProductionType ProductionType { get; set; }
        public int ProductionTypeId { get; set; }

       // public ICollection<ProductionType> ProductionTypes { get; set; }
        public CompensationTypeEnum CompensationType { get; set; }
        [NotMapped]
        public bool CountIsSet { get; set; }

        public ICollection<Role> Roles { get; set; }
        [NotMapped]
        public int RolesCount { get; set; }
        public OrderByValuesEnum RolesSortOrderI { get; set; }
        [NotMapped]
        public int CastingCallsCount { get; set; }
        public ICollection<CastingCall> CastingCalls { get; set; }
        public OrderByValuesEnum CastingCallsSortOrderI { get; set; }
        [NotMapped]
        public int CastingSessionsCount { get; set; }
        public ICollection<CastingSession> CastingSessions { get; set; }
        public ICollection<ShootDetail> ShootDetails { get; set; }
        public ICollection<AuditionDetail> AuditionDetails { get; set; }
        public ICollection<Side> Sides { get; set; }
        public ICollection<PayDetail> PayDetails { get; set; }
        public OrderByValuesEnum CastingSessionsSortOrderI { get; set; }

        public int OrderIndex { get; set; }


    }
}
