import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { UIRouterModule } from "@uirouter/angular";
import { Transition } from "@uirouter/angular";
import { UIRouter } from '@uirouter/angular';
import { Visualizer } from '@uirouter/visualizer';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { GlobalModule } from '../../global/global.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';


import { ErrorInterceptor } from '../../interceptors/errorInterceptor';
import { JwtInterceptor } from '../../interceptors/jwtInterceptor';
import { NewProductionComponent } from './components/new-production/new-production.component';
import { ProductionComponent } from './components/production/production.component';
import { NEWPRODUCTION_STATES } from './production.states';
import { AddRoleComponent } from './components/add-role/add-role.component';
import { NewRoleComponent } from './components/new-role/new-role.component';
import { NewRoleDialogComponent } from './components/new-role-dialog/new-role-dialog.component';
import { RoleListComponent } from './components/role-list/role-list.component';
import { RoleListItemComponent } from './components/role-list-item/role-list-item.component';
import { ProductionListComponent } from './components/production-list/production-list.component';
import { ProductionsComponent } from './components/productions/productions.component';
import { SideQuickLinksComponent } from './components/side-quick-links/side-quick-links.component';
import { ProductionListItemComponent } from './components/production-list-item/production-list-item.component';
import { ShootDetailsListComponent } from './components/shoot-details-list/shoot-details-list.component';
import { ShootDetailsListItemComponent } from './components/shoot-details-list-item/shoot-details-list-item.component';
import { AuditionDetailsListComponent } from './components/audition-details-list/audition-details-list.component';
import { AuditionDetailsListItemComponent } from './components/audition-details-list-item/audition-details-list-item.component';
import { PayDetailsListComponent } from './components/pay-details-list/pay-details-list.component';
import { PayDetailsListItemComponent } from './components/pay-details-list-item/pay-details-list-item.component';
import { SidesListComponent } from './components/sides-list/sides-list.component';
import { SidesListItemComponent } from './components/sides-list-item/sides-list-item.component';
import { SidesDialogComponent } from './components/sides-dialog/sides-dialog.component';
import { AuditionDetailsDialogComponent } from './components/audition-details-dialog/audition-details-dialog.component';
import { PayDetailsDialogComponent } from './components/pay-details-dialog/pay-details-dialog.component';
import { ShootDetailsDialogComponent } from './components/shoot-details-dialog/shoot-details-dialog.component';

//import { SidesDialogComponent } from '../../components/sides-dialog/sides-dialog.component';
//import { DropFileDirective } from '../../directives/drop-file.directive';
import { RoleDialogService } from './services/role-dialog.service';
import { ProductionComponentService } from './services/production-component.service';
import { ProductionDialogService } from './services/production-dialog.service';

@NgModule({
    declarations: [
        NewProductionComponent,
        AddRoleComponent,
        NewRoleComponent,
    NewRoleDialogComponent,
    RoleListComponent,
        RoleListItemComponent,
        ProductionComponent,
        ProductionListComponent,
        ProductionsComponent,
        SideQuickLinksComponent,
        ProductionListItemComponent,
        ShootDetailsListComponent,
        ShootDetailsListItemComponent,
        AuditionDetailsListComponent,
        AuditionDetailsListItemComponent,
        PayDetailsListComponent,
        PayDetailsListItemComponent,
        SidesListComponent,
        SidesListItemComponent,
    SidesDialogComponent,
    AuditionDetailsDialogComponent,
    PayDetailsDialogComponent,
    ShootDetailsDialogComponent,
         //SidesDialogComponent
      //  DropFileDirective,
    ],
  imports: [
      CommonModule,
      UIRouterModule.forChild({ states: NEWPRODUCTION_STATES }),
      GlobalModule,
      FormsModule,
      ReactiveFormsModule,
    ],
    exports: [
        // DropFileDirective,
      ],
  entryComponents: [
    NewRoleComponent
  ],

  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
       RoleDialogService,
    ProductionComponentService,
    ProductionDialogService,

  ],
})
export class NewProductionModule { }
