﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Casting.Model.Entities.Personas;

using Casting.Model.Enums;
using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Entities.Productions;

namespace Casting.Model.Entities.AttributeReferences
{
    public class RoleLanguageReference: LanguageReference
    {
        public RoleLanguageReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.Role;
        }
        public int RoleId { get; set; }
       // public Role Role { get; set; }

    }
}
