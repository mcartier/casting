﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{

    public interface IAuditionDetailService
    {
    
        Task<AuditionDetail> GetAuditionDetailByIdAsync(int auditionDetailId);
        Task<IEnumerable<AuditionDetail>> GetAuditionDetailsByProductionIdAsync(int productionId);
        Task<int> AddNewAuditionDetailAsync(AuditionDetail auditionDetail);
        Task<int> UpdateAuditionDetailAsync(AuditionDetail auditionDetail);
        Task<int> DeleteAuditionDetailAsync(AuditionDetail auditionDetail);
    }
}