import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayDetailsListComponent } from './pay-details-list.component';

describe('PayDetailsListComponent', () => {
  let component: PayDetailsListComponent;
  let fixture: ComponentFixture<PayDetailsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayDetailsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayDetailsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
