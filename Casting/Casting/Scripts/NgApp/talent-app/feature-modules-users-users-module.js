(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["feature-modules-users-users-module"],{

/***/ "./node_modules/@rxweb/reactive-form-validators/@rxweb/reactive-form-validators.es5.js":
/*!*********************************************************************************************!*\
  !*** ./node_modules/@rxweb/reactive-form-validators/@rxweb/reactive-form-validators.es5.js ***!
  \*********************************************************************************************/
/*! exports provided: RxReactiveFormsModule, RxFormBuilder, FormBuilderConfiguration, alpha, alphaNumeric, compare, contains, creditCard, digit, email, hexColor, lowerCase, maxDate, maxLength, minDate, maxNumber, minLength, minNumber, password, pattern, propArray, propObject, prop, range, required, upperCase, time, url, json, greaterThan, greaterThanEqualTo, lessThanEqualTo, lessThan, choice, different, numeric, even, odd, factor, leapYear, allOf, oneOf, noneOf, mac, ascii, dataUri, port, latLong, extension, fileSize, endsWith, startsWith, primeNumber, latitude, longitude, rule, file, custom, unique, image, notEmpty, async, cusip, grid, ReactiveFormConfig, NumericValueType, IpVersion, RxFormControl, RxFormGroup, RxwebValidators, ɵc, ɵe, ɵb, ɵf, ɵd, ɵa, ɵh, ɵi, ɵk, ɵl, ɵg, ɵj, ɵm */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxReactiveFormsModule", function() { return RxReactiveFormsModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxFormBuilder", function() { return RxFormBuilder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FormBuilderConfiguration", function() { return FormBuilderConfiguration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alpha", function() { return alpha; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "alphaNumeric", function() { return alphaNumeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "compare", function() { return compare; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "contains", function() { return contains; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "creditCard", function() { return creditCard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "digit", function() { return digit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "email", function() { return email; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hexColor", function() { return hexColor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lowerCase", function() { return lowerCase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maxDate", function() { return maxDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maxLength", function() { return maxLength; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minDate", function() { return minDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "maxNumber", function() { return maxNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minLength", function() { return minLength; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "minNumber", function() { return minNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "password", function() { return password; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pattern", function() { return pattern; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "propArray", function() { return propArray; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "propObject", function() { return propObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "prop", function() { return prop; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "range", function() { return range; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "required", function() { return required; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "upperCase", function() { return upperCase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "time", function() { return time; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "url", function() { return url; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "json", function() { return json; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "greaterThan", function() { return greaterThan; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "greaterThanEqualTo", function() { return greaterThanEqualTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lessThanEqualTo", function() { return lessThanEqualTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lessThan", function() { return lessThan; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "choice", function() { return choice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "different", function() { return different; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "numeric", function() { return numeric; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "even", function() { return even; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "odd", function() { return odd; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "factor", function() { return factor; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "leapYear", function() { return leapYear; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "allOf", function() { return allOf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "oneOf", function() { return oneOf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "noneOf", function() { return noneOf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "mac", function() { return mac; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ascii", function() { return ascii; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dataUri", function() { return dataUri; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "port", function() { return port; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latLong", function() { return latLong; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extension", function() { return extension; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fileSize", function() { return fileSize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "endsWith", function() { return endsWith; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startsWith", function() { return startsWith; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "primeNumber", function() { return primeNumber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "latitude", function() { return latitude; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "longitude", function() { return longitude; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rule", function() { return rule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "file", function() { return file; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "custom", function() { return custom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "unique", function() { return unique; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "image", function() { return image; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "notEmpty", function() { return notEmpty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "async", function() { return async; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "cusip", function() { return cusip; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "grid", function() { return grid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReactiveFormConfig", function() { return ReactiveFormConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumericValueType", function() { return NumericValueType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IpVersion", function() { return IpVersion; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxFormControl", function() { return RxFormControl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxFormGroup", function() { return RxFormGroup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RxwebValidators", function() { return RxwebValidators; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵc", function() { return RxwebDynamicFormComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵe", function() { return RxwebControlComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵb", function() { return BaseDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵf", function() { return ControlHostDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵd", function() { return HtmlControlTemplateDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵa", function() { return RxwebFormDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵh", function() { return BaseValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵi", function() { return ControlExpressionProcess; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵk", function() { return FileControlDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵl", function() { return ImageFileControlDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵg", function() { return RxFormControlDirective; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵj", function() { return DecimalProvider; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ɵm", function() { return BaseFormBuilder; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();



var Linq = /** @class */ (function () {
    function Linq() {
    }
    /**
     * @param {?} expression
     * @return {?}
     */
    Linq.functionCreator = function (expression) {
        var /** @type {?} */ functionSetter = [];
        var /** @type {?} */ match = expression.match(/^\s*\(?\s*([^)]*)\s*\)?\s*=>(.*)/);
        var /** @type {?} */ splitSelect = match[2].split(",");
        for (var /** @type {?} */ i = 0; i < splitSelect.length; i++) {
            var /** @type {?} */ equalToOperator = splitSelect[i].match(/^\s*\(?\s*([^)]*)\s*\)?\s*|===|!==|==|!=|>=|>|<=|<|(.*)/);
            if (equalToOperator !== null) {
                functionSetter = new Function(match[1], "return " + equalToOperator.input);
            }
            else {
                equalToOperator = splitSelect[i].match(/^\s*\(?\s*([^)]*)\s*\)?\s*=(.*)/);
                if (equalToOperator === null) {
                    functionSetter = new Function(match[1], "return " + splitSelect.input);
                }
                else {
                    functionSetter = new Function(match[1], "return " + equalToOperator.input);
                }
            }
        }
        if (splitSelect.length == 0)
            functionSetter = { accessFunction: new Function(match[1], "return " + match[2]) };
        return functionSetter;
    };
    /**
     * @param {?} jObject
     * @param {?} expression
     * @param {?} parentObject
     * @return {?}
     */
    Linq.IsPassed = function (jObject, expression, parentObject) {
        var /** @type {?} */ expressionFunction = expression;
        if (parentObject && typeof expression == "string")
            expressionFunction = Linq.functionCreator(expression);
        if (parentObject && expressionFunction)
            return expressionFunction(parentObject, jObject);
        return true;
    };
    /**
     * @param {?} expression
     * @return {?}
     */
    Linq.expressionParser = function (expression) {
        var /** @type {?} */ columns = [];
        var /** @type {?} */ expressionString = expression.toString();
        var /** @type {?} */ expressionArguments = Linq.extractArguments(expressionString.match(/\(([^)]+)\)/g));
        if (expressionArguments.length > 0) {
            var /** @type {?} */ splitTexts = expressionString.replace(/\s/g, '').replace(new RegExp(/{|}/, "g"), "").split(new RegExp(/return|===|!==|==|!=|>=|>|<=|<|&&/));
            splitTexts.forEach(function (t) {
                expressionArguments.forEach(function (x) {
                    t = t.trim();
                    if (t.startsWith(x + '.')) {
                        var /** @type {?} */ splitText = t.split('.');
                        if (splitText.length == 2)
                            columns.push({ propName: splitText[1].trim() });
                        else {
                            var /** @type {?} */ arrayProp = splitText[1].split('[');
                            var /** @type {?} */ jObject = {
                                propName: splitText[splitText.length - 1].trim(),
                                objectPropName: arrayProp[0],
                                arrayIndex: arrayProp.length > 1 ? arrayProp[1].replace("]", "") : undefined
                            };
                            columns.push(jObject);
                        }
                    }
                });
            });
        }
        return columns;
    };
    /**
     * @param {?} splitTexts
     * @return {?}
     */
    Linq.extractArguments = function (splitTexts) {
        var /** @type {?} */ expressionArguments = [];
        splitTexts[0].split(",").forEach(function (t) { return expressionArguments.push(t.trim().replace("(", "").replace(")", "")); });
        return expressionArguments;
    };
    /**
     * @param {?} expression
     * @return {?}
     */
    Linq.expressionColumns = function (expression) {
        var /** @type {?} */ columns = [];
        var /** @type {?} */ splitExpressions = [];
        if (typeof expression == "string") {
            expression.split("=>")[1].split(" && ").forEach(function (t) {
                t.split(" || ").forEach(function (x) {
                    splitExpressions.push(x.trim().split(' ')[0]);
                });
            });
            splitExpressions.forEach(function (t) {
                var /** @type {?} */ splitText = t.split('.');
                if (splitText.length == 2)
                    columns.push({ propName: splitText[1].trim() });
                else {
                    var /** @type {?} */ arrayProp = splitText[1].split('[');
                    var /** @type {?} */ jObject = {
                        propName: splitText[splitText.length - 1].trim(),
                        objectPropName: arrayProp[0],
                        arrayIndex: arrayProp.length > 1 ? arrayProp[1].replace("]", "") : undefined
                    };
                    columns.push(jObject);
                }
            });
        }
        else {
            columns = Linq.expressionParser(expression);
        }
        return columns;
    };
    return Linq;
}());
var AnnotationTypes = {
    numeric: 'numeric',
    required: 'required',
    minLength: 'minLength',
    maxLength: 'maxLength',
    minNumber: 'minNumber',
    maxNumber: 'maxNumber',
    pattern: 'pattern',
    password: 'password',
    compare: 'compare',
    minDate: 'minDate',
    maxDate: 'maxDate',
    alpha: 'alpha',
    alphaNumeric: 'alphaNumeric',
    email: 'email',
    hexColor: 'hexColor',
    lowerCase: 'lowerCase',
    url: 'url',
    upperCase: 'upperCase',
    nested: 'nested',
    propArray: 'propArray',
    propObject: 'propObject',
    contains: 'contains',
    range: 'range',
    custom: 'custom',
    digit: "digit",
    creditCard: "creditCard",
    time: "time",
    json: "json",
    greaterThan: "greaterThan",
    greaterThanEqualTo: "greaterThanEqualTo",
    lessThan: "lessThan",
    lessThanEqualTo: "lessThanEqualTo",
    choice: "choice",
    different: "different",
    even: "even",
    odd: "odd",
    factor: "factor",
    leapYear: "leapYear",
    allOf: "allOf",
    oneOf: "oneOf",
    noneOf: "noneOf",
    mac: "mac",
    ascii: "ascii",
    dataUri: "dataUri",
    port: "port",
    latLong: "latLong",
    extension: "extension",
    fileSize: "fileSize",
    endsWith: "endsWith",
    startsWith: "startsWith",
    primeNumber: "primeNumber",
    latitude: "latitude",
    longitude: "longitude",
    compose: "compose",
    rule: "rule",
    file: "file",
    image: "image",
    unique: "unique",
    notEmpty: "notEmpty",
    ip: "ip",
    cusip: "cusip",
    grid: "grid"
};
var PROPERTY = "property";
var OBJECT_PROPERTY = "objectProperty";
var ARRAY_PROPERTY = "arrayProperty";
var STRING = "string";
var MESSAGE = "message";
var BLANK = "";
var ELEMENT_VALUE = "value";
var BLUR = "blur";
var FOCUS = "focus";
var CHANGE = "change";
var INPUT = "INPUT";
var SELECT = "SELECT";
var CHECKBOX = "checkbox";
var RADIO = "radio";
var FILE = "file";
var TEXTAREA = "textarea";
var CONTROLS_ERROR = "controlsError";
var VALUE_CHANGED_SYNC = "valueChangedSync";
var FUNCTION_STRING = "function";
var OBJECT_STRING = "object";
var RX_WEB_VALIDATOR = "rxwebValidator";
var NUMBER = "number";
var BOOLEAN = "boolean";
var defaultContainer = new (/** @class */ (function () {
    function class_1() {
        this.instances = [];
        this.modelIncrementCount = 0;
    }
    /**
     * @template T
     * @param {?} instanceFunc
     * @return {?}
     */
    class_1.prototype.get = function (instanceFunc) {
        var /** @type {?} */ instance = this.instances.filter(function (instance) { return instance.instance === instanceFunc; })[0];
        return instance;
    };
    /**
     * @param {?} target
     * @param {?} parameterIndex
     * @param {?} propertyKey
     * @param {?} annotationType
     * @param {?} config
     * @param {?} isAsync
     * @return {?}
     */
    class_1.prototype.init = function (target, parameterIndex, propertyKey, annotationType, config, isAsync) {
        var /** @type {?} */ decoratorConfiguration = {
            propertyIndex: parameterIndex,
            propertyName: propertyKey,
            annotationType: annotationType,
            config: config,
            isAsync: isAsync
        };
        var /** @type {?} */ isPropertyKey = (propertyKey != undefined);
        this.addAnnotation(!isPropertyKey ? target : target.constructor, decoratorConfiguration);
    };
    /**
     * @param {?} name
     * @param {?} propertyType
     * @param {?} entity
     * @param {?} target
     * @return {?}
     */
    class_1.prototype.initPropertyObject = function (name, propertyType, entity, target) {
        var /** @type {?} */ propertyInfo = {
            name: name,
            propertyType: propertyType,
            entity: entity
        };
        defaultContainer.addProperty(target.constructor, propertyInfo);
    };
    /**
     * @param {?} instanceFunc
     * @return {?}
     */
    class_1.prototype.addInstanceContainer = function (instanceFunc) {
        var /** @type {?} */ instanceContainer = {
            instance: instanceFunc,
            propertyAnnotations: [],
            properties: []
        };
        this.instances.push(instanceContainer);
        return instanceContainer;
    };
    /**
     * @param {?} instanceFunc
     * @param {?} propertyInfo
     * @return {?}
     */
    class_1.prototype.addProperty = function (instanceFunc, propertyInfo) {
        var /** @type {?} */ instance = this.instances.filter(function (instance) { return instance.instance === instanceFunc; })[0];
        if (instance) {
            this.addPropertyInfo(instance, propertyInfo);
        }
        else {
            instance = this.addInstanceContainer(instanceFunc);
            this.addPropertyInfo(instance, propertyInfo);
        }
    };
    /**
     * @param {?} instance
     * @param {?} propertyInfo
     * @return {?}
     */
    class_1.prototype.addPropertyInfo = function (instance, propertyInfo) {
        var /** @type {?} */ property = instance.properties.filter(function (t) { return t.name == propertyInfo.name; })[0];
        if (!property)
            instance.properties.push(propertyInfo);
    };
    /**
     * @param {?} instanceFunc
     * @param {?} decoratorConfiguration
     * @return {?}
     */
    class_1.prototype.addAnnotation = function (instanceFunc, decoratorConfiguration) {
        this.addProperty(instanceFunc, { propertyType: PROPERTY, name: decoratorConfiguration.propertyName });
        var /** @type {?} */ instance = this.instances.filter(function (instance) { return instance.instance === instanceFunc; })[0];
        if (instance)
            instance.propertyAnnotations.push(decoratorConfiguration);
        else {
            instance = this.addInstanceContainer(instanceFunc);
            instance.propertyAnnotations.push(decoratorConfiguration);
        }
        if (decoratorConfiguration.config && decoratorConfiguration.config.conditionalExpression) {
            var /** @type {?} */ columns = Linq.expressionColumns(decoratorConfiguration.config.conditionalExpression);
            this.addChangeValidation(instance, decoratorConfiguration.propertyName, columns);
        }
        if (instance && decoratorConfiguration.config && ((decoratorConfiguration.annotationType == AnnotationTypes.compare || decoratorConfiguration.annotationType == AnnotationTypes.greaterThan || decoratorConfiguration.annotationType == AnnotationTypes.greaterThanEqualTo || decoratorConfiguration.annotationType == AnnotationTypes.lessThan || decoratorConfiguration.annotationType == AnnotationTypes.lessThanEqualTo || decoratorConfiguration.annotationType == AnnotationTypes.different || decoratorConfiguration.annotationType == AnnotationTypes.factor) || (decoratorConfiguration.annotationType == AnnotationTypes.creditCard && decoratorConfiguration.config.fieldName) || ((decoratorConfiguration.annotationType == AnnotationTypes.minDate || decoratorConfiguration.annotationType == AnnotationTypes.maxDate) && decoratorConfiguration.config.fieldName))) {
            this.setConditionalValueProp(instance, decoratorConfiguration.config.fieldName, decoratorConfiguration.propertyName);
        }
    };
    /**
     * @param {?} instance
     * @param {?} propName
     * @param {?} refPropName
     * @return {?}
     */
    class_1.prototype.setConditionalValueProp = function (instance, propName, refPropName) {
        if (!instance.conditionalValidationProps)
            instance.conditionalValidationProps = {};
        if (!instance.conditionalValidationProps[propName])
            instance.conditionalValidationProps[propName] = [];
        if (instance.conditionalValidationProps[propName].indexOf(refPropName) == -1)
            instance.conditionalValidationProps[propName].push(refPropName);
    };
    /**
     * @param {?} instance
     * @param {?} propertyName
     * @param {?} columns
     * @return {?}
     */
    class_1.prototype.addChangeValidation = function (instance, propertyName, columns) {
        if (instance) {
            if (!instance.conditionalValidationProps)
                instance.conditionalValidationProps = {};
            columns.forEach(function (t) {
                if (t.propName && !t.objectPropName) {
                    if (!instance.conditionalValidationProps[t.propName])
                        instance.conditionalValidationProps[t.propName] = [];
                    if (instance.conditionalValidationProps[t.propName].indexOf(propertyName) == -1)
                        instance.conditionalValidationProps[t.propName].push(propertyName);
                }
                else {
                    if (t.propName && t.objectPropName) {
                        if (!instance.conditionalObjectProps)
                            instance.conditionalObjectProps = [];
                        t.referencePropName = propertyName;
                        instance.conditionalObjectProps.push(t);
                    }
                }
            });
        }
    };
    /**
     * @param {?} instanceFunc
     * @return {?}
     */
    class_1.prototype.clearInstance = function (instanceFunc) {
        var /** @type {?} */ instance = this.instances.filter(function (instance) { return instance.instance === instanceFunc; })[0];
        if (instance) {
            var /** @type {?} */ indexOf = this.instances.indexOf(instance);
            this.instances.splice(indexOf, 1);
        }
    };
    return class_1;
}()))();
var EntityService = /** @class */ (function () {
    function EntityService() {
    }
    /**
     * @param {?} jsonObject
     * @return {?}
     */
    EntityService.prototype.clone = function (jsonObject) {
        var /** @type {?} */ jObject = {};
        for (var /** @type {?} */ columnName in jsonObject) {
            if (Array.isArray(jsonObject[columnName])) {
                jObject[columnName] = [];
                for (var _i = 0, _a = jsonObject[columnName]; _i < _a.length; _i++) {
                    var row = _a[_i];
                    jObject[columnName].push(this.clone(row));
                }
            }
            else if (typeof jsonObject[columnName] == "object")
                jObject[columnName] = this.clone(jsonObject[columnName]);
            else
                jObject[columnName] = jsonObject[columnName];
        }
        return jObject;
    };
    /**
     * @param {?} firstObject
     * @param {?} secondObject
     * @return {?}
     */
    EntityService.prototype.merge = function (firstObject, secondObject) {
        for (var /** @type {?} */ columnName in secondObject) {
            if (Array.isArray(secondObject[columnName])) {
                if (!firstObject[columnName])
                    firstObject[columnName] = [];
                for (var _i = 0, _a = secondObject[columnName]; _i < _a.length; _i++) {
                    var row = _a[_i];
                    firstObject[columnName].push(this.clone(row));
                }
            }
            else if (typeof firstObject[columnName] == "object")
                firstObject[columnName] = this.merge(firstObject[columnName], secondObject[columnName]);
            else
                firstObject[columnName] = secondObject[columnName];
        }
        return firstObject;
    };
    return EntityService;
}());
var BaseFormBuilder = /** @class */ (function () {
    function BaseFormBuilder() {
        this.entityService = new EntityService();
    }
    /**
     * @return {?}
     */
    BaseFormBuilder.prototype.createInstance = function () {
        var /** @type {?} */ instance = {};
        defaultContainer.modelIncrementCount = defaultContainer.modelIncrementCount + 1;
        var /** @type {?} */ modelName = "RxWebModel" + defaultContainer.modelIncrementCount;
        instance.constructor = Function("\"use strict\";return(function " + modelName + "(){ })")();
        return instance;
    };
    /**
     * @param {?} model
     * @param {?} formBuilderConfiguration
     * @param {?=} classInstance
     * @return {?}
     */
    BaseFormBuilder.prototype.createClassObject = function (model, formBuilderConfiguration, classInstance) {
        var _this = this;
        var /** @type {?} */ instanceContainer = defaultContainer.get(model);
        var /** @type {?} */ autoInstanceConfig = formBuilderConfiguration ? formBuilderConfiguration.autoInstanceConfig : undefined;
        if (!autoInstanceConfig) {
            return classInstance && typeof classInstance != "function" ? classInstance : this.getInstance(model, []);
        }
        else {
            classInstance = classInstance && typeof classInstance != "function" ? classInstance : this.getInstance(model, autoInstanceConfig.arguments || []);
            if (autoInstanceConfig.objectPropInstanceConfig && autoInstanceConfig.objectPropInstanceConfig.length > 0) {
                autoInstanceConfig.objectPropInstanceConfig.forEach(function (t) {
                    var /** @type {?} */ objectProperty = instanceContainer.properties.filter(function (property) { return property.name == t.propertyName && property.propertyType == OBJECT_PROPERTY; })[0];
                    if (objectProperty)
                        classInstance[t.propertyName] = _this.getInstance(objectProperty.entity, t.arguments || []);
                });
            }
            if (autoInstanceConfig.arrayPropInstanceConfig && autoInstanceConfig.arrayPropInstanceConfig.length > 0) {
                autoInstanceConfig.arrayPropInstanceConfig.forEach(function (t) {
                    var /** @type {?} */ property = instanceContainer.properties.filter(function (property) { return property.name == t.propertyName && property.propertyType == ARRAY_PROPERTY; })[0];
                    if (property) {
                        classInstance[t.propertyName] = [];
                        for (var /** @type {?} */ i = 0; i < t.rowItems; i++) {
                            classInstance[t.propertyName].push(_this.getInstance(property.entity, t.arguments || []));
                        }
                    }
                });
            }
            return classInstance;
        }
    };
    /**
     * @param {?} model
     * @param {?} entityObject
     * @return {?}
     */
    BaseFormBuilder.prototype.updateObject = function (model, entityObject) {
        var _this = this;
        var /** @type {?} */ instanceContainer = defaultContainer.get(model);
        var /** @type {?} */ classInstance = this.getInstance(model, []);
        if (instanceContainer) {
            instanceContainer.properties.forEach(function (t) {
                switch (t.propertyType) {
                    case PROPERTY:
                        classInstance[t.name] = entityObject[t.name];
                        break;
                    case OBJECT_PROPERTY:
                        if (entityObject[t.name])
                            classInstance[t.name] = _this.updateObject(t.entity, entityObject[t.name]);
                        break;
                    case ARRAY_PROPERTY:
                        if (entityObject[t.name] && Array.isArray(entityObject[t.name])) {
                            classInstance[t.name] = [];
                            for (var _i = 0, _a = entityObject[t.name]; _i < _a.length; _i++) {
                                var row = _a[_i];
                                var /** @type {?} */ instanceObject = _this.updateObject(t.entity, row);
                                classInstance[t.name].push(instanceObject);
                            }
                        }
                        break;
                }
            });
        }
        return classInstance;
    };
    /**
     * @param {?} instanceFunc
     * @param {?} entityObject
     * @return {?}
     */
    BaseFormBuilder.prototype.instaceProvider = function (instanceFunc, entityObject) {
        var /** @type {?} */ instance = defaultContainer.get(instanceFunc);
        var /** @type {?} */ prototype = entityObject ? entityObject.__proto__ : this.getInstance(instanceFunc, []).__proto__;
        if (prototype.__proto__) {
            var /** @type {?} */ isLoop = false;
            do {
                isLoop = prototype.__proto__.constructor != Object;
                if (isLoop) {
                    var /** @type {?} */ extendClassInstance = defaultContainer.get(prototype.__proto__.constructor);
                    instance = this.entityService.merge(this.entityService.clone(instance), this.entityService.clone(extendClassInstance));
                    prototype = prototype.__proto__;
                }
            } while (isLoop);
        }
        return instance;
    };
    /**
     * @param {?} model
     * @param {?} objectArguments
     * @return {?}
     */
    BaseFormBuilder.prototype.getInstance = function (model, objectArguments) {
        var /** @type {?} */ classInstance = Object.create(model.prototype);
        model.apply(classInstance, objectArguments);
        return classInstance;
    };
    return BaseFormBuilder;
}());
var FormBuilderConfiguration = /** @class */ (function () {
    /**
     * @param {?=} formBuilderConfiguration
     */
    function FormBuilderConfiguration(formBuilderConfiguration) {
        if (formBuilderConfiguration)
            for (var column in formBuilderConfiguration)
                this[column] = formBuilderConfiguration[column];
    }
    return FormBuilderConfiguration;
}());
var ReactiveFormConfig = /** @class */ (function () {
    function ReactiveFormConfig() {
    }
    /**
     * @param {?} jObject
     * @return {?}
     */
    ReactiveFormConfig.set = function (jObject) {
        if (jObject)
            ReactiveFormConfig.json = jObject;
    };
    return ReactiveFormConfig;
}());
ReactiveFormConfig.json = {};
var ObjectMaker = /** @class */ (function () {
    function ObjectMaker() {
    }
    /**
     * @param {?} key
     * @param {?} config
     * @param {?} values
     * @return {?}
     */
    ObjectMaker.toJson = function (key, config, values) {
        var /** @type {?} */ message = config ? config.message : null;
        var /** @type {?} */ messageText = (message) ? message : (ReactiveFormConfig && ReactiveFormConfig.json && ReactiveFormConfig.json.validationMessage && ReactiveFormConfig.json.validationMessage[key]) ? ReactiveFormConfig.json.validationMessage[key] : '';
        values.forEach(function (t, index) {
            messageText = messageText.replace("{{" + index + "}}", t);
        });
        var /** @type {?} */ jObject = {};
        jObject[key] = {
            message: messageText, refValues: values
        };
        return jObject;
    };
    /**
     * @return {?}
     */
    ObjectMaker.null = function () {
        return null;
    };
    return ObjectMaker;
}());
/**
 * @param {?} conditionalValidationProps
 * @return {?}
 */
function conditionalChangeValidator(conditionalValidationProps) {
    var /** @type {?} */ oldValue = undefined;
    var /** @type {?} */ setTimeOut = function (control) {
        var /** @type {?} */ timeOut = window.setTimeout(function (t) {
            window.clearTimeout(timeOut);
            control.updateValueAndValidity();
        }, 100);
    };
    return function (control) {
        var /** @type {?} */ parentFormGroup = control.parent;
        var /** @type {?} */ value = control.value;
        if (parentFormGroup && oldValue != value) {
            oldValue = value;
            conditionalValidationProps.forEach(function (t) {
                if (t.indexOf("[]") != -1) {
                    var /** @type {?} */ splitText = t.split("[]");
                    var /** @type {?} */ formArray = /** @type {?} */ (parentFormGroup.get([splitText[0]]));
                    if (formArray)
                        formArray.controls.forEach(function (formGroup) {
                            var /** @type {?} */ abstractControl = formGroup.get(splitText[1]);
                            if (abstractControl) {
                                setTimeOut(abstractControl);
                            }
                        });
                }
                else {
                    var /** @type {?} */ control = null;
                    t.split('.').forEach(function (name, index) { control = (index == 0) ? parentFormGroup.controls[name] : control.controls[name]; });
                    if (control) {
                        setTimeOut(control);
                    }
                }
            });
        }
        return ObjectMaker.null();
    };
}
var CreditCardRegex = /** @class */ (function () {
    function CreditCardRegex() {
        this.Visa = new RegExp('^(?:4[0-9]{12})(?:[0-9]{3})?$');
        this.AmericanExpress = new RegExp('^(?:3[47][0-9]{13})$');
        this.Maestro = new RegExp('^(?:(?:5[0678]\\d\\d|6304|6390|67\\d\\d)\\d{8,15})$');
        this.JCB = new RegExp('^(?:(?:2131|1800|35\\d{3})\\d{11})$');
        this.Discover = new RegExp('^(?:6(?:011|5[0-9]{2})(?:[0-9]{12}))$');
        this.DinersClub = new RegExp('^(?:3(?:0[0-5]|[68][0-9])[0-9]{11})$');
        this.MasterCard = new RegExp('^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$');
    }
    return CreditCardRegex;
}());
var RegExRule = {
    alpha: /^[a-zA-Z]+$/,
    alphaExits: /[a-zA-Z]/,
    alphaWithSpace: /^[a-zA-Z\s]+$/,
    macId: /^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/,
    onlyDigit: /^[0-9]+$/,
    isDigitExits: /[0-9]/,
    lowerCase: /[a-z]/,
    upperCase: /[A-Z]/,
    specialCharacter: /[!@#$%^&*(),.?":{}|<>]/,
    advancedEmail: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
    basicEmail: /^(([^<>()\[\]\\.,,:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    alphaNumeric: /^[0-9a-zA-Z]+$/,
    alphaNumericWithSpace: /^[0-9a-zA-Z\s]+$/,
    hexColor: /^#?([0-9A-F]{3}|[0-9A-F]{6})$/i,
    strictHexColor: /^#?([0-9A-F]{3}|[0-9A-F]{6})$/i,
    float: /^(?:[-+]?(?:[0-9]+))?(?:\.[0-9]*)?(?:[eE][\+\-]?(?:[0-9]+))?$/,
    decimal: /^[-+]?([0-9]+|\.[0-9]+|[0-9]+\.[0-9]+)$/,
    hexaDecimal: /^[0-9A-F]+$/i,
    date: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
    time: /^(00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9])$/,
    timeWithSeconds: /^(00|[0-9]|1[0-9]|2[0-3]):([0-9]|[0-5][0-9]):([0-9]|[0-5][0-9])$/,
    url: /^(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})$/,
    creditCard: new CreditCardRegex(),
    ascii: /^[\x00-\x7F]+$/,
    dataUri: /^data:([a-z]+\/[a-z0-9-+.]+(;[a-z0-9-.!#$%*+.{}|~`]+=[a-z0-9-.!#$%*+.{}|~`]+)*)?(;base64)?,([a-z0-9!$&',()*+;=\-._~:@\/?%\s]*?)$/i,
    lat: /^\(?[+-]?(90(\.0+)?|[1-8]?\d(\.\d+)?)$/,
    long: /^\s?[+-]?(180(\.0+)?|1[0-7]\d(\.\d+)?|\d{1,2}(\.\d+)?)\)?$/,
    ipV4: /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/,
    ipV6: /^((?:[a-fA-F\d]{1,4}:){7}(?:[a-fA-F\d]{1,4}|:)|(?:[a-fA-F\d]{1,4}:){6}(?:(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|:[a-fA-F\d]{1,4}|:)|(?:[a-fA-F\d]{1,4}:){5}(?::(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|(:[a-fA-F\d]{1,4}){1,2}|:)|(?:[a-fA-F\d]{1,4}:){4}(?:(:[a-fA-F\d]{1,4}){0,1}:(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|(:[a-fA-F\d]{1,4}){1,3}|:)|(?:[a-fA-F\d]{1,4}:){3}(?:(:[a-fA-F\d]{1,4}){0,2}:(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|(:[a-fA-F\d]{1,4}){1,4}|:)|(?:[a-fA-F\d]{1,4}:){2}(?:(:[a-fA-F\d]{1,4}){0,3}:(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|(:[a-fA-F\d]{1,4}){1,5}|:)|(?:[a-fA-F\d]{1,4}:){1}(?:(:[a-fA-F\d]{1,4}){0,4}:(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|(:[a-fA-F\d]{1,4}){1,6}|:)|(?::((?::[a-fA-F\d]{1,4}){0,5}:(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)(?:\.(?:25[0-5]|2[0-4]\d|1\d\d|[1-9]\d|\d)){3}|(?::[a-fA-F\d]{1,4}){1,7}|:)))(%[0-9a-zA-Z]{1,})?$/,
    cidrV4: /^(3[0-2]|[12]?[0-9])$/,
    cidrV6: /^(12[0-8]|1[01][0-9]|[1-9]?[0-9])$/,
    cusip: /^[0-9A-Z]{9}$/,
    grid: /^[GRID:]*([0-9A-Z]{2})[-\s]*([0-9A-Z]{5})[-\s]*([0-9A-Z]{10})[-\s]*([0-9A-Z]{1})$/g
};
var ALPHABET = "alphabet";
var DIGIT = "digit";
var CONTAINS = "contains";
var LOWERCASE = "lowerCase";
var UPPERCASE = "upperCase";
var SPECIAL_CHARACTER = "specialCharacter";
var MIN_LENGTH = "minLength";
var MAX_LENGTH = "maxLength";
var RegexValidator = /** @class */ (function () {
    function RegexValidator() {
    }
    /**
     * @param {?} value
     * @param {?} regex
     * @return {?}
     */
    RegexValidator.isExits = function (value, regex) {
        return value.match(regex) != null;
    };
    /**
     * @param {?} value
     * @param {?} regex
     * @return {?}
     */
    RegexValidator.isValid = function (value, regex) {
        return regex.test(value);
    };
    /**
     * @param {?} value
     * @param {?=} isRemoveSpace
     * @return {?}
     */
    RegexValidator.isNotBlank = function (value, isRemoveSpace) {
        if (isRemoveSpace === void 0) { isRemoveSpace = false; }
        return !isRemoveSpace ?
            (value === 0) || (value != undefined && value != null && value != "") :
            (value === 0) || (value != undefined && value != null && String(value).trim() != "");
    };
    /**
     * @param {?} passwordValidation
     * @param {?} value
     * @return {?}
     */
    RegexValidator.isValidPassword = function (passwordValidation, value) {
        var /** @type {?} */ isValid = false;
        var /** @type {?} */ keyName = "status";
        var /** @type {?} */ objectProperties = Object.getOwnPropertyNames(passwordValidation);
        for (var _i = 0, objectProperties_1 = objectProperties; _i < objectProperties_1.length; _i++) {
            var propertyName = objectProperties_1[_i];
            switch (propertyName) {
                case ALPHABET:
                    isValid = RegexValidator.isExits(value, RegExRule.alphaExits);
                    keyName = ALPHABET;
                    break;
                case DIGIT:
                    isValid = RegexValidator.isValid(value, RegExRule.isDigitExits);
                    keyName = DIGIT;
                    break;
                case CONTAINS:
                    isValid = value.indexOf(passwordValidation[CONTAINS]) != -1;
                    keyName = CONTAINS;
                    break;
                case LOWERCASE:
                    isValid = RegexValidator.isValid(value, RegExRule.lowerCase);
                    keyName = LOWERCASE;
                    break;
                case UPPERCASE:
                    isValid = RegexValidator.isValid(value, RegExRule.upperCase);
                    keyName = UPPERCASE;
                    break;
                case SPECIAL_CHARACTER:
                    isValid = RegexValidator.isExits(value, RegExRule.specialCharacter);
                    keyName = SPECIAL_CHARACTER;
                    break;
                case MIN_LENGTH:
                    isValid = value.length >= passwordValidation[propertyName];
                    keyName = MIN_LENGTH;
                    break;
                case MAX_LENGTH:
                    isValid = value.length <= passwordValidation[propertyName];
                    keyName = MAX_LENGTH;
                    break;
            }
            if (!isValid)
                break;
        }
        return { isValid: isValid, keyName: keyName };
    };
    /**
     * @param {?} value
     * @return {?}
     */
    RegexValidator.isZero = function (value) {
        return value == 0;
    };
    /**
     * @return {?}
     */
    RegexValidator.commaRegex = function () {
        return new RegExp(",", "g");
    };
    return RegexValidator;
}());
var RxFormArray = /** @class */ (function (_super) {
    __extends(RxFormArray, _super);
    /**
     * @param {?} arrayObject
     * @param {?} controls
     * @param {?=} validatorOrOpts
     * @param {?=} asyncValidator
     */
    function RxFormArray(arrayObject, controls, validatorOrOpts, asyncValidator) {
        var _this = _super.call(this, controls, validatorOrOpts, asyncValidator) || this;
        _this.arrayObject = arrayObject;
        return _this;
    }
    /**
     * @param {?} control
     * @return {?}
     */
    RxFormArray.prototype.push = function (control) {
        var /** @type {?} */ formGroup = this.root;
        if (this.arrayObject)
            if (control.modelInstance)
                this.arrayObject.push(control.modelInstance);
        _super.prototype.push.call(this, control);
        if (formGroup["valueChangedSync"])
            formGroup.valueChangedSync();
    };
    /**
     * @param {?} index
     * @return {?}
     */
    RxFormArray.prototype.removeAt = function (index) {
        var /** @type {?} */ formGroup = this.root;
        this.arrayObject.splice(index, 1);
        _super.prototype.removeAt.call(this, index);
        if (formGroup["valueChangedSync"])
            formGroup.valueChangedSync();
    };
    return RxFormArray;
}(_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]));
var NumericValueType = {};
NumericValueType.PositiveNumber = 1;
NumericValueType.NegativeNumber = 2;
NumericValueType.Both = 3;
NumericValueType[NumericValueType.PositiveNumber] = "PositiveNumber";
NumericValueType[NumericValueType.NegativeNumber] = "NegativeNumber";
NumericValueType[NumericValueType.Both] = "Both";
var IpVersion = {};
IpVersion.V4 = 1;
IpVersion.V6 = 2;
IpVersion.AnyOne = 3;
IpVersion[IpVersion.V4] = "V4";
IpVersion[IpVersion.V6] = "V6";
IpVersion[IpVersion.AnyOne] = "AnyOne";
var ApplicationUtil = /** @class */ (function () {
    function ApplicationUtil() {
    }
    /**
     * @param {?} control
     * @return {?}
     */
    ApplicationUtil.getParentObjectValue = function (control) {
        if (control.parent) {
            var /** @type {?} */ parent = this.parentObjectValue(control.parent);
            return parent.value;
        }
        return {};
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ApplicationUtil.getParentControl = function (control) {
        if (control.parent) {
            var /** @type {?} */ parent = this.parentObjectValue(control.parent);
            return parent;
        }
        return control;
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ApplicationUtil.getFormControlName = function (control) {
        var /** @type {?} */ controlName = '';
        if (control.parent) {
            for (var /** @type {?} */ formControlName in control.parent.controls) {
                if (control.parent.controls[formControlName] == control) {
                    controlName = formControlName;
                    break;
                }
            }
        }
        return controlName;
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ApplicationUtil.getParentFormArray = function (control) {
        if (control.parent && !(control.parent instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"] || control.parent instanceof RxFormArray)) {
            var /** @type {?} */ parent = this.getParentFormArray(control.parent);
            return parent;
        }
        return control.parent;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    ApplicationUtil.toLower = function (value) {
        if (value)
            return String(value).toLowerCase();
        return value;
    };
    /**
     * @param {?} fieldName
     * @param {?} control
     * @return {?}
     */
    ApplicationUtil.getFormControl = function (fieldName, control) {
        var /** @type {?} */ splitText = fieldName.split('.');
        if (splitText.length > 1 && control.parent) {
            var /** @type {?} */ formControl = this.getParentControl(control);
            splitText.forEach(function (name, index) { formControl = formControl.controls[name]; });
            return formControl;
        }
        return (control.parent) ? control.parent.get([fieldName]) : undefined;
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ApplicationUtil.parentObjectValue = function (control) {
        if (!control.parent)
            return control;
        else
            control = this.parentObjectValue(control.parent);
        return control;
    };
    /**
     * @param {?} config
     * @return {?}
     */
    ApplicationUtil.getConfigObject = function (config) {
        return (config != undefined && config != true) ? config : {};
    };
    /**
     * @param {?} value
     * @return {?}
     */
    ApplicationUtil.isNumeric = function (value) {
        return (value - parseFloat(value) + 1) >= 0;
    };
    /**
     * @param {?} primaryValue
     * @param {?} secondaryValue
     * @return {?}
     */
    ApplicationUtil.notEqualTo = function (primaryValue, secondaryValue) {
        var /** @type {?} */ firstValue = (primaryValue == undefined || primaryValue == null) ? "" : primaryValue;
        var /** @type {?} */ secondValue = (secondaryValue == undefined || secondaryValue == null) ? "" : secondaryValue;
        return (firstValue != secondValue);
    };
    /**
     * @param {?} allowDecimal
     * @param {?} acceptValue
     * @return {?}
     */
    ApplicationUtil.numericValidation = function (allowDecimal, acceptValue) {
        acceptValue = (acceptValue == undefined) ? NumericValueType.PositiveNumber : acceptValue;
        var /** @type {?} */ regex = /^[0-9]+$/;
        switch (acceptValue) {
            case NumericValueType.PositiveNumber:
                regex = (!allowDecimal) ? /^[0-9]+$/ : /^[0-9\.]+$/;
                break;
            case NumericValueType.NegativeNumber:
                regex = (!allowDecimal) ? /^[-][0-9]+$/ : /^[-][0-9\.]+$/;
                break;
            case NumericValueType.Both:
                regex = (!allowDecimal) ? /^[-|+]?[0-9]+$/ : /^[-|+]?[0-9\.]+$/;
                break;
        }
        return regex;
    };
    /**
     * @param {?} control
     * @param {?} config
     * @param {?} type
     * @return {?}
     */
    ApplicationUtil.configureControl = function (control, config, type) {
        if (config) {
            if (!control.validatorConfig) {
                var /** @type {?} */ jObject = {};
                jObject[type] = config;
                Object.assign(control, { validatorConfig: jObject });
            }
            else
                control.validatorConfig[type] = config;
        }
    };
    return ApplicationUtil;
}());
var FormProvider = /** @class */ (function () {
    function FormProvider() {
    }
    /**
     * @param {?} control
     * @param {?} config
     * @return {?}
     */
    FormProvider.ProcessRule = function (control, config) {
        var /** @type {?} */ formGroupValue = ApplicationUtil.getParentObjectValue(control);
        var /** @type {?} */ parentObject = (control.parent) ? control.parent.value : undefined;
        return Linq.IsPassed(formGroupValue, config.conditionalExpression, parentObject);
    };
    return FormProvider;
}());
var ValidatorValueChecker = /** @class */ (function () {
    function ValidatorValueChecker() {
    }
    /**
     * @param {?} control
     * @param {?} config
     * @return {?}
     */
    ValidatorValueChecker.pass = function (control, config) {
        if (FormProvider.ProcessRule(control, config))
            return RegexValidator.isNotBlank(control.value);
        else
            return false;
    };
    /**
     * @param {?} control
     * @param {?} config
     * @return {?}
     */
    ValidatorValueChecker.passArrayValue = function (control, config) {
        if (FormProvider.ProcessRule(control, config))
            return control.value instanceof Array;
        else
            return false;
    };
    return ValidatorValueChecker;
}());
/**
 * @param {?} config
 * @return {?}
 */
function alphaValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ isValid = (!config || !config.allowWhiteSpace) ?
                RegexValidator.isValid(control.value, RegExRule.alpha) :
                RegexValidator.isValid(control.value, RegExRule.alphaWithSpace);
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.alpha, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function alphaNumericValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ isValid = (!config || !config.allowWhiteSpace) ?
                RegexValidator.isValid(control.value, RegExRule.alphaNumeric) :
                RegexValidator.isValid(control.value, RegExRule.alphaNumericWithSpace);
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.alphaNumeric, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function compareValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        var /** @type {?} */ compareControl = ApplicationUtil.getFormControl(config.fieldName, control);
        var /** @type {?} */ controlValue = control.value;
        var /** @type {?} */ compareControlValue = (compareControl) ? compareControl.value : '';
        if (RegexValidator.isNotBlank(controlValue) || RegexValidator.isNotBlank(compareControlValue)) {
            if (!(compareControl && compareControl.value === controlValue))
                return ObjectMaker.toJson(AnnotationTypes.compare, config, [controlValue, compareControlValue]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function containsValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (control.value.indexOf(config.value) == -1)
                return ObjectMaker.toJson(AnnotationTypes.contains, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} length
 * @param {?} checks
 * @return {?}
 */
function checkLength(length, checks) {
    var /** @type {?} */ isPassed = false;
    for (var _i = 0, checks_1 = checks; _i < checks_1.length; _i++) {
        var check = checks_1[_i];
        isPassed = (check == length);
        if (isPassed)
            break;
    }
    return isPassed;
}
/**
 * @param {?} config
 * @return {?}
 */
function creditCardValidator(config) {
    return function (control) {
        var /** @type {?} */ controlValue = control.value;
        config = ApplicationUtil.getConfigObject(config);
        var /** @type {?} */ parentObject = (control.parent) ? control.parent.value : undefined;
        if (FormProvider.ProcessRule(control, config)) {
            if (RegexValidator.isNotBlank(controlValue)) {
                var /** @type {?} */ isValid = false;
                var /** @type {?} */ cardTypes = config.fieldName && parentObject[config.fieldName] ? [parentObject[config.fieldName]] : config.creditCardTypes;
                var /** @type {?} */ cardType = '';
                for (var _i = 0, cardTypes_1 = cardTypes; _i < cardTypes_1.length; _i++) {
                    var creditCardType = cardTypes_1[_i];
                    switch (creditCardType) {
                        case "AmericanExpress":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.AmericanExpress) && checkLength(controlValue.length, [15]);
                            break;
                        case "DinersClub":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.DinersClub) && checkLength(controlValue.length, [14, 16, 19]);
                            break;
                        case "Discover":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.Discover) && checkLength(controlValue.length, [16, 19]);
                            break;
                        case "JCB":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.JCB) && checkLength(controlValue.length, [16, 19]);
                            break;
                        case "Maestro":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.Maestro) && checkLength(controlValue.length, [12, 16, 19]);
                            break;
                        case "MasterCard":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.MasterCard) && checkLength(controlValue.length, [16]);
                            break;
                        case "Visa":
                            isValid = RegexValidator.isValid(controlValue, RegExRule.creditCard.Visa) && checkLength(controlValue.length, [13, 16, 19]);
                            break;
                    }
                    cardType = creditCardType;
                }
                if (!isValid)
                    return ObjectMaker.toJson(AnnotationTypes.creditCard, config, [controlValue, cardType]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function digitValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.onlyDigit))
                return ObjectMaker.toJson(AnnotationTypes.digit, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
var DecoratorName = /** @class */ (function () {
    function DecoratorName() {
    }
    return DecoratorName;
}());
DecoratorName.alpha = "alpha";
DecoratorName.alphaNumeric = "alpha-numeric";
DecoratorName.compare = "compare";
DecoratorName.contains = "contains";
DecoratorName.crediCard = "credit-card";
DecoratorName.email = "email";
DecoratorName.hexColor = "hex-color";
DecoratorName.lowerCase = "lower-case";
DecoratorName.maxDate = "max-date";
DecoratorName.minDate = "min-date";
DecoratorName.maxLength = "max-length";
DecoratorName.maxNumber = "max-number";
DecoratorName.minNumber = "min-number";
DecoratorName.minLength = "min-length";
DecoratorName.password = "password";
DecoratorName.pattern = "pattern";
DecoratorName.range = "range";
DecoratorName.required = "required";
DecoratorName.upperCase = "upper-case";
DecoratorName.digit = "digit";
DecoratorName.nested = "nested";
var DateProvider = /** @class */ (function () {
    function DateProvider() {
    }
    /**
     * @param {?} dateFormat
     * @return {?}
     */
    DateProvider.prototype.getRegex = function (dateFormat) {
        var /** @type {?} */ regExp;
        switch (dateFormat) {
            case 'ymd':
                regExp = "^(?:[0-9]{4})-(3[01]|[12][0-9]|0?[1-9])-(1[0-2]|0?[1-9])$";
                break;
            case 'dmy':
                regExp = "^(3[01]|[12][0-9]|0?[1-9])-(1[0-2]|0?[1-9])-(?:[0-9]{2})?[0-9]{2}$";
                break;
            case 'mdy':
                regExp = "^(1[0-2]|0?[1-9])-(3[01]|[12][0-9]|0?[1-9])-(?:[0-9]{2})?[0-9]{2}$";
                break;
        }
        return new RegExp(regExp);
    };
    /**
     * @return {?}
     */
    DateProvider.prototype.regex = function () {
        var /** @type {?} */ regExp;
        if (ReactiveFormConfig && ReactiveFormConfig.json && ReactiveFormConfig.json.internationalization && ReactiveFormConfig.json.internationalization.dateFormat && ReactiveFormConfig.json.internationalization.seperator)
            regExp = this.getRegex(ReactiveFormConfig.json.internationalization.dateFormat);
        else
            regExp = (ReactiveFormConfig && ReactiveFormConfig.json && ReactiveFormConfig.json.baseConfig && ReactiveFormConfig.json.baseConfig.dateFormat) ? this.getRegex(ReactiveFormConfig.json.baseConfig.dateFormat) : this.getRegex("mdy");
        return regExp;
    };
    /**
     * @param {?} value
     * @param {?=} isBaseFormat
     * @return {?}
     */
    DateProvider.prototype.getDate = function (value, isBaseFormat) {
        if (isBaseFormat === void 0) { isBaseFormat = false; }
        var _a, _b, _c;
        var /** @type {?} */ year, /** @type {?} */ month, /** @type {?} */ day;
        if (!(value instanceof Date)) {
            var /** @type {?} */ seperator = ReactiveFormConfig && ReactiveFormConfig.json && ReactiveFormConfig.json.baseConfig && ReactiveFormConfig.json.baseConfig.seperator ? ReactiveFormConfig.json.baseConfig.seperator : "/";
            var /** @type {?} */ dateFormat = ReactiveFormConfig && ReactiveFormConfig.json && ReactiveFormConfig.json.baseConfig && ReactiveFormConfig.json.baseConfig.dateFormat ? ReactiveFormConfig.json.baseConfig.dateFormat : "mdy";
            if (!isBaseFormat && ReactiveFormConfig && ReactiveFormConfig.json && ReactiveFormConfig.json.internationalization && ReactiveFormConfig.json.internationalization.dateFormat && ReactiveFormConfig.json.internationalization.seperator) {
                seperator = ReactiveFormConfig.json.internationalization.seperator;
                dateFormat = ReactiveFormConfig.json.internationalization.dateFormat;
            }
            switch (dateFormat) {
                case 'ymd':
                    _a = value.split(seperator).map(function (val) { return +val; }), year = _a[0], month = _a[1], day = _a[2];
                    break;
                case 'dmy':
                    _b = value.split(seperator).map(function (val) { return +val; }), day = _b[0], month = _b[1], year = _b[2];
                    break;
                case 'mdy':
                    _c = value.split(seperator).map(function (val) { return +val; }), month = _c[0], day = _c[1], year = _c[2];
                    break;
            }
            return new Date(year, month - 1, day);
        }
        else
            return value;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    DateProvider.prototype.isValid = function (value) {
        var /** @type {?} */ seperator = '/';
        if (ReactiveFormConfig.json && ReactiveFormConfig.json.internationalization && ReactiveFormConfig.json.internationalization.seperator)
            seperator = ReactiveFormConfig.json.internationalization.seperator;
        value = value.replace(seperator, '-').replace(seperator, '-');
        return this.regex().test(value);
    };
    /**
     * @param {?} config
     * @return {?}
     */
    DateProvider.prototype.getConfigDateValue = function (config) {
        var /** @type {?} */ date = config.value;
        if (config.value && typeof config.value == "string") {
            date = this.getDate(config.value, true);
        }
        return date;
    };
    /**
     * @param {?} config
     * @param {?} control
     * @return {?}
     */
    DateProvider.prototype.getCompareDate = function (config, control) {
        var /** @type {?} */ date = this.getConfigDateValue(config);
        if (config.fieldName) {
            var /** @type {?} */ checkControl = ApplicationUtil.getFormControl(config.fieldName, control);
            if (checkControl && checkControl.value) {
                date = this.getDate(checkControl.value);
            }
        }
        return date;
    };
    return DateProvider;
}());
/**
 * @param {?} config
 * @return {?}
 */
function emailValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.basicEmail))
                return ObjectMaker.toJson(AnnotationTypes.email, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function hexColorValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.strictHexColor))
                return ObjectMaker.toJson(AnnotationTypes.hexColor, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function lowercaseValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(control.value === control.value.toLowerCase()))
                return ObjectMaker.toJson(AnnotationTypes.lowerCase, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} control
 * @param {?} config
 * @param {?} operationType
 * @return {?}
 */
function dateChecker(control, config, operationType) {
    config = ApplicationUtil.getConfigObject(config);
    var /** @type {?} */ dateProvider = new DateProvider();
    if (FormProvider.ProcessRule(control, config)) {
        if (RegexValidator.isNotBlank(control.value)) {
            if (dateProvider.isValid(control.value)) {
                var /** @type {?} */ checkDate = dateProvider.getCompareDate(config, control);
                var /** @type {?} */ currentControlValue = dateProvider.getDate(control.value);
                var /** @type {?} */ isValid = operationType == AnnotationTypes.minDate ? (currentControlValue >= checkDate) : (checkDate >= currentControlValue);
                if (!isValid)
                    return ObjectMaker.toJson(operationType, config, [control.value]);
            }
            else
                return ObjectMaker.toJson(operationType, config, [control.value]);
        }
    }
    return ObjectMaker.null();
}
/**
 * @param {?} config
 * @return {?}
 */
function maxDateValidator(config) {
    return function (control) {
        return dateChecker(control, config, AnnotationTypes.maxDate);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function maxLengthValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(control.value.length <= config.value))
                return ObjectMaker.toJson(AnnotationTypes.maxLength, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function maxNumberValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(parseFloat(control.value) <= config.value))
                return ObjectMaker.toJson(AnnotationTypes.maxNumber, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function minDateValidator(config) {
    return function (control) {
        return dateChecker(control, config, AnnotationTypes.minDate);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function minLengthValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(String(control.value).length >= config.value))
                return ObjectMaker.toJson(AnnotationTypes.minLength, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function minNumberValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(parseFloat(control.value) >= config.value))
                return ObjectMaker.toJson(AnnotationTypes.minNumber, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function passwordValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        var /** @type {?} */ controlValue = control.value;
        if (RegexValidator.isNotBlank(controlValue)) {
            var /** @type {?} */ validation = RegexValidator.isValidPassword(config.validation, controlValue);
            if (!validation.isValid)
                return ObjectMaker.toJson(AnnotationTypes.password, config, [controlValue]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function rangeValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(String(control.value).indexOf(".") == -1 && parseInt(control.value) >= config.minimumNumber && parseInt(control.value) <= config.maximumNumber))
                return ObjectMaker.toJson(AnnotationTypes.range, config, [control.value, config.minimumNumber, config.maximumNumber]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?=} config
 * @return {?}
 */
function uppercaseValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(control.value === control.value.toUpperCase()))
                return ObjectMaker.toJson(AnnotationTypes.upperCase, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function requiredValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            if (!RegexValidator.isNotBlank(control.value)) {
                return ObjectMaker.toJson(AnnotationTypes.required, config, []);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function patternValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            for (var /** @type {?} */ pattern in config.expression)
                if (!(RegexValidator.isValid(control.value, config.expression[pattern])))
                    return ObjectMaker.toJson(pattern, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function timeValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ isValid = config.allowSeconds ? RegexValidator.isValid(control.value, RegExRule.timeWithSeconds) : RegexValidator.isValid(control.value, RegExRule.time);
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.time, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function urlValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.url))
                return ObjectMaker.toJson(AnnotationTypes.url, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function jsonValidator(config) {
    /**
     * @param {?} value
     * @return {?}
     */
    function process(value) {
        var /** @type {?} */ result = false;
        try {
            var /** @type {?} */ json = JSON.parse(value);
            result = !!json && typeof json === 'object';
        }
        catch ( /** @type {?} */ex) {
            result = false;
        }
        return result;
    }
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (process(control.value))
                return ObjectMaker.toJson(AnnotationTypes.json, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} control
 * @param {?} config
 * @param {?} relationalOperatorName
 * @return {?}
 */
function relationalCheck(control, config, relationalOperatorName) {
    config = ApplicationUtil.getConfigObject(config);
    var /** @type {?} */ matchControl = ApplicationUtil.getFormControl(config.fieldName, control);
    var /** @type {?} */ matchControlValue = (matchControl) ? matchControl.value : '';
    if (FormProvider.ProcessRule(control, config)) {
        if (RegexValidator.isNotBlank(control.value) && RegexValidator.isNotBlank(matchControlValue)) {
            var /** @type {?} */ isValid = false;
            switch (relationalOperatorName) {
                case AnnotationTypes.greaterThan:
                    isValid = parseFloat(control.value) > parseFloat(matchControlValue);
                    break;
                case AnnotationTypes.lessThan:
                    isValid = parseFloat(control.value) < parseFloat(matchControlValue);
                    break;
                case AnnotationTypes.greaterThanEqualTo:
                    isValid = parseFloat(control.value) >= parseFloat(matchControlValue);
                    break;
                case AnnotationTypes.lessThanEqualTo:
                    isValid = parseFloat(control.value) <= parseFloat(matchControlValue);
                    break;
            }
            if (!isValid)
                return ObjectMaker.toJson(relationalOperatorName, config, [control.value, matchControlValue]);
        }
    }
    return ObjectMaker.null();
}
/**
 * @param {?} config
 * @return {?}
 */
function greaterThanValidator(config) {
    return function (control) {
        return relationalCheck(control, config, AnnotationTypes.greaterThan);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function greaterThanEqualToValidator(config) {
    return function (control) {
        return relationalCheck(control, config, AnnotationTypes.greaterThanEqualTo);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function lessThanEqualToValidator(config) {
    return function (control) {
        return relationalCheck(control, config, AnnotationTypes.lessThanEqualTo);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function lessThanValidator(config) {
    return function (control) {
        return relationalCheck(control, config, AnnotationTypes.lessThan);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function choiceValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            if (control.value instanceof Array) {
                config.minLength = (config.minLength == undefined) ? 0 : config.minLength;
                config.maxLength = (config.maxLength == undefined) ? 0 : config.maxLength;
                if (!((config.minLength <= control.value.length && config.maxLength == 0) || (config.maxLength >= control.value.length)))
                    return ObjectMaker.toJson(AnnotationTypes.choice, config, [control.value]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function differentValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ differentControl = ApplicationUtil.getFormControl(config.fieldName, control);
            var /** @type {?} */ differentControlValue = (differentControl) ? differentControl.value : '';
            if (!(differentControl && differentControl.value != control.value))
                return ObjectMaker.toJson(AnnotationTypes.different, config, [control.value, differentControlValue]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function numericValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, ApplicationUtil.numericValidation(config.allowDecimal, config.acceptValue)))
                return ObjectMaker.toJson(AnnotationTypes.numeric, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function evenValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(control.value % 2 == 0))
                return ObjectMaker.toJson(AnnotationTypes.even, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function oddValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!(!(control.value % 2 == 0)) || !ApplicationUtil.isNumeric(control.value))
                return ObjectMaker.toJson(AnnotationTypes.odd, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function factorValidator(config) {
    /**
     * @param {?} dividend
     * @param {?} value
     * @return {?}
     */
    function positiveFactors(dividend, value) {
        var /** @type {?} */ isPositive = false;
        var /** @type {?} */ index = 1;
        for (index = 1; index <= Math.floor(Math.sqrt(dividend)); index += 1) {
            if (dividend % index === 0) {
                if (index == value)
                    isPositive = true;
                if (dividend / index !== index)
                    if ((dividend / index) == value)
                        isPositive = true;
                if (isPositive)
                    break;
            }
        }
        return isPositive;
    }
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        var /** @type {?} */ dividendField = (control.parent && config.fieldName) ? ApplicationUtil.getFormControl(config.fieldName, control) : undefined;
        var /** @type {?} */ dividend = (config.fieldName && dividendField) ? dividendField.value : config.dividend;
        if (FormProvider.ProcessRule(control, config)) {
            if (RegexValidator.isNotBlank(control.value) && dividend > 0) {
                if (!RegexValidator.isValid(control.value, RegExRule.onlyDigit) || !positiveFactors(dividend, parseInt(control.value)))
                    return ObjectMaker.toJson(AnnotationTypes.factor, config, [control.value]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function leapYearValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ isValid = (control.value % 100 === 0) ? (control.value % 400 === 0) : (control.value % 4 === 0);
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.leapYear, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function allOfValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.passArrayValue(control, config)) {
            var /** @type {?} */ testResult = false;
            var _loop_1 = function (value) {
                testResult = control.value.some(function (y) { return y == value; });
                if (!testResult)
                    return "break";
            };
            for (var _i = 0, _a = config.matchValues; _i < _a.length; _i++) {
                var value = _a[_i];
                var state_1 = _loop_1(/** @type {?} */ value);
                if (state_1 === "break")
                    break;
            }
            if (!testResult)
                return ObjectMaker.toJson(AnnotationTypes.allOf, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function oneOfValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.passArrayValue(control, config)) {
            var /** @type {?} */ testResult = false;
            var _loop_2 = function (value) {
                testResult = control.value.some(function (y) { return y == value; });
                if (testResult)
                    return "break";
            };
            for (var _i = 0, _a = config.matchValues; _i < _a.length; _i++) {
                var value = _a[_i];
                var state_2 = _loop_2(/** @type {?} */ value);
                if (state_2 === "break")
                    break;
            }
            if (!testResult)
                return ObjectMaker.toJson(AnnotationTypes.oneOf, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function noneOfValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.passArrayValue(control, config)) {
            var /** @type {?} */ testResult = false;
            var _loop_3 = function (value) {
                testResult = control.value.some(function (y) { return y == value; });
                if (testResult)
                    return "break";
            };
            for (var _i = 0, _a = config.matchValues; _i < _a.length; _i++) {
                var value = _a[_i];
                var state_3 = _loop_3(/** @type {?} */ value);
                if (state_3 === "break")
                    break;
            }
            if (testResult)
                return ObjectMaker.toJson(AnnotationTypes.noneOf, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function macValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.macId))
                return ObjectMaker.toJson(AnnotationTypes.mac, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function asciiValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.ascii))
                return ObjectMaker.toJson(AnnotationTypes.ascii, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function dataUriValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.dataUri))
                return ObjectMaker.toJson(AnnotationTypes.dataUri, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function portValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ isValid = RegexValidator.isValid(control.value, RegExRule.onlyDigit) && (control.value >= 0 && control.value <= 65535);
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.port, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function latLongValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ splitText = control.value.split(',');
            if (!(splitText.length > 1 && RegexValidator.isValid(splitText[0], RegExRule.lat) && RegexValidator.isValid(splitText[1], RegExRule.long)))
                return ObjectMaker.toJson(AnnotationTypes.latLong, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function extensionValidator(config) {
    return function (control, files) {
        config = ApplicationUtil.getConfigObject(config);
        if (!control["validatorConfig"] || !control["validatorConfig"]["extension"])
            ApplicationUtil.configureControl(control, config, AnnotationTypes.extension);
        if (files && FormProvider.ProcessRule(control, config)) {
            if (RegexValidator.isNotBlank(control.value)) {
                var /** @type {?} */ testResult = true;
                var /** @type {?} */ extension_1 = '';
                for (var /** @type {?} */ i = 0; i < files.length; i++) {
                    var /** @type {?} */ file_1 = files.item(i);
                    var /** @type {?} */ splitText = file_1.name.split(".");
                    extension_1 = splitText[splitText.length - 1];
                    var /** @type {?} */ result = config.extensions.filter(function (t) { return extension_1.toLowerCase() == t.toLowerCase(); })[0];
                    if (!result) {
                        testResult = false;
                        break;
                    }
                }
                if (!testResult)
                    return ObjectMaker.toJson(AnnotationTypes.extension, config, [extension_1, config.extensions.join(",")]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function fileSizeValidator(config) {
    return function (control, files) {
        config = ApplicationUtil.getConfigObject(config);
        if (!control["validatorConfig"] || !control["validatorConfig"]["fileSize"])
            ApplicationUtil.configureControl(control, config, AnnotationTypes.fileSize);
        if (files && FormProvider.ProcessRule(control, config)) {
            if (RegexValidator.isNotBlank(control.value)) {
                var /** @type {?} */ minFileSize = config.minSize ? config.minSize : 0;
                var /** @type {?} */ testResult = false;
                var /** @type {?} */ fileSize_1 = 0;
                for (var /** @type {?} */ i = 0; i < files.length; i++) {
                    var /** @type {?} */ file_2 = files.item(i);
                    fileSize_1 = file_2.size;
                    testResult = (!(fileSize_1 >= minFileSize && fileSize_1 <= config.maxSize));
                    if (testResult)
                        break;
                }
                if (testResult)
                    return ObjectMaker.toJson(AnnotationTypes.fileSize, config, [fileSize_1, config.maxSize]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function endsWithValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ endString = String(control.value).substr(control.value.length - config.value.length, config.value.length);
            if (endString != config.value)
                return ObjectMaker.toJson(AnnotationTypes.endsWith, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function startsWithValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ startString = String(control.value).substr(0, config.value.length);
            if (startString != config.value)
                return ObjectMaker.toJson(AnnotationTypes.startsWith, config, [control.value, config.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function primeNumberValidator(config) {
    /**
     * @param {?} value
     * @return {?}
     */
    function isPrime(value) {
        var /** @type {?} */ isPrimeNumber = value != 1;
        for (var /** @type {?} */ i = 2; i < value; i++) {
            if (value % i == 0) {
                isPrimeNumber = false;
                break;
            }
        }
        return isPrimeNumber;
    }
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!ApplicationUtil.isNumeric(control.value) || !isPrime(control.value))
                return ObjectMaker.toJson(AnnotationTypes.primeNumber, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function latitudeValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.lat))
                return ObjectMaker.toJson(AnnotationTypes.latitude, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function longitudeValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            if (!RegexValidator.isValid(control.value, RegExRule.long))
                return ObjectMaker.toJson(AnnotationTypes.longitude, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function composeValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            if (config.validators) {
                var /** @type {?} */ result = undefined;
                for (var _i = 0, _a = config.validators; _i < _a.length; _i++) {
                    var validator = _a[_i];
                    result = validator(control);
                    if (result)
                        break;
                }
                if (result)
                    return ObjectMaker.toJson(config.messageKey || AnnotationTypes.compose, config, [control.value]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @param {?} entity
 * @return {?}
 */
function ruleValidator(config, entity) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            var /** @type {?} */ result = null;
            for (var _i = 0, _a = config.customRules; _i < _a.length; _i++) {
                var rule_1 = _a[_i];
                result = rule_1(entity);
                if (result)
                    break;
            }
            if (result)
                return result;
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function fileValidator(config) {
    return function (control, files) {
        config = ApplicationUtil.getConfigObject(config);
        if (!control["validatorConfig"] || !control["validatorConfig"]["file"])
            ApplicationUtil.configureControl(control, config, AnnotationTypes.file);
        if (FormProvider.ProcessRule(control, config)) {
            if (RegexValidator.isNotBlank(control.value)) {
                var /** @type {?} */ minFiles = config.minFiles ? config.minFiles : 0;
                var /** @type {?} */ maxFiles = config.maxFiles ? config.maxFiles : files.length;
                if (!(files.length > 0 && files[0] instanceof File && files.length >= minFiles && files.length <= maxFiles))
                    return ObjectMaker.toJson(AnnotationTypes.file, config, [files.length, minFiles, maxFiles]);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function customValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            var /** @type {?} */ formGroupValue = ApplicationUtil.getParentObjectValue(control);
            var /** @type {?} */ parentObject = (control.parent) ? control.parent.value : undefined;
            var /** @type {?} */ result = null;
            for (var _i = 0, _a = config.customRules; _i < _a.length; _i++) {
                var rule_2 = _a[_i];
                result = rule_2(formGroupValue, parentObject, config.additionalValue);
                if (result)
                    break;
            }
            if (result)
                return result;
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function uniqueValidator(config) {
    var /** @type {?} */ setTimeout = function (invalidateControls, controlValues) {
        var /** @type {?} */ timeOut = window.setTimeout(function () {
            invalidateControls.forEach(function (t) {
                var /** @type {?} */ isMatched = controlValues.filter(function (x) { return x == t.value; })[0];
                if (!isMatched)
                    t.updateValueAndValidity();
            });
            window.clearTimeout(timeOut);
        }, 200);
    };
    var /** @type {?} */ additionalValidation = function (config, fieldName, formGroup, formArray, currentValue) {
        var /** @type {?} */ indexOf = formArray.controls.indexOf(formGroup);
        var /** @type {?} */ formArrayValue = [];
        if (indexOf != -1) {
            formArray.value.forEach(function (t, i) {
                if (indexOf != i)
                    formArrayValue.push(t);
            });
            return config.additionalValidation(currentValue, indexOf, fieldName, formGroup.value, formArrayValue);
        }
        return false;
    };
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            if (RegexValidator.isNotBlank(control.value)) {
                var /** @type {?} */ formArray = ApplicationUtil.getParentFormArray(control);
                var /** @type {?} */ parentFormGroup = control.parent ? control.parent : undefined;
                var /** @type {?} */ invalidateControls = [];
                var /** @type {?} */ controlValues = [];
                if (formArray && parentFormGroup) {
                    var /** @type {?} */ currentValue = control.value;
                    var /** @type {?} */ fieldName_1 = ApplicationUtil.getFormControlName(control);
                    var /** @type {?} */ isMatched = false;
                    var _loop_4 = function (formGroup) {
                        if (formGroup != parentFormGroup) {
                            isMatched = (ApplicationUtil.toLower(formGroup.controls[fieldName_1].value) == ApplicationUtil.toLower(currentValue) && !(formGroup.controls[fieldName_1].errors && formGroup.controls[fieldName_1].errors[AnnotationTypes.unique]));
                            if (formGroup.controls[fieldName_1].errors && formGroup.controls[fieldName_1].errors[AnnotationTypes.unique]) {
                                matchedControl = formArray.controls.filter(function (t) { return t.controls[fieldName_1] != formGroup.controls[fieldName_1] && ApplicationUtil.toLower(t.controls[fieldName_1].value) == ApplicationUtil.toLower(formGroup.controls[fieldName_1].value); })[0];
                                if (!matchedControl)
                                    invalidateControls.push(formGroup.controls[fieldName_1]);
                            }
                            else
                                controlValues.push(formGroup.controls[fieldName_1].value);
                        }
                        if (isMatched)
                            return "break";
                    };
                    var matchedControl;
                    for (var _i = 0, _a = formArray.controls; _i < _a.length; _i++) {
                        var formGroup = _a[_i];
                        var state_4 = _loop_4(/** @type {?} */ formGroup);
                        if (state_4 === "break")
                            break;
                    }
                    if (invalidateControls.length > 0)
                        setTimeout(invalidateControls, controlValues);
                    var /** @type {?} */ validation = false;
                    if (config.additionalValidation) {
                        validation = additionalValidation(config, fieldName_1, parentFormGroup, formArray, currentValue);
                    }
                    if (isMatched && !validation)
                        return ObjectMaker.toJson(AnnotationTypes.unique, config, [control.value]);
                }
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function imageValidator(config) {
    return function (control, files) {
        config = ApplicationUtil.getConfigObject(config);
        if (!control["validatorConfig"] || !control["validatorConfig"]["image"])
            ApplicationUtil.configureControl(control, config, AnnotationTypes.image);
        if (!files)
            return ObjectMaker.null();
        return new Promise(function (resolve, reject) {
            if (FormProvider.ProcessRule(control, config)) {
                if (RegexValidator.isNotBlank(control.value)) {
                    var /** @type {?} */ testResult_1 = false;
                    var _loop_5 = function () {
                        var /** @type {?} */ file_3 = files.item(i);
                        var /** @type {?} */ type = file_3.type ? file_3.type.split('/') : [];
                        testResult_1 = type.length > 1 && type[0] == "image";
                        if (!testResult_1)
                            return "break";
                        var /** @type {?} */ image_1 = new Image();
                        config.minWidth = config.minWidth ? config.minWidth : 0;
                        config.minHeight = config.minHeight ? config.minHeight : 0;
                        image_1.onload = function () {
                            testResult_1 = (image_1.width >= config.minWidth && image_1.height >= config.minHeight) && (image_1.width <= config.maxWidth && image_1.height <= config.maxHeight);
                            if (!testResult_1)
                                resolve(ObjectMaker.toJson(AnnotationTypes.image, config, [image_1.width, image_1.height]));
                            else
                                resolve(ObjectMaker.null());
                        };
                        image_1.onerror = function () {
                            resolve(ObjectMaker.toJson(AnnotationTypes.image, config, []));
                        };
                        image_1.src = URL.createObjectURL(file_3);
                    };
                    for (var /** @type {?} */ i = 0; i < files.length; i++) {
                        var state_5 = _loop_5();
                        if (state_5 === "break")
                            break;
                    }
                    if (!testResult_1)
                        resolve(ObjectMaker.toJson(AnnotationTypes.image, config, []));
                }
            }
            return ObjectMaker.null();
        });
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function notEmptyValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (FormProvider.ProcessRule(control, config)) {
            if (!RegexValidator.isNotBlank(control.value, true)) {
                return ObjectMaker.toJson(AnnotationTypes.notEmpty, config, []);
            }
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} value
 * @return {?}
 */
function checkIpV4(value) {
    var /** @type {?} */ isValid = RegexValidator.isValid(value, RegExRule.ipV4);
    if (isValid) {
        var /** @type {?} */ splitDots = value.split('.');
        for (var _i = 0, splitDots_1 = splitDots; _i < splitDots_1.length; _i++) {
            var ipNum = splitDots_1[_i];
            isValid = ipNum <= 255;
            if (!isValid)
                break;
        }
    }
    return isValid;
}
/**
 * @param {?} value
 * @return {?}
 */
function checkIpV6(value) {
    return RegexValidator.isValid(value, RegExRule.ipV6);
}
/**
 * @param {?} config
 * @return {?}
 */
function ipValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ values = config.isCidr ? control.value.split('/') : [control.value];
            var /** @type {?} */ isValid = (config.version == IpVersion.V4) ?
                checkIpV4(values[0]) :
                (config.version == IpVersion.V6) ?
                    checkIpV6(values[0]) :
                    (checkIpV4(values[0]) || checkIpV6(values[0]));
            if (config.isCidr && isValid) {
                isValid = (values.length > 1) ?
                    config.version == IpVersion.V4 ?
                        RegexValidator.isValid(values[1], RegExRule.cidrV4) :
                        config.version == IpVersion.V6 ?
                            RegexValidator.isValid(values[1], RegExRule.cidrV6) :
                            (RegexValidator.isValid(values[1], RegExRule.cidrV4) || RegexValidator.isValid(values[1], RegExRule.cidrV6)) :
                    false;
            }
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.ip, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function cusipValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ controlValue = control.value.toUpperCase();
            var /** @type {?} */ isValid = RegexValidator.isValid(controlValue, RegExRule.cusip);
            if (isValid) {
                var /** @type {?} */ numericValues = controlValue.split("").map(function (value) {
                    var /** @type {?} */ charCode = value.charCodeAt(0);
                    return charCode >= "A".charCodeAt(0) && charCode <= "Z".charCodeAt(0) ? charCode - "A".charCodeAt(0) + 10 : value;
                });
                var /** @type {?} */ totalCount = 0;
                for (var /** @type {?} */ i = 0; i < numericValues.length - 1; i++) {
                    var /** @type {?} */ numericValue = parseInt(numericValues[i], 10);
                    if (i % 2 !== 0) {
                        numericValue *= 2;
                    }
                    if (numericValue > 9) {
                        numericValue -= 9;
                    }
                    totalCount += numericValue;
                }
                totalCount = (10 - (totalCount % 10)) % 10;
                isValid = totalCount == numericValues[numericValues.length - 1];
            }
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.cusip, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function gridValidator(config) {
    return function (control) {
        config = ApplicationUtil.getConfigObject(config);
        if (ValidatorValueChecker.pass(control, config)) {
            var /** @type {?} */ controlValue = control.value.toUpperCase();
            var /** @type {?} */ isValid = RegexValidator.isValid(controlValue, RegExRule.grid);
            if (isValid) {
                controlValue = controlValue.replace(/\s/g, '').replace(/-/g, '');
                if ('GRID:' === controlValue.substr(0, 5)) {
                    controlValue = controlValue.substr(5);
                }
                var /** @type {?} */ alphaNums = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                var /** @type {?} */ alphaNumLength = alphaNums.length, /** @type {?} */ length = controlValue.length, /** @type {?} */ check = Math.floor(alphaNumLength / 2);
                for (var /** @type {?} */ i = 0; i < length; i++) {
                    check = (((check || alphaNumLength) * 2) % (alphaNumLength + 1) + alphaNums.indexOf(controlValue.charAt(i))) % alphaNumLength;
                }
                isValid = (check === 1);
            }
            if (!isValid)
                return ObjectMaker.toJson(AnnotationTypes.grid, config, [control.value]);
        }
        return ObjectMaker.null();
    };
}
var APP_VALIDATORS = {
    "alphaNumeric": alphaNumericValidator,
    "alpha": alphaValidator,
    "compare": compareValidator,
    "email": emailValidator,
    "hexColor": hexColorValidator,
    "lowerCase": lowercaseValidator,
    "maxDate": maxDateValidator,
    "maxNumber": maxNumberValidator,
    "minDate": minDateValidator,
    "minNumber": minNumberValidator,
    "contains": containsValidator,
    "upperCase": uppercaseValidator,
    "maxLength": maxLengthValidator,
    "minLength": minLengthValidator,
    "password": passwordValidator,
    "range": rangeValidator,
    "required": requiredValidator,
    "creditCard": creditCardValidator,
    "digit": digitValidator,
    "pattern": patternValidator,
    "time": timeValidator,
    "url": urlValidator,
    "json": jsonValidator,
    "greaterThan": greaterThanValidator,
    "greaterThanEqualTo": greaterThanEqualToValidator,
    "lessThan": lessThanValidator,
    "lessThanEqualTo": lessThanEqualToValidator,
    "choice": choiceValidator,
    "different": differentValidator,
    "numeric": numericValidator,
    "even": evenValidator,
    "odd": oddValidator,
    "factor": factorValidator,
    "leapYear": leapYearValidator,
    "allOf": allOfValidator,
    "oneOf": oneOfValidator,
    "noneOf": noneOfValidator,
    "mac": macValidator,
    "ascii": asciiValidator,
    "dataUri": dataUriValidator,
    "port": portValidator,
    "latLong": latLongValidator,
    "extension": extensionValidator,
    "fileSize": fileSizeValidator,
    "endsWith": endsWithValidator,
    "startsWith": startsWithValidator,
    "primeNumber": primeNumberValidator,
    "latitude": latitudeValidator,
    "longitude": longitudeValidator,
    "compose": composeValidator,
    "rule": ruleValidator,
    "file": fileValidator,
    "unique": uniqueValidator,
    "image": imageValidator,
    "notEmpty": notEmptyValidator,
    "ip": ipValidator,
    "cusip": cusipValidator,
    "grid": gridValidator
};
var RxFormControl = /** @class */ (function (_super) {
    __extends(RxFormControl, _super);
    /**
     * @param {?} formState
     * @param {?} validator
     * @param {?} asyncValidator
     * @param {?} entityObject
     * @param {?} baseObject
     * @param {?} controlName
     */
    function RxFormControl(formState, validator, asyncValidator, entityObject, baseObject, controlName) {
        var _this = _super.call(this, formState, validator, asyncValidator) || this;
        _this.entityObject = entityObject;
        _this.baseObject = baseObject;
        _this.keyName = controlName;
        return _this;
    }
    /**
     * @param {?} value
     * @param {?=} options
     * @return {?}
     */
    RxFormControl.prototype.setValue = function (value, options) {
        var _this = this;
        if (options && options.dirty)
            this.baseObject[this.keyName] = value;
        this.entityObject[this.keyName] = value;
        _super.prototype.setValue.call(this, value, options);
        if (this.errors) {
            Object.keys(this.errors).forEach(function (t) {
                _this.parent[CONTROLS_ERROR][_this.keyName] = _this.errorMessage = _this.getErrorMessage(_this.errors, t);
                if (!_this.errorMessage) {
                    var /** @type {?} */ errorObject = ObjectMaker.toJson(t, undefined, [_this.errors[t][t]]);
                    _this.parent[CONTROLS_ERROR][_this.keyName] = _this.errorMessage = _this.getErrorMessage(errorObject, t);
                }
            });
        }
        else {
            this.errorMessage = undefined;
            this.parent[CONTROLS_ERROR][this.keyName] = undefined;
            delete this.parent[CONTROLS_ERROR][this.keyName];
        }
        if (options && !options.updateChanged && this.root[VALUE_CHANGED_SYNC]) {
            this.root[VALUE_CHANGED_SYNC]();
        }
    };
    /**
     * @param {?} errorObject
     * @param {?} keyName
     * @return {?}
     */
    RxFormControl.prototype.getErrorMessage = function (errorObject, keyName) {
        if (errorObject[keyName][MESSAGE])
            return errorObject[keyName][MESSAGE];
        return;
    };
    return RxFormControl;
}(_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]));
var RxFormGroup = /** @class */ (function (_super) {
    __extends(RxFormGroup, _super);
    /**
     * @param {?} model
     * @param {?} entityObject
     * @param {?} controls
     * @param {?=} validatorOrOpts
     * @param {?=} asyncValidator
     */
    function RxFormGroup(model, entityObject, controls, validatorOrOpts, asyncValidator) {
        var _this = _super.call(this, controls, validatorOrOpts, asyncValidator) || this;
        _this.model = model;
        _this.entityObject = entityObject;
        _this.controlsError = {};
        _this.baseObject = Object.assign({}, _this.entityObject);
        _this.entityService = new EntityService();
        return _this;
    }
    /**
     * @return {?}
     */
    RxFormGroup.prototype.isDirty = function () {
        var /** @type {?} */ isDirty = false;
        for (var /** @type {?} */ name in this.value) {
            var /** @type {?} */ currentValue = this.controls[name].value;
            if (!(this.controls[name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"] || this.controls[name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"])) {
                isDirty = ApplicationUtil.notEqualTo(this.baseObject[name], currentValue);
            }
            else if (this.controls[name] instanceof RxFormGroup)
                isDirty = ( /** @type {?} */(this.controls[name])).isDirty();
            else if (this.controls[name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                for (var _i = 0, _a = ( /** @type {?} */(this.controls[name])).controls; _i < _a.length; _i++) {
                    var formGroup = _a[_i];
                    isDirty = ( /** @type {?} */(formGroup)).isDirty();
                }
            }
            if (isDirty)
                break;
        }
        return isDirty;
    };
    ;
    /**
     * @return {?}
     */
    RxFormGroup.prototype.resetForm = function () {
        for (var /** @type {?} */ name in this.controls) {
            if (this.controls[name] instanceof RxFormGroup)
                ( /** @type {?} */(this.controls[name])).resetForm();
            else if (this.controls[name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                for (var _i = 0, _a = ( /** @type {?} */(this.controls[name])).controls; _i < _a.length; _i++) {
                    var formGroup = _a[_i];
                    ( /** @type {?} */(formGroup)).resetForm();
                }
            }
            else {
                if (RegexValidator.isNotBlank(this.baseObject[name]))
                    this.controls[name].setValue(this.baseObject[name]);
                else
                    this.controls[name].setValue(undefined);
            }
        }
    };
    /**
     * @param {?} onlyMessage
     * @return {?}
     */
    RxFormGroup.prototype.getErrorSummary = function (onlyMessage) {
        var _this = this;
        var /** @type {?} */ jObject = {};
        Object.keys(this.controls).forEach(function (columnName) {
            if (_this.controls[columnName] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]) {
                var /** @type {?} */ error = ( /** @type {?} */(_this.controls[columnName])).getErrorSummary(false);
                if (Object.keys(error).length > 0)
                    jObject[columnName] = error;
            }
            else if (_this.controls[columnName] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                var /** @type {?} */ index = 0;
                for (var _i = 0, _a = ( /** @type {?} */(_this.controls[columnName])).controls; _i < _a.length; _i++) {
                    var formGroup = _a[_i];
                    var /** @type {?} */ error = ( /** @type {?} */(formGroup)).getErrorSummary(false);
                    if (Object.keys(error).length > 0) {
                        error.index = index;
                        if (!jObject[columnName])
                            jObject[columnName] = [];
                        jObject[columnName].push(error);
                    }
                    index++;
                }
            }
            else {
                if (_this.controls[columnName].errors) {
                    var /** @type {?} */ error = _this.controls[columnName].errors;
                    if (onlyMessage)
                        for (var /** @type {?} */ validationName in error)
                            jObject[columnName] = error[validationName].message;
                    else
                        jObject[columnName] = error;
                }
            }
        });
        return jObject;
    };
    /**
     * @return {?}
     */
    RxFormGroup.prototype.valueChangedSync = function () {
        var _this = this;
        Object.keys(this.controls).forEach(function (columnName) {
            if (!(_this.controls[columnName] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"] || _this.controls[columnName] instanceof RxFormArray) && !(_this.controls[columnName] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"] || _this.controls[columnName] instanceof RxFormGroup) && !(_this.entityObject[columnName] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"] || _this.entityObject[columnName] instanceof RxFormControl) && _this.controls[columnName].value != _this.entityObject[columnName]) {
                _this.controls[columnName].setValue(_this.entityObject[columnName], { updateChanged: true });
            }
            else if ((_this.controls[columnName] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"] || _this.controls[columnName] instanceof RxFormArray)) {
                for (var _i = 0, _a = ( /** @type {?} */(_this.controls[columnName])).controls; _i < _a.length; _i++) {
                    var formGroup = _a[_i];
                    ( /** @type {?} */(formGroup)).valueChangedSync();
                }
            }
            else if ((_this.controls[columnName] instanceof RxFormGroup)) {
                ( /** @type {?} */(_this.controls[columnName])).valueChangedSync();
            }
        });
    };
    Object.defineProperty(RxFormGroup.prototype, "modelInstanceValue", {
        /**
         * @return {?}
         */
        get: function () {
            return this.entityService.clone(this.entityObject);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RxFormGroup.prototype, "modelInstance", {
        /**
         * @return {?}
         */
        get: function () {
            return this.entityObject;
        },
        enumerable: true,
        configurable: true
    });
    return RxFormGroup;
}(_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]));
var RxFormBuilder = /** @class */ (function (_super) {
    __extends(RxFormBuilder, _super);
    function RxFormBuilder() {
        var _this = _super.call(this) || this;
        _this.conditionalObjectProps = [];
        _this.conditionalValidationInstance = {};
        _this.builderConfigurationConditionalObjectProps = [];
        _this.formGroupPropOtherValidator = {};
        _this.currentFormGroupPropOtherValidator = {};
        _this.isNested = false;
        _this.isGroupCalled = false;
        return _this;
    }
    /**
     * @param {?} instanceFunc
     * @param {?} entityObject
     * @return {?}
     */
    RxFormBuilder.prototype.getInstanceContainer = function (instanceFunc, entityObject) {
        return this.instaceProvider(instanceFunc, entityObject);
    };
    /**
     * @param {?} formGroup
     * @param {?} object
     * @return {?}
     */
    RxFormBuilder.prototype.setValue = function (formGroup, object) {
        for (var /** @type {?} */ col in object) {
            var /** @type {?} */ control = formGroup.get([col]);
            control.setValue(object[col]);
            control.updateValueAndValidity();
        }
    };
    /**
     * @param {?} fomrBuilderConfiguration
     * @return {?}
     */
    RxFormBuilder.prototype.extractExpressions = function (fomrBuilderConfiguration) {
        if (fomrBuilderConfiguration && fomrBuilderConfiguration.dynamicValidation) {
            for (var /** @type {?} */ property in fomrBuilderConfiguration.dynamicValidation) {
                for (var /** @type {?} */ decorator in fomrBuilderConfiguration.dynamicValidation[property]) {
                    if (fomrBuilderConfiguration.dynamicValidation[property][decorator].conditionalExpression) {
                        var /** @type {?} */ columns = Linq.expressionColumns(fomrBuilderConfiguration.dynamicValidation[property][decorator].conditionalExpression);
                        defaultContainer.addChangeValidation(this.conditionalValidationInstance, property, columns);
                    }
                }
            }
        }
        return null;
    };
    /**
     * @param {?} property
     * @param {?} propertyValidators
     * @param {?} propValidationConfig
     * @return {?}
     */
    RxFormBuilder.prototype.addAsyncValidation = function (property, propertyValidators, propValidationConfig) {
        var /** @type {?} */ asyncValidators = [];
        if (propertyValidators) {
            for (var _i = 0, propertyValidators_1 = propertyValidators; _i < propertyValidators_1.length; _i++) {
                var propertyValidator = propertyValidators_1[_i];
                if (propertyValidator.isAsync)
                    propertyValidator.config.forEach(function (t) { asyncValidators.push(t); });
            }
        }
        if (propValidationConfig && propValidationConfig["async"]) {
            propValidationConfig["async"].forEach(function (t) { asyncValidators.push(t); });
        }
        return asyncValidators;
    };
    /**
     * @param {?} property
     * @param {?} propertyValidators
     * @param {?} propValidationConfig
     * @param {?} instance
     * @param {?} entity
     * @return {?}
     */
    RxFormBuilder.prototype.addFormControl = function (property, propertyValidators, propValidationConfig, instance, entity) {
        var /** @type {?} */ validators = [];
        var /** @type {?} */ columns = [];
        if ((instance.conditionalValidationProps && instance.conditionalValidationProps[property.name]) || (this.conditionalValidationInstance.conditionalValidationProps && this.conditionalValidationInstance.conditionalValidationProps[property.name])) {
            var /** @type {?} */ props_1 = [];
            if ((instance.conditionalValidationProps && instance.conditionalValidationProps[property.name]))
                instance.conditionalValidationProps[property.name].forEach(function (t) { return props_1.push(t); });
            if (this.conditionalValidationInstance.conditionalValidationProps && this.conditionalValidationInstance.conditionalValidationProps[property.name])
                this.conditionalValidationInstance.conditionalValidationProps[property.name].forEach(function (t) { return props_1.push(t); });
            validators.push(conditionalChangeValidator(props_1));
        }
        if (this.conditionalObjectProps.length > 0 || this.builderConfigurationConditionalObjectProps.length > 0) {
            var /** @type {?} */ propConditions_1 = [];
            if (this.conditionalObjectProps)
                propConditions_1 = this.conditionalObjectProps.filter(function (t) { return t.propName == property.name; });
            if (this.builderConfigurationConditionalObjectProps)
                this.builderConfigurationConditionalObjectProps.filter(function (t) { return t.propName == property.name; }).forEach(function (t) { return propConditions_1.push(t); });
            propConditions_1.forEach(function (t) {
                if (t.referencePropName && columns.indexOf(t.referencePropName) == -1)
                    columns.push(t.referencePropName);
            });
            if (columns.length > 0)
                validators.push(conditionalChangeValidator(columns));
        }
        for (var _i = 0, propertyValidators_2 = propertyValidators; _i < propertyValidators_2.length; _i++) {
            var propertyValidator = propertyValidators_2[_i];
            if (!propertyValidator.isAsync)
                propertyValidator.annotationType == AnnotationTypes.rule ? validators.push(APP_VALIDATORS[propertyValidator.annotationType](propertyValidator.config, entity)) : validators.push(APP_VALIDATORS[propertyValidator.annotationType](propertyValidator.config));
        }
        if (propValidationConfig)
            this.additionalValidation(validators, propValidationConfig);
        if (this.currentFormGroupPropOtherValidator[property.name])
            this.currentFormGroupPropOtherValidator[property.name].forEach(function (t) { validators.push(t); });
        return validators;
    };
    /**
     * @param {?} validations
     * @param {?} propValidationConfig
     * @return {?}
     */
    RxFormBuilder.prototype.additionalValidation = function (validations, propValidationConfig) {
        for (var /** @type {?} */ col in AnnotationTypes) {
            if (propValidationConfig[AnnotationTypes[col]] && col != "custom") {
                validations.push(APP_VALIDATORS[AnnotationTypes[col]](propValidationConfig[AnnotationTypes[col]]));
            }
            else if (col == AnnotationTypes.custom && propValidationConfig[AnnotationTypes[col]])
                validations.push(propValidationConfig[col]);
        }
    };
    /**
     * @template T
     * @param {?} instanceContainer
     * @param {?} object
     * @return {?}
     */
    RxFormBuilder.prototype.checkObjectPropAdditionalValidation = function (instanceContainer, object) {
        var _this = this;
        var /** @type {?} */ props = instanceContainer.properties.filter(function (t) { return t.propertyType == OBJECT_PROPERTY || t.propertyType == ARRAY_PROPERTY; });
        props.forEach(function (t) {
            var /** @type {?} */ instance = _this.getInstanceContainer(t.entity, null);
            if (instance.conditionalValidationProps) {
                for (var /** @type {?} */ key in instance.conditionalValidationProps) {
                    var /** @type {?} */ prop = instanceContainer.properties.filter(function (t) { return t.name == key; })[0];
                    if (prop) {
                        if (!instanceContainer.conditionalValidationProps)
                            instanceContainer.conditionalValidationProps = {};
                        if (!instanceContainer.conditionalValidationProps[key])
                            instanceContainer.conditionalValidationProps[key] = [];
                        instance.conditionalValidationProps[key].forEach(function (x) {
                            if (t.propertyType != ARRAY_PROPERTY)
                                instanceContainer.conditionalValidationProps[key].push([t.name, x].join('.'));
                            else
                                instanceContainer.conditionalValidationProps[key].push([t.name, x].join('[]'));
                        });
                    }
                }
            }
        });
    };
    /**
     * @param {?} model
     * @param {?=} entityObject
     * @param {?=} formBuilderConfiguration
     * @return {?}
     */
    RxFormBuilder.prototype.getObject = function (model, entityObject, formBuilderConfiguration) {
        var /** @type {?} */ json = {};
        if (typeof model == FUNCTION_STRING)
            json.model = model;
        if (typeof model == FUNCTION_STRING && (entityObject instanceof FormBuilderConfiguration)) {
            json.entityObject = this.createClassObject(json.model, entityObject);
        }
        if (entityObject && !(entityObject instanceof FormBuilderConfiguration))
            json.entityObject = entityObject;
        if (entityObject instanceof FormBuilderConfiguration && !formBuilderConfiguration)
            json.formBuilderConfiguration = entityObject;
        else if (!(entityObject instanceof FormBuilderConfiguration) && formBuilderConfiguration) {
            json.formBuilderConfiguration = formBuilderConfiguration;
            json.entityObject = this.createClassObject(json.model, json.formBuilderConfiguration, json.entityObject);
        }
        if (!entityObject) {
            if (typeof model == OBJECT_STRING)
                json.model = model.constructor;
            json.entityObject = this.createClassObject(json.model, json.formBuilderConfiguration, model);
        }
        else if (model && (entityObject instanceof FormBuilderConfiguration) && (typeof model == OBJECT_STRING)) {
            json["model"] = model.constructor;
            json["entityObject"] = this.createClassObject(json.model, json.formBuilderConfiguration, model);
        }
        return json;
    };
    /**
     * @param {?} groupObject
     * @param {?=} validatorConfig
     * @return {?}
     */
    RxFormBuilder.prototype.group = function (groupObject, validatorConfig) {
        var /** @type {?} */ modelInstance = _super.prototype.createInstance.call(this);
        var /** @type {?} */ entityObject = {};
        this.formGroupPropOtherValidator = {};
        this.currentFormGroupPropOtherValidator = this.formGroupPropOtherValidator;
        this.createValidatorFormGroup(groupObject, entityObject, modelInstance, validatorConfig);
        this.currentFormGroupPropOtherValidator = this.formGroupPropOtherValidator;
        this.isGroupCalled = true;
        var /** @type {?} */ formGroup = this.formGroup(modelInstance.constructor, entityObject, validatorConfig);
        this.isGroupCalled = false;
        this.formGroupPropOtherValidator = {};
        this.currentFormGroupPropOtherValidator = this.formGroupPropOtherValidator;
        this.formGroupPropOtherValidator = {};
        return formGroup;
    };
    /**
     * @param {?} propName
     * @param {?} validatorConfig
     * @param {?} modelInstance
     * @return {?}
     */
    RxFormBuilder.prototype.applyAllPropValidator = function (propName, validatorConfig, modelInstance) {
        var _this = this;
        if (validatorConfig && validatorConfig.applyAllProps) {
            if (!(validatorConfig.excludeProps && validatorConfig.excludeProps.length > 0 && validatorConfig.excludeProps.indexOf(propName) == -1)) {
                validatorConfig.applyAllProps.forEach(function (t) {
                    if (t.name == RX_WEB_VALIDATOR) {
                        t(propName, modelInstance);
                    }
                    else {
                        if (!_this.currentFormGroupPropOtherValidator[propName])
                            _this.currentFormGroupPropOtherValidator[propName] = [];
                        _this.currentFormGroupPropOtherValidator[propName].push(t);
                    }
                });
            }
        }
    };
    /**
     * @param {?} propName
     * @param {?} validatorConfig
     * @return {?}
     */
    RxFormBuilder.prototype.dynamicValidationPropCheck = function (propName, validatorConfig) {
        return (validatorConfig == undefined) ? true : (!validatorConfig.dynamicValidationConfigurationPropertyName) ? true : validatorConfig.dynamicValidationConfigurationPropertyName == propName ? false : true;
    };
    /**
     * @param {?} groupObject
     * @param {?} entityObject
     * @param {?} modelInstance
     * @param {?} validatorConfig
     * @return {?}
     */
    RxFormBuilder.prototype.createValidatorFormGroup = function (groupObject, entityObject, modelInstance, validatorConfig) {
        for (var /** @type {?} */ propName in groupObject) {
            var /** @type {?} */ prop = groupObject[propName];
            if (prop instanceof Array && prop.length > 0 && typeof prop[0] != OBJECT_STRING) {
                var /** @type {?} */ propValidators = (prop.length > 1 && prop[1] instanceof Array) ? prop[1] : (prop.length == 2) ? [prop[1]] : [];
                var /** @type {?} */ propertyAdded = false;
                for (var /** @type {?} */ i = 0; i < propValidators.length; i++) {
                    if (propValidators[i].name == RX_WEB_VALIDATOR) {
                        propValidators[i](propName, modelInstance);
                        propertyAdded = true;
                    }
                    else {
                        if (!this.currentFormGroupPropOtherValidator[propName])
                            this.currentFormGroupPropOtherValidator[propName] = [];
                        this.currentFormGroupPropOtherValidator[propName].push(propValidators[i]);
                    }
                }
                if (!propertyAdded)
                    defaultContainer.initPropertyObject(propName, PROPERTY, undefined, typeof modelInstance == OBJECT_STRING ? modelInstance : { constructor: modelInstance });
                this.applyAllPropValidator(propName, validatorConfig, modelInstance);
            }
            else if (typeof prop == STRING || typeof prop == NUMBER || typeof prop == BOOLEAN) {
                defaultContainer.initPropertyObject(propName, PROPERTY, undefined, typeof modelInstance == OBJECT_STRING ? modelInstance : { constructor: modelInstance });
                this.applyAllPropValidator(propName, validatorConfig, modelInstance);
            }
            else if (prop instanceof Array) {
                if (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                    entityObject[propName] = prop;
                }
                else {
                    var /** @type {?} */ propModelInstance = _super.prototype.createInstance.call(this);
                    if (typeof modelInstance == "function")
                        modelInstance.constructor = modelInstance;
                    defaultContainer.initPropertyObject(propName, ARRAY_PROPERTY, propModelInstance.constructor, modelInstance);
                    entityObject[propName] = [];
                    for (var _i = 0, prop_1 = prop; _i < prop_1.length; _i++) {
                        var row = prop_1[_i];
                        var /** @type {?} */ jObject = {};
                        entityObject[propName].push(jObject);
                        this.createValidatorFormGroup(row, jObject, propModelInstance.constructor, validatorConfig);
                    }
                }
            }
            else if (typeof prop == OBJECT_STRING && !(prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"] || prop instanceof RxFormControl)) {
                var /** @type {?} */ formGroup = (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) ? prop.controls[0] : prop;
                if (!formGroup.model && (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"] || prop instanceof RxFormGroup)) {
                    formGroup = this.group(formGroup.controls);
                }
                if (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"] || prop instanceof RxFormGroup) {
                    entityObject[propName] = prop;
                    defaultContainer.initPropertyObject(propName, OBJECT_PROPERTY, formGroup.model, modelInstance);
                }
                else if (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                    entityObject[propName] = prop;
                    defaultContainer.initPropertyObject(propName, ARRAY_PROPERTY, formGroup.model, modelInstance);
                }
                else {
                    if (this.dynamicValidationPropCheck(propName, validatorConfig)) {
                        this.formGroupPropOtherValidator[propName] = {};
                        this.currentFormGroupPropOtherValidator = this.formGroupPropOtherValidator[propName];
                        var /** @type {?} */ propModelInstance = _super.prototype.createInstance.call(this);
                        entityObject[propName] = {};
                        entityObject[propName].constructor = propModelInstance.constructor;
                        defaultContainer.initPropertyObject(propName, OBJECT_PROPERTY, entityObject[propName].constructor, modelInstance);
                        var /** @type {?} */ objectValidationConfig = this.getValidatorConfig(validatorConfig, groupObject, propName + ".");
                        this.createValidatorFormGroup(groupObject[propName], entityObject[propName], entityObject[propName].constructor, objectValidationConfig);
                    }
                    else
                        entityObject[propName] = groupObject[propName];
                }
            }
            if (typeof prop == STRING || typeof prop == NUMBER || typeof prop == BOOLEAN) {
                entityObject[propName] = prop;
            }
            else if ((prop && prop.length > 0 && (typeof prop[0] != OBJECT_STRING) && !(prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"] || prop instanceof RxFormControl) && !(prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]))) {
                entityObject[propName] = prop[0];
            }
            else if (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                entityObject[propName] = prop;
            }
            else if (prop instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"] || prop instanceof RxFormControl) {
                entityObject[propName] = prop;
                defaultContainer.initPropertyObject(propName, PROPERTY, undefined, modelInstance.constructor ? modelInstance : { constructor: modelInstance });
            }
        }
    };
    /**
     * @param {?} validatorConfig
     * @param {?} entityObject
     * @param {?} rootPropertyName
     * @param {?=} arrayPropertyName
     * @return {?}
     */
    RxFormBuilder.prototype.getValidatorConfig = function (validatorConfig, entityObject, rootPropertyName, arrayPropertyName) {
        var /** @type {?} */ validationProps = {};
        var /** @type {?} */ excludeProps = [];
        var /** @type {?} */ includeProps = [];
        if (validatorConfig) {
            for (var /** @type {?} */ propName in validatorConfig.dynamicValidation) {
                if (propName.indexOf(rootPropertyName) != -1 || (arrayPropertyName && propName.indexOf(arrayPropertyName) != -1)) {
                    var /** @type {?} */ splitProp = propName.split(".")[1];
                    if (splitProp)
                        validationProps[splitProp] = validatorConfig.dynamicValidation[propName];
                }
            }
            if (validatorConfig.excludeProps)
                excludeProps = this.getProps(validatorConfig.excludeProps, rootPropertyName);
            if (validatorConfig.includeProps)
                includeProps = this.getProps(validatorConfig.includeProps, rootPropertyName);
            return { includeProps: includeProps, dynamicValidation: (validatorConfig.dynamicValidationConfigurationPropertyName && entityObject[validatorConfig.dynamicValidationConfigurationPropertyName]) ? entityObject[validatorConfig.dynamicValidationConfigurationPropertyName] : validationProps, excludeProps: excludeProps };
        }
        return {};
    };
    /**
     * @param {?} properties
     * @param {?} rootPropertyName
     * @return {?}
     */
    RxFormBuilder.prototype.getProps = function (properties, rootPropertyName) {
        var /** @type {?} */ props = [];
        for (var _i = 0, properties_1 = properties; _i < properties_1.length; _i++) {
            var prop_2 = properties_1[_i];
            if (prop_2.indexOf(rootPropertyName) != -1) {
                var /** @type {?} */ splitProp = prop_2.split(".")[1];
                if (splitProp)
                    props.push(splitProp);
            }
        }
        return props;
    };
    /**
     * @template T
     * @param {?} model
     * @param {?=} entityObject
     * @param {?=} formBuilderConfiguration
     * @return {?}
     */
    RxFormBuilder.prototype.formGroup = function (model, entityObject, formBuilderConfiguration) {
        var _this = this;
        var /** @type {?} */ json = this.getObject(model, entityObject, formBuilderConfiguration);
        model = json.model;
        entityObject = json.entityObject;
        if (entityObject.constructor != model && !this.isGroupCalled) {
            entityObject = json.entityObject = this.updateObject(model, json.entityObject);
        }
        formBuilderConfiguration = json.formBuilderConfiguration;
        if (formBuilderConfiguration)
            this.extractExpressions(formBuilderConfiguration);
        var /** @type {?} */ instanceContainer = this.getInstanceContainer(model, entityObject);
        this.checkObjectPropAdditionalValidation(instanceContainer, entityObject);
        var /** @type {?} */ formGroupObject = {};
        var /** @type {?} */ additionalValidations = {};
        instanceContainer.properties.forEach(function (property) {
            var /** @type {?} */ isIncludeProp = true;
            if (formBuilderConfiguration && formBuilderConfiguration.excludeProps && formBuilderConfiguration.excludeProps.length > 0)
                isIncludeProp = formBuilderConfiguration.excludeProps.indexOf(property.name) == -1;
            if (formBuilderConfiguration && formBuilderConfiguration.dynamicValidation)
                additionalValidations = formBuilderConfiguration.dynamicValidation;
            if (formBuilderConfiguration && formBuilderConfiguration.includeProps && formBuilderConfiguration.includeProps.length > 0)
                isIncludeProp = formBuilderConfiguration.includeProps.indexOf(property.name) != -1;
            if (isIncludeProp) {
                switch (property.propertyType) {
                    case PROPERTY:
                        if (!(entityObject[property.name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"] || entityObject[property.name] instanceof RxFormControl)) {
                            var /** @type {?} */ propertyValidators = instanceContainer.propertyAnnotations.filter(function (t) { return t.propertyName == property.name; });
                            formGroupObject[property.name] = new RxFormControl(entityObject[property.name], _this.addFormControl(property, propertyValidators, additionalValidations[property.name], instanceContainer, entityObject), _this.addAsyncValidation(property, propertyValidators, additionalValidations[property.name]), json.entityObject, Object.assign({}, json.entityObject), property.name);
                            _this.isNested = false;
                        }
                        else
                            formGroupObject[property.name] = entityObject[property.name];
                        break;
                    case OBJECT_PROPERTY:
                        if (entityObject[property.name] && entityObject[property.name] instanceof Object && !(entityObject[property.name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"] || entityObject[property.name] instanceof RxFormGroup)) {
                            _this.isNested = true;
                            if (instanceContainer && instanceContainer.conditionalObjectProps)
                                _this.conditionalObjectProps = instanceContainer.conditionalObjectProps.filter(function (t) { return t.objectPropName == property.name; });
                            if (_this.conditionalValidationInstance && _this.conditionalValidationInstance.conditionalObjectProps)
                                _this.builderConfigurationConditionalObjectProps = _this.conditionalValidationInstance.conditionalObjectProps.filter(function (t) { return t.objectPropName == property.name; });
                            if (_this.formGroupPropOtherValidator[property.name])
                                _this.currentFormGroupPropOtherValidator = _this.formGroupPropOtherValidator[property.name];
                            var /** @type {?} */ objectValidationConfig = _this.getValidatorConfig(formBuilderConfiguration, entityObject[property.name], property.name + ".");
                            formGroupObject[property.name] = _this.formGroup(property.entity, entityObject[property.name], objectValidationConfig);
                            _this.conditionalObjectProps = [];
                            _this.builderConfigurationConditionalObjectProps = [];
                            _this.isNested = false;
                        }
                        else if (entityObject[property.name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"] || entityObject[property.name] instanceof RxFormGroup)
                            formGroupObject[property.name] = entityObject[property.name];
                        break;
                    case ARRAY_PROPERTY:
                        if (entityObject[property.name] && entityObject[property.name] instanceof Array && !(entityObject[property.name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"])) {
                            _this.isNested = true;
                            var /** @type {?} */ formArrayGroup = [];
                            var /** @type {?} */ index_1 = 0;
                            for (var _i = 0, _a = entityObject[property.name]; _i < _a.length; _i++) {
                                var subObject = _a[_i];
                                if (instanceContainer && instanceContainer.conditionalObjectProps)
                                    _this.conditionalObjectProps = instanceContainer.conditionalObjectProps.filter(function (t) { return t.objectPropName == property.name && t.arrayIndex == index_1; });
                                if (_this.conditionalValidationInstance && _this.conditionalValidationInstance.conditionalObjectProps)
                                    _this.builderConfigurationConditionalObjectProps = _this.conditionalValidationInstance.conditionalObjectProps.filter(function (t) { return t.objectPropName == property.name && t.arrayIndex == index_1; });
                                if (_this.formGroupPropOtherValidator[property.name])
                                    _this.currentFormGroupPropOtherValidator = _this.formGroupPropOtherValidator[property.name];
                                var /** @type {?} */ objectValidationConfig = _this.getValidatorConfig(formBuilderConfiguration, subObject, property.name + ".", property.name + "[" + index_1 + "].");
                                formArrayGroup.push(_this.formGroup(property.entity, subObject, objectValidationConfig));
                                index_1++;
                                _this.conditionalObjectProps = [];
                                _this.builderConfigurationConditionalObjectProps = [];
                            }
                            var /** @type {?} */ formBuilder = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]();
                            formGroupObject[property.name] = new RxFormArray(entityObject[property.name], formArrayGroup);
                            _this.isNested = false;
                        }
                        else if (entityObject[property.name] instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"])
                            formGroupObject[property.name] = entityObject[property.name];
                        break;
                }
            }
        });
        if (!this.isNested) {
            this.conditionalValidationInstance = {};
            this.builderConfigurationConditionalObjectProps = [];
        }
        return new RxFormGroup(json.model, json.entityObject, formGroupObject, undefined);
    };
    return RxFormBuilder;
}(BaseFormBuilder));
RxFormBuilder.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/**
 * @nocollapse
 */
RxFormBuilder.ctorParameters = function () { return []; };
/**
 * @abstract
 */
var BaseDirective = /** @class */ (function () {
    function BaseDirective() {
    }
    return BaseDirective;
}());
var RxwebFormDirective = /** @class */ (function (_super) {
    __extends(RxwebFormDirective, _super);
    function RxwebFormDirective() {
        var _this = _super.apply(this, arguments) || this;
        _this.clearTimeout = 0;
        _this.validationRule = {};
        return _this;
    }
    /**
     * @return {?}
     */
    RxwebFormDirective.prototype.ngAfterContentInit = function () {
        if (this.formGroup && !this.formGroup["model"] && this.formGroup.parent == null) {
            this.expressionProcessor(this.formGroup.controls);
            this.setConditionalValidator(this.formGroup.controls);
        }
        else if (this.ngForm) {
            this.configureModelValidations();
        }
    };
    /**
     * @return {?}
     */
    RxwebFormDirective.prototype.configureModelValidations = function () {
        var _this = this;
        this.clearTimeout = window.setTimeout(function () {
            window.clearTimeout(_this.clearTimeout);
            _this.expressionProcessor(_this.ngForm.form.controls);
            _this.setConditionalValidator(_this.ngForm.form.controls);
            Object.keys(_this.ngForm.form.controls).forEach(function (key) {
                _this.ngForm.form.controls[key].updateValueAndValidity();
            });
        }, 500);
    };
    /**
     * @param {?} controls
     * @param {?=} rootFieldName
     * @return {?}
     */
    RxwebFormDirective.prototype.expressionProcessor = function (controls, rootFieldName) {
        var _this = this;
        if (rootFieldName === void 0) { rootFieldName = ""; }
        Object.keys(controls).forEach(function (fieldName) {
            var /** @type {?} */ formControl = controls[fieldName];
            if (formControl.validatorConfig) {
                Object.keys(AnnotationTypes).forEach(function (validatorName) {
                    if (formControl.validatorConfig[validatorName] && formControl.validatorConfig[validatorName].conditionalExpression) {
                        var /** @type {?} */ columns = Linq.expressionColumns(formControl.validatorConfig[validatorName].conditionalExpression);
                        defaultContainer.addChangeValidation(_this.validationRule, rootFieldName + fieldName, columns);
                    }
                    if (formControl.validatorConfig[validatorName] && ((validatorName == AnnotationTypes.compare || validatorName == AnnotationTypes.greaterThan || validatorName == AnnotationTypes.greaterThanEqualTo || validatorName == AnnotationTypes.lessThan || validatorName == AnnotationTypes.lessThanEqualTo || validatorName == AnnotationTypes.different || validatorName == AnnotationTypes.factor) || (validatorName == AnnotationTypes.creditCard && formControl.validatorConfig[validatorName].fieldName) || ((validatorName == AnnotationTypes.minDate || validatorName == AnnotationTypes.maxDate) && formControl.validatorConfig[validatorName].fieldName))) {
                        defaultContainer.setConditionalValueProp(_this.validationRule, formControl.validatorConfig[validatorName].fieldName, fieldName);
                    }
                });
            }
            else if (formControl instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]) {
                _this.expressionProcessor(formControl.controls, fieldName + ".");
            }
            else if (formControl instanceof _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormArray"]) {
                if (formControl.controls)
                    formControl.controls.forEach(function (t, i) {
                        if (t.controls)
                            _this.expressionProcessor(t.controls, fieldName + "[]");
                    });
            }
        });
    };
    /**
     * @param {?} controls
     * @return {?}
     */
    RxwebFormDirective.prototype.setConditionalValidator = function (controls) {
        var _this = this;
        Object.keys(controls).forEach(function (fieldName) {
            if (_this.validationRule.conditionalValidationProps && _this.validationRule.conditionalValidationProps[fieldName]) {
                controls[fieldName]["conditionalValidator"] = conditionalChangeValidator(_this.validationRule.conditionalValidationProps[fieldName]);
            }
        });
    };
    /**
     * @return {?}
     */
    RxwebFormDirective.prototype.ngOnDestroy = function () {
    };
    return RxwebFormDirective;
}(BaseDirective));
RxwebFormDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[formGroup],[rxwebForm]',
            },] },
];
/**
 * @nocollapse
 */
RxwebFormDirective.ctorParameters = function () { return []; };
RxwebFormDirective.propDecorators = {
    'formGroup': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'ngForm': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['rxwebForm',] },],
};
var DecimalProvider = /** @class */ (function () {
    /**
     * @param {?} decimalPipe
     */
    function DecimalProvider(decimalPipe) {
        this.decimalPipe = decimalPipe;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    DecimalProvider.prototype.replacer = function (value) {
        value = value.replace(RegexValidator.commaRegex(), BLANK);
        var /** @type {?} */ splitValue = value.split(".");
        value = (splitValue.length > 1 && splitValue[1] && RegexValidator.isZero(splitValue[1])) ? splitValue[0] : value;
        return value;
    };
    /**
     * @param {?} value
     * @param {?} digitsInfo
     * @return {?}
     */
    DecimalProvider.prototype.transFormDecimal = function (value, digitsInfo) {
        return this.decimalPipe.transform(value, digitsInfo);
    };
    return DecimalProvider;
}());
DecimalProvider.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"] },
];
/**
 * @nocollapse
 */
DecimalProvider.ctorParameters = function () { return [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_1__["DecimalPipe"], },
]; };
var HtmlControlTemplateDirective = /** @class */ (function () {
    /**
     * @param {?} templateRef
     */
    function HtmlControlTemplateDirective(templateRef) {
        this.templateRef = templateRef;
    }
    ;
    return HtmlControlTemplateDirective;
}());
HtmlControlTemplateDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[htmlControlTemplate]'
            },] },
];
/**
 * @nocollapse
 */
HtmlControlTemplateDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["TemplateRef"], },
]; };
HtmlControlTemplateDirective.propDecorators = {
    'type': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['htmlControlTemplate',] },],
};
var RxwebDynamicFormComponent = /** @class */ (function () {
    function RxwebDynamicFormComponent() {
    }
    /**
     * @return {?}
     */
    RxwebDynamicFormComponent.prototype.ngAfterContentInit = function () {
    };
    return RxwebDynamicFormComponent;
}());
RxwebDynamicFormComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                template: '',
                selector: 'rxweb-dynamic-form',
                exportAs: 'rxwebForm'
            },] },
];
/**
 * @nocollapse
 */
RxwebDynamicFormComponent.ctorParameters = function () { return []; };
RxwebDynamicFormComponent.propDecorators = {
    'htmlControlTemplates': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChildren"], args: [HtmlControlTemplateDirective,] },],
};
var RxwebControlComponent = /** @class */ (function () {
    function RxwebControlComponent() {
    }
    /**
     * @return {?}
     */
    RxwebControlComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.dynamicForm && this.dynamicForm.htmlControlTemplates) {
            var /** @type {?} */ htmlControl = this.dynamicForm.htmlControlTemplates.filter(function (t) { return t.type == _this.type; })[0];
            if (htmlControl)
                this.control = htmlControl;
        }
    };
    return RxwebControlComponent;
}());
RxwebControlComponent.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                template: "<ng-template [controlHost]=\"{templateRef:control.templateRef, data:data, $implicit: data}\">\n            </ng-template>",
                selector: 'rxweb-control'
            },] },
];
/**
 * @nocollapse
 */
RxwebControlComponent.ctorParameters = function () { return []; };
RxwebControlComponent.propDecorators = {
    'type': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'dynamicForm': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'data': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var ControlHostDirective = /** @class */ (function () {
    /**
     * @param {?} viewContainerRef
     */
    function ControlHostDirective(viewContainerRef) {
        this.viewContainerRef = viewContainerRef;
    }
    Object.defineProperty(ControlHostDirective.prototype, "portal", {
        /**
         * @param {?} context
         * @return {?}
         */
        set: function (context) {
            if (context.templateRef) {
                if (this.view) {
                    this.view.destroy();
                    this.view = undefined;
                }
                this.view = this.viewContainerRef.createEmbeddedView(context.templateRef, context);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    ControlHostDirective.prototype.ngOnDestroy = function () {
        if (this.view)
            this.view.destroy();
        if (this.viewContainerRef)
            this.viewContainerRef.clear();
    };
    return ControlHostDirective;
}());
ControlHostDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[controlHost]'
            },] },
];
/**
 * @nocollapse
 */
ControlHostDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"], },
]; };
ControlHostDirective.propDecorators = {
    'portal': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"], args: ['controlHost',] },],
};
/**
 * @abstract
 */
var ControlExpressionProcess = /** @class */ (function () {
    function ControlExpressionProcess() {
        this.controlConfig = {};
        this.isProcessed = false;
    }
    /**
     * @param {?} control
     * @param {?} name
     * @return {?}
     */
    ControlExpressionProcess.prototype.process = function (control, name) {
        var /** @type {?} */ validationRule = {};
        var /** @type {?} */ controls = control.parent.controls;
        Object.keys(controls).forEach(function (fieldName) {
            var /** @type {?} */ formControl = controls[fieldName];
            if (formControl.validatorConfig) {
                Object.keys(AnnotationTypes).forEach(function (validatorName) {
                    if (formControl.validatorConfig[validatorName] && formControl.validatorConfig[validatorName].conditionalExpression) {
                        var /** @type {?} */ columns = Linq.expressionColumns(formControl.validatorConfig[validatorName].conditionalExpression);
                        defaultContainer.addChangeValidation(validationRule, fieldName, columns);
                    }
                    if (formControl.validatorConfig[validatorName] && ((validatorName == AnnotationTypes.compare || validatorName == AnnotationTypes.greaterThan || validatorName == AnnotationTypes.greaterThanEqualTo || validatorName == AnnotationTypes.lessThan || validatorName == AnnotationTypes.lessThanEqualTo || validatorName == AnnotationTypes.different || validatorName == AnnotationTypes.factor) || (validatorName == AnnotationTypes.creditCard && formControl.validatorConfig[validatorName].fieldName))) {
                        defaultContainer.setConditionalValueProp(validationRule, formControl.validatorConfig[validatorName].fieldName, fieldName);
                    }
                });
            }
        });
        this.isProcessed = true;
        if (!this.conditionalValidator && validationRule.conditionalValidationProps && validationRule.conditionalValidationProps[name])
            this.conditionalValidator = conditionalChangeValidator(validationRule.conditionalValidationProps[name]);
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ControlExpressionProcess.prototype.setModelConfig = function (control) {
        this.isProcessed = true;
        if (this.controlConfig && this.controlConfig.validatorConfig) {
            control["validatorConfig"] = this.controlConfig.validatorConfig;
            this.controlConfig = undefined;
        }
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ControlExpressionProcess.prototype.expressionProcessor = function (control) {
        if (!this.isProcessed && this.name && control.parent && control.parent["marked"]) {
            this.process(control, this.name);
        }
    };
    return ControlExpressionProcess;
}());
ControlExpressionProcess.propDecorators = {
    'name': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'formControlName': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var BaseValidator = /** @class */ (function (_super) {
    __extends(BaseValidator, _super);
    function BaseValidator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @param {?} control
     * @return {?}
     */
    BaseValidator.prototype.validation = function (control) {
        var /** @type {?} */ result = null;
        for (var _i = 0, _a = this.validators; _i < _a.length; _i++) {
            var validator = _a[_i];
            result = validator(control);
            if (result)
                break;
        }
        return result;
    };
    /**
     * @return {?}
     */
    BaseValidator.prototype.setEventName = function () {
        var /** @type {?} */ eventName = '';
        switch (this.element.tagName) {
            case INPUT:
            case TEXTAREA:
                eventName = (this.element.type == CHECKBOX || this.element.type == RADIO || this.element.type == FILE) ? CHANGE : INPUT;
                break;
            case SELECT:
                eventName = CHANGE;
                break;
        }
        this.eventName = eventName.toLowerCase();
    };
    return BaseValidator;
}(ControlExpressionProcess));
BaseValidator.propDecorators = {
    'formControl': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var NGMODEL_BINDING = {
    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
    useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return RxFormControlDirective; }),
    multi: true
};
var ALLOW_VALIDATOR_WITHOUT_CONFIG = ['required', 'notEmpty', 'alpha', 'alphaNumeric', 'ascii', 'dataUri', 'digit', 'email', 'even', 'hexColor', 'json', 'latitude', 'latLong', 'leapYear', 'longitude', 'lowerCase', 'mac', 'odd', 'port', 'primeNumber', 'time', 'upperCase', 'url', 'unique', 'cusip', 'gird'];
var RxFormControlDirective = /** @class */ (function (_super) {
    __extends(RxFormControlDirective, _super);
    /**
     * @param {?} elementRef
     * @param {?} renderer
     * @param {?} decimalProvider
     */
    function RxFormControlDirective(elementRef, renderer, decimalProvider) {
        var _this = _super.call(this) || this;
        _this.elementRef = elementRef;
        _this.renderer = renderer;
        _this.decimalProvider = decimalProvider;
        _this.eventListeners = [];
        _this.isNumericSubscribed = false;
        _this.element = elementRef.nativeElement;
        _this.setEventName();
        return _this;
    }
    Object.defineProperty(RxFormControlDirective.prototype, "validationControls", {
        /**
         * @return {?}
         */
        get: function () {
            return this.controls;
        },
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this.controls = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    RxFormControlDirective.prototype.ngOnInit = function () {
        var _this = this;
        var /** @type {?} */ validators = [];
        Object.keys(APP_VALIDATORS).forEach(function (validatorName) {
            if ((_this[validatorName]) || (ALLOW_VALIDATOR_WITHOUT_CONFIG.indexOf(validatorName) != -1 && _this[validatorName] == BLANK)) {
                validators.push(APP_VALIDATORS[validatorName](_this[validatorName]));
                if (_this.name && !(_this.formControlName && _this.formControl)) {
                    ApplicationUtil.configureControl(_this.controlConfig, _this[validatorName], validatorName);
                }
            }
        });
        if (validators.length > 0)
            this.validators = validators;
        if (this.numeric && this.numeric.isFormat)
            this.bindNumericElementEvent();
    };
    /**
     * @param {?=} config
     * @return {?}
     */
    RxFormControlDirective.prototype.bindNumericElementEvent = function (config) {
        var _this = this;
        if (config)
            this.numeric = config;
        var /** @type {?} */ listener = this.renderer.listen(this.element, BLUR, function (event) {
            if (!(_this.formControl && _this.formControl.errors && _this.formControl.errors.numeric)) {
                var /** @type {?} */ value = _this.decimalProvider.transFormDecimal(_this.formControl.value, _this.numeric.digitsInfo);
                _this.setValueOnElement(value);
            }
        });
        this.eventListeners.push(listener);
        listener = this.renderer.listen(this.element, FOCUS, function (event) {
            if (!(_this.formControl && _this.formControl.errors && _this.formControl.errors.numeric) && _this.formControl.value != null) {
                var /** @type {?} */ value = _this.decimalProvider.replacer(_this.formControl.value);
                _this.setValueOnElement(value);
            }
        });
        this.eventListeners.push(listener);
    };
    /**
     * @return {?}
     */
    RxFormControlDirective.prototype.bindValueChangeEvent = function () {
        var _this = this;
        if (this.eventName != BLANK) {
            var /** @type {?} */ listener = this.renderer.listen(this.element, this.eventName, function () {
                Object.keys(_this.validationControls).forEach(function (fieldName) {
                    _this.validationControls[fieldName].updateValueAndValidity();
                });
            });
            this.eventListeners.push(listener);
        }
    };
    /**
     * @return {?}
     */
    RxFormControlDirective.prototype.subscribeNumericFormatter = function () {
        if (this.formControl["validatorConfig"] && this.formControl["validatorConfig"]["numeric"] && this.formControl["validatorConfig"]["numeric"]["isFormat"] && !this.isNumericSubscribed) {
            this.bindNumericElementEvent(this.formControl["validatorConfig"]["numeric"]);
            this.isNumericSubscribed = true;
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    RxFormControlDirective.prototype.setValueOnElement = function (value) {
        this.renderer.setElementProperty(this.element, ELEMENT_VALUE, value);
    };
    /**
     * @param {?} control
     * @return {?}
     */
    RxFormControlDirective.prototype.validate = function (control) {
        if (!this.formControl)
            this.formControl = control;
        if (!this.isNumericSubscribed)
            this.subscribeNumericFormatter();
        if (control["conditionalValidator"]) {
            this.conditionalValidator = control["conditionalValidator"];
            delete control["conditionalValidator"];
        }
        if (this.conditionalValidator)
            this.conditionalValidator(control);
        if (!this.isProcessed)
            this.setModelConfig(control);
        return this.validators && this.validators.length > 0 ? this.validation(control) : null;
    };
    /**
     * @return {?}
     */
    RxFormControlDirective.prototype.ngOnDestroy = function () {
        this.controls = undefined;
        var /** @type {?} */ eventCount = this.eventListeners.length;
        for (var /** @type {?} */ i = 0; i < eventCount; i++) {
            this.eventListeners[0]();
            this.eventListeners.splice(0, 1);
        }
        this.eventListeners = [];
    };
    return RxFormControlDirective;
}(BaseValidator));
RxFormControlDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: '[ngModel],[formControlName],[formControl]',
                providers: [NGMODEL_BINDING],
            },] },
];
/**
 * @nocollapse
 */
RxFormControlDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer"], },
    { type: DecimalProvider, },
]; };
RxFormControlDirective.propDecorators = {
    'allOf': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'alpha': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'alphaNumeric': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'ascii': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'choice': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'compare': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'compose': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'contains': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'creditCard': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'dataUri': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'different': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'digit': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'email': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'endsWith': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'even': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'extension': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'factor': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'fileSize': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'greaterThanEqualTo': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'greaterThan': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'hexColor': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'json': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'latitude': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'latLong': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'leapYear': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'lessThan': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'lessThanEqualTo': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'longitude': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'lowerCase': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'mac': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'maxDate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'maxLength': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'maxNumber': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'minDate': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'minLength': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'minNumber': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'noneOf': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'numeric': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'odd': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'oneOf': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'password': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'pattern': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'port': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'primeNumber': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'required': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'range': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'rule': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'startsWith': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'time': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'upperCase': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'url': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'unique': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'notEmpty': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'cusip': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'grid': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var VALIDATOR_CONFIG = "validatorConfig";
var FILE_VALIDATOR_NAMES = ["extension", "fileSize", "file"];
var FileControlDirective = /** @class */ (function () {
    /**
     * @param {?} elementRef
     */
    function FileControlDirective(elementRef) {
        this.elementRef = elementRef;
        this.isProcessed = false;
        this.validators = [];
        this.element = elementRef.nativeElement;
    }
    Object.defineProperty(FileControlDirective.prototype, "extension", {
        /**
         * @param {?} config
         * @return {?}
         */
        set: function (config) {
            this.pushValidator(FILE_VALIDATOR_NAMES[0], config);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileControlDirective.prototype, "fileSize", {
        /**
         * @param {?} config
         * @return {?}
         */
        set: function (config) {
            this.pushValidator(FILE_VALIDATOR_NAMES[1], config);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileControlDirective.prototype, "file", {
        /**
         * @param {?} config
         * @return {?}
         */
        set: function (config) {
            this.pushValidator(FILE_VALIDATOR_NAMES[2], config);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} control
     * @return {?}
     */
    FileControlDirective.prototype.setConfig = function (control) {
        var _this = this;
        FILE_VALIDATOR_NAMES.forEach(function (t) {
            if (!_this[t] && control[VALIDATOR_CONFIG] && control[VALIDATOR_CONFIG][t])
                _this[t] = control[VALIDATOR_CONFIG][t];
        });
        this.isProcessed = true;
    };
    /**
     * @param {?} validatorName
     * @param {?} config
     * @return {?}
     */
    FileControlDirective.prototype.pushValidator = function (validatorName, config) {
        if (config)
            this.validators.push(APP_VALIDATORS[validatorName](config));
    };
    /**
     * @param {?} control
     * @return {?}
     */
    FileControlDirective.prototype.validate = function (control) {
        if (!this.isProcessed)
            this.setConfig(control);
        var /** @type {?} */ result = null;
        for (var _i = 0, _a = this.validators; _i < _a.length; _i++) {
            var validator = _a[_i];
            result = validator(control, this.element.files);
            if (result)
                break;
        }
        return result;
    };
    return FileControlDirective;
}());
FileControlDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: "input[type=file]",
                providers: [{
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALIDATORS"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return FileControlDirective; }),
                        multi: true
                    }]
            },] },
];
/**
 * @nocollapse
 */
FileControlDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
]; };
FileControlDirective.propDecorators = {
    'extension': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'fileSize': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
    'file': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var VALIDATOR_CONFIG$1 = "validatorConfig";
var ImageFileControlDirective = /** @class */ (function () {
    /**
     * @param {?} elementRef
     */
    function ImageFileControlDirective(elementRef) {
        this.elementRef = elementRef;
        this.isProcessed = false;
        this.element = elementRef.nativeElement;
    }
    Object.defineProperty(ImageFileControlDirective.prototype, "image", {
        /**
         * @param {?} config
         * @return {?}
         */
        set: function (config) {
            this.imageValidation = APP_VALIDATORS.image(config);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} control
     * @return {?}
     */
    ImageFileControlDirective.prototype.setConfig = function (control) {
        var /** @type {?} */ image = "image";
        if (!this[image] && control[VALIDATOR_CONFIG$1] && control[VALIDATOR_CONFIG$1][image])
            this[image] = control[VALIDATOR_CONFIG$1][image];
        this.isProcessed = true;
    };
    /**
     * @param {?} control
     * @return {?}
     */
    ImageFileControlDirective.prototype.validate = function (control) {
        if (!this.isProcessed)
            this.setConfig(control);
        if (this.imageValidation) {
            return this.imageValidation(control, this.element.files);
        }
        return new Promise(function (resolve, reject) { resolve(null); });
    };
    return ImageFileControlDirective;
}());
ImageFileControlDirective.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"], args: [{
                selector: "input[type=file]",
                providers: [{
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_ASYNC_VALIDATORS"],
                        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(function () { return ImageFileControlDirective; }),
                        multi: true
                    }]
            },] },
];
/**
 * @nocollapse
 */
ImageFileControlDirective.ctorParameters = function () { return [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"], },
]; };
ImageFileControlDirective.propDecorators = {
    'image': [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] },],
};
var RxReactiveFormsModule = /** @class */ (function () {
    function RxReactiveFormsModule() {
    }
    /**
     * @return {?}
     */
    RxReactiveFormsModule.forRoot = function () { return { ngModule: RxReactiveFormsModule, providers: [] }; };
    return RxReactiveFormsModule;
}());
RxReactiveFormsModule.decorators = [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                declarations: [RxwebFormDirective, RxwebDynamicFormComponent, HtmlControlTemplateDirective, RxwebControlComponent, ControlHostDirective, RxFormControlDirective, FileControlDirective, ImageFileControlDirective],
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]],
                providers: [RxFormBuilder, DecimalProvider, _angular_common__WEBPACK_IMPORTED_MODULE_1__["DecimalPipe"]],
                exports: [RxwebFormDirective, RxwebDynamicFormComponent, HtmlControlTemplateDirective, RxwebControlComponent, RxFormControlDirective, FileControlDirective, ImageFileControlDirective]
            },] },
];
/**
 * @nocollapse
 */
RxReactiveFormsModule.ctorParameters = function () { return []; };
/**
 * @param {?} annotationType
 * @param {?} config
 * @param {?=} isAsync
 * @return {?}
 */
function baseDecoratorFunction(annotationType, config, isAsync) {
    if (isAsync === void 0) { isAsync = false; }
    return function (target, propertyKey, parameterIndex) {
        defaultContainer.init(target, parameterIndex, propertyKey, annotationType, config, isAsync);
    };
}
/**
 * @param {?=} config
 * @return {?}
 */
function alpha(config) {
    return baseDecoratorFunction(AnnotationTypes.alpha, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function alphaNumeric(config) {
    return baseDecoratorFunction(AnnotationTypes.alphaNumeric, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function compare(config) {
    return baseDecoratorFunction(AnnotationTypes.compare, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function contains(config) {
    return baseDecoratorFunction(AnnotationTypes.contains, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function creditCard(config) {
    return baseDecoratorFunction(AnnotationTypes.creditCard, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function digit(config) {
    return baseDecoratorFunction(AnnotationTypes.digit, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function email(config) {
    return baseDecoratorFunction(AnnotationTypes.email, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function hexColor(config) {
    return baseDecoratorFunction(AnnotationTypes.hexColor, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function lowerCase(config) {
    return baseDecoratorFunction(AnnotationTypes.lowerCase, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function maxDate(config) {
    return baseDecoratorFunction(AnnotationTypes.maxDate, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function maxLength(config) {
    return baseDecoratorFunction(AnnotationTypes.maxLength, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function minDate(config) {
    return baseDecoratorFunction(AnnotationTypes.minDate, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function maxNumber(config) {
    return baseDecoratorFunction(AnnotationTypes.maxNumber, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function minLength(config) {
    return baseDecoratorFunction(AnnotationTypes.minLength, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function minNumber(config) {
    return baseDecoratorFunction(AnnotationTypes.minNumber, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function password(config) {
    return baseDecoratorFunction(AnnotationTypes.password, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function pattern(config) {
    return baseDecoratorFunction(AnnotationTypes.pattern, config);
}
/**
 * @template T
 * @param {?} entity
 * @return {?}
 */
function propArray(entity) {
    return function (target, propertyKey, parameterIndex) {
        var /** @type {?} */ propertyInfo = {
            name: propertyKey,
            propertyType: ARRAY_PROPERTY,
            entity: entity
        };
        defaultContainer.addProperty(target.constructor, propertyInfo);
    };
}
/**
 * @template T
 * @param {?} entity
 * @return {?}
 */
function propObject(entity) {
    return function (target, propertyKey, parameterIndex) {
        defaultContainer.initPropertyObject(propertyKey, OBJECT_PROPERTY, entity, target);
    };
}
/**
 * @return {?}
 */
function prop() {
    return function (target, propertyKey, parameterIndex) {
        var /** @type {?} */ propertyInfo = {
            name: propertyKey,
            propertyType: PROPERTY
        };
        defaultContainer.addProperty(target.constructor, propertyInfo);
    };
}
/**
 * @param {?} config
 * @return {?}
 */
function range(config) {
    return baseDecoratorFunction(AnnotationTypes.range, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function required(config) {
    return baseDecoratorFunction(AnnotationTypes.required, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function upperCase(config) {
    return baseDecoratorFunction(AnnotationTypes.upperCase, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function time(config) {
    return baseDecoratorFunction(AnnotationTypes.time, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function url(config) {
    return baseDecoratorFunction(AnnotationTypes.url, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function json(config) {
    return baseDecoratorFunction(AnnotationTypes.json, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function greaterThan(config) {
    return baseDecoratorFunction(AnnotationTypes.greaterThan, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function greaterThanEqualTo(config) {
    return baseDecoratorFunction(AnnotationTypes.greaterThanEqualTo, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function lessThanEqualTo(config) {
    return baseDecoratorFunction(AnnotationTypes.lessThanEqualTo, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function lessThan(config) {
    return baseDecoratorFunction(AnnotationTypes.lessThan, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function choice(config) {
    return baseDecoratorFunction(AnnotationTypes.choice, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function different(config) {
    return baseDecoratorFunction(AnnotationTypes.different, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function numeric(config) {
    return baseDecoratorFunction(AnnotationTypes.numeric, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function even(config) {
    return baseDecoratorFunction(AnnotationTypes.even, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function odd(config) {
    return baseDecoratorFunction(AnnotationTypes.odd, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function factor(config) {
    return baseDecoratorFunction(AnnotationTypes.factor, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function leapYear(config) {
    return baseDecoratorFunction(AnnotationTypes.leapYear, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function allOf(config) {
    return baseDecoratorFunction(AnnotationTypes.allOf, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function oneOf(config) {
    return baseDecoratorFunction(AnnotationTypes.oneOf, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function noneOf(config) {
    return baseDecoratorFunction(AnnotationTypes.noneOf, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function mac(config) {
    return baseDecoratorFunction(AnnotationTypes.mac, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function ascii(config) {
    return baseDecoratorFunction(AnnotationTypes.ascii, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function dataUri(config) {
    return baseDecoratorFunction(AnnotationTypes.dataUri, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function port(config) {
    return baseDecoratorFunction(AnnotationTypes.port, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function latLong(config) {
    return baseDecoratorFunction(AnnotationTypes.latLong, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function extension(config) {
    return baseDecoratorFunction(AnnotationTypes.extension, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function fileSize(config) {
    return baseDecoratorFunction(AnnotationTypes.fileSize, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function endsWith(config) {
    return baseDecoratorFunction(AnnotationTypes.endsWith, config);
}
/**
 * @param {?} config
 * @return {?}
 */
function startsWith(config) {
    return baseDecoratorFunction(AnnotationTypes.startsWith, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function primeNumber(config) {
    return baseDecoratorFunction(AnnotationTypes.primeNumber, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function latitude(config) {
    return baseDecoratorFunction(AnnotationTypes.latitude, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function longitude(config) {
    return baseDecoratorFunction(AnnotationTypes.longitude, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function rule(config) {
    return baseDecoratorFunction(AnnotationTypes.rule, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function file(config) {
    return baseDecoratorFunction(AnnotationTypes.file, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function custom(config) {
    return baseDecoratorFunction(AnnotationTypes.custom, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function unique(config) {
    return baseDecoratorFunction(AnnotationTypes.unique, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function image(config) {
    return baseDecoratorFunction(AnnotationTypes.image, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function notEmpty(config) {
    return baseDecoratorFunction(AnnotationTypes.notEmpty, config);
}
/**
 * @param {?} validators
 * @return {?}
 */
function async(validators) {
    return baseDecoratorFunction(AnnotationTypes.async, validators, true);
}
/**
 * @param {?=} config
 * @return {?}
 */
function cusip(config) {
    return baseDecoratorFunction(AnnotationTypes.cusip, config);
}
/**
 * @param {?=} config
 * @return {?}
 */
function grid(config) {
    return baseDecoratorFunction(AnnotationTypes.grid, config);
}
/**
 * @param {?} config
 * @param {?} type
 * @param {?} validator
 * @return {?}
 */
function baseValidator(config, type, validator) {
    var /** @type {?} */ rxwebValidator = function (control, target) {
        if (typeof control == STRING)
            defaultContainer.init(target, 0, control, type, config, false);
        else
            return ApplicationUtil.configureControl(control, config, type), validator(control);
        return null;
    };
    return rxwebValidator;
}
/**
 * @param {?=} config
 * @return {?}
 */
function alphaValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.alpha, alphaValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function allOfValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.allOf, allOfValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function alphaNumericValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.alphaNumeric, alphaNumericValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function choiceValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.choice, choiceValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function compareValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.compare, compareValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function containsValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.contains, containsValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function creditCardValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.creditCard, creditCardValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function differentValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.different, differentValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function digitValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.digit, digitValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function emailValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.email, emailValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function evenValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.even, evenValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function factorValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.factor, factorValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function greaterThanEqualToValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.greaterThanEqualTo, greaterThanEqualToValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function greaterThanValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.greaterThan, greaterThanValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function hexColorValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.hexColor, hexColorValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function jsonValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.json, jsonValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function leapYearValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.leapYear, leapYearValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function lessThanEqualToValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.lessThanEqualTo, lessThanEqualToValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function lessThanValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.lessThan, lessThanValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function lowerCaseValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.lowerCase, lowercaseValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function macValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.mac, macValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function maxDateValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.maxDate, maxDateValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function maxLengthValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.maxLength, maxLengthValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function maxNumberValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.maxNumber, maxNumberValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function minDateValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.minDate, minDateValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function minLengthValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.minLength, minLengthValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function minNumberValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.minNumber, minNumberValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function noneOfValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.noneOf, noneOfValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function numericValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.numeric, numericValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function oddValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.odd, oddValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function oneOfValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.oneOf, oneOfValidator(config));
}
/**
 * @param {?} config
 * @return {?}
 */
function passwordcValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.password, passwordValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function patternValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.pattern, patternValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function rangeValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.range, rangeValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function requiredValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.required, requiredValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function timeValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.time, timeValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function upperCaseValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.upperCase, uppercaseValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function urlValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.url, urlValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function asciiValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.ascii, asciiValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function dataUriValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.dataUri, dataUriValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function portValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.port, portValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function latLongValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.latLong, latLongValidator(config));
}
/**
 * @param {?} config
 * @return {?}
 */
function extensionValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.extension, function (control) { return null; });
}
/**
 * @param {?} config
 * @return {?}
 */
function fileSizeValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.fileSize, function (control) { return null; });
}
/**
 * @param {?} config
 * @return {?}
 */
function endsWithValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.endsWith, endsWithValidator(config));
}
/**
 * @param {?} config
 * @return {?}
 */
function startsWithValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.startsWithWith, startsWithValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function primeNumberValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.primeNumber, primeNumberValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function latitudeValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.latitude, latitudeValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function longitudeValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.longitude, longitudeValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function composeValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.compose, composeValidator(config));
}
/**
 * @param {?} config
 * @return {?}
 */
function fileValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.file, function (control) { return null; });
}
/**
 * @param {?=} config
 * @return {?}
 */
function customValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.custom, customValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function uniqueValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.unique, uniqueValidator(config));
}
/**
 * @param {?} config
 * @return {?}
 */
function imageValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.image, function (control) { return null; });
}
/**
 * @param {?=} config
 * @return {?}
 */
function notEmptyValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.notEmpty, notEmptyValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function ipValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.ip, ipValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function cusipValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.cusip, cusipValidator(config));
}
/**
 * @param {?=} config
 * @return {?}
 */
function gridValidatorExtension(config) {
    return baseValidator(config, AnnotationTypes.grid, gridValidator(config));
}
var RxwebValidators = /** @class */ (function () {
    function RxwebValidators() {
    }
    return RxwebValidators;
}());
RxwebValidators.alpha = alphaValidatorExtension;
RxwebValidators.allOf = allOfValidatorExtension;
RxwebValidators.alphaNumeric = alphaNumericValidatorExtension;
RxwebValidators.choice = choiceValidatorExtension;
RxwebValidators.compare = compareValidatorExtension;
RxwebValidators.contains = containsValidatorExtension;
RxwebValidators.creditCard = creditCardValidatorExtension;
RxwebValidators.different = differentValidatorExtension;
RxwebValidators.digit = digitValidatorExtension;
RxwebValidators.email = emailValidatorExtension;
RxwebValidators.even = evenValidatorExtension;
RxwebValidators.factor = factorValidatorExtension;
RxwebValidators.greaterThanEqualTo = greaterThanEqualToValidatorExtension;
RxwebValidators.greaterThan = greaterThanValidatorExtension;
RxwebValidators.hexColor = hexColorValidatorExtension;
RxwebValidators.json = jsonValidatorExtension;
RxwebValidators.leapYear = leapYearValidatorExtension;
RxwebValidators.lessThanEqualTo = lessThanEqualToValidatorExtension;
RxwebValidators.lessThan = lessThanValidatorExtension;
RxwebValidators.lowerCase = lowerCaseValidatorExtension;
RxwebValidators.mac = macValidatorExtension;
RxwebValidators.maxDate = maxDateValidatorExtension;
RxwebValidators.maxLength = maxLengthValidatorExtension;
RxwebValidators.maxNumber = maxNumberValidatorExtension;
RxwebValidators.minDate = minDateValidatorExtension;
RxwebValidators.minLength = minLengthValidatorExtension;
RxwebValidators.minNumber = minNumberValidatorExtension;
RxwebValidators.noneOf = noneOfValidatorExtension;
RxwebValidators.numeric = numericValidatorExtension;
RxwebValidators.odd = oddValidatorExtension;
RxwebValidators.oneOf = oneOfValidatorExtension;
RxwebValidators.password = passwordcValidatorExtension;
RxwebValidators.pattern = patternValidatorExtension;
RxwebValidators.range = rangeValidatorExtension;
RxwebValidators.required = requiredValidatorExtension;
RxwebValidators.time = timeValidatorExtension;
RxwebValidators.upperCase = upperCaseValidatorExtension;
RxwebValidators.url = urlValidatorExtension;
RxwebValidators.ascii = asciiValidatorExtension;
RxwebValidators.dataUri = dataUriValidatorExtension;
RxwebValidators.port = portValidatorExtension;
RxwebValidators.latLong = latLongValidatorExtension;
RxwebValidators.extension = extensionValidatorExtension;
RxwebValidators.fileSize = fileSizeValidatorExtension;
RxwebValidators.endsWith = endsWithValidatorExtension;
RxwebValidators.startsWith = startsWithValidatorExtension;
RxwebValidators.primeNumber = primeNumberValidatorExtension;
RxwebValidators.latitude = latitudeValidatorExtension;
RxwebValidators.longitude = longitudeValidatorExtension;
RxwebValidators.compose = composeValidatorExtension;
RxwebValidators.file = fileValidatorExtension;
RxwebValidators.custom = customValidatorExtension;
RxwebValidators.unique = uniqueValidatorExtension;
RxwebValidators.image = imageValidatorExtension;
RxwebValidators.notEmpty = notEmptyValidatorExtension;
RxwebValidators.ip = ipValidatorExtension;
RxwebValidators.cusip = cusipValidatorExtension;
RxwebValidators.grid = gridValidatorExtension;
/**
 * Generated bundle index. Do not edit.
 */

//# sourceMappingURL=reactive-form-validators.es5.js.map


/***/ }),

/***/ "./src/app/feature-modules/users/components/login/login.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/feature-modules/users/components/login/login.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form [formGroup]=\"loginForm\" novalidate (ngSubmit)=\"onSubmit()\">\r\n    <mat-card class=\"login-form\">\r\n        <mat-card-header>\r\n            <mat-card-title>Login to Casting WEb Book</mat-card-title>\r\n        </mat-card-header>\r\n        <mat-card-content>\r\n            <p>\r\n                <mat-form-field appearance=\"outline\">\r\n                    <input matInput placeholder=\"Enter Username\" formControlName=\"userName\">\r\n                    <mat-error *ngIf=\"loginForm.controls['userName'].hasError('required')\">\r\n                        Username is <strong>required</strong>\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </p>\r\n            <p>\r\n                <mat-form-field appearance=\"outline\">\r\n                    <input matInput type=\"password\" placeholder=\"Password\" formControlName=\"password\">\r\n                    <mat-error *ngIf=\"loginForm.controls['password'].hasError('required')\">\r\n                        Password is <strong>required</strong>\r\n                    </mat-error>\r\n                </mat-form-field>\r\n            </p>\r\n        </mat-card-content>\r\n        <mat-card-actions>\r\n            <button mat-raised-button color=\"primary\" type=\"submit\">Submit</button>\r\n        </mat-card-actions>\r\n    </mat-card>\r\n</form>"

/***/ }),

/***/ "./src/app/feature-modules/users/components/login/login.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/feature-modules/users/components/login/login.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-form {\n  width: 80%;\n  min-width: 120px;\n  margin: 20px auto; }\n\n.mat-radio-button {\n  display: block;\n  margin: 5px 0; }\n\n.row {\n  display: flex;\n  flex-direction: row; }\n\n.col {\n  flex: 1;\n  margin-right: 20px; }\n\n.col:last-child {\n  margin-right: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3VzZXJzL2NvbXBvbmVudHMvbG9naW4vRTpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xcdXNlcnNcXGNvbXBvbmVudHNcXGxvZ2luXFxsb2dpbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJQTtFQUNFLFVBQVU7RUFDVixnQkFBZ0I7RUFDaEIsaUJBQWlCLEVBQUE7O0FBR25CO0VBQ0UsY0FBYztFQUNkLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGFBQWE7RUFDYixtQkFBbUIsRUFBQTs7QUFHckI7RUFDRSxPQUFPO0VBQ1Asa0JBQWtCLEVBQUE7O0FBR3BCO0VBQ0UsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3VzZXJzL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC13aWR0aCB7XHJcbiBcclxufVxyXG5cclxuLmxvZ2luLWZvcm0ge1xyXG4gIHdpZHRoOiA4MCU7XHJcbiAgbWluLXdpZHRoOiAxMjBweDtcclxuICBtYXJnaW46IDIwcHggYXV0bztcclxufVxyXG5cclxuLm1hdC1yYWRpby1idXR0b24ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogNXB4IDA7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLmNvbCB7XHJcbiAgZmxleDogMTtcclxuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5jb2w6bGFzdC1jaGlsZCB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwO1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/feature-modules/users/components/login/login.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/feature-modules/users/components/login/login.component.ts ***!
  \***************************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/authentication.service */ "./src/app/services/authentication.service.ts");





//import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(_formBuilder, authenticationService, stateService) {
        this._formBuilder = _formBuilder;
        this.authenticationService = authenticationService;
        this.stateService = stateService;
        this.submitted = false;
        this.credentials = { username: null, password: null };
    }
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        this.submitting = true;
        var showError = function (errorMessage) {
            return _this.errorMessage = errorMessage;
        };
        var stop = function () { return _this.submitting = false; };
        this.authenticationService.login(this.loginForm.value.userName, this.loginForm.value.password)
            .then(function (r) {
            alert("good");
            _this.stateService.go('productions');
        })
            .catch(function (error) {
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
        this.loginForm = this._formBuilder.group({
            userName: ['bobob', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['Brazil99?', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/feature-modules/users/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/feature-modules/users/components/login/login.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"], _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/users/components/register/register.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/feature-modules/users/components/register/register.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"login-form\">\r\n    <mat-card-header>\r\n        <mat-card-title>Login to Casting WEb Book</mat-card-title>\r\n    </mat-card-header>\r\n    <mat-card-content>\r\n        <mat-vertical-stepper [linear]=\"true\" #stepper (selectionChange)=\"stepChanged($event, stepper);\">\r\n            <mat-step [stepControl]=\"basicUserInfo\">\r\n                <form [formGroup]=\"basicUserInfo\">\r\n                    <ng-template matStepLabel>Fill out your basic user information</ng-template>\r\n                  <div class=\"row\">\r\n                  <div class=\"col\">\r\n\r\n                        <mat-form-field class=\"full-width email\" appearance=\"outline\">\r\n                            <mat-label>Your email</mat-label>\r\n                            <input matInput placeholder=\"Email\" type=\"email\" formControlName=\"email\">\r\n                            <mat-spinner *ngIf=\"isSearchingEmail\" diameter=\"20\" matSuffix></mat-spinner>\r\n                            <mat-icon color=\"warn\" matSuffix *ngIf=\"(emailHasBeenChecked && !emailIsAvailable && !isSearchingEmail)\">error</mat-icon>\r\n                            <mat-icon color=\"primary\" matSuffix *ngIf=\"(emailHasBeenChecked && emailIsAvailable && !isSearchingEmail)\">check_circle</mat-icon>\r\n                            <mat-error *ngIf=\"basicUserInfo.controls['email'].hasError('emailTaken')\">\r\n                              Email is <strong>already in use</strong>\r\n                            </mat-error>\r\n                            <mat-error *ngIf=\"basicUserInfo.controls['email'].hasError('required')\">\r\n                              Email is <strong>required</strong>\r\n                            </mat-error>\r\n                            <mat-error *ngIf=\"basicUserInfo.controls['email'].hasError('email')\">\r\n                              Email format is <strong>incorrect</strong>\r\n                            </mat-error>\r\n                          <mat-hint align=\"start\">e.g. name@company.com</mat-hint>\r\n                        </mat-form-field>\r\n                  </div>\r\n                  </div>\r\n\r\n                  <div class=\"row\">\r\n                  <div class=\"col\">\r\n\r\n                        <mat-form-field class=\"full-width names\" appearance=\"outline\">\r\n                            <mat-label>Your first names</mat-label>\r\n                            <input matInput placeholder=\"First names\" formControlName=\"firstNames\">\r\n                            <mat-error *ngIf=\"basicUserInfo.controls['firstNames'].hasError('required')\">\r\n                                Some first name is <strong>required</strong>\r\n                            </mat-error>\r\n                            <mat-hint align=\"start\">e.g. Billy Ray Bob</mat-hint>\r\n                        </mat-form-field>\r\n                  </div>\r\n                  </div>\r\n\r\n                  <div class=\"row\">\r\n                  <div class=\"col\">\r\n\r\n                        <mat-form-field class=\"full-width names\" appearance=\"outline\">\r\n                            <mat-label>Your last names</mat-label>\r\n                            <input matInput placeholder=\"Last names\" formControlName=\"lastNames\">\r\n                            <mat-error *ngIf=\"basicUserInfo.controls['lastNames'].hasError('required')\">\r\n                                Some last name is <strong>required</strong>\r\n                            </mat-error>\r\n                            <mat-hint align=\"start\">e.g. Cohen Johnson De La Renta</mat-hint>\r\n                        </mat-form-field>\r\n                  </div>\r\n                  </div>\r\n\r\n                  <div class=\"row\">\r\n                  <div class=\"col\">\r\n\r\n                        <mat-form-field class=\"full-width roles\" appearance=\"outline\">\r\n                            <mat-label>Your role in the production-role-casting industry</mat-label>\r\n                          <mat-select placeholder=\"Your role\" formControlName=\"role\" required>\r\n                            <mat-option>--</mat-option>\r\n                            <mat-optgroup *ngFor=\"let group of roleGroups\" [label]=\"group.name\">\r\n                              <mat-option *ngFor=\"let role of group.roles\" [value]=\"role.roleId\">\r\n                                {{role.name}}\r\n                              </mat-option>\r\n                             \r\n                            </mat-optgroup>\r\n                            <mat-option value=\"1\">Other</mat-option>\r\n\r\n                          </mat-select>\r\n                          <mat-error *ngIf=\"basicUserInfo.controls['role'].hasError('required')\">\r\n                            Your role selection is <strong>required</strong>\r\n                          </mat-error>\r\n                          <mat-hint align=\"end\">Select a role</mat-hint>\r\n                          </mat-form-field>\r\n                  </div>\r\n                  </div>\r\n\r\n                  <div class=\"row\">\r\n                  <div class=\"col\">\r\n\r\n                        <mat-form-field *ngIf=\"roleOtherIsShowed\" class=\"full-width roleOther\" appearance=\"outline\">\r\n                            <mat-label align=\"end\">Tell us what exactly</mat-label>\r\n                          <input matInput placeholder=\"Specify\" formControlName=\"roleOther\">\r\n                            <mat-error *ngIf=\"basicUserInfo.controls['roleOther'].hasError('required')\">\r\n                                Telling us something is <strong>required</strong>\r\n                            </mat-error>\r\n                            <mat-hint align=\"start\">Specify a bit more about what you do</mat-hint>\r\n                        </mat-form-field>\r\n                  </div>\r\n                  </div>\r\n\r\n                    <div>\r\n                      <button (click)=\"test\" >go</button>\r\n                      <button mat-raised-button color=\"primary\" matStepperNext>Next</button>\r\n                    </div>\r\n                </form>\r\n            </mat-step>\r\n           \r\n            <mat-step [stepControl]=\"userLoginInfo\">\r\n                <form [formGroup]=\"userLoginInfo\">\r\n                    <ng-template matStepLabel>Fill out your address</ng-template>\r\n                     <div class=\"row\">\r\n                        <div class=\"col\">\r\n                            <mat-form-field class=\"full-width username\" appearance=\"outline\">\r\n                                <mat-label>Choose your username</mat-label>\r\n                                <input matInput placeholder=\"Username\" type=\"text\" formControlName=\"username\">\r\n                                <mat-spinner *ngIf=\"isSearchingUsername\" diameter=\"20\" matSuffix></mat-spinner>\r\n                                <mat-icon color=\"warn\" matSuffix *ngIf=\"(usernameHasBeenChecked && !usernameIsAvailable && !isSearchingUsername)\">error</mat-icon>\r\n                                <mat-icon color=\"primary\" matSuffix *ngIf=\"(usernameHasBeenChecked && usernameIsAvailable && !isSearchingUsername)\">check_circle</mat-icon>\r\n                                <mat-error *ngIf=\"userLoginInfo.controls['username'].hasError('usernameTaken')\">\r\n                                    Username is <strong>already in use</strong>\r\n                                </mat-error>\r\n                                <mat-error *ngIf=\"userLoginInfo.controls['username'].hasError('required')\">\r\n                                    Username is <strong>required</strong>\r\n                                </mat-error>\r\n                                <mat-hint align=\"start\">e.g. bobthepanther</mat-hint>\r\n                            </mat-form-field>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                    <div class=\"col\">        <mat-form-field class=\"full-width password\" appearance=\"outline\">\r\n                                    <mat-label>Chose a password</mat-label>\r\n                                    <input matInput placeholder=\"Your new password\" formControlName=\"password\">\r\n                                    <mat-error *ngIf=\"userLoginInfo.controls['password'].hasError('required')\">\r\n                                        Password is <strong>required</strong>\r\n                                    </mat-error>\r\n                                    <mat-error *ngIf=\"userLoginInfo.controls['password'].hasError('minlength')\">\r\n                                        Password must be 5 characters long <strong>required</strong>\r\n                                    </mat-error>\r\n                                    <mat-hint align=\"start\">Your new password</mat-hint>\r\n                                </mat-form-field>\r\n                    </div>\r\n                    </div>\r\n\r\n                    <div class=\"row\">\r\n                      <div class=\"col\">        \r\n                        <mat-form-field class=\"full-width password\" appearance=\"outline\">\r\n                                    <mat-label>Re-enter your chosen password</mat-label>\r\n                                    <input matInput placeholder=\"Re-enter password\" formControlName=\"passwordCompare\">\r\n                                    <mat-error *ngIf=\"userLoginInfo.controls['passwordCompare'].hasError('required')\">\r\n                                        Password repeat is <strong>required</strong>\r\n                                    </mat-error>\r\n                                    <mat-error *ngIf=\"userLoginInfo.controls['passwordCompare'].hasError('compare')\">\r\n                                        Passwords <strong>must match</strong>\r\n                                    </mat-error>\r\n                                    <mat-hint align=\"start\">Re-enter password</mat-hint>\r\n                                </mat-form-field>\r\n                    </div>\r\n                    </div>\r\n                 <div>\r\n                                <button mat-raised-button matStepperPrevious>Back</button>\r\n                                <button mat-raised-button color=\"primary\" (click)=\"register(stepper)\">Register</button>\r\n                            </div>\r\n                </form>\r\n            </mat-step>\r\n\r\n\r\n          <mat-step>\r\n                <ng-template matStepLabel>Done</ng-template>\r\n                You are now done.\r\n                <div>\r\n                     <button mat-raised-button (click)=\"stepper.reset()\">Reset</button>\r\n                </div>\r\n            </mat-step>\r\n        </mat-vertical-stepper>\r\n    </mat-card-content>\r\n    <mat-card-actions>\r\n\r\n    </mat-card-actions>\r\n</mat-card>"

/***/ }),

/***/ "./src/app/feature-modules/users/components/register/register.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/feature-modules/users/components/register/register.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-form-field.email {\n  /*   display: flex;\r\n max-width: 350px;*/ }\n\n.mat-form-field.names {\n  /*   max-width: 300px;*/ }\n\n.mat-form-field.roleOther {\n  /*  max-width: 250px;*/ }\n\n.mat-form-field.roles {\n  /* max-width: 250px;*/ }\n\n.mat-form-field.password {\n  /* max-width: 250px;*/ }\n\n.mat-form-field.username {\n  /* max-width: 250px;*/ }\n\n.full-width {\n  width: 100%;\n  max-width: 350px; }\n\n.row {\n  display: flex;\n  flex-direction: row; }\n\n.col {\n  flex: 1;\n  margin-right: 20px; }\n\n.col:last-child {\n  margin-right: 0; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3VzZXJzL2NvbXBvbmVudHMvcmVnaXN0ZXIvRTpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xcdXNlcnNcXGNvbXBvbmVudHNcXHJlZ2lzdGVyXFxyZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3VzZXJzL2NvbXBvbmVudHMvcmVnaXN0ZXIvcmVnaXN0ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUM7RUFDRTttQkNDZ0IsRURBQzs7QUFFbkI7RUFDRSx1QkFBQSxFQUF3Qjs7QUFFMUI7RUFDRSxzQkFBQSxFQUF1Qjs7QUFHekI7RUFDRSxxQkFBQSxFQUFzQjs7QUFHeEI7RUFDRSxxQkFBQSxFQUFzQjs7QUFHeEI7RUFDQyxxQkFBQSxFQUFzQjs7QUFJdkI7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsYUFBYTtFQUNiLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLE9BQU87RUFDUCxrQkFBa0IsRUFBQTs7QUFFcEI7RUFDRSxlQUFlLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9mZWF0dXJlLW1vZHVsZXMvdXNlcnMvY29tcG9uZW50cy9yZWdpc3Rlci9yZWdpc3Rlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiAubWF0LWZvcm0tZmllbGQuZW1haWwgIHtcclxuICAgLyogICBkaXNwbGF5OiBmbGV4O1xyXG4gbWF4LXdpZHRoOiAzNTBweDsqL1xyXG4gfVxyXG4gLm1hdC1mb3JtLWZpZWxkLm5hbWVzICB7XHJcbiAgIC8qICAgbWF4LXdpZHRoOiAzMDBweDsqL1xyXG4gfVxyXG4gLm1hdC1mb3JtLWZpZWxkLnJvbGVPdGhlciAge1xyXG4gICAvKiAgbWF4LXdpZHRoOiAyNTBweDsqL1xyXG4gfVxyXG4gXHJcbiAubWF0LWZvcm0tZmllbGQucm9sZXMgIHtcclxuICAgLyogbWF4LXdpZHRoOiAyNTBweDsqL1xyXG4gfVxyXG5cclxuIC5tYXQtZm9ybS1maWVsZC5wYXNzd29yZCAge1xyXG4gICAvKiBtYXgtd2lkdGg6IDI1MHB4OyovXHJcbiB9XHJcblxyXG4gLm1hdC1mb3JtLWZpZWxkLnVzZXJuYW1lICB7XHJcbiAgLyogbWF4LXdpZHRoOiAyNTBweDsqL1xyXG4gfVxyXG5cclxuIFxyXG4gLmZ1bGwtd2lkdGgge1xyXG4gICB3aWR0aDogMTAwJTtcclxuICAgbWF4LXdpZHRoOiAzNTBweDtcclxuIH1cclxuXHJcbiAucm93IHtcclxuICAgZGlzcGxheTogZmxleDtcclxuICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuIH1cclxuXHJcbiAuY29sIHtcclxuICAgZmxleDogMTtcclxuICAgbWFyZ2luLXJpZ2h0OiAyMHB4O1xyXG4gfVxyXG4gLmNvbDpsYXN0LWNoaWxkIHtcclxuICAgbWFyZ2luLXJpZ2h0OiAwO1xyXG4gfVxyXG4iLCIubWF0LWZvcm0tZmllbGQuZW1haWwge1xuICAvKiAgIGRpc3BsYXk6IGZsZXg7XHJcbiBtYXgtd2lkdGg6IDM1MHB4OyovIH1cblxuLm1hdC1mb3JtLWZpZWxkLm5hbWVzIHtcbiAgLyogICBtYXgtd2lkdGg6IDMwMHB4OyovIH1cblxuLm1hdC1mb3JtLWZpZWxkLnJvbGVPdGhlciB7XG4gIC8qICBtYXgtd2lkdGg6IDI1MHB4OyovIH1cblxuLm1hdC1mb3JtLWZpZWxkLnJvbGVzIHtcbiAgLyogbWF4LXdpZHRoOiAyNTBweDsqLyB9XG5cbi5tYXQtZm9ybS1maWVsZC5wYXNzd29yZCB7XG4gIC8qIG1heC13aWR0aDogMjUwcHg7Ki8gfVxuXG4ubWF0LWZvcm0tZmllbGQudXNlcm5hbWUge1xuICAvKiBtYXgtd2lkdGg6IDI1MHB4OyovIH1cblxuLmZ1bGwtd2lkdGgge1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LXdpZHRoOiAzNTBweDsgfVxuXG4ucm93IHtcbiAgZGlzcGxheTogZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdzsgfVxuXG4uY29sIHtcbiAgZmxleDogMTtcbiAgbWFyZ2luLXJpZ2h0OiAyMHB4OyB9XG5cbi5jb2w6bGFzdC1jaGlsZCB7XG4gIG1hcmdpbi1yaWdodDogMDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/feature-modules/users/components/register/register.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/feature-modules/users/components/register/register.component.ts ***!
  \*********************************************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _rxweb_reactive_form_validators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @rxweb/reactive-form-validators */ "./node_modules/@rxweb/reactive-form-validators/@rxweb/reactive-form-validators.es5.js");





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(_formBuilder, authenticationService) {
        this._formBuilder = _formBuilder;
        this.authenticationService = authenticationService;
        //email checking
        this.currentEmailValue = "";
        this.isSearchingEmail = false;
        this.emailIsAvailable = false;
        this.emailHasBeenChecked = false;
        //username checking
        this.currentUsernameValue = "";
        this.isSearchingUsername = false;
        this.usernameIsAvailable = false;
        this.usernameHasBeenChecked = false;
        this.roleOtherIsShowed = false;
        this.roleGroups = [
            {
                name: "Talent",
                roles: [
                    { name: 'Actor', roleId: 3 },
                    { name: 'Voice Actor', roleId: 4 },
                    { name: 'Model', roleId: 5 },
                    { name: 'Dancer', roleId: 6 }
                ]
            },
            {
                name: "Representation",
                roles: [
                    { name: 'Agent', roleId: 7 }
                ]
            },
            {
                name: "Production",
                roles: [
                    { name: 'Casting Director', roleId: 15 },
                    { name: 'Producer', roleId: 16 },
                    { name: 'Director', roleId: 17 }
                ]
            }
        ];
    }
    // @ViewChild('formDirective') private formDirective: NgForm;
    RegisterComponent.prototype.test = function () {
        debugger;
        var t = this.basicUserInfo;
    };
    RegisterComponent.prototype.validateEmailNotTaken = function (control) {
        if (this.emailHasBeenChecked && !this.emailIsAvailable && (!this.basicUserInfo.get('email').hasError('required') &&
            !this.basicUserInfo.get('email').hasError('email'))) {
            return {
                emailTaken: true
            };
        }
        return null;
    };
    RegisterComponent.prototype.validateRoleOther = function (control) {
        // debugger;
        if (this.basicUserInfo.get('role').value && parseInt(this.basicUserInfo.get('role').value) === 1) {
            if (!this.basicUserInfo.get('roleOther').value ||
                this.basicUserInfo.get('roleOther').value === '') {
                return { required: true };
            }
        }
        return null;
    };
    RegisterComponent.prototype.validateUsernameNotTaken = function (control) {
        if (this.usernameHasBeenChecked && !this.usernameIsAvailable && !this.userLoginInfo.get('username').hasError('required')) {
            return {
                usernameTaken: true
            };
        }
        return null;
    };
    RegisterComponent.prototype.register = function (stepper) {
        if (this.basicUserInfo.invalid) {
            stepper.selectedIndex = 0;
        }
        else {
            if (this.userLoginInfo.valid) {
                this.authenticationService.register(this.userLoginInfo.get('username').value, this.userLoginInfo.get('password').value, this.userLoginInfo.get('passwordCompare').value, this.basicUserInfo.get('email').value, this.basicUserInfo.get('firstNames').value, this.basicUserInfo.get('lastNames').value, parseInt(this.basicUserInfo.get('role').value), this.basicUserInfo.get('roleOther').value)
                    .then(function (r) {
                    stepper.next();
                })
                    .catch(function (error) {
                });
                //.then(stop, stop);
            }
        }
    };
    RegisterComponent.prototype.stepChanged = function (event, stepper) {
        this.basicUserInfo.get('roleOther').clearValidators();
        event.selectedStep.interacted = false;
        event.selectedStep.interacted = false;
        event.selectedStep.hasError = false;
        event.selectedStep.stepControl.markAsPristine();
        event.selectedStep.stepControl.markAsUntouched();
        //this.basicUserInfo.get('roleOther').setErrors(null);
        //this.basicUserInfo.get('roleOther').markAsUntouched();
        // this.basicUserInfo.get('roleOther').markAsPristine();
        //stepper.selected.interacted = false;
    };
    RegisterComponent.prototype.ngOnInit = function () {
        var _this = this;
        //basic user info
        this.basicUserInfo = this._formBuilder.group({
            email: ['fdgld@kjtrtr.com', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email, this.validateEmailNotTaken.bind(this)]],
            firstNames: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            lastNames: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            role: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
            roleOther: [''],
        }, { updateOn: 'blur' });
        //userlogin info
        this.userLoginInfo = this._formBuilder.group({
            username: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, this.validateUsernameNotTaken.bind(this)]],
            //password: ['', [Validators.required, RxwebValidators.password({ validation: { maxLength: 10, minLength: 5, digit: true, specialCharacter: true } })]],
            password: ['Brazil99?', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, , _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)]],
            passwordCompare: ['Brazil99?', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _rxweb_reactive_form_validators__WEBPACK_IMPORTED_MODULE_4__["RxwebValidators"].compare({ fieldName: 'password' })]]
        }, { updateOn: 'blur' });
        this.basicUserInfo.get('role').valueChanges.subscribe(function (value) {
            debugger;
            //this.basicUserInfo.get('roleOther').validator('required')
            _this.basicUserInfo.get('roleOther').clearValidators();
            _this.basicUserInfo.get('roleOther').reset();
            _this.basicUserInfo.get('roleOther').setErrors(null);
            _this.basicUserInfo.get('roleOther').markAsUntouched();
            _this.basicUserInfo.get('roleOther').markAsPristine();
            if (value && parseInt(value) === 1) {
                _this.basicUserInfo.get('roleOther').setValidators([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]);
                _this.roleOtherIsShowed = true;
            }
            else {
                _this.roleOtherIsShowed = false;
            }
            _this.basicUserInfo.get('roleOther').reset();
            _this.basicUserInfo.get('roleOther').setErrors(null);
            _this.basicUserInfo.get('roleOther').markAsUntouched();
            _this.basicUserInfo.get('roleOther').markAsPristine();
        });
        //email availability check
        this.basicUserInfo.get('email').valueChanges.subscribe(function (value) {
            //make sure the value really changed as we force the value change call at the end of this method
            if (_this.currentEmailValue !== _this.basicUserInfo.get('email').value) {
                if (!_this.basicUserInfo.get('email').hasError('required') &&
                    !_this.basicUserInfo.get('email').hasError('email')) {
                    _this.isSearchingEmail = true;
                    _this.authenticationService.checkEmailAvailability(_this.basicUserInfo.get('email').value)
                        .then(function (response) {
                        _this.currentEmailValue = _this.basicUserInfo.get('email').value;
                        _this.isSearchingEmail = false;
                        _this.emailHasBeenChecked = true;
                        _this.emailIsAvailable = response;
                        _this.basicUserInfo.get('email').updateValueAndValidity();
                        if (response === false) {
                        }
                        else {
                        }
                    })
                        .catch(function (error) {
                        _this.isSearchingEmail = false;
                    });
                }
            }
        });
        //username availability check
        this.userLoginInfo.get('username').valueChanges.subscribe(function (value) {
            //make sure the value really changed as we force the value change call at the end of this method
            if (_this.currentUsernameValue !== _this.userLoginInfo.get('username').value) {
                if (!_this.userLoginInfo.get('username').hasError('required')) {
                    _this.isSearchingUsername = true;
                    _this.authenticationService.checkUsernameAvailability(_this.userLoginInfo.get('username').value)
                        .then(function (response) {
                        _this.currentUsernameValue = _this.userLoginInfo.get('username').value;
                        _this.isSearchingUsername = false;
                        _this.usernameHasBeenChecked = true;
                        _this.usernameIsAvailable = response;
                        _this.userLoginInfo.get('username').updateValueAndValidity();
                        if (response === false) {
                        }
                        else {
                        }
                    })
                        .catch(function (error) {
                        _this.isSearchingUsername = false;
                    });
                }
            }
        });
    };
    RegisterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/feature-modules/users/components/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.scss */ "./src/app/feature-modules/users/components/register/register.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/users/users.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/feature-modules/users/users.module.ts ***!
  \*******************************************************/
/*! exports provided: UsersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersModule", function() { return UsersModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _users_states__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./users.states */ "./src/app/feature-modules/users/users.states.ts");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/feature-modules/users/components/register/register.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/feature-modules/users/components/login/login.component.ts");










var UsersModule = /** @class */ (function () {
    function UsersModule() {
    }
    UsersModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _uirouter_angular__WEBPACK_IMPORTED_MODULE_5__["UIRouterModule"].forChild({ states: _users_states__WEBPACK_IMPORTED_MODULE_6__["USERS_STATES"] }),
                _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _global_global_module__WEBPACK_IMPORTED_MODULE_7__["GlobalModule"]
            ],
            declarations: [
                _components_register_register_component__WEBPACK_IMPORTED_MODULE_8__["RegisterComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_9__["LoginComponent"]
            ],
            providers: [],
        })
    ], UsersModule);
    return UsersModule;
}());



/***/ }),

/***/ "./src/app/feature-modules/users/users.states.ts":
/*!*******************************************************!*\
  !*** ./src/app/feature-modules/users/users.states.ts ***!
  \*******************************************************/
/*! exports provided: loginState, registerState, returnTo, USERS_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginState", function() { return loginState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registerState", function() { return registerState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "returnTo", function() { return returnTo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USERS_STATES", function() { return USERS_STATES; });
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/feature-modules/users/components/login/login.component.ts");
/* harmony import */ var _components_register_register_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/register/register.component */ "./src/app/feature-modules/users/components/register/register.component.ts");



/**
 * This is the login state.  It is activated when the user navigates to /login, or if a unauthenticated
 * user attempts to access a protected state (or substate) which requires authentication. (see routerhooks/requiresAuth.js)
 *
 * It shows a fake login dialog and prompts the user to authenticate.  Once the user authenticates, it then
 * reactivates the state that the user originally came from.
 */
var loginState = {
    name: 'login',
    url: '/login',
    parent: "mainScrollContainer",
    views: {
        "main@mainScrollContainer": { component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_1__["LoginComponent"] }
    },
    resolve: [
        { token: 'returnTo', deps: [_uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: returnTo },
    ]
};
var registerState = {
    name: 'register',
    url: '/register',
    parent: "mainScrollContainer",
    views: {
        "main@mainScrollContainer": { component: _components_register_register_component__WEBPACK_IMPORTED_MODULE_2__["RegisterComponent"] }
    },
    resolve: [
        { token: 'returnTo', deps: [_uirouter_angular__WEBPACK_IMPORTED_MODULE_0__["Transition"]], resolveFn: returnTo },
    ]
};
/**
 * A resolve function for 'login' state which figures out what state to return to, after a successful login.
 *
 * If the user was initially redirected to login state (due to the requiresAuth redirect), then return the toState/params
 * they were redirected from.  Otherwise, if they transitioned directly, return the fromState/params.  Otherwise
 * return the main "home" state.
 */
function returnTo($transition$) {
    if ($transition$.redirectedFrom() != null) {
        // The user was redirected to the login state (e.g., via the requiresAuth hook when trying to activate contacts)
        // Return to the original attempted target state (e.g., contacts)
        return $transition$.redirectedFrom().targetState();
    }
    var $state = $transition$.router.stateService;
    // The user was not redirected to the login state; they directly activated the login state somehow.
    // Return them to the state they came from.
    if ($transition$.from().name !== '') {
        return $state.target($transition$.from(), $transition$.params('from'));
    }
    // If the fromState's name is empty, then this was the initial transition. Just return them to the home state
    return $state.target('home');
}
var USERS_STATES = [
    registerState,
    loginState
];


/***/ })

}]);
//# sourceMappingURL=feature-modules-users-users-module.js.map