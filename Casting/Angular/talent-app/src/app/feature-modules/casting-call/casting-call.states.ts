﻿import { Transition } from "@uirouter/angular";
import { StateTransitionHandlerService } from '../../services/state-transition-handler.service';
import { ProductionService } from '../../services/production.service';
import { StateDependencyResolverService } from '../../services/state-dependency-resolver.service';
import { Production, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../models/production.models';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../models/metadata';
import { CastingCall, NewCastingCall, NewCastingCallRole, UpdateCastingCall, UpdateCastingCallRole } from '../../models/casting-call.models';

import { NewCastingCallComponent } from './components/new-casting-call/new-casting-call.component';



export function getMetadataFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<MetadataModel> {
  let metaDate: MetadataModel;
  let promise: Promise<MetadataModel>;

  return stateTransitionService.getMetadataFromServer(transition);

}

export function getEmptyCastingCall() {
  let prod: CastingCall;
  return prod;
}
export function getCastingCallFromTransactionOrServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<CastingCall> {
  let castingCallId: number;
  let castingCall: CastingCall;
  let promise: Promise<CastingCall>;

  return stateTransitionService.getCastingCallFromTransactionOrServer(transition);
}

export function getCastingCallsFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<CastingCall[]> {
  let castingCalls: CastingCall[];
  let promise: Promise<CastingCall>;
  return stateTransitionService.getCastingCallsFromServer(transition);

}
export function getProductionCastingCallsFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<CastingCall[]> {
  let castingCalls: CastingCall[];
  let productionId: number;

  return stateTransitionService.getProductionCastingCallsFromServer(transition);

}
export function getEmptyProduction() {
  let prod: Production;
  return prod;
}
export function getProductionFromTransactionOrServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<Production> {
  let productionId: number;
  let production: Production;
  let promise: Promise<Production>;

  return stateTransitionService.getProductionFromTransactionOrServer(transition);

}
export function getProductionsFromServer(stateTransitionService: StateTransitionHandlerService, transition: Transition): Promise<Production[]> {
  let productions: Production[];
  let promise: Promise<Production>;

  return stateTransitionService.getProductionsFromServer(transition);

}

/*
export const castingCallListState = {
  name: 'castingCalls',
  url: '/casting-calls',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: CastingCallsComponent }
  },
  resolve: [
    {
      token: 'castingCalls', deps: [StateTransitionHandlerService, Transition], resolveFn: getCastingCallsFromServer
    },
  ]


};
export const productionCastingCallListState = {
  name: 'productioncastingCalls',
  url: '/productions/:productionId/casting-calls',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: CastingCallsComponent }
  },
  params: {
    production: null
  },
  resolve: [
    {
      token: 'castingCalls', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionCastingCallsFromServer
    },
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
    },

  ]


};
export const castingCallState = {
  name: 'castingCall',
  url: '/productions/:productionId/casting-calls/:castingCallId',
  parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: CastingCallComponent }
  },
  params: {
    castingCall: null
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'castingCall', deps: [StateTransitionHandlerService, Transition], resolveFn: getCastingCallFromTransactionOrServer
    },
  ]
};
*/

export const newcastingCallState = {
  name: 'newcastingCall',
  url: '/productions/:productionId/casting-calls/new',
    parent: "mainPulloutNonScrollContainer",
  views: {
    "main@mainPulloutNonScrollContainer": { component: NewCastingCallComponent },
    // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
    //production: null,
    castingCall: null,
    popRole: false,
    isDraft: false,
    isEdit: false
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
    },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ]
};

export const draftcastingCallState = {
  name: 'draftcastingCall',
  url: '/casting-calls/draft/:draftId',
  parent: "mainPulloutNonScrollContainer",
  views: {
    "main@mainPulloutNonScrollContainer": { component: NewCastingCallComponent },
    // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
    castingCall: null,
    popRole: false,
    isDraft: true,
    isEdit: false
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'castingCall', deps: [StateTransitionHandlerService, Transition], resolveFn: getCastingCallFromTransactionOrServer
    },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ],
};


export const editcastingCallState = {
  name: 'editcastingCall',
  url: '/productions/:productionId/casting-calls/:castingCallId/edit',
  parent: "mainPulloutNonScrollContainer",
  views: {
    "main@mainPulloutNonScrollContainer": { component: NewCastingCallComponent },
    // "side@pullOutSideMainContainer": { component: SideQuickLinksComponent }
  },
  params: {
    castingCall: null,
    popRole: false,
    isDraft: false,
    isEdit: true
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'castingCall', deps: [StateTransitionHandlerService, Transition], resolveFn: getCastingCallFromTransactionOrServer
    },
    {
      token: 'metadata', deps: [StateTransitionHandlerService, Transition], resolveFn: getMetadataFromServer
    },
  ],
};

export const CAMPAIGN_STATES = [
 // productionCastingCallListState,
 // castingCallListState,
 // castingCallState,
  newcastingCallState,
  draftcastingCallState,
  editcastingCallState,
  //newcastingCallContainerState,
];

