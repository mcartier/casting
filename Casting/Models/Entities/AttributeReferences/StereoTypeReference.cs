﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Attributes;
using Casting.Model.Enums;

namespace Casting.Model.Entities.AttributeReferences
{
    public abstract class StereoTypeReference : Logger
    {
        public int Id { get; set; }
       
        public int StereoTypeId { get; set; }
        public StereoType StereoType { get; set; }

        public AttributeReferenceTypeEnum AttributeReferenceTypeEnum { get; set; }

        public int OrderIndex { get; set; }
    }
}