import { TestBed } from '@angular/core/testing';

import { VideoBlobService } from './video-blob.service';

describe('VideoBlobService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideoBlobService = TestBed.get(VideoBlobService);
    expect(service).toBeTruthy();
  });
});
