﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;
using Casting.Model.Entities.Metadata;

namespace Casting.DAL.Configurations
{
    class ProductionTypeConfiguration : EntityTypeConfiguration<ProductionType>
    {
        public ProductionTypeConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(c => c.ProductionTypeCategory)
                .WithMany(t => t.ProductionTypes);
          
        }
    }

    class ProductionTypeCategoryConfiguration : EntityTypeConfiguration<ProductionTypeCategory>
    {
        public ProductionTypeCategoryConfiguration()
        {
            HasKey(x => x.Id);
            HasMany(p => p.ProductionTypes)
                .WithRequired(c => c.ProductionTypeCategory);
        }
    }

  
}
;