﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.DAL.Repositories.Interfaces;

using Casting.Model.Entities.Attributes;

namespace Casting.DAL.Repositories
{
   
   public class MetadataAsyncRepository : AsyncRepository, IMetadataAsyncRepository
   {
        ICastingContext _castingContext;
        public MetadataAsyncRepository(ICastingContext castingContext) : base(castingContext)
        {
            _castingContext = castingContext;
        }

        #region Unions
        public async Task<Union> GetUnionByIdAsync(int id)
        {
            var unions = GetUnionsQueryable();

            return await unions.FirstOrDefaultAsync(m=>m.Id == id);
        }

        public async Task<IEnumerable<Union>> GetUnionsAsync()
        {
            var unions = GetUnionsQueryable().OrderBy(n=>n.Name);

            return await unions.ToListAsync();
        }


        public async Task<int> AddNewUnionAsync(Union union)
        {
            _castingContext.Unions.Add(union);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteUnionAsync(Union union)
        {

            _castingContext.Unions.Remove(union);
            return await _castingContext.SaveChangesAsync();

          
        }

        public async Task<int> UpdateUnionAsync(Union union)
        {
            SetEntityState(union, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }

       

        #region PrivateMethods

        private IQueryable<Union> GetUnionsQueryable()
        {
            IQueryable<Union> unions = from union in _castingContext.Unions select union;
            
            return unions;
        }

        #endregion
        #endregion

        #region EthnicStereoTypes
        public async Task<EthnicStereoType> GetEthnicStereoTypeByIdAsync(int id)
        {
            var ethnicStereoTypes = GetEthnicStereoTypesQueryable();

            return await ethnicStereoTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<EthnicStereoType>> GetEthnicStereoTypesAsync()
        {
            var ethnicStereoTypes = GetEthnicStereoTypesQueryable().OrderBy(n=>n.Name);

            return await ethnicStereoTypes.ToListAsync();
        }


        public async Task<int> AddNewEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType)
        {
            _castingContext.EthnicStereoTypes.Add(ethnicStereoType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType)
        {

            _castingContext.EthnicStereoTypes.Remove(ethnicStereoType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateEthnicStereoTypeAsync(EthnicStereoType ethnicStereoType)
        {
            SetEntityState(ethnicStereoType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<EthnicStereoType> GetEthnicStereoTypesQueryable()
        {
            IQueryable<EthnicStereoType> ethnicStereoTypes = from ethnicStereoType in _castingContext.EthnicStereoTypes select ethnicStereoType;

            return ethnicStereoTypes;
        }

        #endregion
        #endregion

        #region Ethnicities
        public async Task<Ethnicity> GetEthnicityByIdAsync(int id)
        {
            var ethnicities = GetEthnicitiesQueryable();

            return await ethnicities.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Ethnicity>> GetEthnicitiesAsync()
        {
            var ethnicities = GetEthnicitiesQueryable().OrderBy(n=>n.Name);

            return await ethnicities.ToListAsync();
        }


        public async Task<int> AddNewEthnicityAsync(Ethnicity ethnicity)
        {
            _castingContext.Ethnicities.Add(ethnicity);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteEthnicityAsync(Ethnicity ethnicity)
        {

            _castingContext.Ethnicities.Remove(ethnicity);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateEthnicityAsync(Ethnicity ethnicity)
        {
            SetEntityState(ethnicity, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<Ethnicity> GetEthnicitiesQueryable()
        {
            IQueryable<Ethnicity> ethnicities = from ethnicity in _castingContext.Ethnicities select ethnicity;

            return ethnicities;
        }

        #endregion
        #endregion

        #region StereoTypes
        public async Task<StereoType> GetStereoTypeByIdAsync(int id)
        {
            var stereoTypes = GetStereoTypesQueryable();

            return await stereoTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<StereoType>> GetStereoTypesAsync()
        {
            var stereoTypes = GetStereoTypesQueryable().OrderBy(n=>n.Name);

            return await stereoTypes.ToListAsync();
        }


        public async Task<int> AddNewStereoTypeAsync(StereoType stereoType)
        {
            _castingContext.StereoTypes.Add(stereoType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteStereoTypeAsync(StereoType stereoType)
        {

            _castingContext.StereoTypes.Remove(stereoType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateStereoTypeAsync(StereoType stereoType)
        {
            SetEntityState(stereoType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<StereoType> GetStereoTypesQueryable()
        {
            IQueryable<StereoType> stereoTypes = from stereoType in _castingContext.StereoTypes select stereoType;

            return stereoTypes;
        }

        #endregion
        #endregion

        #region Accents
        public async Task<Accent> GetAccentByIdAsync(int id)
        {
            var accents = GetAccentsQueryable();

            return await accents.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Accent>> GetAccentsAsync()
        {
            var accents = GetAccentsQueryable().OrderBy(n=>n.Name);

            return await accents.ToListAsync();
        }


        public async Task<int> AddNewAccentAsync(Accent accent)
        {
            _castingContext.Accents.Add(accent);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteAccentAsync(Accent accent)
        {

            _castingContext.Accents.Remove(accent);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateAccentAsync(Accent accent)
        {
            SetEntityState(accent, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<Accent> GetAccentsQueryable()
        {
            IQueryable<Accent> accents = from accent in _castingContext.Accents select accent;

            return accents;
        }

        #endregion
        #endregion

        #region Languages
        public async Task<Language> GetLanguageByIdAsync(int id)
        {
            var languages = GetLanguagesQueryable();

            return await languages.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Language>> GetLanguagesAsync()
        {
            var languages = GetLanguagesQueryable().OrderBy(n=>n.Name);

            return await languages.ToListAsync();
        }


        public async Task<int> AddNewLanguageAsync(Language language)
        {
            _castingContext.Languages.Add(language);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteLanguageAsync(Language language)
        {

            _castingContext.Languages.Remove(language);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateLanguageAsync(Language language)
        {
            SetEntityState(language, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<Language> GetLanguagesQueryable()
        {
            IQueryable<Language> languages = from language in _castingContext.Languages select language;

            return languages;
        }

        #endregion
        #endregion

        #region HairColors
        public async Task<HairColor> GetHairColorByIdAsync(int id)
        {
            var hairColors = GetHairColorsQueryable();

            return await hairColors.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<HairColor>> GetHairColorsAsync()
        {
            var hairColors = GetHairColorsQueryable().OrderBy(n=>n.Name);

            return await hairColors.ToListAsync();
        }


        public async Task<int> AddNewHairColorAsync(HairColor hairColor)
        {
            _castingContext.HairColors.Add(hairColor);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteHairColorAsync(HairColor hairColor)
        {

            _castingContext.HairColors.Remove(hairColor);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateHairColorAsync(HairColor hairColor)
        {
            SetEntityState(hairColor, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<HairColor> GetHairColorsQueryable()
        {
            IQueryable<HairColor> hairColors = from hairColor in _castingContext.HairColors select hairColor;

            return hairColors;
        }

        #endregion
        #endregion

        #region EyeColors
        public async Task<EyeColor> GetEyeColorByIdAsync(int id)
        {
            var eyeColors = GetEyeColorsQueryable();

            return await eyeColors.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<EyeColor>> GetEyeColorsAsync()
        {
            var eyeColors = GetEyeColorsQueryable().OrderBy(n=>n.Name);

            return await eyeColors.ToListAsync();
        }


        public async Task<int> AddNewEyeColorAsync(EyeColor eyeColor)
        {
            _castingContext.EyeColors.Add(eyeColor);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteEyeColorAsync(EyeColor eyeColor)
        {

            _castingContext.EyeColors.Remove(eyeColor);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateEyeColorAsync(EyeColor eyeColor)
        {
            SetEntityState(eyeColor, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<EyeColor> GetEyeColorsQueryable()
        {
            IQueryable<EyeColor> eyeColors = from eyeColor in _castingContext.EyeColors select eyeColor;

            return eyeColors;
        }

        #endregion
        #endregion

        #region BodyTypes
        public async Task<BodyType> GetBodyTypeByIdAsync(int id)
        {
            var bodyTypes = GetBodyTypesQueryable();

            return await bodyTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<BodyType>> GetBodyTypesAsync()
        {
            var bodyTypes = GetBodyTypesQueryable().OrderBy(n=>n.Name);

            return await bodyTypes.ToListAsync();
        }


        public async Task<int> AddNewBodyTypeAsync(BodyType bodyType)
        {
            _castingContext.BodyTypes.Add(bodyType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteBodyTypeAsync(BodyType bodyType)
        {

            _castingContext.BodyTypes.Remove(bodyType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateBodyTypeAsync(BodyType bodyType)
        {
            SetEntityState(bodyType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<BodyType> GetBodyTypesQueryable()
        {
            IQueryable<BodyType> bodyTypes = from bodyType in _castingContext.BodyTypes select bodyType;

            return bodyTypes;
        }

        #endregion
        #endregion

        
        #region Skills
        public async Task<Skill> GetSkillByIdAsync(int id,bool includeCategory)
        {
            var skills = GetSkillsQueryable(includeCategory);

            return await skills.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Skill>> GetSkillsAsync(bool includeCategory)
        {
            var skills = GetSkillsQueryable(includeCategory).OrderBy(n=>n.Name);

            return await skills.ToListAsync();
        }


        public async Task<int> AddNewSkillAsync(Skill skill)
        {
            _castingContext.Skills.Add(skill);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteSkillAsync(Skill skill)
        {

            _castingContext.Skills.Remove(skill);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateSkillAsync(Skill skill)
        {
            SetEntityState(skill, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<Skill> GetSkillsQueryable(bool includeCategory)
        {
            IQueryable<Skill> skills = from skill in _castingContext.Skills select skill;
            if (includeCategory)
            {
                skills = skills.Include(c => c.SkillCategory);
            }
            return skills;
        }

        #endregion
        #endregion

        #region SkillCategories
        public async Task<SkillCategory> GetSkillCategoryByIdAsync(int id,bool includeSkills)
        {
            var skillCategories = GetSkillCategoriesQueryable(includeSkills);

            return await skillCategories.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<SkillCategory>> GetSkillCategoriesAsync(bool includeSkills)
        {
            var skillCategories = GetSkillCategoriesQueryable(includeSkills).OrderBy(n=>n.Name);

            return await skillCategories.ToListAsync();
        }


        public async Task<int> AddNewSkillCategoryAsync(SkillCategory skillCategory)
        {
            _castingContext.SkillCategories.Add(skillCategory);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteSkillCategoryAsync(SkillCategory skillCategory)
        {

            _castingContext.SkillCategories.Remove(skillCategory);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateSkillCategoryAsync(SkillCategory skillCategory)
        {
            SetEntityState(skillCategory, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<SkillCategory> GetSkillCategoriesQueryable(bool includeSkills)
        {
            IQueryable<SkillCategory> skillCategories = from skillCategory in _castingContext.SkillCategories select skillCategory;
            if (includeSkills)
            {
                skillCategories = skillCategories.Include(c => c.Skills);
            }
            return skillCategories;
        }

        #endregion
        #endregion


        #region RoleTypes
        public async Task<RoleType> GetRoleTypeByIdAsync(int id)
        {
            var roleTypes = GetRoleTypesQueryable();

            return await roleTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<RoleType>> GetRoleTypesAsync()
        {
            var roleTypes = GetRoleTypesQueryable().OrderBy(n=>n.Name);

            return await roleTypes.ToListAsync();
        }


        public async Task<int> AddNewRoleTypeAsync(RoleType roleType)
        {
            _castingContext.RoleTypes.Add(roleType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteRoleTypeAsync(RoleType roleType)
        {

            _castingContext.RoleTypes.Remove(roleType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateRoleTypeAsync(RoleType roleType)
        {
            SetEntityState(roleType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<RoleType> GetRoleTypesQueryable()
        {
            IQueryable<RoleType> roleTypes = from roleType in _castingContext.RoleTypes select roleType;

            return roleTypes;
        }

        #endregion
        #endregion

        #region RoleGenderTypes
        public async Task<RoleGenderType> GetRoleGenderTypeByIdAsync(int id)
        {
            var roleGenderTypes = GetRoleGenderTypesQueryable();

            return await roleGenderTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<RoleGenderType>> GetRoleGenderTypesAsync()
        {
            var roleGenderTypes = GetRoleGenderTypesQueryable().OrderBy(n=>n.Name);

            return await roleGenderTypes.ToListAsync();
        }


        public async Task<int> AddNewRoleGenderTypeAsync(RoleGenderType roleGenderType)
        {
            _castingContext.RoleGenderTypes.Add(roleGenderType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteRoleGenderTypeAsync(RoleGenderType roleGenderType)
        {

            _castingContext.RoleGenderTypes.Remove(roleGenderType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateRoleGenderTypeAsync(RoleGenderType roleGenderType)
        {
            SetEntityState(roleGenderType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<RoleGenderType> GetRoleGenderTypesQueryable()
        {
            IQueryable<RoleGenderType> roleGenderTypes = from roleGenderType in _castingContext.RoleGenderTypes select roleGenderType;

            return roleGenderTypes;
        }

        #endregion
        #endregion

        #region CastingCallTypes
        public async Task<CastingCallType> GetCastingCallTypeByIdAsync(int id)
        {
            var castingCallTypes = GetCastingCallTypesQueryable();

            return await castingCallTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<CastingCallType>> GetCastingCallTypesAsync()
        {
            var castingCallTypes = GetCastingCallTypesQueryable().OrderBy(n => n.Name);

            return await castingCallTypes.ToListAsync();
        }
        public async Task<IEnumerable<CastingCallType>> GetCastingCallTypesByCastingCallAsync(int castingCallId)
        {
            var castingCallTypes = GetCastingCallTypesQueryable()
                // .Where(p => p.CastingCalls.Any(i => i.Id == castingCallId))
                .OrderBy(n => n.Name);

            return await castingCallTypes.ToListAsync();
        }


        public async Task<int> AddNewCastingCallTypeAsync(CastingCallType castingCallType)
        {
            _castingContext.CastingCallTypes.Add(castingCallType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteCastingCallTypeAsync(CastingCallType castingCallType)
        {

            _castingContext.CastingCallTypes.Remove(castingCallType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateCastingCallTypeAsync(CastingCallType castingCallType)
        {
            SetEntityState(castingCallType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<CastingCallType> GetCastingCallTypesQueryable()
        {
            IQueryable<CastingCallType> castingCallTypes = from castingCallType in _castingContext.CastingCallTypes select castingCallType;

            return castingCallTypes;
        }


        #endregion
        #endregion

        #region CastingCallViewTypes
        public async Task<CastingCallViewType> GetCastingCallViewTypeByIdAsync(int id)
        {
            var castingCallViewTypes = GetCastingCallViewTypesQueryable();

            return await castingCallViewTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<CastingCallViewType>> GetCastingCallViewTypesAsync()
        {
            var castingCallViewTypes = GetCastingCallViewTypesQueryable().OrderBy(n => n.Name);

            return await castingCallViewTypes.ToListAsync();
        }
        public async Task<IEnumerable<CastingCallViewType>> GetCastingCallViewTypesByCastingCallAsync(int castingCallId)
        {
            var castingCallViewTypes = GetCastingCallViewTypesQueryable()
                // .Where(p => p.CastingCalls.Any(i => i.Id == castingCallId))
                .OrderBy(n => n.Name);

            return await castingCallViewTypes.ToListAsync();
        }


        public async Task<int> AddNewCastingCallViewTypeAsync(CastingCallViewType castingCallViewType)
        {
            _castingContext.CastingCallViewTypes.Add(castingCallViewType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteCastingCallViewTypeAsync(CastingCallViewType castingCallViewType)
        {

            _castingContext.CastingCallViewTypes.Remove(castingCallViewType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateCastingCallViewTypeAsync(CastingCallViewType castingCallViewType)
        {
            SetEntityState(castingCallViewType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<CastingCallViewType> GetCastingCallViewTypesQueryable()
        {
            IQueryable<CastingCallViewType> castingCallViewTypes = from castingCallViewType in _castingContext.CastingCallViewTypes select castingCallViewType;

            return castingCallViewTypes;
        }


        #endregion
        #endregion


        #region CastingCallPrivacies
        public async Task<CastingCallPrivacy> GetCastingCallPrivacyByIdAsync(int id)
        {
            var castingCallPrivacies = GetCastingCallPrivaciesQueryable();

            return await castingCallPrivacies.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<CastingCallPrivacy>> GetCastingCallPrivaciesAsync()
        {
            var castingCallPrivacies = GetCastingCallPrivaciesQueryable().OrderBy(n => n.Name);

            return await castingCallPrivacies.ToListAsync();
        }
        public async Task<IEnumerable<CastingCallPrivacy>> GetCastingCallPrivaciesByCastingCallAsync(int castingCallId)
        {
            var castingCallPrivacies = GetCastingCallPrivaciesQueryable()
                // .Where(p => p.CastingCalls.Any(i => i.Id == castingCallId))
                .OrderBy(n => n.Name);

            return await castingCallPrivacies.ToListAsync();
        }


        public async Task<int> AddNewCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy)
        {
            _castingContext.CastingCallPrivacies.Add(castingCallPrivacy);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy)
        {

            _castingContext.CastingCallPrivacies.Remove(castingCallPrivacy);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateCastingCallPrivacyAsync(CastingCallPrivacy castingCallPrivacy)
        {
            SetEntityState(castingCallPrivacy, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<CastingCallPrivacy> GetCastingCallPrivaciesQueryable()
        {
            IQueryable<CastingCallPrivacy> castingCallPrivacies = from castingCallPrivacy in _castingContext.CastingCallPrivacies select castingCallPrivacy;

            return castingCallPrivacies;
        }


        #endregion
        #endregion



        #region ProductionTypes
        public async Task<ProductionType> GetProductionTypeByIdAsync(int id)
        {
            var productionTypes = GetProductionTypesQueryable();

            return await productionTypes.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<ProductionType>> GetProductionTypesAsync()
        {
            var productionTypes = GetProductionTypesQueryable().OrderBy(n => n.Name);

            return await productionTypes.ToListAsync();
        }
        public async Task<IEnumerable<ProductionType>> GetProductionTypesByProductionTypeCatigoryAsync(int productionTypeCategoryId)
        {
            var productionTypes = GetProductionTypesQueryable()
                .Where(p=>p.ProductionTypeCategoryId == productionTypeCategoryId)
                .OrderBy(n => n.Name);

            return await productionTypes.ToListAsync();
        }
        public async Task<IEnumerable<ProductionType>> GetProductionTypesByProductionAsync(int productionId)
        {
            var productionTypes = GetProductionTypesQueryable()
               // .Where(p => p.Productions.Any(i => i.Id == productionId))
                .OrderBy(n => n.Name);

            return await productionTypes.ToListAsync();
        }


        public async Task<int> AddNewProductionTypeAsync(ProductionType productionType)
        {
            _castingContext.ProductionTypes.Add(productionType);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteProductionTypeAsync(ProductionType productionType)
        {

            _castingContext.ProductionTypes.Remove(productionType);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateProductionTypeAsync(ProductionType productionType)
        {
            SetEntityState(productionType, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<ProductionType> GetProductionTypesQueryable()
        {
            IQueryable<ProductionType> productionTypes = from productionType in _castingContext.ProductionTypes select productionType;

            return productionTypes;
        }
        

        #endregion
        #endregion

        #region ProductionTypeCategories
        public async Task<ProductionTypeCategory> GetProductionTypeCategoryByIdAsync(int id)
        {
            var productionTypeCategories = GetProductionTypeCategoriesQueryable(false);

            return await productionTypeCategories.FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<ProductionTypeCategory>> GetProductionTypeCategoriesAsync()
        {
            var productionTypeCategories = GetProductionTypeCategoriesQueryable(false).OrderBy(n => n.Name);

            return await productionTypeCategories.ToListAsync();
        }
        public async Task<IEnumerable<ProductionTypeCategory>> GetCompleteProductionTypeCategoriesAsync()
        {
            var productionTypeCategories = GetProductionTypeCategoriesQueryable(true).OrderBy(n => n.Name);

            return await productionTypeCategories.ToListAsync();
        }


        public async Task<int> AddNewProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory)
        {
            _castingContext.ProductionTypeCategories.Add(productionTypeCategory);
            return await _castingContext.SaveChangesAsync();

        }
        public async Task<int> DeleteProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory)
        {

            _castingContext.ProductionTypeCategories.Remove(productionTypeCategory);
            return await _castingContext.SaveChangesAsync();


        }

        public async Task<int> UpdateProductionTypeCategoryAsync(ProductionTypeCategory productionTypeCategory)
        {
            SetEntityState(productionTypeCategory, EntityState.Modified);
            return await _castingContext.SaveChangesAsync();
        }



        #region PrivateMethods

        private IQueryable<ProductionTypeCategory> GetProductionTypeCategoriesQueryable(bool withProductionTypes)
        {
            IQueryable<ProductionTypeCategory> productionTypeCategories = from productionTypeCategory in _castingContext.ProductionTypeCategories select productionTypeCategory;
            if (withProductionTypes)
            {
                productionTypeCategories = productionTypeCategories.Include(p => p.ProductionTypes);
            }
            return productionTypeCategories;
        }

        #endregion
        #endregion


    }
}
