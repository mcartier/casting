﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Profiles.References
{
    public class TalentProfileReference:Logger
    {
        [ForeignKey("Talent")]
        public int TalentId { get; set; }
        public ProfileTypeEnum ProfileType { get; set; }
        public TalentTypeEnum TalentType { get; set; }
        private Talent _talent;
        public virtual Talent Talent
        {
            get { return _talent; }
            set
            {
                _talent = value;
                if (_talent != null)
                {
                    this.TalentType = _talent.TalentType;
                  //  _talent.ProfileType = this.ProfileType;
                }

            }
        }
        [ForeignKey("Profile")]
        public int? ProfileId { get; set; }
        private Profile _profile;
        public virtual Profile Profile
        {
            get { return _profile; }
            set
            {
                _profile = value;
                if (_profile != null)
                {
                //    _profile.TalentType = this.TalentType;
                   // this.ProfileType = _profile.ProfileType;
                }

            }
        }

    }
}
