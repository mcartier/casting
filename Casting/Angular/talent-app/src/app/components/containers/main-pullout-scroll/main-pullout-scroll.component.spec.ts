import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPulloutScrollComponent } from './main-pullout-scroll.component';

describe('MainPulloutScrollComponent', () => {
  let component: MainPulloutScrollComponent;
  let fixture: ComponentFixture<MainPulloutScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainPulloutScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPulloutScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
