﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Campaigns;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions.ShoutDetails
{
   public class CampaignRoleShoutDetailReference : Logger
    {
        public CampaignRoleShoutDetailReference()
        {
          

        }
        public int Id { get; set; }
      
        public int ShoutDetailId { get; set; }
        public ShoutDetail ShoutDetail { get; set; }

        public int CampaignRoleId { get; set; }
        public CampaignRole CampaignRole { get; set; }


    }
}
