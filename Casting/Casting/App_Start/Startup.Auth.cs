﻿using System;
using System.Collections.Generic;
using System.Linq;
using AspNetWebApi.JWT;
using AspNetWebApi.JWT.Options;
using AspNetWebApi.JWT.Providers;
using Casting.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using Casting.Web.Providers;
using Casting.Web.Models;
using StructureMap;

namespace Casting.Web
{
    public partial class Startup
    {
        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
           // var castingContext = new Container().GetInstance<ICastingContext>();

            // Configure the db context and user manager to use a single instance per request
            //app.CreatePerOwinContext<ICastingContext>(castingContext.);
           // app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            var easyJwtAuthorizationServerOptions = new JwtAuthorizationServerOptions();
            easyJwtAuthorizationServerOptions.AllowInsecureHttp = true;
            easyJwtAuthorizationServerOptions.TokenEndpointPath = "/oauth2/token";
            easyJwtAuthorizationServerOptions.AccessTokenExpireTimeSpan = TimeSpan.FromDays(7);


            app.UseJwtAuthorizationServer(easyJwtAuthorizationServerOptions, new JWTOAuthProvider());
            app.UseJwtAuthentication();

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            //app.UseGoogleAuthentication(new GoogleOAuth2AuthenticationOptions()
            //{
            //    ClientId = "",
            //    ClientSecret = ""
            //});
        }
    }
}
