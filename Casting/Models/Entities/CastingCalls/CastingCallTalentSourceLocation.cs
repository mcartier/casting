﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.CastingCalls
{
    public class CastingCallTalentSourceLocation:Logger
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CastingCallTalentSourceLocationTypeEnum SourceLocationType { get; set; }


      

    }
}
