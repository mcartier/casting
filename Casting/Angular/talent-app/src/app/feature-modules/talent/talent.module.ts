import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule } from '@angular/forms';
import { TALENTS_STATES } from './talent.states';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { UIRouterModule } from "@uirouter/angular";
import { Transition } from "@uirouter/angular";
import { UIRouter } from '@uirouter/angular';
import { Visualizer } from '@uirouter/visualizer';
import { APP_INITIALIZER } from '@angular/core';
import { GlobalModule } from '../../global/global.module';
import { NewTalentComponent } from './components/new-talent/new-talent.component';
import { EditProfileComponent } from './components/edit-profile/edit-profile.component';


@NgModule({
  imports: [
    CommonModule,
    UIRouterModule.forChild({ states: TALENTS_STATES }),
    HttpClientModule,
      ReactiveFormsModule,
    GlobalModule
  ],
  declarations: [
    
     
  NewTalentComponent,
    
  EditProfileComponent],
  providers: [
  
  ],
})
export class TalentModule {
}
