(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["feature-modules-casting-call-casting-call-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.html":
/*!************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.html ***!
  \************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"p-grid\">\r\n    <div class=\"p-col-9\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n            <form [formGroup]=\"basicCastingCallInfo\">\r\n\r\n                <p-card class=\"p-col\" header=\"Create a new castingCall\">\r\n                    <p-header>\r\n                        <div #basiccard></div>\r\n                    </p-header>\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Name of CastingCall</label>\r\n                                <input class=\"p-col\" pInput placeholder=\"{{namePlaceHolder}}\" #name formControlName=\"name\" required>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"CastingCall Name is <strong>required</strong>\" *ngIf=\"basicCastingCallInfo.controls['name'].hasError('required') && basicCastingCallInfo.controls['name'].dirty\"></p-message>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">CastingCall Type</label>\r\n                                <p-dropdown class=\"p-col\" [options]=\"metadata.castingCallTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" #castingCallTypeId placeholder=\"Select a Type of CastingCall\" formControlName=\"castingCallTypeId\" required>\r\n                                </p-dropdown>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"CastingCall Type is <strong>required</strong>\" *ngIf=\"basicCastingCallInfo.controls['castingCallTypeId'].hasError('required') && basicCastingCallInfo.controls['castingCallTypeId'].dirty\">\r\n\r\n                                </p-message>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">Select a Type of CastingCall</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Publish this castingCall?</label>\r\n                                <p-radioButton #castingCallViewType class=\"p-col\" name=\"castingCallViewTypes\" value=\"7\" label=\"Make it public and visible to anyone\" formControlName=\"castingCallViewType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"castingCallViewTypes\" value=\"3\" label=\"Make it public and visible only to members\" formControlName=\"castingCallViewType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"castingCallViewTypes\" value=\"1\" label=\"Make it private and visible to only select members\" formControlName=\"castingCallViewType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"castingCallViewTypes\" value=\"15\" label=\"By invitation only\" formControlName=\"castingCallViewType\"></p-radioButton>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"CastingCall Type is <strong>required</strong>\" *ngIf=\"basicCastingCallInfo.controls['castingCallViewType'].hasError('required') && basicCastingCallInfo.controls['castingCallViewType'].dirty\">\r\n                                </p-message>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">When should this castingCall expire?</label>\r\n                                <p-calendar [inputStyleClass]=\"p-col\" #expires required formControlName=\"expires\" (onSelect)=\"selectExpireDate($event)\" dataType=\"string\" placeholder=\"Chose an expiration date\" dateFormat=\"DD MM d yy\" [minDate]=\"minDate\" [showIcon]=\"true\" [readonlyInput]=\"true\"></p-calendar>\r\n                                <p-message severity=\"error\" class=\"p-col\" text=\"CastingCall Expiration Date is <strong>required</strong>\" *ngIf=\"basicCastingCallInfo.controls['expires'].hasError('required') && basicCastingCallInfo.controls['expires'].dirty\">\r\n\r\n                                </p-message>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">This is the date at which this castingCall will not be publicly visible any more</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Do you have any special submission or audition instructions?</label>\r\n                                <textarea rows=\"3\" class=\"p-col\" pInput placeholder=\"e.g., 'In your cover letter, note your availability. Include a video with your submission. For the auditions, be prepared to sing. For more info about the project, visit www.example.com.'\" formControlName=\"instructions\"></textarea>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">You don't need to include your email address here. Instead, you can manage your email settings in the next section.</div>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n\r\n                </p-card>\r\n\r\n                <p-card header=\"Unions\" class=\"p-col\">\r\n                    <p-header>\r\n                        <div #unionscard></div>\r\n                    </p-header>\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Unions</label>\r\n                                <p-multiSelect class=\"p-col\" formControlName=\"unions\" (onChange)=\"changeUnionSelection($event)\" [options]=\"metadata.unions\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Unions\">\r\n                                </p-multiSelect>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">Select Unions to include in the castingCall</div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </p-card>\r\n\r\n                <p-card header=\"Roles\" class=\"p-col\">\r\n                    <p-header>\r\n                        <div #rolescard></div>\r\n                    </p-header>\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Roles</label>\r\n                                <p-multiSelect class=\"p-col\" formControlName=\"roles\" (onChange)=\"changeRoleSelection($event)\" [options]=\"production.roles\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Roles\">\r\n                                </p-multiSelect>\r\n                                <div class=\"p-col\" *ngIf=\"showHints\">Select Roles to include in the castingCall</div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </p-card>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-justify-end p-align-center\">\r\n                        <button pButton (click)=\"cancelEditCastingCallClick()\">Cancel</button>\r\n                        <button pButton (click)=\"saveCastingCallClick()\">Save</button>\r\n\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n    <div class=\"p-col-3\">\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n\r\n            <button pButton class=\"p-col\" (click)=\"scrollToBasic()\">go Basic</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToRoles()\">go Roles</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToCasting()\">go Casting</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToCastingCall()\">go CastingCall</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToArtistic()\">go Artistic</button>\r\n            <button pButton class=\"p-col\" (click)=\"scrollToOther()\">go Other</button>\r\n            <button pButton class=\"p-col\" (click)=\"saveCastingCallClick()\">Save</button>\r\n            <button pButton class=\"p-col\" (click)=\"deleteCastingCallClick()\">Delete</button>\r\n            <button pButton class=\"p-col\" (click)=\"cancelEditCastingCallClick()\">Cancel</button>\r\n        </div>\r\n\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n");

/***/ }),

/***/ "./src/app/feature-modules/casting-call/casting-call.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/feature-modules/casting-call/casting-call.module.ts ***!
  \*********************************************************************/
/*! exports provided: CastingCallModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallModule", function() { return CastingCallModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../interceptors/errorInterceptor */ "./src/app/interceptors/errorInterceptor.ts");
/* harmony import */ var _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../interceptors/jwtInterceptor */ "./src/app/interceptors/jwtInterceptor.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _casting_call_states__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./casting-call.states */ "./src/app/feature-modules/casting-call/casting-call.states.ts");
/* harmony import */ var _services_casting_call_component_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/casting-call-component.service */ "./src/app/feature-modules/casting-call/services/casting-call-component.service.ts");
/* harmony import */ var _components_new_casting_call_new_casting_call_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/new-casting-call/new-casting-call.component */ "./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.ts");













var CastingCallModule = /** @class */ (function () {
    function CastingCallModule() {
    }
    CastingCallModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_components_new_casting_call_new_casting_call_component__WEBPACK_IMPORTED_MODULE_11__["NewCastingCallComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__["UIRouterModule"].forChild({ states: _casting_call_states__WEBPACK_IMPORTED_MODULE_9__["CAMPAIGN_STATES"] }),
                _global_global_module__WEBPACK_IMPORTED_MODULE_5__["GlobalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            ],
            exports: [
            // DropFileDirective,
            ],
            entryComponents: [],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HTTP_INTERCEPTORS"], useClass: _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_7__["JwtInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HTTP_INTERCEPTORS"], useClass: _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_6__["ErrorInterceptor"], multi: true },
                _services_casting_call_component_service__WEBPACK_IMPORTED_MODULE_10__["CastingCallComponentService"]
            ],
        })
    ], CastingCallModule);
    return CastingCallModule;
}());



/***/ }),

/***/ "./src/app/feature-modules/casting-call/casting-call.states.ts":
/*!*********************************************************************!*\
  !*** ./src/app/feature-modules/casting-call/casting-call.states.ts ***!
  \*********************************************************************/
/*! exports provided: getMetadataFromServer, getEmptyCastingCall, getCastingCallFromTransactionOrServer, getCastingCallsFromServer, getProductionCastingCallsFromServer, getEmptyProduction, getProductionFromTransactionOrServer, getProductionsFromServer, newcastingCallState, draftcastingCallState, editcastingCallState, CAMPAIGN_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMetadataFromServer", function() { return getMetadataFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyCastingCall", function() { return getEmptyCastingCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCastingCallFromTransactionOrServer", function() { return getCastingCallFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCastingCallsFromServer", function() { return getCastingCallsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionCastingCallsFromServer", function() { return getProductionCastingCallsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyProduction", function() { return getEmptyProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionFromTransactionOrServer", function() { return getProductionFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionsFromServer", function() { return getProductionsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newcastingCallState", function() { return newcastingCallState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draftcastingCallState", function() { return draftcastingCallState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editcastingCallState", function() { return editcastingCallState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CAMPAIGN_STATES", function() { return CAMPAIGN_STATES; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/state-transition-handler.service */ "./src/app/services/state-transition-handler.service.ts");
/* harmony import */ var _components_new_casting_call_new_casting_call_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/new-casting-call/new-casting-call.component */ "./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.ts");




function getMetadataFromServer(stateTransitionService, transition) {
    var metaDate;
    var promise;
    return stateTransitionService.getMetadataFromServer(transition);
}
function getEmptyCastingCall() {
    var prod;
    return prod;
}
function getCastingCallFromTransactionOrServer(stateTransitionService, transition) {
    var castingCallId;
    var castingCall;
    var promise;
    return stateTransitionService.getCastingCallFromTransactionOrServer(transition);
}
function getCastingCallsFromServer(stateTransitionService, transition) {
    var castingCalls;
    var promise;
    return stateTransitionService.getCastingCallsFromServer(transition);
}
function getProductionCastingCallsFromServer(stateTransitionService, transition) {
    var castingCalls;
    var productionId;
    return stateTransitionService.getProductionCastingCallsFromServer(transition);
}
function getEmptyProduction() {
    var prod;
    return prod;
}
function getProductionFromTransactionOrServer(stateTransitionService, transition) {
    var productionId;
    var production;
    var promise;
    return stateTransitionService.getProductionFromTransactionOrServer(transition);
}
function getProductionsFromServer(stateTransitionService, transition) {
    var productions;
    var promise;
    return stateTransitionService.getProductionsFromServer(transition);
}
/*
export const castingCallListState = {
  name: 'castingCalls',
  url: '/casting-calls',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: CastingCallsComponent }
  },
  resolve: [
    {
      token: 'castingCalls', deps: [StateTransitionHandlerService, Transition], resolveFn: getCastingCallsFromServer
    },
  ]


};
export const productionCastingCallListState = {
  name: 'productioncastingCalls',
  url: '/productions/:productionId/casting-calls',
  parent: "mainPulloutScrollContainer",
  views: {
    "main@mainPulloutScrollContainer": { component: CastingCallsComponent }
  },
  params: {
    production: null
  },
  resolve: [
    {
      token: 'castingCalls', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionCastingCallsFromServer
    },
    {
      token: 'production', deps: [StateTransitionHandlerService, Transition], resolveFn: getProductionFromTransactionOrServer
    },

  ]


};
export const castingCallState = {
  name: 'castingCall',
  url: '/productions/:productionId/casting-calls/:castingCallId',
  parent: "mainSidePulloutScrollContainer",
  views: {
    "main@mainSidePulloutScrollContainer": { component: CastingCallComponent }
  },
  params: {
    castingCall: null
  },
  resolve: [
    // All the folders are fetched from the Folders service
    {
      token: 'castingCall', deps: [StateTransitionHandlerService, Transition], resolveFn: getCastingCallFromTransactionOrServer
    },
  ]
};
*/
var newcastingCallState = {
    name: 'newcastingCall',
    url: '/productions/:productionId/casting-calls/new',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_casting_call_new_casting_call_component__WEBPACK_IMPORTED_MODULE_3__["NewCastingCallComponent"] },
    },
    params: {
        //production: null,
        castingCall: null,
        popRole: false,
        isDraft: false,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ]
};
var draftcastingCallState = {
    name: 'draftcastingCall',
    url: '/casting-calls/draft/:draftId',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_casting_call_new_casting_call_component__WEBPACK_IMPORTED_MODULE_3__["NewCastingCallComponent"] },
    },
    params: {
        castingCall: null,
        popRole: false,
        isDraft: true,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'castingCall', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getCastingCallFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var editcastingCallState = {
    name: 'editcastingCall',
    url: '/productions/:productionId/casting-calls/:castingCallId/edit',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_casting_call_new_casting_call_component__WEBPACK_IMPORTED_MODULE_3__["NewCastingCallComponent"] },
    },
    params: {
        castingCall: null,
        popRole: false,
        isDraft: false,
        isEdit: true
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'castingCall', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getCastingCallFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_2__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var CAMPAIGN_STATES = [
    // productionCastingCallListState,
    // castingCallListState,
    // castingCallState,
    newcastingCallState,
    draftcastingCallState,
    editcastingCallState,
];


/***/ }),

/***/ "./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.scss":
/*!**********************************************************************************************************!*\
  !*** ./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.scss ***!
  \**********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9jYXN0aW5nLWNhbGwvY29tcG9uZW50cy9uZXctY2FzdGluZy1jYWxsL25ldy1jYXN0aW5nLWNhbGwuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.ts":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.ts ***!
  \********************************************************************************************************/
/*! exports provided: NewCastingCallComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCastingCallComponent", function() { return NewCastingCallComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_casting_call_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/casting-call.models */ "./src/app/models/casting-call.models.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_casting_call_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/casting-call.service */ "./src/app/services/casting-call.service.ts");
/* harmony import */ var _services_casting_call_component_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/casting-call-component.service */ "./src/app/feature-modules/casting-call/services/casting-call-component.service.ts");
/* harmony import */ var _services_casting_call_role_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../services/casting-call-role.service */ "./src/app/services/casting-call-role.service.ts");
/* harmony import */ var _animations_router_animation__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../animations/router.animation */ "./src/app/animations/router.animation.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var primeng_calendar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! primeng/calendar */ "./node_modules/primeng/fesm5/primeng-calendar.js");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/fesm5/primeng-radiobutton.js");








//import { RoleDialogService } from '../../services/role-dialog.service';



//import { NewRoleComponent } from '../new-role/new-role.component'




var NewCastingCallComponent = /** @class */ (function () {
    function NewCastingCallComponent(CastingCallRoleService, _formBuilder, metadataService, castingCallService, stateService, transitionService, transition, castingCallComponentService) {
        this.CastingCallRoleService = CastingCallRoleService;
        this._formBuilder = _formBuilder;
        this.metadataService = metadataService;
        this.castingCallService = castingCallService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.castingCallComponentService = castingCallComponentService;
        this.meta = new _models_metadata__WEBPACK_IMPORTED_MODULE_4__["MetadataModel"]();
        this.compensationType = _models_production_models__WEBPACK_IMPORTED_MODULE_6__["CompensationTypeEnum"].notPaid;
        //private castingCall: CastingCall;
        this.showHints = false;
    }
    NewCastingCallComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deleteRoleSubscription = this.CastingCallRoleService.subscribeToDeleteRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.castingCall.roles.splice(_this.castingCall.roles.indexOf(role), 1);
            }
        });
        /*this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(
                (role: CastingCallRole) => {
                    if (role) {
                        this.castingCall.roles[this.castingCall.roles.indexOf(role)] = role;
                    }
                });*/
        this.deleteCastingCallIntentSubscription = this.castingCallComponentService.subscribeToDeleteCastingCallIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToCastingCallsView();
            }
        });
        this.cancelEditCastingCallIntentSubscription = this.castingCallComponentService.subscribeToCancelEditCastingCallIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToCastingCallsView();
            }
        });
        this.saveCastingCallIntentSubscription = this.castingCallComponentService.subscribeToSaveCastingCallIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.saveCastingCallClick();
            }
        });
        if (this.transition.params().popRole) {
            this.popRole = this.transition.params().popRole;
        }
        if (this.transition.params().isDraft) {
            this.isDraft = this.transition.params().isDraft;
        }
        if (this.transition.params().isEdit) {
            this.isEdit = this.transition.params().isEdit;
        }
        if (this.transition.params().castingCall) {
            this.castingCall = this.transition.params().castingCall;
        }
        if (this.production) {
            this.namePlaceHolder = this.production.name;
        }
        if (this.castingCall) {
            this.basicCastingCallInfo = this._formBuilder.group({
                name: [this.castingCall.name, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                instructions: [this.castingCall.instructions],
                expires: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                castingCallTypeId: [this.castingCall.castingCallType, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                castingCallViewType: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                unions: [this.castingCall.unions.map(function (a) { return a.unionId; })],
                roles: [this.castingCall.roles.map(function (a) { return a.roleId; })]
                // castingCallCompany: [this.castingCall.castingCallCompany],
                // description: [this.castingCall.description, Validators.required],
                // compensationType: [this.castingCall.compensationType],
                //   castingCallUnionStatus: [this.castingCall.castingCallUnionStatus]
            }, { updateOn: 'blur' });
        }
        else {
            this.castingCall = new _models_casting_call_models__WEBPACK_IMPORTED_MODULE_5__["CastingCall"]();
            this.basicCastingCallInfo = this._formBuilder.group({
                name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                instructions: [''],
                expires: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                castingCallTypeId: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                castingCallViewType: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required],
                //castingCallViewType: [0, Validators.required],
                unions: [[]],
                roles: [[]],
            }, { updateOn: 'blur' });
        }
    };
    NewCastingCallComponent.prototype.ngOnDestroy = function () {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();
        }
        if (this.saveCastingCallIntentSubscription) {
            this.saveCastingCallIntentSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    NewCastingCallComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
        //    this.castingCallComponentService.registerRolesScrollElement(this.rolesCard);
        // this.castingCallComponentService.registerBasicScrollElement(this.basicCard);
        if (this.popRole) {
            /* setTimeout(() => {
                 this.newRoleSubscription = this.roleDialogService.createRole(this.castingCall)
                     .subscribe(
                         role => {
                             if (role) {
                                 this.castingCall.roles.push(role);
                             }
                             this.newRoleSubscription.unsubscribe();
                         });
 
 
             })*/
        }
    };
    NewCastingCallComponent.prototype.setDate = function () {
        // Set today date using the patchValue function
        var date = new Date();
        this.basicCastingCallInfo.patchValue({
            expires: {
                date: {
                    year: date.getFullYear(),
                    month: date.getMonth() + 1,
                    day: date.getDate()
                }
            }
        });
    };
    NewCastingCallComponent.prototype.clearDate = function () {
        // Clear the date using the patchValue function
        this.basicCastingCallInfo.patchValue({ myDate: null });
    };
    NewCastingCallComponent.prototype.moveToCastingCallView = function (castingCallId) {
        this.stateService.go('castingCall', { castingCall: this.castingCall, castingCallId: castingCallId });
    };
    NewCastingCallComponent.prototype.moveToCastingCallsView = function () {
        this.stateService.go('castingCalls');
    };
    NewCastingCallComponent.prototype.scrollToRoles = function () {
        //    this.basicCastingCallInfo.reset();
        // this.rolesCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
        //this.castingCallComponentService.scrollToRoles();
    };
    NewCastingCallComponent.prototype.scrollToBasic = function () {
        //   this.basicCastingCallInfo.reset();
        //this.basicCard.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
        //this.castingCallComponentService.scrollToBasic();
    };
    NewCastingCallComponent.prototype.deleteCastingCallClick = function () {
        //   this.basicCastingCallInfo.reset();
        this.castingCallComponentService.tryDeleteCastingCall();
    };
    NewCastingCallComponent.prototype.cancelEditCastingCallClick = function () {
        //   this.basicCastingCallInfo.reset();
        this.moveToCastingCallsView();
    };
    NewCastingCallComponent.prototype.selectExpireDate = function (date) {
        this.basicCastingCallInfo.patchValue({
            expires: date,
        });
    };
    NewCastingCallComponent.prototype.changeUnionSelection = function (event) {
        this.basicCastingCallInfo.patchValue({
            unions: event.value,
        });
    };
    NewCastingCallComponent.prototype.changeRoleSelection = function (event) {
        this.basicCastingCallInfo.patchValue({
            roles: event.value,
        });
    };
    NewCastingCallComponent.prototype.saveCastingCallClick = function () {
        var _this = this;
        if (this.basicCastingCallInfo.valid) {
            //this.role = this.basicRoleInfo.value;
            var submissionObject = Object.assign(this.basicCastingCallInfo.value, {
                productionId: this.production.id
            });
            if (this.castingCall.id && this.castingCall.id !== 0) {
                this.castingCallService.saveCastingCall(this.castingCall.id, this.production.id, submissionObject).subscribe(function (castingCall) {
                    _this.moveToCastingCallView(_this.castingCall.id);
                    //this.castingCall = castingCall;
                }, function (error) {
                    console.log("error " + error);
                });
            }
            else {
                // submissionObject.productionId = this.productionId;
                this.castingCallService.createCastingCall(submissionObject, this.production.id).subscribe(function (castingCall) {
                    _this.castingCall = castingCall;
                    _this.moveToCastingCallView(_this.castingCall.id);
                }, function (error) {
                    console.log("error " + error);
                });
            }
        }
        else {
            if (!this.basicCastingCallInfo.get('name').valid) {
                this.basicCastingCallInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicCastingCallInfo.get('castingCallTypeId').valid) {
                this.basicCastingCallInfo.get('castingCallTypeId').markAsDirty();
                this.castingCallTypeIdField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.castingCallTypeIdField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicCastingCallInfo.get('castingCallViewType').valid) {
                this.basicCastingCallInfo.get('castingCallViewType').markAsDirty();
                this.castingCallViewTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.castingCallViewTypeField.inputViewChild.nativeElement.focus();
            }
            else if (!this.basicCastingCallInfo.get('expires').valid) {
                this.basicCastingCallInfo.get('expires').markAsDirty();
                this.calendarInputField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.calendarInputField.inputfieldViewChild.nativeElement.focus();
            }
        }
    };
    NewCastingCallComponent.prototype.addRoleClick = function () {
        var _this = this;
        // this.basicCastingCallInfo.reset();
        if (this.castingCall.id && this.castingCall.id !== 0) {
            //setTimeout(() => {
            /*this.newRoleSubscription = this.roleDialogService.createRole(this.castingCall)
                .subscribe(
                    role => {
                        if (role) {
                            this.castingCall.roles.push(role);
                        }
                        this.newRoleSubscription.unsubscribe();
                    });*/
            // });
        }
        else {
            var submissionObject = Object.assign(this.basicCastingCallInfo.value);
            this.castingCallService.createCastingCall(submissionObject, this.production.id).subscribe(function (castingCall) {
                _this.isLoading = false;
                _this.stateService.go('draftcastingCall', { castingCall: castingCall, draftId: castingCall.id, popRole: true });
            }, function (error) {
            });
        }
    };
    NewCastingCallComponent.ctorParameters = function () { return [
        { type: _services_casting_call_role_service__WEBPACK_IMPORTED_MODULE_10__["CastingCallRoleService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] },
        { type: _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"] },
        { type: _services_casting_call_service__WEBPACK_IMPORTED_MODULE_8__["CastingCallService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] },
        { type: _services_casting_call_component_service__WEBPACK_IMPORTED_MODULE_9__["CastingCallComponentService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], NewCastingCallComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_6__["Production"])
    ], NewCastingCallComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_4__["MetadataModel"])
    ], NewCastingCallComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], NewCastingCallComponent.prototype, "castingCallId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_casting_call_models__WEBPACK_IMPORTED_MODULE_5__["CastingCall"])
    ], NewCastingCallComponent.prototype, "castingCall", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewCastingCallComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('expires', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_calendar__WEBPACK_IMPORTED_MODULE_13__["Calendar"])
    ], NewCastingCallComponent.prototype, "calendarInputField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('castingCallTypeId', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_12__["Dropdown"])
    ], NewCastingCallComponent.prototype, "castingCallTypeIdField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('castingCallViewType', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_radiobutton__WEBPACK_IMPORTED_MODULE_14__["RadioButton"])
    ], NewCastingCallComponent.prototype, "castingCallViewTypeField", void 0);
    NewCastingCallComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-casting-call',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-casting-call.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.html")).default,
            // make fade in animation available to this component
            animations: [_animations_router_animation__WEBPACK_IMPORTED_MODULE_11__["fadeInAnimation"]],
            // attach the fade in animation to the host (root) element of this component
            host: { '[@fadeInAnimation]': '' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-casting-call.component.scss */ "./src/app/feature-modules/casting-call/components/new-casting-call/new-casting-call.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_casting_call_role_service__WEBPACK_IMPORTED_MODULE_10__["CastingCallRoleService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"], _services_casting_call_service__WEBPACK_IMPORTED_MODULE_8__["CastingCallService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"],
            _services_casting_call_component_service__WEBPACK_IMPORTED_MODULE_9__["CastingCallComponentService"]])
    ], NewCastingCallComponent);
    return NewCastingCallComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/casting-call/services/casting-call-component.service.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/feature-modules/casting-call/services/casting-call-component.service.ts ***!
  \*****************************************************************************************/
/*! exports provided: CastingCallComponentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallComponentService", function() { return CastingCallComponentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var CastingCallComponentService = /** @class */ (function () {
    function CastingCallComponentService() {
        this._saveCastingCallIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.saveCastingCallIntentObs = this._saveCastingCallIntentObs.asObservable();
        this._cancelEditCastingCallIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.cancelEditCastingCallIntentObs = this._cancelEditCastingCallIntentObs.asObservable();
        this._deleteCastingCallIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.deleteCastingCallIntentObs = this._deleteCastingCallIntentObs.asObservable();
    }
    CastingCallComponentService.prototype.subscribeToSaveCastingCallIntentObs = function () {
        return this.saveCastingCallIntentObs;
    };
    CastingCallComponentService.prototype.trySaveCastingCall = function () {
        this._saveCastingCallIntentObs.next(true);
    };
    CastingCallComponentService.prototype.subscribeToCancelEditCastingCallIntentObs = function () {
        return this.cancelEditCastingCallIntentObs;
    };
    CastingCallComponentService.prototype.tryCancelEditCastingCall = function () {
        this._cancelEditCastingCallIntentObs.next(true);
    };
    CastingCallComponentService.prototype.subscribeToDeleteCastingCallIntentObs = function () {
        return this.deleteCastingCallIntentObs;
    };
    CastingCallComponentService.prototype.tryDeleteCastingCall = function () {
        this._deleteCastingCallIntentObs.next(true);
    };
    CastingCallComponentService.prototype.registerCastingCallScrollElement = function (element) {
        this.castingCallElement = element;
    };
    CastingCallComponentService.prototype.registerOtherScrollElement = function (element) {
        this.otherElement = element;
    };
    CastingCallComponentService.prototype.registerArtisticScrollElement = function (element) {
        this.artisticElement = element;
    };
    CastingCallComponentService.prototype.registerCastingScrollElement = function (element) {
        this.castingElement = element;
    };
    CastingCallComponentService.prototype.registerRolesScrollElement = function (element) {
        this.rolesElement = element;
    };
    CastingCallComponentService.prototype.registerBasicScrollElement = function (element) {
        this.basicElement = element;
    };
    //
    CastingCallComponentService.prototype.scrollToCastingCall = function () {
        this.castingCallElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CastingCallComponentService.prototype.scrollToOther = function () {
        this.otherElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CastingCallComponentService.prototype.scrollToArtistic = function () {
        this.artisticElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CastingCallComponentService.prototype.scrollToCasting = function () {
        this.castingElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CastingCallComponentService.prototype.scrollToRoles = function () {
        this.rolesElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CastingCallComponentService.prototype.scrollToBasic = function () {
        this.basicElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    CastingCallComponentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CastingCallComponentService);
    return CastingCallComponentService;
}());



/***/ }),

/***/ "./src/app/models/casting-call.models.ts":
/*!***********************************************!*\
  !*** ./src/app/models/casting-call.models.ts ***!
  \***********************************************/
/*! exports provided: CastingCallRoleTransferObject, BaseCastingCall, CastingCall, NewCastingCall, UpdateCastingCall, BaseCastingCallRole, CastingCallRole, NewCastingCallRole, UpdateCastingCallRole, BaseCastingCallFolder, CastingCallFolder, NewCastingCallFolder, UpdateCastingCallFolder, CastingCallFolderRole, NewCastingCallFolderRole, CastingCallTypeEnum, CastingCallUnionStatusEnum, castingCallLocationReference, LocationTypeEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallRoleTransferObject", function() { return CastingCallRoleTransferObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCastingCall", function() { return BaseCastingCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCall", function() { return CastingCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCastingCall", function() { return NewCastingCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCastingCall", function() { return UpdateCastingCall; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCastingCallRole", function() { return BaseCastingCallRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallRole", function() { return CastingCallRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCastingCallRole", function() { return NewCastingCallRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCastingCallRole", function() { return UpdateCastingCallRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseCastingCallFolder", function() { return BaseCastingCallFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallFolder", function() { return CastingCallFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCastingCallFolder", function() { return NewCastingCallFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateCastingCallFolder", function() { return UpdateCastingCallFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallFolderRole", function() { return CastingCallFolderRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewCastingCallFolderRole", function() { return NewCastingCallFolderRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallTypeEnum", function() { return CastingCallTypeEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CastingCallUnionStatusEnum", function() { return CastingCallUnionStatusEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "castingCallLocationReference", function() { return castingCallLocationReference; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocationTypeEnum", function() { return LocationTypeEnum; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

var CastingCallRoleTransferObject = /** @class */ (function () {
    function CastingCallRoleTransferObject() {
    }
    return CastingCallRoleTransferObject;
}());

var BaseCastingCall = /** @class */ (function () {
    function BaseCastingCall() {
    }
    return BaseCastingCall;
}());

var CastingCall = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CastingCall, _super);
    function CastingCall() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.roles = [];
        return _this;
    }
    return CastingCall;
}(BaseCastingCall));

var NewCastingCall = /** @class */ (function () {
    function NewCastingCall() {
    }
    return NewCastingCall;
}());

var UpdateCastingCall = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateCastingCall, _super);
    function UpdateCastingCall() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateCastingCall;
}(BaseCastingCall));

var BaseCastingCallRole = /** @class */ (function () {
    function BaseCastingCallRole() {
    }
    return BaseCastingCallRole;
}());

var CastingCallRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CastingCallRole, _super);
    function CastingCallRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CastingCallRole;
}(BaseCastingCallRole));

var NewCastingCallRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NewCastingCallRole, _super);
    function NewCastingCallRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewCastingCallRole;
}(BaseCastingCallRole));

var UpdateCastingCallRole = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateCastingCallRole, _super);
    function UpdateCastingCallRole() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateCastingCallRole;
}(BaseCastingCallRole));

var BaseCastingCallFolder = /** @class */ (function () {
    function BaseCastingCallFolder() {
    }
    return BaseCastingCallFolder;
}());

var CastingCallFolder = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](CastingCallFolder, _super);
    function CastingCallFolder() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.folderRoles = [];
        return _this;
    }
    return CastingCallFolder;
}(BaseCastingCallFolder));

var NewCastingCallFolder = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](NewCastingCallFolder, _super);
    function NewCastingCallFolder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return NewCastingCallFolder;
}(BaseCastingCallFolder));

var UpdateCastingCallFolder = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](UpdateCastingCallFolder, _super);
    function UpdateCastingCallFolder() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return UpdateCastingCallFolder;
}(BaseCastingCallFolder));

var CastingCallFolderRole = /** @class */ (function () {
    function CastingCallFolderRole() {
    }
    return CastingCallFolderRole;
}());

var NewCastingCallFolderRole = /** @class */ (function () {
    function NewCastingCallFolderRole() {
    }
    return NewCastingCallFolderRole;
}());

var CastingCallTypeEnum;
(function (CastingCallTypeEnum) {
    CastingCallTypeEnum[CastingCallTypeEnum["publicAll"] = 1] = "publicAll";
    CastingCallTypeEnum[CastingCallTypeEnum["publicMembers"] = 3] = "publicMembers";
    CastingCallTypeEnum[CastingCallTypeEnum["privateSelectMembers"] = 7] = "privateSelectMembers";
})(CastingCallTypeEnum || (CastingCallTypeEnum = {}));
var CastingCallUnionStatusEnum;
(function (CastingCallUnionStatusEnum) {
    CastingCallUnionStatusEnum[CastingCallUnionStatusEnum["notRelevan"] = 0] = "notRelevan";
    CastingCallUnionStatusEnum[CastingCallUnionStatusEnum["nonUnion"] = 1] = "nonUnion";
    CastingCallUnionStatusEnum[CastingCallUnionStatusEnum["union"] = 3] = "union";
    CastingCallUnionStatusEnum[CastingCallUnionStatusEnum["both"] = 7] = "both";
})(CastingCallUnionStatusEnum || (CastingCallUnionStatusEnum = {}));
var castingCallLocationReference = /** @class */ (function () {
    function castingCallLocationReference() {
    }
    return castingCallLocationReference;
}());

var LocationTypeEnum;
(function (LocationTypeEnum) {
    LocationTypeEnum[LocationTypeEnum["na"] = 0] = "na";
    LocationTypeEnum[LocationTypeEnum["city"] = 1] = "city";
    LocationTypeEnum[LocationTypeEnum["region"] = 3] = "region";
    LocationTypeEnum[LocationTypeEnum["country"] = 7] = "country";
})(LocationTypeEnum || (LocationTypeEnum = {}));


/***/ })

}]);
//# sourceMappingURL=feature-modules-casting-call-casting-call-module.js.map