﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Agents;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IAgentAsyncRepository
    {
        Task<Agent> GetAgentByIdAsync(int agentId);
        Task<Agent> GetAgentByIdAsync(int agentId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<IEnumerable<Agent>> GetAgentsAsync();
        Task<IEnumerable<Agent>> GetAgentsAsync(bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<IEnumerable<Agent>> GetAgentsByTalentIdAsync(int talentId);
        Task<IEnumerable<Agent>> GetAgentsByTalentIdAsync(int talentId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<IEnumerable<Agent>> GetAgentsByCityIdAsync(int cityId);
        Task<IEnumerable<Agent>> GetAgentsByCityIdAsync(int cityId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<IEnumerable<Agent>> GetAgentsByCityIdsAsync(List<int> cityIds);
        Task<IEnumerable<Agent>> GetAgentsByCityIdsAsync(List<int> cityIds, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<IEnumerable<Agent>> GetAgentsByAgencyOfficeIdAsync(int agencyOfficeId);
        Task<IEnumerable<Agent>> GetAgentsByAgencyOfficeIdAsync(int agencyOfficeId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<IEnumerable<Agent>> GetAgentsByAgencyIdAsync(int agencyId);
        Task<IEnumerable<Agent>> GetAgentsByAgencyIdAsync(int agencyId, bool withAgencyOffice, bool withAgency, bool withTalentReferences, bool withTalents, bool withManagedTalents, bool withCoverageCityReferences, bool withCities);
        Task<int> AddNewAgentAsync(Agent agent);
        Task<int> DeleteAgentAsync(Agent agent);
        Task<int> UpdateAgentAsync(Agent agent);
        Task<Agency> GetAgencyByIdAsync(int agencyId);
        Task<Agency> GetAgencyByIdAsync(int agencyId, bool withAgents, bool withOffices);
        Task<IEnumerable<Agency>> GetAgenciesAsync();
        Task<IEnumerable<Agency>> GetAgenciesAsync(bool withAgents, bool withOffices);
        Task<int> AddNewAgencyAsync(Agency agency);
        Task<int> DeleteAgencyAsync(Agency agency);
        Task<int> UpdateAgencyAsync(Agency agency);



        Task<AgencyOffice> GetAgencyOfficeByIdAsync(int agencyOfficeId);
        Task<IEnumerable<AgencyOffice>> GetAgencyOfficesAsync();
        Task<int> AddNewAgencyOfficeAsync(AgencyOffice agencyOffice);
        Task<int> DeleteAgencyOfficeAsync(AgencyOffice agencyOffice);
        Task<int> UpdateAgencyOfficeAsync(AgencyOffice agencyOffice);


        Task<AgentCityReference> GetAgentCityReferenceByIdAsync(int agentCityReferenceId);
        Task<IEnumerable<AgentCityReference>> GetAgentCityReferencesAsync();
        Task<int> AddNewAgentCityReferenceAsync(AgentCityReference agentCityReference);
        Task<int> DeleteAgentCityReferenceAsync(AgentCityReference agentCityReference);
        Task<int> UpdateAgentCityReferenceAsync(AgentCityReference agentCityReference);


        Task<AgentTalentReference> GetAgentTalentReferenceByIdAsync(int agentTalentReferenceId, bool withAgent, bool withTalent);
        Task<IEnumerable<AgentTalentReference>> GetAgentTalentReferencesByAgentIdAsync(int agentId, bool withAgent, bool withTalent);
        Task<IEnumerable<AgentTalentReference>> GetAgentTalentReferencesByTalentIdAsync(int talentId, bool withAgent, bool withTalent);
        Task<int> AddNewAgentTalentReferenceAsync(AgentTalentReference agentTalentReference);
        Task<int> DeleteAgentTalentReferenceAsync(AgentTalentReference agentTalentReference);
        Task<int> UpdateAgentTalentReferenceAsync(AgentTalentReference agentTalentReference);

        IEnumerable<Agent> GetComplete();
       
    }
}