﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class FileService : BaseService, IFileService
    {
        private IFileAsyncRepository _fileRepository;

        public FileService(IFileAsyncRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

     
        public async Task<File> GetFileByFileIdAsync(string fileId)
        {
            return await _fileRepository.GetFileByFileIdAsync(fileId);
        }
       
        public async Task<int> AddNewFileAsync(File file)
        {
            return await _fileRepository.AddNewFileAsync(file);
        }

        public async Task<int> DeleteFileAsync(File file)
        {
            return await _fileRepository.DeleteFileAsync(file);
        }
      
    }
}
