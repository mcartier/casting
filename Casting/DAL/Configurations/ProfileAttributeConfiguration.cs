﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;

using Casting.Model.Entities.AttributeReferences;
using Casting.Model.Entities.Attributes;

namespace Casting.DAL.Configurations
{

    #region Attributes
    class StereoTypeConfiguration : EntityTypeConfiguration<StereoType>
{
    public StereoTypeConfiguration()
    {
        HasKey(x => x.Id);

    }
}

class EthnicStereoTypeConfiguration : EntityTypeConfiguration<EthnicStereoType>
    {
        public EthnicStereoTypeConfiguration()
        {
            HasKey(x => x.Id);
          
        }
    }
class EthnicityConfiguration : EntityTypeConfiguration<Ethnicity>
{
    public EthnicityConfiguration()
    {
        HasKey(x => x.Id);

    }
}

class UnionConfiguration : EntityTypeConfiguration<Union>
{
    public UnionConfiguration()
    {
        HasKey(x => x.Id);

    }
}
class AccentConfiguration : EntityTypeConfiguration<Accent>
{
    public AccentConfiguration()
    {
        HasKey(x => x.Id);

    }
}
class LanguageConfiguration : EntityTypeConfiguration<Language>
{
    public LanguageConfiguration()
    {
        HasKey(x => x.Id);

    }
}
class SkillConfiguration : EntityTypeConfiguration<Skill>
{
    public SkillConfiguration()
    {
        HasKey(x => x.Id);
        HasRequired(c => c.SkillCategory);
        //.WithMany(s => s.Skills)
        //.HasForeignKey(k => k.SkillCategoryId);

    }
}
class SkillCategoryConfiguration : EntityTypeConfiguration<SkillCategory>
{
    public SkillCategoryConfiguration()
    {
        HasKey(x => x.Id);
        HasMany(s => s.Skills)
            .WithRequired(c => c.SkillCategory);
        //.HasForeignKey(k => k.SkillCategoryId);

    }
}
    #endregion

    #region Attribute References
    class StereoTypeReferenceConfiguration : EntityTypeConfiguration<StereoTypeReference>
    {
        public StereoTypeReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.StereoType);

        }
    }

    class EthnicStereoTypeReferenceConfiguration : EntityTypeConfiguration<EthnicStereoTypeReference>
    {
        public EthnicStereoTypeReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.EthnicStereoType);
        }
    }
    class EthnicityReferenceConfiguration : EntityTypeConfiguration<EthnicityReference>
    {
        public EthnicityReferenceConfiguration()
        {
            HasKey(x => x.Id);
           HasRequired(s => s.Ethnicity);
        }
    }

    class UnionReferenceConfiguration : EntityTypeConfiguration<UnionReference>
    {
        public UnionReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.Union);
        }
    }
    class AccentReferenceConfiguration : EntityTypeConfiguration<AccentReference>
    {
        public AccentReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.Accent);
        }
    }
    class LanguageReferenceConfiguration : EntityTypeConfiguration<LanguageReference>
    {
        public LanguageReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.Language);
        }
    }
    class BodyTypeReferenceConfiguration : EntityTypeConfiguration<BodyTypeReference>
    {
        public BodyTypeReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.BodyType);
        }
    }
    class HairColorReferenceConfiguration : EntityTypeConfiguration<HairColorReference>
    {
        public HairColorReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.HairColor);
        }
    }
    class EyeColorReferenceConfiguration : EntityTypeConfiguration<EyeColorReference>
    {
        public EyeColorReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.EyeColor);
        }
    }
    class SkillReferenceConfiguration : EntityTypeConfiguration<SkillReference>
    {
        public SkillReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(s => s.Skill);
        }
    }

    #endregion



    #region ProfileAttributes
    class ProfileStereoTypeReferenceConfiguration : EntityTypeConfiguration<ProfileStereoTypeReference>
    {
        public ProfileStereoTypeReferenceConfiguration()
        {
            ToTable("ProfileStereoTypeReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Profile).WithMany(s=>s.StereoTypes);
            HasRequired(s => s.StereoType);

        }
    }

    class ProfileEthnicStereoTypeReferenceConfiguration : EntityTypeConfiguration<ProfileEthnicStereoTypeReference>
    {
        public ProfileEthnicStereoTypeReferenceConfiguration()
        {
            ToTable("ProfileEthnicStereoTypeReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Profile).WithMany(s => s.EthnicStereoTypes);
            HasRequired(s => s.EthnicStereoType);
        }
    }
    class ProfileEthnicityReferenceConfiguration : EntityTypeConfiguration<ProfileEthnicityReference>
    {
        public ProfileEthnicityReferenceConfiguration()
        {
            ToTable("ProfileEthnicityReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Profile).WithMany(s => s.Ethnicities);
            HasRequired(s => s.Ethnicity);
        }
    }

    class ProfileUnionReferenceConfiguration : EntityTypeConfiguration<ProfileUnionReference>
    {
        public ProfileUnionReferenceConfiguration()
        {
            ToTable("ProfileUnionReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Profile).WithMany(s => s.Unions);
            HasRequired(s => s.Union);
        }
    }
    class ProfileAccentReferenceConfiguration : EntityTypeConfiguration<ProfileAccentReference>
    {
        public ProfileAccentReferenceConfiguration()
        {
            ToTable("ProfileAccentReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Profile).WithMany(s => s.Accents);
            HasRequired(s => s.Accent);
        }
    }
    class ProfileLanguageReferenceConfiguration : EntityTypeConfiguration<ProfileLanguageReference>
    {
        public ProfileLanguageReferenceConfiguration()
        {
            ToTable("ProfileLanguageReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Profile).WithMany(s => s.Languages);
            HasRequired(s => s.Language);
        }
    }

    class ProfileBodyTypeReferenceConfiguration : EntityTypeConfiguration<ProfileBodyTypeReference>
    {
        public ProfileBodyTypeReferenceConfiguration()
        {
            ToTable("ProfileBodyTypeReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Profile).WithMany(s => s.Languages).HasForeignKey(k => k.ProfileId);
            HasRequired(s => s.BodyType);
        }
    }
    class ProfileEyeColorReferenceConfiguration : EntityTypeConfiguration<ProfileEyeColorReference>
    {
        public ProfileEyeColorReferenceConfiguration()
        {
            ToTable("ProfileEyeColorReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Profile).WithMany(s => s.Languages).HasForeignKey(k => k.ProfileId);
            HasRequired(s => s.EyeColor);
        }
    }
    class ProfileHairColorReferenceConfiguration : EntityTypeConfiguration<ProfileHairColorReference>
    {
        public ProfileHairColorReferenceConfiguration()
        {
            ToTable("ProfileHairColorReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Profile).WithMany(s => s.Languages).HasForeignKey(k => k.ProfileId);
            HasRequired(s => s.HairColor);
        }
    }
    class ProfileSkillReferenceConfiguration : EntityTypeConfiguration<ProfileSkillReference>
    {
        public ProfileSkillReferenceConfiguration()
        {
            ToTable("ProfileSkillReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Profile).WithMany(s => s.Languages).HasForeignKey(k => k.ProfileId);
            HasRequired(s => s.Skill);
        }
    }
    #endregion

    #region RoleAttributes
    class RoleStereoTypeReferenceConfiguration : EntityTypeConfiguration<RoleStereoTypeReference>
    {
        public RoleStereoTypeReferenceConfiguration()
        {
            ToTable("RoleStereoTypeReference");
            HasKey(x => x.Id);
           // HasRequired(p => p.Role).WithMany(s => s.StereoTypes).HasForeignKey(k=>k.RoleId);
            HasRequired(s => s.StereoType);

        }
    }

    class RoleEthnicStereoTypeReferenceConfiguration : EntityTypeConfiguration<RoleEthnicStereoTypeReference>
    {
        public RoleEthnicStereoTypeReferenceConfiguration()
        {
            ToTable("RoleEthnicStereoTypeReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.EthnicStereoTypes).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.EthnicStereoType);
        }
    }
    class RoleEthnicityReferenceConfiguration : EntityTypeConfiguration<RoleEthnicityReference>
    {
        public RoleEthnicityReferenceConfiguration()
        {
            ToTable("RoleEthnicityReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.Ethnicities).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.Ethnicity);
        }
    }

     class RoleAccentReferenceConfiguration : EntityTypeConfiguration<RoleAccentReference>
    {
        public RoleAccentReferenceConfiguration()
        {
            ToTable("RoleAccentReference");
            HasKey(x => x.Id);
           // HasRequired(p => p.Role).WithMany(s => s.Accents).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.Accent);
        }
    }
    class RoleLanguageReferenceConfiguration : EntityTypeConfiguration<RoleLanguageReference>
    {
        public RoleLanguageReferenceConfiguration()
        {
            ToTable("RoleLanguageReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.Languages).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.Language);
        }
    }
    class RoleBodyTypeReferenceConfiguration : EntityTypeConfiguration<RoleBodyTypeReference>
    {
        public RoleBodyTypeReferenceConfiguration()
        {
            ToTable("RoleBodyTypeReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.Languages).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.BodyType);
        }
    }
    class RoleEyeColorReferenceConfiguration : EntityTypeConfiguration<RoleEyeColorReference>
    {
        public RoleEyeColorReferenceConfiguration()
        {
            ToTable("RoleEyeColorReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.Languages).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.EyeColor);
        }
    }
    class RoleHairColorReferenceConfiguration : EntityTypeConfiguration<RoleHairColorReference>
    {
        public RoleHairColorReferenceConfiguration()
        {
            ToTable("RoleHairColorReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.Languages).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.HairColor);
        }
    }
    class RoleSkillReferenceConfiguration : EntityTypeConfiguration<RoleSkillReference>
    {
        public RoleSkillReferenceConfiguration()
        {
            ToTable("RoleSkillReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.Role).WithMany(s => s.Languages).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.Skill);
        }
    }
    #endregion

    #region CastingCallAttributes
    class CastingCallUnionReferenceConfiguration : EntityTypeConfiguration<CastingCallUnionReference>
    {
        public CastingCallUnionReferenceConfiguration()
        {
            ToTable("CastingCallUnionReference");
            HasKey(x => x.Id);
            HasRequired(p => p.CastingCall).WithMany(s => s.Unions).HasForeignKey(k => k.CastingCallId);
            HasRequired(s => s.Union);
        }
    }

    class CastingCallRoleUnionReferenceConfiguration : EntityTypeConfiguration<CastingCallRoleUnionReference>
    {
        public CastingCallRoleUnionReferenceConfiguration()
        {
            ToTable("CastingCallRoleUnionReference");
            HasKey(x => x.Id);
            HasRequired(p => p.Role).WithMany(s => s.Unions).HasForeignKey(k => k.RoleId);
            HasRequired(s => s.Union);
        }
    }

    #endregion
    /*
    #region ProfileSearchCriteriaAttributes
    class ProfileSearchCriteriaStereoTypeReferenceConfiguration : EntityTypeConfiguration<ProfileSearchCriteriaStereoTypeReference>
    {
        public ProfileSearchCriteriaStereoTypeReferenceConfiguration()
        {
            ToTable("ProfileSearchCriteriaStereoTypeReference");
            HasKey(x => x.Id);
            //HasRequired(p => p.ProfileSearchCriteria).WithMany(s => s.StereoTypes);
            HasRequired(s => s.StereoType);

        }
    }

    class ProfileSearchCriteriaEthnicStereoTypeReferenceConfiguration : EntityTypeConfiguration<ProfileSearchCriteriaEthnicStereoTypeReference>
    {
        public ProfileSearchCriteriaEthnicStereoTypeReferenceConfiguration()
        {
            ToTable("ProfileSearchCriteriaEthnicStereoTypeReference");
            HasKey(x => x.Id);
           // HasRequired(p => p.ProfileSearchCriteria).WithMany(s => s.EthnicStereoTypes);
            HasRequired(s => s.EthnicStereoType);
        }
    }
    class ProfileSearchCriteriaEthnicityReferenceConfiguration : EntityTypeConfiguration<ProfileSearchCriteriaEthnicityReference>
    {
        public ProfileSearchCriteriaEthnicityReferenceConfiguration()
        {
            ToTable("ProfileSearchCriteriaEthnicityReference");
            HasKey(x => x.Id);
          //  HasRequired(p => p.ProfileSearchCriteria).WithMany(s => s.Ethnicities);
            HasRequired(s => s.Ethnicity);
        }
    }

    class ProfileSearchCriteriaUnionReferenceConfiguration : EntityTypeConfiguration<ProfileSearchCriteriaUnionReference>
    {
        public ProfileSearchCriteriaUnionReferenceConfiguration()
        {
            ToTable("ProfileSearchCriteriaUnionReference");
            HasKey(x => x.Id);
            HasRequired(p => p.ProfileSearchCriteria).WithMany(s => s.Unions);
            HasRequired(s => s.Union);
        }
    }
    class ProfileSearchCriteriaAccentReferenceConfiguration : EntityTypeConfiguration<ProfileSearchCriteriaAccentReference>
    {
        public ProfileSearchCriteriaAccentReferenceConfiguration()
        {
            ToTable("ProfileSearchCriteriaAccentReference");
            HasKey(x => x.Id);
            HasRequired(p => p.ProfileSearchCriteria).WithMany(s => s.Accents);
            HasRequired(s => s.Accent);
        }
    }
    class ProfileSearchCriteriaLanguageReferenceConfiguration : EntityTypeConfiguration<ProfileSearchCriteriaLanguageReference>
    {
        public ProfileSearchCriteriaLanguageReferenceConfiguration()
        {
            ToTable("ProfileSearchCriteriaLanguageReference");
            HasKey(x => x.Id);
            HasRequired(p => p.ProfileSearchCriteria).WithMany(s => s.Languages);
            HasRequired(s => s.Language);
        }
    }
    #endregion
    */
}
;