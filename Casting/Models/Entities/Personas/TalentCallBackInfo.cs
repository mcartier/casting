﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Talents;

namespace Casting.Model.Entities.Personas
{
    public class TalentCallBackInfo:Logger
    {
        [Key]
        public int Id { get; set; }

        public int? SubmittingAgentId { get; set; }
        [ForeignKey("SubmittingAgentId")]
        public Agent SubmittingAgent { get; set; }

        public string ContactAgentEmail { get; set; }
        public string ContactAgentPhoneNumber { get; set; }

        public int? ContactAgentId { get; set; }
        [ForeignKey("ContactAgentId")]
        public Agent ContactAgent { get; set; }

        public string TalentEmail { get; set; }
        public string TalentPhoneNumber { get; set; }
    
        public int TalentId { get; set; }
        [ForeignKey("TalentId")]
        public Talent Talent { get; set; }

    }
}
