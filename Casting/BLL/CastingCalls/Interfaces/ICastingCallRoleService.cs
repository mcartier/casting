﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;

namespace Casting.BLL.CastingCalls.Interfaces
{
    public interface ICastingCallRoleService
    {
        Task<CastingCallRole> GetCastingCallRoleByIdAsync(int castingCallRoleId);
        Task<CastingCallRole> GetCastingCallRoleByIdWithPersonasAsync(int castingCallRoleId);
        Task<CastingCallRole> GetCastingCallRoleByIdWithPersonasAndAssetsAsync(int castingCallRoleId);
        Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdAsync(int castingCallId);
        Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdWithPersonasAsync(int castingCallId);
        Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdWithPersonasAndAssetsAsync(int castingCallId);
        Task<int> AddNewCastingCallRoleAsync(bool orderAtTheTop, CastingCallRole castingCallRole);
        Task<int> UpdateCastingCallRoleAsync(CastingCallRole castingCallRole);
        Task<int> DeleteCastingCallRole(CastingCallRole castingCallRole);
    }
}