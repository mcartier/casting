import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';

import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Production,Role,NewRole, UpdateRole, UpdateProduction, NewProduction } from '../models/production.models';
import { WINDOW } from './browser.window.provider';
import { HttpHelpers } from '../utilities/HttpHelpers';
@Injectable({
    providedIn: 'root'
})
export class ProductionRoleService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }
    private _deleteRoleObs: Subject<Role> = new Subject<Role>();
    public readonly deleteRoleObs: Observable<Role> = this._deleteRoleObs.asObservable();

    private _editRoleObs: Subject<Role> = new Subject<Role>();
    public readonly editRoleObs: Observable<Role> = this._editRoleObs.asObservable();

    private _newRoleObs: Subject<Role> = new Subject<Role>();
    public readonly newRoleObs: Observable<Role> = this._newRoleObs.asObservable();

    public subscribeToCreateRoleObs(): Observable<Role> {
      return this.newRoleObs;
    }
    public subscribeToEditRoleObs(): Observable<Role> {
      return this.editRoleObs;
    }

    public subscribeToDeleteRoleObs(): Observable<Role> {
      return this.deleteRoleObs;
    }

    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;
    private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/roles/";
        return "/api/productions/";
    }

    public createRole(productionId: number, role: Role): Observable<Role> {
        role.productionId = productionId;
        let controllerQuery = this.getBaseURL() + productionId + "/roles/";
        return this.postAction<Role>(role, controllerQuery)
            .pipe(
                map((res: Role) => {
                    //do anything with the returned data here...
                this._newRoleObs.next(res);
                    return res
                })
            )
    };

    public saveRole(id: number, productionId: number, role: Role): Observable<Role> {
        let controllerQuery = this.getBaseURL() + productionId + "/roles/" + id;
        return this.putAction<Role>(role, controllerQuery)
            .pipe(
            map((res: Role) => {
                    //do anything with the returned data here...
                this._editRoleObs.next(res);
                return res;
                })
            )
    };


    public deleteRole(roleId: number, productionId: number): Observable<Role> {
        let controllerQuery = this.getBaseURL() + productionId + "/roles/" + roleId;
        return this.deleteAction<Role>(controllerQuery)
            .pipe(
                map((res: Role) => {
                    //do anything with the returned data here...
                this._deleteRoleObs.next(res);
                    return res
                })
            )
    };



    public getRoles(orderByIndex: number): Observable<Role[]> {
        let controllerQuery = this.getBaseURL() + "?orderBy=" + orderByIndex;
        return this.getAction<Role[]>(controllerQuery)
            .pipe(
                map((res: Role[]) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };

    public getRole(roleId: number, productionId:number): Observable<Role> {
       // let controllerQuery = this.getBaseURL() + roleId;
        let controllerQuery = this.getBaseURL() +productionId + "/roles/" + roleId;
        return this.getAction<Role>(controllerQuery)
            .pipe(
                map((res: Role) => {
                    //do anything with the returned data here...
                    return res
                })
            );
    };
}
