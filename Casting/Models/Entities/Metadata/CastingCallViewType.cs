﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Metadata
{
   public class CastingCallViewType:Logger
    {
        public int Id { get; set; }

        public string Name { get; set; }

    }
}
