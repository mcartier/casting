import { Component, OnDestroy, OnInit, Input } from '@angular/core';
import { Transition, StateService, equals, TransitionService } from '@uirouter/core';
import { Subscription } from 'rxjs';
import { MetadataService } from '../../../../services/metadata.service';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../../../../models/metadata';
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../../../../models/production.models';
import { ProductionService } from '../../../../services/production.service';
import { ProductionComponentService } from '../../services/production-component.service';
import { fadeInAnimation } from '../../../../animations/router.animation';
import { PageEvent } from '@angular/material';


@Component({
  selector: 'app-productions',
  templateUrl: './productions.component.html',
    styleUrls: ['./productions.component.scss'],
    // make fade in animation available to this component
  animations: [fadeInAnimation],

  // attach the fade in animation to the host (root) element of this component
  host: { '[@fadeInAnimation]': '' }
})
export class ProductionsComponent implements OnInit {

  constructor(

    private metadataService: MetadataService,
    private productionService: ProductionService,
    public stateService: StateService,
    public transitionService: TransitionService,
    public transition: Transition,
    private productionComponentService: ProductionComponentService
  ) { }
 
  @Input() productions: Production[];

    ngOnInit() {
     }
  
  private createNewProduction() {
    this.stateService.go('newproduction');
    //this.productionComponentService.scrollToProduction();
  }

}
