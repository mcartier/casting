﻿using Casting.BLL.Productions;
using Casting.BLL.Productions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Productions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.Production;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.CastingCalls.Interfaces;
using Casting.Model.Entities.CastingCalls;
using Casting.DAL;
using System.Data.SqlTypes;
using TaskExtensions = System.Data.Entity.Utilities.TaskExtensions;
using System.Web.Hosting;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/productions/{productionId}/extras")]
    [Authorize]
    public class ProductionExtrasController : BaseDirectorApiController
    {
        private IProductionService _productionService;
        private ICastingCallService _castingCallService;
        private IProductionRoleService _productionRoleService;
        private IMetadataService _metadataService;


        private IAuditionDetailService _auditionDetailService;
        private IPayDetailService _payDetailService;
        private IShootDetailService _shootDetailService;
        private ISideService _sideService;
        private IFileService _fileService;

        private readonly UserManager<ApplicationUser> _userManager;

        public ProductionExtrasController(UserManager<ApplicationUser> userManager, IMetadataService metadataService, IProductionService productionService, IProductionRoleService productionRoleService, ICastingCallService castingCallService, ICastingContext castingContext
            , IAuditionDetailService auditionDetailService
            , IPayDetailService payDetailService
            , IShootDetailService shootDetailService
            , ISideService sideService
            , IFileService fileService) : base(castingContext)
        {
            _productionService = productionService;
            _auditionDetailService = auditionDetailService;
            _payDetailService = payDetailService;
            _shootDetailService = shootDetailService;
            _sideService = sideService;
            _fileService = fileService;
            _castingCallService = castingCallService;
            _productionRoleService = productionRoleService;
            _metadataService = metadataService;
            _userManager = userManager;
        }
      
     

        #region auditionMetaData


        // GET: api/productions/[id}/auditionDetails
        [Route("auditiondetails")]
        [ResponseType(typeof(IEnumerable<AuditionDetail>))]
        public async Task<IHttpActionResult> GetProductionAuditionDetails(int productionId)
        {

            var productionAuditionDetailss =
                await _auditionDetailService.GetAuditionDetailsByProductionIdAsync(productionId);

            return Ok(productionAuditionDetailss);
        }


        // POST: api/productions/[id}/auditionDetails
        [Route("auditiondetails")]
        [ResponseType(typeof(AuditionDetail))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionAuditionDetail(int productionId, AuditionDetailBindingModel postedAuditionDetail)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            if (postedAuditionDetail.IsOneDayOnly)
            {
                postedAuditionDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                
            }
            if (postedAuditionDetail.DatesToBeDecidedLater)
            {
                postedAuditionDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                postedAuditionDetail.StartDate = (DateTime)SqlDateTime.MinValue;

            }

            if (postedAuditionDetail.auditionTypeEnum == AuditionTypeEnum.TapedSubmissionsOnly)
            {
                postedAuditionDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                postedAuditionDetail.StartDate = (DateTime)SqlDateTime.MinValue;
                postedAuditionDetail.DatesToBeDecidedLater = false;
                postedAuditionDetail.DatesAreTemptative = false;
                postedAuditionDetail.IsOneDayOnly = false;
                postedAuditionDetail.LocationDetail = "";
                postedAuditionDetail.Location = "";
            }
            var auditionDetail = new AuditionDetail()
            {
                ProductionId = postedAuditionDetail.ProductionId,
                Name=postedAuditionDetail.Name,
                StartDate = postedAuditionDetail.StartDate.Value,
                 EndDate = postedAuditionDetail.EndDate.Value,
                 Location = postedAuditionDetail.Location,
                 LocationDetail = postedAuditionDetail.LocationDetail,
                 AuditionTypeEnum = postedAuditionDetail.auditionTypeEnum,
                 OnlyThoseSelectedWillParticipate = postedAuditionDetail.OnlyThoseSelectedWillParticipate,
                 DatesToBeDecidedLater = postedAuditionDetail.DatesToBeDecidedLater,
                 DatesAreTemptative = postedAuditionDetail.DatesAreTemptative,
                 Instructions = postedAuditionDetail.Instructions,
                 Details = postedAuditionDetail.Details,
                 IsOneDayOnly = postedAuditionDetail.IsOneDayOnly,
                // DurationInDays = 
            };
            var ret =
                await _auditionDetailService.AddNewAuditionDetailAsync(auditionDetail);

            return Created("api/productions/" + productionId + "/extras/auditionDetail/" + auditionDetail.Id, auditionDetail);
        }

        // PUT: api/productions/{id}/auditiondetails/{auditionDetailId}
        [Route("auditiondetails/{id}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionAuditionDetail(int productionId, int id, AuditionDetailBindingModel postedAuditionDetail)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }

            var auditionDetail = await _auditionDetailService.GetAuditionDetailByIdAsync(id);
            if (auditionDetail == null || auditionDetail.ProductionId != productionId)
            {
                return NotFound();
            }
            if (postedAuditionDetail.IsOneDayOnly)
            {
                postedAuditionDetail.EndDate = (DateTime)SqlDateTime.MinValue;
              
            }
            if (postedAuditionDetail.DatesToBeDecidedLater)
            {
                postedAuditionDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                postedAuditionDetail.StartDate = (DateTime)SqlDateTime.MinValue;

            }

            if (postedAuditionDetail.auditionTypeEnum == AuditionTypeEnum.TapedSubmissionsOnly)
            {
                postedAuditionDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                postedAuditionDetail.StartDate = (DateTime)SqlDateTime.MinValue;
                postedAuditionDetail.DatesToBeDecidedLater = false;
                postedAuditionDetail.DatesAreTemptative = false;
                postedAuditionDetail.IsOneDayOnly = false;
                postedAuditionDetail.LocationDetail = "";
                postedAuditionDetail.Location = "";
            }

            //make modifications to auditionDetail object here from values of the AuditionDetailBindingModel
            auditionDetail.Name = postedAuditionDetail.Name;
            auditionDetail.ProductionId = postedAuditionDetail.ProductionId;
            auditionDetail.IsOneDayOnly = postedAuditionDetail.IsOneDayOnly;
            auditionDetail.StartDate = postedAuditionDetail.StartDate.Value;
            auditionDetail.EndDate = postedAuditionDetail.EndDate.Value;
            auditionDetail.Location = postedAuditionDetail.Location;
            auditionDetail.Instructions = postedAuditionDetail.Instructions;
            auditionDetail.Details = postedAuditionDetail.Details;
            auditionDetail.LocationDetail = postedAuditionDetail.LocationDetail;
            auditionDetail.AuditionTypeEnum = postedAuditionDetail.auditionTypeEnum;
            auditionDetail.OnlyThoseSelectedWillParticipate = postedAuditionDetail.OnlyThoseSelectedWillParticipate;
            auditionDetail.DatesToBeDecidedLater = postedAuditionDetail.DatesToBeDecidedLater;
            auditionDetail.DatesAreTemptative = postedAuditionDetail.DatesAreTemptative;

            try
            {
                await _auditionDetailService.UpdateAuditionDetailAsync(auditionDetail);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/productions/[id}/auditionDetails/{auditionDetailId}
        [Route("auditiondetails/{id}")]
        [ResponseType(typeof(AuditionDetail))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionAuditionDetail(int productionId, int id)
        {

            var auditionDetail = await _auditionDetailService.GetAuditionDetailByIdAsync(id);
            if (auditionDetail == null || auditionDetail.ProductionId != productionId)
            {
                return NotFound();
            }


            await _auditionDetailService.DeleteAuditionDetailAsync(auditionDetail);

            return Ok(auditionDetail);

        }


        // GET: api/productions/[id}/sides
        [Route("sides")]
        [ResponseType(typeof(IEnumerable<Side>))]
        public async Task<IHttpActionResult> GetProductionSides(int productionId)
        {

            var productionSidess =
                await _sideService.GetSidesByProductionIdAsync(productionId);

            return Ok(productionSidess);
        }


        // POST: api/productions/[id}/sides
        [Route("sides")]
        [ResponseType(typeof(Side))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionSide(int productionId, SideBindingModel postedSide)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
              var sideFile = await _fileService.GetFileByFileIdAsync(postedSide.FileId);
            if (sideFile == null)
            {
                return BadRequest();
            }
            var side = new Side()
            {
                ProductionId = postedSide.ProductionId,
                Name = postedSide.Name,
                FileName = sideFile.FileName,
                //OriginalFileName = sideFile.OriginalFileName,
                PastedText = postedSide.PastedText,
                FileId = postedSide.FileId

                // DurationInDays = 
            };
            var ret =
                await _sideService.AddNewSideAsync(side);
            if (ret == 0)
            {
                return InternalServerError(new Exception("unable to remove sideFile record from db"));
            }
            var ret2 = await _fileService.DeleteFileAsync(sideFile);

            return Created("api/productions/" + productionId + "/extras/side/" + side.Id, side);
        }

        // PUT: api/productions/{id}/sides/{sideId}
        [Route("sides/{id}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionSide(int productionId, int id, SideBindingModel postedSide)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var side = await _sideService.GetSideByIdAsync(id);
            if (side == null || side.ProductionId != productionId)
            {
                return NotFound();
            }

            //make modifications to side object here from values of the SideBindingModel
           side.Name = postedSide.Name;
            side.FileName = postedSide.FileName;
            side.PastedText = postedSide.PastedText;
            try
            {
                await _sideService.UpdateSideAsync(side);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

         [Route("sides/{id}")]
        [ResponseType(typeof(Side))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionSide(int productionId, int id)
        {

            var side = await _sideService.GetSideByIdAsync(id);
            if (side == null || side.ProductionId != productionId)
            {
                return NotFound();
            }
                 try
               {
                   string fullPath = HostingEnvironment.MapPath("C:\\processedfiles\\sides\\" + side.FileName);
                   if (System.IO.File.Exists(fullPath))
                   {
                       System.IO.File.Delete(fullPath);
                   }
                   var ret = await _sideService.DeleteSideAsync(side);

                }
                catch (Exception e)
               {
                   throw e;
               }
               
              
            
            return Ok(side);

        }



        // GET: api/productions/[id}/shootDetails
        [Route("shootdetails")]
        [ResponseType(typeof(IEnumerable<ShootDetail>))]
        public async Task<IHttpActionResult> GetProductionShootDetails(int productionId)
        {

            var productionShootDetailss =
                await _shootDetailService.GetShootDetailsByProductionIdAsync(productionId);

            return Ok(productionShootDetailss);
        }


        // POST: api/productions/[id}/shootDetails
        [Route("shootdetails")]
        [ResponseType(typeof(ShootDetail))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionShootDetail(int productionId, ShootDetailBindingModel postedShootDetail)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            if (postedShootDetail.IsOneDayOnly)
            {
                postedShootDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                
            }
            if (postedShootDetail.DatesToBeDecidedLater)
            {
                postedShootDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                postedShootDetail.StartDate = (DateTime)SqlDateTime.MinValue;

            }

           
            var shootDetail = new ShootDetail()
            {
                ProductionId = postedShootDetail.ProductionId,
                Name = postedShootDetail.Name,
                StartDate = postedShootDetail.StartDate.Value,
                EndDate = postedShootDetail.EndDate.Value,
                Location = postedShootDetail.Location,
                LocationDetail = postedShootDetail.LocationDetail,
                DatesToBeDecidedLater = postedShootDetail.DatesToBeDecidedLater,
                DatesAreTemptative = postedShootDetail.DatesAreTemptative,
                Instructions = postedShootDetail.Instructions,
                Details = postedShootDetail.Details,
                IsOneDayOnly = postedShootDetail.IsOneDayOnly,
                DaysOfShooting = postedShootDetail.DaysOfShooting
            };
            var ret =
                await _shootDetailService.AddNewShootDetailAsync(shootDetail);

           return Created("api/productions/" + productionId + "/extras/shootDetail/" + shootDetail.Id, shootDetail);
        }

        // PUT: api/productions/{id}/shootdetails/{shootDetailId}
        [Route("shootdetails/{id}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionShootDetail(int productionId, int id, ShootDetailBindingModel postedShootDetail)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var shootDetail = await _shootDetailService.GetShootDetailByIdAsync(id);
            if (shootDetail == null || shootDetail.ProductionId != productionId)
            {
                return NotFound();
            }
            if (postedShootDetail.IsOneDayOnly)
            {
                postedShootDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                
            }
            if (postedShootDetail.DatesToBeDecidedLater)
            {
                postedShootDetail.EndDate = (DateTime)SqlDateTime.MinValue;
                postedShootDetail.StartDate = (DateTime)SqlDateTime.MinValue;
            }

            //make modifications to shootDetail object here from values of the ShootDetailBindingModel
            shootDetail.Name = postedShootDetail.Name;
            shootDetail.ProductionId = postedShootDetail.ProductionId;
            shootDetail.IsOneDayOnly = postedShootDetail.IsOneDayOnly;
            shootDetail.StartDate = postedShootDetail.StartDate.Value;
            shootDetail.EndDate = postedShootDetail.EndDate.Value;
            shootDetail.Location = postedShootDetail.Location;
            shootDetail.Instructions = postedShootDetail.Instructions;
            shootDetail.Details = postedShootDetail.Details;
            shootDetail.DaysOfShooting = postedShootDetail.DaysOfShooting;
            shootDetail.LocationDetail = postedShootDetail.LocationDetail;
            shootDetail.DatesToBeDecidedLater = postedShootDetail.DatesToBeDecidedLater;
            shootDetail.DatesAreTemptative = postedShootDetail.DatesAreTemptative;
            try
            {
                await _shootDetailService.UpdateShootDetailAsync(shootDetail);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/productions/[id}/shootDetails/{shootDetailId}
        [Route("shootdetails/{id}")]
        [ResponseType(typeof(ShootDetail))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionShootDetail(int productionId, int id)
        {

            var shootDetail = await _shootDetailService.GetShootDetailByIdAsync(id);
            if (shootDetail == null || shootDetail.ProductionId != productionId)
            {
                return NotFound();
            }


            await _shootDetailService.DeleteShootDetailAsync(shootDetail);

            return Ok(shootDetail);

        }



        // GET: api/productions/[id}/payDetails
        [Route("paydetails")]
        [ResponseType(typeof(IEnumerable<PayDetail>))]
        public async Task<IHttpActionResult> GetProductionPayDetails(int productionId)
        {

            var productionPayDetailss =
                await _payDetailService.GetPayDetailsByProductionIdAsync(productionId);

            return Ok(productionPayDetailss);
        }


        // POST: api/productions/[id}/payDetails
        [Route("paydetails")]
        [ResponseType(typeof(PayDetail))]
        [HttpPost]
        public async Task<IHttpActionResult> PostProductionPayDetail(int productionId, PayDetailBindingModel postedPayDetail)
        {
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var payDetail = new PayDetail()
            {
                ProductionId = productionId,
                Name = postedPayDetail.Name,
                PayPackageTypeEnum = postedPayDetail.PayPackageType,
                Details = postedPayDetail.Details,
            };
            var ret =
                await _payDetailService.AddNewPayDetailAsync(payDetail);

                return Created("api/productions/" + productionId + "/extras/payDetail/" + payDetail.Id, payDetail);
        }

        // PUT: api/productions/{id}/paydetails/{payDetailId}
        [Route("paydetails/{id}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public async Task<IHttpActionResult> PutProductionPayDetail(int productionId, int id, PayDetailBindingModel postedPayDetail)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            /* if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Director)
            {

                return BadRequest("User is not an Director");

            }*/
            if (!ModelState.IsValid)
            {
               
                return BadRequest(ModelState);

            }
            var payDetail = await _payDetailService.GetPayDetailByIdAsync(id);
            if (payDetail == null || payDetail.ProductionId != productionId)
            {
                return NotFound();
            }

            //make modifications to payDetail object here from values of the PayDetailBindingModel
            payDetail.PayPackageTypeEnum = postedPayDetail.PayPackageType;
            payDetail.Details = postedPayDetail.Details;

            try
            {
                await _payDetailService.UpdatePayDetailAsync(payDetail);
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // DELETE: api/productions/[id}/payDetails/{payDetailId}
        [Route("paydetails/{id}")]
        [ResponseType(typeof(PayDetail))]
        [HttpDelete]
        public async Task<IHttpActionResult> DeleteProductionPayDetail(int productionId, int id)
        {

            var payDetail = await _payDetailService.GetPayDetailByIdAsync(id);
            if (payDetail == null || payDetail.ProductionId != productionId)
            {
                return NotFound();
            }


            await _payDetailService.DeletePayDetailAsync(payDetail);

            return Ok(payDetail);

        }

        #endregion
    }
}
