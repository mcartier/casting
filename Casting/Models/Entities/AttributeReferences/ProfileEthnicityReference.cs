﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.AttributeReferences;

using Casting.Model.Enums;

namespace Casting.Model.Entities.AttributeReferences
{
    public class ProfileEthnicityReference: EthnicityReference
    {
        public ProfileEthnicityReference()
        {
            base.AttributeReferenceTypeEnum = AttributeReferenceTypeEnum.Profile;
        }
        public int ProfileId { get; set; }
        public ActorProfile Profile { get; set; }

    }
}
