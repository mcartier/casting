import { Injectable } from '@angular/core';
import { OnDestroy, OnInit } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import * as RecordRTC from 'recordrtc/RecordRTC';
import { map, retry, catchError } from 'rxjs/operators';
import { MediaSourceState, CameraState, ServiceState, MediaSources } from './video-recorder';
//import * as tus from 'tus-js-client';
import { TusHelper} from '../utilities/tus.helper';
import { VideoBlobService } from '../services/video-blob.service';

@Injectable({
    providedIn: 'root'
})
export class VideoRecorderService implements OnDestroy, OnInit {
    constructor(private videoBlobService:VideoBlobService) {
      this._serviceIsLoadingObs.next(true);
      
    }
    private camera: MediaStream;
    private recordRTC: any;
    private recorder:any;

    private videoDevices: any = {};
    private audioDevices: any = {};
    private audioOutputDevices: any = {};

    private audioSource: any;
    private videoSource: any;
    private get videoDeviceCollection(): any[] {
        let collection: any[] = [];
        for (var key in this.videoDevices) {
            collection.push(this.videoDevices[key]);
        }
        return collection;
    }
    private get audioDeviceCollection(): any[] {
        let collection: any[] = [];
        for (var key in this.audioDevices) {
            collection.push(this.audioDevices[key]);
        }
        return collection;
    }
    private get audioOutputDeviceCollection(): any[] {
        let collection: any[] = [];
        for (var key in this.audioOutputDevices) {
            collection.push(this.audioOutputDevices[key]);
        }
        return collection;
    }
    private mediaSources: MediaSources = new MediaSources();
    private cameraState: CameraState = new CameraState();

    private _mdediaSourcesObs: Subject<MediaSources> = new Subject<MediaSources>();
    private _cameraStateObs: Subject<CameraState> = new Subject<CameraState>();
    private _serviceIsLoadingObs: Subject<boolean> = new Subject<boolean>();

    public registerForMediaSourcesState(): Observable<MediaSources> {
        return this._mdediaSourcesObs.asObservable();
    }
    public registerForCameraState(): Observable<CameraState> {
        return this._cameraStateObs.asObservable();
    }
    public registerForServiceState(): Observable<boolean> {
        return this._serviceIsLoadingObs.asObservable();
    }
    

    ngOnInit() {
       
    }

    ngOnDestroy() {
        if (this.cameraState.camera) {
            this.cameraState.camera.getAudioTracks().forEach(track => track.stop());
            this.cameraState.camera.getVideoTracks().forEach(track => track.stop());
            this.cameraState.camera = null;

        }
        if (this.recordRTC) {
          this.recordRTC.destroy();
          this.recordRTC = null;
        }
       
    }
    private checkAudioSource(deviceInfo: any) {

    }
    private checkVideoSource(deviceInfo: any) {

    }
    public requestDevicesFromSystem() {
        this.mediaSources.videoDeviceInfos = [];
        this.mediaSources.audioDeviceInfos = [];
        this.mediaSources.audioOutpurDeviceInfos = [];
        this.mediaSources.isLoading = true;
        this.mediaSources.isError = false;

        this._mdediaSourcesObs.next(this.mediaSources);
        navigator.mediaDevices.enumerateDevices().then(this.gotRequestedDevices.bind(this)).catch(this.requestedDevicesError.bind(this));
    }
    private gotRequestedDevices(deviceInfos) {
        // Handles being called several times to update labels. Preserve values.
        this.mediaSources.isError = false;
        this.mediaSources.error = null;
        for (let i = 0; i !== deviceInfos.length; ++i) {
            const deviceInfo = deviceInfos[i];
            //deviceInfo.deviceId;
            if (deviceInfo.kind === 'audioinput') {
                //deviceInfo.label
                if (!(deviceInfo.deviceId in this.audioDevices)) {
                    this.audioDevices[deviceInfo.deviceId] = deviceInfo;

                }
            } else if (deviceInfo.kind === 'audiooutput') {
                if (!(deviceInfo.deviceId in this.audioOutputDevices)) {
                    this.audioOutputDevices[deviceInfo.deviceId] = deviceInfo;
                }

            } else if (deviceInfo.kind === 'videoinput') {
                if (!(deviceInfo.deviceId in this.videoDevices)) {
                    this.videoDevices[deviceInfo.deviceId] = deviceInfo;
                }

            } else {
                console.log('Some other kind of source/device: ', deviceInfo);
            }
        }
        this.mediaSources.videoDeviceInfos = this.videoDeviceCollection;
        this.mediaSources.audioDeviceInfos = this.audioDeviceCollection;
        this.mediaSources.audioOutpurDeviceInfos = this.audioOutputDeviceCollection;
        this.mediaSources.isLoading = false;
        this._mdediaSourcesObs.next(this.mediaSources);
    }

    private requestedDevicesError(error) {
        this.mediaSources.videoDeviceInfos = [];
        this.mediaSources.audioDeviceInfos = [];
        this.mediaSources.audioOutpurDeviceInfos = [];
        this.mediaSources.isLoading = false;
        this.mediaSources.isError = true;
        this.mediaSources.error = error;
        this._mdediaSourcesObs.next(this.mediaSources);
        console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
    }

    public startCapture(videoSource: any, audioSource: any) {
        this.cameraState.isLoading = true;
        this._cameraStateObs.next(this.cameraState);

        if (this.cameraState.camera) {
            this.cameraState.camera.getTracks().forEach(track => {
                track.stop();
            });
        }
        this.audioSource = audioSource;
        this.videoSource = videoSource;


        let constraints = {
            audio: { deviceId: this.audioSource ? { exact: this.audioSource.deviceId } : undefined },
            video: { deviceId: this.videoSource ? { exact: this.videoSource.deviceId } : undefined }
        };

        navigator.mediaDevices
            .getUserMedia(constraints).then(this.startupSuccessCallback.bind(this)).catch(this.startupErrorCallback.bind(this));

    }



    private startupSuccessCallback(camera: MediaStream) {
        this.cameraState.camera = camera;
        this.cameraState.isLoading = false;
        this.cameraState.isError = false;
        this.cameraState.error = null;
        this._cameraStateObs.next(this.cameraState);

        var options = {
            mimeType: 'video/webm', // or video/webm\;codecs=h264 or video/webm\;codecs=vp9
            // audioBitsPerSecond: 128000,
            // videoBitsPerSecond: 128000,
            bitsPerSecond: 128000, // if this line is provided, skip above two,
            //  ignoreMutedMedia: false, // ---- do not ignore muted videos,
            // timeSlice: 1000 // add this line
        };
        this.recorder = new RecordRTC.RecordRTCPromisesHandler(camera, options);
      //   this.recordRTC = RecordRTC(this.cameraState.camera, options);
      //  this.recordRTC.camera = this.cameraState.camera;

    }

    private startupErrorCallback(error: any) {
      this.cameraState.camera = null;
        this.cameraState.isLoading = false;
        this.cameraState.isError = true;
        this.cameraState.error = error;
        this._cameraStateObs.next(this.cameraState);
        console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);

    }

    private getDataURLCallback(dataURL: any) {
      //  this.videoUrls.push(dataURL);
    }
    private newBlob:Blob;
    private processVideo(audioVideoWebMURL) {
       var blob: Blob = new Blob();
        this.videoBlobService.newBlobRecorded(blob);
        // this.recordRTC.getDataURL(this.getDataURLCallback.bind(this));
      //  let uploadState: BlobUploadState = new BlobUploadState(fileo, 'https://cast.com/fileso/', {}, {});
       // var sb = uploadState.obs.asObservable();
       // sb.subscribe((state: BlobUploadState) => {
       //   debugger;
       // });
      //  var tu = TusHelper.tusUploadHelper(uploadState);
        // var tu = this.tusUpload(uploadState);
      //   uploadState.sartUpload();
      //  tu.start();
        //this.recordRTC.camera.stop();
        // this.recordRTC.destroy();
        // this.recordRTC = null;

    }

    public startRecording() {

        this.recorder.startRecording(function() {

          })
            .then()
          .catch(function(error) {

            });
          // this.recordRTC.startRecording();
    }

    public stopRecording() {
      //this.recordRTC.stopRecording(this.processVideo.bind(this));
        this.recorder.stopRecording().then(function () {
          var blob: Blob = new Blob();
           this.recorder.getBlob().then(function (blob) {
             this.videoBlobService.newBlobRecorded(blob);
               }.bind(this));
         
         }.bind(this)).catch(function (error) {
            console.error('stopRecording failure', error);
        });
    }

    private download() {
        this.recordRTC.save('video.webm');
    }
}
