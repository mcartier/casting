import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { Observable, Subject, Subscription } from 'rxjs';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';

@Component({
  selector: 'app-pay-details-list',
  templateUrl: './pay-details-list.component.html',
  styleUrls: ['./pay-details-list.component.scss']
})
export class PayDetailsListComponent implements OnInit {

  constructor(
    private productionExtraService: ProductionExtraService,
    private messageDispatchService: MessageDispatchService
  ) { }

  private newPayDetailSubscription: Subscription;
  private deletePayDetailSubscription: Subscription;
  private editPayDetailSubscription: Subscription;

  @Input() payDetails: PayDetail[];

  ngOnInit() {
    this.editPayDetailSubscription = this.productionExtraService.subscribeToEditPayDetailObs()
      .subscribe(
        (detail: PayDetail) => {
        });

    this.newPayDetailSubscription = this.productionExtraService.subscribeToCreatePayDetailObs()
      .subscribe(
        (detail: PayDetail) => {
        });

    this.deletePayDetailSubscription = this.productionExtraService.subscribeToDeletePayDetailObs()
      .subscribe(
        (detail: PayDetail) => {
          if (detail) {
          }
        });

  }

  ngOnDestroy() {

    if (this.editPayDetailSubscription) {
      this.editPayDetailSubscription.unsubscribe();

    }
    if (this.newPayDetailSubscription) {
      this.newPayDetailSubscription.unsubscribe();

    }
    if (this.deletePayDetailSubscription) {
      this.deletePayDetailSubscription.unsubscribe();

    }
  }

}
