import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, NgForm } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { RxwebValidators, RxFormBuilder } from "@rxweb/reactive-form-validators";
import { MenuItem } from 'primeng/api';
import { SelectItem, SelectItemGroup } from 'primeng/api';
import { Dropdown } from 'primeng/dropdown';
import { MessageDispatchService } from '../../services/message-dispatch.service';
export interface IndustryRole {
    name: string;
    roleId: number;
}
export interface RoleGroup {
    roles: IndustryRole[];
    name: string;
}
@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    constructor(private _formBuilder: FormBuilder,
        private messageDispatchService: MessageDispatchService,
        private authenticationService: AuthenticationService) { }

   // @ViewChild('formDirective', {static: false}) private formDirective: NgForm;
    test() {
      debugger;
      var t = this.basicUserInfo;
    }
    formSteps: MenuItem[];
    activeStepIndex:number = 1;
    //email checking
    currentEmailValue: string = "";
    isSearchingEmail: boolean = false;
    emailIsAvailable = false;
    emailHasBeenChecked = false;
    roleOtherIsShowed: boolean = false;
    currentUsernameValue: string = "";
    isSearchingUsername: boolean = false;
    usernameIsAvailable = false;
    usernameHasBeenChecked = false;

    private messages: any[] = [];

    basicUserInfo: FormGroup;
    userLoginInfo: FormGroup;
    roleGroups: SelectItemGroup[] = [
        {
            label: "Talent",
            items: [
                { label: 'Actor', value: 3 },
                { label: 'Voice Actor', value: 4 },
                { label: 'Model', value: 5 },
                { label: 'Dancer', value: 6 }
            ]
        },
        {
            label: "Representation",
            items: [
                { label: 'Agent', value: 7 }
            ]
        },
        {
            label: "Production",
            items: [
                { label: 'Casting Director', value: 15 },
                { label: 'Producer', value: 16 },
                { label: 'Director', value: 17 }
            ]
        }
    ];

    ngOnInit() {
        this.formSteps = [
            { label: 'Step 1' },
            { label: 'Step 2' },
            { label: 'Step 3' }
        ];
        //basic user info
        this.basicUserInfo = this._formBuilder.group({
            email: ['fdgld@kjtrtr.com', [Validators.required, Validators.email, this.validateEmailNotTaken.bind(this)]],
            firstNames: ['', Validators.required],
            lastNames: ['', Validators.required],
            role: ['', Validators.required],
            roleOther: [''],
        }, { updateOn: 'blur' });

        //userlogin info
        this.userLoginInfo = this._formBuilder.group({
            username: ['', [Validators.required, this.validateUsernameNotTaken.bind(this)]],
            //password: ['', [Validators.required, RxwebValidators.password({ validation: { maxLength: 10, minLength: 5, digit: true, specialCharacter: true } })]],
            password: ['Brazil99?', [Validators.required, , Validators.minLength(6)]],
            passwordCompare: ['Brazil99?', [Validators.required, RxwebValidators.compare({ fieldName: 'password' })]]
        }, { updateOn: 'blur' });


        this.basicUserInfo.get('role').valueChanges.subscribe(
            (value) => {
                debugger;
                //this.basicUserInfo.get('roleOther').validator('required')
                this.basicUserInfo.get('roleOther').clearValidators();
                this.basicUserInfo.get('roleOther').reset();
                this.basicUserInfo.get('roleOther').setErrors(null);
                this.basicUserInfo.get('roleOther').markAsUntouched();
                this.basicUserInfo.get('roleOther').markAsPristine();
                if (value && parseInt(value) === 1) {
                    this.basicUserInfo.get('roleOther').setValidators([Validators.required]);
                    this.roleOtherIsShowed = true;
                } else {
                    this.roleOtherIsShowed = false;
                }
                this.basicUserInfo.get('roleOther').reset();
                this.basicUserInfo.get('roleOther').setErrors(null);
                this.basicUserInfo.get('roleOther').markAsUntouched();
                this.basicUserInfo.get('roleOther').markAsPristine();

            });

        //email availability check
        this.basicUserInfo.get('email').valueChanges.subscribe(
            (value) => {
                this.clearMessages();

                //make sure the value really changed as we force the value change call at the end of this method
                if (this.currentEmailValue !== this.basicUserInfo.get('email').value) {
                    if (!this.basicUserInfo.get('email').hasError('required') &&
                        !this.basicUserInfo.get('email').hasError('email')) {
                        this.isSearchingEmail = true;

                        this.authenticationService.checkEmailAvailability(this.basicUserInfo.get('email').value)
                            .then((response) => {
                                this.currentEmailValue = this.basicUserInfo.get('email').value;
                                this.isSearchingEmail = false;
                                this.emailHasBeenChecked = true;
                                this.emailIsAvailable = response;
                                this.basicUserInfo.get('email').updateValueAndValidity();
                                if (response === false) {
                                } else {

                                }
                            })
                            .catch((error) => {
                                this.isSearchingEmail = false;
                                this.messageDispatchService.dispatchErrorMessage(
                                    "Error verifying email:'" + this.userLoginInfo.get('email').value + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                                    "Email Verification Error");
                                this.messages.push({
                                    severity: 'error',
                                    summary: "Email Verification Error",
                                    detail: "Error verifying email:'" + this.userLoginInfo.get('email').value + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                                });

                            });
                    }
                }
            });


        //username availability check
        this.userLoginInfo.get('username').valueChanges.subscribe(
            (value) => {
                this.clearMessages();

                //make sure the value really changed as we force the value change call at the end of this method
                if (this.currentUsernameValue !== this.userLoginInfo.get('username').value) {
                    if (!this.userLoginInfo.get('username').hasError('required')) {
                        this.isSearchingUsername = true;

                        this.authenticationService.checkUsernameAvailability(this.userLoginInfo.get('username').value)
                            .then((response) => {
                                this.currentUsernameValue = this.userLoginInfo.get('username').value;
                                this.isSearchingUsername = false;
                                this.usernameHasBeenChecked = true;
                                this.usernameIsAvailable = response;
                                this.userLoginInfo.get('username').updateValueAndValidity();
                                if (response === false) {
                                } else {

                                }
                            })
                            .catch((error) => {
                                this.isSearchingUsername = false;
                                this.messageDispatchService.dispatchErrorMessage(
                                    "Error verifying username:'" + this.userLoginInfo.get('username').value + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                                    "Username Verification Error");
                                this.messages.push({
                                    severity: 'error',
                                    summary: "Username Verification Error",
                                    detail: "Error verifying username:'" + this.userLoginInfo.get('username').value + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                                });

                            });
                    }
                }
            });



    }

    validateEmailNotTaken(control: AbstractControl) {
        if (this.emailHasBeenChecked && !this.emailIsAvailable && (!this.basicUserInfo.get('email').hasError('required') &&
            !this.basicUserInfo.get('email').hasError('email'))) {
            return {
                emailTaken: true
            };
        }
        return null;
    }

    validateRoleOther(control: AbstractControl) {
     // debugger;
        if (this.basicUserInfo.get('role').value && parseInt(this.basicUserInfo.get('role').value) === 1) {
            if (!this.basicUserInfo.get('roleOther').value ||
                this.basicUserInfo.get('roleOther').value === '') {
              return { required: true };
            }
        }
        return null;
    }
    //username checking
      validateUsernameNotTaken(control: AbstractControl) {

         if (this.usernameHasBeenChecked && !this.usernameIsAvailable && !this.userLoginInfo.get('username').hasError('required')) {
            return {
                usernameTaken: true
            };
        }
        return null;
    }

    private clearMessages() {
        this.messages = [];
    }
    private moveToStep(stepIndex: number) {
        this.clearMessages();
        this.activeStepIndex = stepIndex;
    }
    private moveToRegistterStep() {
        if (this.basicUserInfo.valid) {
            this.moveToStep(2);

        }
        else {

        }
    }
    register() {
        this.clearMessages();
         if (this.basicUserInfo.invalid) {
             this.moveToStep(1);

        } else {
             if (this.userLoginInfo.valid && this.basicUserInfo.valid) {
              this.authenticationService.register(
                  this.userLoginInfo.get('username').value,
                  this.userLoginInfo.get('password').value,
                  this.userLoginInfo.get('passwordCompare').value,
                  this.basicUserInfo.get('email').value,
                  this.basicUserInfo.get('firstNames').value,
                  this.basicUserInfo.get('lastNames').value,
                  parseInt(this.basicUserInfo.get('role').value),
                  this.basicUserInfo.get('roleOther').value)
                  .then((r) => {
                      this.messageDispatchService.dispatchSuccesMessage("Accout:'" + this.userLoginInfo.get('username').value + "' was created successfully",
                          "Accout Created");

            //        stepper.next();
                  })
                  .catch((error) => {
                      this.messageDispatchService.dispatchErrorMessage(
                          "Error creating account:'" + this.userLoginInfo.get('username').value + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                          "Accout Creation Error");
                      this.messages.push({
                          severity: 'error',
                          summary: "Accout Creation Error",
                          detail: "Error creating account:'" + this.userLoginInfo.get('username').value + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error"
                      });

                 });
                //.then(stop, stop);
            }
        }
    }
        stepChanged(event, stepper) {
        this.basicUserInfo.get('roleOther').clearValidators();
        event.selectedStep.interacted = false;
        event.selectedStep.interacted = false;
        event.selectedStep.hasError = false;
        event.selectedStep.stepControl.markAsPristine();
        event.selectedStep.stepControl.markAsUntouched();
        //this.basicUserInfo.get('roleOther').setErrors(null);
        //this.basicUserInfo.get('roleOther').markAsUntouched();
       // this.basicUserInfo.get('roleOther').markAsPristine();

        //stepper.selected.interacted = false;
    }
   
}
