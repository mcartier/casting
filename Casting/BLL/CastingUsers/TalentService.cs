﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users.Talents;

namespace Casting.BLL.CastingUsers
{


   public class TalentService : BaseService, ITalentService
    {
        private ITalentAsyncRepository _talentRepository;

        public TalentService(ITalentAsyncRepository talentAsyncRepository)
        {
            _talentRepository = talentAsyncRepository;
        }

        public async Task<Talent> GetTalentByIdAsync(int talentId)
        {
            return await _talentRepository.GetTalentByIdAsync(talentId, false, false, false);
        }

        public async Task<Talent> GetTalentByIdWithAllAgentsAndMainAgentAsync(int talentId)
        {
            return await _talentRepository.GetTalentByIdAsync(talentId, false, true, true);
        }

        public async Task<Talent> GetTalentByIdWithAllAgentsReferencesAsync(int talentId)
        {
            return await _talentRepository.GetTalentByIdAsync(talentId, true, false, false);
        }

        public async Task<IEnumerable<Talent>> GetTalentsByAgentIdAsync(int agentId)
        {
            return await _talentRepository.GetTalentsByAgentIdAsync(agentId, false, false, false);
        }

        public async Task<IEnumerable<Talent>> GetTalentsByMainAgentIdAsync(int agentId)
        {
            return await _talentRepository.GetTalentsByMainAgentIdAsync(agentId, false, false, false);
        }

        public async Task<IEnumerable<Talent>> GetTalentsByDirectorIdAsync(int directorId)
        {
            return await _talentRepository.GetTalentsByAgentIdAsync(directorId, false, false, false);
        }


        public async Task<int> AddNewTalentAsync(Talent talent)
        {
            return await _talentRepository.AddNewTalentAsync(talent);
        }

        public async Task<int> UpdateTalentAsync(Talent talent)
        {
            return await _talentRepository.UpdateTalentAsync(talent);
        }

        public async Task<int> DeleteTalent(Talent talent)
        {
            return await _talentRepository.DeleteTalentAsync(talent);
        }

    }


}