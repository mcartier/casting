﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;
using Casting.BLL.CastingSessions.Interfaces;

namespace Casting.BLL.CastingSessions
{
    public class CastingSessionService : BaseService, ICastingSessionService
    {
        private ICastingSessionAsyncRepository _castingSessionRepository;

        public CastingSessionService(ICastingSessionAsyncRepository castingSessionAsyncRepository)
        {
            _castingSessionRepository = castingSessionAsyncRepository;
        }

        public async Task<CastingSession> GetCastingSessionByIdWithFoldersAndRolesAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionByIdAsync(CastingSessionParameterIncludeInQueryEnum.FodersRoles, CastingSessionFolderParameterIncludeInQueryEnum.Roles, castingSessionId);
        }
        public async Task<CastingSession> GetCastingSessionByIdWithFoldersRolesAndPersonasAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionByIdAsync(CastingSessionParameterIncludeInQueryEnum.FoldersRolesAndPersonas, CastingSessionFolderParameterIncludeInQueryEnum.RolesAndPersonas, castingSessionId);
        }
        public async Task<CastingSession> GetCastingSessionByIdWithFoldersRolesPersonasAndAssetsAsync(int castingSessionId)
        {
            return await _castingSessionRepository.GetCastingSessionByIdAsync(CastingSessionParameterIncludeInQueryEnum.FolderRolesPersonasAndAssets, CastingSessionFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets, castingSessionId);
        }

        public async Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdWithFolderRolesAndRolesAsync(int productionId)
        {
            return await _castingSessionRepository.GetCastingSessionsByProductionIdAsync(CastingSessionParameterIncludeInQueryEnum.FodersRoles, CastingSessionFolderParameterIncludeInQueryEnum.Roles, productionId);
        }

        public async Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdWithFolderRolesPersonasAndRolesPersonasAsync(int productionId)
        {
            return await _castingSessionRepository.GetCastingSessionsByProductionIdAsync(CastingSessionParameterIncludeInQueryEnum.FoldersRolesAndPersonas, CastingSessionFolderParameterIncludeInQueryEnum.RolesAndPersonas, productionId);
        }

        public async Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdWithFolderRolesPersonasAssetsAndRolesPersonasAssetsAsync(int productionId)
        {
            return await _castingSessionRepository.GetCastingSessionsByProductionIdAsync(CastingSessionParameterIncludeInQueryEnum.FolderRolesPersonasAndAssets, CastingSessionFolderParameterIncludeInQueryEnum.RolesPersonasAndAssets, productionId);
        }

        public async Task<int> AddNewCastingSessionAsync(CastingSession castingSession)
        {
            return await _castingSessionRepository.AddNewCastingSessionAsync(castingSession);
        }

        public async Task<int> UpdateCastingSessionAsync(CastingSession castingSession)
        {
            return await _castingSessionRepository.UpdateCastingSessionAsync(castingSession);
        }
        public async Task<int> DeleteCastingSession(CastingSession castingSession)
        {
            return await _castingSessionRepository.DeleteCastingSessionAsync(castingSession);
        }
    }
}
