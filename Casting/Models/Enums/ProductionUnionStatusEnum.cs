﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
public enum ProductionUnionStatusEnum
    {
        NotRelevan = 0,
        NonUnion = 1,
            Union = 3,
            Both = 7
    }
}
