import { TestBed } from '@angular/core/testing';

import { StateTransitionHandlerService } from './state-transition-handler.service';

describe('StateTransitionHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StateTransitionHandlerService = TestBed.get(StateTransitionHandlerService);
    expect(service).toBeTruthy();
  });
});
