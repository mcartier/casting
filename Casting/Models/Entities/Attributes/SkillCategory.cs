﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Profiles;

namespace Casting.Model.Entities.Attributes
{
    public class SkillCategory:Logger
    {
        public SkillCategory()
        {
            Skills = new List<Skill>();
        }
        public int Id { get; set; }
        public List<Skill> Skills { get; set; }
        public string Name { get; set; }
        public string Name2 { get; set; }

    }
}
