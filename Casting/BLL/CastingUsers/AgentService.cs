﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users.Agents;

namespace Casting.BLL.CastingUsers
{
public class AgentService : BaseService, IAgentService
{
        private IAgentAsyncRepository _agentRepository;

        public AgentService(IAgentAsyncRepository agentAsyncRepository)
        {
            _agentRepository = agentAsyncRepository;
        }

        public async Task<Agent> GetAgentByIdAsync(int agentId)
        {
            return await _agentRepository.GetAgentByIdAsync( agentId);
        }
      
        public async Task<IEnumerable<Agent>> GetAgentsByAgencyIdAsync(int agentId)
        {
            return await _agentRepository.GetAgentsByAgencyIdAsync( agentId);
        }

        public async Task<IEnumerable<Agent>> GetAgentsByAgencyOfficeIdAsync(int agentId)
        {
            return await _agentRepository.GetAgentsByAgencyOfficeIdAsync( agentId);
        }

        public async Task<int> AddNewAgentAsync(Agent agent)
        {
            return await _agentRepository.AddNewAgentAsync(agent);
        }

        public async Task<int> UpdateAgentAsync(Agent agent)
        {
            return await _agentRepository.UpdateAgentAsync(agent);
        }
        public async Task<int> DeleteAgent(Agent agent)
        {
            return await _agentRepository.DeleteAgentAsync(agent);
        }
    }
}