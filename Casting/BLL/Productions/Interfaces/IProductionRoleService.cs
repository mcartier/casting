﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{
    public interface IProductionRoleService
    {
        Task<Role> GetRoleByIdAsync(int roleId);
        Task<IEnumerable<Role>> GetRolesByProductionIdAsync(int productionId);
        Task<Role> GetCompleteRoleByIdAsync(int roleId);
        Task<IEnumerable<Role>> GetCompleteRolesByProductionIdAsync(int productionId);
        Task<int> AddNewRoleAsync(Role role);
        Task<int> UpdateRoleAsync(Role role);
        Task<int> DeleteRoleAsync(Role role);
    }
}