﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Sessions;

namespace Casting.Model.Entities.CastingCalls
{
    public class CastingCallCastingSessionReference:Logger
    {
        public int Id    { get; set; }
        public int CastingCallId { get; set; }
        public CastingCall CastingCall { get; set; }
        public int CastingSessionId { get; set; }
        public CastingSession CastingSession { get; set; }


    }
}
