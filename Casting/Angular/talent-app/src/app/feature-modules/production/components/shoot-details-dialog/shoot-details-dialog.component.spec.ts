import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShootDetailsDialogComponent } from './shoot-details-dialog.component';

describe('ShootDetailsDialogComponent', () => {
  let component: ShootDetailsDialogComponent;
  let fixture: ComponentFixture<ShootDetailsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShootDetailsDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShootDetailsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
