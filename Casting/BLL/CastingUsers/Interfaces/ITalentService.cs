﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Talents;

namespace Casting.BLL.CastingUsers.Interfaces
{
    public interface ITalentService
    {
        Task<Talent> GetTalentByIdAsync(int talentId);
        Task<Talent> GetTalentByIdWithAllAgentsAndMainAgentAsync(int talentId);
        Task<Talent> GetTalentByIdWithAllAgentsReferencesAsync(int talentId);
        Task<IEnumerable<Talent>> GetTalentsByAgentIdAsync(int agentId);
        Task<IEnumerable<Talent>> GetTalentsByMainAgentIdAsync(int agentId);
        Task<IEnumerable<Talent>> GetTalentsByDirectorIdAsync(int directorId);
        Task<int> AddNewTalentAsync(Talent talent);
        Task<int> UpdateTalentAsync(Talent talent);
        Task<int> DeleteTalent(Talent talent);
    }
}