﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Directors;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IDirectorAsyncRepository
    {
        Task<Director> GetDirectorByIdAsync(int directorId, bool withProductions);
        Task<IEnumerable<Director>> GetDirectorsAsync(bool withProductions);
        Task<int> AddNewDirectorAsync(Director director);
        Task<int> DeleteDirectorAsync(Director director);
        Task<int> UpdateDirectorAsync(Director director);
    }
}