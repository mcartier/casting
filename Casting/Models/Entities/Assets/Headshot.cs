﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets
{
    public class Image:Logger
    {
        public int Id { get; set; }
        public int OwnerId { get; set; }
        public OwnerTypeEnum OwnerType { get; set; }
        public string FileName { get; set; }


    }
}
