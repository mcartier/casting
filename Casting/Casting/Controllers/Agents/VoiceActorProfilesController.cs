﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Casting.DAL;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Users;

using Microsoft.AspNet.Identity;
using Casting.BLL.CastingUsers.Interfaces;
using Casting.BLL.Profiles.Interfaces;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Enums;
using Casting.Web.Helpers;
using Casting.Web.Models.Talent;

namespace Casting.Web.Controllers.Agents
{
    public class VoiceActorProfilesController : BaseApiController
    {
        private IProfileService _profileService;
        private readonly UserManager<ApplicationUser> _userManager;
          public VoiceActorProfilesController(UserManager<ApplicationUser> userManager, IProfileService profileService,ICastingContext castingContext) : base(castingContext)
        {
            _profileService = profileService;// castingContext;
            _userManager = userManager;
        }


        // GET: api/VoiceActorProfiles
        [ResponseType(typeof(IEnumerable<VoiceActorProfile>))]
        public async Task<IHttpActionResult> GetVoiceActorProfiles()
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profiles = await _profileService.GetCompleteVoiceActorProfilesByAgentIdAsync(user.BaseUserId);
            
            return Ok(profiles);
        }

        // GET: api/VoiceActorProfiles/5
        [ResponseType(typeof(VoiceActorProfile))]
        public async Task<IHttpActionResult> GetVoiceActorProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            var profile = await _profileService.GetCompleteVoiceActorProfileByIdAsync(id);
            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

      
      
        // PUT: api/VoiceActorProfiles/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutVoiceActorProfile(int id, VoiceActorProfileBindingModels profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentVoiceActorProfiles = await 
            var tempVoiceActorProfile = ((VoiceActorProfile)await _profileService.GetProfileByIdAsync(id));
            if (tempVoiceActorProfile == null)
            {
                return NotFound();
            }

            tempVoiceActorProfile.AgeMaximum = profile.AgeMaximum;
            tempVoiceActorProfile.AgeMinimum = profile.AgeMinimum;
            //tempVoiceActorProfile.Gender = profile.Gender;
           // tempVoiceActorProfile.LocationId = profile.Location;
           
            try
            {
                await _profileService.UpdateProfileAsync(tempVoiceActorProfile);
            }
            catch (DbUpdateConcurrencyException)
            {
               
                    throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/VoiceActorProfiles
        [ResponseType(typeof(VoiceActorProfile))]
        public async Task<IHttpActionResult> PostVoiceActorProfile(NewVoiceActorProfileBindingModels profile)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }

            var existingProfile = await _profileService.GetProfileByTalentIdAsync(profile.TalentId);
            if (existingProfile != null)
            {
                return BadRequest("profile already exists for this talent");
            }
            VoiceActorProfile newVoiceActorProfile = new VoiceActorProfile();
            newVoiceActorProfile.AgeMaximum = profile.AgeMaximum;
            newVoiceActorProfile.AgeMinimum = profile.AgeMinimum;
              //newVoiceActorProfile.Gender = profile.Gender;
            // newVoiceActorProfile.LocationId = profile.Location;
            await _profileService.AddNewProfileAsync(newVoiceActorProfile);

            return CreatedAtRoute("DefaultApi", new { id = newVoiceActorProfile.Id }, newVoiceActorProfile);
        }

        // DELETE: api/VoiceActorProfiles/5
        [ResponseType(typeof(VoiceActorProfile))]
        public async Task<IHttpActionResult> DeleteVoiceActorProfile(int id)
        {
            var user = _userManager.FindById(User.Identity.GetUserId());
            if (user == null || user.ApplicationUserType != ApplicationUserTypeEnum.Agent)
            {

                return BadRequest("User is not an Agent");

            }
            //var agentVoiceActorProfiles = await 
            var profile = ((VoiceActorProfile) await _profileService.GetProfileByIdAsync(id));
            if (profile == null)
            {
                return NotFound();
            }
          
            await _profileService.DeleteProfile(profile);

            return Ok(profile);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
               
            }
            base.Dispose(disposing);
        }

      
    }
}