﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Users
{
    public abstract class BaseCastingUser:Logger
    {

        [Key]
        public int Id { get; set; }
        public ApplicationUserTypeEnum ApplicationUserType { get; set; }
        public string FirstNames { get; set; }
        public string LastNames { get; set; }

    }
}
