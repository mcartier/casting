﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Profiles;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IProfileAsyncRepository : IAsyncRepository, IDisposable
    {


        Task<Profile> GetProfileByIdAsync(int profileId, bool withTalent);
        Task<Profile> GetProfileByIdAsync(int profileId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType);
        Task<Profile> GetProfileByTalentIdAsync(int talentId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType);
        Task<Profile> GetProfileByTalentIdAsync(int talentId, bool withTalent);

        Task<IEnumerable<Profile>> GetAllProfilesByAgentIdAsync(int agentId, bool withTalent);
        Task<IEnumerable<Profile>> GetAllProfilesByMainAgentIdAsync(int agentId, bool withTalent);
        Task<IEnumerable<Profile>> GetProfilesByAgentIdAsync(int agentId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType);
        Task<IEnumerable<Profile>> GetProfilesByMainAgentIdAsync(int agentId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType);

        Task<IEnumerable<Profile>> GetAllProfilesByDirectorIdAsync(int directorId, bool withTalent);
        Task<IEnumerable<Profile>> GetProfilesByDirectorIdAsync(int directorId, bool withTalent, bool withAssets, bool withAttributes, ProfileTypeEnum profileType);
        Task<int> AddNewProfileAsync(Profile profile);
       Task<int> DeleteProfileAsync(Profile profile);
       Task<int> UpdateProfileAsync(Profile profile);

    }
}
