﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets.References;

namespace Casting.BLL.TalentAssets.Interfaces

{
    public interface ITalentAudioReferenceService
    {
        Task<AudioReference> GetAudioReferenceByIdAsync(int audioId);
        Task<IEnumerable<AudioReference>> GetProfileAudioReferencesForParentAsync(int profileId, bool withAssets);
        Task<IEnumerable<AudioReference>> GetPersonaAudioReferencesForParentAsync(int personaId, bool withAssets);
        Task<IEnumerable<AudioReference>> GetBasketAudioReferencesForParentAsync(int basketId, bool withAssets);

        Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            AudioReference assetReference);

        Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            int parentId, AudioReference assetReference);

        Task<int> AddNewProfileAudioAssetReferenceAsync(bool orderAtTheTop, ProfileAudioReference assetReference);
        Task<int> AddNewPersonaAudioAssetReferenceAsync(bool orderAtTheTop, PersonaAudioReference assetReference);
        Task<int> AddNewBasketAudioAssetReferenceAsync(bool orderAtTheTop, BasketAudioReference assetReference);
        Task<int> DeleteAudioAssetReferenceAsync(int parentId, AudioReference assetReference);
        Task<int> DeleteProfileAudioAssetReferenceAsync(ProfileAudioReference assetReference);
        Task<int> DeleteBasketAudioAssetReferenceAsync(BasketAudioReference assetReference);
        Task<int> DeletePersonaAudioAssetReferenceAsync(PersonaAudioReference assetReference);

        Task<int> UpdateAudioAssetReferenceAsync(int parentId,
            AudioReference assetReference);

        Task<int> UpdateProfileAudioAssetReferenceAsync(ProfileAudioReference assetReference);
        Task<int> UpdateBasketAudioAssetReferenceAsync(BasketAudioReference assetReference);
        Task<int> UpdatePersonaAudioAssetReferenceAsync(PersonaAudioReference assetReference);

        Task<int> ReOrderAudioAssetAsync(int parentId,
            int desiredOrderIndex, AudioReference assetReference);

        Task<int> ReOrderProfileAudioAssetAsync(int desiredOrderIndex, ProfileAudioReference assetReference);
        Task<int> ReOrderBasketAudioAssetAsync(int desiredOrderIndex, BasketAudioReference assetReference);
        Task<int> ReOrderPersonaAudioAssetAsync(int desiredOrderIndex, PersonaAudioReference assetReference);
    }
}