import { Injectable } from '@angular/core';
import { OnDestroy, OnInit } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { StudioTalentBlob, CreatedBlob } from '../models/blobs';
import { UUID } from 'angular2-uuid';
//import * as tus from 'tus-js-client';
import { TusHelper } from '../utilities/tus.helper';
import { FileUploadService } from './file-upload.service';
@Injectable({
    providedIn: 'root'
})
export class VideoBlobService {

    constructor(private fileUploadService:FileUploadService) {
        if (localStorage.getItem('currentUser')) {
            let currentUser = JSON.parse(localStorage.getItem('currentUser'));
            if (currentUser && currentUser.access_token) {
                this.headers =
                    {
                        Authorization: `Bearer ${currentUser.access_token.replace(" ", "")}`
                    }

            }
        }
    }
    private _newBlob:Blob;
    private _blobs: StudioTalentBlob[] = [];

    private headers: any;
    private videoUrls: any[] = [];
    private _newBlogRecordingObserverCallback = null;
    private _createdBlobObs: Subject<StudioTalentBlob> = new Subject<StudioTalentBlob>();
    private _blobsObs: BehaviorSubject<StudioTalentBlob[]> = new BehaviorSubject<StudioTalentBlob[]>([]);
    public readonly createdblobObs: Observable<StudioTalentBlob> = this._createdBlobObs.asObservable();
    public readonly blobsObs: Observable<StudioTalentBlob[]> = this._blobsObs.asObservable();
    private norifyNewBlogRecordingObserver(newBlob: StudioTalentBlob) {
      if (this._newBlogRecordingObserverCallback) {
        this._newBlogRecordingObserverCallback(newBlob)
      }
    }
    public registerNewBlogRecordingObserver(observer) {
      this._newBlogRecordingObserverCallback = observer;

    }
    public unRegisterNewBlogRecordingObserver() {
      this._newBlogRecordingObserverCallback = null;

    }
    public subscribeToCreatedBlobObs(): Observable<StudioTalentBlob> {
      return this.createdblobObs;
    }
    public subscribeToBlobsObs(): Observable<StudioTalentBlob[]> {
        return this.blobsObs;
    }
     public newBlobRecorded(newBlob: Blob): void {
        if (newBlob) {
          this._newBlob = newBlob;
          let newCreatedBlob: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
            this.norifyNewBlogRecordingObserver(newCreatedBlob);
            this._createdBlobObs.next(newCreatedBlob);
      }
    }
    public addNewCreatedBlob(newBlob: StudioTalentBlob): void {
        
      //let newCreatedBlob: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
     // let newCreatedBlob2: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
      //let newCreatedBlob3: StudioTalentBlob = new StudioTalentBlob(UUID.UUID(), newBlob, 1, 1, 1);
        if (!this._blobs.some(s => s.id == newBlob.id)) {
            let blobsCol: StudioTalentBlob[] = [newBlob];
           this._blobs.push(newBlob);
          // this._blobs = this._blobs;
         
        } else {
            this._blobs[this._blobs.findIndex(s => s.id == newBlob.id)] = newBlob;

        }
        this._blobsObs.next(this._blobs);

         // this.fileUploadService.uploadFiles(blobsCol);

        //this._blobsObs.next(this._blobs);
    }
    private _blobObsArray = {};
    public SubscribeToBlobObs(blobId: number): Observable<StudioTalentBlob> {

        return this._getOrCreateBlobObsInArray(blobId).asObservable();
    }

    private makeString(value: number): string {
        return value.toString();
    };
    private _getOrCreateBlobObsInArray(blobId: number): BehaviorSubject<StudioTalentBlob> {
        var blobId$ = this.makeString(blobId);
        if (!(blobId$ in this._blobObsArray)) {
           // this._blobObsArray[blobId$] = new BehaviorSubject<CreatedBlob>(new Blob());
        }
        return this._blobObsArray[blobId$];
    }
    private _removeBlobObsFromArray(blobId: number): void {
        var blobId$ = this.makeString(blobId);
        if (blobId$ in this._blobObsArray) {
            delete this._blobObsArray[blobId$];
        }
    }

    private _isBlobIsBlobArray(blobId: number): boolean {
        return this._blobs.some(s => s.id == blobId);
    }

    private _getBlobs(): Observable<StudioTalentBlob[]> {

        if (this._blobs !== null && this._blobs.length > 0) {
            return of(this._blobs)
                .pipe(
                map((res: StudioTalentBlob[]) => {
                        return res
                    })
                );
        } else {

        }

    }

    private _getBlob(blobId: number): Observable<StudioTalentBlob> {

        if (this._isBlobIsBlobArray(blobId)) {
            return of(this._blobs.find(s => s.id == blobId))
                .pipe(
                map((res: StudioTalentBlob) => {
                        return res;
                    })
                );
        }
        else {

        }
    };
    public getBlobs(): Observable<StudioTalentBlob[]> {

        return this._getBlobs()
            .pipe(
            map((res: StudioTalentBlob[]) => {
                    this._blobsObs.next(this._blobs);

                    return res
                })
            );
    };
    public getBlob(blobId: number): Observable<StudioTalentBlob> {

        return this._getBlob(blobId)
            .pipe(
            map((res: StudioTalentBlob) => {
                    this._getOrCreateBlobObsInArray(blobId).next(res);
                    return res
                })
            );
    };

    public addBlob(blob: StudioTalentBlob): Observable<StudioTalentBlob[]> {
        //do anything with the returned data here...
        if (!this._isBlobIsBlobArray(blob.id)) {
            this._blobs.push(blob);
            this._blobsObs.next(this._blobs);
        };

        //
        +this._getOrCreateBlobObsInArray(blob.id).next(blob);
        this._blobsObs.next(this._blobs);

        return this._blobsObs;

    };



    public saveBlob(blob: StudioTalentBlob): any {


        //do anything with the returned data here...
        if (!this._isBlobIsBlobArray(blob.id)) {
            this._blobs.push(blob);
        }
        else {
            this._blobs[this._blobs.findIndex(s => s.id == blob.id)] = blob;
        }
        this._blobsObs.next(this._blobs);
        this._getOrCreateBlobObsInArray(blob.id).next(blob);

        return blob;

    };

    public deleteBlob(blobId: number): any {
        //do anything with the returned data here...
        if (this._blobs.some(s => s.id == blobId)) {
          this._blobs.splice(this._blobs.findIndex(s => s.id == blobId), 1);
          //this._getOrCreateBlobObsInArray(blobId).complete();
          /// this._removeBlobObsFromArray(blobId);
          this._blobsObs.next(this._blobs);
        }

        return true

    };
    private _handleSuccessError(message: string, dispatchAlert: boolean) {

    }
    //  private _handleError(error: HttpErrorResponse, message: string, dispatchAlert: boolean) {

    //      return throwError(error);
    //  }


}
