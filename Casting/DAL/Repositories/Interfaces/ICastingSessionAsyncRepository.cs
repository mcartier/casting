﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Model.Entities.Sessions;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface ICastingSessionAsyncRepository : IAsyncRepository, IDisposable
    {
        #region CastingSessions

        Task<CastingSession> GetCastingSessionByIdAsync(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery, int castingSessionId);
        Task<int> AddNewCastingSessionAsync(CastingSession castingSession);
        Task<int> DeleteCastingSessionAsync(CastingSession castingSession);
        Task<int> UpdateCastingSessionAsync(CastingSession castingSession);
        Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdAsync(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery, int productionId);
        Task<IEnumerable<CastingSession>> GetCastingSessionsAsync(CastingSessionParameterIncludeInQueryEnum folderParameterIncludeInQuery, CastingSessionFolderParameterIncludeInQueryEnum parameterIncludeInQuery, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending);



        #endregion


        #region Roles

        Task<CastingSessionRole> GetCastingSessionRoleByIdAsync(CastingSessionRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionRoleId);
        Task<int> AddNewCastingSessionRoleAsync(bool orderAtTheTop, CastingSessionRole castingSessionRole);
        Task<int> DeleteCastingSessionRoleAsync(CastingSessionRole castingSessionRole);
        Task<int> UpdateCastingSessionRoleAsync(CastingSessionRole castingSessionRole);
        Task<IEnumerable<CastingSessionRole>> GetCastingSessionRolesByCastingSessionIdAsync(CastingSessionRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionId);
        Task<int> ReOrderCastingSessionRoleAsync(int desiredOrderIndex, CastingSessionRole castingSessionRole);



        #endregion


        #region Folders

        Task<CastingSessionFolder> GetCastingSessionFolderByIdAsync(CastingSessionFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId);
        Task<int> AddNewCastingSessionFolderAsync(bool orderAtTheTop, CastingSessionFolder castingSessionFolder);
        Task<int> DeleteCastingSessionFolderAsync(CastingSessionFolder castingSessionFolder);
        Task<int> UpdateCastingSessionFolderAsync(CastingSessionFolder castingSessionFolder);
        Task<IEnumerable<CastingSessionFolder>> GetCastingSessionFoldersByCastingSessionIdAsync(CastingSessionFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionId);
        Task<int> ReOrderCastingSessionFolderAsync(int desiredOrderIndex, CastingSessionFolder castingSessionFolder);



        #endregion



        #region Roles

        Task<CastingSessionFolderRole> GetCastingSessionFolderRoleByIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderRoleId);
        Task<int> AddNewCastingSessionFolderRoleAsync(bool orderAtTheTop, CastingSessionFolderRole castingSessionFolderRole);
        Task<int> DeleteCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole);
        Task<int> UpdateCastingSessionFolderRoleAsync(CastingSessionFolderRole castingSessionFolderRole);
        Task<IEnumerable<CastingSessionFolderRole>> GetCastingSessionFolderRolesByCastingSessionFolderIdAsync(CastingSessionFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId);
        Task<int> ReOrderCastingSessionFolderRoleAsync(int desiredOrderIndex, CastingSessionFolderRole castingSessionFolderRole);



        #endregion

    }
}
