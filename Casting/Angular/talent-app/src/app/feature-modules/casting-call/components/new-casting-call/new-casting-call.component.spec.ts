import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCastingCallComponent } from './new-casting-call.component';

describe('NewCastingCallComponent', () => {
  let component: NewCastingCallComponent;
  let fixture: ComponentFixture<NewCastingCallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCastingCallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCastingCallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
