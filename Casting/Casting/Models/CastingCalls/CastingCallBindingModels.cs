﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Casting.Model.Entities.Metadata;
using Casting.Model.Enums;

namespace Casting.Web.Models.CastingCalls
{
    public class NewCastingCallBindingModel
    {
        public int ProductionId { get; set; }
        public CastingCallType CastingCallType { get; set; }
        public CastingCallPrivacy CastingCallPrivacy { get; set; }
        // public int ViewTypeId { get; set; }
        public CastingCallViewType CastingCallViewType { get; set; }
        public int ViewTypeId { get; set; }
        public CastingCallTalentSourceLocationEnum TalentSourceLocation { get; set; }
        public bool AcceptingSubmissions { get; set; }

        public string Instructions { get; set; }

        public DateTimeOffset Expires { get; set; }
        public string LocationType { get; set; }

        public DateTimeOffset PostedDate { get; set; }

        public bool RequiresHeadshot { get; set; }
        public int HeadshotMaxCount { get; set; }
        public bool RequiresVideo { get; set; }
        public int VideoMaxCount { get; set; }
        public bool RequiresAudio { get; set; }
        public int AudioMaxCount { get; set; }
        public bool RequiresCoverLetter { get; set; }
        public ICollection<int> Unions { get; set; }


    }
    public class UpdateCastingCallBindingModel : NewCastingCallBindingModel
    {
        public int Id { get; set; }
 
    }
    public class NewCastingCallFolderBindingModel
    {
       
        public string Name { get; set; }
        public int CastingCallId { get; set; }

    }
    public class UpdateCastingCallFolderBindingModel : NewCastingCallFolderBindingModel
    {
        public int Id { get; set; }

    }
    public class NewCastingCallRoleBindingModel
    {

        public int CastingCallId { get; set; }
        public int RoleId { get; set; }
        public bool RequiresHeadshot { get; set; }
        public int HeadshotMaxCount { get; set; }
        public bool RequiresVideo { get; set; }
        public int VideoMaxCount { get; set; }
        public bool RequiresAudio { get; set; }
        public int AudioMaxCount { get; set; }
        public bool RequiresCoverLetter { get; set; }
        public ICollection<int> Unions { get; set; }


    }
    public class UpdateCastingCallRoleBindingModel
    {
        public bool RequiresHeadshot { get; set; }
        public int HeadshotMaxCount { get; set; }
        public bool RequiresVideo { get; set; }
        public int VideoMaxCount { get; set; }
        public bool RequiresAudio { get; set; }
        public int AudioMaxCount { get; set; }
        public bool RequiresCoverLetter { get; set; }
        public ICollection<int> Unions { get; set; }


    }
    public class NewCastingCallFolderRoleBindingModel
    {
       
        public string Name { get; set; }
        public int CastingCallFolderId { get; set; }
        public int CastingCallRoleId { get; set; }
        public int RoleId { get; set; }

    }
    public class UpdateCastingCallFolderRoleBindingModel : NewCastingCallFolderRoleBindingModel
    {
        public int Id { get; set; }

    }

    public class NewCastingCallLocationReference
    {
        public int CastingCallId { get; set; }
        public int LocationId { get; set; }
    }
}