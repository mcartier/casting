﻿using Casting.Model.Entities.Personas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Assets.References
{
    public class PersonaVideoReference: VideoReference
    {
        public PersonaVideoReference()
        {
            base.AssetReferenceTypeEnum = AssetReferenceTypeEnum.Persona;
        }
        public int PersonaId { get; set; }
        public Persona Persona { get; set; }


    }
}
