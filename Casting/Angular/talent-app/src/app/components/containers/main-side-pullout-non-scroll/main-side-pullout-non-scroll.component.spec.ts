import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainSidePulloutNonScrollComponent } from './main-side-pullout-non-scroll.component';

describe('MainSidePulloutNonScrollComponent', () => {
  let component: MainSidePulloutNonScrollComponent;
  let fixture: ComponentFixture<MainSidePulloutNonScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainSidePulloutNonScrollComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainSidePulloutNonScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
