﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
public class CastingSessionRoleSelectionPersonaReference: PersonaReference
    {
        public CastingSessionRoleSelectionPersonaReference()
        {
            base.PersonaReferenceType = PersonaReferenceTypeEnum.CastingSessionRoleSelection;
        }

        public int CastingSessionRoleId { get; set; }
        public CastingSessionRole CastingSessionRole { get; set; }
    }
}
