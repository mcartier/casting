﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Sessions;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
   public class CastingSessionFolderRoleSelectionPersonaReference: PersonaReference
    {
        public CastingSessionFolderRoleSelectionPersonaReference()
        {
            base.PersonaReferenceType = PersonaReferenceTypeEnum.CastingSessionFolderRoleSelection;
        }
        public int CastingSessionFolderRoleId { get; set; }
        public CastingSessionFolderRole CastingSessionFolderRole { get; set; }

    }
}
