﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using System.Data.Entity.Infrastructure;
using Casting.DAL.Repositories;
using Casting.Model.Entities.Users.Talents;


namespace Casting.DAL.Tests
{
    public class TestRepository 
    {
        ICastingContext _CastingContext;
        public TestRepository(ICastingContext castingContext)
        {
            _CastingContext = castingContext;
        }
        public async Task<int> CreateTalentAsync(Talent talent)
        {
            _CastingContext.Talents.Add(talent);
            return await _CastingContext.SaveChangesAsync();
        }

        public async Task<int> DeleteTalentAsync(Talent talent)
        {
            _CastingContext.Talents.Remove(talent);
            return await _CastingContext.SaveChangesAsync();
        }

        public async Task<Talent> FindTalentAsync(int id)
        {
            return await _CastingContext.Talents.FindAsync(id);
        }

        public IQueryable<Talent> Talents()
        {
            return _CastingContext.Talents;
        }

        public async Task<int> UpdateTalentAsync(Talent talent)
        {
            try
            {
                _CastingContext.SetModified(talent);
                return await _CastingContext.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public Task<IQueryable<Talent>> TalentsAsync()
        {
            throw new NotImplementedException();
        }
        public void SetEntityState(Talent talent, EntityState state)
        {
            _CastingContext.SetState(talent, state);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~TestRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
