﻿using Casting.BLL.Productions;
using Casting.BLL.Productions.Interfaces;
using Casting.Model.Entities.Users;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Casting.Model.Entities.Productions;
using System.Web.Http.Description;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Web.Models.Production;
using System.Data.Entity.Infrastructure;
using Casting.BLL.Metadata.Interfaces;
using Casting.BLL.Location.Interfaces;
using Casting.Model.Entities.Locations;
using Casting.DAL;

namespace Casting.Web.Controllers.Directors
{
    [RoutePrefix("api/locations")]
    public class LocationsController : BaseApiController
    {
        private ILocationsService _locationsService;
     
        public LocationsController(ILocationsService locationsService, ICastingContext castingContext) : base(castingContext)
        {
            _locationsService = locationsService;
        }

        // GET: api/locations/countries
        [System.Web.Http.ActionName("countries")]
        [Route("countries")]
        [ResponseType(typeof(IEnumerable<Country>))]
        public async Task<IHttpActionResult> GetCountries()
        {
            var countries =
                await _locationsService.GetCountriesAsync();

            return Ok(countries);
        }


        // GET: api/locations/country
        [System.Web.Http.ActionName("country")]
        [Route("country/{countryId}")]
        [ResponseType(typeof(Country))]
        public async Task<IHttpActionResult> GetCountry(int countryId)
        {
            var country =
                await _locationsService.GetCountrieByIdAsync(countryId);

            return Ok(country);
        }
        // GET: api/locations/regions/1
        [System.Web.Http.ActionName("regions")]
        [Route("regions/{countryId}")]
        [ResponseType(typeof(IEnumerable<Region>))]
        public async Task<IHttpActionResult> GetRegions(int countryId)
        {
            var regions =
                await _locationsService.GetRegionsByCountryIdAsync(countryId);

            return Ok(regions);
        }
        // GET: api/locations/region
        [System.Web.Http.ActionName("region")]
        [Route("region/{regionId}")]
        [ResponseType(typeof(Region))]
        public async Task<IHttpActionResult> GetRegion(int regionId)
        {
            var region =
                await _locationsService.GetCountrieByIdAsync(regionId);

            return Ok(region);
        }


        // GET: api/locations/regioncities/1
        [System.Web.Http.ActionName("regioncities")]
        [Route("regioncities/{regionId}")]
        [ResponseType(typeof(IEnumerable<Region>))]
        public async Task<IHttpActionResult> GetCitiesByRegion(int regionId)
        {
            var cities =
                await _locationsService.GetCitiesByCountryIdAsync(regionId);

            return Ok(cities);
        }
        // GET: api/locations/countrycities/1
        [System.Web.Http.ActionName("countrycities")]
        [Route("countrycities/{countryId}")]
        [ResponseType(typeof(IEnumerable<Region>))]
        public async Task<IHttpActionResult> GetCitiesByCountry(int countryId)
        {
            var cities =
                await _locationsService.GetCitiesByCountryIdAsync(countryId);

            return Ok(cities);
        }

        // GET: api/locations/city
        [System.Web.Http.ActionName("city")]
        [Route("city/{cityId}")]
        [ResponseType(typeof(City))]
        public async Task<IHttpActionResult> GetCity(int cityId)
        {
            var city =
                await _locationsService.GetCountrieByIdAsync(cityId);

            return Ok(city);
        }


    }
}
