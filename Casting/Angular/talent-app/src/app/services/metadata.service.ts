import { Injectable, Inject } from '@angular/core';
import { Observable, BehaviorSubject,of } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { HttpHelpers } from '../utilities/HttpHelpers';
import { WINDOW } from './browser.window.provider';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../models/metadata';
@Injectable({
  providedIn: 'root'
})
export class MetadataService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) 
      {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }
    //private _metadataObs: BehaviorSubject<MetadataModel>;// = new BehaviorSubject(new MetadataModel());
    private _metadata:MetadataModel;
    //public readonly metadataObs: Observable<MetadataModel> = this._metadataObs.asObservable();

      private host: string;
      private hostName: string;
      private protocol: string;
      private port: string;
      private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/talents/";
        return "/api/metadata/";
      }
    public getMetadata(): Promise<MetadataModel> {
        let prom: Promise<MetadataModel>;
        if (this._metadata) {
          prom = new Promise((resolve, reject) => {
            resolve(this._metadata);

          });
        } else {
          let controllerQuery = this.getBaseURL() + "metadata";
            prom = new Promise((resolve, reject) => {
                this.getAction<MetadataModel>(controllerQuery)
                    .toPromise()
                    .then(
                      res => {
                          this._metadata = res;
                          resolve(this._metadata);
                    })
                .catch(error => reject(error))
         
          })
        }
          
        
        return prom;
      }
    
}
