﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Enums;
using Casting.BLL.TalentAssets.Interfaces;

namespace Casting.BLL.TalentAssets
{
    public class TalentAudioReferenceService : BaseService, ITalentAudioReferenceService
    {
        private ITalentAssetAsyncRepository _assetRepository;

        public TalentAudioReferenceService(ITalentAssetAsyncRepository assetRepository)
        {
            _assetRepository = assetRepository;
        }

        public async Task<AudioReference> GetAudioReferenceByIdAsync(int audioId)
        {
            var audio = await _assetRepository.GetAudioAssetReferenceByIdAsync(true, audioId);
            return audio;
        }
        public async Task<IEnumerable<AudioReference>> GetProfileAudioReferencesForParentAsync(int profileId, bool withAssets)
        {
            var audioReferences = await _assetRepository.GetAudioReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Profile, profileId);
            return audioReferences;
        }
        public async Task<IEnumerable<AudioReference>> GetPersonaAudioReferencesForParentAsync(int personaId, bool withAssets)
        {
            var audioReferences = await _assetRepository.GetAudioReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Persona, personaId);
            return audioReferences;
        }
        public async Task<IEnumerable<AudioReference>> GetBasketAudioReferencesForParentAsync(int basketId, bool withAssets)
        {
            var audioReferences = await _assetRepository.GetAudioReferencesForParentAsync(withAssets, AssetReferenceTypeEnum.Basket, basketId);
            return audioReferences;
        }
          public async Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            AudioReference assetReference)
        {
            return await _assetRepository.AddNewAudioAssetReferenceAsync(orderAtTheTop,assetReference);
        }

        public async Task<int> AddNewAudioAssetReferenceAsync(bool orderAtTheTop,
            int parentId, AudioReference assetReference)
        {
            return await _assetRepository.AddNewAudioAssetReferenceAsync(orderAtTheTop, parentId, assetReference);
        }

        public async Task<int> AddNewProfileAudioAssetReferenceAsync(bool orderAtTheTop, ProfileAudioReference assetReference)
        {
            return await AddNewAudioAssetReferenceAsync(orderAtTheTop, assetReference.ProfileId, assetReference);
        }
        public async Task<int> AddNewPersonaAudioAssetReferenceAsync(bool orderAtTheTop, PersonaAudioReference assetReference)
        {
            return await AddNewAudioAssetReferenceAsync(orderAtTheTop, assetReference.PersonaId, assetReference);
        }
        public async Task<int> AddNewBasketAudioAssetReferenceAsync(bool orderAtTheTop, BasketAudioReference assetReference)
        {
            return await AddNewAudioAssetReferenceAsync(orderAtTheTop, assetReference.BasketId, assetReference);
        }

        public async Task<int> DeleteAudioAssetReferenceAsync(int parentId, AudioReference assetReference)
        {


            return await _assetRepository.DeleteAudioAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> DeleteProfileAudioAssetReferenceAsync(ProfileAudioReference assetReference)
        {


            return await DeleteAudioAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> DeleteBasketAudioAssetReferenceAsync(BasketAudioReference assetReference)
        {


            return await DeleteAudioAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> DeletePersonaAudioAssetReferenceAsync(PersonaAudioReference assetReference)
        {


            return await DeleteAudioAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }
        public async Task<int> UpdateAudioAssetReferenceAsync(int parentId,
            AudioReference assetReference)
        {
            return await _assetRepository.UpdateAudioAssetReferenceAsync(parentId, assetReference);
        }
        public async Task<int> UpdateProfileAudioAssetReferenceAsync(ProfileAudioReference assetReference)
        {
            return await UpdateAudioAssetReferenceAsync(assetReference.ProfileId, assetReference);
        }
        public async Task<int> UpdateBasketAudioAssetReferenceAsync(BasketAudioReference assetReference)
        {
            return await UpdateAudioAssetReferenceAsync(assetReference.BasketId, assetReference);
        }
        public async Task<int> UpdatePersonaAudioAssetReferenceAsync(PersonaAudioReference assetReference)
        {
            return await UpdateAudioAssetReferenceAsync(assetReference.PersonaId, assetReference);
        }

        public async Task<int> ReOrderAudioAssetAsync(int parentId,
            int desiredOrderIndex, AudioReference assetReference)


        {
            return await _assetRepository.ReOrderAudioAssetAsync(parentId, desiredOrderIndex, assetReference);
        }

        public async Task<int> ReOrderProfileAudioAssetAsync(int desiredOrderIndex, ProfileAudioReference assetReference)


        {
            return await ReOrderAudioAssetAsync(assetReference.ProfileId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderBasketAudioAssetAsync(int desiredOrderIndex, BasketAudioReference assetReference)


        {
            return await ReOrderAudioAssetAsync(assetReference.BasketId, desiredOrderIndex, assetReference);
        }
        public async Task<int> ReOrderPersonaAudioAssetAsync(int desiredOrderIndex, PersonaAudioReference assetReference)


        {
            return await ReOrderAudioAssetAsync(assetReference.PersonaId, desiredOrderIndex, assetReference);
        }
    }
}
