﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface IPersonaAsyncRepository : IAsyncRepository, IDisposable
    {

        #region Personas
        Task<Persona> GetPersonaByIdAsync(bool withTalent, bool withAssets, bool withAttributes, bool withAgents, int personaId);
        Task<int> AddNewPersonaAsync(Persona persona);
        Task<int> DeletePersonaAsync(Persona persona);
        Task<int> UpdatePersonaAsync(Persona persona);

        Task<IEnumerable<Persona>> GetPersonasByCastingCallFolderRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId);

        Task<IEnumerable<Persona>> GetPersonasByCastingCallRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId);

        Task<IEnumerable<Persona>> GetPersonasBySessionFolderRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId);

        Task<IEnumerable<Persona>> GetPersonasBySessionRoleIdAsync(bool withTalent, bool withAssets,
            bool withAttributes, bool withAgents, int roleId);

        #endregion

        #region PersonaReferences

        

        
        Task<int> DeletePersonaReferenceAsync(int parentId, PersonaReference personaReference);

        Task<int> UpdatePersonaReferenceAsync(int parentId,
            PersonaReference personaReference);

        Task<int> ReOrderPersonaReferenceAsync(int parentId,
            int desiredOrderIndex, PersonaReference personaReference);

        Task<int> AddNewPersonaReferenceAsync(bool orderAtTheTop, int parentId,
            PersonaReference personaReference);

        Task<int> AddNewPersonaReferenceAsync(bool orderAtTheTop,
            PersonaReference personaReference);

        Task<PersonaReference> GetPersonaReferenceByIdAsync(bool withPersona, int referenceId);
        IQueryable<PersonaReference> GetPersonaReferencesQueryableForParent(bool withPersona, PersonaReferenceTypeEnum personaReferenceType, int parentId);
        Task<int> GetReferenceCountPerPersonaAsync(int personaId);
        #endregion
    }
}
