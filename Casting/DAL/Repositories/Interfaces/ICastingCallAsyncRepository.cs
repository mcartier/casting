﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Enums;

namespace Casting.DAL.Repositories.Interfaces
{
    public interface ICastingCallAsyncRepository : IAsyncRepository, IDisposable
    {
        #region CastingCalls

        Task<CastingCall> GetCastingCallByIdAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery, int castingCallId);
        Task<int> AddNewCastingCallAsync(CastingCall castingCall);
        Task<int> DeleteCastingCallAsync(CastingCall castingCall);
        Task<int> UpdateCastingCallAsync(CastingCall castingCall);
        Task<IEnumerable<CastingCall>> GetCastingCallsByProductionIdAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery, int productionId);
        Task<IEnumerable<CastingCall>> GetCastingCallsAsync(CastingCallParameterIncludeInQueryEnum parameterIncludeInQuery, int pageIndex = 0, int pageSize = 12, OrderByValuesEnum orderBy = OrderByValuesEnum.CreatedDateDescending);



        #endregion


        #region Roles

        Task<CastingCallRole> GetCastingCallRoleByIdAsync(CastingCallRoleParameterIncludeInQueryEnum includeInQuery, int castingCallRoleId);
        Task<int> AddNewCastingCallRoleAsync(bool orderAtTheTop, CastingCallRole castingCallRole);
        Task<int> DeleteCastingCallRoleAsync(CastingCallRole castingCallRole);
        Task<int> UpdateCastingCallRoleAsync(CastingCallRole castingCallRole);
        Task<IEnumerable<CastingCallRole>> GetCastingCallRolesByCastingCallIdAsync(CastingCallRoleParameterIncludeInQueryEnum includeInQuery, int castingCallId);
        Task<int> ReOrderCastingCallRoleAsync(int desiredOrderIndex, CastingCallRole castingCallRole);



        #endregion

        #region Folders

        Task<CastingCallFolder> GetCastingCallFolderByIdAsync(CastingCallFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId);
        Task<int> AddNewCastingCallFolderAsync(bool orderAtTheTop, CastingCallFolder castingSessionFolder);
        Task<int> DeleteCastingCallFolderAsync(CastingCallFolder castingSessionFolder);
        Task<int> UpdateCastingCallFolderAsync(CastingCallFolder castingSessionFolder);
        Task<IEnumerable<CastingCallFolder>> GetCastingCallFoldersByCastingCallIdAsync(CastingCallFolderParameterIncludeInQueryEnum includeInQuery, int castingSessionId);
        Task<int> ReOrderCastingCallFolderAsync(int desiredOrderIndex, CastingCallFolder castingSessionFolder);



        #endregion



        #region Roles

        Task<CastingCallFolderRole> GetCastingCallFolderRoleByIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderRoleId);
        Task<int> AddNewCastingCallFolderRoleAsync(bool orderAtTheTop, CastingCallFolderRole castingSessionFolderRole);
        Task<int> DeleteCastingCallFolderRoleAsync(CastingCallFolderRole castingSessionFolderRole);
        Task<int> UpdateCastingCallFolderRoleAsync(CastingCallFolderRole castingSessionFolderRole);
        Task<IEnumerable<CastingCallFolderRole>> GetCastingCallFolderRolesByCastingCallFolderIdAsync(CastingCallFolderRoleParameterIncludeInQueryEnum includeInQuery, int castingSessionFolderId);
        Task<int> ReOrderCastingCallFolderRoleAsync(int desiredOrderIndex, CastingCallFolderRole castingSessionFolderRole);



        #endregion

        #region TalentLocations

        Task<IEnumerable<CastingCallCityReference>> GetCastingCallTalentCityReferencesByCastingCallId(int castingCallId);
        Task<CastingCallCityReference> GetCastingCallTalentCityReferenceById(int castingCallLocationReferenceId);
        Task<IEnumerable<CastingCallRegionReference>> GetCastingCallTalentRegionReferencesByCastingCallId(int castingCallId);
        Task<CastingCallRegionReference> GetCastingCallTalentRegionReferenceById(int castingCallLocationReferenceId);
        Task<IEnumerable<CastingCallCountryReference>> GetCastingCallTalentCountryReferencesByCastingCallId(int castingCallId);
        Task<CastingCallCountryReference> GetCastingCallTalentCountryReferenceById(int castingCallLocationReferenceId);
        Task<int> AddNewCityTalentLocationAsync(CastingCallCityReference castingCallCityReference);
        Task<int> DeleteCityTalentReferenceAsync(CastingCallCityReference castingCallCityReference);
        Task<int> AddNewRegionTalentLocationAsync(CastingCallRegionReference castingCallRegionReference);
        Task<int> DeleteRegionTalentReferenceAsync(CastingCallRegionReference castingCallRegionReference);
        Task<int> AddNewCountryTalentLocationAsync(CastingCallCountryReference castingCallCountryReference);
        Task<int> DeleteCountryTalentReferenceAsync(CastingCallCountryReference castingCallCountryReference);
        #endregion
    }
}
