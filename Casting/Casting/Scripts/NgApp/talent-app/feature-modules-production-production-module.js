(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["feature-modules-production-production-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/add-role/add-role.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/add-role/add-role.component.html ***!
  \******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p>\r\n  add-role works!\r\n</p>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.html":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.html ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-dialog header=\"Audition Details\" #detaildialog\r\n          [focusOnShow]=\"false\" [responsive]=\"false\" styleClass=\"dialogsizer\" (onShow)=\"onDialogShow($event, detaildialog)\" (onHide)=\"onDialogHide($event, detaildialog)\" [(visible)]=\"isVisible\" [modal]=\"true\" [minY]=\"70\"\r\n          [maximizable]=\"true\" [baseZIndex]=\"10000\">\r\n    <p-card header=\"Enter the audition details\">\r\n        <!--[breakpoint]=\"640\" [responsive]=\"true\" [style]=\"{width: '80%', minWidth: '640px'}\"-->\r\n\r\n        <form [formGroup]=\"basicDialogInfo\" #formDirective=\"ngForm\">\r\n            <div class=\"p-grid p-dir-col p-justify-around\">\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                  <label class=\"p-col\">Internal Name (for your use only)</label>\r\n                  <input class=\"p-col\" #name pInputText placeholder=\"Internal Name\" formControlName=\"name\" required>\r\n                  <p-message class=\"p-col\" severity=\"error\" text=\"Internal Name is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['name'].hasError('required') && basicDialogInfo.controls['name'].dirty\"></p-message>\r\n                </div>\r\n              </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                  <label class=\"p-col\">Audition Format</label>\r\n                  <p-radioButton #auditionType class=\"p-col\" (onClick)=\"auditionTypeOptionClick(auditionTypeEnum.InPersonAuditionOnly)\" name=\"auditionType\" value=\"15\" label=\"In person auditions\" formControlName=\"auditionType\"></p-radioButton>\r\n                  <p-radioButton class=\"p-col\" (onClick)=\"auditionTypeOptionClick(auditionTypeEnum.TapedSubmissionThenInPersonAudition)\" name=\"auditionType\" value=\"7\" label=\"Taped audition submissions followed by in person auditions\" formControlName=\"auditionType\"></p-radioButton>\r\n                  <p-radioButton class=\"p-col\" (onClick)=\"auditionTypeOptionClick(auditionTypeEnum.TapedSubmissionAndInPersonAudition)\" name=\"auditionType\" value=\"3\" label=\"Taped audition submissions OR in person auditions\" formControlName=\"auditionType\"></p-radioButton>\r\n                  <p-radioButton class=\"p-col\" (onClick)=\"auditionTypeOptionClick(auditionTypeEnum.TapedSubmissionsOnly)\" name=\"auditionType\" value=\"1\" label=\"Taped audition submissions\" formControlName=\"auditionType\"></p-radioButton>\r\n                  <p-message severity=\"error\" class=\"p-col\" text=\"CastingCall Type is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['auditionType'].hasError('required') && basicDialogInfo.controls['auditionType'].dirty\">\r\n                  </p-message>\r\n                  <div class=\"p-col\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n\r\n                </div>\r\n              </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-align-start switchoffset\">\r\n                  <p-inputSwitch styleClass=\"p-col\" formControlName=\"onlyThoseSelectedWillParticipate\"></p-inputSwitch>\r\n                  <label class=\"p-col\">Only selected candidates will audition</label>\r\n                </div>\r\n              </div>\r\n\r\n                <div *ngIf=\"isInPersonAudition\" class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                        <label class=\"p-col\">Audition Location</label>\r\n                        <input class=\"p-col\" pInputText #location placeholder=\"Location name\" formControlName=\"location\" required>\r\n                        <p-message styleClass=\"p-col\" severity=\"error\" text=\"Location name is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['location'].hasError('required') && basicDialogInfo.controls['location'].dirty\"></p-message>\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"isInPersonAudition\" class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                        <label class=\"p-col\">Location Details</label>\r\n                        <textarea class=\"p-col\" rows=\"6\" #locationDetail pInputTextarea placeholder=\"Location details\" formControlName=\"locationDetail\"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                        <label class=\"p-col\">Audition Details</label>\r\n                        <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Audition Details\" formControlName=\"details\"></textarea>\r\n                    </div>\r\n                </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                  <label class=\"p-col\">Special Instructions</label>\r\n                  <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Special instructions\" formControlName=\"instructions\"></textarea>\r\n                </div>\r\n              </div>\r\n\r\n              <div *ngIf=\"isInPersonAudition\" class=\"p-col\">\r\n                <div class=\"p-grid p-align-start switchoffset\">\r\n                  <p-inputSwitch styleClass=\"p-col\" formControlName=\"datesToBeDecidedLater\" (onChange)=\"clickDateDecisionSwitch($event)\"></p-inputSwitch>\r\n                  <label class=\"p-col\">Audition dates will be decided at a later time</label>\r\n                </div>\r\n              </div>\r\n\r\n                <div *ngIf=\"isInPersonAudition\" class=\"p-col\">\r\n                    <div class=\"p-grid p-align-start\">\r\n\r\n                        <div class=\"p-col-12 p-md-6\">\r\n                            <div class=\"p-grid p-dir-col\">\r\n                                <label class=\"p-col\">Audition Start Date</label>\r\n                                <p-calendar [inputStyleClass]=\"p-col\" #startDate required formControlName=\"startDate\" (onSelect)=\"selectStartDate($event)\" dataType=\"string\" placeholder=\"Start Date\" dateFormat=\"DD MM d yy\" [minDate]=\"minDate\" [showIcon]=\"true\" [readonlyInput]=\"true\"></p-calendar>\r\n                                <p-message styleClass=\"p-col\" severity=\"error\" text=\"Start Date is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['startDate'].hasError('required') && basicDialogInfo.controls['startDate'].dirty\"></p-message>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col-12 p-md-6\">\r\n                            <div class=\"p-grid p-align-start switchoffset\">\r\n                                <p-inputSwitch styleClass=\"p-col\" formControlName=\"isOneDayOnly\" (onChange)=\"clickOneDaySwitch($event)\"></p-inputSwitch>\r\n                                <label class=\"p-col\">This is a 1 day only audition</label>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div *ngIf=\"isInPersonAudition\" class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Audition End Date</label>\r\n                        <p-calendar [inputStyleClass]=\"p-col\" #endDate formControlName=\"endDate\" required dateFormat=\"DD MM d yy\" (onSelect)=\"selectEndDate($event)\" [minDate]=\"minDate\" placeholder=\"End Date\" [showIcon]=\"true\" [readonlyInput]=\"true\"></p-calendar>\r\n                        <p-message styleClass=\"p-col\" severity=\"error\" text=\"End Date is <strong>required</strong> unless this is a 1 day only event.\" *ngIf=\"basicDialogInfo.controls['endDate'].hasError('required') && basicDialogInfo.controls['endDate'].dirty\"></p-message>\r\n                    </div>\r\n                </div>\r\n              <div *ngIf=\"isInPersonAudition\" class=\"p-col\">\r\n                <div class=\"p-grid p-align-start switchoffset\">\r\n                  <p-inputSwitch styleClass=\"p-col\" formControlName=\"datesAreTemptative\"></p-inputSwitch>\r\n                  <label class=\"p-col\">Audition dates are temptative</label>\r\n                </div>\r\n              </div>\r\n\r\n\r\n            </div>\r\n        </form>\r\n        <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n    </p-card>\r\n    <p-footer>\r\n        <button type=\"button\" [disabled]=\"saveIsDisabled\" pButton icon=\"pi pi-check\" (click)=\"saveClick()\" label=\"Save\"></button>\r\n        <button type=\"button\" [disabled]=\"cancelIsDisabled\" pButton icon=\"pi pi-close\" (click)=\"cancelClick()\" label=\"Cancel\" class=\"ui-button-secondary\"></button>\r\n    </p-footer>\r\n</p-dialog>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.html":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.html ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-card>\r\n  <div class=\"p-grid p-dir-col\">\r\n    <div class=\"p-col\">\r\n      <a (click)=\"viewDetails()\">{{detail.name}}</a>\r\n    </div>\r\n  <div class=\"p-col\">\r\n    <div class=\"p-grid p-dir-col p-align-center p-nogutter\">\r\n      <div class=\"p-col p-col-align-start labeler\">Audition Type</div>\r\n      <div class=\"p-col infodata\" *ngIf=\"(detail.auditionTypeEnum == auditionTypeEnum.InPersonAuditionOnly || \r\n           detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionAndInPersonAudition)\">In Person Auditions</div>\r\n      <div class=\"p-col midlelabeler\" *ngIf=\"detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionAndInPersonAudition\">or</div>\r\n      <div class=\"p-col infodata\" *ngIf=\"(detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionsOnly || \r\n           detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionAndInPersonAudition || \r\n           detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionThenInPersonAudition)\">Taped Audition Submissions</div>\r\n      <div class=\"p-col midlelabeler\" *ngIf=\"detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionThenInPersonAudition\">followed by</div>\r\n      <div class=\"p-col infodata\" *ngIf=\"detail.auditionTypeEnum == auditionTypeEnum.TapedSubmissionThenInPersonAudition\">In Person Auditions</div>\r\n      <div class=\"p-col p-col-align-start hotinfodata\" *ngIf=\"detail.onlyThoseSelectedWillParticipate\">Only selected candidates will audition</div>\r\n\r\n        </div>\r\n    </div>\r\n    <div class=\"p-col\" *ngIf=\"detail.auditionTypeEnum != auditionTypeEnum.TapedSubmissionsOnly\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n        <div class=\"p-col labeler\">Location</div>\r\n        <div class=\"p-col infodata\">{{detail.location}}</div>\r\n        <div *ngIf=\"detail.locationDetail.length > 0\" class=\"p-col\">   \r\n          <a class=\"linktoggler\" (click)=\"locationDetail.toggle($event)\">Location details..</a>\r\n        </div>\r\n\r\n        <p-overlayPanel #locationDetail>\r\n          <pre>{{detail.locationDetail | linebreak}}</pre>\r\n        </p-overlayPanel>\r\n      </div>\r\n    </div>\r\n    <div class=\"p-col\" *ngIf=\"detail.details.length > 0\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n        <div class=\"p-col\">   \r\n          <a class=\"linktoggler\" (click)=\"details.toggle($event)\">Audition details..</a>\r\n        </div>\r\n        <p-overlayPanel #details>\r\n          <pre>{{detail.details | linebreak}}</pre>\r\n        </p-overlayPanel>\r\n      </div>\r\n    </div>\r\n    <div class=\"p-col\" *ngIf=\"detail.instructions.length > 0\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n        <div class=\"p-col\">   \r\n          <a class=\"linktoggler\" (click)=\"instructions.toggle($event)\">Special instructions..</a>\r\n        </div>\r\n        <p-overlayPanel #instructions>\r\n          <pre>{{detail.instructions | linebreak}}</pre>\r\n        </p-overlayPanel>\r\n      </div>\r\n    </div>\r\n    <div class=\"p-col\" *ngIf=\"detail.auditionTypeEnum != auditionTypeEnum.TapedSubmissionsOnly\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n        <div class=\"p-col labeler\" *ngIf=\"detail.isOneDayOnly\">Event Date</div>\r\n        <div class=\"p-col labeler\" *ngIf=\"!detail.isOneDayOnly\">Start Date</div>\r\n          <div *ngIf=\"!detail.datesToBeDecidedLater\" class=\"p-col infodata\">{{detail.startDate | date}}</div>\r\n        <div class=\"p-col infodata\" *ngIf=\"detail.datesToBeDecidedLater\">To be decided later</div>\r\n        <div class=\"p-col hotinfodata\" *ngIf=\"detail.isOneDayOnly\">This is a one day only event</div>\r\n        <div class=\"p-col labeler\" *ngIf=\"!detail.isOneDayOnly\">End Date</div>\r\n          <div class=\"p-col infodata\" *ngIf=\"!detail.datesToBeDecidedLater && !detail.isOneDayOnly\">{{detail.endDate | date}}</div>\r\n        <div class=\"p-col infodata\" *ngIf=\"detail.datesToBeDecidedLater && !detail.isOneDayOnly\">To be decided later</div>\r\n        <div class=\"p-col hotinfodata\" *ngIf=\"detail.datesAreTemptative\">Dates are tentative</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <p-footer>\r\n    <a class=\"pi pi-pencil\" (click)=\"viewDetails()\"></a>\r\n    <a class=\"pi pi-trash\" (click)=\"deleteDetails()\"></a>\r\n  </p-footer>\r\n</p-card>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.html":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.html ***!
  \********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col p-justify-around\">\r\n      <app-audition-details-list-item class=\"p-col\" *ngFor=\"let detail of auditionDetails\" [detail]=\"detail\">\r\n    </app-audition-details-list-item>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-production/new-production.component.html":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-production/new-production.component.html ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"p-grid\">\r\n    <div class=\"p-col-9\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n            <p-card class=\"p-col\" header=\"Create a new project\">\r\n               <p-header>\r\n                <div #basiccard></div>\r\n                </p-header>\r\n                <form [formGroup]=\"basicProductionInfo\">\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Title of Production</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"e.g. 'Batman lives'\" #name formControlName=\"name\" required>\r\n                                <p-message severity=\"error\" text=\"Production Title is <strong>required</strong>\" class=\"p-col\" *ngIf=\"basicProductionInfo.controls['name'].hasError('required') && basicProductionInfo.controls['name'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Production Type</label>\r\n                                <p-dropdown [options]=\"productionTypes\" [group]=\"true\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #productiontype placeholder=\"Select a Type of Production\" formControlName=\"productionTypeId\" required>\r\n                                  <ng-template let-item pTemplate=\"selectedItem\"> \r\n                                    {{item.label}} <!-- highlighted selected item -->\r\n                                  </ng-template> \r\n                                  <ng-template let-item pTemplate=\"item\"> \r\n                                    {{item.label}}\r\n                                  </ng-template>\r\n                                  <ng-template let-group pTemplate=\"group\">\r\n                                    <span style=\"margin-left:.25em\">{{group.label}}</span>\r\n                                  </ng-template>\r\n                                  </p-dropdown>\r\n                                <!-- <mat-select placeholder=\"Select a Type of Production\" formControlName=\"productionTypeId\" required>\r\n                                     <mat-optgroup *ngFor=\"let cat of meta.productionTypeCategories\" [label]=\"cat.name\">\r\n                                         <mat-option *ngFor=\"let prodType of cat.productionTypes\" [value]=\"prodType.id\">\r\n                                                </mat-option>\r\n\r\n                                     </mat-optgroup>\r\n\r\n                                 </mat-select>-->\r\n                                <p-message severity=\"error\" text=\"Production Type is <strong>required</strong>\" class=\"p-col\" *ngIf=\"basicProductionInfo.controls['productionTypeId'].hasError('required') && basicProductionInfo.controls['productionTypeId'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Production</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Production Company</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Production Company Name\" formControlName=\"productionCompany\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. Starlight Productions</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Production Description</label>\r\n                                <textarea rows=\"3\" class=\"p-col\" pInputTextarea #description placeholder=\"Casting 'Batman Lives', a new CBS TV series based on the classic legends but taking place in a modern-day Gotham City.\" formControlName=\"description\" required></textarea>\r\n                                <p-message severity=\"error\" text=\"Production Description is <strong>required</strong>\" class=\"p-col\" *ngIf=\"basicProductionInfo.controls['description'].hasError('required') && basicProductionInfo.controls['description'].dirty\">\r\n                                    \r\n                                </p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Casting 'Batman Lives', a new CBS TV series based on the classic legends but taking place in a modern-day Gotham City.</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Union Status</label>\r\n                              <div class=\"p-col\">\r\n                                <div class=\"p-grid p-justify-around\">\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"7\" label=\"Union and Non-Union\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"3\" label=\"Union\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"1\" label=\"Non-Union\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                  <p-radioButton class=\"p-col\" name=\"unions\" value=\"15\" label=\"N/A\" formControlName=\"productionUnionStatus\"></p-radioButton>\r\n                                </div>\r\n                                </div>\r\n\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-start\">\r\n                                <label class=\"p-col\">Will the talent be paid?</label>\r\n                              <div class=\"p-col\">\r\n                                <div class=\"p-grid p-justify-around\">\r\n                                <p-radioButton class=\"p-col\" name=\"pays\" value=\"1\" label=\"No\" formControlName=\"compensationType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"pays\" value=\"3\" label=\"Some\" formControlName=\"compensationType\"></p-radioButton>\r\n                                <p-radioButton class=\"p-col\" name=\"pays\" value=\"7\" label=\"Yes\" formControlName=\"compensationType\"></p-radioButton>\r\n                                </div>\r\n                              </div>\r\n\r\n\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Compensation & Union Contract Details</label>\r\n                                <textarea rows=\"3\" class=\"p-col\" pInputTextarea placeholder=\"e.g., Pays $100/day, plus travel and meals provided. No participation fees required. The producers plan to apply for a SAG-AFTRA New Media Agreement.\" formControlName=\"compensationContractDetails\"></textarea>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g., Pays $100/day, plus travel and meals provided. No participation fees required. The producers plan to apply for a SAG-AFTRA New Media Agreement.</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </p-card>\r\n\r\n            <p-card header=\"Roles\" class=\"p-col\">\r\n              <p-header>\r\n                <div #rolescard></div>\r\n              </p-header>\r\n\r\n\r\n                <app-role-list [production]=\"production\" [roles]=\"production.roles\">\r\n\r\n                </app-role-list>\r\n\r\n\r\n                <button (click)=\"addRoleClick()\" label=\"Add Role\" pButton></button>\r\n            </p-card>\r\n            <p-card header=\"Casting\" class=\"p-col\">\r\n              <p-header>\r\n                <div #castingcard></div>\r\n              </p-header>\r\n\r\n                <form [formGroup]=\"castingDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Casting Director</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Casting Director\" formControlName=\"castingDirector\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Casting Director</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Casting Assistant</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Casting Assistant\" formControlName=\"castingAssistant\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Casting Assistant</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Casting Associate</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Casting Associate\" formControlName=\"castingAssociate\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Casting Associate</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Assistant Casting Associate</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Assistant Casting Associate\" formControlName=\"assistantCastingAssociate\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Assistant Casting Associate</div>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n\r\n            </p-card>\r\n            <p-card header=\"Producing\" class=\"p-col\">\r\n              <p-header>\r\n                <div #productioncard></div>\r\n              </p-header>\r\n\r\n                <form [formGroup]=\"productionDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Executive Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Executive Producer\" formControlName=\"executiveProducer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Executive Producer</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Co Executive Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Co Executive Producer\" formControlName=\"coExecutiveProducer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Co Executive Producer</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Producer\" formControlName=\"producer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Producer</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Co Producer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Co Producer\" formControlName=\"coProducer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Co Producer</div>\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </form>\r\n\r\n            </p-card>\r\n            <p-card header=\"Artistic\" class=\"p-col\">\r\n              <p-header>\r\n                <div #artisticcard></div>\r\n              </p-header>\r\n                <form [formGroup]=\"artisticDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">Artistic Director</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Artistic Director\" formControlName=\"artisticDirector\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Artistic Director</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Music Director</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Music Director\" formControlName=\"musicDirector\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Music Director</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Choreographer</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Choreographer\" formControlName=\"choreographer\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Choreographer</div>\r\n\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </form>\r\n            </p-card>\r\n            <p-card header=\"Other\" class=\"p-col\">\r\n              <p-header>\r\n                <div #othercard></div>\r\n              </p-header>\r\n                <form [formGroup]=\"extraDetails\">\r\n\r\n                    <div class=\"p-grid p-dir-col p-justify-around\">\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <label class=\"p-col\">TV Network</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the TV Network\" formControlName=\"TVNetwork\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the TV Network</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                <label class=\"p-col\">Venue</label>\r\n                                <input class=\"p-col\" pInputText placeholder=\"Enter the Name of the Venue\" formControlName=\"venue\">\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Enter the Name of the Venue</div>\r\n\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            \r\n            </p-card>\r\n            <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n            <div class=\"p-grid p-justify-end p-align-center\">\r\n                <button pButton (click)=\"cancelEditProductionClick()\" label=\"Cancel\"></button>\r\n                <button pButton color=\"primary\" (click)=\"saveProductionClick()\" label=\"Save\"></button>\r\n\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n    <div class=\"p-col-3\">\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n            <button class=\"p-col\" pButton (click)=\"scrollToBasic()\">go Basic</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToRoles()\">go Roles</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToCasting()\">go Casting</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToProduction()\">go Production</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToArtistic()\">go Artistic</button>\r\n            <button class=\"p-col\" pButton (click)=\"scrollToOther()\">go Other</button>\r\n            <button class=\"p-col\" pButton color=\"primary\" (click)=\"saveProductionClick()\">Save</button>\r\n            <button class=\"p-col\" pButton color=\"primary\" (click)=\"deleteProductionClick()\">Delete</button>\r\n            <button class=\"p-col\" pButton color=\"primary\" (click)=\"cancelEditProductionClick()\">Cancel</button>\r\n        </div>\r\n\r\n\r\n    </div>\r\n\r\n</div>\r\n\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.html ***!
  \********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n  <p-dialog header=\"Sides\" #newroledialog\r\n            [focusOnShow]=\"false\" [responsive]=\"false\" (onShow)=\"onDialogShow($event, newroledialog)\" (onHide)=\"onDialogHide($event, newroledialog)\"\r\n            [(visible)]=\"isVisible\" [modal]=\"true\" [minY]=\"70\"\r\n            [baseZIndex]=\"9995\">\r\n\r\n    <div class=\"p-grid p-dir-col\">\r\n\r\n\r\n\r\n      <p-card header=\"Enter the details about the roles you are casting\" class=\"p-col\">\r\n\r\n        <form [formGroup]=\"basicRoleInfo\" #formDirective=\"ngForm\">\r\n          <div class=\"p-grid p-dir-col p-justify-around\">\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n\r\n                <label class=\"p-col\">Role Name</label>\r\n                <input class=\"p-col\" pInputText #name placeholder=\"Role Name\" formControlName=\"name\" required>\r\n                <p-message class=\"p-col\" severity=\"error\" text=\"Role Name is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['name'].hasError('required') && basicRoleInfo.controls['name'].dirty\"></p-message>\r\n                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. 'Batman'</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-dir-col p-justify-between\">\r\n                <label class=\"p-col\">Role Type</label>\r\n                <p-dropdown class=\"p-col\" [options]=\"roleTypes\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #roletype placeholder=\"Select a Type of Role\" formControlName=\"roleTypeId\" required>\r\n                </p-dropdown>\r\n                <p-message class=\"p-col\" severity=\"error\" text=\"Role Type is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['roleTypeId'].hasError('required') && basicRoleInfo.controls['roleTypeId'].dirty\"></p-message>\r\n                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Role</div>\r\n              </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-align-start\">\r\n\r\n                <div class=\"p-col-12 p-md-6\">\r\n                  <div class=\"p-grid p-dir-col\">\r\n                    <label class=\"p-col\">Gender (optional)</label>\r\n                    <p-dropdown class=\"p-col\" [options]=\"genderTypes\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #gendertype placeholder=\"Select Gender\" formControlName=\"roleGenderTypeId\" required>\r\n                    </p-dropdown>\r\n                    <p-message class=\"p-col\" severity=\"error\" text=\"Gender Type is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['roleGenderTypeId'].hasError('required') && basicRoleInfo.controls['roleGenderTypeId'].dirty\"></p-message>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Gender</div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"p-col-12 p-md-6\">\r\n                  <div class=\"p-grid p-align-start switchoffset\">\r\n                    <p-inputSwitch class=\"p-col\" formControlName=\"transgender\"></p-inputSwitch>\r\n                    <label class=\"p-col\">Transgender</label>\r\n\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-dir-col p-justify-between\">\r\n                <label class=\"p-col\">Role Description (optional)</label>\r\n                <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Role Description\" formControlName=\"description\"></textarea>\r\n              </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-dir-col p-justify-between\">\r\n                <label class=\"p-col\" class=\"p-col\">Age Range (optional): {{minAge}} to {{maxAge}}</label>\r\n                <p-slider class=\"p-col\" formControlName=\"age\" (onChange)=\"handleAgeSliderChange($event)\" [style]=\"{'width':'100%'}\" [min]=\"0\" [max]=\"100\" [range]=\"true\"></p-slider>\r\n                <div class=\"p-col\">\r\n                  <div class=\"p-grid p-justify-between p-align-end\">\r\n                    <label>{{minAge}}</label><label>{{maxAge}}</label>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-justify-between\">\r\n                <p-inputSwitch class=\"p-col\" formControlName=\"requiresNudity\"></p-inputSwitch>\r\n                <label class=\"p-col\">This role requires nudity</label>\r\n\r\n              </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-dir-col p-justify-between\">\r\n                <label class=\"p-col\">Pay Detail</label>\r\n                <div class=\"p-grid p-justify-between\">\r\n\r\n                  <div class=\"p-col-8\">\r\n                    <p-dropdown class=\"p-col\" [options]=\"payDetails\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #paydetail placeholder=\"Select a pay package\" formControlName=\"payDetailId\">\r\n                      <ng-template let-item pTemplate=\"selectedItem\">\r\n                        <div>\r\n                          {{item.name}} {{item.id}} <button pButton label=\"Remove\" (click)=\"removePayDetail()\"></button>\r\n                        </div>\r\n                      </ng-template>\r\n                      <ng-template let-item pTemplate=\"item\">\r\n                        <div>\r\n                          {{item.name}} {{item.id}}\r\n                        </div>\r\n                      </ng-template>\r\n                    </p-dropdown>\r\n\r\n                  </div>\r\n                  <div class=\"p-col-4\">\r\n                    <button pButton label=\"Add new pay details\" (click)=\"addNewPayDetails()\"></button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n\r\n          </div>\r\n        </form>\r\n\r\n\r\n\r\n        <div class=\"p-grid p-dir-col p-justify-around\">\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Languages (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"languages\" [options]=\"metadata.languages\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Languages\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Languages</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n              <label class=\"p-col\">Accents (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"accents\" [options]=\"metadata.accents\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Accents\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Accents</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n              <label class=\"p-col\">StereoTypes (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"stereoTypes\" [options]=\"metadata.stereoTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select StereoTypes\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select StereoTypes</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n              <label class=\"p-col\">Ethnicities (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"ethnicities\" [options]=\"metadata.ethnicities\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Ethnicities\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Ethnicities</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n              <label class=\"p-col\">Ethnic Stereotypes (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"ethnicStereoTypes\" [options]=\"metadata.ethnicStereoTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Ethnic Stereotypes\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Ethnic Stereotypes</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Hair Colors (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"hairColors\" [options]=\"metadata.hairColors\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select HairColors\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Hair Colors</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Eye Colors (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"eyeColors\" [options]=\"metadata.eyeColors\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select EyeColors\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Eye Colors</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Body Types (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"bodyTypes\" [options]=\"metadata.bodyTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select BodyTypes\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Body Types</div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n        <p-footer class=\"p-grid p-justify-end\">\r\n          <button pButton type=\"button\" label=\"Cancel\" (click)=\"onNoClick()\" class=\"ui-button-secondary\"></button>\r\n          <button pButton type=\"button\" label=\"Save Role\" (click)=\"saveRoleClick()\"></button>\r\n        </p-footer>\r\n\r\n      </p-card>\r\n\r\n    </div>\r\n\r\n\r\n  </p-dialog>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-role/new-role.component.html":
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-role/new-role.component.html ***!
  \******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-pay-details-dialog [productionId]=production.id></app-pay-details-dialog>\r\n\r\n<div class=\"p-grid p-dir-col\">\r\n\r\n\r\n\r\n    <p-card header=\"Enter the details about the roles you are casting\" class=\"p-col\">\r\n\r\n        <form [formGroup]=\"basicRoleInfo\">\r\n            <div class=\"p-grid p-dir-col p-justify-around\">\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n\r\n                        <label class=\"p-col\">Role Name</label>\r\n                        <input class=\"p-col\" pInputText #name  placeholder=\"Role Name\" formControlName=\"name\" required>\r\n                        <p-message class=\"p-col\" severity=\"error\" text=\"Role Name is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['name'].hasError('required') && basicRoleInfo.controls['name'].dirty\"></p-message>\r\n                        <div class=\"p-col p-align-end\" *ngIf=\"showHints\">e.g. 'Batman'</div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Role Type</label>\r\n                        <p-dropdown class=\"p-col\" [options]=\"roleTypes\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #roletype placeholder=\"Select a Type of Role\" formControlName=\"roleTypeId\" required>\r\n                        </p-dropdown>\r\n                        <p-message class=\"p-col\" severity=\"error\" text=\"Role Type is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['roleTypeId'].hasError('required') && basicRoleInfo.controls['roleTypeId'].dirty\"></p-message>\r\n                        <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Role</div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-align-start\">\r\n\r\n                        <div class=\"p-col-12 p-md-6\">\r\n                            <div class=\"p-grid p-dir-col\">\r\n                                <label class=\"p-col\">Gender (optional)</label>\r\n                                <p-dropdown class=\"p-col\" [options]=\"genderTypes\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #gendertype placeholder=\"Select Gender\" formControlName=\"roleGenderTypeId\" required>\r\n                                </p-dropdown>\r\n                                <p-message class=\"p-col\" severity=\"error\" text=\"Gender Type is <strong>required</strong>\" *ngIf=\"basicRoleInfo.controls['roleGenderTypeId'].hasError('required') && basicRoleInfo.controls['roleGenderTypeId'].dirty\"></p-message>\r\n                                <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select a Type of Gender</div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"p-col-12 p-md-6\">\r\n                            <div class=\"p-grid p-align-start switchoffset\">\r\n                                <p-inputSwitch class=\"p-col\" formControlName=\"transgender\"></p-inputSwitch>\r\n                                <label class=\"p-col\">Transgender</label>\r\n\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Role Description (optional)</label>\r\n                        <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Role Description\" formControlName=\"description\"></textarea>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\" class=\"p-col\">Age Range (optional): {{minAge}} to {{maxAge}}</label>\r\n                        <p-slider class=\"p-col\" formControlName=\"age\" (onChange)=\"handleAgeSliderChange($event)\" [style]=\"{'width':'100%'}\" [min]=\"0\" [max]=\"100\" [range]=\"true\"></p-slider>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-justify-between p-align-end\">\r\n                                <label>{{minAge}}</label><label>{{maxAge}}</label>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-justify-between\">\r\n                  <p-inputSwitch class=\"p-col\" formControlName=\"requiresNudity\"></p-inputSwitch>\r\n                  <label class=\"p-col\">This role requires nudity</label>\r\n\r\n                </div>\r\n              </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                   <label class=\"p-col\">Pay Detail</label>\r\n                 <div class=\"p-grid p-justify-between\">\r\n\r\n                     <div class=\"p-col-8\">\r\n                         <p-dropdown class=\"p-col\" [options]=\"payDetails\" [style]=\"{'width':'100%'}\" autoWidth=\"false\" #paydetail placeholder=\"Select a pay package\" formControlName=\"payDetailId\">\r\n                             <ng-template let-item pTemplate=\"selectedItem\">\r\n                                 <div>\r\n                                     {{item.name}} {{item.id}} <button pButton label=\"Remove\" (click)=\"removePayDetail()\"></button>\r\n                                 </div>\r\n                             </ng-template>\r\n                             <ng-template let-item pTemplate=\"item\">\r\n                                 <div>\r\n                                     {{item.name}} {{item.id}}\r\n                                 </div>\r\n                             </ng-template>\r\n                         </p-dropdown>\r\n\r\n                     </div>\r\n                     <div class=\"p-col-4\">\r\n                  <button pButton label=\"Add new pay details\" (click)=\"addNewPayDetails()\"></button>\r\n                         </div>\r\n                    </div>\r\n                           </div>\r\n              </div>\r\n\r\n\r\n            </div>\r\n        </form>\r\n\r\n\r\n\r\n        <div class=\"p-grid p-dir-col p-justify-around\">\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                    <label class=\"p-col\">Languages (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"languages\" [options]=\"metadata.languages\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Languages\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Languages</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">Accents (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"accents\" [options]=\"metadata.accents\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Accents\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Accents</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">StereoTypes (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"stereoTypes\" [options]=\"metadata.stereoTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select StereoTypes\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select StereoTypes</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">Ethnicities (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"ethnicities\" [options]=\"metadata.ethnicities\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Ethnicities\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Ethnicities</div>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                    <label class=\"p-col\">Ethnic Stereotypes (optional)</label>\r\n                    <p-multiSelect class=\"p-col\" [(ngModel)]=\"ethnicStereoTypes\" [options]=\"metadata.ethnicStereoTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select Ethnic Stereotypes\">\r\n                    </p-multiSelect>\r\n                    <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Ethnic Stereotypes</div>\r\n                </div>\r\n            </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Hair Colors (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"hairColors\" [options]=\"metadata.hairColors\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select HairColors\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Hair Colors</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Eye Colors (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"eyeColors\" [options]=\"metadata.eyeColors\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select EyeColors\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Eye Colors</div>\r\n            </div>\r\n          </div>\r\n          <div class=\"p-col\">\r\n            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Body Types (optional)</label>\r\n              <p-multiSelect class=\"p-col\" [(ngModel)]=\"bodyTypes\" [options]=\"metadata.bodyTypes\" [style]=\"{'width':'100%'}\" optionLabel=\"name\" autoWidth=\"false\" placeholder=\"Select BodyTypes\">\r\n              </p-multiSelect>\r\n              <div class=\"p-col p-align-end\" *ngIf=\"showHints\">Select Body Types</div>\r\n            </div>\r\n          </div>\r\n\r\n        </div>\r\n        <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n        <p-footer class=\"p-grid p-justify-end\">\r\n            <button pButton type=\"button\" label=\"Cancel\" (click)=\"onNoClick()\" class=\"ui-button-secondary\"></button>\r\n            <button pButton type=\"button\" label=\"Save Role\" (click)=\"saveRoleClick()\"></button>\r\n        </p-footer>\r\n\r\n    </p-card>\r\n\r\n</div>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.html":
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.html ***!
  \**************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-dialog header=\"Pay Details\" #detaildialog\r\n          [focusOnShow]=\"false\" [responsive]=\"false\" styleClass=\"dialogsizer\" (onShow)=\"onDialogShow($event, detaildialog)\" (onHide)=\"onDialogHide($event, detaildialog)\" [(visible)]=\"isVisible\" [modal]=\"true\" [minY]=\"70\"\r\n          [maximizable]=\"true\" [baseZIndex]=\"10000\">\r\n    <p-card header=\"Enter the pay details\">\r\n        <!--[breakpoint]=\"640\" [responsive]=\"true\" [style]=\"{width: '80%', minWidth: '640px'}\"-->\r\n\r\n        <form [formGroup]=\"basicDialogInfo\" #formDirective=\"ngForm\">\r\n            <div class=\"p-grid p-dir-col p-justify-around\">\r\n            <div class=\"p-col\">\r\n              <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                    <label class=\"p-col\">Internal Name (for your use only)</label>\r\n                    <input class=\"p-col\" #name pInputText placeholder=\"Internal Name\" formControlName=\"name\" required>\r\n                    <p-message class=\"p-col\" severity=\"error\" text=\"Internal Name is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['name'].hasError('required') && basicDialogInfo.controls['name'].dirty\"></p-message>\r\n              </div>\r\n            </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n                  <label class=\"p-col\">Pay Type</label>\r\n                  <p-radioButton #payPackageType class=\"p-col\" (onClick)=\"payTypeOptionClick(payPackageTypeEnum.gift)\" name=\"payPackageType\" value=\"7\" label=\"Gift\" formControlName=\"payPackageType\"></p-radioButton>\r\n                  <p-radioButton class=\"p-col\" (onClick)=\"payTypeOptionClick(payPackageTypeEnum.credit)\" name=\"payPackageType\" value=\"3\" label=\"Credit\" formControlName=\"payPackageType\"></p-radioButton>\r\n                  <p-radioButton class=\"p-col\" (onClick)=\"payTypeOptionClick(payPackageTypeEnum.money)\" name=\"payPackageType\" value=\"1\" label=\"Money\" formControlName=\"payPackageType\"></p-radioButton>\r\n                  <p-message severity=\"error\" class=\"p-col\" text=\"Pay Type is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['payPackageType'].hasError('required') && basicDialogInfo.controls['payPackageType'].dirty\">\r\n                  </p-message>\r\n                  <div class=\"p-col\" *ngIf=\"showHints\">e.g. 'Batman lives'</div>\r\n\r\n                </div>\r\n              </div>\r\n              <div class=\"p-col\">\r\n                <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                  <label class=\"p-col\">Pay Details</label>\r\n                  <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Pay Details\" formControlName=\"details\"></textarea>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n        </form>\r\n        <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n    </p-card>\r\n    <p-footer>\r\n      <button type=\"button\" [disabled]=\"saveIsDisabled\" pButton icon=\"pi pi-check\" (click)=\"saveClick()\" label=\"Save\"></button>\r\n      <button type=\"button\" [disabled]=\"cancelIsDisabled\" pButton icon=\"pi pi-close\" (click)=\"cancelClick()\" label=\"Cancel\" class=\"ui-button-secondary\"></button>\r\n    </p-footer>\r\n</p-dialog>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.html":
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.html ***!
  \********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <p-card>\r\n    <div class=\"p-grid p-dir-col\">\r\n      <div class=\"p-col\">\r\n        <a (click)=\"viewDetails()\">{{detail.name}}</a>\r\n      </div>\r\n    </div>\r\n    <p-footer>\r\n      <a class=\"pi pi-pencil\" (click)=\"viewDetails()\"></a>\r\n      <a class=\"pi pi-trash\" (click)=\"deleteDetails()\"></a>\r\n    </p-footer>\r\n  </p-card>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.html ***!
  \**********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col p-justify-around\">\r\n    <app-pay-details-list-item class=\"p-col\" *ngFor=\"let detail of payDetails\" [detail]=\"detail\">\r\n    </app-pay-details-list-item>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production-list-item/production-list-item.component.html":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production-list-item/production-list-item.component.html ***!
  \******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-card header=\"{{production.name}}\">\r\n\r\n    <div class=\"p-grid p-dir-col p-justify-between\">\r\n        <div class=\"p-col\">\r\n            <div class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>Production Type:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.productionType.name}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>Role Count:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.roles.length}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n        <div class=\"p-col\">\r\n            <div class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>CastingCall Count:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.castingCalls.length}}</span>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col-6\">\r\n                    <div class=\"p-grid p-justify-start\">\r\n                        <div class=\"p-col-8\">\r\n                            <span>Casting Session Count:</span>\r\n                        </div>\r\n                        <div class=\"p-col-4\">\r\n                            <span class=\"valuelabel\">{{production.castingSessions.length}}</span>\r\n                         </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <p-footer>\r\n        <a uiSref=\"production\" [uiParams]=\"{ productionId:production.id }\">{{production.name}}</a>\r\n        <a pButton uiSref=\"editproduction\" [uiParams]=\"{ productionId:production.id }\">Edit</a>\r\n        <a pButton uiSref=\"production\" [uiParams]=\"{ productionId:production.id }\">View</a>\r\n    </p-footer>\r\n</p-card>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production-list/production-list.component.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production-list/production-list.component.html ***!
  \********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n                <app-production-list-item *ngFor=\"let production of productions\" [production]=\"production\">\r\n              </app-production-list-item>\r\n\r\n           \r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production/production.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production/production.component.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-sides-dialog [side]=side [productionId]=production.id></app-sides-dialog>\r\n<app-audition-details-dialog [productionId]=production.id></app-audition-details-dialog>\r\n<app-shoot-details-dialog [productionId]=production.id></app-shoot-details-dialog>\r\n<app-pay-details-dialog [productionId]=production.id></app-pay-details-dialog>\r\n<app-new-role-dialog [productionId]=production.id [production]=\"production\" [metadata]=\"metadata\"></app-new-role-dialog>\r\n\r\n<div class=\"p-grid p-dir-col p-justify-between\">\r\n  <div class=\"p-col\">\r\n\r\n    <div class=\"p-grid p-align-start\">\r\n      <div class=\"p-col-6\">\r\n        <span>Production:</span>\r\n      </div>\r\n      <div class=\"p-col-6\">\r\n        <span class=\"valuelabel\">{{production.name}}</span>\r\n      </div>\r\n    </div>\r\n  </div>\r\n\r\n\r\n  <div class=\"p-col\">\r\n    <div class=\"p-grid p-justify-around p-align-start\">\r\n      <div class=\"p-col-6\">\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" toggler=\"header\" header=\"Roles ({{production.roles.length}})\">\r\n            <p-header>\r\n              <div class=\"ui-helper-clearfix\">\r\n                <p-button [style]=\"{'float':'right'}\" label=\"New Role\" (click)=\"createNewRole()\"></p-button>\r\n              </div>\r\n            </p-header>\r\n            <p-scrollPanel [style]=\"{'width': '100%', 'max-height': '400px'}\">\r\n              <app-role-list [production]=\"production\" [roles]=\"production.roles\">\r\n              </app-role-list>\r\n            </p-scrollPanel>\r\n            <p-footer>\r\n              <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addSide()\" style=\"margin-right: .25em\"></button>\r\n              <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n\r\n\r\n\r\n\r\n        </div>\r\n      </div>\r\n      <div class=\"p-col-3\">\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"CastingCalls ({{production.castingCalls.length}})\">\r\n\r\n            <p-footer>\r\n              <a pButton uiSref=\"newcastingCall\" icon=\"pi pi-plus\" label=\"New\" [uiParams]=\"{ productionId:production.id, production:production }\" class=\"ui-button-info\"></a>\r\n              <a pButton uiSref=\"productioncastingCalls\" [uiParams]=\"{ productionId:production.id }\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></a>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Casting Sessions ({{production.castingSessions.length}})\">\r\n            <p-footer>\r\n              <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addSide()\" style=\"margin-right: .25em\"></button>\r\n              <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Sides ({{production.sides.length}})\">\r\n            <app-sides-list [sides]=\"production.sides\"></app-sides-list>\r\n\r\n            <p-footer>\r\n              <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addSide()\" style=\"margin-right: .25em\"></button>\r\n              <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n\r\n\r\n        </div>\r\n      </div>\r\n      <div class=\"p-col-3\">\r\n\r\n        <div class=\"p-grid p-dir-col\">\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Shoot Details ({{production.shootDetails.length}})\">\r\n\r\n            <app-shoot-details-list [shootDetails]=\"production.shootDetails\"></app-shoot-details-list>\r\n            <p-footer>\r\n              <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addShootDetails()\" style=\"margin-right: .25em\"></button>\r\n              <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Audition Details ({{production.auditionDetails.length}})\">\r\n            <app-audition-details-list [auditionDetails]=\"production.auditionDetails\"></app-audition-details-list>\r\n\r\n            <p-footer>\r\n              <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addAuditionDetails()\" style=\"margin-right: .25em\"></button>\r\n              <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n          <p-panel class=\"p-col\" [toggleable]=\"true\" header=\"Pay Details ({{production.payDetails.length}})\">\r\n            <app-pay-details-list [payDetails]=\"production.payDetails\"></app-pay-details-list>\r\n\r\n\r\n            <p-footer>\r\n              <button pButton type=\"button\" icon=\"pi pi-plus\" label=\"New\" class=\"ui-button-info\" (click)=\"addPayDetails()\" style=\"margin-right: .25em\"></button>\r\n              <button pButton type=\"button\" icon=\"pi pi-search\" label=\"View\" class=\"ui-button-success\"></button>\r\n            </p-footer>\r\n          </p-panel>\r\n\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/productions/productions.component.html":
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/productions/productions.component.html ***!
  \************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<div class=\"p-grid p-justify-center\">\r\n    <div class=\"p-col-9\">\r\n       <!-- <div fxLayout=\"column\">\r\n            <mat-card fxFlex class=\"production_card\">\r\n                <mat-card-header>\r\n                    <mat-card-title>Your Projects</mat-card-title>\r\n                </mat-card-header>\r\n                <mat-card-content>\r\n                    <div fxLayout=\"column\">\r\n                        <mat-paginator [length]=\"length\"\r\n                                       [pageSize]=\"pageSize\"\r\n                                       [pageSizeOptions]=\"pageSizeOptions\"\r\n                                       (page)=\"changePageSizeOption($event)\">\r\n                        </mat-paginator>\r\n                        <div fxFlex=\"1 1 auto\" fxLayout=\"column\" class=\"production_list_wrapper scroll_me_wrapper\" fxLayoutAlign=\"start stretch\">\r\n                            <div fxFlex=\"1 1 auto\" class=\"production_list scroll_me\">-->\r\n                                <app-production-list [productions]=\"productions\">\r\n                                </app-production-list>\r\n    <!--    </div>\r\n                        </div>\r\n                        <mat-paginator [length]=\"length\"\r\n                                       [pageSize]=\"pageSize\"\r\n                                       [pageSizeOptions]=\"pageSizeOptions\"\r\n                                       (page)=\"changePageSizeOption($event)\">\r\n                        </mat-paginator>\r\n\r\n                    </div>\r\n                </mat-card-content>\r\n            </mat-card>\r\n\r\n        </div>-->\r\n    </div>\r\n    <div class=\"p-col-3\">\r\n        <div class=\"p-grid\">\r\n\r\n            <button class=\"p-col\" pButton label=\"Create New Project\" (click)=\"createNewProduction()\"></button>\r\n            <button class=\"p-col\" pButton label=\"go Roles\"></button>\r\n        </div>\r\n\r\n    </div>\r\n</div>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/role-list-item/role-list-item.component.html":
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/role-list-item/role-list-item.component.html ***!
  \******************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-card header=\"{{role.name}}\">\r\n\r\n    <div class=\"p-grid p-dir-col p-justify-between\">\r\n        <div class=\"p-col\" *ngIf=\"role.roleGenderTypeId !== 0 || role.roleTypeId !== 0 || role.requiresNudity || role.transgender\">\r\n           <!-- <mat-chip-list fxLayout=\"row\" fxLayoutAlign=\"end\">\r\n                <mat-chip *ngIf=\"role.requiresNudity\" color=\"warn\" selected=\"true\">Requires Nudity</mat-chip>\r\n                <mat-chip *ngIf=\"role.transgender\" color=\"accent\" selected=\"true\">Transgendered</mat-chip>\r\n                <mat-chip *ngIf=\"role.roleGenderTypeId !== 0\" color=\"primary\" selected=\"true\">{{role.roleGenderType.name}}</mat-chip>\r\n                <mat-chip *ngIf=\"role.roleTypeId !== 0\" color=\"primary\" selected=\"true\">{{role.roleType.name}}</mat-chip>\r\n            </mat-chip-list>-->\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.languages.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Languages:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let language of role.languages; last as islast\">{{language.language.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.accents.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Accents:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let accent of role.accents; last as islast\">{{accent.accent.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.ethnicities.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Ethicity:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let ethnicity of role.ethnicities; last as islast\">{{ethnicity.ethnicity.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.stereoTypes.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Stereotypes:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let stereoType of role.stereoTypes; last as islast\">{{stereoType.stereoType.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n            <div *ngIf=\"role.ethnicStereoTypes.length > 0\" class=\"p-grid p-justify-start\">\r\n                <div class=\"p-col-4\">\r\n                    Ethic Stereotypes:\r\n                </div>\r\n                <div class=\"p-col-8\">\r\n                    <span *ngFor=\"let ethnicStereoType of role.ethnicStereoTypes; last as islast\">{{ethnicStereoType.ethnicStereoType.name}}<span *ngIf=\"!islast\">, </span></span>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n\r\n    <p-footer>\r\n        <button pButton (click)=\"deleteRoleClick(role)\" class=\"ui-button-secondary\" label=\"Delete Role\"></button>\r\n        <button pButton (click)=\"editRoleClick(role)\" label=\"Edit Role\"></button>\r\n    </p-footer>\r\n</p-card>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/role-list/role-list.component.html":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/role-list/role-list.component.html ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("  <app-role-list-item *ngFor=\"let role of production.roles\" [role]=\"role\" [production]=\"production\">\r\n\r\n    </app-role-list-item>\r\n \r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.html":
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.html ***!
  \******************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-dialog header=\"Shoot Details\" #detaildialog\r\n   [focusOnShow]=\"false\" [responsive]=\"false\" styleClass=\"dialogsizer\" (onShow)=\"onDialogShow($event, detaildialog)\" (onHide)=\"onDialogHide($event, detaildialog)\" [(visible)]=\"isVisible\" [modal]=\"true\" [minY]=\"70\"\r\n  [maximizable]=\"true\" [baseZIndex]=\"10000\">\r\n  <p-card header=\"Enter the shoot details\">\r\n    <!--[breakpoint]=\"640\" [responsive]=\"true\" [style]=\"{width: '80%', minWidth: '640px'}\"-->\r\n\r\n    <form [formGroup]=\"basicDialogInfo\" #formDirective=\"ngForm\">\r\n      <div class=\"p-grid p-dir-col p-justify-around\">\r\n      <div class=\"p-col\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n\r\n          <label class=\"p-col\">Internal Name (for your use only)</label>\r\n          <input class=\"p-col\" #name pInputText placeholder=\"Internal Name\" formControlName=\"name\" required>\r\n          <p-message class=\"p-col\" severity=\"error\" text=\"Internal Name is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['name'].hasError('required') && basicDialogInfo.controls['name'].dirty\"></p-message>\r\n        </div>\r\n      </div>\r\n      <div class=\"p-col\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n              <label class=\"p-col\">Shoot Location</label>\r\n            <input class=\"p-col\" pInputText  #location placeholder=\"Location name\" formControlName=\"location\" required>\r\n             <p-message styleClass=\"p-col\" severity=\"error\" text=\"Location name is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['location'].hasError('required') && basicDialogInfo.controls['location'].dirty\"></p-message>\r\n          </div>\r\n      </div>\r\n      <div class=\"p-col\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n          <label class=\"p-col\">Location Details</label>\r\n          <textarea class=\"p-col\" rows=\"6\" #locationDetail pInputTextarea placeholder=\"Location details\" formControlName=\"locationDetail\"></textarea>\r\n       </div>\r\n      </div>\r\n      <div class=\"p-col\">\r\n        <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n          <label class=\"p-col\">Shoot Details</label>\r\n          <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Audition Details\" formControlName=\"details\"></textarea>\r\n        </div>\r\n      </div>\r\n        <div class=\"p-col\">\r\n          <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n            <label class=\"p-col\">Special Instructions</label>\r\n            <textarea class=\"p-col\" rows=\"3\" pInputTextarea placeholder=\"Special instructions\" formControlName=\"instructions\"></textarea>\r\n          </div>\r\n        </div>\r\n        <div class=\"p-col\">\r\n          <div class=\"p-grid p-align-start switchoffset\">\r\n            <p-inputSwitch styleClass=\"p-col\" formControlName=\"datesToBeDecidedLater\" (onChange)=\"clickDateDecisionSwitch($event)\"></p-inputSwitch>\r\n            <label class=\"p-col\">Audition dates will be decided at a later time</label>\r\n          </div>\r\n        </div>\r\n\r\n\r\n      <div class=\"p-col\">\r\n          <div class=\"p-grid p-align-start\">\r\n\r\n            <div class=\"p-col-12 p-md-6\">\r\n                <div class=\"p-grid p-dir-col\">\r\n                    <label class=\"p-col\">Shoot Start Date</label>\r\n                    <p-calendar [inputStyleClass]=\"p-col\" #startDate required formControlName=\"startDate\" (onSelect)=\"selectStartDate($event)\" dataType=\"string\" placeholder=\"Start Date\" dateFormat=\"DD MM d yy\" [minDate]=\"minDate\" [showIcon]=\"true\" [readonlyInput]=\"true\"></p-calendar>\r\n                    <p-message styleClass=\"p-col\" severity=\"error\" text=\"Start Date is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['startDate'].hasError('required') && basicDialogInfo.controls['startDate'].dirty\"></p-message>\r\n                </div>\r\n            </div>\r\n            <div class=\"p-col-12 p-md-6\">\r\n              <div class=\"p-grid p-align-start switchoffset\">\r\n                <p-inputSwitch styleClass=\"p-col\" formControlName=\"isOneDayOnly\" (onChange)=\"clickOneDaySwitch($event)\"></p-inputSwitch>\r\n                <label class=\"p-col\">This is a 1 day only shoot</label>\r\n              </div>\r\n              </div>\r\n          </div>\r\n        </div>\r\n      <div class=\"p-col\">\r\n          <div class=\"p-grid p-dir-col p-justify-between\">\r\n              <label class=\"p-col\">Shoot End Date</label>\r\n              <p-calendar [inputStyleClass]=\"p-col\" #endDate formControlName=\"endDate\" required dateFormat=\"DD MM d yy\" (onSelect)=\"selectEndDate($event)\" [minDate]=\"minDate\" placeholder=\"End Date\" [showIcon]=\"true\" [readonlyInput]=\"true\"></p-calendar>\r\n              <p-message styleClass=\"p-col\" severity=\"error\" text=\"End Date is <strong>required</strong> unless this is a 1 day only event.\" *ngIf=\"basicDialogInfo.controls['endDate'].hasError('required') && basicDialogInfo.controls['endDate'].dirty\"></p-message>\r\n          </div>\r\n      </div>\r\n        <div class=\"p-col\">\r\n          <div class=\"p-grid p-align-start switchoffset\">\r\n            <p-inputSwitch styleClass=\"p-col\" formControlName=\"datesAreTemptative\"></p-inputSwitch>\r\n            <label class=\"p-col\">Audition dates are temptative</label>\r\n          </div>\r\n        </div>\r\n\r\n\r\n      </div>\r\n    </form>\r\n    <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n  </p-card>\r\n  <p-footer>\r\n    <button type=\"button\" [disabled]=\"saveIsDisabled\" pButton icon=\"pi pi-check\" (click)=\"saveClick()\" label=\"Save\"></button>\r\n    <button type=\"button\" [disabled]=\"cancelIsDisabled\" pButton icon=\"pi pi-close\" (click)=\"cancelClick()\" label=\"Cancel\" class=\"ui-button-secondary\"></button>\r\n  </p-footer>\r\n</p-dialog>\r\n\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.html":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.html ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-card>\r\n  <div class=\"p-grid p-dir-col\">\r\n    <div class=\"p-col\">\r\n      <a (click)=\"viewDetails()\">{{detail.name}}</a>\r\n    </div>\r\n    <div class=\"p-col\">\r\n        <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n            <div class=\"p-col labeler\">Location</div>\r\n            <div class=\"p-col infodata\">{{detail.location}}</div>\r\n            <div *ngIf=\"detail.locationDetail && detail.locationDetail.length > 0\" class=\"p-col\">\r\n              <a class=\"linktoggler\" (click)=\"locationDetail.toggle($event)\">Location details..</a>\r\n            </div>\r\n\r\n            <p-overlayPanel #locationDetail>\r\n                <pre>{{detail.locationDetail | linebreak}}</pre>\r\n            </p-overlayPanel>\r\n        </div>\r\n    </div>\r\n    <div class=\"p-col\" *ngIf=\"detail.details && detail.details.length > 0\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n        <div class=\"p-col\">   \r\n          <a class=\"linktoggler\" (click)=\"details.toggle($event)\">Shoot details..</a>\r\n        </div>\r\n        <p-overlayPanel #details>\r\n          <pre>{{detail.details | linebreak}}</pre>\r\n        </p-overlayPanel>\r\n      </div>\r\n    </div>\r\n    <div class=\"p-col\" *ngIf=\"detail.instructions && detail.instructions.length > 0\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n        <div class=\"p-col\">   \r\n          <a class=\"linktoggler\" (click)=\"instructions.toggle($event)\">Special instructions..</a>\r\n        </div>\r\n        <p-overlayPanel #instructions>\r\n          <pre>{{detail.instructions | linebreak}}</pre>\r\n        </p-overlayPanel>\r\n      </div>\r\n    </div>\r\n    <div class=\"p-col\">\r\n      <div class=\"p-grid p-dir-col p-align-start p-nogutter\">\r\n          <div class=\"p-col labeler\" *ngIf=\"detail.isOneDayOnly\">Event Date</div>\r\n        <div class=\"p-col labeler\" *ngIf=\"!detail.isOneDayOnly\">Start Date</div>\r\n        <div *ngIf=\"!detail.datesToBeDecidedLater\" class=\"p-col infodata\">{{detail.startDate | date}}</div>\r\n        <div class=\"p-col infodata\" *ngIf=\"detail.datesToBeDecidedLater\">To be decided later</div>\r\n        <div class=\"p-col hotinfodata\" *ngIf=\"detail.isOneDayOnly\">This is a one day only event</div>\r\n        <div class=\"p-col labeler\" *ngIf=\"!detail.isOneDayOnly\">End Date</div>\r\n        <div class=\"p-col infodata\" *ngIf=\"!detail.datesToBeDecidedLater && !detail.isOneDayOnly\">{{detail.endDate | date}}</div>\r\n        <div class=\"p-col infodata\" *ngIf=\"detail.datesToBeDecidedLater && !detail.isOneDayOnly\">To be decided later</div>\r\n        <div class=\"p-col hotinfodata\" *ngIf=\"detail.datesAreTemptative\">Dates are tentative</div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <p-footer>\r\n    <a class=\"pi pi-pencil\" (click)=\"viewDetails()\"></a>\r\n    <a class=\"pi pi-trash\" (click)=\"deleteDetails()\"></a>\r\n  </p-footer>\r\n</p-card>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.html":
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.html ***!
  \**************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col p-justify-around\">\r\n  <app-shoot-details-list-item class=\"p-col\" *ngFor=\"let detail of shootDetails\" [detail]=\"detail\">\r\n\r\n  </app-shoot-details-list-item>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.html":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.html ***!
  \**********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div fxLayout=\"column\" fxLayoutAlign=\"start stretch\">\r\n  <button mat-raised-button (click)=\"scrollToBasic()\">go Basic</button>\r\n  <button mat-raised-button (click)=\"scrollToRoles()\">go Roles</button>\r\n  <button mat-raised-button (click)=\"scrollToCasting()\">go Casting</button>\r\n  <button mat-raised-button (click)=\"scrollToProduction()\">go Production</button>\r\n  <button mat-raised-button (click)=\"scrollToArtistic()\">go Artistic</button>\r\n  <button mat-raised-button (click)=\"scrollToOther()\">go Other</button>\r\n  <button mat-raised-button (click)=\"saveProductionClick()\">Save</button>\r\n  <button mat-raised-button (click)=\"deleteProductionClick()\">Delete</button>\r\n  <button mat-raised-button (click)=\"cancelEditProductionClick()\">Cancel</button>\r\n\r\n     \r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.html":
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.html ***!
  \**************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<p-dialog header=\"Sides\" #detaildialog\r\n          [focusOnShow]=\"false\" [responsive]=\"false\" styleClass=\"dialogsizer\" (onShow)=\"onDialogShow($event, detaildialog)\" (onHide)=\"onDialogHide($event, detaildialog)\" \r\n          [(visible)]=\"isVisible\" [modal]=\"true\" [minY]=\"70\"\r\n          [maximizable]=\"true\" [baseZIndex]=\"10000\">\r\n    <p-card header=\"Enter the pay details\">\r\n        <!--[breakpoint]=\"640\" [responsive]=\"true\" [style]=\"{width: '80%', minWidth: '640px'}\"-->\r\n        <form [formGroup]=\"basicDialogInfo\" #formDirective=\"ngForm\">\r\n            <input type=\"file\"\r\n                   #file\r\n                   style=\"display: none\"\r\n                   (change)=\"onFilesAdded()\"\r\n                   multiple />\r\n            <div class=\"p-grid p-dir-col p-justify-around\">\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Internal Name (for your use only)</label>\r\n                        <input [disabled]=\"nameIsDisable\" class=\"p-col\" #name pInputText placeholder=\"Internal Name\" formControlName=\"name\" required>\r\n                        <p-message class=\"p-col\" severity=\"error\" text=\"Internal Name is <strong>required</strong>\" *ngIf=\"basicDialogInfo.controls['name'].hasError('required') && basicDialogInfo.controls['name'].dirty\"></p-message>\r\n                    </div>\r\n                </div>\r\n                <div class=\"p-col\">\r\n                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                        <label class=\"p-col\">Side Document File (pdf file)</label>\r\n                        <div class=\"p-col\">\r\n                            <div class=\"p-grid p-dir-col p-justify-between\">\r\n\r\n                                <div *ngFor=\"let fileUploadState of sideFiles\" class=\"p-col files-list\">\r\n                                    <div class=\"p-grid p-dir-col p-justify-between\">\r\n                                        <div class=\"p-col\">\r\n                                            <div class=\"p-grid p-justify-center p-align-center\">\r\n                                                <div class=\"p-col\">\r\n                                                    {{fileUploadState.file.name}}({{fileUploadState.totalSize | bytes}})\r\n                                                </div>\r\n                                                <button [disabled]=\"removeFileIsDisabled\" class=\"p-col-fixed\" style=\"width:auto\" pButton (click)=\"remove(fileUploadState)\" icon=\"pi pi-trash\"></button>\r\n                                            </div>\r\n                                        </div>\r\n                                        <p-progressBar [value]=\"fileUploadState.percentageUploaded\" *ngIf=\"fileUploadState.isUploading || fileUploadState.isComplete\"></p-progressBar>\r\n                                        <div class=\"p-col\">\r\n                                            <span class=\"p-col\" *ngIf=\"fileUploadState.isUploading || fileUploadState.isComplete\">{{fileUploadState.percentageUploaded}}% </span><span *ngIf=\"fileUploadState.isUploading || fileUploadState.isComplete\"> {{fileUploadState.uploadedSize | bytes}} of {{fileUploadState.totalSize | bytes}}</span>\r\n                                        </div>\r\n\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div [hidden]=\"dropPanelIsHiden\" class=\"p-col uploadfilecontainer\" id=\"drop_zone\" appDropFile (click)=\"addFiles()\" (onFileDropped)=\"onFilesDropped($event)\">\r\n                                <strong>Drag one or more files to this Drop Zone ...</strong>\r\n                            </div>\r\n                            <button class=\"p-col-fixed\" style=\"width:120px\" type=\"button\" [disabled]=\"selectIsDisabled\" pButton icon=\"pi pi-plus\" (click)=\"addFiles()\" label=\"Select file\"></button>\r\n\r\n                        </div>\r\n                    </div>\r\n\r\n                </div>\r\n            </div>\r\n        </form>\r\n        <p-messages [(value)]=\"messages\"></p-messages>\r\n\r\n    </p-card>\r\n    <p-footer>\r\n        <button type=\"button\" [disabled]=\"saveIsDisabled\" pButton icon=\"pi pi-check\" (click)=\"saveClick()\" label=\"Upload and Save\"></button>\r\n        <button type=\"button\" [disabled]=\"cancelIsDisabled\" pButton icon=\"pi pi-close\" (click)=\"cancelClick()\" label=\"Cancel\" class=\"ui-button-secondary\"></button>\r\n    </p-footer>\r\n</p-dialog>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.html":
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.html ***!
  \********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <p-card>\r\n    <div class=\"p-grid p-dir-col\">\r\n      <div class=\"p-col\">\r\n          <a (click)=\"viewDetails()\">{{detail.name}}</a>\r\n   </div>\r\n    </div>\r\n    <p-footer>\r\n      <a class=\"pi pi-pencil\" (click)=\"viewDetails()\"></a>\r\n      <a class=\"pi pi-trash\" (click)=\"deleteDetails()\"></a>\r\n    </p-footer>\r\n\r\n  </p-card>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-list/sides-list.component.html":
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-list/sides-list.component.html ***!
  \**********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"p-grid p-dir-col p-justify-around\">\r\n    <app-sides-list-item class=\"p-col\" *ngFor=\"let detail of sides\" [detail]=\"detail\">\r\n    </app-sides-list-item>\r\n</div>");

/***/ }),

/***/ "./src/app/feature-modules/production/components/add-role/add-role.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/add-role/add-role.component.scss ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvYWRkLXJvbGUvYWRkLXJvbGUuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/add-role/add-role.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/add-role/add-role.component.ts ***!
  \**************************************************************************************/
/*! exports provided: AddRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddRoleComponent", function() { return AddRoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AddRoleComponent = /** @class */ (function () {
    function AddRoleComponent() {
    }
    AddRoleComponent.prototype.ngOnInit = function () {
    };
    AddRoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-add-role',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./add-role.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/add-role/add-role.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./add-role.component.scss */ "./src/app/feature-modules/production/components/add-role/add-role.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AddRoleComponent);
    return AddRoleComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.scss":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.scss ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvYXVkaXRpb24tZGV0YWlscy1kaWFsb2cvYXVkaXRpb24tZGV0YWlscy1kaWFsb2cuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.ts":
/*!********************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.ts ***!
  \********************************************************************************************************************/
/*! exports provided: AuditionDetailsDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuditionDetailsDialogComponent", function() { return AuditionDetailsDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm5/primeng-dialog.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/fesm5/primeng-radiobutton.js");










var AuditionDetailsDialogComponent = /** @class */ (function () {
    function AuditionDetailsDialogComponent(productionDialogService, productionExtraService, _formBuilder, mediaMatcher, messageDispatchService) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this._formBuilder = _formBuilder;
        this.mediaMatcher = mediaMatcher;
        this.messageDispatchService = messageDispatchService;
        this.isSmallDevice = false;
        this.messages = [];
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.isOneDayOnly = false;
        this.datesToBeDecidedLater = false;
        this.isVisible = false;
        this.auditionTypeEnum = _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"];
        this.isInPersonAudition = true;
    }
    AuditionDetailsDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSmallDevice = window.innerWidth < 768;
        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));
        var today = new Date();
        var month = today.getMonth();
        var year = today.getFullYear();
        this.minDate = new Date();
        this.minDate.setDate(today.getDate() + 1);
        this.maxDate = new Date();
        this.maxDate.setDate(today.getDate() + 1);
        this.basicDialogInfo = this._formBuilder.group({
            location: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            locationDetail: [''],
            isOneDayOnly: [false],
            details: [''],
            instructions: [''],
            startDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            endDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            auditionType: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            onlyThoseSelectedWillParticipate: [false],
            datesToBeDecidedLater: [false],
            datesAreTemptative: [false]
        }, { updateOn: 'blur' });
        // this.basicDialogInfo.patchValue({
        //     isOneDayOnly: false,
        //  });
        //  this.basicDialogInfo.controls['endDate'].enable();
        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToAuditionDetailsDialogShowStateObs()
            .subscribe(function (dialogObject) {
            _this.clearMessages();
            _this.formDirective.resetForm();
            _this.basicDialogInfo.reset();
            if (dialogObject.isVisible && dialogObject.auditionDetail != null) {
                _this.auditionDetail = dialogObject.auditionDetail;
                _this.basicDialogInfo.patchValue({
                    location: _this.auditionDetail.location,
                    locationDetail: _this.auditionDetail.locationDetail,
                    isOneDayOnly: _this.auditionDetail.isOneDayOnly,
                    details: _this.auditionDetail.details,
                    instructions: _this.auditionDetail.instructions,
                    startDate: new Date(_this.auditionDetail.startDate),
                    endDate: new Date(_this.auditionDetail.endDate),
                    name: _this.auditionDetail.name,
                    auditionType: _this.auditionDetail.auditionTypeEnum,
                    onlyThoseSelectedWillParticipate: _this.auditionDetail.onlyThoseSelectedWillParticipate,
                    datesToBeDecidedLater: _this.auditionDetail.datesToBeDecidedLater,
                    datesAreTemptative: _this.auditionDetail.datesAreTemptative
                });
                _this.auditionTypeChange(_this.auditionDetail.auditionTypeEnum);
                //check if dates disabled for later
                if (_this.auditionDetail.auditionTypeEnum != _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].TapedSubmissionsOnly) {
                    if (_this.auditionDetail.datesToBeDecidedLater) {
                        _this.dateDecidionChange(_this.auditionDetail.datesToBeDecidedLater);
                    }
                    else {
                        //check if one day only
                        if (_this.auditionDetail.isOneDayOnly) {
                            _this.oneDayAuditionChange(_this.auditionDetail.isOneDayOnly);
                        }
                        else {
                            _this.basicDialogInfo.controls['endDate'].enable();
                            _this.basicDialogInfo.controls['startDate'].enable();
                        }
                    }
                }
            }
            else {
                _this.auditionDetail = null;
                _this.basicDialogInfo.patchValue({
                    location: '',
                    locationDetail: '',
                    isOneDayOnly: false,
                    details: '',
                    instructions: '',
                    startDate: '',
                    endDate: '',
                    name: '',
                    auditionType: _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].InPersonAuditionOnly,
                    onlyThoseSelectedWillParticipate: false,
                    datesToBeDecidedLater: false,
                    datesAreTemptative: false
                });
                _this.auditionTypeChange(_models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].InPersonAuditionOnly);
                _this.dateDecidionChange(false);
                _this.oneDayAuditionChange(false);
                //this.basicDialogInfo.controls['endDate'].enable();
            }
            _this.isVisible = dialogObject.isVisible;
            // this.detaildialog.maximized = true;
            //this.detaildialog.toggleMaximize();
            _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            _this.nameField.nativeElement.focus();
        });
    };
    AuditionDetailsDialogComponent.prototype.ngAfterViewInit = function () {
    };
    AuditionDetailsDialogComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();
        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);
    };
    AuditionDetailsDialogComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    AuditionDetailsDialogComponent.prototype.mediaQueryBigChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = false;
        }
    };
    AuditionDetailsDialogComponent.prototype.mediaQuerySmallChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = true;
        }
    };
    AuditionDetailsDialogComponent.prototype.selectStartDate = function (date) {
        this.basicDialogInfo.patchValue({
            startDate: date,
        });
    };
    AuditionDetailsDialogComponent.prototype.selectEndDate = function (date) {
        this.basicDialogInfo.patchValue({
            endDate: date,
        });
    };
    AuditionDetailsDialogComponent.prototype.auditionTypeOptionClick = function (auditionType) {
        this.auditionTypeChange(auditionType);
    };
    AuditionDetailsDialogComponent.prototype.auditionTypeChange = function (auditionType) {
        var isInPersonAudition;
        switch (auditionType) {
            case _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].InPersonAuditionOnly:
            case _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].TapedSubmissionAndInPersonAudition:
            case _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].TapedSubmissionThenInPersonAudition:
                isInPersonAudition = true;
                break;
            case _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionTypeEnum"].TapedSubmissionsOnly:
                isInPersonAudition = false;
                break;
        }
        if (this.isInPersonAudition != isInPersonAudition) {
            this.isInPersonAudition = isInPersonAudition;
            if (this.isInPersonAudition) {
                this.basicDialogInfo.controls['endDate'].enable();
                this.basicDialogInfo.controls['startDate'].enable();
                this.basicDialogInfo.controls['location'].enable();
            }
            else {
                this.basicDialogInfo.controls['endDate'].disable();
                this.basicDialogInfo.controls['startDate'].disable();
                this.basicDialogInfo.controls['location'].disable();
                this.basicDialogInfo.patchValue({
                    location: '',
                    locationDetail: '',
                    startDate: '',
                    endDate: '',
                    datesToBeDecidedLater: false,
                    datesAreTemptative: false
                });
                // this.basicDialogInfo.controls['location'].setValidators([]);
                // this.basicDialogInfo.controls['location'].updateValueAndValidity();
                //   this.basicDialogInfo.get('title').setValidators([]); // or clearValidators()
                //  this.basicDialogInfo.get('title').updateValueAndValidity();
            }
        }
    };
    AuditionDetailsDialogComponent.prototype.oneDayAuditionChange = function (value) {
        this.isOneDayOnly = value;
        if (value) {
            this.basicDialogInfo.controls['endDate'].disable();
        }
        else {
            if (!this.datesToBeDecidedLater) {
                this.basicDialogInfo.controls['endDate'].enable();
            }
        }
        this.basicDialogInfo.patchValue({
            endDate: '',
        });
    };
    AuditionDetailsDialogComponent.prototype.clickOneDaySwitch = function (selection) {
        this.oneDayAuditionChange(selection.checked);
    };
    AuditionDetailsDialogComponent.prototype.dateDecidionChange = function (value) {
        this.datesToBeDecidedLater = value;
        if (value) {
            this.basicDialogInfo.controls['endDate'].disable();
            this.basicDialogInfo.controls['startDate'].disable();
        }
        else {
            this.basicDialogInfo.controls['endDate'].enable();
            if (!this.isOneDayOnly) {
                this.basicDialogInfo.controls['startDate'].enable();
            }
        }
        this.basicDialogInfo.patchValue({
            endDate: '',
        });
        this.basicDialogInfo.patchValue({
            startDate: '',
        });
    };
    AuditionDetailsDialogComponent.prototype.clickDateDecisionSwitch = function (selection) {
        this.dateDecidionChange(selection.checked);
    };
    AuditionDetailsDialogComponent.prototype.onDialogShow = function (event, dialog) {
        if (this.isSmallDevice) {
            setTimeout(function () {
                dialog.maximize();
            }, 0);
        }
    };
    AuditionDetailsDialogComponent.prototype.onDialogHide = function (event, dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();
    };
    AuditionDetailsDialogComponent.prototype.cancelClick = function () {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
    };
    AuditionDetailsDialogComponent.prototype.resetDialog = function () {
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.messages = [];
        this.isInPersonAudition = true;
    };
    AuditionDetailsDialogComponent.prototype.saveClick = function () {
        var _this = this;
        this.clearMessages();
        if (this.basicDialogInfo.valid) {
            this.saveIsDisabled = true;
            this.cancelIsDisabled = true;
            var tauditionDetail_1 = new _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionDetail"];
            tauditionDetail_1.productionId = this.productionId;
            tauditionDetail_1.name = this.basicDialogInfo.get('name').value;
            tauditionDetail_1.auditionTypeEnum = this.basicDialogInfo.get('auditionType').value;
            tauditionDetail_1.location = this.basicDialogInfo.get('location').value;
            tauditionDetail_1.locationDetail = this.basicDialogInfo.get('locationDetail').value;
            tauditionDetail_1.details = this.basicDialogInfo.get('details').value;
            tauditionDetail_1.instructions = this.basicDialogInfo.get('instructions').value;
            tauditionDetail_1.startDate = this.basicDialogInfo.get('startDate').value;
            tauditionDetail_1.endDate = this.basicDialogInfo.get('endDate').value;
            tauditionDetail_1.isOneDayOnly = this.basicDialogInfo.get('isOneDayOnly').value;
            tauditionDetail_1.onlyThoseSelectedWillParticipate = this.basicDialogInfo.get('onlyThoseSelectedWillParticipate').value;
            tauditionDetail_1.datesToBeDecidedLater = this.basicDialogInfo.get('datesToBeDecidedLater').value;
            tauditionDetail_1.datesAreTemptative = this.basicDialogInfo.get('datesAreTemptative').value;
            if (this.auditionDetail && this.auditionDetail.id && this.auditionDetail.id !== 0) {
                this.productionExtraService.saveAuditionDetail(this.auditionDetail.id, this.productionId, tauditionDetail_1)
                    .subscribe(function (auditionDetail) {
                    //this.auditionDetail = auditionDetail;
                    _this.formDirective.resetForm();
                    _this.basicDialogInfo.reset();
                    _this.isVisible = false;
                    _this.messageDispatchService.dispatchSuccesMessage("Audition detail '" + auditionDetail.name + "' was saved successfully", "Audition Detail Saved");
                }, function (error) {
                    _this.saveIsDisabled = false;
                    _this.cancelIsDisabled = false;
                    _this.messageDispatchService.dispatchErrorMessage( true
                        ? (error.error)
                            ? (error.error.exceptionMessage)
                                ? error.error.exceptionMessage
                                : error.error
                            : error
                        : undefined, "Audition Detail Save Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Audition Detail Save Error",
                        detail:  true
                            ? (error.error)
                                ? (error.error.exceptionMessage)
                                    ? error.error.exceptionMessage
                                    : error.error
                                : error
                            : undefined
                    });
                });
            }
            else {
                this.productionExtraService.createAuditionDetail(this.productionId, tauditionDetail_1).subscribe(function (auditionDetail) {
                    if (auditionDetail != null) {
                        _this.formDirective.resetForm();
                        _this.basicDialogInfo.reset();
                        _this.isVisible = false;
                        _this.messageDispatchService.dispatchSuccesMessage("New audition detail '" + auditionDetail.name + "' was created successfully", "Audition Detail Created");
                    }
                }, function (error) {
                    _this.saveIsDisabled = false;
                    _this.cancelIsDisabled = false;
                    _this.messageDispatchService.dispatchErrorMessage( true
                        ? (error.error)
                            ? (error.error.exceptionMessage)
                                ? error.error.exceptionMessage
                                : error.error
                            : error
                        : undefined, "Audition Detail Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Audition Detail Creation Error",
                        detail:  true
                            ? (error.error)
                                ? (error.error.exceptionMessage)
                                    ? error.error.exceptionMessage
                                    : error.error
                                : error
                            : undefined
                    });
                });
            }
        }
        else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('auditionType').valid) {
                this.basicDialogInfo.get('auditionType').markAsDirty();
                this.auditionTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.auditionTypeField.inputViewChild.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('location').valid) {
                this.basicDialogInfo.get('location').markAsDirty();
                this.locationField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.locationField.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('startDate').valid) {
                this.basicDialogInfo.get('startDate').markAsDirty();
                this.startDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.startDateField.inputfieldViewChild.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('endDate').valid) {
                this.basicDialogInfo.get('endDate').markAsDirty();
                this.endDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.endDateField.inputfieldViewChild.nativeElement.focus();
            }
        }
    };
    AuditionDetailsDialogComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_5__["AuditionDetail"])
    ], AuditionDetailsDialogComponent.prototype, "auditionDetail", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], AuditionDetailsDialogComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formDirective', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"])
    ], AuditionDetailsDialogComponent.prototype, "formDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], AuditionDetailsDialogComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('location', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], AuditionDetailsDialogComponent.prototype, "locationField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('locationDetail', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], AuditionDetailsDialogComponent.prototype, "locationDetailField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('startDate', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AuditionDetailsDialogComponent.prototype, "startDateField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('endDate', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AuditionDetailsDialogComponent.prototype, "endDateField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('detaildialog', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dialog__WEBPACK_IMPORTED_MODULE_7__["Dialog"])
    ], AuditionDetailsDialogComponent.prototype, "detaildialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('auditionType', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__["RadioButton"])
    ], AuditionDetailsDialogComponent.prototype, "auditionTypeField", void 0);
    AuditionDetailsDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-audition-details-dialog',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./audition-details-dialog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./audition-details-dialog.component.scss */ "./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"]])
    ], AuditionDetailsDialogComponent);
    return AuditionDetailsDialogComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.scss":
/*!****************************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.scss ***!
  \****************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvYXVkaXRpb24tZGV0YWlscy1saXN0LWl0ZW0vYXVkaXRpb24tZGV0YWlscy1saXN0LWl0ZW0uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.ts":
/*!**************************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.ts ***!
  \**************************************************************************************************************************/
/*! exports provided: AuditionDetailsListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuditionDetailsListItemComponent", function() { return AuditionDetailsListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");
/* harmony import */ var _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/confirm-decision-subscription.service */ "./src/app/services/confirm-decision-subscription.service.ts");








var AuditionDetailsListItemComponent = /** @class */ (function () {
    function AuditionDetailsListItemComponent(productionDialogService, productionExtraService, messageDispatchService, messageService, decisionDispatcher) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
        this.messageService = messageService;
        this.decisionDispatcher = decisionDispatcher;
        this.auditionTypeEnum = _models_production_models__WEBPACK_IMPORTED_MODULE_2__["AuditionTypeEnum"];
    }
    AuditionDetailsListItemComponent.prototype.ngOnInit = function () {
    };
    AuditionDetailsListItemComponent.prototype.ngOnDestroy = function () {
        if (this.decisionDispatch) {
            this.decisionDispatch.unsubscribe();
        }
    };
    AuditionDetailsListItemComponent.prototype.viewDetails = function () {
        this.productionDialogService.showAuditionDetailsDialog(this.detail);
    };
    AuditionDetailsListItemComponent.prototype.deleteDetails = function () {
        var _this = this;
        this.decisionDispatcher.poseConfirmDecision('Delete audition detail ' + this.detail.name + '?', 'Confirm to proceed');
        this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
            .subscribe(function (decision) {
            _this.decisionDispatch.unsubscribe();
            if (decision) {
                _this.productionExtraService.deleteAuditionDetail(_this.detail.id, _this.detail.productionId).subscribe(function (shootDetail) {
                    //this.shootDetail = shootDetail;
                    _this.messageDispatchService.dispatchSuccesMessage("Audition detail '" + _this.detail.name + "' was deleted successfully", "Audition Detail Deleted");
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Audition Detail Save Error");
                });
            }
        });
    };
    AuditionDetailsListItemComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"] },
        { type: primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"] },
        { type: _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["AuditionDetail"])
    ], AuditionDetailsListItemComponent.prototype, "detail", void 0);
    AuditionDetailsListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-audition-details-list-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./audition-details-list-item.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./audition-details-list-item.component.scss */ "./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"],
            primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
            _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"]])
    ], AuditionDetailsListItemComponent);
    return AuditionDetailsListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.scss":
/*!******************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.scss ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvYXVkaXRpb24tZGV0YWlscy1saXN0L2F1ZGl0aW9uLWRldGFpbHMtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: AuditionDetailsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuditionDetailsListComponent", function() { return AuditionDetailsListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");




var AuditionDetailsListComponent = /** @class */ (function () {
    function AuditionDetailsListComponent(productionExtraService, messageDispatchService) {
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
    }
    AuditionDetailsListComponent.prototype.ngOnInit = function () {
        this.editAuditionDetailSubscription = this.productionExtraService.subscribeToEditAuditionDetailObs()
            .subscribe(function (detail) {
        });
        this.newAuditionDetailSubscription = this.productionExtraService.subscribeToCreateAuditionDetailObs()
            .subscribe(function (detail) {
        });
        this.deleteAuditionDetailSubscription = this.productionExtraService.subscribeToDeleteAuditionDetailObs()
            .subscribe(function (detail) {
            if (detail) {
            }
        });
    };
    AuditionDetailsListComponent.prototype.ngOnDestroy = function () {
        if (this.editAuditionDetailSubscription) {
            this.editAuditionDetailSubscription.unsubscribe();
        }
        if (this.newAuditionDetailSubscription) {
            this.newAuditionDetailSubscription.unsubscribe();
        }
        if (this.deleteAuditionDetailSubscription) {
            this.deleteAuditionDetailSubscription.unsubscribe();
        }
    };
    AuditionDetailsListComponent.ctorParameters = function () { return [
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], AuditionDetailsListComponent.prototype, "auditionDetails", void 0);
    AuditionDetailsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-audition-details-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./audition-details-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./audition-details-list.component.scss */ "./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"]])
    ], AuditionDetailsListComponent);
    return AuditionDetailsListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/new-production/new-production.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/new-production/new-production.component.scss ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* Absolute Center Spinner */\n/*\n@import '~@angular/material/prebuilt-themes/deeppurple-amber.css';\n@mixin primary-color($theme) {\n\n  $primary: map-get($theme, primary);\n\n  .mat-card-header, mat-card-header {\n        background: mat-color($primary) !important;\n       }\n\n}*/\n/*\n@import '../../../../../_variables.scss';\n*/\n:host {\n  height: 100%;\n}\n.production_card {\n  margin-left: 16px;\n  margin-right: 16px;\n}\n/*\nmat-card-header {\n  background-color: $primary;\n}*/\nmat-form-field, .mat-form-field {\n  width: 100%;\n}\n.my-form-field {\n  /*margin-bottom:24px;*/\n}\n.mokformfield {\n  width: 100%;\n  padding-bottom: 1.34375em;\n}\n.mokformfield mat-label, .mokformfield mat-radion-group {\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9uZXctcHJvZHVjdGlvbi9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcZmVhdHVyZS1tb2R1bGVzXFxwcm9kdWN0aW9uXFxjb21wb25lbnRzXFxuZXctcHJvZHVjdGlvblxcbmV3LXByb2R1Y3Rpb24uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXByb2R1Y3Rpb24vbmV3LXByb2R1Y3Rpb24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNEJBQUE7QUFJQTs7Ozs7Ozs7OztFQUFBO0FBV0E7O0NBQUE7QUFHQTtFQUNFLFlBQUE7QUNGRjtBRE1BO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQ0hGO0FES0E7OztFQUFBO0FBSUE7RUFFRSxXQUFBO0FDSEY7QURLQTtFQUNFLHNCQUFBO0FDRkY7QURJQTtFQUVFLFdBQUE7RUFDQSx5QkFBQTtBQ0ZGO0FESUE7RUFFQyxjQUFBO0FDRkQiLCJmaWxlIjoic3JjL2FwcC9mZWF0dXJlLW1vZHVsZXMvcHJvZHVjdGlvbi9jb21wb25lbnRzL25ldy1wcm9kdWN0aW9uL25ldy1wcm9kdWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogQWJzb2x1dGUgQ2VudGVyIFNwaW5uZXIgKi9cclxuXHJcblxyXG5cclxuLypcclxuQGltcG9ydCAnfkBhbmd1bGFyL21hdGVyaWFsL3ByZWJ1aWx0LXRoZW1lcy9kZWVwcHVycGxlLWFtYmVyLmNzcyc7XHJcbkBtaXhpbiBwcmltYXJ5LWNvbG9yKCR0aGVtZSkge1xyXG5cclxuICAkcHJpbWFyeTogbWFwLWdldCgkdGhlbWUsIHByaW1hcnkpO1xyXG5cclxuICAubWF0LWNhcmQtaGVhZGVyLCBtYXQtY2FyZC1oZWFkZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IG1hdC1jb2xvcigkcHJpbWFyeSkgIWltcG9ydGFudDtcclxuICAgICAgIH1cclxuXHJcbn0qL1xyXG4vKlxyXG5AaW1wb3J0ICcuLi8uLi8uLi8uLi8uLi9fdmFyaWFibGVzLnNjc3MnO1xyXG4qL1xyXG46aG9zdCB7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbn1cclxuXHJcblxyXG4ucHJvZHVjdGlvbl9jYXJkIHtcclxuICBtYXJnaW4tbGVmdDoxNnB4O1xyXG4gIG1hcmdpbi1yaWdodDoxNnB4O1xyXG59XHJcbi8qXHJcbm1hdC1jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XHJcbn0qL1xyXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkXHJcbntcclxuICB3aWR0aDoxMDAlO1xyXG59XHJcbi5teS1mb3JtLWZpZWxkIHtcclxuICAvKm1hcmdpbi1ib3R0b206MjRweDsqL1xyXG59XHJcbi5tb2tmb3JtZmllbGRcclxue1xyXG4gIHdpZHRoOjEwMCU7XHJcbiAgcGFkZGluZy1ib3R0b206MS4zNDM3NWVtO1xyXG59XHJcbi5tb2tmb3JtZmllbGQgbWF0LWxhYmVsLCAubW9rZm9ybWZpZWxkIG1hdC1yYWRpb24tZ3JvdXBcclxue1xyXG4gZGlzcGxheTpibG9jaztcclxufSIsIi8qIEFic29sdXRlIENlbnRlciBTcGlubmVyICovXG4vKlxuQGltcG9ydCAnfkBhbmd1bGFyL21hdGVyaWFsL3ByZWJ1aWx0LXRoZW1lcy9kZWVwcHVycGxlLWFtYmVyLmNzcyc7XG5AbWl4aW4gcHJpbWFyeS1jb2xvcigkdGhlbWUpIHtcblxuICAkcHJpbWFyeTogbWFwLWdldCgkdGhlbWUsIHByaW1hcnkpO1xuXG4gIC5tYXQtY2FyZC1oZWFkZXIsIG1hdC1jYXJkLWhlYWRlciB7XG4gICAgICAgIGJhY2tncm91bmQ6IG1hdC1jb2xvcigkcHJpbWFyeSkgIWltcG9ydGFudDtcbiAgICAgICB9XG5cbn0qL1xuLypcbkBpbXBvcnQgJy4uLy4uLy4uLy4uLy4uL192YXJpYWJsZXMuc2Nzcyc7XG4qL1xuOmhvc3Qge1xuICBoZWlnaHQ6IDEwMCU7XG59XG5cbi5wcm9kdWN0aW9uX2NhcmQge1xuICBtYXJnaW4tbGVmdDogMTZweDtcbiAgbWFyZ2luLXJpZ2h0OiAxNnB4O1xufVxuXG4vKlxubWF0LWNhcmQtaGVhZGVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XG59Ki9cbm1hdC1mb3JtLWZpZWxkLCAubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cblxuLm15LWZvcm0tZmllbGQge1xuICAvKm1hcmdpbi1ib3R0b206MjRweDsqL1xufVxuXG4ubW9rZm9ybWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctYm90dG9tOiAxLjM0Mzc1ZW07XG59XG5cbi5tb2tmb3JtZmllbGQgbWF0LWxhYmVsLCAubW9rZm9ybWZpZWxkIG1hdC1yYWRpb24tZ3JvdXAge1xuICBkaXNwbGF5OiBibG9jaztcbn0iXX0= */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/new-production/new-production.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/new-production/new-production.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: NewProductionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProductionComponent", function() { return NewProductionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/production-component.service */ "./src/app/feature-modules/production/services/production-component.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _animations_router_animation__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../../../animations/router.animation */ "./src/app/animations/router.animation.ts");












//import { NewRoleComponent } from '../new-role/new-role.component'

var NewProductionComponent = /** @class */ (function () {
    function NewProductionComponent(ProductionRoleService, _formBuilder, productionService, stateService, transitionService, messageDispatchService, transition, roleDialogService, productionComponentService) {
        this.ProductionRoleService = ProductionRoleService;
        this._formBuilder = _formBuilder;
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.messageDispatchService = messageDispatchService;
        this.transition = transition;
        this.roleDialogService = roleDialogService;
        this.productionComponentService = productionComponentService;
        this.compensationType = _models_production_models__WEBPACK_IMPORTED_MODULE_4__["CompensationTypeEnum"].notPaid;
        //private production: Production;
        this.showHints = false;
        this.productionTypes = [];
        this.messages = [];
    }
    NewProductionComponent.prototype.ngOnInit = function () {
        var _this = this;
        for (var i = 0; i < this.metadata.productionTypeCategories.length; i++) {
            var prodType = {
                label: this.metadata.productionTypeCategories[i].name,
                // value: this.metadata.productionTypeCategories[i].id,
                items: []
            };
            for (var x = 0; x < this.metadata.productionTypeCategories[i].productionTypes.length; x++) {
                prodType.items.push({
                    label: this.metadata.productionTypeCategories[i].productionTypes[x].name,
                    value: this.metadata.productionTypeCategories[i].productionTypes[x].id
                });
            }
            this.productionTypes.push(prodType);
        }
        this.deleteRoleSubscription = this.ProductionRoleService.subscribeToDeleteRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles.splice(_this.production.roles.indexOf(role), 1);
            }
        });
        this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles[_this.production.roles.indexOf(role)] = role;
            }
        });
        this.deleteProductionIntentSubscription = this.productionComponentService.subscribeToDeleteProductionIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToProductionsView();
            }
        });
        this.cancelEditProductionIntentSubscription = this.productionComponentService.subscribeToCancelEditProductionIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.moveToProductionsView();
            }
        });
        this.saveProductionIntentSubscription = this.productionComponentService.subscribeToSaveProductionIntentObs()
            .subscribe(function (value) {
            if (value) {
                _this.saveProductionClick();
            }
        });
        if (this.transition.params().popRole) {
            this.popRole = this.transition.params().popRole;
        }
        if (this.transition.params().isDraft) {
            this.isDraft = this.transition.params().isDraft;
        }
        if (this.transition.params().isEdit) {
            this.isEdit = this.transition.params().isEdit;
        }
        if (this.transition.params().production) {
            this.production = this.transition.params().production;
        }
        if (this.production) {
            this.basicProductionInfo = this._formBuilder.group({
                name: [this.production.name, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionTypeId: [this.production.productionTypeId, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionCompany: [this.production.productionCompany],
                description: [this.production.description, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                compensationType: [this.production.compensationType],
                compensationContractDetails: [this.production.compensationContractDetails],
                productionUnionStatus: [this.production.productionUnionStatus]
            }, { updateOn: 'blur' });
            this.castingDetails = this._formBuilder.group({
                castingDirector: [this.production.castingDirector],
                castingAssistant: [this.production.castingAssistant],
                castingAssociate: [this.production.castingAssociate],
                assistantCastingAssociate: [this.production.assistantCastingAssociate]
            }, { updateOn: 'blur' });
            this.productionDetails = this._formBuilder.group({
                executiveProducer: [this.production.executiveProducer],
                coExecutiveProducer: [this.production.coExecutiveProducer],
                producer: [this.production.producer],
                coProducer: [this.production.coProducer]
            }, { updateOn: 'blur' });
            this.artisticDetails = this._formBuilder.group({
                artisticDirector: [this.production.artisticDirector],
                musicDirector: [this.production.musicDirector],
                choreographer: [this.production.choreographer]
            }, { updateOn: 'blur' });
            this.extraDetails = this._formBuilder.group({
                TVNetwork: [this.production.tVNetwork],
                venue: [this.production.venue],
            }, { updateOn: 'blur' });
        }
        else {
            this.production = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"]();
            this.basicProductionInfo = this._formBuilder.group({
                name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionTypeId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                productionCompany: [''],
                description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                compensationType: [0],
                compensationContractDetails: [''],
                productionUnionStatus: [0]
            }, { updateOn: 'blur' });
            this.castingDetails = this._formBuilder.group({
                castingDirector: [''],
                castingAssistant: [''],
                castingAssociate: [''],
                assistantCastingAssociate: ['']
            }, { updateOn: 'blur' });
            this.productionDetails = this._formBuilder.group({
                executiveProducer: [''],
                coExecutiveProducer: [''],
                producer: [''],
                coProducer: ['']
            }, { updateOn: 'blur' });
            this.artisticDetails = this._formBuilder.group({
                artisticDirector: [''],
                musicDirector: [''],
                choreographer: ['']
            }, { updateOn: 'blur' });
            this.extraDetails = this._formBuilder.group({
                TVNetwork: [''],
                venue: [''],
            }, { updateOn: 'blur' });
        }
    };
    NewProductionComponent.prototype.ngOnDestroy = function () {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();
        }
        if (this.saveProductionIntentSubscription) {
            this.saveProductionIntentSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    NewProductionComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
        this.productionComponentService.registerProductionScrollElement(this.productionCard);
        this.productionComponentService.registerArtisticScrollElement(this.artisticCard);
        this.productionComponentService.registerCastingScrollElement(this.castingCard);
        this.productionComponentService.registerRolesScrollElement(this.rolesCard);
        this.productionComponentService.registerBasicScrollElement(this.basicCard);
        this.productionComponentService.registerOtherScrollElement(this.otherCard);
        if (this.popRole) {
            if (true) {
                this.stateService.go("draftproductionnewrole", { production: this.production, productionId: this.production.id, draftId: this.production.id });
            }
            else {}
        }
    };
    NewProductionComponent.prototype.moveToProductionView = function (productionId) {
        this.stateService.go('production', { productionId: productionId });
    };
    NewProductionComponent.prototype.moveToProductionsView = function () {
        this.stateService.go('productions');
    };
    NewProductionComponent.prototype.scrollToProduction = function () {
        //  this.basicProductionInfo.reset();
        this.productionCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToProduction();
    };
    NewProductionComponent.prototype.scrollToArtistic = function () {
        //  this.basicProductionInfo.reset();
        this.artisticCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToArtistic();
    };
    NewProductionComponent.prototype.scrollToCasting = function () {
        //   this.basicProductionInfo.reset();
        this.castingCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToCasting();
    };
    NewProductionComponent.prototype.scrollToRoles = function () {
        //    this.basicProductionInfo.reset();
        this.rolesCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToRoles();
    };
    NewProductionComponent.prototype.scrollToBasic = function () {
        //   this.basicProductionInfo.reset();
        this.basicCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToBasic();
    };
    NewProductionComponent.prototype.scrollToOther = function () {
        //   this.basicProductionInfo.reset();
        this.otherCard.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
        //this.productionComponentService.scrollToOther();
    };
    NewProductionComponent.prototype.deleteProductionClick = function () {
        //   this.basicProductionInfo.reset();
        this.productionComponentService.tryDeleteProduction();
    };
    NewProductionComponent.prototype.cancelEditProductionClick = function () {
        //   this.basicProductionInfo.reset();
        this.moveToProductionsView();
    };
    NewProductionComponent.prototype.getSubmissionObject = function () {
        var production = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["BaseProduction"];
        production.productionCompany = this.basicProductionInfo.get('productionCompany').value;
        production.productionTypeId = this.basicProductionInfo.get('productionTypeId').value;
        production.name = this.basicProductionInfo.get('name').value;
        production.description = this.basicProductionInfo.get('description').value;
        production.compensationType = this.basicProductionInfo.get('compensationType').value;
        production.compensationContractDetails = this.basicProductionInfo.get('compensationContractDetails').value;
        production.productionUnionStatus = this.basicProductionInfo.get('productionUnionStatus').value;
        production.castingDirector = this.castingDetails.get('castingDirector').value;
        production.castingAssistant = this.castingDetails.get('castingAssistant').value;
        production.castingAssociate = this.castingDetails.get('castingAssociate').value;
        production.assistantCastingAssociate = this.castingDetails.get('assistantCastingAssociate').value;
        production.executiveProducer = this.productionDetails.get('executiveProducer').value;
        production.coExecutiveProducer = this.productionDetails.get('coExecutiveProducer').value;
        production.producer = this.productionDetails.get('producer').value;
        production.coProducer = this.productionDetails.get('coProducer').value;
        production.artisticDirector = this.artisticDetails.get('artisticDirector').value;
        production.musicDirector = this.artisticDetails.get('musicDirector').value;
        production.choreographer = this.artisticDetails.get('choreographer').value;
        production.tVNetwork = this.extraDetails.get('TVNetwork').value;
        production.venue = this.extraDetails.get('venue').value;
        return production;
    };
    NewProductionComponent.prototype.getUpdateSubmissionObject = function () {
        var production = Object.assign(this.getSubmissionObject(), { id: this.production.id });
        return production;
    };
    NewProductionComponent.prototype.getNewSubmissionObject = function () {
        var production = Object.assign(this.getSubmissionObject());
        return production;
    };
    NewProductionComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    NewProductionComponent.prototype.saveProductionClick = function () {
        var _this = this;
        this.clearMessages();
        if (this.basicProductionInfo.valid) {
            //this.role = this.basicRoleInfo.value;
            //submissionProduction = Object.assign(this.basicProductionInfo.value, this.castingDetails.value, this.productionDetails.value, this.artisticDetails.value, this.extraDetails.value);
            if (this.production.id && this.production.id !== 0) {
                var submissionProduction_1 = this.getUpdateSubmissionObject();
                this.productionService.saveProduction(this.production.id, submissionProduction_1).subscribe(function (production) {
                    _this.messageDispatchService.dispatchSuccesMessage("Production:'" + production.name + "' was saved successfully", "Production Saved");
                    _this.moveToProductionView(_this.production.id);
                    //this.production = production;
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Production Save Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Production Save Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
            else {
                var submissionProduction_2 = this.getNewSubmissionObject();
                this.productionService.createProduction(submissionProduction_2).subscribe(function (production) {
                    _this.production = production;
                    _this.messageDispatchService.dispatchSuccesMessage("New production:'" + production.name + "' was created successfully", "Production Created");
                    _this.moveToProductionView(_this.production.id);
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Production Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Production Creation Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
        }
        else {
            if (!this.basicProductionInfo.get('name').valid) {
                this.basicProductionInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicProductionInfo.get('productionTypeId').valid) {
                this.basicProductionInfo.get('productionTypeId').markAsDirty();
                this.productionTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.productionTypeField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicProductionInfo.get('description').valid) {
                this.basicProductionInfo.get('description').markAsDirty();
                this.descriptionField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.descriptionField.nativeElement.focus();
            }
        }
    };
    NewProductionComponent.prototype.addRoleClick = function () {
        var _this = this;
        // this.basicProductionInfo.reset();
        if (this.production.id && this.production.id !== 0) {
            //setTimeout(() => {
            if (true) {
                this.stateService.go("draftproductionnewrole", { production: this.production, productionId: this.production.id, draftId: this.production.id });
            }
            else {}
            // });
        }
        else {
            var submissionObject = Object.assign(this.basicProductionInfo.value, this.castingDetails.value, this.productionDetails.value, this.artisticDetails.value, this.extraDetails.value);
            this.productionService.createProduction(submissionObject).subscribe(function (production) {
                _this.isLoading = false;
                _this.stateService.go('draftproduction', { production: production, draftId: production.id, popRole: true });
            }, function (error) {
            });
        }
    };
    NewProductionComponent.ctorParameters = function () { return [
        { type: _services_production_role_service__WEBPACK_IMPORTED_MODULE_9__["ProductionRoleService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
        { type: _services_production_service__WEBPACK_IMPORTED_MODULE_7__["ProductionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_11__["MessageDispatchService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] },
        { type: _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_6__["RoleDialogService"] },
        { type: _services_production_component_service__WEBPACK_IMPORTED_MODULE_8__["ProductionComponentService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_3__["MetadataModel"])
    ], NewProductionComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"])
    ], NewProductionComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('description', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "descriptionField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('productiontype', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__["Dropdown"])
    ], NewProductionComponent.prototype, "productionTypeField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('productioncard', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "productionCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('othercard', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "otherCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('artisticcard', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "artisticCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('castingcard', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "castingCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('rolescard', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "rolesCard", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('basiccard', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewProductionComponent.prototype, "basicCard", void 0);
    NewProductionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-production',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-production.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-production/new-production.component.html")).default,
            // make fade in animation available to this component
            animations: [_animations_router_animation__WEBPACK_IMPORTED_MODULE_12__["fadeInAnimation"]],
            // attach the fade in animation to the host (root) element of this component
            host: { '[@fadeInAnimation]': '' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-production.component.scss */ "./src/app/feature-modules/production/components/new-production/new-production.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_role_service__WEBPACK_IMPORTED_MODULE_9__["ProductionRoleService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _services_production_service__WEBPACK_IMPORTED_MODULE_7__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_11__["MessageDispatchService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_6__["RoleDialogService"],
            _services_production_component_service__WEBPACK_IMPORTED_MODULE_8__["ProductionComponentService"]])
    ], NewProductionComponent);
    return NewProductionComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.scss":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.scss ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("mat-dialog-content {\n  overflow: visible !important;\n}\n\n.role_card {\n  /* margin-left:16px;\n   margin-right:16px;*/\n}\n\n/*mat-card-header {\n  background-color: $primary;\n}*/\n\nmat-form-field, .mat-form-field {\n  width: 100%;\n}\n\nmat-slider {\n  display: block;\n}\n\n.smallage {\n  width: 90%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9uZXctcm9sZS1kaWFsb2cvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xccHJvZHVjdGlvblxcY29tcG9uZW50c1xcbmV3LXJvbGUtZGlhbG9nXFxuZXctcm9sZS1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXJvbGUtZGlhbG9nL25ldy1yb2xlLWRpYWxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLDRCQUFBO0FDQUo7O0FERUE7RUFDQztzQkFBQTtBQ0VEOztBRENBOztFQUFBOztBQUdBO0VBRUUsV0FBQTtBQ0NGOztBRENBO0VBQ0ksY0FBQTtBQ0VKOztBREFBO0VBQ0ksVUFBQTtBQ0dKIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9uZXctcm9sZS1kaWFsb2cvbmV3LXJvbGUtZGlhbG9nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbm1hdC1kaWFsb2ctY29udGVudCB7XHJcbiAgICBvdmVyZmxvdzp2aXNpYmxlICFpbXBvcnRhbnQ7XHJcbn1cclxuLnJvbGVfY2FyZCB7XHJcbiAvKiBtYXJnaW4tbGVmdDoxNnB4O1xyXG4gIG1hcmdpbi1yaWdodDoxNnB4OyovXHJcbn1cclxuLyptYXQtY2FyZC1oZWFkZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5O1xyXG59Ki9cclxubWF0LWZvcm0tZmllbGQsIC5tYXQtZm9ybS1maWVsZFxyXG57XHJcbiAgd2lkdGg6MTAwJTtcclxufVxyXG5tYXQtc2xpZGVyIHtcclxuICAgIGRpc3BsYXk6YmxvY2s7XHJcbn1cclxuLnNtYWxsYWdle1xyXG4gICAgd2lkdGg6OTAlO1xyXG59ICIsIm1hdC1kaWFsb2ctY29udGVudCB7XG4gIG92ZXJmbG93OiB2aXNpYmxlICFpbXBvcnRhbnQ7XG59XG5cbi5yb2xlX2NhcmQge1xuICAvKiBtYXJnaW4tbGVmdDoxNnB4O1xuICAgbWFyZ2luLXJpZ2h0OjE2cHg7Ki9cbn1cblxuLyptYXQtY2FyZC1oZWFkZXIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeTtcbn0qL1xubWF0LWZvcm0tZmllbGQsIC5tYXQtZm9ybS1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5tYXQtc2xpZGVyIHtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5zbWFsbGFnZSB7XG4gIHdpZHRoOiA5MCU7XG59Il19 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: NewRoleDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRoleDialogComponent", function() { return NewRoleDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm5/primeng-dialog.js");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");












var NewRoleDialogComponent = /** @class */ (function () {
    function NewRoleDialogComponent(_formBuilder, productionDialogService, ProductionRoleService, stateService, transitionService, transition, messageDispatchService, productionExtraService) {
        this._formBuilder = _formBuilder;
        this.productionDialogService = productionDialogService;
        this.ProductionRoleService = ProductionRoleService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.messageDispatchService = messageDispatchService;
        this.productionExtraService = productionExtraService;
        this.minMinAge = 0;
        this.minMaxAge = 49;
        // private minAge: number = 0;
        // private maxAge: number = 50;
        this.maxMinAge = 1;
        this.maxMaxAge = 100;
        this.showHints = false;
        this.age = [0, 100];
        this.minAge = 0;
        this.maxAge = 0;
        //roleTypes: SelectItem[];
        this.languages = [];
        this.bodyTypes = [];
        this.eyeColors = [];
        this.hairColors = [];
        this.accents = [];
        this.stereoTypes = [];
        this.ethnicStereoTypes = [];
        this.ethnicities = [];
        this.payDetails = [];
        this.genderTypes = [];
        this.roleTypes = [];
        this.messages = [];
        this.saveIsDisabled = true;
        this.cancelIsDisabled = false;
        this.isVisible = false;
    }
    NewRoleDialogComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
    };
    NewRoleDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.basicRoleInfo = this._formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            roleTypeId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            roleGenderTypeId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
            transgender: [false],
            age: [this.age],
            //minAge: [0],
            //maxAge: [100],
            description: [''],
            //accents: [[]],
            //languages: [[]],
            //ethnicities: [[]],
            //ethnicStereoTypes: [[]],
            // stereoTypes: [[]],
            requiresNudity: [false],
            payDetailId: [0],
        }, { updateOn: 'blur' });
        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToNewRoleDialogShowStateObs()
            .subscribe(function (dialogObject) {
            _this.clearMessages();
            _this.formDirective.resetForm();
            _this.basicRoleInfo.reset();
            if (dialogObject.isVisible && dialogObject.role != null) {
                _this.role = dialogObject.role;
                _this.age = [_this.role.minAge, _this.role.maxAge];
                _this.minAge = _this.role.minAge;
                _this.maxAge = _this.role.maxAge;
                _this.languages = _this.role.languages.map(function (l) { return l.language; });
                _this.bodyTypes = _this.role.bodyTypes.map(function (l) { return l.bodyType; });
                _this.eyeColors = _this.role.eyeColors.map(function (l) { return l.eyeColor; });
                _this.hairColors = _this.role.hairColors.map(function (l) { return l.hairColor; });
                _this.accents = _this.role.accents.map(function (l) { return l.accent; });
                _this.ethnicities = _this.role.ethnicities.map(function (l) { return l.ethnicity; });
                _this.stereoTypes = _this.role.stereoTypes.map(function (l) { return l.stereoType; });
                _this.ethnicStereoTypes = _this.role.ethnicStereoTypes.map(function (l) { return l.ethnicStereoType; });
                _this.basicRoleInfo.patchValue({
                    name: _this.role.name,
                    roleTypeId: _this.role.roleTypeId,
                    roleGenderTypeId: _this.role.roleGenderTypeId,
                    transgender: _this.role.transgender,
                    age: _this.age,
                    //minAge: this.role.minAge,
                    //maxAge: this.role.maxAge,
                    description: _this.role.description,
                    //accents: this.role.accents.map(a => a.accentId),
                    //languages: this.role.languages,
                    //ethnicities: this.role.ethnicities.map(a => a.ethnicityId),
                    //ethnicStereoTypes: this.role.ethnicStereoTypes.map(a => a.ethnicStereoTypeId),
                    //stereoTypes: this.role.stereoTypes.map(a => a.stereoTypeId),
                    requiresNudity: _this.role.requiresNudity,
                    // payDetailId: this.production.payDetails[this.production.payDetails.findIndex(t => t.id == this.role.payDetailId)],
                    payDetailId: _this.role.payDetailId,
                });
            }
            else {
                _this.role = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"];
                _this.basicRoleInfo.patchValue({
                    name: '',
                    roleTypeId: null,
                    roleGenderTypeId: null,
                    transgender: false,
                    age: _this.age,
                    //minAge: 0,
                    //maxAge: 100,
                    description: '',
                    //accents: [],
                    //languages: [],
                    //ethnicities: [],
                    //ethnicStereoTypes: [],
                    // stereoTypes: [],
                    requiresNudity: false,
                    payDetailId: 0,
                });
            }
            _this.newPayDetailSubscription = _this.productionExtraService.subscribeToCreatePayDetailObs()
                .subscribe(function (detail) {
                if (detail) {
                    _this.production.payDetails.push(detail);
                    _this.payDetails.push({
                        value: detail.id,
                        label: detail.name,
                        id: detail.id,
                        productionId: detail.productionId,
                        name: detail.name
                    });
                    _this.basicRoleInfo.patchValue({
                        payDetailId: detail.id
                    });
                }
            });
            _this.isVisible = dialogObject.isVisible;
            // this.detaildialog.maximized = true;
            //this.detaildialog.toggleMaximize();
            _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            _this.nameField.nativeElement.focus();
        });
        this.payDetails = this.production.payDetails.map(function (x) { return ({
            value: x.id,
            label: x.name,
            id: x.id,
            productionId: x.productionId,
            name: x.name
        }); });
        this.roleTypes = this.metadata.roleTypes.map(function (x) { return ({
            value: x.id,
            label: x.name,
            id: x.id,
            name: x.name
        }); });
        this.genderTypes = this.metadata.roleGenderTypes.map(function (x) { return ({
            value: x.id,
            label: x.name,
            id: x.id,
            name: x.name
        }); });
    };
    NewRoleDialogComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();
        }
        if (this.newPayDetailSubscription) {
            this.newPayDetailSubscription.unsubscribe();
        }
    };
    NewRoleDialogComponent.prototype.onDialogShow = function (event, dialog) {
        setTimeout(function () {
            dialog.maximize();
        }, 0);
    };
    NewRoleDialogComponent.prototype.onDialogHide = function (event, dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicRoleInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();
    };
    NewRoleDialogComponent.prototype.onNoClick = function () {
        this.stateService.go(this.transition.from());
        //this.dialogRef.close();
    };
    NewRoleDialogComponent.prototype.handleAgeSliderChange = function (e) {
        this.minAge = e.values[0];
        this.maxAge = e.values[1];
    };
    NewRoleDialogComponent.prototype.onMinSliderChange = function ($event) {
        this.minAge = $event.value;
        this.maxMaxAge = this.minAge + 1;
        this.basicRoleInfo.get('minAge').setValue($event.value);
    };
    NewRoleDialogComponent.prototype.removePayDetail = function () {
        this.basicRoleInfo.patchValue({
            payDetailId: null,
        });
    };
    NewRoleDialogComponent.prototype.addNewPayDetails = function () {
        this.productionDialogService.showPayDetailsDialog(null);
    };
    NewRoleDialogComponent.prototype.onMaxSliderChange = function ($event) {
        this.maxAge = $event.value;
        this.minMaxAge = this.maxAge - 1;
        this.basicRoleInfo.get('maxAge').setValue($event.value);
    };
    NewRoleDialogComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    NewRoleDialogComponent.prototype.resetDialog = function () {
        this.age = [0, 100];
        this.minAge = 0;
        this.maxAge = 0;
        //roleTypes =  SelectItem[];
        this.languages = [];
        this.bodyTypes = [];
        this.eyeColors = [];
        this.hairColors = [];
        this.accents = [];
        this.stereoTypes = [];
        this.ethnicStereoTypes = [];
        this.ethnicities = [];
        if (this.newPayDetailSubscription) {
            this.newPayDetailSubscription.unsubscribe();
        }
        this.saveIsDisabled = true;
        this.cancelIsDisabled = false;
        this.messages = [];
    };
    NewRoleDialogComponent.prototype.getSubmissionObject = function () {
        var role = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"];
        role.productionId = this.production.id;
        role.name = this.basicRoleInfo.get('name').value;
        role.transgender = this.basicRoleInfo.get('transgender').value;
        role.minAge = this.basicRoleInfo.get('age').value[0];
        role.maxAge = this.basicRoleInfo.get('age').value[1];
        role.description = this.basicRoleInfo.get('description').value;
        role.languages = this.languages.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.bodyTypes = this.bodyTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.eyeColors = this.eyeColors.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.hairColors = this.hairColors.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.accents = this.accents.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.ethnicities = this.ethnicities.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.ethnicStereoTypes = this.ethnicStereoTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.stereoTypes = this.stereoTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.skills = [];
        //role.accents = this.basicRoleInfo.get('accents').value;
        // role.ethnicities= this.basicRoleInfo.get('ethnicities').value;
        //  role.ethnicStereoTypes= this.basicRoleInfo.get('ethnicStereoTypes').value;
        //  role.stereoTypes= this.basicRoleInfo.get('stereoTypes').value;
        role.requiresNudity = this.basicRoleInfo.get('requiresNudity').value;
        role.roleTypeId = this.basicRoleInfo.get('roleTypeId').value;
        role.roleGenderTypeId = this.basicRoleInfo.get('roleGenderTypeId').value;
        if (this.basicRoleInfo.get('payDetailId').value) {
            role.payDetailId = this.basicRoleInfo.get('payDetailId').value;
        }
        else {
            role.payDetailId = 0;
        }
        return role;
    };
    NewRoleDialogComponent.prototype.saveRoleClick = function () {
        var _this = this;
        this.clearMessages();
        debugger;
        if (this.basicRoleInfo.valid) {
            var submissionRole_1 = this.getSubmissionObject();
            if (this.role && this.role.id && this.role.id !== 0) {
                this.ProductionRoleService.saveRole(this.role.id, this.production.id, submissionRole_1).subscribe(function (role) {
                    _this.production.roles[_this.production.roles.findIndex(function (r) { return r.id == role.id; })] = role;
                    _this.messageDispatchService.dispatchSuccesMessage("Role:'" + role.name + "' was saved successfully", "Role Saved");
                    var params = {
                        productionId: _this.production.id,
                        production: _this.production,
                        draftId: _this.production.id
                    };
                    if (_this.transition.from().name != "draftproduction" &&
                        _this.transition.from().name != "editproduction") {
                        _this.stateService.go("production", params);
                    }
                    else {
                        _this.stateService.go(_this.transition.from(), params);
                    }
                    _this.formDirective.resetForm();
                    _this.basicRoleInfo.reset();
                    _this.isVisible = false;
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Role Save Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Role Save Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
            else {
                this.ProductionRoleService.createRole(this.production.id, submissionRole_1).subscribe(function (role) {
                    if (role != null) {
                        _this.production.roles.push(role);
                    }
                    _this.messageDispatchService.dispatchSuccesMessage("New role:'" + submissionRole_1.name + "' was created successfully", "Role Created");
                    var params = {
                        productionId: _this.production.id,
                        production: _this.production,
                        draftId: _this.production.id
                    };
                    if (_this.transition.from().name != "draftproduction" &&
                        _this.transition.from().name != "editproduction") {
                        _this.stateService.go("production", params);
                    }
                    else {
                        _this.stateService.go(_this.transition.from(), params);
                    }
                    _this.formDirective.resetForm();
                    _this.basicRoleInfo.reset();
                    _this.isVisible = false;
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Role Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Role Creation Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
        }
        else {
            if (!this.basicRoleInfo.get('name').valid) {
                this.basicRoleInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicRoleInfo.get('roleTypeId').valid) {
                this.basicRoleInfo.get('roleTypeId').markAsDirty();
                this.roleTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.roleTypeField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicRoleInfo.get('roleGenderTypeId').valid) {
                this.basicRoleInfo.get('roleGenderTypeId').markAsDirty();
                this.genderTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.genderTypeField.focusViewChild.nativeElement.focus();
            }
        }
    };
    NewRoleDialogComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_7__["ProductionDialogService"] },
        { type: _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_9__["ProductionExtraService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], NewRoleDialogComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"])
    ], NewRoleDialogComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_3__["MetadataModel"])
    ], NewRoleDialogComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"])
    ], NewRoleDialogComponent.prototype, "role", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formDirective', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgForm"])
    ], NewRoleDialogComponent.prototype, "formDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewRoleDialogComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roletype', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_11__["Dropdown"])
    ], NewRoleDialogComponent.prototype, "roleTypeField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('gendertype', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_11__["Dropdown"])
    ], NewRoleDialogComponent.prototype, "genderTypeField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('newroledialog', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dialog__WEBPACK_IMPORTED_MODULE_10__["Dialog"])
    ], NewRoleDialogComponent.prototype, "newroledialog", void 0);
    NewRoleDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-role-dialog',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-role-dialog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-role-dialog.component.scss */ "./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_7__["ProductionDialogService"],
            _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_9__["ProductionExtraService"]])
    ], NewRoleDialogComponent);
    return NewRoleDialogComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/new-role/new-role.component.scss":
/*!****************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/new-role/new-role.component.scss ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("mat-dialog-content {\n  overflow: visible !important;\n}\n\n.role_card {\n  /* margin-left:16px;\n   margin-right:16px;*/\n}\n\n/*mat-card-header {\n  background-color: $primary;\n}*/\n\nmat-form-field, .mat-form-field {\n  width: 100%;\n}\n\nmat-slider {\n  display: block;\n}\n\n.smallage {\n  width: 90%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9uZXctcm9sZS9GOlxcd29ya1xcY2FzdGluZ1xcQ2FzdGluZ1xcQW5ndWxhclxcdGFsZW50LWFwcC9zcmNcXGFwcFxcZmVhdHVyZS1tb2R1bGVzXFxwcm9kdWN0aW9uXFxjb21wb25lbnRzXFxuZXctcm9sZVxcbmV3LXJvbGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXJvbGUvbmV3LXJvbGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDSSw0QkFBQTtBQ0FKOztBREVBO0VBQ0M7c0JBQUE7QUNFRDs7QURDQTs7RUFBQTs7QUFHQTtFQUVFLFdBQUE7QUNDRjs7QURDQTtFQUNJLGNBQUE7QUNFSjs7QURBQTtFQUNJLFVBQUE7QUNHSiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvbmV3LXJvbGUvbmV3LXJvbGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxubWF0LWRpYWxvZy1jb250ZW50IHtcclxuICAgIG92ZXJmbG93OnZpc2libGUgIWltcG9ydGFudDtcclxufVxyXG4ucm9sZV9jYXJkIHtcclxuIC8qIG1hcmdpbi1sZWZ0OjE2cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OjE2cHg7Ki9cclxufVxyXG4vKm1hdC1jYXJkLWhlYWRlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJHByaW1hcnk7XHJcbn0qL1xyXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkXHJcbntcclxuICB3aWR0aDoxMDAlO1xyXG59XHJcbm1hdC1zbGlkZXIge1xyXG4gICAgZGlzcGxheTpibG9jaztcclxufVxyXG4uc21hbGxhZ2V7XHJcbiAgICB3aWR0aDo5MCU7XHJcbn0gIiwibWF0LWRpYWxvZy1jb250ZW50IHtcbiAgb3ZlcmZsb3c6IHZpc2libGUgIWltcG9ydGFudDtcbn1cblxuLnJvbGVfY2FyZCB7XG4gIC8qIG1hcmdpbi1sZWZ0OjE2cHg7XG4gICBtYXJnaW4tcmlnaHQ6MTZweDsqL1xufVxuXG4vKm1hdC1jYXJkLWhlYWRlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICRwcmltYXJ5O1xufSovXG5tYXQtZm9ybS1maWVsZCwgLm1hdC1mb3JtLWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbm1hdC1zbGlkZXIge1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLnNtYWxsYWdlIHtcbiAgd2lkdGg6IDkwJTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/new-role/new-role.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/new-role/new-role.component.ts ***!
  \**************************************************************************************/
/*! exports provided: NewRoleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRoleComponent", function() { return NewRoleComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! primeng/dropdown */ "./node_modules/primeng/fesm5/primeng-dropdown.js");











var NewRoleComponent = /** @class */ (function () {
    function NewRoleComponent(_formBuilder, ProductionRoleService, stateService, transitionService, transition, productionDialogService, messageDispatchService, productionExtraService) {
        this._formBuilder = _formBuilder;
        this.ProductionRoleService = ProductionRoleService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.productionDialogService = productionDialogService;
        this.messageDispatchService = messageDispatchService;
        this.productionExtraService = productionExtraService;
        this.minMinAge = 0;
        this.minMaxAge = 49;
        // private minAge: number = 0;
        // private maxAge: number = 50;
        this.maxMinAge = 1;
        this.maxMaxAge = 100;
        this.showHints = false;
        this.age = [0, 100];
        this.minAge = 0;
        this.maxAge = 0;
        //roleTypes: SelectItem[];
        this.languages = [];
        this.bodyTypes = [];
        this.eyeColors = [];
        this.hairColors = [];
        this.accents = [];
        this.stereoTypes = [];
        this.ethnicStereoTypes = [];
        this.ethnicities = [];
        this.payDetails = [];
        this.genderTypes = [];
        this.roleTypes = [];
        this.messages = [];
    }
    NewRoleComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        setTimeout(function () { return _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' }); });
    };
    NewRoleComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.payDetails = this.production.payDetails.map(function (x) { return ({
            value: x.id,
            label: x.name,
            id: x.id,
            productionId: x.productionId,
            name: x.name
        }); });
        this.roleTypes = this.metadata.roleTypes.map(function (x) { return ({
            value: x.id,
            label: x.name,
            id: x.id,
            name: x.name
        }); });
        this.genderTypes = this.metadata.roleGenderTypes.map(function (x) { return ({
            value: x.id,
            label: x.name,
            id: x.id,
            name: x.name
        }); });
        if (this.role) {
            this.age = [this.role.minAge, this.role.maxAge];
            this.minAge = this.role.minAge;
            this.maxAge = this.role.maxAge;
            this.languages = this.role.languages.map(function (l) { return l.language; });
            this.bodyTypes = this.role.bodyTypes.map(function (l) { return l.bodyType; });
            this.eyeColors = this.role.eyeColors.map(function (l) { return l.eyeColor; });
            this.hairColors = this.role.hairColors.map(function (l) { return l.hairColor; });
            this.accents = this.role.accents.map(function (l) { return l.accent; });
            this.ethnicities = this.role.ethnicities.map(function (l) { return l.ethnicity; });
            this.stereoTypes = this.role.stereoTypes.map(function (l) { return l.stereoType; });
            this.ethnicStereoTypes = this.role.ethnicStereoTypes.map(function (l) { return l.ethnicStereoType; });
            this.basicRoleInfo = this._formBuilder.group({
                name: [this.role.name, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleTypeId: [this.role.roleTypeId, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleGenderTypeId: [this.role.roleGenderTypeId, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                transgender: [this.role.transgender],
                age: [this.age],
                //minAge: [this.role.minAge],
                //maxAge: [this.role.maxAge],
                description: [this.role.description],
                //accents: [this.role.accents.map(a => a.accentId)],
                //languages: [this.role.languages],
                //ethnicities: [this.role.ethnicities.map(a => a.ethnicityId)],
                //ethnicStereoTypes: [this.role.ethnicStereoTypes.map(a => a.ethnicStereoTypeId)],
                //stereoTypes: [this.role.stereoTypes.map(a => a.stereoTypeId)],
                requiresNudity: [this.role.requiresNudity],
                // payDetailId: [this.production.payDetails[this.production.payDetails.findIndex(t => t.id == this.role.payDetailId)]],
                payDetailId: [this.role.payDetailId],
            }, { updateOn: 'blur' });
        }
        else {
            this.role = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"];
            this.basicRoleInfo = this._formBuilder.group({
                name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleTypeId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                roleGenderTypeId: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required],
                transgender: [false],
                age: [this.age],
                //minAge: [0],
                //maxAge: [100],
                description: [''],
                //accents: [[]],
                //languages: [[]],
                //ethnicities: [[]],
                //ethnicStereoTypes: [[]],
                // stereoTypes: [[]],
                requiresNudity: [false],
                payDetailId: [0],
            }, { updateOn: 'blur' });
        }
        this.newPayDetailSubscription = this.productionExtraService.subscribeToCreatePayDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.payDetails.push(detail);
                _this.payDetails.push({
                    value: detail.id,
                    label: detail.name,
                    id: detail.id,
                    productionId: detail.productionId,
                    name: detail.name
                });
                _this.basicRoleInfo.patchValue({
                    payDetailId: detail.id
                });
            }
        });
    };
    NewRoleComponent.prototype.ngOnDestroy = function () {
        if (this.newPayDetailSubscription) {
            this.newPayDetailSubscription.unsubscribe();
        }
    };
    NewRoleComponent.prototype.onNoClick = function () {
        this.stateService.go(this.transition.from());
        //this.dialogRef.close();
    };
    NewRoleComponent.prototype.handleAgeSliderChange = function (e) {
        this.minAge = e.values[0];
        this.maxAge = e.values[1];
    };
    NewRoleComponent.prototype.onMinSliderChange = function ($event) {
        this.minAge = $event.value;
        this.maxMaxAge = this.minAge + 1;
        this.basicRoleInfo.get('minAge').setValue($event.value);
    };
    NewRoleComponent.prototype.removePayDetail = function () {
        this.basicRoleInfo.patchValue({
            payDetailId: null,
        });
    };
    NewRoleComponent.prototype.addNewPayDetails = function () {
        this.productionDialogService.showPayDetailsDialog(null);
    };
    NewRoleComponent.prototype.onMaxSliderChange = function ($event) {
        this.maxAge = $event.value;
        this.minMaxAge = this.maxAge - 1;
        this.basicRoleInfo.get('maxAge').setValue($event.value);
    };
    NewRoleComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    NewRoleComponent.prototype.getSubmissionObject = function () {
        var role = new _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"];
        role.productionId = this.production.id;
        role.name = this.basicRoleInfo.get('name').value;
        role.transgender = this.basicRoleInfo.get('transgender').value;
        role.minAge = this.basicRoleInfo.get('age').value[0];
        role.maxAge = this.basicRoleInfo.get('age').value[1];
        role.description = this.basicRoleInfo.get('description').value;
        role.languages = this.languages.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.bodyTypes = this.bodyTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.eyeColors = this.eyeColors.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.hairColors = this.hairColors.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.accents = this.accents.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.ethnicities = this.ethnicities.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.ethnicStereoTypes = this.ethnicStereoTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.stereoTypes = this.stereoTypes.map(function (l) { return l.id; }); // this.basicRoleInfo.controls['languages'].value;
        role.skills = [];
        //role.accents = this.basicRoleInfo.get('accents').value;
        // role.ethnicities= this.basicRoleInfo.get('ethnicities').value;
        //  role.ethnicStereoTypes= this.basicRoleInfo.get('ethnicStereoTypes').value;
        //  role.stereoTypes= this.basicRoleInfo.get('stereoTypes').value;
        role.requiresNudity = this.basicRoleInfo.get('requiresNudity').value;
        role.roleTypeId = this.basicRoleInfo.get('roleTypeId').value;
        role.roleGenderTypeId = this.basicRoleInfo.get('roleGenderTypeId').value;
        if (this.basicRoleInfo.get('payDetailId').value) {
            role.payDetailId = this.basicRoleInfo.get('payDetailId').value;
        }
        else {
            role.payDetailId = 0;
        }
        return role;
    };
    NewRoleComponent.prototype.saveRoleClick = function () {
        var _this = this;
        this.clearMessages();
        if (this.basicRoleInfo.valid) {
            var submissionRole_1 = this.getSubmissionObject();
            if (this.role && this.role.id && this.role.id !== 0) {
                this.ProductionRoleService.saveRole(this.role.id, this.production.id, submissionRole_1).subscribe(function (role) {
                    _this.production.roles[_this.production.roles.findIndex(function (r) { return r.id == role.id; })] = role;
                    _this.messageDispatchService.dispatchSuccesMessage("Role:'" + role.name + "' was saved successfully", "Role Saved");
                    var params = {
                        productionId: _this.production.id,
                        production: _this.production,
                        draftId: _this.production.id
                    };
                    if (_this.transition.from().name != "draftproduction" &&
                        _this.transition.from().name != "editproduction") {
                        _this.stateService.go("production", params);
                    }
                    else {
                        _this.stateService.go(_this.transition.from(), params);
                    }
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Role Save Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Role Save Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
            else {
                this.ProductionRoleService.createRole(this.production.id, submissionRole_1).subscribe(function (role) {
                    if (role != null) {
                        _this.production.roles.push(role);
                    }
                    _this.messageDispatchService.dispatchSuccesMessage("New role:'" + submissionRole_1.name + "' was created successfully", "Role Created");
                    var params = {
                        productionId: _this.production.id,
                        production: _this.production,
                        draftId: _this.production.id
                    };
                    if (_this.transition.from().name != "draftproduction" &&
                        _this.transition.from().name != "editproduction") {
                        _this.stateService.go("production", params);
                    }
                    else {
                        _this.stateService.go(_this.transition.from(), params);
                    }
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Role Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Role Creation Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
        }
        else {
            if (!this.basicRoleInfo.get('name').valid) {
                this.basicRoleInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicRoleInfo.get('roleTypeId').valid) {
                this.basicRoleInfo.get('roleTypeId').markAsDirty();
                this.roleTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.roleTypeField.focusViewChild.nativeElement.focus();
            }
            else if (!this.basicRoleInfo.get('roleGenderTypeId').valid) {
                this.basicRoleInfo.get('roleGenderTypeId').markAsDirty();
                this.genderTypeField.focusViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.genderTypeField.focusViewChild.nativeElement.focus();
            }
        }
    };
    NewRoleComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
        { type: _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] },
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_7__["ProductionDialogService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_9__["ProductionExtraService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Production"])
    ], NewRoleComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_3__["MetadataModel"])
    ], NewRoleComponent.prototype, "metadata", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_4__["Role"])
    ], NewRoleComponent.prototype, "role", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], NewRoleComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('roletype', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__["Dropdown"])
    ], NewRoleComponent.prototype, "roleTypeField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('gendertype', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dropdown__WEBPACK_IMPORTED_MODULE_10__["Dropdown"])
    ], NewRoleComponent.prototype, "genderTypeField", void 0);
    NewRoleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-new-role',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./new-role.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/new-role/new-role.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./new-role.component.scss */ "./src/app/feature-modules/production/components/new-role/new-role.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"],
            _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"],
            _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_7__["ProductionDialogService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_9__["ProductionExtraService"]])
    ], NewRoleComponent);
    return NewRoleComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.scss":
/*!************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.scss ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcGF5LWRldGFpbHMtZGlhbG9nL3BheS1kZXRhaWxzLWRpYWxvZy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: PayDetailsDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayDetailsDialogComponent", function() { return PayDetailsDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm5/primeng-dialog.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! primeng/radiobutton */ "./node_modules/primeng/fesm5/primeng-radiobutton.js");










var PayDetailsDialogComponent = /** @class */ (function () {
    function PayDetailsDialogComponent(productionDialogService, productionExtraService, _formBuilder, mediaMatcher, messageDispatchService) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this._formBuilder = _formBuilder;
        this.mediaMatcher = mediaMatcher;
        this.messageDispatchService = messageDispatchService;
        this.isSmallDevice = false;
        this.messages = [];
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.payPackageTypeEnum = _models_production_models__WEBPACK_IMPORTED_MODULE_5__["PayPackageTypeEnum"];
        this.isVisible = false;
    }
    PayDetailsDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSmallDevice = window.innerWidth < 768;
        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));
        this.basicDialogInfo = this._formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            payPackageType: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            details: [''],
        }, { updateOn: 'blur' });
        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToPayDetailsDialogShowStateObs()
            .subscribe(function (dialogObject) {
            _this.clearMessages();
            _this.formDirective.resetForm();
            _this.basicDialogInfo.reset();
            if (dialogObject.isVisible && dialogObject.payDetail != null) {
                _this.payDetail = dialogObject.payDetail;
                _this.basicDialogInfo.patchValue({
                    name: _this.payDetail.name,
                    details: _this.payDetail.details,
                    payPackageType: _this.payDetail.payPackageType,
                });
            }
            else {
                _this.payDetail = null;
                _this.basicDialogInfo.patchValue({
                    name: '',
                    details: '',
                    payPackageType: 0,
                });
            }
            _this.isVisible = dialogObject.isVisible;
            // this.detaildialog.maximized = true;
            //this.detaildialog.toggleMaximize();
            _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            _this.nameField.nativeElement.focus();
        });
    };
    PayDetailsDialogComponent.prototype.ngAfterViewInit = function () {
    };
    PayDetailsDialogComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();
        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);
    };
    PayDetailsDialogComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    PayDetailsDialogComponent.prototype.mediaQueryBigChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = false;
        }
    };
    PayDetailsDialogComponent.prototype.mediaQuerySmallChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = true;
        }
    };
    PayDetailsDialogComponent.prototype.payTypeOptionClick = function (payPackageType) {
        this.payTypeChange(payPackageType);
    };
    PayDetailsDialogComponent.prototype.payTypeChange = function (payPackageType) {
    };
    PayDetailsDialogComponent.prototype.onDialogShow = function (event, dialog) {
        if (this.isSmallDevice) {
            setTimeout(function () {
                dialog.maximize();
            }, 0);
        }
    };
    PayDetailsDialogComponent.prototype.onDialogHide = function (event, dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();
    };
    PayDetailsDialogComponent.prototype.cancelClick = function () {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
    };
    PayDetailsDialogComponent.prototype.resetDialog = function () {
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.messages = [];
    };
    PayDetailsDialogComponent.prototype.saveClick = function () {
        var _this = this;
        this.clearMessages();
        if (this.basicDialogInfo.valid) {
            this.saveIsDisabled = true;
            this.cancelIsDisabled = true;
            var tpayDetail_1 = new _models_production_models__WEBPACK_IMPORTED_MODULE_5__["PayDetail"];
            tpayDetail_1.productionId = this.productionId;
            tpayDetail_1.name = this.basicDialogInfo.get('name').value;
            tpayDetail_1.details = this.basicDialogInfo.get('details').value;
            tpayDetail_1.payPackageType = this.basicDialogInfo.get('payPackageType').value;
            if (this.payDetail && this.payDetail.id && this.payDetail.id !== 0) {
                this.productionExtraService.savePayDetail(this.payDetail.id, this.productionId, tpayDetail_1).subscribe(function (payDetail) {
                    //this.payDetail = payDetail;
                    _this.formDirective.resetForm();
                    _this.basicDialogInfo.reset();
                    _this.isVisible = false;
                    _this.messageDispatchService.dispatchSuccesMessage("Pay detail '" + payDetail.name + "' was saved successfully", "Pay Detail Saved");
                }, function (error) {
                    _this.saveIsDisabled = false;
                    _this.cancelIsDisabled = false;
                    _this.messageDispatchService.dispatchErrorMessage( true
                        ? (error.error)
                            ? (error.error.exceptionMessage)
                                ? error.error.exceptionMessage
                                : error.error
                            : error
                        : undefined, "Pay Detail Save Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Pay Detail Save Error",
                        detail:  true
                            ? (error.error)
                                ? (error.error.exceptionMessage)
                                    ? error.error.exceptionMessage
                                    : error.error
                                : error
                            : undefined
                    });
                });
            }
            else {
                this.productionExtraService.createPayDetail(this.productionId, tpayDetail_1).subscribe(function (payDetail) {
                    if (payDetail != null) {
                        _this.formDirective.resetForm();
                        _this.basicDialogInfo.reset();
                        _this.isVisible = false;
                        _this.messageDispatchService.dispatchSuccesMessage("New pay detail '" + payDetail.name + "' was created successfully", "Pay Detail Created");
                    }
                }, function (error) {
                    _this.saveIsDisabled = false;
                    _this.cancelIsDisabled = false;
                    _this.messageDispatchService.dispatchErrorMessage( true
                        ? (error.error)
                            ? (error.error.exceptionMessage)
                                ? error.error.exceptionMessage
                                : error.error
                            : error
                        : undefined, "Pay Detail Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Pay Detail Creation Error",
                        detail:  true
                            ? (error.error)
                                ? (error.error.exceptionMessage)
                                    ? error.error.exceptionMessage
                                    : error.error
                                : error
                            : undefined
                    });
                });
            }
        }
        else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('payPackageType').valid) {
                this.basicDialogInfo.get('payPackageType').markAsDirty();
                this.payPackageTypeField.inputViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.payPackageTypeField.inputViewChild.nativeElement.focus();
            }
        }
    };
    PayDetailsDialogComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_5__["PayDetail"])
    ], PayDetailsDialogComponent.prototype, "payDetail", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], PayDetailsDialogComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formDirective', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"])
    ], PayDetailsDialogComponent.prototype, "formDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], PayDetailsDialogComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('detaildialog', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dialog__WEBPACK_IMPORTED_MODULE_7__["Dialog"])
    ], PayDetailsDialogComponent.prototype, "detaildialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('payPackageType', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_radiobutton__WEBPACK_IMPORTED_MODULE_9__["RadioButton"])
    ], PayDetailsDialogComponent.prototype, "payPackageTypeField", void 0);
    PayDetailsDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pay-details-dialog',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./pay-details-dialog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./pay-details-dialog.component.scss */ "./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"]])
    ], PayDetailsDialogComponent);
    return PayDetailsDialogComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.scss":
/*!******************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.scss ***!
  \******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcGF5LWRldGFpbHMtbGlzdC1pdGVtL3BheS1kZXRhaWxzLWxpc3QtaXRlbS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.ts":
/*!****************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.ts ***!
  \****************************************************************************************************************/
/*! exports provided: PayDetailsListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayDetailsListItemComponent", function() { return PayDetailsListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");
/* harmony import */ var _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/confirm-decision-subscription.service */ "./src/app/services/confirm-decision-subscription.service.ts");








var PayDetailsListItemComponent = /** @class */ (function () {
    function PayDetailsListItemComponent(productionDialogService, productionExtraService, messageDispatchService, messageService, decisionDispatcher) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
        this.messageService = messageService;
        this.decisionDispatcher = decisionDispatcher;
    }
    PayDetailsListItemComponent.prototype.ngOnInit = function () {
    };
    PayDetailsListItemComponent.prototype.ngOnDestroy = function () {
        if (this.decisionDispatch) {
            this.decisionDispatch.unsubscribe();
        }
    };
    PayDetailsListItemComponent.prototype.viewDetails = function () {
        this.productionDialogService.showPayDetailsDialog(this.detail);
    };
    PayDetailsListItemComponent.prototype.deleteDetails = function () {
        var _this = this;
        this.decisionDispatcher.poseConfirmDecision('Delete pay detail ' + this.detail.name + '?', 'Confirm to proceed');
        this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
            .subscribe(function (decision) {
            _this.decisionDispatch.unsubscribe();
            if (decision) {
                _this.productionExtraService.deletePayDetail(_this.detail.id, _this.detail.productionId).subscribe(function (payDetail) {
                    //this.payDetail = payDetail;
                    _this.messageDispatchService.dispatchSuccesMessage("Pay detail '" + _this.detail.name + "' was deleted successfully", "Pay Detail Deleted");
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Pay Detail Save Error");
                });
            }
        });
    };
    PayDetailsListItemComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"] },
        { type: primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"] },
        { type: _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["PayDetail"])
    ], PayDetailsListItemComponent.prototype, "detail", void 0);
    PayDetailsListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pay-details-list-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./pay-details-list-item.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./pay-details-list-item.component.scss */ "./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"],
            primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
            _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"]])
    ], PayDetailsListItemComponent);
    return PayDetailsListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.scss ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcGF5LWRldGFpbHMtbGlzdC9wYXktZGV0YWlscy1saXN0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: PayDetailsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayDetailsListComponent", function() { return PayDetailsListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");




var PayDetailsListComponent = /** @class */ (function () {
    function PayDetailsListComponent(productionExtraService, messageDispatchService) {
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
    }
    PayDetailsListComponent.prototype.ngOnInit = function () {
        this.editPayDetailSubscription = this.productionExtraService.subscribeToEditPayDetailObs()
            .subscribe(function (detail) {
        });
        this.newPayDetailSubscription = this.productionExtraService.subscribeToCreatePayDetailObs()
            .subscribe(function (detail) {
        });
        this.deletePayDetailSubscription = this.productionExtraService.subscribeToDeletePayDetailObs()
            .subscribe(function (detail) {
            if (detail) {
            }
        });
    };
    PayDetailsListComponent.prototype.ngOnDestroy = function () {
        if (this.editPayDetailSubscription) {
            this.editPayDetailSubscription.unsubscribe();
        }
        if (this.newPayDetailSubscription) {
            this.newPayDetailSubscription.unsubscribe();
        }
        if (this.deletePayDetailSubscription) {
            this.deletePayDetailSubscription.unsubscribe();
        }
    };
    PayDetailsListComponent.ctorParameters = function () { return [
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], PayDetailsListComponent.prototype, "payDetails", void 0);
    PayDetailsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pay-details-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./pay-details-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./pay-details-list.component.scss */ "./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"]])
    ], PayDetailsListComponent);
    return PayDetailsListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/production-list-item/production-list-item.component.scss":
/*!****************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/production-list-item/production-list-item.component.scss ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi1saXN0LWl0ZW0vcHJvZHVjdGlvbi1saXN0LWl0ZW0uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/production-list-item/production-list-item.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/production-list-item/production-list-item.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: ProductionListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionListItemComponent", function() { return ProductionListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");



var ProductionListItemComponent = /** @class */ (function () {
    function ProductionListItemComponent() {
    }
    ProductionListItemComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["Production"])
    ], ProductionListItemComponent.prototype, "production", void 0);
    ProductionListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-production-list-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./production-list-item.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production-list-item/production-list-item.component.html")).default,
            host: { 'class': 'p-col' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./production-list-item.component.scss */ "./src/app/feature-modules/production/components/production-list-item/production-list-item.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductionListItemComponent);
    return ProductionListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/production-list/production-list.component.scss":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/production-list/production-list.component.scss ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".production_list_wrapper {\n  height: 400px;\n}\n\n.production_list {\n  padding: 8px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9wcm9kdWN0aW9uLWxpc3QvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xccHJvZHVjdGlvblxcY29tcG9uZW50c1xccHJvZHVjdGlvbi1saXN0XFxwcm9kdWN0aW9uLWxpc3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi1saXN0L3Byb2R1Y3Rpb24tbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUNDSjs7QURDQTtFQUNJLGNBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi1saXN0L3Byb2R1Y3Rpb24tbGlzdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wcm9kdWN0aW9uX2xpc3Rfd3JhcHBlcntcclxuICAgIGhlaWdodDo0MDBweDtcclxufVxyXG4ucHJvZHVjdGlvbl9saXN0e1xyXG4gICAgcGFkZGluZzo4cHggMDtcclxufSIsIi5wcm9kdWN0aW9uX2xpc3Rfd3JhcHBlciB7XG4gIGhlaWdodDogNDAwcHg7XG59XG5cbi5wcm9kdWN0aW9uX2xpc3Qge1xuICBwYWRkaW5nOiA4cHggMDtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/production-list/production-list.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/production-list/production-list.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: ProductionListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionListComponent", function() { return ProductionListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");




var ProductionListComponent = /** @class */ (function () {
    function ProductionListComponent(
    // private metadataService: MetadataService,
    productionService, stateService, transitionService, transition) {
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.length = 100;
        this.pageSize = 10;
        this.pageSizeOptions = [5, 10, 25, 100];
    }
    ProductionListComponent.prototype.ngOnInit = function () {
    };
    ProductionListComponent.prototype.changePageSizeOption = function ($event) {
        var _this = this;
        this.length = $event.length;
        this.pageSize = $event.pageSize;
        this.length = $event.length;
        this.productionService.getProductions($event.pageIndex, $event.pageSize)
            .subscribe(function (productions) {
            _this.productions = productions;
        });
    };
    ProductionListComponent.prototype.createNewProduction = function () {
        this.stateService.go('newproduction');
        //this.productionComponentService.scrollToProduction();
    };
    ProductionListComponent.ctorParameters = function () { return [
        { type: _services_production_service__WEBPACK_IMPORTED_MODULE_3__["ProductionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductionListComponent.prototype, "productions", void 0);
    ProductionListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-production-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./production-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production-list/production-list.component.html")).default,
            host: { 'class': 'p-grid p-dir-col' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./production-list.component.scss */ "./src/app/feature-modules/production/components/production-list/production-list.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_service__WEBPACK_IMPORTED_MODULE_3__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"]])
    ], ProductionListComponent);
    return ProductionListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/production/production.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/production/production.component.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".production_wrapper {\n  height: 400px;\n}\n\n.production {\n  padding: 8px 0;\n}\n\n.role_list_wrapper {\n  height: 400px;\n  width: 100%;\n}\n\n.role_list {\n  padding: 8px 0;\n}\n\n.bobo {\n  background-color: red;\n  color: red;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9wcm9kdWN0aW9uL0Y6XFx3b3JrXFxjYXN0aW5nXFxDYXN0aW5nXFxBbmd1bGFyXFx0YWxlbnQtYXBwL3NyY1xcYXBwXFxmZWF0dXJlLW1vZHVsZXNcXHByb2R1Y3Rpb25cXGNvbXBvbmVudHNcXHByb2R1Y3Rpb25cXHByb2R1Y3Rpb24uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbi9wcm9kdWN0aW9uLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtBQ0NKOztBRENBO0VBQ0ksY0FBQTtBQ0VKOztBRENBO0VBQ0ksYUFBQTtFQUNBLFdBQUE7QUNFSjs7QURBQTtFQUNJLGNBQUE7QUNHSjs7QURBQTtFQUNJLHFCQUFBO0VBQ0EsVUFBQTtBQ0dKIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9wcm9kdWN0aW9uL3Byb2R1Y3Rpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJvZHVjdGlvbl93cmFwcGVye1xyXG4gICAgaGVpZ2h0OjQwMHB4O1xyXG59XHJcbi5wcm9kdWN0aW9ue1xyXG4gICAgcGFkZGluZzo4cHggMDtcclxufVxyXG5cclxuLnJvbGVfbGlzdF93cmFwcGVye1xyXG4gICAgaGVpZ2h0OjQwMHB4O1xyXG4gICAgd2lkdGg6MTAwJTtcclxufVxyXG4ucm9sZV9saXN0e1xyXG4gICAgcGFkZGluZzo4cHggMDtcclxufVxyXG5cclxuLmJvYm8ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7XHJcbiAgICBjb2xvcjpyZWQ7XHJcbn0iLCIucHJvZHVjdGlvbl93cmFwcGVyIHtcbiAgaGVpZ2h0OiA0MDBweDtcbn1cblxuLnByb2R1Y3Rpb24ge1xuICBwYWRkaW5nOiA4cHggMDtcbn1cblxuLnJvbGVfbGlzdF93cmFwcGVyIHtcbiAgaGVpZ2h0OiA0MDBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5yb2xlX2xpc3Qge1xuICBwYWRkaW5nOiA4cHggMDtcbn1cblxuLmJvYm8ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XG4gIGNvbG9yOiByZWQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/production/production.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/production/production.component.ts ***!
  \******************************************************************************************/
/*! exports provided: ProductionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionComponent", function() { return ProductionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _models_metadata__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../models/metadata */ "./src/app/models/metadata.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");








//import { MatDialog } from '@angular/material';




var ProductionComponent = /** @class */ (function () {
    function ProductionComponent(ProductionRoleService, productionExtraService, metadataService, productionService, stateService, transitionService, transition, roleDialogService, productionDialogService, messageDispatchService) {
        this.ProductionRoleService = ProductionRoleService;
        this.productionExtraService = productionExtraService;
        this.metadataService = metadataService;
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.roleDialogService = roleDialogService;
        this.productionDialogService = productionDialogService;
        this.messageDispatchService = messageDispatchService;
    }
    ProductionComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.deleteRoleSubscription = this.ProductionRoleService.subscribeToDeleteRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles.splice(_this.production.roles.indexOf(role), 1);
            }
        });
        this.editRoleSubscription = this.roleDialogService.subscribeToEditRoleObs()
            .subscribe(function (role) {
            if (role) {
                _this.production.roles[_this.production.roles.indexOf(role)] = role;
            }
        });
        this.editShootDetailSubscription = this.productionExtraService.subscribeToEditShootDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.shootDetails[_this.production.shootDetails.indexOf(_this.production.shootDetails.find(function (d) { return d.id == detail.id; }))] = detail;
            }
        });
        this.newShootDetailSubscription = this.productionExtraService.subscribeToCreateShootDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.shootDetails.push(detail);
            }
        });
        this.deleteShootDetailSubscription = this.productionExtraService.subscribeToDeleteShootDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.shootDetails.splice(_this.production.shootDetails.indexOf(_this.production.shootDetails.find(function (d) { return d.id == detail.id; })), 1);
            }
        });
        this.editSideSubscription = this.productionExtraService.subscribeToEditSideObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.sides[_this.production.sides.indexOf(_this.production.sides.find(function (d) { return d.id == detail.id; }))] = detail;
            }
        });
        this.newSideSubscription = this.productionExtraService.subscribeToCreateSideObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.sides.push(detail);
            }
        });
        this.deleteSideSubscription = this.productionExtraService.subscribeToDeleteSideObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.sides.splice(_this.production.sides.indexOf(_this.production.sides.find(function (d) { return d.id == detail.id; })), 1);
            }
        });
        this.editAuditionDetailSubscription = this.productionExtraService.subscribeToEditAuditionDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.auditionDetails[_this.production.auditionDetails.indexOf(_this.production.auditionDetails.find(function (d) { return d.id == detail.id; }))] = detail;
            }
        });
        this.newAuditionDetailSubscription = this.productionExtraService.subscribeToCreateAuditionDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.auditionDetails.push(detail);
            }
        });
        this.deleteAuditionDetailSubscription = this.productionExtraService.subscribeToDeleteAuditionDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.auditionDetails.splice(_this.production.auditionDetails.indexOf(_this.production.auditionDetails.find(function (d) { return d.id == detail.id; })), 1);
            }
        });
        this.editPayDetailSubscription = this.productionExtraService.subscribeToEditPayDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.payDetails[_this.production.payDetails.indexOf(_this.production.payDetails.find(function (d) { return d.id == detail.id; }))] = detail;
            }
        });
        this.newPayDetailSubscription = this.productionExtraService.subscribeToCreatePayDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.payDetails.push(detail);
            }
        });
        this.deletePayDetailSubscription = this.productionExtraService.subscribeToDeletePayDetailObs()
            .subscribe(function (detail) {
            if (detail) {
                _this.production.payDetails.splice(_this.production.payDetails.indexOf(_this.production.payDetails.find(function (d) { return d.id == detail.id; })), 1);
            }
        });
    };
    ProductionComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.newRoleSubscription) {
            this.newRoleSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
        if (this.editShootDetailSubscription) {
            this.editShootDetailSubscription.unsubscribe();
        }
        if (this.newShootDetailSubscription) {
            this.newShootDetailSubscription.unsubscribe();
        }
        if (this.deleteShootDetailSubscription) {
            this.deleteShootDetailSubscription.unsubscribe();
        }
        if (this.editSideSubscription) {
            this.editSideSubscription.unsubscribe();
        }
        if (this.newSideSubscription) {
            this.newSideSubscription.unsubscribe();
        }
        if (this.deleteSideSubscription) {
            this.deleteSideSubscription.unsubscribe();
        }
        if (this.editAuditionDetailSubscription) {
            this.editAuditionDetailSubscription.unsubscribe();
        }
        if (this.newAuditionDetailSubscription) {
            this.newAuditionDetailSubscription.unsubscribe();
        }
        if (this.deleteAuditionDetailSubscription) {
            this.deleteAuditionDetailSubscription.unsubscribe();
        }
        if (this.editPayDetailSubscription) {
            this.editPayDetailSubscription.unsubscribe();
        }
        if (this.newPayDetailSubscription) {
            this.newPayDetailSubscription.unsubscribe();
        }
        if (this.deletePayDetailSubscription) {
            this.deletePayDetailSubscription.unsubscribe();
        }
    };
    ProductionComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        if (false) {}
    };
    ProductionComponent.prototype.addSide = function () {
        this.productionDialogService.showSidesDialog(null);
    };
    ProductionComponent.prototype.addAuditionDetails = function () {
        this.productionDialogService.showAuditionDetailsDialog(null);
    };
    ProductionComponent.prototype.addShootDetails = function () {
        this.productionDialogService.showShootDetailsDialog(null);
    };
    ProductionComponent.prototype.addPayDetails = function () {
        this.productionDialogService.showPayDetailsDialog(null);
    };
    ProductionComponent.prototype.viewAuditionDetails = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showAuditionDetailsDialog(detail);
    };
    ProductionComponent.prototype.viewPayDetails = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showPayDetailsDialog(detail);
    };
    ProductionComponent.prototype.viewSides = function (detail) {
        // this.shootDetail = detail;
        this.productionDialogService.showSidesDialog(detail);
    };
    ProductionComponent.prototype.createNewProduction = function () {
    };
    ProductionComponent.prototype.createNewRole = function () {
        var _this = this;
        if (true) {
            this.productionDialogService.showNewRolesDialog(null);
            // this.stateService.go("productionnewrole", { production: this.production, productionId: this.production.id });
        }
        else {}
    };
    ProductionComponent.prototype.createNewCastingCall = function () {
        this.stateService.go("newcastingCall", { productionId: this.production.id });
    };
    ProductionComponent.ctorParameters = function () { return [
        { type: _services_production_role_service__WEBPACK_IMPORTED_MODULE_8__["ProductionRoleService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_7__["ProductionExtraService"] },
        { type: _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"] },
        { type: _services_production_service__WEBPACK_IMPORTED_MODULE_6__["ProductionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] },
        { type: _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_9__["RoleDialogService"] },
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_10__["ProductionDialogService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_11__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_5__["Production"])
    ], ProductionComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_metadata__WEBPACK_IMPORTED_MODULE_4__["MetadataModel"])
    ], ProductionComponent.prototype, "metadata", void 0);
    ProductionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-production',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./production.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/production/production.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./production.component.scss */ "./src/app/feature-modules/production/components/production/production.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_role_service__WEBPACK_IMPORTED_MODULE_8__["ProductionRoleService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_7__["ProductionExtraService"],
            _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"],
            _services_production_service__WEBPACK_IMPORTED_MODULE_6__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_9__["RoleDialogService"],
            _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_10__["ProductionDialogService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_11__["MessageDispatchService"]])
    ], ProductionComponent);
    return ProductionComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/productions/productions.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/productions/productions.component.scss ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcHJvZHVjdGlvbnMvcHJvZHVjdGlvbnMuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/productions/productions.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/productions/productions.component.ts ***!
  \********************************************************************************************/
/*! exports provided: ProductionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionsComponent", function() { return ProductionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/metadata.service */ "./src/app/services/metadata.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/production-component.service */ "./src/app/feature-modules/production/services/production-component.service.ts");
/* harmony import */ var _animations_router_animation__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../animations/router.animation */ "./src/app/animations/router.animation.ts");







var ProductionsComponent = /** @class */ (function () {
    function ProductionsComponent(metadataService, productionService, stateService, transitionService, transition, productionComponentService) {
        this.metadataService = metadataService;
        this.productionService = productionService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
        this.productionComponentService = productionComponentService;
    }
    ProductionsComponent.prototype.ngOnInit = function () {
    };
    ProductionsComponent.prototype.createNewProduction = function () {
        this.stateService.go('newproduction');
        //this.productionComponentService.scrollToProduction();
    };
    ProductionsComponent.ctorParameters = function () { return [
        { type: _services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"] },
        { type: _services_production_service__WEBPACK_IMPORTED_MODULE_4__["ProductionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] },
        { type: _services_production_component_service__WEBPACK_IMPORTED_MODULE_5__["ProductionComponentService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ProductionsComponent.prototype, "productions", void 0);
    ProductionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-productions',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./productions.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/productions/productions.component.html")).default,
            // make fade in animation available to this component
            animations: [_animations_router_animation__WEBPACK_IMPORTED_MODULE_6__["fadeInAnimation"]],
            // attach the fade in animation to the host (root) element of this component
            host: { '[@fadeInAnimation]': '' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./productions.component.scss */ "./src/app/feature-modules/production/components/productions/productions.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_metadata_service__WEBPACK_IMPORTED_MODULE_3__["MetadataService"],
            _services_production_service__WEBPACK_IMPORTED_MODULE_4__["ProductionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"],
            _services_production_component_service__WEBPACK_IMPORTED_MODULE_5__["ProductionComponentService"]])
    ], ProductionsComponent);
    return ProductionsComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/role-list-item/role-list-item.component.scss":
/*!****************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/role-list-item/role-list-item.component.scss ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcm9sZS1saXN0LWl0ZW0vcm9sZS1saXN0LWl0ZW0uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/role-list-item/role-list-item.component.ts":
/*!**************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/role-list-item/role-list-item.component.ts ***!
  \**************************************************************************************************/
/*! exports provided: RoleListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleListItemComponent", function() { return RoleListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _uirouter_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @uirouter/core */ "./node_modules/@uirouter/core/lib-esm/index.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/production/services/role-dialog.service.ts");
/* harmony import */ var _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/confirmation-dialog.service */ "./src/app/services/confirmation-dialog.service.ts");
/* harmony import */ var _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../services/production-role.service */ "./src/app/services/production-role.service.ts");







var RoleListItemComponent = /** @class */ (function () {
    function RoleListItemComponent(ProductionRoleService, confirmationService, roleDialogService, stateService, transitionService, transition) {
        this.ProductionRoleService = ProductionRoleService;
        this.confirmationService = confirmationService;
        this.roleDialogService = roleDialogService;
        this.stateService = stateService;
        this.transitionService = transitionService;
        this.transition = transition;
    }
    RoleListItemComponent.prototype.ngOnInit = function () {
    };
    RoleListItemComponent.prototype.ngOnDestroy = function () {
        if (this.editRoleSubscription) {
            this.editRoleSubscription.unsubscribe();
        }
        if (this.deleteRoleSubscription) {
            this.deleteRoleSubscription.unsubscribe();
        }
    };
    RoleListItemComponent.prototype.editRoleClick = function (role) {
        var _this = this;
        if (true) {
            this.stateService.go("editrole", { production: this.production, productionId: this.production.id, role: role, roleId: role.id });
        }
        else {}
    };
    RoleListItemComponent.prototype.deleteRoleClick = function (role) {
        var _this = this;
        //setTimeout(() => {
        this.deleteRoleSubscription = this.confirmationService.confirmAction("Do you really want to delete role '" + this.role.name + "' from production '" + this.production.name + "'?")
            .subscribe(function (result) {
            if (result) {
                _this.ProductionRoleService.deleteRole(_this.role.id, _this.role.productionId).subscribe(function (role) {
                }, function (error) {
                    console.log("error " + error);
                });
            }
            _this.deleteRoleSubscription.unsubscribe();
        });
        //});
    };
    RoleListItemComponent.ctorParameters = function () { return [
        { type: _services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"] },
        { type: _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmationDialogService"] },
        { type: _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_4__["RoleDialogService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"] },
        { type: _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_3__["Production"])
    ], RoleListItemComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], RoleListItemComponent.prototype, "role", void 0);
    RoleListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-list-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./role-list-item.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/role-list-item/role-list-item.component.html")).default,
            host: { 'class': 'p-col' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./role-list-item.component.scss */ "./src/app/feature-modules/production/components/role-list-item/role-list-item.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_role_service__WEBPACK_IMPORTED_MODULE_6__["ProductionRoleService"], _services_confirmation_dialog_service__WEBPACK_IMPORTED_MODULE_5__["ConfirmationDialogService"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_4__["RoleDialogService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["StateService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["TransitionService"],
            _uirouter_core__WEBPACK_IMPORTED_MODULE_2__["Transition"]])
    ], RoleListItemComponent);
    return RoleListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/role-list/role-list.component.scss":
/*!******************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/role-list/role-list.component.scss ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvcm9sZS1saXN0L3JvbGUtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/role-list/role-list.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/role-list/role-list.component.ts ***!
  \****************************************************************************************/
/*! exports provided: RoleListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleListComponent", function() { return RoleListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/role-dialog.service */ "./src/app/feature-modules/production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production.service */ "./src/app/services/production.service.ts");





var RoleListComponent = /** @class */ (function () {
    function RoleListComponent(productionService, roleDialogService) {
        this.productionService = productionService;
        this.roleDialogService = roleDialogService;
        this.roles = [];
    }
    RoleListComponent.prototype.ngOnInit = function () {
    };
    RoleListComponent.ctorParameters = function () { return [
        { type: _services_production_service__WEBPACK_IMPORTED_MODULE_4__["ProductionService"] },
        { type: _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_3__["RoleDialogService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["Production"])
    ], RoleListComponent.prototype, "production", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], RoleListComponent.prototype, "roles", void 0);
    RoleListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-role-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./role-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/role-list/role-list.component.html")).default,
            host: { 'class': 'p-grid p-dir-col' },
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./role-list.component.scss */ "./src/app/feature-modules/production/components/role-list/role-list.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_service__WEBPACK_IMPORTED_MODULE_4__["ProductionService"], _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_3__["RoleDialogService"]])
    ], RoleListComponent);
    return RoleListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.scss":
/*!****************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.scss ***!
  \****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2hvb3QtZGV0YWlscy1kaWFsb2cvc2hvb3QtZGV0YWlscy1kaWFsb2cuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.ts":
/*!**************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.ts ***!
  \**************************************************************************************************************/
/*! exports provided: ShootDetailsDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShootDetailsDialogComponent", function() { return ShootDetailsDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm5/primeng-dialog.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");









var ShootDetailsDialogComponent = /** @class */ (function () {
    function ShootDetailsDialogComponent(productionDialogService, productionExtraService, _formBuilder, mediaMatcher, messageDispatchService) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this._formBuilder = _formBuilder;
        this.mediaMatcher = mediaMatcher;
        this.messageDispatchService = messageDispatchService;
        this.isSmallDevice = false;
        this.messages = [];
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.isOneDayOnly = false;
        this.datesToBeDecidedLater = false;
        this.isVisible = false;
    }
    ShootDetailsDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSmallDevice = window.innerWidth < 768;
        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));
        var today = new Date();
        var month = today.getMonth();
        var year = today.getFullYear();
        this.minDate = new Date();
        this.minDate.setDate(today.getDate() + 1);
        this.maxDate = new Date();
        this.maxDate.setDate(today.getDate() + 1);
        this.basicDialogInfo = this._formBuilder.group({
            location: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            locationDetail: [''],
            isOneDayOnly: [false],
            details: [''],
            instructions: [''],
            startDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            endDate: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required],
            datesToBeDecidedLater: [false],
            datesAreTemptative: [false]
        }, { updateOn: 'blur' });
        this.basicDialogInfo.patchValue({
            isOneDayOnly: false,
        });
        this.basicDialogInfo.controls['endDate'].enable();
        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToShootDetailsDialogShowStateObs()
            .subscribe(function (dialogObject) {
            _this.clearMessages();
            _this.formDirective.resetForm();
            _this.basicDialogInfo.reset();
            if (dialogObject.isVisible && dialogObject.shootDetail != null) {
                _this.shootDetail = dialogObject.shootDetail;
                _this.basicDialogInfo.patchValue({
                    location: _this.shootDetail.location,
                    locationDetail: _this.shootDetail.locationDetail,
                    isOneDayOnly: _this.shootDetail.isOneDayOnly,
                    details: _this.shootDetail.details,
                    instructions: _this.shootDetail.instructions,
                    startDate: new Date(_this.shootDetail.startDate),
                    endDate: new Date(_this.shootDetail.endDate),
                    name: _this.shootDetail.name,
                    datesToBeDecidedLater: _this.shootDetail.datesToBeDecidedLater,
                    datesAreTemptative: _this.shootDetail.datesAreTemptative
                });
                if (_this.shootDetail.datesToBeDecidedLater) {
                    _this.dateDecidionChange(_this.shootDetail.datesToBeDecidedLater);
                }
                else {
                    //check if one day only
                    if (_this.shootDetail.isOneDayOnly) {
                        _this.oneDayAuditionChange(_this.shootDetail.isOneDayOnly);
                    }
                    else {
                        _this.basicDialogInfo.controls['endDate'].enable();
                        _this.basicDialogInfo.controls['startDate'].enable();
                    }
                }
            }
            else {
                _this.shootDetail = null;
                _this.basicDialogInfo.patchValue({
                    location: '',
                    locationDetail: '',
                    isOneDayOnly: false,
                    details: '',
                    instructions: '',
                    startDate: '',
                    endDate: '',
                    name: '',
                    datesToBeDecidedLater: false,
                    datesAreTemptative: false
                });
                _this.dateDecidionChange(false);
                _this.oneDayAuditionChange(false);
            }
            _this.isVisible = dialogObject.isVisible;
            // this.detaildialog.maximized = true;
            //this.detaildialog.toggleMaximize();
            _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            _this.nameField.nativeElement.focus();
        });
    };
    ShootDetailsDialogComponent.prototype.ngAfterViewInit = function () {
    };
    ShootDetailsDialogComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();
        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);
    };
    ShootDetailsDialogComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    ShootDetailsDialogComponent.prototype.mediaQueryBigChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = false;
        }
    };
    ShootDetailsDialogComponent.prototype.mediaQuerySmallChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = true;
        }
    };
    ShootDetailsDialogComponent.prototype.selectStartDate = function (date) {
        this.basicDialogInfo.patchValue({
            startDate: date,
        });
    };
    ShootDetailsDialogComponent.prototype.selectEndDate = function (date) {
        this.basicDialogInfo.patchValue({
            endDate: date,
        });
    };
    ShootDetailsDialogComponent.prototype.oneDayAuditionChange = function (value) {
        this.isOneDayOnly = value;
        if (value) {
            this.basicDialogInfo.controls['endDate'].disable();
        }
        else {
            if (!this.datesToBeDecidedLater) {
                this.basicDialogInfo.controls['endDate'].enable();
            }
        }
        this.basicDialogInfo.patchValue({
            endDate: '',
        });
    };
    ShootDetailsDialogComponent.prototype.clickOneDaySwitch = function (selection) {
        this.oneDayAuditionChange(selection.checked);
    };
    ShootDetailsDialogComponent.prototype.dateDecidionChange = function (value) {
        this.datesToBeDecidedLater = value;
        if (value) {
            this.basicDialogInfo.controls['endDate'].disable();
            this.basicDialogInfo.controls['startDate'].disable();
        }
        else {
            this.basicDialogInfo.controls['endDate'].enable();
            if (!this.isOneDayOnly) {
                this.basicDialogInfo.controls['startDate'].enable();
            }
        }
        this.basicDialogInfo.patchValue({
            endDate: '',
        });
        this.basicDialogInfo.patchValue({
            startDate: '',
        });
    };
    ShootDetailsDialogComponent.prototype.clickDateDecisionSwitch = function (selection) {
        this.dateDecidionChange(selection.checked);
    };
    ShootDetailsDialogComponent.prototype.onDialogShow = function (event, dialog) {
        if (this.isSmallDevice) {
            setTimeout(function () {
                dialog.maximize();
            }, 0);
        }
    };
    ShootDetailsDialogComponent.prototype.onDialogHide = function (event, dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();
    };
    ShootDetailsDialogComponent.prototype.cancelClick = function () {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
    };
    ShootDetailsDialogComponent.prototype.resetDialog = function () {
        this.saveIsDisabled = false;
        this.cancelIsDisabled = false;
        this.messages = [];
    };
    ShootDetailsDialogComponent.prototype.saveClick = function () {
        var _this = this;
        this.clearMessages();
        if (this.basicDialogInfo.valid) {
            this.saveIsDisabled = true;
            this.cancelIsDisabled = true;
            var tshootDetail_1 = new _models_production_models__WEBPACK_IMPORTED_MODULE_5__["ShootDetail"];
            tshootDetail_1.productionId = this.productionId;
            tshootDetail_1.name = this.basicDialogInfo.get('name').value;
            tshootDetail_1.location = this.basicDialogInfo.get('location').value;
            tshootDetail_1.locationDetail = this.basicDialogInfo.get('locationDetail').value;
            tshootDetail_1.details = this.basicDialogInfo.get('details').value;
            tshootDetail_1.instructions = this.basicDialogInfo.get('instructions').value;
            tshootDetail_1.startDate = this.basicDialogInfo.get('startDate').value;
            tshootDetail_1.endDate = this.basicDialogInfo.get('endDate').value;
            tshootDetail_1.isOneDayOnly = this.basicDialogInfo.get('isOneDayOnly').value;
            tshootDetail_1.datesToBeDecidedLater = this.basicDialogInfo.get('datesToBeDecidedLater').value;
            tshootDetail_1.datesAreTemptative = this.basicDialogInfo.get('datesAreTemptative').value;
            if (this.shootDetail && this.shootDetail.id && this.shootDetail.id !== 0) {
                this.productionExtraService.saveShootDetail(this.shootDetail.id, this.productionId, tshootDetail_1).subscribe(function (shootDetail) {
                    //this.shootDetail = shootDetail;
                    _this.formDirective.resetForm();
                    _this.basicDialogInfo.reset();
                    _this.isVisible = false;
                    _this.messageDispatchService.dispatchSuccesMessage("Shoot detail '" + shootDetail.name + "' was saved successfully", "Shoot Detail Saved");
                }, function (error) {
                    _this.saveIsDisabled = false;
                    _this.cancelIsDisabled = false;
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Shoot Detail Save Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Shoot Detail Save Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
            else {
                this.productionExtraService.createShootDetail(this.productionId, tshootDetail_1).subscribe(function (shootDetail) {
                    if (shootDetail != null) {
                        _this.formDirective.resetForm();
                        _this.basicDialogInfo.reset();
                        _this.isVisible = false;
                        _this.messageDispatchService.dispatchSuccesMessage("New shoot detail '" + shootDetail.name + "' was created successfully", "Shoot Detail Created");
                    }
                }, function (error) {
                    _this.saveIsDisabled = false;
                    _this.cancelIsDisabled = false;
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Shoot Detail Creation Error");
                    _this.messages.push({
                        severity: 'error',
                        summary: "Shoot Detail Creation Error",
                        detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                    });
                });
            }
        }
        else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('location').valid) {
                this.basicDialogInfo.get('location').markAsDirty();
                this.locationField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.locationField.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('startDate').valid) {
                this.basicDialogInfo.get('startDate').markAsDirty();
                this.startDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.startDateField.inputfieldViewChild.nativeElement.focus();
            }
            else if (!this.basicDialogInfo.get('endDate').valid) {
                this.basicDialogInfo.get('endDate').markAsDirty();
                this.endDateField.inputfieldViewChild.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.endDateField.inputfieldViewChild.nativeElement.focus();
            }
        }
    };
    ShootDetailsDialogComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_5__["ShootDetail"])
    ], ShootDetailsDialogComponent.prototype, "shootDetail", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], ShootDetailsDialogComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formDirective', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"])
    ], ShootDetailsDialogComponent.prototype, "formDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ShootDetailsDialogComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('location', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ShootDetailsDialogComponent.prototype, "locationField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('locationDetail', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], ShootDetailsDialogComponent.prototype, "locationDetailField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('startDate', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ShootDetailsDialogComponent.prototype, "startDateField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('endDate', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ShootDetailsDialogComponent.prototype, "endDateField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('detaildialog', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dialog__WEBPACK_IMPORTED_MODULE_7__["Dialog"])
    ], ShootDetailsDialogComponent.prototype, "detaildialog", void 0);
    ShootDetailsDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-shoot-details-dialog',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./shoot-details-dialog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./shoot-details-dialog.component.scss */ "./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_8__["MessageDispatchService"]])
    ], ShootDetailsDialogComponent);
    return ShootDetailsDialogComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.scss":
/*!**********************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.scss ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2hvb3QtZGV0YWlscy1saXN0LWl0ZW0vc2hvb3QtZGV0YWlscy1saXN0LWl0ZW0uY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.ts":
/*!********************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.ts ***!
  \********************************************************************************************************************/
/*! exports provided: ShootDetailsListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShootDetailsListItemComponent", function() { return ShootDetailsListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");
/* harmony import */ var _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/confirm-decision-subscription.service */ "./src/app/services/confirm-decision-subscription.service.ts");








var ShootDetailsListItemComponent = /** @class */ (function () {
    function ShootDetailsListItemComponent(productionDialogService, productionExtraService, messageDispatchService, messageService, decisionDispatcher) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
        this.messageService = messageService;
        this.decisionDispatcher = decisionDispatcher;
    }
    ShootDetailsListItemComponent.prototype.ngOnInit = function () {
    };
    ShootDetailsListItemComponent.prototype.ngOnDestroy = function () {
        if (this.decisionDispatch) {
            this.decisionDispatch.unsubscribe();
        }
    };
    ShootDetailsListItemComponent.prototype.viewDetails = function () {
        this.productionDialogService.showShootDetailsDialog(this.detail);
    };
    ShootDetailsListItemComponent.prototype.deleteDetails = function () {
        var _this = this;
        this.decisionDispatcher.poseConfirmDecision('Delete shootdetail ' + this.detail.name + '?', 'Confirm to proceed');
        this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
            .subscribe(function (decision) {
            _this.decisionDispatch.unsubscribe();
            if (decision) {
                _this.productionExtraService.deleteShootDetail(_this.detail.id, _this.detail.productionId).subscribe(function (shootDetail) {
                    //this.shootDetail = shootDetail;
                    _this.messageDispatchService.dispatchSuccesMessage("Shoot detail '" + _this.detail.name + "' was deleted successfully", "Shoot Detail Deleted");
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Shoot Detail Save Error");
                });
            }
        });
    };
    ShootDetailsListItemComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"] },
        { type: primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"] },
        { type: _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ShootDetail"])
    ], ShootDetailsListItemComponent.prototype, "detail", void 0);
    ShootDetailsListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-shoot-details-list-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./shoot-details-list-item.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./shoot-details-list-item.component.scss */ "./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"],
            primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
            _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"]])
    ], ShootDetailsListItemComponent);
    return ShootDetailsListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.scss":
/*!************************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.scss ***!
  \************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2hvb3QtZGV0YWlscy1saXN0L3Nob290LWRldGFpbHMtbGlzdC5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.ts ***!
  \**********************************************************************************************************/
/*! exports provided: ShootDetailsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShootDetailsListComponent", function() { return ShootDetailsListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");




var ShootDetailsListComponent = /** @class */ (function () {
    function ShootDetailsListComponent(productionExtraService, messageDispatchService) {
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
    }
    ShootDetailsListComponent.prototype.ngOnInit = function () {
        this.editShootDetailSubscription = this.productionExtraService.subscribeToEditShootDetailObs()
            .subscribe(function (detail) {
        });
        this.newShootDetailSubscription = this.productionExtraService.subscribeToCreateShootDetailObs()
            .subscribe(function (detail) {
        });
        this.deleteShootDetailSubscription = this.productionExtraService.subscribeToDeleteShootDetailObs()
            .subscribe(function (detail) {
            if (detail) {
            }
        });
    };
    ShootDetailsListComponent.prototype.ngOnDestroy = function () {
        if (this.editShootDetailSubscription) {
            this.editShootDetailSubscription.unsubscribe();
        }
        if (this.newShootDetailSubscription) {
            this.newShootDetailSubscription.unsubscribe();
        }
        if (this.deleteShootDetailSubscription) {
            this.deleteShootDetailSubscription.unsubscribe();
        }
    };
    ShootDetailsListComponent.ctorParameters = function () { return [
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], ShootDetailsListComponent.prototype, "shootDetails", void 0);
    ShootDetailsListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-shoot-details-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./shoot-details-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./shoot-details-list.component.scss */ "./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"]])
    ], ShootDetailsListComponent);
    return ShootDetailsListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.scss":
/*!********************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.scss ***!
  \********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".fixed {\n  position: fixed;\n  top: 40px;\n  z-index: 999;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9zaWRlLXF1aWNrLWxpbmtzL0Y6XFx3b3JrXFxjYXN0aW5nXFxDYXN0aW5nXFxBbmd1bGFyXFx0YWxlbnQtYXBwL3NyY1xcYXBwXFxmZWF0dXJlLW1vZHVsZXNcXHByb2R1Y3Rpb25cXGNvbXBvbmVudHNcXHNpZGUtcXVpY2stbGlua3NcXHNpZGUtcXVpY2stbGlua3MuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2lkZS1xdWljay1saW5rcy9zaWRlLXF1aWNrLWxpbmtzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsZUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FDQ0YiLCJmaWxlIjoic3JjL2FwcC9mZWF0dXJlLW1vZHVsZXMvcHJvZHVjdGlvbi9jb21wb25lbnRzL3NpZGUtcXVpY2stbGlua3Mvc2lkZS1xdWljay1saW5rcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5maXhlZCB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIHRvcDogNDBweDtcclxuICB6LWluZGV4OiA5OTk7XHJcbn0iLCIuZml4ZWQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHRvcDogNDBweDtcbiAgei1pbmRleDogOTk5O1xufSJdfQ== */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.ts":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.ts ***!
  \******************************************************************************************************/
/*! exports provided: SideQuickLinksComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideQuickLinksComponent", function() { return SideQuickLinksComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/production-component.service */ "./src/app/feature-modules/production/services/production-component.service.ts");



var SideQuickLinksComponent = /** @class */ (function () {
    function SideQuickLinksComponent(productionComponentService) {
        this.productionComponentService = productionComponentService;
        this.fixed = false;
    }
    SideQuickLinksComponent.prototype.onWindowScroll = function ($event) {
        /* let top = window.scrollY;
         if (top > 50) {
           this.fixed = true;
           console.log(top);
         } else if (this.fixed && top < 5) {
           this.fixed = false;
           console.log(top);
         }*/
        // $event.stopPropagation();
    };
    SideQuickLinksComponent.prototype.ngOnInit = function () {
    };
    SideQuickLinksComponent.prototype.scrollToProduction = function () {
        this.productionComponentService.scrollToProduction();
    };
    SideQuickLinksComponent.prototype.scrollToArtistic = function () {
        this.productionComponentService.scrollToArtistic();
    };
    SideQuickLinksComponent.prototype.scrollToCasting = function () {
        this.productionComponentService.scrollToCasting();
    };
    SideQuickLinksComponent.prototype.scrollToRoles = function () {
        this.productionComponentService.scrollToRoles();
    };
    SideQuickLinksComponent.prototype.scrollToBasic = function () {
        this.productionComponentService.scrollToBasic();
    };
    SideQuickLinksComponent.prototype.scrollToOther = function () {
        this.productionComponentService.scrollToOther();
    };
    SideQuickLinksComponent.prototype.saveProductionClick = function () {
        this.productionComponentService.trySaveProduction();
    };
    SideQuickLinksComponent.prototype.deleteProductionClick = function () {
        this.productionComponentService.tryDeleteProduction();
    };
    SideQuickLinksComponent.prototype.cancelEditProductionClick = function () {
        this.productionComponentService.tryCancelEditProduction();
    };
    SideQuickLinksComponent.ctorParameters = function () { return [
        { type: _services_production_component_service__WEBPACK_IMPORTED_MODULE_2__["ProductionComponentService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("window:scroll", ['$event']),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], SideQuickLinksComponent.prototype, "onWindowScroll", null);
    SideQuickLinksComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-side-quick-links',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./side-quick-links.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./side-quick-links.component.scss */ "./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_component_service__WEBPACK_IMPORTED_MODULE_2__["ProductionComponentService"]])
    ], SideQuickLinksComponent);
    return SideQuickLinksComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.scss ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".uploadfilecontainer {\n  background-image: url('/scripts/ngapp/talent-app/cloud-2044823_960_720.png');\n  background-repeat: no-repeat;\n  background-size: 100px;\n  background-position: center;\n  height: 200px;\n  /*margin: 20px auto;*/\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border: 2px dashed #1C8ADB;\n  border-radius: 10px;\n}\n\n.uploadfilecontainer:hover {\n  cursor: pointer;\n  background-color: #9ecbec !important;\n  opacity: 0.8;\n}\n\n.files-list {\n  /*display: flex;\n  justify-content: space-between;\n  margin: 10px auto;*/\n  margin-top: 10px;\n  margin-bottom: 10px;\n  background: #ffffff;\n  border: 1px dashed;\n  border-radius: 12px;\n  padding: 5px;\n  color: #1c8adb;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9zaWRlcy1kaWFsb2cvRjpcXHdvcmtcXGNhc3RpbmdcXENhc3RpbmdcXEFuZ3VsYXJcXHRhbGVudC1hcHAvc3JjXFxhcHBcXGZlYXR1cmUtbW9kdWxlc1xccHJvZHVjdGlvblxcY29tcG9uZW50c1xcc2lkZXMtZGlhbG9nXFxzaWRlcy1kaWFsb2cuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2lkZXMtZGlhbG9nL3NpZGVzLWRpYWxvZy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDRFQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7QUNDRjs7QURFQTtFQUNJLGVBQUE7RUFDQSxvQ0FBQTtFQUNBLFlBQUE7QUNDSjs7QURFQTtFQUNJOztxQkFBQTtFQUdBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvZmVhdHVyZS1tb2R1bGVzL3Byb2R1Y3Rpb24vY29tcG9uZW50cy9zaWRlcy1kaWFsb2cvc2lkZXMtZGlhbG9nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVwbG9hZGZpbGVjb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uLy4uLy4uL2Fzc2V0cy9jbG91ZC0yMDQ0ODIzXzk2MF83MjAucG5nXCIpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1zaXplOiAxMDBweDtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxuICAvKm1hcmdpbjogMjBweCBhdXRvOyovXHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIGJvcmRlcjogMnB4IGRhc2hlZCAjMUM4QURCO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn1cclxuXHJcbi51cGxvYWRmaWxlY29udGFpbmVyOmhvdmVyIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM5ZWNiZWMgIWltcG9ydGFudDtcclxuICAgIG9wYWNpdHk6IDAuODtcclxufVxyXG5cclxuLmZpbGVzLWxpc3Qge1xyXG4gICAgLypkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgbWFyZ2luOiAxMHB4IGF1dG87Ki9cclxuICAgIG1hcmdpbi10b3A6MTBweDtcclxuICAgIG1hcmdpbi1ib3R0b206MTBweDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICBib3JkZXI6IDFweCBkYXNoZWQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6ICMxYzhhZGI7XHJcbn1cclxuIiwiLnVwbG9hZGZpbGVjb250YWluZXIge1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCIuLi8uLi8uLi8uLi8uLi9hc3NldHMvY2xvdWQtMjA0NDgyM185NjBfNzIwLnBuZ1wiKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAxMDBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBoZWlnaHQ6IDIwMHB4O1xuICAvKm1hcmdpbjogMjBweCBhdXRvOyovXG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJvcmRlcjogMnB4IGRhc2hlZCAjMUM4QURCO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4udXBsb2FkZmlsZWNvbnRhaW5lcjpob3ZlciB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzllY2JlYyAhaW1wb3J0YW50O1xuICBvcGFjaXR5OiAwLjg7XG59XG5cbi5maWxlcy1saXN0IHtcbiAgLypkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIG1hcmdpbjogMTBweCBhdXRvOyovXG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlcjogMXB4IGRhc2hlZDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogIzFjOGFkYjtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: SidesDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidesDialogComponent", function() { return SidesDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/layout */ "./node_modules/@angular/cdk/esm5/layout.es5.js");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var _services_file_upload_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/file-upload.service */ "./src/app/services/file-upload.service.ts");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var primeng_dialog__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! primeng/dialog */ "./node_modules/primeng/fesm5/primeng-dialog.js");
/* harmony import */ var _models_blobs__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../../models/blobs */ "./src/app/models/blobs.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");











var SidesDialogComponent = /** @class */ (function () {
    function SidesDialogComponent(productionDialogService, productionExtraService, _formBuilder, mediaMatcher, fileUploadService, messageDispatchService) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this._formBuilder = _formBuilder;
        this.mediaMatcher = mediaMatcher;
        this.fileUploadService = fileUploadService;
        this.messageDispatchService = messageDispatchService;
        this.isSmallDevice = false;
        this.sideFiles = [];
        this.messages = [];
        this.saveIsDisabled = true;
        this.cancelIsDisabled = false;
        this.selectIsDisabled = false;
        this.dropPanelIsHiden = false;
        this.nameIsDisable = false;
        this.removeFileIsDisabled = false;
        this.uploadWasSuccessfull = false;
        this.successfullFileUploadId = "";
        this.isVisible = false;
    }
    SidesDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSmallDevice = window.innerWidth < 768;
        this.bigSizeMatcher = this.mediaMatcher.matchMedia('(min-width: 768px)');
        this.bigSizeMatcher.addListener(this.mediaQueryBigChangeListener.bind(this));
        this.smallSizeMatcher = this.mediaMatcher.matchMedia('(max-width: 767px)');
        this.smallSizeMatcher.addListener(this.mediaQuerySmallChangeListener.bind(this));
        this.basicDialogInfo = this._formBuilder.group({
            name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__["Validators"].required]
        }, { updateOn: 'blur' });
        this.dialogVisibleStateSubscription = this.productionDialogService.subscribeToSidesDialogShowStateObs()
            .subscribe(function (dialogObject) {
            _this.clearMessages();
            _this.formDirective.resetForm();
            _this.basicDialogInfo.reset();
            if (dialogObject.isVisible && dialogObject.side != null) {
                _this.side = dialogObject.side;
                _this.basicDialogInfo.patchValue({
                    name: _this.side.name
                });
            }
            else {
                _this.side = null;
                _this.basicDialogInfo.patchValue({
                    name: ''
                });
            }
            _this.isVisible = dialogObject.isVisible;
            // this.detaildialog.maximized = true;
            //this.detaildialog.toggleMaximize();
            _this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
            _this.nameField.nativeElement.focus();
        });
    };
    SidesDialogComponent.prototype.ngAfterViewInit = function () {
    };
    SidesDialogComponent.prototype.ngOnDestroy = function () {
        this.productionDialogService.hideSidesDialog();
        if (this.dialogVisibleStateSubscription) {
            this.dialogVisibleStateSubscription.unsubscribe();
        }
        if (this.fileUploadSubscription) {
            this.fileUploadSubscription.unsubscribe();
        }
        this.bigSizeMatcher.removeListener(this.mediaQueryBigChangeListener);
        this.smallSizeMatcher.removeListener(this.mediaQuerySmallChangeListener);
    };
    SidesDialogComponent.prototype.clearMessages = function () {
        this.messages = [];
    };
    SidesDialogComponent.prototype.addFiles = function () {
        this.file.nativeElement.click();
    };
    SidesDialogComponent.prototype.onFilesAdded = function () {
        var files = this.file.nativeElement.files;
        for (var key in files) {
            if (!isNaN(parseInt(key))) {
                this.sideFiles.push(new _models_blobs__WEBPACK_IMPORTED_MODULE_9__["ProductionSideFile"](0, files[key], 0, this.productionId));
            }
        }
        if (this.sideFiles && this.sideFiles.length > 0) {
            this.dropPanelIsHiden = true;
            this.saveIsDisabled = false;
            this.selectIsDisabled = true;
        }
    };
    SidesDialogComponent.prototype.remove = function (fileUploadState) {
        this.clearMessages();
        if (fileUploadState.upload) {
            fileUploadState.upload.abort();
        }
        this.sideFiles.splice(this.sideFiles.indexOf(fileUploadState), 1);
        this.saveIsDisabled = true;
        this.selectIsDisabled = false;
        this.dropPanelIsHiden = false;
    };
    SidesDialogComponent.prototype.onFilesDropped = function (event) {
        if (event.length > 1) {
        }
        else if (event.length == 1) {
            this.dropPanelIsHiden = true;
            this.saveIsDisabled = false;
            this.selectIsDisabled = true;
            this.sideFiles.push(new _models_blobs__WEBPACK_IMPORTED_MODULE_9__["ProductionSideFile"](0, event[0], 0, this.productionId));
        }
    };
    SidesDialogComponent.prototype.mediaQueryBigChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = false;
        }
    };
    SidesDialogComponent.prototype.mediaQuerySmallChangeListener = function (event) {
        if (event.matches) {
            this.isSmallDevice = true;
        }
    };
    SidesDialogComponent.prototype.onDialogShow = function (event, dialog) {
        if (this.isSmallDevice) {
            setTimeout(function () {
                dialog.maximize();
            }, 0);
        }
    };
    SidesDialogComponent.prototype.onDialogHide = function (event, dialog) {
        this.isVisible = false;
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
        //dialog.revertMaximize();
        this.resetDialog();
    };
    SidesDialogComponent.prototype.cancelClick = function () {
        this.isVisible = false;
        this.clearMessages();
        this.formDirective.resetForm();
        this.basicDialogInfo.reset();
    };
    SidesDialogComponent.prototype.resetDialog = function () {
        this.sideFiles = [];
        this.saveIsDisabled = true;
        this.selectIsDisabled = false;
        this.dropPanelIsHiden = false;
        this.nameIsDisable = false;
        this.removeFileIsDisabled = false;
        this.cancelIsDisabled = false;
        this.uploadWasSuccessfull = false;
        this.successfullFileUploadId = "";
        this.messages = [];
    };
    SidesDialogComponent.prototype.saveFormData = function () {
        var _this = this;
        var tside = new _models_production_models__WEBPACK_IMPORTED_MODULE_6__["Side"];
        tside.productionId = this.productionId;
        tside.name = this.basicDialogInfo.get('name').value;
        tside.fileName = this.successfullFileUploadId;
        tside.fileId = this.successfullFileUploadId;
        if (this.side && this.side.id && this.side.id !== 0) {
            this.productionExtraService.saveSide(this.side.id, this.productionId, tside).subscribe(function (side) {
                //this.side = side;
                _this.formDirective.resetForm();
                _this.basicDialogInfo.reset();
                _this.isVisible = false;
                _this.messageDispatchService.dispatchSuccesMessage("New Side '" + side.name + "' was created successfully", "Side Created");
            }, function (error) {
                _this.nameIsDisable = false;
                _this.saveIsDisabled = false;
                _this.selectIsDisabled = true;
                _this.dropPanelIsHiden = true;
                _this.removeFileIsDisabled = true;
                _this.cancelIsDisabled = false;
                _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Side Creation Error");
                _this.messages.push({
                    severity: 'error',
                    summary: "Side Creation Error",
                    detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                });
            });
        }
        else {
            this.productionExtraService.createSide(this.productionId, tside).subscribe(function (side) {
                if (side != null) {
                    _this.formDirective.resetForm();
                    _this.basicDialogInfo.reset();
                    _this.isVisible = false;
                    _this.messageDispatchService.dispatchSuccesMessage("New Side '" + side.name + "' was created successfully", "Side Created");
                }
            }, function (error) {
                _this.nameIsDisable = false;
                _this.saveIsDisabled = false;
                _this.selectIsDisabled = true;
                _this.dropPanelIsHiden = true;
                _this.removeFileIsDisabled = true;
                _this.cancelIsDisabled = false;
                _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Side Creation Error");
                _this.messages.push({
                    severity: 'error',
                    summary: "Side Creation Error",
                    detail:  true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined
                });
            });
        }
    };
    SidesDialogComponent.prototype.uploadFile = function () {
        var _this = this;
        this.fileUploadSubscription = this.sideFiles[0].obs.asObservable()
            .subscribe(function (blobUploadUpdate) {
            if (_this.sideFiles[0].isComplete && _this.sideFiles[0].isSuccess) {
                _this.uploadWasSuccessfull = true;
                _this.successfullFileUploadId = _this.sideFiles[0].uploadedFileId;
                _this.messageDispatchService.dispatchSuccesMessage("File " + _this.sideFiles[0].file.name + "." + _this.sideFiles[0].file.type + " uploaded successfully", "Upload Success");
                _this.fileUploadSubscription.unsubscribe();
                _this.nameIsDisable = false;
                _this.saveIsDisabled = false;
                _this.selectIsDisabled = true;
                _this.dropPanelIsHiden = true;
                _this.removeFileIsDisabled = true;
                _this.cancelIsDisabled = false;
                _this.saveFormData();
            }
            else if (_this.sideFiles[0].isComplete && _this.sideFiles[0].isError) {
                _this.nameIsDisable = false;
                _this.saveIsDisabled = false;
                _this.selectIsDisabled = true;
                _this.dropPanelIsHiden = true;
                _this.removeFileIsDisabled = false;
                _this.cancelIsDisabled = false;
                _this.messageDispatchService.dispatchErrorMessage(_this.sideFiles[0].error, "Upload Error");
                _this.messages.push({
                    severity: 'error',
                    summary: "Upload Error",
                    detail: _this.sideFiles[0].error
                });
                _this.fileUploadSubscription.unsubscribe();
                //this.sideFiles[0] = blobUploadUpdate;
            }
        });
        this.fileUploadService.uploadFiles(this.sideFiles);
    };
    SidesDialogComponent.prototype.saveClick = function () {
        this.clearMessages();
        if (this.basicDialogInfo.valid) {
            var tside = new _models_production_models__WEBPACK_IMPORTED_MODULE_6__["Side"];
            tside.productionId = this.productionId;
            tside.name = this.basicDialogInfo.get('name').value;
            if (this.uploadWasSuccessfull && this.successfullFileUploadId.length > 0) {
                this.saveFormData();
            }
            else {
                this.nameIsDisable = true;
                this.saveIsDisabled = true;
                this.selectIsDisabled = true;
                this.dropPanelIsHiden = true;
                this.removeFileIsDisabled = true;
                this.cancelIsDisabled = true;
                this.uploadFile();
            }
        }
        else {
            if (!this.basicDialogInfo.get('name').valid) {
                this.basicDialogInfo.get('name').markAsDirty();
                this.nameField.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
                this.nameField.nativeElement.focus();
            }
        }
    };
    SidesDialogComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"] },
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"] },
        { type: _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"] },
        { type: _services_file_upload_service__WEBPACK_IMPORTED_MODULE_5__["FileUploadService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_10__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_6__["Side"])
    ], SidesDialogComponent.prototype, "side", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], SidesDialogComponent.prototype, "productionId", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('formDirective', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgForm"])
    ], SidesDialogComponent.prototype, "formDirective", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('name', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])
    ], SidesDialogComponent.prototype, "nameField", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('detaildialog', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", primeng_dialog__WEBPACK_IMPORTED_MODULE_8__["Dialog"])
    ], SidesDialogComponent.prototype, "detaildialog", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('file', { static: false }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], SidesDialogComponent.prototype, "file", void 0);
    SidesDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sides-dialog',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sides-dialog.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sides-dialog.component.scss */ "./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_4__["ProductionExtraService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _angular_cdk_layout__WEBPACK_IMPORTED_MODULE_2__["MediaMatcher"],
            _services_file_upload_service__WEBPACK_IMPORTED_MODULE_5__["FileUploadService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_10__["MessageDispatchService"]])
    ], SidesDialogComponent);
    return SidesDialogComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.scss":
/*!******************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.scss ***!
  \******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2lkZXMtbGlzdC1pdGVtL3NpZGVzLWxpc3QtaXRlbS5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.ts":
/*!****************************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: SidesListItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidesListItemComponent", function() { return SidesListItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");
/* harmony import */ var primeng_api__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! primeng/api */ "./node_modules/primeng/fesm5/primeng-api.js");
/* harmony import */ var _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../../services/confirm-decision-subscription.service */ "./src/app/services/confirm-decision-subscription.service.ts");








var SidesListItemComponent = /** @class */ (function () {
    function SidesListItemComponent(productionDialogService, productionExtraService, messageDispatchService, messageService, decisionDispatcher) {
        this.productionDialogService = productionDialogService;
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
        this.messageService = messageService;
        this.decisionDispatcher = decisionDispatcher;
    }
    SidesListItemComponent.prototype.ngOnInit = function () {
    };
    SidesListItemComponent.prototype.ngOnDestroy = function () {
        if (this.decisionDispatch) {
            this.decisionDispatch.unsubscribe();
        }
    };
    SidesListItemComponent.prototype.viewDetails = function () {
        this.productionDialogService.showSidesDialog(this.detail);
    };
    SidesListItemComponent.prototype.deleteDetails = function () {
        var _this = this;
        this.decisionDispatcher.poseConfirmDecision('Delete side ' + this.detail.name + '?', 'Confirm to proceed');
        this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
            .subscribe(function (decision) {
            _this.decisionDispatch.unsubscribe();
            if (decision) {
                _this.productionExtraService.deleteSide(_this.detail.id, _this.detail.productionId).subscribe(function (shootDetail) {
                    //this.shootDetail = shootDetail;
                    _this.messageDispatchService.dispatchSuccesMessage("Side '" + _this.detail.name + "' was deleted successfully", "Side Deleted");
                }, function (error) {
                    _this.messageDispatchService.dispatchErrorMessage( true ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : undefined, "Side Save Error");
                });
            }
        });
    };
    SidesListItemComponent.ctorParameters = function () { return [
        { type: _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"] },
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"] },
        { type: primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"] },
        { type: _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _models_production_models__WEBPACK_IMPORTED_MODULE_2__["Side"])
    ], SidesListItemComponent.prototype, "detail", void 0);
    SidesListItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sides-list-item',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sides-list-item.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sides-list-item.component.scss */ "./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_dialog_service__WEBPACK_IMPORTED_MODULE_3__["ProductionDialogService"],
            _services_production_extra_service__WEBPACK_IMPORTED_MODULE_5__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_4__["MessageDispatchService"],
            primeng_api__WEBPACK_IMPORTED_MODULE_6__["MessageService"],
            _services_confirm_decision_subscription_service__WEBPACK_IMPORTED_MODULE_7__["ConfirmDecisionSubscriptionService"]])
    ], SidesListItemComponent);
    return SidesListItemComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/components/sides-list/sides-list.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/sides-list/sides-list.component.scss ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ZlYXR1cmUtbW9kdWxlcy9wcm9kdWN0aW9uL2NvbXBvbmVudHMvc2lkZXMtbGlzdC9zaWRlcy1saXN0LmNvbXBvbmVudC5zY3NzIn0= */");

/***/ }),

/***/ "./src/app/feature-modules/production/components/sides-list/sides-list.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/feature-modules/production/components/sides-list/sides-list.component.ts ***!
  \******************************************************************************************/
/*! exports provided: SidesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidesListComponent", function() { return SidesListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../services/message-dispatch.service */ "./src/app/services/message-dispatch.service.ts");
/* harmony import */ var _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../services/production-extra.service */ "./src/app/services/production-extra.service.ts");




var SidesListComponent = /** @class */ (function () {
    function SidesListComponent(productionExtraService, messageDispatchService) {
        this.productionExtraService = productionExtraService;
        this.messageDispatchService = messageDispatchService;
    }
    SidesListComponent.prototype.ngOnInit = function () {
        this.editSideSubscription = this.productionExtraService.subscribeToEditSideObs()
            .subscribe(function (detail) {
        });
        this.newSideSubscription = this.productionExtraService.subscribeToCreateSideObs()
            .subscribe(function (detail) {
        });
        this.deleteSideSubscription = this.productionExtraService.subscribeToDeleteSideObs()
            .subscribe(function (detail) {
            if (detail) {
            }
        });
    };
    SidesListComponent.prototype.ngOnDestroy = function () {
        if (this.editSideSubscription) {
            this.editSideSubscription.unsubscribe();
        }
        if (this.newSideSubscription) {
            this.newSideSubscription.unsubscribe();
        }
        if (this.deleteSideSubscription) {
            this.deleteSideSubscription.unsubscribe();
        }
    };
    SidesListComponent.ctorParameters = function () { return [
        { type: _services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"] },
        { type: _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Array)
    ], SidesListComponent.prototype, "sides", void 0);
    SidesListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sides-list',
            template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./sides-list.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/feature-modules/production/components/sides-list/sides-list.component.html")).default,
            styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./sides-list.component.scss */ "./src/app/feature-modules/production/components/sides-list/sides-list.component.scss")).default]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_production_extra_service__WEBPACK_IMPORTED_MODULE_3__["ProductionExtraService"],
            _services_message_dispatch_service__WEBPACK_IMPORTED_MODULE_2__["MessageDispatchService"]])
    ], SidesListComponent);
    return SidesListComponent;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/production.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/feature-modules/production/production.module.ts ***!
  \*****************************************************************/
/*! exports provided: NewProductionModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewProductionModule", function() { return NewProductionModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _global_global_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../global/global.module */ "./src/app/global/global.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../interceptors/errorInterceptor */ "./src/app/interceptors/errorInterceptor.ts");
/* harmony import */ var _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../interceptors/jwtInterceptor */ "./src/app/interceptors/jwtInterceptor.ts");
/* harmony import */ var _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/new-production/new-production.component */ "./src/app/feature-modules/production/components/new-production/new-production.component.ts");
/* harmony import */ var _components_production_production_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/production/production.component */ "./src/app/feature-modules/production/components/production/production.component.ts");
/* harmony import */ var _production_states__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./production.states */ "./src/app/feature-modules/production/production.states.ts");
/* harmony import */ var _components_add_role_add_role_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/add-role/add-role.component */ "./src/app/feature-modules/production/components/add-role/add-role.component.ts");
/* harmony import */ var _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/new-role/new-role.component */ "./src/app/feature-modules/production/components/new-role/new-role.component.ts");
/* harmony import */ var _components_new_role_dialog_new_role_dialog_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/new-role-dialog/new-role-dialog.component */ "./src/app/feature-modules/production/components/new-role-dialog/new-role-dialog.component.ts");
/* harmony import */ var _components_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/role-list/role-list.component */ "./src/app/feature-modules/production/components/role-list/role-list.component.ts");
/* harmony import */ var _components_role_list_item_role_list_item_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/role-list-item/role-list-item.component */ "./src/app/feature-modules/production/components/role-list-item/role-list-item.component.ts");
/* harmony import */ var _components_production_list_production_list_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/production-list/production-list.component */ "./src/app/feature-modules/production/components/production-list/production-list.component.ts");
/* harmony import */ var _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/productions/productions.component */ "./src/app/feature-modules/production/components/productions/productions.component.ts");
/* harmony import */ var _components_side_quick_links_side_quick_links_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/side-quick-links/side-quick-links.component */ "./src/app/feature-modules/production/components/side-quick-links/side-quick-links.component.ts");
/* harmony import */ var _components_production_list_item_production_list_item_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/production-list-item/production-list-item.component */ "./src/app/feature-modules/production/components/production-list-item/production-list-item.component.ts");
/* harmony import */ var _components_shoot_details_list_shoot_details_list_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/shoot-details-list/shoot-details-list.component */ "./src/app/feature-modules/production/components/shoot-details-list/shoot-details-list.component.ts");
/* harmony import */ var _components_shoot_details_list_item_shoot_details_list_item_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/shoot-details-list-item/shoot-details-list-item.component */ "./src/app/feature-modules/production/components/shoot-details-list-item/shoot-details-list-item.component.ts");
/* harmony import */ var _components_audition_details_list_audition_details_list_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/audition-details-list/audition-details-list.component */ "./src/app/feature-modules/production/components/audition-details-list/audition-details-list.component.ts");
/* harmony import */ var _components_audition_details_list_item_audition_details_list_item_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/audition-details-list-item/audition-details-list-item.component */ "./src/app/feature-modules/production/components/audition-details-list-item/audition-details-list-item.component.ts");
/* harmony import */ var _components_pay_details_list_pay_details_list_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/pay-details-list/pay-details-list.component */ "./src/app/feature-modules/production/components/pay-details-list/pay-details-list.component.ts");
/* harmony import */ var _components_pay_details_list_item_pay_details_list_item_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./components/pay-details-list-item/pay-details-list-item.component */ "./src/app/feature-modules/production/components/pay-details-list-item/pay-details-list-item.component.ts");
/* harmony import */ var _components_sides_list_sides_list_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./components/sides-list/sides-list.component */ "./src/app/feature-modules/production/components/sides-list/sides-list.component.ts");
/* harmony import */ var _components_sides_list_item_sides_list_item_component__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./components/sides-list-item/sides-list-item.component */ "./src/app/feature-modules/production/components/sides-list-item/sides-list-item.component.ts");
/* harmony import */ var _components_sides_dialog_sides_dialog_component__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./components/sides-dialog/sides-dialog.component */ "./src/app/feature-modules/production/components/sides-dialog/sides-dialog.component.ts");
/* harmony import */ var _components_audition_details_dialog_audition_details_dialog_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/audition-details-dialog/audition-details-dialog.component */ "./src/app/feature-modules/production/components/audition-details-dialog/audition-details-dialog.component.ts");
/* harmony import */ var _components_pay_details_dialog_pay_details_dialog_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./components/pay-details-dialog/pay-details-dialog.component */ "./src/app/feature-modules/production/components/pay-details-dialog/pay-details-dialog.component.ts");
/* harmony import */ var _components_shoot_details_dialog_shoot_details_dialog_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./components/shoot-details-dialog/shoot-details-dialog.component */ "./src/app/feature-modules/production/components/shoot-details-dialog/shoot-details-dialog.component.ts");
/* harmony import */ var _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./services/role-dialog.service */ "./src/app/feature-modules/production/services/role-dialog.service.ts");
/* harmony import */ var _services_production_component_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./services/production-component.service */ "./src/app/feature-modules/production/services/production-component.service.ts");
/* harmony import */ var _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./services/production-dialog.service */ "./src/app/feature-modules/production/services/production-dialog.service.ts");


































//import { SidesDialogComponent } from '../../components/sides-dialog/sides-dialog.component';
//import { DropFileDirective } from '../../directives/drop-file.directive';



var NewProductionModule = /** @class */ (function () {
    function NewProductionModule() {
    }
    NewProductionModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_9__["NewProductionComponent"],
                _components_add_role_add_role_component__WEBPACK_IMPORTED_MODULE_12__["AddRoleComponent"],
                _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_13__["NewRoleComponent"],
                _components_new_role_dialog_new_role_dialog_component__WEBPACK_IMPORTED_MODULE_14__["NewRoleDialogComponent"],
                _components_role_list_role_list_component__WEBPACK_IMPORTED_MODULE_15__["RoleListComponent"],
                _components_role_list_item_role_list_item_component__WEBPACK_IMPORTED_MODULE_16__["RoleListItemComponent"],
                _components_production_production_component__WEBPACK_IMPORTED_MODULE_10__["ProductionComponent"],
                _components_production_list_production_list_component__WEBPACK_IMPORTED_MODULE_17__["ProductionListComponent"],
                _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_18__["ProductionsComponent"],
                _components_side_quick_links_side_quick_links_component__WEBPACK_IMPORTED_MODULE_19__["SideQuickLinksComponent"],
                _components_production_list_item_production_list_item_component__WEBPACK_IMPORTED_MODULE_20__["ProductionListItemComponent"],
                _components_shoot_details_list_shoot_details_list_component__WEBPACK_IMPORTED_MODULE_21__["ShootDetailsListComponent"],
                _components_shoot_details_list_item_shoot_details_list_item_component__WEBPACK_IMPORTED_MODULE_22__["ShootDetailsListItemComponent"],
                _components_audition_details_list_audition_details_list_component__WEBPACK_IMPORTED_MODULE_23__["AuditionDetailsListComponent"],
                _components_audition_details_list_item_audition_details_list_item_component__WEBPACK_IMPORTED_MODULE_24__["AuditionDetailsListItemComponent"],
                _components_pay_details_list_pay_details_list_component__WEBPACK_IMPORTED_MODULE_25__["PayDetailsListComponent"],
                _components_pay_details_list_item_pay_details_list_item_component__WEBPACK_IMPORTED_MODULE_26__["PayDetailsListItemComponent"],
                _components_sides_list_sides_list_component__WEBPACK_IMPORTED_MODULE_27__["SidesListComponent"],
                _components_sides_list_item_sides_list_item_component__WEBPACK_IMPORTED_MODULE_28__["SidesListItemComponent"],
                _components_sides_dialog_sides_dialog_component__WEBPACK_IMPORTED_MODULE_29__["SidesDialogComponent"],
                _components_audition_details_dialog_audition_details_dialog_component__WEBPACK_IMPORTED_MODULE_30__["AuditionDetailsDialogComponent"],
                _components_pay_details_dialog_pay_details_dialog_component__WEBPACK_IMPORTED_MODULE_31__["PayDetailsDialogComponent"],
                _components_shoot_details_dialog_shoot_details_dialog_component__WEBPACK_IMPORTED_MODULE_32__["ShootDetailsDialogComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _uirouter_angular__WEBPACK_IMPORTED_MODULE_3__["UIRouterModule"].forChild({ states: _production_states__WEBPACK_IMPORTED_MODULE_11__["NEWPRODUCTION_STATES"] }),
                _global_global_module__WEBPACK_IMPORTED_MODULE_5__["GlobalModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"],
            ],
            exports: [
            // DropFileDirective,
            ],
            entryComponents: [
                _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_13__["NewRoleComponent"]
            ],
            providers: [
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"], useClass: _interceptors_jwtInterceptor__WEBPACK_IMPORTED_MODULE_8__["JwtInterceptor"], multi: true },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HTTP_INTERCEPTORS"], useClass: _interceptors_errorInterceptor__WEBPACK_IMPORTED_MODULE_7__["ErrorInterceptor"], multi: true },
                _services_role_dialog_service__WEBPACK_IMPORTED_MODULE_33__["RoleDialogService"],
                _services_production_component_service__WEBPACK_IMPORTED_MODULE_34__["ProductionComponentService"],
                _services_production_dialog_service__WEBPACK_IMPORTED_MODULE_35__["ProductionDialogService"],
            ],
        })
    ], NewProductionModule);
    return NewProductionModule;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/production.states.ts":
/*!*****************************************************************!*\
  !*** ./src/app/feature-modules/production/production.states.ts ***!
  \*****************************************************************/
/*! exports provided: getMetadataFromServer, getEmptyProduction, getProductionFromTransactionOrServer, getProductionsFromServer, getEmptyRole, getRoleFromTransactionOrServer, getRolesFromServer, productionListState, productionState, newproductionState, draftproductionState, editproductionState, productionNewRoleState, draftproductionNewRoleState, editRoleState, NEWPRODUCTION_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMetadataFromServer", function() { return getMetadataFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyProduction", function() { return getEmptyProduction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionFromTransactionOrServer", function() { return getProductionFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProductionsFromServer", function() { return getProductionsFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEmptyRole", function() { return getEmptyRole; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRoleFromTransactionOrServer", function() { return getRoleFromTransactionOrServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRolesFromServer", function() { return getRolesFromServer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionListState", function() { return productionListState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionState", function() { return productionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "newproductionState", function() { return newproductionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draftproductionState", function() { return draftproductionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editproductionState", function() { return editproductionState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "productionNewRoleState", function() { return productionNewRoleState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draftproductionNewRoleState", function() { return draftproductionNewRoleState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editRoleState", function() { return editRoleState; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NEWPRODUCTION_STATES", function() { return NEWPRODUCTION_STATES; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @uirouter/angular */ "./node_modules/@uirouter/angular/lib/index.js");
/* harmony import */ var _components_production_production_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./components/production/production.component */ "./src/app/feature-modules/production/components/production/production.component.ts");
/* harmony import */ var _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/new-role/new-role.component */ "./src/app/feature-modules/production/components/new-role/new-role.component.ts");
/* harmony import */ var _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/productions/productions.component */ "./src/app/feature-modules/production/components/productions/productions.component.ts");
/* harmony import */ var _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/new-production/new-production.component */ "./src/app/feature-modules/production/components/new-production/new-production.component.ts");
/* harmony import */ var _services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/state-transition-handler.service */ "./src/app/services/state-transition-handler.service.ts");







function getMetadataFromServer(stateTransitionService, transition) {
    var metaDate;
    var promise;
    return stateTransitionService.getMetadataFromServer(transition);
}
function getEmptyProduction() {
    var prod;
    return prod;
}
function getProductionFromTransactionOrServer(stateTransitionService, transition) {
    var productionId;
    var production;
    var promise;
    return stateTransitionService.getProductionFromTransactionOrServer(transition);
}
function getProductionsFromServer(stateTransitionService, transition) {
    var productions;
    var promise;
    return stateTransitionService.getProductionsFromServer(transition);
}
function getEmptyRole() {
    var prod;
    return prod;
}
function getRoleFromTransactionOrServer(stateTransitionService, transition) {
    var roleId;
    var role;
    var promise;
    return stateTransitionService.getRoleFromTransactionOrServer(transition);
}
function getRolesFromServer(stateTransitionService, transition) {
    var roles;
    var promise;
    return stateTransitionService.getRolesFromServer(transition);
}
var productionListState = {
    name: 'productions',
    url: '/productions',
    parent: "mainPulloutScrollContainer",
    views: {
        "main@mainPulloutScrollContainer": { component: _components_productions_productions_component__WEBPACK_IMPORTED_MODULE_4__["ProductionsComponent"] }
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'productions', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionsFromServer
        }
    ]
};
var productionState = {
    name: 'production',
    url: '/productions/:productionId',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_production_production_component__WEBPACK_IMPORTED_MODULE_2__["ProductionComponent"] }
    },
    params: {
        production: null
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        }
    ]
};
var newproductionState = {
    name: 'newproduction',
    url: '/productions/new',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_5__["NewProductionComponent"] },
    },
    params: {
        production: null,
        popRole: false,
        isDraft: false,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ]
};
var draftproductionState = {
    name: 'draftproduction',
    url: '/productions/draft/:draftId',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_5__["NewProductionComponent"] },
    },
    params: {
        production: null,
        popRole: false,
        isDraft: true,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var editproductionState = {
    name: 'editproduction',
    url: '/productions/edit/:productionId',
    parent: "mainPulloutNonScrollContainer",
    views: {
        "main@mainPulloutNonScrollContainer": { component: _components_new_production_new_production_component__WEBPACK_IMPORTED_MODULE_5__["NewProductionComponent"] },
    },
    params: {
        production: null,
        popRole: false,
        isDraft: false,
        isEdit: true
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var productionNewRoleState = {
    name: 'productionnewrole',
    url: '/productions/:productionId/roles/new',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_3__["NewRoleComponent"] }
    },
    params: {},
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ]
};
var draftproductionNewRoleState = {
    name: 'draftproductionnewrole',
    url: '/productions/draft/:draftId/roles/new',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_3__["NewRoleComponent"] },
    },
    params: {
        // production: null,
        popRole: false,
        isDraft: true,
        isEdit: false
    },
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var editRoleState = {
    name: 'editrole',
    url: '/productions/:productionId/roles/:roleId/edit',
    parent: "mainSidePulloutScrollContainer",
    views: {
        "main@mainSidePulloutScrollContainer": { component: _components_new_role_new_role_component__WEBPACK_IMPORTED_MODULE_3__["NewRoleComponent"] },
    },
    params: {},
    resolve: [
        // All the folders are fetched from the Folders service
        {
            token: 'production', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getProductionFromTransactionOrServer
        },
        {
            token: 'role', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getRoleFromTransactionOrServer
        },
        {
            token: 'metadata', deps: [_services_state_transition_handler_service__WEBPACK_IMPORTED_MODULE_6__["StateTransitionHandlerService"], _uirouter_angular__WEBPACK_IMPORTED_MODULE_1__["Transition"]], resolveFn: getMetadataFromServer
        },
    ],
};
var NEWPRODUCTION_STATES = [
    productionListState,
    productionState,
    newproductionState,
    draftproductionState,
    editproductionState,
    productionNewRoleState,
    draftproductionNewRoleState,
    editRoleState,
];


/***/ }),

/***/ "./src/app/feature-modules/production/services/production-component.service.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/feature-modules/production/services/production-component.service.ts ***!
  \*************************************************************************************/
/*! exports provided: ProductionComponentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionComponentService", function() { return ProductionComponentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var ProductionComponentService = /** @class */ (function () {
    function ProductionComponentService() {
        this._saveProductionIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.saveProductionIntentObs = this._saveProductionIntentObs.asObservable();
        this._cancelEditProductionIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.cancelEditProductionIntentObs = this._cancelEditProductionIntentObs.asObservable();
        this._deleteProductionIntentObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.deleteProductionIntentObs = this._deleteProductionIntentObs.asObservable();
    }
    ProductionComponentService.prototype.subscribeToSaveProductionIntentObs = function () {
        return this.saveProductionIntentObs;
    };
    ProductionComponentService.prototype.trySaveProduction = function () {
        this._saveProductionIntentObs.next(true);
    };
    ProductionComponentService.prototype.subscribeToCancelEditProductionIntentObs = function () {
        return this.cancelEditProductionIntentObs;
    };
    ProductionComponentService.prototype.tryCancelEditProduction = function () {
        this._cancelEditProductionIntentObs.next(true);
    };
    ProductionComponentService.prototype.subscribeToDeleteProductionIntentObs = function () {
        return this.deleteProductionIntentObs;
    };
    ProductionComponentService.prototype.tryDeleteProduction = function () {
        this._deleteProductionIntentObs.next(true);
    };
    ProductionComponentService.prototype.registerProductionScrollElement = function (element) {
        this.productionElement = element;
    };
    ProductionComponentService.prototype.registerOtherScrollElement = function (element) {
        this.otherElement = element;
    };
    ProductionComponentService.prototype.registerArtisticScrollElement = function (element) {
        this.artisticElement = element;
    };
    ProductionComponentService.prototype.registerCastingScrollElement = function (element) {
        this.castingElement = element;
    };
    ProductionComponentService.prototype.registerRolesScrollElement = function (element) {
        this.rolesElement = element;
    };
    ProductionComponentService.prototype.registerBasicScrollElement = function (element) {
        this.basicElement = element;
    };
    //
    ProductionComponentService.prototype.scrollToProduction = function () {
        this.productionElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToOther = function () {
        this.otherElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToArtistic = function () {
        this.artisticElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToCasting = function () {
        this.castingElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToRoles = function () {
        this.rolesElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService.prototype.scrollToBasic = function () {
        this.basicElement.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'center' });
    };
    ProductionComponentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ProductionComponentService);
    return ProductionComponentService;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/services/production-dialog.service.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/feature-modules/production/services/production-dialog.service.ts ***!
  \**********************************************************************************/
/*! exports provided: AuditionDetailDialogObject, NewRoleDialogObject, ShootDetailDialogObject, PayDetailDialogObject, SideDialogObject, ProductionDialogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuditionDetailDialogObject", function() { return AuditionDetailDialogObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewRoleDialogObject", function() { return NewRoleDialogObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShootDetailDialogObject", function() { return ShootDetailDialogObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayDetailDialogObject", function() { return PayDetailDialogObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SideDialogObject", function() { return SideDialogObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductionDialogService", function() { return ProductionDialogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var AuditionDetailDialogObject = /** @class */ (function () {
    function AuditionDetailDialogObject() {
    }
    AuditionDetailDialogObject = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        })
    ], AuditionDetailDialogObject);
    return AuditionDetailDialogObject;
}());

var NewRoleDialogObject = /** @class */ (function () {
    function NewRoleDialogObject() {
    }
    return NewRoleDialogObject;
}());

var ShootDetailDialogObject = /** @class */ (function () {
    function ShootDetailDialogObject() {
    }
    return ShootDetailDialogObject;
}());

var PayDetailDialogObject = /** @class */ (function () {
    function PayDetailDialogObject() {
    }
    return PayDetailDialogObject;
}());

var SideDialogObject = /** @class */ (function () {
    function SideDialogObject() {
    }
    return SideDialogObject;
}());

var ProductionDialogService = /** @class */ (function () {
    function ProductionDialogService() {
        this._sidesDialogShowStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._payDetailsDialogShowStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._shootDetailsDialogShowStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._auditionDetailsDialogShowStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this._newRoleDialogShowStateObs = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.newRoleDialogShowStateObs = this._newRoleDialogShowStateObs.asObservable();
        this.auditionDetailsDialogShowStateObs = this._auditionDetailsDialogShowStateObs.asObservable();
        this.shootDetailsDialogShowStateObs = this._shootDetailsDialogShowStateObs.asObservable();
        this.payDetailsDialogShowStateObs = this._payDetailsDialogShowStateObs.asObservable();
        this.sidesDialogShowStateObs = this._sidesDialogShowStateObs.asObservable();
    }
    ProductionDialogService.prototype.subscribeToNewRoleDialogShowStateObs = function () {
        return this._newRoleDialogShowStateObs;
    };
    ProductionDialogService.prototype.subscribeToAuditionDetailsDialogShowStateObs = function () {
        return this._auditionDetailsDialogShowStateObs;
    };
    ProductionDialogService.prototype.subscribeToShootDetailsDialogShowStateObs = function () {
        return this._shootDetailsDialogShowStateObs;
    };
    ProductionDialogService.prototype.subscribeToPayDetailsDialogShowStateObs = function () {
        return this._payDetailsDialogShowStateObs;
    };
    ProductionDialogService.prototype.subscribeToSidesDialogShowStateObs = function () {
        return this._sidesDialogShowStateObs;
    };
    ProductionDialogService.prototype.showSidesDialog = function (side) {
        this._sidesDialogIsVisible = true;
        this._sidesDialogShowStateObs.next({ side: side, isVisible: this._sidesDialogIsVisible });
    };
    ProductionDialogService.prototype.hideSidesDialog = function () {
        this._sidesDialogIsVisible = false;
        this._sidesDialogShowStateObs.next({ side: null, isVisible: this._sidesDialogIsVisible });
    };
    ProductionDialogService.prototype.toggleSidesDialog = function () {
        this._sidesDialogIsVisible = !this._sidesDialogIsVisible;
        this._sidesDialogShowStateObs.next({ side: null, isVisible: this._sidesDialogIsVisible });
    };
    ProductionDialogService.prototype.showPayDetailsDialog = function (payDetail) {
        this._payDetailsDialogIsVisible = true;
        this._payDetailsDialogShowStateObs.next({ payDetail: payDetail, isVisible: this._payDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.hidePayDetailsDialog = function () {
        this._payDetailsDialogIsVisible = false;
        this._payDetailsDialogShowStateObs.next({ payDetail: null, isVisible: this._payDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.togglePayDetailsDialog = function () {
        this._payDetailsDialogIsVisible = !this._payDetailsDialogIsVisible;
        this._payDetailsDialogShowStateObs.next({ payDetail: null, isVisible: this._payDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.showShootDetailsDialog = function (shootDetail) {
        this._shootDetailsDialogIsVisible = true;
        this._shootDetailsDialogShowStateObs.next({ shootDetail: shootDetail, isVisible: this._shootDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.hideShootDetailsDialog = function () {
        this._shootDetailsDialogIsVisible = false;
        this._shootDetailsDialogShowStateObs.next({ shootDetail: null, isVisible: this._shootDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.toggleShootDetailsDialog = function () {
        this._shootDetailsDialogIsVisible = !this._shootDetailsDialogIsVisible;
        this._shootDetailsDialogShowStateObs.next({ shootDetail: null, isVisible: this._shootDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.showAuditionDetailsDialog = function (auditionDetail) {
        this._auditionDetailsDialogIsVisible = true;
        this._auditionDetailsDialogShowStateObs.next({ auditionDetail: auditionDetail, isVisible: this._auditionDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.hideAuditionDetailsDialog = function () {
        this._auditionDetailsDialogIsVisible = false;
        this._auditionDetailsDialogShowStateObs.next({ auditionDetail: null, isVisible: this._auditionDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.toggleAuditionDetailsDialog = function () {
        this._auditionDetailsDialogIsVisible = !this._auditionDetailsDialogIsVisible;
        this._auditionDetailsDialogShowStateObs.next({ auditionDetail: null, isVisible: this._auditionDetailsDialogIsVisible });
    };
    ProductionDialogService.prototype.showNewRolesDialog = function (newRole) {
        this._newRoleDialogIsVisible = true;
        this._newRoleDialogShowStateObs.next({ role: newRole, isVisible: this._newRoleDialogIsVisible });
    };
    ProductionDialogService.prototype.hideNewRolesDialog = function () {
        this._newRoleDialogIsVisible = false;
        this._newRoleDialogShowStateObs.next({ role: null, isVisible: this._newRoleDialogIsVisible });
    };
    ProductionDialogService.prototype.toggleNewRolesDialog = function () {
        this._newRoleDialogIsVisible = !this._newRoleDialogIsVisible;
        this._newRoleDialogShowStateObs.next({ role: null, isVisible: this._newRoleDialogIsVisible });
    };
    ProductionDialogService.prototype.uploadSide = function () {
    };
    ProductionDialogService.prototype.pasteAndSaveSide = function () {
    };
    return ProductionDialogService;
}());



/***/ }),

/***/ "./src/app/feature-modules/production/services/role-dialog.service.ts":
/*!****************************************************************************!*\
  !*** ./src/app/feature-modules/production/services/role-dialog.service.ts ***!
  \****************************************************************************/
/*! exports provided: RoleDialogService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleDialogService", function() { return RoleDialogService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_production_models__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../models/production.models */ "./src/app/models/production.models.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var RoleDialogService = /** @class */ (function () {
    function RoleDialogService() {
        this._editRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.editRoleObs = this._editRoleObs.asObservable();
        this._newRoleObs = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Subject"]();
        this.newRoleObs = this._newRoleObs.asObservable();
    }
    RoleDialogService.prototype.subscribeToCreateRoleObs = function () {
        return this.newRoleObs;
    };
    RoleDialogService.prototype.subscribeToEditRoleObs = function () {
        return this.editRoleObs;
    };
    RoleDialogService.prototype.createRole = function (production) {
        var transferObject = new _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ProductionRoleTransferObject"];
        transferObject.production = production;
        /* const dialogRef = this.dialog.open(NewRoleComponent, {
           //width: '90%',
             data: transferObject,
           autoFocus: false,
           backdropClass: 'logindialog-overlay',
           panelClass: 'logindialog-panel'
         });
   
         dialogRef.afterClosed().subscribe(result => {
             let role: Role = result;
             this._newRoleObs.next(role);
           // this.animal = result;
         });*/
        return this.newRoleObs;
    };
    RoleDialogService.prototype.editRole = function (production, role) {
        var transferObject = new _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ProductionRoleTransferObject"];
        transferObject.production = production;
        transferObject.role = role;
        /* const dialogRef = this.dialog.open(NewRoleComponent, {
           //width: '90%',
           data: transferObject,
           autoFocus: false,
           backdropClass: 'logindialog-overlay',
           panelClass: 'logindialog-panel'
         });
   
           dialogRef.afterClosed().subscribe((result: Role) => {
          
          // let role: Role = result;
               this._editRoleObs.next(result);
           // this.animal = result;
         });*/
        return this.editRoleObs;
    };
    RoleDialogService.prototype.deleteRole = function (production, role) {
        var transferObject = new _models_production_models__WEBPACK_IMPORTED_MODULE_2__["ProductionRoleTransferObject"];
        transferObject.production = production;
        transferObject.role = role;
        /*  const dialogRef = this.dialog.open(NewRoleComponent, {
            //width: '90%',
            data: transferObject,
            autoFocus: false,
            backdropClass: 'logindialog-overlay',
            panelClass: 'logindialog-panel'
          });
    
          dialogRef.afterClosed().subscribe(result => {
            let role: Role = result;
            this._editRoleObs.next(role);
            // this.animal = result;
          });*/
        return this.editRoleObs;
    };
    RoleDialogService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RoleDialogService);
    return RoleDialogService;
}());



/***/ })

}]);
//# sourceMappingURL=feature-modules-production-production-module.js.map