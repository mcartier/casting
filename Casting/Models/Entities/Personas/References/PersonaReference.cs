﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Assets;
using Casting.Model.Enums;

namespace Casting.Model.Entities.Personas.References
{
    public abstract class PersonaReference :Logger
    {
        [Key]
        public int Id { get; set; }
        public int PersonaId { get; set; }
        public Persona Persona { get; set; }
        public PersonaReferenceTypeEnum PersonaReferenceType { get; set; }
        public int OrderIndex { get; set; }
    }
}
