﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Casting.Model;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;

namespace Casting.DAL.Configurations
{
    class ResumeConfiguration : EntityTypeConfiguration<Resume>
    {
        public ResumeConfiguration()
        {
            HasKey(x => x.Id);
          
        }
    }
    class ResumeReferenceConfiguration : EntityTypeConfiguration<ResumeReference>
    {
        public ResumeReferenceConfiguration()
        {
            HasKey(x => x.Id);
            HasRequired(m => m.Resume);

        }
    }
    class PersonaResumeReferenceConfiguration : EntityTypeConfiguration<PersonaResumeReference>
    {
        public PersonaResumeReferenceConfiguration()
        {
            ToTable("PersonaResumeReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Resume);
            HasRequired(p => p.Persona)
                .WithMany(r => r.ResumeReferences)
                .HasForeignKey(k => k.PersonaId);


        }
    }
    class BasketResumeReferenceConfiguration : EntityTypeConfiguration<BasketResumeReference>
    {
        public BasketResumeReferenceConfiguration()
        {
            ToTable("BasketResumeReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Resume);
            HasRequired(p => p.Basket)
                .WithMany(r => r.ResumeReferences)
                .HasForeignKey(k => k.BasketId);
        }
    }

    class ProfileResumeReferenceConfiguration : EntityTypeConfiguration<ProfileResumeReference>
    {
        public ProfileResumeReferenceConfiguration()
        {
            ToTable("ProfileResumeReference");
            HasKey(x => x.Id);
            HasRequired(m => m.Resume);
            HasRequired(p => p.Profile)
                .WithMany(r => r.ResumeReferences)
                .HasForeignKey(k => k.ProfileId);
        }
    }
}
;