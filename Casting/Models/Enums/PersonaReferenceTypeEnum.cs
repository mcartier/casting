﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum PersonaReferenceTypeEnum
    {
        HouseTalent = 1,
        StudioTalent = 2,
        CastingCallRoleSubmission = 3,
        CastingCallFolderRoleSubmission = 4,
        CastingSessionRoleSelection = 5,
        CastingSessionFolderRoleSelection = 6
    }
}
