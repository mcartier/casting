import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { Production, AuditionDetail, ShootDetail, PayDetail, Side } from '../../../../models/production.models';
import { ProductionDialogService } from '../../services/production-dialog.service';
import { MessageDispatchService } from '../../../../services/message-dispatch.service';
import { ProductionExtraService } from '../../../../services/production-extra.service';
import { MessageService } from 'primeng/api';
import { ConfirmDecisionSubscriptionService } from '../../../../services/confirm-decision-subscription.service';

@Component({
  selector: 'app-shoot-details-list-item',
  templateUrl: './shoot-details-list-item.component.html',
    styleUrls: ['./shoot-details-list-item.component.scss']
})
export class ShootDetailsListItemComponent implements OnInit {

    constructor(
        private productionDialogService: ProductionDialogService,
        private productionExtraService: ProductionExtraService,
        private messageDispatchService: MessageDispatchService,
        private messageService: MessageService,
        private decisionDispatcher: ConfirmDecisionSubscriptionService
    ) { }

    private decisionDispatch: Subscription;
    @Input() detail: ShootDetail;

    ngOnInit() {
    }

    ngOnDestroy() {

        if (this.decisionDispatch) {
            this.decisionDispatch.unsubscribe();

      }
     
    }
  private viewDetails() {
    this.productionDialogService.showShootDetailsDialog(this.detail);

  }
    private deleteDetails() {
             this.decisionDispatcher.poseConfirmDecision('Delete shootdetail ' + this.detail.name + '?', 'Confirm to proceed');
         this.decisionDispatch = this.decisionDispatcher.subscribeToConfirmDecisionDispatcherObs()
        .subscribe(
          (decision: boolean) => {
              this.decisionDispatch.unsubscribe();

              if (decision) {
                this.productionExtraService.deleteShootDetail(this.detail.id, this.detail.productionId).subscribe(
                  (shootDetail: ShootDetail) => {
                    //this.shootDetail = shootDetail;
                    this.messageDispatchService.dispatchSuccesMessage("Shoot detail '" + this.detail.name + "' was deleted successfully",
                      "Shoot Detail Deleted");

                  },
                  (error) => {
                    this.messageDispatchService.dispatchErrorMessage(
                      "Error saving shoot detail '" + this.detail.name + "'. " + (error) ? (error.error) ? (error.error.exceptionMessage) ? error.error.exceptionMessage : error.error : error : "unknown error",
                      "Shoot Detail Save Error");
                  });

              }
          });
    
    
  }
    
}
