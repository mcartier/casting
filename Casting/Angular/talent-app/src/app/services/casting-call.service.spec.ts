import { TestBed } from '@angular/core/testing';

import { CastingCallService } from './casting-call.service';

describe('CastingCallService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CastingCallService = TestBed.get(CastingCallService);
    expect(service).toBeTruthy();
  });
});
