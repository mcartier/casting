﻿export class MetadataItem {
  id: number;
    name: string;
    createdDate: Date;
    lastModifiedDate: Date;
}
export class ProjectTypeCategoryMetadataItem {
  id: number;
    name: string;
    productionTypes:MetadataItem[];
  createdDate: Date;
  lastModifiedDate: Date;
}
export class MetadataModel {
    unions: MetadataItem[];
    stereoTypes: MetadataItem
    ethnicStereoTypes: MetadataItem[];
    ethnicities: MetadataItem[];
    languages: MetadataItem[];
    accents: MetadataItem[];
    bodyTypes: MetadataItem[];
    hairColors: MetadataItem[];
    eyeColors: MetadataItem[];
    roleTypes: MetadataItem[];
    roleGenderTypes: MetadataItem[];
    castingCallTypes: MetadataItem[];
    castingCallViewTypes: MetadataItem[];
    productionTypes: MetadataItem[];
    productionTypeCategories: ProjectTypeCategoryMetadataItem[];
}