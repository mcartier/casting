﻿using Casting.Model.Entities.Profiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Enums;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Entities.Users.Agents;

namespace Casting.Model.Entities.Assets.References
{
    public class ActorProfileVideoReference: VideoReference
    {
        public ActorProfileVideoReference()
        {
            base.AssetReferenceTypeEnum = AssetReferenceTypeEnum.Profile;
        }
        public int ProfileId { get; set; }
        public ActorProfile Profile { get; set; }

        public int? AgentId { get; set; }
        public Agent Agent { get; set; }
        public int? TalentId { get; set; }
        public Talent Talent;
    }
}
