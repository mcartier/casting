import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { UIRouterModule } from "@uirouter/angular";
import { Transition } from "@uirouter/angular";
import { UIRouter } from '@uirouter/angular';
import { Visualizer } from '@uirouter/visualizer';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
//import { BytesPipePipe } from '../../pipes/bytes-pipe.pipe';
//import { MatButtonModule, MatDialogModule, MatListModule, MatProgressBarModule, MatIconModule, MatCardModule } from '@angular/material';
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RECORDINGS_STATES } from './recording.states';
import { GlobalModule } from '../../global/global.module';
import { RecordingsComponent } from './components/recordings/recordings.component';
import { RecordingItemComponent } from './components/recording-item/recording-item.component';
import { RecordingsContainerComponent } from './components/recordings-container/recordings-container.component';
import { NewRecordingComponent } from './components/new-recording/new-recording.component';
import { VideoRecorderModule } from '../../video-recorder/video-recorder.module';
//import { BytesPipePipe } from '../../pipes/bytes-pipe.pipe';
@NgModule({
    declarations: [
      RecordingsComponent,
        RecordingItemComponent,
        RecordingsContainerComponent,
        NewRecordingComponent,
       //BytesPipePipe
      
      ],
  imports: [
      CommonModule,
      UIRouterModule.forChild({ states: RECORDINGS_STATES }),
     // GlobalModule,
      VideoRecorderModule,
   // MatButtonModule, MatDialogModule, MatListModule,
     // MatProgressBarModule, MatIconModule, MatCardModule,
      GlobalModule,
      FormsModule,
      ReactiveFormsModule,
     // BrowserAnimationsModule, 
    ],
    entryComponents: [
        NewRecordingComponent
    ],
    exports: [
      //  BytesPipePipe
    ]
})
export class RecordingManagerModule { }
