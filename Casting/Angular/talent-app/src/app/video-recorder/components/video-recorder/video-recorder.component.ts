import { Inject } from '@angular/core';
import { Component, OnDestroy, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Subscription } from 'rxjs';
import { MediaSourceState, CameraState, ServiceState, MediaSources } from '../../video-recorder'
//import { MatDialog} from '@angular/material';

import { VideoRecorderService } from '../../video-recorder.service'
import { MediaSourceSettingsComponent } from '../media-source-settings/media-source-settings.component'


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
    selector: 'app-video-recorder',
    templateUrl: './video-recorder.component.html',
    styleUrls: ['./video-recorder.component.scss']

})
export class VideoRecorderComponent implements OnInit {


    constructor(private recorderService: VideoRecorderService) { }
    private settingsAreVisible = false;
    private ready: boolean = false;
    private isRecordNow: boolean = false;
    private video: HTMLVideoElement;

    private videoBlob: Blob;
    private blobDataURL: any;
    @ViewChild('videoElement', {static: false}) videoElement;

    private audioSource: any = null;
    private videoSource: any = null;

    private videoDeviceInfos: any[] = [];
    private audioDeviceInfos: any[] = [];
    private audioOutputDeviceInfos: any[] = [];

    private deviceInfoIsLoading: boolean;
    private deviceInfoIsError: boolean;
    private deviceInfoError: any;

    private camera: MediaStream;
    private cameraIsLoading: boolean;
    private cameraIsError: boolean;
    private cameraError: any;

    private mediaSourcesStateSubscription: Subscription;
    private cameraStateSubscription: Subscription;
    private videoServiceStateSubscription: Subscription;

    private serviceIsLoading: boolean;

   /* openDialog(): void {
        const dialogRef = this.dialog.open(MediaSourceSettingsComponent, {
        width: '90%',
        data: {  }
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
       // this.animal = result;
      });
    }*/

    private processMediaSourcesState(mediaSourcesState: MediaSources) {


        this.videoDeviceInfos = mediaSourcesState.videoDeviceInfos;
        this.audioDeviceInfos = mediaSourcesState.audioDeviceInfos;
        this.audioOutputDeviceInfos = mediaSourcesState.audioOutpurDeviceInfos;
        this.deviceInfoIsLoading = mediaSourcesState.isLoading;
        this.deviceInfoError = mediaSourcesState.error;
        this.deviceInfoIsError = mediaSourcesState.isError;
        if (!mediaSourcesState.isError) {

            if (this.videoDeviceInfos.length > 0) {
                this.videoSource = this.videoDeviceInfos[0];
            }
            if (this.audioDeviceInfos.length > 0) {
                this.audioSource = this.audioDeviceInfos[0];
            }
            if (this.audioSource && this.videoSource) {
                this.recorderService.startCapture(this.videoSource, this.audioSource);
            }
        }
    }

    private processCameraState(cameraState: CameraState) {
        this.cameraError = cameraState.error;
        this.cameraIsError = cameraState.isError;
        this.cameraIsLoading = cameraState.isLoading;

        if (!this.cameraIsError && !this.cameraIsLoading) {
           // localStorage.setItem('audioSource', JSON.stringify(this.audioSource));
           // localStorage.setItem('videoSource', JSON.stringify(this.videoSource));
           this.camera = cameraState.camera;
           this.video.muted = true;
          this.video.volume = 0;
          this.video.srcObject = this.camera;
          this.video.play();
          this.ready = true;
          this.settingsAreVisible = false;

        }
        if (this.cameraIsError) {
          //this.openDialog();
         this.settingsAreVisible = true;
        }
    }
    private processServiceState(serviceStateIdLoading: boolean) {
        this.serviceIsLoading = serviceStateIdLoading;
    }

    ngOnInit() {

     // this.mediaSourcesStateSubscription = this.recorderService.registerForMediaSourcesState()
      //      .subscribe(mediaSourcesState => this.processMediaSourcesState(mediaSourcesState));

        this.cameraStateSubscription = this.recorderService.registerForCameraState()
            .subscribe(cameraState => this.processCameraState(cameraState));

        this.videoServiceStateSubscription = this.recorderService.registerForServiceState()
            .subscribe(serviceState => this.processServiceState(serviceState));
        if (localStorage.getItem('videoSource')) {
            this.videoSource = JSON.parse(localStorage.getItem('videoSource'));
        }

        if (localStorage.getItem('audioSource')) {
            this.audioSource = JSON.parse(localStorage.getItem('audioSource'));
        }
        if (!this.audioSource || !this.videoSource) {
            //this.recorderService.requestDevicesFromSystem();

            this.settingsAreVisible = true;
           // this.openDialog();
        }
        if (this.audioSource && this.videoSource && !this.cameraIsLoading && !this.cameraIsError) {
          this.recorderService.startCapture(this.videoSource,this.audioSource);
        }
    }
    ngOnDestroy() {
       // this.mediaSourcesStateSubscription.unsubscribe();
        this.cameraStateSubscription.unsubscribe();
        this.videoServiceStateSubscription.unsubscribe();
        if (this.camera) {

          this.camera.getAudioTracks().forEach(track => track.stop());
          this.camera.getVideoTracks().forEach(track => track.stop());
          this.camera = null;
        }
    }

    ngAfterViewInit() {
        // set the initial state of the video
         this.video = this.videoElement.nativeElement;
         this.video.muted = false;
         this.video.controls = true;
     
    }

    toggleControls() {
        this.video.muted = !this.video.muted;
        this.video.controls = !this.video.controls;
        this.video.autoplay = !this.video.autoplay;
    }


   
    startRecording() {
        this.isRecordNow = true;
       // this.video.src = this.video.srcObject = null;
       // this.video.srcObject = this.camera;
       // this.video.play();
       // this.recordRTC.startRecording();
        this.recorderService.startRecording();
    }

    stopRecording() {
        this.isRecordNow = false;
       // this.recordRTC.stopRecording(this.processVideo.bind(this));
        this.recorderService.stopRecording();
    }
    setupCameraClick() {
    //  this.openDialog();
    }
    download() {
       // this.recordRTC.save('video.webm');
    }
}
