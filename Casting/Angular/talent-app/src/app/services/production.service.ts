import { Injectable, Inject } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';
import { map, retry, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Production, UpdateProduction, NewProduction, BaseProduction } from '../models/production.models';
import { WINDOW } from './browser.window.provider';
import { HttpHelpers } from '../utilities/HttpHelpers';
@Injectable({
    providedIn: 'root'
})
export class ProductionService extends HttpHelpers {

    constructor(private http: HttpClient, @Inject(WINDOW) private window: Window) {
        super(http);
        this.host = window.location.host;
        this.hostName = window.location.hostname;
        this.protocol = window.location.protocol;
        this.port = window.location.port;

    }


    private host: string;
    private hostName: string;
    private protocol: string;
    private port: string;

    private _deleteProductionObs: Subject<Production> = new Subject<Production>();
    public readonly deleteProductionObs: Observable<Production> = this._deleteProductionObs.asObservable();

    private _editProductionObs: Subject<Production> = new Subject<Production>();
    public readonly editProductionObs: Observable<Production> = this._editProductionObs.asObservable();

    private _newProductionObs: Subject<Production> = new Subject<Production>();
    public readonly newProductionObs: Observable<Production> = this._newProductionObs.asObservable();

    public subscribeToCreateProductionObs(): Observable<Production> {
        return this.newProductionObs;
    }
    public subscribeToEditProductionObs(): Observable<Production> {
        return this.editProductionObs;
    }

    public subscribeToDeleteProductionObs(): Observable<Production> {
        return this.deleteProductionObs;
    }

    private getBaseURL(): string {
        // return this.protocol + "://" + this.hostName + ":" + this.port + "/api/productions/";
        return "/api/productions/";
    }

    public createProduction(production: NewProduction): Observable<Production> {
        let controllerQuery = this.getBaseURL();
        return this.postAction<NewProduction>(production, controllerQuery)
          .pipe(
            map((res: Production) => {
              //do anything with the returned data here...
              this._newProductionObs.next(res);
              return res;
            })
          );
    };

    public saveProduction(id: number, production: UpdateProduction): Observable<Production> {
        production.id = id;
        let controllerQuery = this.getBaseURL() + id;
        return this.putAction<UpdateProduction>(production, controllerQuery)
            .pipe(
                map((res: Production) => {
                    //do anything with the returned data here...
                    this._editProductionObs.next(res);
                    return res;
                })
            );
    };


    public deleteProduction(productionId: number): Observable<Production> {
        let controllerQuery = this.getBaseURL() + productionId;
        return this.deleteAction<Production>(controllerQuery)
            .pipe(
                map((res: Production) => {
                    //do anything with the returned data here...
                    this._deleteProductionObs.next(res);
                    return res;
                })
            );
    };



    public getProductions(pageIndex: number = 0, pageSize: number = 24): Observable<Production[]> {
        let controllerQuery = this.getBaseURL() + "?pageIndex=" + pageIndex + "&pageSize=" + pageSize;
        return this.getAction<Production[]>(controllerQuery)
            .pipe(
                map((res: Production[]) => {
                    //do anything with the returned data here...
                    return res;
                })
            );
    };

    public getProduction(productionId: number): Observable<Production> {
        let controllerQuery = this.getBaseURL() + productionId;
        return this.getAction<Production>(controllerQuery)
            .pipe(
                map((res: Production) => {
                    //do anything with the returned data here...
                    return res;
                })
            );
    };
}
