﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Casting.Model.Entities.Metadata;

using Casting.Model.Entities.Attributes;

namespace Casting.Web.Models
{
    public class MetadataViewModel
    {
        public IEnumerable<Union> Unions { get; set; }
        public IEnumerable<StereoType> StereoTypes { get; set; }
        public IEnumerable<EthnicStereoType> EthnicStereoTypes { get; set; }
        public IEnumerable<Ethnicity> Ethnicities { get; set; }
        public IEnumerable<Language> Languages { get; set; }
        public IEnumerable<Accent> Accents { get; set; }
        public IEnumerable<BodyType> BodyTypes { get; set; }
        public IEnumerable<HairColor> HairColors { get; set; }
        public IEnumerable<EyeColor> EyeColors { get; set; }
        public IEnumerable<SkillCategory> SkillCategories { get; set; }
        public IEnumerable<RoleType> RoleTypes { get; set; }
        public IEnumerable<RoleGenderType> RoleGenderTypes { get; set; }
        public IEnumerable<CastingCallType> CastingCallTypes { get; set; }
        public IEnumerable<CastingCallViewType> CastingCallViewTypes { get; set; }
        public IEnumerable<CastingCallPrivacy> CastingCallPrivacies { get; set; }
        public IEnumerable<ProductionType> ProductionTypes { get; set; }
         public IEnumerable<ProductionTypeCategory> ProductionTypeCategories { get; set; }

    }
  
}