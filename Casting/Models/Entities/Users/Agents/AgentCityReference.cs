﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Locations;



namespace Casting.Model.Entities.Users.Agents
{
    public class AgentCityReference
    {
        [Key]
        public int Id { get; set; }

        public int AgentId { get; set; }
        public Agent Agent { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }

    }
}
