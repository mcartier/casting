﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Users.Agents;

namespace Casting.BLL.CastingUsers.Interfaces
{
    public interface IAgentService
    {
        Task<Agent> GetAgentByIdAsync(int agentId);
        Task<IEnumerable<Agent>> GetAgentsByAgencyIdAsync(int agentId);
        Task<IEnumerable<Agent>> GetAgentsByAgencyOfficeIdAsync(int agentId);
        Task<int> AddNewAgentAsync(Agent agent);
        Task<int> UpdateAgentAsync(Agent agent);
        Task<int> DeleteAgent(Agent agent);
    }
}