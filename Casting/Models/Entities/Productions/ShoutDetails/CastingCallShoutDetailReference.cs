﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Campaigns;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions.ShoutDetails
{
   public class CampaignShoutDetailReference : Logger
    {
        public CampaignShoutDetailReference()
        {
          

        }
        public int Id { get; set; }

        public int ShoutDetailId { get; set; }
        public ShoutDetail ShoutDetail { get; set; }



        public int CampaignId { get; set; }
        public Campaign Campaign { get; set; }

    }
}
