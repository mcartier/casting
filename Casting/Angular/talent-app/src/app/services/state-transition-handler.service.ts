import { Injectable } from '@angular/core';
import { MetadataService } from './metadata.service';
import { ProductionService } from './production.service';
import { ProductionRoleService } from './production-role.service';
import { CastingCallService } from './casting-call.service';
import { Transition } from "@uirouter/angular";
import { Production, Role, NewProduction, NewRole, UpdateProduction, UpdateRole, CompensationTypeEnum, ProductionUnionStatusEnum } from '../models/production.models';
import { CastingCall, NewCastingCall, NewCastingCallRole, UpdateCastingCall, UpdateCastingCallRole } from '../models/casting-call.models';
import { MetadataModel, MetadataItem, ProjectTypeCategoryMetadataItem } from '../models/metadata';

@Injectable({
  providedIn: 'root'
})
export class StateTransitionHandlerService {

    constructor(private productionService: ProductionService, private roleService: ProductionRoleService, private castingCallService: CastingCallService
        , private metaDataService: MetadataService) {

      //  this.productionService = productionService;
      //  this.castingCallService = castingCallService;
    }

    public getMetadataFromServer(transition: Transition): Promise<MetadataModel> {
      let metadataId: number;
        let metadata: MetadataModel;
       
    return new Promise<MetadataModel>((resolve) => {
                resolve(this.metaDataService.getMetadata());
          });
        
      
    }


    public getEmptyProduction() {
      let prod: Production;
      return prod;
    }
    public getProductionFromTransactionOrServer(transition: Transition): Promise<Production> {
      let productionId: number;
      let production: Production;
      let promise: Promise<Production>;

      if (transition.params().production) {
        return new Promise<Production>((resolve) => {
          resolve(transition.params().production);
        });
      } else {
        if (transition.params().productionId && transition.params().productionId !== 0) {
          productionId = transition.params().productionId;
        }
        else if (transition.params().draftId && transition.params().draftId !== 0) {
          productionId = transition.params().draftId;

        }
        if (productionId !== 0) {
          return new Promise<Production>((resolve) => {
            resolve(this.productionService.getProduction(productionId).toPromise());
          });
        } else {
          return new Promise<Production>((resolve) => {
            resolve(null);
          });
        }

      }
    }

    public getProductionsFromServer(transition: Transition): Promise<Production[]> {
      let productions: Production[];
      let promise: Promise<Production>;
      return new Promise<Production[]>((resolve) => {
        resolve(this.productionService.getProductions().toPromise());
      });


    }

    public getEmptyRole() {
      let prod: Role;
      return prod;
    }
    public getRoleFromTransactionOrServer(transition: Transition): Promise<Role> {
      let roleId: number;
      let productionId: number;
      let role: Role;
      let promise: Promise<Role>;

      if (transition.params().role) {
        return new Promise<Role>((resolve) => {
          resolve(transition.params().role);
        });
      } else {
        if (transition.params().roleId && transition.params().roleId !== 0) {
          roleId = transition.params().roleId;
        }
          if (transition.params().productionId && transition.params().productionId !== 0) {
              productionId = transition.params().productionId;
        }

        if (roleId !== 0 && productionId !== 0) {
          return new Promise<Role>((resolve) => {
              resolve(this.roleService.getRole(roleId, productionId).toPromise());
          });
        } else {
          return new Promise<Role>((resolve) => {
            resolve(null);
          });
        }

      }
    }

    public getRolesFromServer(transition: Transition): Promise<Role[]> {
      let roles: Role[];
      let promise: Promise<Role>;
      return new Promise<Role[]>((resolve) => {
        resolve(this.roleService.getRoles(0).toPromise());
      });


    }




public getEmptyCastingCall() {
    let prod: CastingCall;
    return prod;
}
public getCastingCallFromTransactionOrServer(transition: Transition): Promise<CastingCall> {
    let castingCallId: number;
    let castingCall: CastingCall;
    let productionId: number;
    let promise: Promise<CastingCall>;
    if (transition.params().castingCall) {
        return new Promise<CastingCall>((resolve) => {
            resolve(transition.params().castingCall);
        });
    } else {
        if (transition.params().castingCallId && transition.params().castingCallId !== 0) {
            castingCallId = transition.params().castingCallId;
        }
        else if (transition.params().draftId && transition.params().draftId !== 0) {
            castingCallId = transition.params().draftId;

        }
        if (transition.params().productionId && transition.params().productionId !== 0) {
          productionId = transition.params().productionId;
        }


        if (castingCallId !== 0) {
            return new Promise<CastingCall>((resolve) => {
                resolve(this.castingCallService.getCastingCall(castingCallId, productionId).toPromise());
            });
        } else {
            return new Promise<CastingCall>((resolve) => {
                resolve(null);
            });
        }

    }
}

public getCastingCallsFromServer(transition: Transition): Promise<CastingCall[]> {
    let castingCalls: CastingCall[];
    let promise: Promise<CastingCall>;
    return new Promise<CastingCall[]>((resolve) => {
        resolve(this.castingCallService.getCastingCalls().toPromise());
    });


}
public getProductionCastingCallsFromServer(transition: Transition): Promise<CastingCall[]> {
    let castingCalls: CastingCall[];
    let productionId: number;
   
    if (transition.params().productionId && transition.params().productionId !== 0) {
        productionId = transition.params().productionId;
    }
    if (productionId !== 0) {
        return new Promise<CastingCall[]>((resolve) => {
            resolve(this.castingCallService.getProductionCastingCalls(productionId).toPromise());
        });
    } else {
        return new Promise<CastingCall[]>((resolve) => {
            resolve(null);
        });
    }

}


}