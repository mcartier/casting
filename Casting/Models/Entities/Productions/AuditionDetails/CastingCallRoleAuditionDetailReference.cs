﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model.Entities.Campaigns;

using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Sessions;
using System.ComponentModel.DataAnnotations.Schema;
using Casting.Model.Entities.AttributeReferences;

namespace Casting.Model.Entities.Productions.AuditionDetails
{
   public class CampaignRoleAuditionDetailReference : Logger
    {
        public CampaignRoleAuditionDetailReference()
        {
          

        }
        public int Id { get; set; }
      
        public int AuditionDetailId { get; set; }
        public AuditionDetail AuditionDetail { get; set; }

        public int CampaignRoleId { get; set; }
        public CampaignRole CampaignRole { get; set; }


    }
}
