import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRecordingComponent } from './new-recording.component';

describe('NewRecordingComponent', () => {
  let component: NewRecordingComponent;
  let fixture: ComponentFixture<NewRecordingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewRecordingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRecordingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
