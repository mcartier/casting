﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Profiles;

namespace Casting.BLL.Profiles.Interfaces
{
    public interface IProfileService
    {
        Task<Profile> GetProfileByIdAsync(int profileId);
        Task<Profile> GetActorProfileByIdAsync(int profileId);
        Task<Profile> GetVoiceActorProfileByIdAsync(int profileId);
        Task<Profile> GetCompleteActorProfileByIdAsync(int profileId);
        Task<Profile> GetCompleteVoiceActorProfileByIdAsync(int profileId);

        Task<Profile> GetProfileByTalentIdAsync(int talentId);
        Task<Profile> GetCompleteActorProfileByTalentIdAsync(int talentId);
        Task<Profile> GetCompleteVoiceActorProfileByTalentIdAsync(int talentId);

        Task<IEnumerable<Profile>> GetCompleteActorProfilesByAgentIdAsync(int agentId);
        Task<IEnumerable<Profile>> GetCompleteVoiceActorProfilesByAgentIdAsync(int agentId);
        Task<IEnumerable<Profile>> GetProfilesByAgentIdAsync(int agentId);

        Task<IEnumerable<Profile>> GetCompleteActorProfilesByMainAgentIdAsync(int agentId);
        Task<IEnumerable<Profile>> GetCompleteVoiceActorProfilesByMainAgentIdAsync(int agentId);
        Task<IEnumerable<Profile>> GetProfileByMainAgentIdAsync(int agentId);

        Task<IEnumerable<Profile>> GetCompleteActorProfilesByDirectorId(int directorId);
        Task<IEnumerable<Profile>> GetCompleteVoiceActorProfilesByDirectorId(int directorId);
        Task<IEnumerable<Profile>> GetProfilesByDirectorId(int directorId);
        Task<int> AddNewProfileAsync(Profile profile);
        Task<int> UpdateProfileAsync(Profile profile);
        Task<int> DeleteProfile(Profile profile);

    }
}