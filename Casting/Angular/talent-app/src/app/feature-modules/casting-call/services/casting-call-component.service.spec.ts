import { TestBed } from '@angular/core/testing';

import { CastingCallComponentService } from './casting-call-component.service';

describe('CastingCallComponentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CastingCallComponentService = TestBed.get(CastingCallComponentService);
    expect(service).toBeTruthy();
  });
});
