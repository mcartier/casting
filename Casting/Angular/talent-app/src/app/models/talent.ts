﻿export class Talent {
    id: number;
    firstName: string;
    lastName: string;
    createdDate: Date;
}