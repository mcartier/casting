﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Sessions;

namespace Casting.BLL.CastingSessions.Interfaces
{
    public interface ICastingSessionService
    {
        Task<CastingSession> GetCastingSessionByIdWithFoldersAndRolesAsync(int castingSessionId);
        Task<CastingSession> GetCastingSessionByIdWithFoldersRolesAndPersonasAsync(int castingSessionId);
        Task<CastingSession> GetCastingSessionByIdWithFoldersRolesPersonasAndAssetsAsync(int castingSessionId);
        Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdWithFolderRolesAndRolesAsync(int productionId);
        Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdWithFolderRolesPersonasAndRolesPersonasAsync(int productionId);
        Task<IEnumerable<CastingSession>> GetCastingSessionsByProductionIdWithFolderRolesPersonasAssetsAndRolesPersonasAssetsAsync(int productionId);
        Task<int> AddNewCastingSessionAsync(CastingSession castingSession);
        Task<int> UpdateCastingSessionAsync(CastingSession castingSession);
        Task<int> DeleteCastingSession(CastingSession castingSession);
    }
}