﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.DAL.Repositories.Interfaces
{
   public interface IRepository
    {
        IEnumerable<T> GetAll<T>() where T : class;
        T GetById<T>(int id) where T : class;
        int Add<T>(T entity) where T : class;
        int Delete<T>(T entity) where T : class;
        int Update<T>(T entity) where T : class;
        void SetEntityState<T>(T entity, EntityState state) where T : class;
    }

}
