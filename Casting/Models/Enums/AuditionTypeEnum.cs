﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum AuditionTypeEnum
    {
        TapedSubmissionsOnly = 1,
        TapedSubmissionAndInPersonAudition = 3,
        TapedSubmissionThenInPersonAudition = 7,
        InPersonAuditionOnly = 15
    }
}
