﻿using Casting.Model.Entities.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Entities.Productions
{
    public class ProductionProductionTypeReference:Logger
    {
        public int Id { get; set; }
        public int ProductionId { get; set; }
        public Production Production { get; set; }
        public int ProductionTypeId { get; set; }
        public ProductionType ProducuctionType { get; set; }

    }
}
