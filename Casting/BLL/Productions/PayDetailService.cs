﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.DAL.Repositories.Interfaces;
using Casting.Model.Entities.Productions;
using Casting.BLL.Productions.Interfaces;

namespace Casting.BLL.Productions
{
    public class PayDetailService : BaseService, IPayDetailService
    {
        private IProductionAsyncRepository _productionRepository;

        public PayDetailService(IProductionAsyncRepository productionAsyncRepository)
        {
            _productionRepository = productionAsyncRepository;
        }


        public async Task<PayDetail> GetPayDetailByIdAsync(int payDetailId)
        {
            return await _productionRepository.GetPayDetailByIdAsync(payDetailId, false);
        }
           public async Task<IEnumerable<PayDetail>> GetPayDetailsByProductionIdAsync(int productionId)
        {
            return await _productionRepository.GetPayDetailsByProductionIdAsync(productionId,false);
        }
        
        public async Task<int> AddNewPayDetailAsync(PayDetail payDetail)
        {
            return await _productionRepository.AddNewPayDetailAsync(payDetail);
        }

        public async Task<int> UpdatePayDetailAsync(PayDetail payDetail)
        {
            return await _productionRepository.UpdatePayDetailAsync(payDetail);
        }

        public async Task<int> DeletePayDetailAsync(PayDetail payDetail)
        {
            return await _productionRepository.DeletePayDetailAsync(payDetail);
        }
    }
}
