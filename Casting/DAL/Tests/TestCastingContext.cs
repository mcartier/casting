﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Casting.Model;

using Casting.Model.Entities.Assets;
using Casting.Model.Entities.Assets.References;
using Casting.Model.Entities.Baskets;
using Casting.Model.Entities.CastingCalls;
using Casting.Model.Entities.Locations;
using Casting.Model.Entities.Metadata;
using Casting.Model.Entities.Personas;
using Casting.Model.Entities.Personas.References;
using Casting.Model.Entities.Productions;
using Casting.Model.Entities.Profiles;
using Casting.Model.Entities.Profiles.References;
using Casting.Model.Entities.Sessions;
using Casting.Model.Entities.Users;
using Casting.Model.Entities.Users.Agents;
using Casting.Model.Entities.Users.Directors;
using Casting.Model.Entities.Users.Guests;
using Casting.Model.Entities.Users.Talents;
using Casting.Model.Entities.AttributeReferences;

using Casting.Model.Entities.Attributes;

namespace Casting.DAL.Tests
{
    public class TestCastingContext : ICastingContext, IDisposable
    {
        public TestCastingContext()
        {
            //this.Personas = new TestTalentDbSet();
        }

        //locations

        public DbSet<Country> Countries { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<Region> Regions { get; set; }
        public DbSet<BaseCastingUser> BaseUsers { get; set; }
       // public DbSet<UserReference> UserReferences { get; set; }
        public DbSet<Persona> Personas { get; set; }
        //ublic DbSet<HouseTalentPersona> HouseTalentPersonas { get; set; }
        //ublic DbSet<StudioTalentPersona> StudioTalentPersonas { get; set; }
        public DbSet<PersonaReference> PersonaReferences { get; set; }
        public DbSet<CastingCallFolderRoleSubmissionPersonaReference> FolderRoleSubmissionPersonaReferences { get; set; }
        public DbSet<CastingCallRoleSubmissionPersonaReference> RoleSubmissionPersonaReferences { get; set; }
        public DbSet<CastingSessionFolderRoleSelectionPersonaReference> FolderRoleSelectionPersonaReferences { get; set; }
        public DbSet<CastingSessionRoleSelectionPersonaReference> RoleSelectionPersonaReferences { get; set; }

      //  public DbSet<TalentProfileReference> TalentProfileReferences { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ActorProfile> ActorProfiles { get; set; }
        public DbSet<VoiceActorProfile> VoiceActorProfiles { get; set; }
        public DbSet<Basket> Baskets { get; set; }
         public DbSet<Guest> Guests { get; set; }
        public DbSet<Director> Directors { get; set; }

        public DbSet<Agent> Agents { get; set; }
        public DbSet<Agency> Agencies { get; set; }
        public DbSet<AgencyOffice> AgencyOffices { get; set; }
        public DbSet<AgentCityReference> AgentCityReferences { get; set; }
        public DbSet<AgentTalentReference> AgentTalentReferences { get; set; }

        //talents
        public DbSet<Talent> Talents { get; set; }
        public DbSet<HouseTalent> HouseTalents { get; set; }
        public DbSet<StudioTalent> StudioTalents { get; set; }
        // public DbSet<HouseTalentPersonaReference> HouseTalentPersonaReferences { get; set; }
        // public DbSet<StudioTalentPersonaReference> StudioTalentPersonaReferences { get; set; }

        //assets
        public DbSet<Audio> Audios { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Resume> Resumes { get; set; }
        public DbSet<Image> Images { get; set; }

        //asset references
        public DbSet<AudioReference> AudioReferences { get; set; }
        public DbSet<VideoReference> VideoReferences { get; set; }
        public DbSet<ResumeReference> ResumeReferences { get; set; }
        public DbSet<ImageReference> ImageReferences { get; set; }

        //persona asset references
        public DbSet<PersonaAudioReference> PersonaAudioReferences { get; set; }
        public DbSet<PersonaVideoReference> PersonaVideoReferences { get; set; }
        public DbSet<PersonaResumeReference> PersonaResumeReferences { get; set; }
        public DbSet<PersonaImageReference> PersonaImageReferences { get; set; }

        //Basket asset references
        public DbSet<BasketAudioReference> BasketAudioReferences { get; set; }
        public DbSet<BasketVideoReference> BasketVideoReferences { get; set; }
        public DbSet<BasketResumeReference> BasketResumeReferences { get; set; }
        public DbSet<BasketImageReference> BasketImageReferences { get; set; }

        //Profile asset references
        public DbSet<ProfileAudioReference> ProfileAudioReferences { get; set; }
        public DbSet<ActorProfileVideoReference> ProfileVideoReferences { get; set; }
        public DbSet<ProfileResumeReference> ProfileResumeReferences { get; set; }
        public DbSet<ActorProfileImageReference> ProfileImageReferences { get; set; }

        //productions
        public DbSet<Production> Productions { get; set; }
        public DbSet<Role> Roles { get; set; }

        public DbSet<ShootDetail> ShootDetails { get; set; }
        public DbSet<AuditionDetail> AuditionDetails { get; set; }
        public DbSet<PayDetail> PayDetails { get; set; }
        public DbSet<Side> Sides { get; set; }
        public DbSet<File> Files { get; set; }
        //production metadata
        public DbSet<ProductionTypeCategory> ProductionTypeCategories { get; set; }

        public DbSet<ProductionType> ProductionTypes { get; set; }
        public DbSet<RoleType> RoleTypes { get; set; }
        public DbSet<RoleGenderType> RoleGenderTypes { get; set; }

        //profile attributes
        public DbSet<Union> Unions { get; set; }
        public DbSet<EthnicStereoType> EthnicStereoTypes { get; set; }
        public DbSet<Ethnicity> Ethnicities { get; set; }
        public DbSet<StereoType> StereoTypes { get; set; }
        public DbSet<Accent> Accents { get; set; }
        public DbSet<Language> Languages { get; set; }

        public DbSet<HairColor> HairColors { get; set; }
        public DbSet<EyeColor> EyeColors { get; set; }
        public DbSet<BodyType> BodyTypes { get; set; }
        public DbSet<Skill> Skills { get; set; }
        public DbSet<SkillCategory> SkillCategories { get; set; }

        // attribute references
        public DbSet<UnionReference> UnionReferences { get; set; }
        public DbSet<EthnicStereoTypeReference> EthnicStereoTypeReferences { get; set; }
        public DbSet<EthnicityReference> EthnicityReferences { get; set; }
        public DbSet<StereoTypeReference> StereoTypeReferences { get; set; }
        public DbSet<AccentReference> AccentReferences { get; set; }
        public DbSet<LanguageReference> LanguageReferences { get; set; }
        public DbSet<EyeColorReference> EyeColorReferences { get; set; }
        public DbSet<HairColorReference> HairColorReferences { get; set; }
        public DbSet<BodyTypeReference> BodyTypeReferences { get; set; }
        public DbSet<SkillReference> SkillReferences { get; set; }

        //profile attribute references
        public DbSet<ProfileUnionReference> ProfileUnionReferences { get; set; }
        public DbSet<ProfileEthnicStereoTypeReference> ProfileEthnicStereoTypeReferences { get; set; }
        public DbSet<ProfileEthnicityReference> ProfileEthnicityReferences { get; set; }
        public DbSet<ProfileStereoTypeReference> ProfileStereoTypeReferences { get; set; }
        public DbSet<ProfileAccentReference> ProfileAccentReferences { get; set; }
        public DbSet<ProfileLanguageReference> ProfileLanguageReferences { get; set; }
        public DbSet<ProfileEyeColorReference> ProfileEyeColorReferences { get; set; }
        public DbSet<ProfileHairColorReference> ProfileHairColorReferences { get; set; }
        public DbSet<ProfileBodyTypeReference> ProfileBodyTypeReferences { get; set; }
        public DbSet<ProfileSkillReference> ProfileSkillReferences { get; set; }

        //role attribute references
       public DbSet<RoleEthnicStereoTypeReference> RoleEthnicStereoTypeReferences { get; set; }
        public DbSet<RoleEthnicityReference> RoleEthnicityReferences { get; set; }
        public DbSet<RoleStereoTypeReference> RoleStereoTypeReferences { get; set; }
        public DbSet<RoleAccentReference> RoleAccentReferences { get; set; }
        public DbSet<RoleLanguageReference> RoleLanguageReferences { get; set; }
        public DbSet<RoleEyeColorReference> RoleEyeColorReferences { get; set; }
        public DbSet<RoleHairColorReference> RoleHairColorReferences { get; set; }
        public DbSet<RoleBodyTypeReference> RoleBodyTypeReferences { get; set; }
        public DbSet<RoleSkillReference> RoleSkillReferences { get; set; }

        //castingCall attribute references
        public DbSet<CastingCallUnionReference> CastingCallUnionReferences { get; set; }
        public DbSet<CastingCallRoleUnionReference> CastingCallRoleUnionReferences { get; set; }

        //profile search creiteria attribute references
        /* public DbSet<ProfileSearchCriteriaUnionReference> ProfileSearchCriteriaUnionReferences { get; set; }
         public DbSet<ProfileSearchCriteriaEthnicStereoTypeReference> ProfileSearchCriteriaEthnicStereoTypeReferences { get; set; }
         public DbSet<ProfileSearchCriteriaEthnicityReference> ProfileSearchCriteriaEthnicityReferences { get; set; }
         public DbSet<ProfileSearchCriteriaStereoTypeReference> ProfileSearchCriteriaStereoTypeReferences { get; set; }
         public DbSet<ProfileSearchCriteriaAccentReference> ProfileSearchCriteriaAccentReferences { get; set; }
         public DbSet<ProfileSearchCriteriaLanguageReference> ProfileSearchCriteriaLanguageReferences { get; set; }
         */
        //castingCalls
        public DbSet<CastingCall> CastingCalls { get; set; }
        public DbSet<CastingCallPrivacy> CastingCallPrivacies { get; set; }
        public DbSet<CastingCallViewType> CastingCallViewTypes { get; set; }
        public DbSet<CastingCallType> CastingCallTypes { get; set; }

        public DbSet<CastingCallRole> CastingCallRoles { get; set; }
        public DbSet<CastingCallFolder> CastingCallFolders { get; set; }

        public DbSet<CastingCallFolderRole> CastingCallFolderRoles { get; set; }

        public DbSet<CastingCallCityReference> CastingCallCityReferences { get; set; }
        public DbSet<CastingCallRegionReference> CastingCallRegionReferences { get; set; }
        public DbSet<CastingCallCountryReference> CastingCallCountryReferences { get; set; }


        //casting sessions

        public DbSet<CastingSession> CastingSessions { get; set; }

        public DbSet<CastingSessionRole> CastingSessionRoles { get; set; }

        public DbSet<CastingSessionFolder> CastingSessionFolders { get; set; }

        public DbSet<CastingSessionFolderRole> CastingSessionFolderRoles { get; set; }
        public int SaveChangesCount { get; private set; }

        public Database Database
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public int SaveChanges()
        {
            this.SaveChangesCount++;
            return 1;
        }

        public Task<int> SaveChangesAsync()
        {
            this.SaveChangesCount++;
            return Task.FromResult<int>(1);
        }
       

        public void SetModified(object entity)
        {
          //  throw new NotImplementedException();
        }
        public void SetState(object entity, EntityState state)
        {
           
        }

        public Task<IQueryable<Talent>> TalentsAsync()
        {
            throw new NotImplementedException();
        }

        public DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            throw new NotImplementedException();
        }

        public DbSet Set(Type entityType)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DbEntityValidationResult> GetValidationErrors()
        {
            throw new NotImplementedException();
        }

        public DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public DbEntityEntry Entry(object entity)
        {
            throw new NotImplementedException();
        }
        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                  
                }


                disposedValue = true;
            }
        }



        public void Dispose()
        {
            Dispose(true);

        }

        
        #endregion
    }
}
