﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Casting.Model.Entities.Productions;

namespace Casting.BLL.Productions.Interfaces
{

    public interface IShootDetailService
    {
        Task<ShootDetail> GetShootDetailByIdAsync(int shootDetailId);
        Task<IEnumerable<ShootDetail>> GetShootDetailsByProductionIdAsync(int productionId);
        Task<int> AddNewShootDetailAsync(ShootDetail shootDetail);
        Task<int> UpdateShootDetailAsync(ShootDetail shootDetail);
        Task<int> DeleteShootDetailAsync(ShootDetail shootDetail);
    }
}