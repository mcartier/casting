﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum ProfileAttributeTypeEnum
    {
        Language = 1,
        Accent = 2,
        Union = 3,
        Ethnicity = 4,
        EthnicStereoType = 5,
        StereoType = 6,
        BodyType =7,
        HairColor = 8,
        EyeColor = 9,
        Skill = 10
    }
}
