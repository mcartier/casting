import { Injectable } from '@angular/core';
import { ElementRef } from '@angular/core';
import { Observable, of, BehaviorSubject, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ProductionComponentService {

    constructor() { }

    private _saveProductionIntentObs: Subject<any> = new Subject();
    public readonly saveProductionIntentObs: Observable<any> = this._saveProductionIntentObs.asObservable();

    public subscribeToSaveProductionIntentObs(): Observable<any> {
      return this.saveProductionIntentObs;
    }
    public trySaveProduction() {
      this._saveProductionIntentObs.next(true);
    }
    private _cancelEditProductionIntentObs: Subject<any> = new Subject();
    public readonly cancelEditProductionIntentObs: Observable<any> = this._cancelEditProductionIntentObs.asObservable();

    public subscribeToCancelEditProductionIntentObs(): Observable<any> {
      return this.cancelEditProductionIntentObs;
    }
    public tryCancelEditProduction() {
      this._cancelEditProductionIntentObs.next(true);
    }
    private _deleteProductionIntentObs: Subject<any> = new Subject();
    public readonly deleteProductionIntentObs: Observable<any> = this._deleteProductionIntentObs.asObservable();

    public subscribeToDeleteProductionIntentObs(): Observable<any> {
      return this.deleteProductionIntentObs;
    }
    public tryDeleteProduction() {
      this._deleteProductionIntentObs.next(true);
    }

    private productionElement: ElementRef;
    private artisticElement: ElementRef;
    private castingElement: ElementRef;
    private rolesElement: ElementRef;
    private basicElement: ElementRef;
    private otherElement: ElementRef;

    public registerProductionScrollElement(element: ElementRef) {
        this.productionElement = element;
    }
    public registerOtherScrollElement(element: ElementRef) {
        this.otherElement = element;
    }
    public registerArtisticScrollElement(element: ElementRef) {
        this.artisticElement = element;
    }
    public registerCastingScrollElement(element: ElementRef) {
        this.castingElement = element;
    }
    public registerRolesScrollElement(element: ElementRef) {
        this.rolesElement = element;
    }
    public registerBasicScrollElement(element: ElementRef) {
        this.basicElement = element;
    }

    //

    public scrollToProduction() {
        this.productionElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToOther() {
        this.otherElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToArtistic() {
        this.artisticElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToCasting() {
        this.castingElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToRoles() {
        this.rolesElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
    public scrollToBasic() {
        this.basicElement.nativeElement.scrollIntoView({ behavior: 'smooth', block:'center' });
    }
   
}
