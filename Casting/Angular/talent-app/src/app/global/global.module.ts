import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { AuthenticationService } from "../services/authentication.service.js";
import { BytesPipePipe } from '../pipes/bytes-pipe.pipe';
import { LinebreakPipe } from '../pipes/linebreak.pipe';

//import { TextMaskModule } from 'angular2-text-mask';
//primeNg components
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { InputTextModule } from 'primeng/inputtext';
import { ChipsModule } from 'primeng/chips';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ListboxModule } from 'primeng/listbox';
import { MultiSelectModule } from 'primeng/multiselect';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SliderModule } from 'primeng/slider';
import { SelectButtonModule } from 'primeng/selectbutton';
import { TriStateCheckboxModule } from 'primeng/tristatecheckbox';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { CheckboxModule } from 'primeng/checkbox';
import { ColorPickerModule } from 'primeng/colorpicker';
//import { EditorModule } from 'primeng/editor';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputMaskModule } from 'primeng/inputmask';
import { PasswordModule } from 'primeng/password';
import { RatingModule } from 'primeng/rating';
import { SpinnerModule } from 'primeng/spinner';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ButtonModule } from 'primeng/button';
import { ToolbarModule } from 'primeng/toolbar';
import { CardModule } from 'primeng/card';
import { PanelModule } from 'primeng/panel';
import { TabViewModule } from 'primeng/tabview';
import { SidebarModule } from 'primeng/sidebar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { DialogModule } from 'primeng/dialog';
import { FileUploadModule } from 'primeng/fileupload';
import { ToastModule } from 'primeng/toast';
import { MenuModule } from 'primeng/menu';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { StepsModule } from 'primeng/steps';
import {FieldsetModule} from 'primeng/fieldset';
import {OverlayPanelModule} from 'primeng/overlaypanel';

import { DropFileDirective } from '../directives/drop-file.directive';
import { MessageService } from 'primeng/api';
@NgModule({
    declarations: [
        BytesPipePipe,
        DropFileDirective,
        LinebreakPipe,

  ],
    imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
    ScrollDispatchModule,
   // BrowserAnimationsModule,
   
       // TextMaskModule,
      
      //primeNg components
    DialogModule,
        MessagesModule,
        MessageModule,
      InputTextModule,
      ChipsModule,
      DropdownModule,
      CalendarModule,
      KeyFilterModule,
      ListboxModule,
      MultiSelectModule,
      RadioButtonModule,
      SliderModule,
      SelectButtonModule,
      TriStateCheckboxModule,
      AutoCompleteModule,
      CheckboxModule,
      ColorPickerModule,
     // EditorModule,
      InputSwitchModule,
      InputTextareaModule,
      InputMaskModule,
      PasswordModule,
      RatingModule,
      SpinnerModule,
      ToggleButtonModule,
      ButtonModule,
      ToolbarModule,
      CardModule,
      PanelModule,
      TabViewModule, 
        SidebarModule,
        ScrollPanelModule,
        FileUploadModule,
        ToastModule,
        MenuModule,
        ProgressSpinnerModule,
        StepsModule,
    FieldsetModule,
    OverlayPanelModule,

     ],
    exports: [
      ScrollDispatchModule,
    
        BytesPipePipe,
      LinebreakPipe,
       // TextMaskModule,
        //primeNg components
      MessagesModule,
        MessageModule,
      InputTextModule,
      ChipsModule,
      DropdownModule,
      CalendarModule,
      KeyFilterModule,
      ListboxModule,
      MultiSelectModule,
      RadioButtonModule,
      SliderModule,
      SelectButtonModule,
      TriStateCheckboxModule,
      AutoCompleteModule,
      CheckboxModule,
      ColorPickerModule,
      //EditorModule,
      InputSwitchModule,
      InputTextareaModule,
      InputMaskModule,
      PasswordModule,
      RatingModule,
      SpinnerModule,
      ToggleButtonModule,
      ButtonModule,
      ToolbarModule,
      CardModule,
      PanelModule,
      TabViewModule, 
        SidebarModule,
        ScrollPanelModule,
        DialogModule,
        FileUploadModule,
        DropFileDirective,
        ToastModule,
      MenuModule,
        ProgressSpinnerModule,
        StepsModule,
      FieldsetModule,
      OverlayPanelModule,

    ],
    providers: [
      //WINDOW_PROVIDERS,
      MessageService,
        AuthenticationService,
     ]
})
export class GlobalModule { }
