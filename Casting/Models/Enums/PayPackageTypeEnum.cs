﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Casting.Model.Enums
{
    public enum PayPackageTypeEnum
    {
        money = 1,
        credit = 3,
        gift = 7
    }
}
